<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tourfrom_balieng2' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-d6]Ky9G%@EG8T0l}}Qdh[4u,PXoUOlHl :/A*hrzW [E3#oF!U~@KOMG.P*1)Dq' );
define( 'SECURE_AUTH_KEY',  'dk1GC[?@w[}|Z<$l=mjEg:lqve>frK%+Y*[2Kz?KY$M2asuOr8F/M]I:=LoP.P!O' );
define( 'LOGGED_IN_KEY',    'rY30O`p)C 2A%|EWRjpnTKEZk_o!*(B/1&:?#gMkpDj_TvU-:T^g*0D)B0@h97D)' );
define( 'NONCE_KEY',        'K[huM-A!uB=R85$mq!4eKn~B1ni qAY~C.9s;+rCz)`P;d#+aDF%jl736*8%}c%^' );
define( 'AUTH_SALT',        '_!XG4R,f=>Z6oq-=n]4*~Cn~st#?E-CleH=BMgo<t|T0wJS^t|>LOj6:(bu<0Lpm' );
define( 'SECURE_AUTH_SALT', 'os$!h|}&P!6)HhGa7tBFBt7V1&=hR f{=Yv&g1gqR%z03^Cc%I|]qJ8xY?b:oi3@' );
define( 'LOGGED_IN_SALT',   '0>4^!6aEc-Od?a8%izqQvS3*@^4a%MUk?Z>Y@A:CI{LsF;!_{,3C1C9@W4=P$hB%' );
define( 'NONCE_SALT',       'CEYU,KlJG61-rS!;]{ryQOh/Z?{dq:.v NK#Hiyr)3B+s1_q4d31>IKIWoJ_Md?e' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
