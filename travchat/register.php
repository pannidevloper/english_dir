<?php

include('conn.php');
require_once("../wp-load.php");
session_start();
global $wpdb;

function check_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function sendmailtouser($to, $subject, $message)
{
    $key="Tt3At58P6ZoYJ0qhLvqdYyx21";
    $postdata=array('to'=>$to,
                    'subject'=>$subject,
                    'message'=>$message);
    $url="https://www.tourfrombali.com/api.php?action=sendmail&key={$key}";
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $data=curl_exec($ch);
    if (curl_errno($ch) || $data==false) {
        curl_close($ch);
        return false;
    } else {
        curl_close($ch);
        return true;
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = check_input($_POST['username']);
    $pw1 = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_SPECIAL_CHARS);
    $pw2 = filter_input(INPUT_POST, 'password2', FILTER_SANITIZE_SPECIAL_CHARS);

    $firstnameforusrmail = $_POST['firstname'];
    $lastnameforusrmail = $_POST['lastname'];
    $emailforusrmail = $_POST['email'];
    $phoneforusrmail = ltrim($_POST['phone'], '0');
    
    $from_travcust=(!empty($_GET['from']) && $_GET['from']=='travcust')?true:false;

    if (empty($username)) {
        if ($from_travcust) {
            echo "<p>The username can't be empty.</p>";
        } else {
            $_SESSION['sign_msg'] = "The username can't be empty.";
            header('location: signup.php');
        }
    } elseif (!preg_match("/^[a-zA-Z0-9_]*$/", $username)) {
        if ($from_travcust) {
            echo "<p>Username should not contain space and special characters!</p>";
        } else {
            $_SESSION['sign_msg'] = "Username should not contain space and special characters!";
            header('location: signup.php');
        }
    } elseif (username_exists($username) || $wpdb->get_var("SELECT COUNT(*) from user where username = '{$username}'")>0) {
        if ($from_travcust) {
            echo "<p>The username has been registered.</p>";
        } else {
            $_SESSION['sign_msg'] = "The username has been registered.";
            header('location: signup.php');
        }
    } elseif (empty($_POST['email'])) {
        if ($from_travcust) {
            echo "<p>The email can't be empty.</p>";
        } else {
            $_SESSION['sign_msg'] = "The email can't be empty.";
            header('location: signup.php');
        }
    } elseif (!is_email($_POST['email'])) {
        if ($from_travcust) {
            echo "<p>The email address is invaild.</p>";
        } else {
            $_SESSION['sign_msg'] = "The email address is invaild.";
            header('location: signup.php');
        }
    } elseif (empty($_POST['phone'])) {
        if ($from_travcust) {
            echo "<p>The phone number can't be empty.</p>";
        } else {
            $_SESSION['sign_msg'] = "The phone number can't be empty.";
            header('location: signup.php');
        }
    } elseif (!preg_match('!^.*(?=.{8,})(?=.*\d)(?=.*[A-Z]{1,})(?=.*[a-z]{1,}).*$!', $pw1)) {
        if ($from_travcust) {
            echo "<p>Invaild password. the password must have 8 characters or longer and include upper and lowercase letters and numbers.</p>";
        } else {
            $_SESSION['sign_msg'] = "Invaild password. the password must have 8 characters or longer and include upper and lowercase letters and numbers.";
            header('location: signup.php');
        }
    } elseif ($pw1 != $pw2) {
        if ($from_travcust) {
            echo "<p>Password Doesn't Match</p>";
        } else {
            $_SESSION['sign_msg'] = "Password Doesn't Match";
            header('location: signup.php');
        }
    } else {
        $fusername = $username;
        $email=empty($_POST['email'])?'':check_input($_POST['email']);
        $phone=empty($_POST['phone'])?'': check_input('+'.$_POST['tel-ext'] . ltrim(check_input($_POST['phone']), '0')) ;
        

		$res=$wpdb->get_row("SELECT username FROM `user` WHERE email='{$email}'", ARRAY_A);

        $numForPhone=$wpdb->get_var("SELECT count(*) FROM `user` WHERE phone='{$phone}'");

        if (!empty($res['username']) && $res['username']!='Guest') {
            if ($from_travcust) {
                echo "<p>Your e-mail id has been already registered.</p>";
            } else {
                $_SESSION['sign_msg'] = "Your e-mail id has been already registered.";
                header('location: signup.php');
            }
        } elseif ($numForPhone>0) {
            if ($from_travcust) {
                echo "<p>Your Phone has been already registered.</p>";
            } else {
                $_SESSION['sign_msg'] = "Your Phone has been already registered.";
                header('location: signup.php');
            }
        } else {
            $password = check_input($_POST["password"]);
            $fpassword = md5($password);
            $fname = check_input($_POST['firstname'].' '.$_POST['lastname']);
            $access = $_POST['signup_as'];

            if ($access == 2) {
                $access_text = 'Buyer';
            } else {
                $access_text = 'Seller';
            }
         

            // sendSMS($phone, $email, $password); // to send sms
        
            $activated=password_hash($password.time(), PASSWORD_DEFAULT);

            $fileInfo = PATHINFO($_FILES["image"]["name"]);

        
    
            if (empty($_FILES["image"]["name"])) {
                $location=$srow['photo'];
            } else {
                if ($fileInfo['extension'] == "jpg" or $fileInfo['extension'] == "png") {
                    $newFilename = $fileInfo['filename'] . "_" . time() . "." . $fileInfo['extension'];
                    move_uploaded_file($_FILES["image"]["tmp_name"], "upload/" . $newFilename);
                    $location = "upload/" . $newFilename;
                } else {
                }
            }
                        
            $inviteby='';
            if (!empty($_COOKIE['inviteby'])) {
                $inviteby=$_COOKIE['inviteby'];
                setcookie('inviteby', '', time()-3600, '/English/');
                setcookie('inviteby', '', time()-3600, '/english/');
            }
            if (!empty($res['username']) && $res['username']=='Guest') {
                $wpdb->query("UPDATE `user` SET uname='{$fname}',username='{$fusername}',password='{$fpassword}',access='{$access}',activated='{$activated}',photo='{$location}',phone='{$phone}',inviteby='{$inviteby}' WHERE email='{$email}'");
            } else {
                $wpdb->query("insert into `user` (uname, username,email,password,access,activated,photo,phone,inviteby) values ('$fname', '$fusername','$email', '$fpassword', '$access','$activated','$location','$phone','$inviteby')");
            }

            if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_potential_sale_agent` WHERE email='{$email}'")>=1) {
                $wpdb->delete("wp_potential_sale_agent", array('email'=>$email));
            }

            $mail_content = '<!DOCTYPE html><html><head><style type="text/css">
            .header_activation_div{padding: 40px;margin-left: 200px;margin-right: 200px;background-color: #27CD59;text-align: center;padding-bottom:0px;}
            .header_activation_inner_div{margin-left: 200px;margin-right: 200px;background-color: #EFEEEA;padding-bottom: 20px;}
            .header_activation_bottom_div{padding: 20px;margin-left:50px;margin-right:50px;background-color: #ffffff;}
            .header_activation_inner_inner_div{margin-left: 90px;margin-right: 90px;padding: 20px;background-color: #FFFFFF;padding-top:0px;}
            @media only screen and (max-width: 600px) {.header_activation_div{margin-left: 10px;margin-right: 10px;}.header_activation_inner_div{margin-left: 10px;margin-right: 10px;}.header_activation_bottom_div{margin-left:0px;margin-right:0px;}.header_activation_inner_inner_div{margin-left: 40px;margin-right: 40px;}}
            </style></head><body><div><div class="header_activation_div">
            <img src="https://www.travpart.com/wp-content/uploads/2020/06/travpart_logo_mail_new.jpg" style="width: 60px;height: 60px;margin-bottom:10px;">
            <div class="header_activation_bottom_div"></div></div>
            <div class="header_activation_inner_div">
            <div class="header_activation_inner_inner_div">
            <p style="text-align: center;font-size: 15px;margin-bottom: 5px;margin-top:0px;">We are glad you are here,</p>
              <h3 style="text-align: center;margin-top: 5px;">'.$firstnameforusrmail.' '.$lastnameforusrmail.'</h3>
              <div style="text-align: center;padding-top: 20px;">
              <a style="background-color: #007C89;color:white;padding:12px;border:none;text-decoration:none;" href="http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '">Activate Account</a></div>
              <p style="text-align: center;padding-top: 10px;">Or copy the link to the browser to open:</p>
              <div style="text-align: center;"><a href="http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '">http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '</a></div>
              <p style="text-align: center;">If you did not register in travpart,please ignore</p>
              <p style="margin-top: 50px;text-align: center;color: gray;margin-bottom: 5px;">© Travpart, All Rights Reserved</p>
              <p style="text-align: center;color: gray;margin-top: 5px;">Jakarta,13630 Indonesia</p>
              <div style="text-align: center;"><a href="https://www.travpart.com/English/contact/" style="text-decoration: none;"><span>Contact Us</span></a>
              <span>  -  </span><a href="https://www.travpart.com/English/termsofuse/" style="text-decoration: none;"><span>Terms of Use</span></a></div>
              </div></div>
            </div></body></html>';
        
            sendmailtouser($email, 'Travpart - Please activate your account', $mail_content);

            $num2=$wpdb->get_var("SELECT count(*) FROM `user` WHERE email='{$email}' and activated=1");
            if ($from_travcust) {
                echo 1;
            } else {
                if ($num2>0) {
                    $_SESSION['msg'] = "Sign up successful. You may login now!";
                    header('location: index.php');
                } else {
                    $_SESSION['msg'] = "You need to activate the link in your e-mail !";
                    header('location: index.php');
                }
            }
        }
    }
}


// function sendSMS($phone, $email, $pw){
// 	// Account details
// 	$apiKey = urlencode('wf0RH2mb/Nk-L0OtcMWFFsSUZkdMfyDem0AqzJGjVr');
    
// 	// Message details
// 	// $numbers = array(9893543952, 7769083532);
// 	$numbers = array($phone);
// 	$sender = urlencode('TXTLCL');
// 	$message = rawurlencode('Email'.$email.' and pw:'.$pw);
 
// 	$numbers = implode(',', $numbers);
 
// 	// Prepare data for POST request
// 	$data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
 
// 	// Send the POST request with cURL
// 	$ch = curl_init('https://api.textlocal.in/send/');
// 	curl_setopt($ch, CURLOPT_POST, true);
// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// 	$response = curl_exec($ch);
// 	curl_close($ch);
    
// 	// Process your response here
// 	echo $response;
// }
