<!DOCTYPE html>
<html>
    <head>
        <title>Travchat</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>
            #signup_form{
                width:350px;
                height:350px;
                position:relative;
                top:50px;
                margin: auto;
                padding: auto;
            }
        </style>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQ4N2X6');</script>
<!-- End Google Tag Manager -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-65718700-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-65718700-6');
</script>
<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/e34f1763-afe4-4f4e-9fc6-df81af96c04f.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>
    </head>
    <body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ4N2X6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <div class="container">
            <div id="signup_form" class="well">
                <h2><center><span class="glyphicon glyphicon-user"></span>Forgot password</center></h2>
                <hr>
                <form method="POST" action="recoverpassword.php" enctype="multipart/form-data">
                    	
                    Email: <input type="text" name="email" class="form-control" required>                    
                    <div style="height: 10px;"></div>
                    
                    
                    Phone:<input type="tel" name="phone" class="form-control">
                    <div style="height: 10px;"></div>

                    <div style="height: 10px;"></div>
                    <button type="submit" class="btn btn-primary">Submit</button> <a href="index.php"> Back to Login</a>
                </form>
                <div style="height: 15px;"></div>
                <div style="color: red; font-size: 15px;">
                    <center>
                        <?php
                        session_start();
                        if (isset($_SESSION['sign_msg'])) {
                            echo $_SESSION['sign_msg'];
                            unset($_SESSION['sign_msg']);
                        }
                        ?>
                    </center>
                </div>
            </div>
        </div>
    </body>
</html>