<?php
require_once('glogin/settings.php');
require 'twlogin/autoload.php';
require_once($_SERVER['DOCUMENT_ROOT']."/English/wp-load.php");
use Abraham\TwitterOAuth\TwitterOAuth;

/*
if(is_user_logged_in() ){
    $user = wp_get_current_user();
	echo "<pre>"; print_r($user); exit;
}
*/
session_start();

if(!empty($_SESSION['id']))
	header('location: user/');
else if(is_user_logged_in())
{
	global $wpdb;
	global $current_user;

	$row=$wpdb->get_row("select * from `user` where username='{$current_user->user_login}' and activated=1");

	if( !empty($row->userid) )
	{
		$_SESSION['id'] = $row->userid;
		$_SESSION['access'] = $row->access;
		$_SESSION['user_name'] = $row->uname;
		setcookie('access', $row->access,time() + (86400 * 30), "/");
		header('location: user/');
	}
}
?>
<!DOCTYPE html>
<html>
<head>
   	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Travpart - be part of us | Travchat</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<script src="https://www.travpart.com/Chinese/wp-content/themes/bali/js/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="icon" href="https://www.travpart.com/English/wp-content/uploads/2020/05/travpart_icon_lnA_icon.ico" type="image/gif" sizes="16x16">
<style>
    #login_form{
        width:350px;
        margin: 60px auto 0 auto;       
    }

    .btn-facebook {
        background-color: #052940;
    }

    .btn-twitter {
        background-color: #1DA1F2;
    }

    .btn-google {
        background-color: #DB4437;
    }
</style>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQ4N2X6');</script>
<!-- End Google Tag Manager -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-65718700-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-65718700-6');
</script>
<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/e34f1763-afe4-4f4e-9fc6-df81af96c04f.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ4N2X6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '243014099743892',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
  }
</script>

<style>
.desktop_menu{
	color: black !important;
    text-decoration: none !important; 
	font-size: 16px !important;
	margin-left: 13px !important;
}
select.form-control:not([size]):not([multiple]) {
    height: 26px;
    padding: 0px !important;
}
.dropdown-toggle::after{
	content:"";
	display: none;
}
.mega-menu-link span {
    display: inline-block;
    padding: 0px 2px;
    line-height: 20px;
    color: white;
    border-radius: 3px;
}
.topnav {
  overflow: hidden;
  background-color: #368A8C;
}

.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
  text-align:right!important;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.active {
  background-color: #5bbc2e!important;
  color: white;
}

.topnav .icon {
  display: none;
}
.navbar-default {
    background-color: white !important;
    border-color: white !important;
    border-bottom: solid 7px #1A6D84 !important;
}

@media screen and (max-width: 600px) {
  .topnav a:not(:first-child) {display: none;}
  .topnav a.icon {
    float: right;
    display: block;
  }
}

@media screen and (max-width: 600px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  #login_form{
  	max-width:100%;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
  .mobilenone{display:none;}
  //.mobileonly{display:block!important;}
}
</style>

<style>
.input-group-prepend button{
	width: 100%;
}
.input-group-prepend{
	width: 10%;
	background: #fff;
	display: inline-block;
}.lighten-3 i {
    color: #1bba9a !important;
}
.navbar-nav{
	width:27%;
	padding-top: 22px;
}
.search_hea, .lighten-3 {
    border: 1px solid #1bba9a;
}.lighten-3 {
    border-right: 0px;
    background: none;
}
.py-1 {
    border: 0px !important;
    font-size: 11px !important;
    letter-spacing: 1px !important;
    padding:9px 0px !important;
    font-family: 'Heebo', sans-serif !important;
}
.search_hea{
	width: 80%;
	display: inline-block;
	margin: 0px;
}

.input-group {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-align: stretch;
    -ms-flex-align: stretch;
    align-items: stretch;
    width: 100%;
}

.search_hea {
    border-left: 0px !important;
}
.input-group .form-control, .input-group-addon, .input-group-btn {
    display: table-cell;
}
.input-group .form-control {
    position: relative;
    z-index: 2;
    float: left;
    width: 100%;
    margin-bottom: 0;
}


span.notify {
    position: relative;
    padding: 2px 5px !important;
    color: #fff;
    font-size: 10px;
    border-radius: 50%;
    top: -12px;
    left: -8px;
}
.mobile_header_balouch_show{
display: none;
}
.for_logo_mobile{
display: inline-block;
width: 60%;
}
.custom_image_mobile{
width: 85%;
}
.for_menu_mobile{
width: 35%; 
padding: 15px;
display: inline-block;
}

.English-logo img{
	width: auto;height: 50px;
}
@media only screen and (max-width: 1024px){
		.mobilenone{
			   	display: none;
			   }	 
	.mobile_header_balouch_show{
		display: block !important;
	}
	.mobile_header_balouch_hide{
		display: none !important;
	}
}
.min_height{
	height: 105px;
	overflow: scroll;
}
.mega-menu-link a { 
    color: #000;
}
span.badge{
	padding:.25em .4em !important;
	background:red !important;
}

#content{
	margin-top: 0px !important;
}
.row_c{
	border-bottom:1px solid #ccc;
}
.notification_div{
	background: #fff;
}
.for_border{
	border-left: 4px solid #22b14c;
    border-right: 4px solid #22b14c;
    border-bottom: 4px solid #22b14c;
}
.profile_s{
	background:#22b14c;
	padding-bottom: 0px !important;
}

.dropdown-item {
    display: block;
    width: 100%;
    padding: .25rem 1.5rem;
    clear: both;
    font-weight: 400;
    color: #212529;
    text-align: inherit;
    white-space: nowrap;
    background-color: transparent;
    border: 0;
}
.input-group-prepend{
	background: #fff;
}
.home_mytime_msg{
	background: #368a8c;
}
.col-mmd-4 img{ 
	border-radius: 100%;
	border: 1px solid #fff;
}
p{
	font-family: 'Heebo', sans-serif;
}
.col-mmd-4 a,.col-mmd-3 a,.col-mmd-6 a{
	color: #fff;
	font-family: 'Heebo', sans-serif;
	font-size: 12px;
	text-transform: uppercase;
}
.space_top{
	margin-top: 10px;
}
.col-mmd-4{
	text-align: center;
	width: 32%;
	display: inline-block;
}
.avatar-25{
	border-radius: 50%;
	border:1px solid #ccc;
	padding: 1px;
}
.col-mmd-3{
	text-align: center;
	width: 18%;
	display: inline-block;
}
.col-mmd-6{
	text-align: center;
	width: 60%;
	display: inline-block;
}
.remove_pad{
	padding: 0px !important;
}
.custom_menu_btn_mobile i{
	color: #1a6d84;position: absolute;font-size: 22px;margin-left: 5px;
}
	.custom_menu_btn_mobile{
		padding: 0px 15px !important;
		background: none !important;
	    border:0px !important;
	    outline: 0px !important;
	    position: relative;
	    box-shadow: none !important;
	    font-size: 16px !important;
	    color: #1a6d84;	
	}				
		@media only screen and (max-width: 1024px){
			.custom_remove_pd{
				padding: 0px !important;
				max-width: 100% !important;
			} 
			.search_hea{
				margin: 0px;
			}
			.col-mmd-4 a:visited {
			    color: #fff !important;
			}
			.mobile_header_balouch_show{
				display: block !important;
			}
			.mobile_header_balouch_hide{
				display: none !important;
			}
			.mobilenone{
			   	display: none;
			   }
		}
		
		
		
		
		@media only screen and (max-width: 900px) and (min-width: 768px){
			.custom_menu_btn_mobile{
				font-size: 22px !important;
			}
			.custom_menu_btn_mobile i{
				font-size: 30px;
			}
			.for_menu_mobile{
				padding: 44px !important;
			}
			.mobilenone{
			   	display: none;
			   }
		}
		
		@media only screen and (max-width: 1024px) and (min-width: 900px){
			.custom_menu_btn_mobile{
				font-size: 40px !important;
			}
			.custom_menu_btn_mobile i{
				font-size: 53px;
			}
			.for_menu_mobile{
				padding: 53px !important;
			}.mobilenone{
			   	display: none;
			   } 
		}
</style>


<script type="text/javascript">
jQuery(document).ready(function($){
	$('.hide_not,.hide_pro').hide();
	$('.lighten-3').click(function(){
		$('.custom_d').toggle();
	})
	$('.custom_menu_btn_mobile').click(function(){
		$(this).find('i').toggleClass('fa-bars fa-times-circle');
		$('.home_mytime_msg').slideToggle();
	})
	$('.not_slide').click(function(){
		$(this).find('i').toggleClass('fa-angle-down fa-angle-up');
		$('.hide_not').slideToggle();
	});
	$('.pro_slide').click(function(){
		$(this).find('i').toggleClass('fa-angle-down fa-angle-up');
		$('.hide_pro').slideToggle();
	});
	
	$('.click_main_search').click(function(){
		var color_c = $(this);
		$('.search_hea').attr('action',color_c.data('url'));
		$('.py-1').attr('name',color_c.data('name'));
		$('.click_main_search').removeClass('for_b_left');
		$('.py-1').attr('placeholder',color_c.data('text'));
		color_c.addClass('for_b_left');
	});
	
});
</script>

<script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>
<div class="mobile_header_balouch_show">
  <div class="row" style="padding: 10px 0px 10px 10px;">
  	<div class="for_logo_mobile text-center">
	  <a href="https://www.travpart.com/English/">
	<img class="custom_image_mobile" src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png" alt="travpart">
  </a>
	</div> 
	<div class="for_menu_mobile text-center">
		<button class="btn custom_menu_btn_mobile">
			MENU
			<i class="fas fa-bars"></i>
		</button>
	</div>	
  </div>
  
  
  <div class="home_mytime_msg" style="display: none;">
	
  	<div class="rows" style="padding-top: 20px;">
  		<div class="col-mmd-4">
  		  <a href="https://www.travpart.com/English/timeline/">
	  		<i class="fas fa-home" style="font-size: 30px;margin-left:9px"></i>
	  		<p>Home</p>
	  	  </a>	
	  	</div>
	  	<div class="col-mmd-4">
	  	<?php $current_user = wp_get_current_user(); ?>
	  	  <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->ID;?>">	
	  		<i class="fas fa-clock" style="font-size: 30px;"></i>
	  		
	  		<p>My Timeline</p>
	  	  </a>	
	  	</div>
	  	<div class="col-mmd-4">
	  	  <a href="/English/travchat/">
	  	 	<i class="fas fa-envelope" style="font-size: 30px;margin-right:9px"></i>
	  		<p>Message</p>
	  	  </a>	
	  	</div>
  	</div>
  	
  	<div class="for_border">
	 	
	  	<?php
        $display_name = um_user('display_name');
        $current_user = wp_get_current_user();
        if (is_user_logged_in()) { ?>
	  	<div class="rows profile_s" style="padding: 5px;">
	  		<div class="col-mmd-4">
	  		  <a href="#" class="custpm_pr">
		  		<?php echo um_get_avatar('', wp_get_current_user()->ID, 30); ?> 
		  	  </a>	
		  	</div>
		  	<div class="col-mmd-4">
		  	  <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>?profiletab=pages" style="font-size: 9px;line-height: 30px">
		  	  	<strong> 
                     <?php echo $display_name; ?>
		  	  	</strong>
		  	  </a>	
		  	</div>
		  	<div class="col-mmd-4">
		  	  <a class="pro_slide" style="color:#fff;font-weight: bold;font-size: 18px;margin-top: -10px;font-family: 'Heebo', sans-serif;padding: 5px 15px !important">
		  	  	V
		  	  </a>
		  	</div> 
	  	</div>
	  	<?php } ?>
	  	
	  	<div class="hide_pro">
	  	<div class="rows "  style="padding-top: 10px;">
	  		<div class="col-mmd-4">
	  		  <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>?profiletab=pages">
		  		<i class="fas fa-user-circle" style="font-size: 30px;"></i>
		  		<p>My Profile</p>
		  	  </a>	
		  	</div>
		  	<div class="col-mmd-4">
		  	  <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=referal">	
		  		<i class="fas fa-undo-alt" style="font-size: 30px;"></i>
		  		<p>My Refferal</p>
		  	  </a>	
		  	</div>
		  	<div class="col-mmd-4">
		  	  <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=settlement">
		  	 	<i class="fas fa-calendar-check" style="font-size: 30px;"></i>
		  		<p>Settlement</p>
		  	  </a>	
		  	</div>
	  	</div>
  	
  	
  	<div class="rows ">
  		<div class="col-mmd-4">
  		  <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=transaction">
	  		<i class="fas fa-dollar"  style="font-size: 30px;"></i>
	  		<p>Transaction</p>
	  	  </a>	
	  	</div>
	  	<div class="col-mmd-4">
	  	  <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=verification">	
	  		<i class="fas fa-calendar-check" style="font-size: 30px;"></i>
	  		<p>Verification</p>
	  	  </a>	
	  	</div>
	  	<div class="col-mmd-4">
	  	  <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=pages2">
	  	 	<i class="fas fa-book-open" style="font-size: 30px;"></i>
	  		<p>Booking</p>
	  	  </a>	
	  	</div>
  	</div>
   </div>
  </div> 
  <div class="rows " style="padding: 10px 0px 0px 0px">
  		<div class="col-mmd-4">
  		  <a href="/English/about-us/">
	  		<i class="fas fa-briefcase" style="font-size: 30px;"></i>
	  		<p>About</p>
	  	  </a>	
	  	</div>
	  	<div class="col-mmd-4">
	  	  <a href="https://www.travpart.com/Chinese">
	  		<i class="fas fa-language" style="font-size: 30px;"></i>
	  		<p>中文</p>
	  	  </a>	
	  	</div>
	  	<div class="col-mmd-4" style="position: absolute;">
	  	  <select class="header_curreny form-control search_select" style="margin: 5px 0px;border-radius: 0px;min-height: 35px;">
             <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
             <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
             <option full_name="USD" syml="USD" value="USD"> USD </option>
             <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
          </select>
	  	</div>
  	</div>
  <?php
        //$display_name = um_user('display_name');
        //$current_user = wp_get_current_user();
        //if (is_user_logged_in()) { ?>
  
   <?php //} ?>
   
  <?php if (is_user_logged_in()) { ?>
  	<a href="<?php echo wp_logout_url(get_permalink()); ?>" class="btn btn-primary" style="font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;"><i class="fas fa-sign-out-alt"></i> Logout</a>
  <?php }else{ ?>
  	<a href="/English/login/" class="btn btn-primary" style="font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;"><i class="fas fa-sign-out-alt"></i> Login</a>
	<?php } ?>
  </div>
  
</div>



<div class="topnav mobileonly" id="myTopnav">
 <a class="navbar-brand logo-contentimage" href="./index.php">
 <img src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png" />
 </a> 
  <a href="#" style="padding-bottom:20px"></a>
  <a href="https://www.travpart.com/English/about-us/">About Us</a>
  <a href="https://www.travpart.com/English/travchat">Message</a>
  <?php
    ob_start();
    wp_nav_menu(array('theme_location' => 'main-menu'));
    $main_menu = ob_get_contents();
    ob_end_clean();
    ?>
    <?php ob_start(); ?>

                <?php if (is_user_logged_in()) { ?>
                 
                    <a href="./logout.php" class="buttype">LOGOUT<i class="fa fa-caret-down" style="display: inline;"></i></a>
                <?php } else { ?>

                    <a href="/English/login/" class="buttype">LOGIN</a>
                    
                <?php } ?>
  <a href="http://travpart.com/English/travchat/user/myorders.php">My Order</a>
  <a href="https://www.travpart.com/English/travcust/">Create a tour package</a>
  <?php if(current_user_can('um_travel-advisor')) { ?>
  <a href="https://www.travpart.com/English/customers-request/">CUSTOMER REQUEST</a>
<?php } ?>
  <a href="https://www.travpart.com/English/tutorial/"><img src="https://www.travpart.com/English/wp-content/themes/bali/tutorial-downloadpdf.png"></a>
  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
</div>
    

<nav class="navbar navbar-default mobilenone">
    <div class="container-fluid">
        <div class="navbar-header col-md-2"> 
	        <a class="navbar-brand logo-contentimage" href="https://www.travpart.com/English">
	        	<img src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png" />
	        </a>
        </div>


        <ul class="nav navbar-nav col-md-4">
			<div class="input-group md-form form-sm form-1 pl-0">
	         <div class="input-group-prepend">
	            <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="basic-text1" style="height: 36px;">
	            	<i class="fas fa-search text-white" aria-hidden="true"></i>
	            	<!--<span class="dropdown_title">People</span>-->
	            </button>
	        	<div class="dropdown-menu" style="width: 100%;position: absolute;">
			      <a style="font-size:12px !important;margin-left:0px; " class="dropdown-item click_main_search for_b_left" data-name="friend" data-url="<?php bloginfo('home'); ?>/people" data-text="Search for People" href="#">People</a>
			      <a style="font-size:12px !important;margin-left:0px; " class="dropdown-item click_main_search" data-name="search" data-url="<?php bloginfo('home'); ?>/toursearch" data-text="Search for Tour Packages" href="#">Tour Packages</a>
			    </div>
	          </div>
	          <form class="search_hea"  action="<?php bloginfo('home'); ?>/people" method="get">
	              <input class="form-control my-0 py-1" type="text" placeholder="Search for People" aria-label="Search" name="friend">
	          </form>
	        </div>
            <!-- <li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Lobby</a></li>-->
        </ul>
      
       
<div class="main-menu pull-right mobilenone col-md-6">
     
	 <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
     	<a class="desktop_menu" href="https://www.travpart.com/English/login">LOGIN</a>
     </li> 
     <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
        <a class="desktop_menu" href="https://www.travpart.com/English/register2/">REGISTER</a>
    </li>
    <li class="mega-menu-link">
        <a href="https://www.travpart.com/English/contact/"><span style="margin-left: 10px;padding:5px;background: #37a000 !important;">Contact us</span></a>
    </li>
    <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
        <a class="desktop_menu" href="https://www.travpart.com/Chinese">
        	<img src="/English/wp-content/themes/bali//images/flg-cn.png" alt="中文" class="mobilenotshow"> 中文
        </a>
    </li>
     <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"> 	  	
	  	<select class="header_curreny form-control search_select" style="background: none;margin:0px 0px 0px 8px;">
            <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
            <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
            <option full_name="USD" syml="USD" value="USD"> USD </option>
            <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
        </select>
	  	
     </li>
     
	 
	

                  
</div>
</div>
        <!-- <ul class="nav navbar-nav navbar-right">
         

            <li><a class="ttl-1-myordr" href="./myorders.php">My Orders</a></li>
            <li class="ttl-1-li"><a class="ttl-1" href="http://www.travpart.com/English/travcust">Customize Travel</a>
                <a class="ttl-1-logo below-logo-fl" href="http://www.travpart.com/English/travcust"><img id="logo-imgview"
                        src="http://www.travpart.com/wp-content/uploads/2018/08/C_Users_ilham_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_5d530297-1236-49a1-8d98-13dadb59abc0.jpg" /></a>
            </li>
            <?php if(!empty(@$srow['photo'])){
            ?>
            <li><img src="../<?php echo $srow['photo']; ?>" width="42px;" style="margin: 3px;"></li>
            <?php }?>

            <li><a href="#account" data-toggle="modal"><span class="glyphicon glyphicon-lock"></span>
                    <?php echo @$user; ?></a></li>
            <?php if($_COOKIE['access'] == 0):   ?>
            <li><a href="guestLogout.php"><span class="glyphicon glyphicon-log-out"></span> Out</a></li>
            <?php  endif;  ?>
            <?php  if($_COOKIE['access'] != 0):   ?>
            <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#photo" data-toggle="modal"><span class="glyphicon glyphicon-picture"></span> Update Photo</a></li>
                    <li class="divider"></li>
                    <li><a href="#logout" data-toggle="modal"><span class="glyphicon glyphicon-log-out"></span> LOGOUT</a></li>
                </ul>
            </li>
            <?php endif;   ?>
        </ul> -->
    </div>
</nav>
<script type="text/javascript">
    $(".ttl-1-logo").hover(function () {
        $("#logo-imgview").slideToggle(800);
    });

      //$("li.ttl-2-li").hover(function(){
          //  $(".ttl-2-logo").slideToggle(800);
      //  });
</script>
<style>

.click_main_search {
    outline: 0px;
}

.for_b_left {
    border-left: 3px solid #1bba9a !important;
}
.navbar-brand{
	height: auto !important;
}
    button.searchButton2 {
        height: 35px !important;
        margin: 98px 0 0px 20px !important;
    }

    .below-logo {
        display: flex;
        justify-content: center;
    }

    ul.list-unstyle.header-logo {
        list-style: none;
        padding: 0;
        margin: 0;
    }

    ul.list-unstyle.header-logo li {
        background-color: #f2f2f2;
        display: block;
        padding: 20px;
        width: 47%;
        text-align: center;
        float: left;
        margin: 10px;
    }

    .below-logo a {
        display: block;
        font-weight: 600;
    }

    .below-logo .ttl-1-logo,
    .below-logo .ttl-2-logo {
        display: none;
    }

    .below-logo img {
        width: 100%;

    }

    .ttl-1-logo img {
        position: absolute;
        width: 200px;
        left: 0px;
        top: 40px;
        z-index: 99999;
        display: none;
    }

    ul.list-unstyle.header-logo li {
        position: relative;
    }

    .divider-row {
        border-bottom: 1px solid #000;
    }

    #login_form {
        width: 350px;
        margin: 60px auto 0 auto;
    }

    .btn-facebook {
        background-color: #052940;
    }

    .btn-twitter {
        background-color: #1DA1F2;
    }

    .btn-google {
        background-color: #DB4437;
    }

    ul.list-unstyle.header-logo {

        float: right;
        display: inline-block;
    }

    ul.list-unstyle.header-logo li {
        width: auto;
    }

    a.ttl-1 {
        width: auto;
        display: inline-block;
        float: right;
    }

    a.ttl-1-logo.below-logo-fl {
        border: 1px solid white;
        padding: 20px;
        top: 2px;
        position: relative;
    }

    .logo-contentimage {
        display: inline-block;
        text-align: center;
        padding: 0px;
    }
a{
	text-decoration: none;
}
    .logo-contentimage img {
        max-width: 100%;
        height: auto;
		margin: auto;
		margin-top: 10px;
    }

    .dropbtn {
    background-color: transparent;
    color: white;
    border: none;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute!important;
    background-color: #3b898a!important;
    min-width: 330px !important;
    padding:5px 1px!important;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2)!important;
    z-index: 1!important;
}

.dropdown-content a {
    color: black;
    padding: 5px!important;
    text-decoration: none;
    display: block;
}

.dropdown:hover .dropdown-content {display: block;}

.mobileonly,.mobilephoneonly{display:none}
.profileicon {margin-right:-10px!important}
.profileicon img{width:25px;height:25px;border-radius:50%;border:1px solid #ccc}
.mega-menu-item{display:inline-block;list-style: none}
.mega-menu-link{display:inline-block;z-index:9999!important;margin-right:10px;}
#mega-menu-wrap-main-menu #mega-menu-main-menu a.mega-menu-link{color:black!important;line-height:2.5em!important}

.navbar-right{margin-right:0!important;}
ul#mega-menu-main-menu {
    margin-bottom: 2px;
}

#mega-toggle-block-1{display:none;}
.main-menu{
	padding-top: 25px !important;
	text-align: right;
}
.navbar-brand img {
    padding-top: 5px !important;
    padding-bottom: 0px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
}
.pull-right{padding-top:10px;}

@media only screen and (max-width: 600px) {
.panel-default{margin-top:30px;}
.modal-body{padding:0!important;}
.pull-right{display:none;}

}
</style>


<div class="container">
    <div id="login_form" class="card">
        <div class="card-header">
            <h2><center><i class="fas fa-lock"></i> Please Login</center></h2>
        </div>
        <div class="card-body">
			<p class="mb-0">
                <a  class="btn btn-primary btn-block" href="guestUserLogin.php?id=<?php echo rand (0100,9999); ?>">Guest Login</a>   
            </p>
			<hr>
            <div style="color: red; font-size: 15px;">
                <center>
                <?php
                    //session_start();
                                       
                    if(isset($_SESSION['msg'])){
                        echo $_SESSION['msg'];
                        unset($_SESSION['msg']);
                    }
                ?>
                </center>
            </div>
            <form method="POST" action="login.php">
                <div class="form-group">
                    <label for="username">Username: </label>
                    <input type="text" name="username" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="password">Password: </label>
                    <input type="password" name="password" class="form-control" required>
                </div>
                <button type="submit" class="btn btn-primary btn-block"><span class="fas fa-sign-in-alt"></span> Login</button> 
                <?php /*<p class="text-center my-3 font-weight-bold">OR</p>
                <div class="row">
                    <div class="col">
                    <div class="social facebook"><i class="fab fa-facebook-f"></i></div>
                         <fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>

<div id="status">
</div>    
                        <!-- <button class="btn btn-facebook btn-block text-white" onlogin="checkLoginState();"> <i class="fab fa-facebook-f"></i></button> -->
                    </div>
                    <div class="col">
                    <a href="<?= 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me') . '&redirect_uri=' . urlencode(CLIENT_REDIRECT_URL) . '&response_type=code&client_id=' . CLIENT_ID . '&access_type=online' ?>"><div class="social google-pluse"><i class="fab fa-google"></i></div></a>

                        <!-- <button class="btn btn-twitter btn-block text-white"> <i class="fab fa-google"></i> </button> -->
                    </div>
                    <div class="col">
                    <?php
                    if (!isset($_SESSION['access_token'])) {

                    $connection = new TwitterOAuth('yIPKVo4BYbA9AsELDtNkAHTon', 'RuX0ge812pXNDrEwprOIC56ydHbxiGyjr5bGc10Y6S4wENy9bl');
    $request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => 'http://www.travpart.com/English/travchat/twlogin/callback.php'));
    $_SESSION['oauth_token'] = $request_token['oauth_token'];
    $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
    $url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
    
    echo "<a href='$url'><div class='social twitter'><i class='fab fa-twitter'></i></div></a>";

}
                    
                    ?>
                     
                        <!-- <button class="btn btn-google btn-block text-white"> <i class="fab fa-twitter"></i> </button> -->
                    </div>
                </div>
				*/ ?>
            </form>

            <!-- <button class="btn btn-primary"  name="guest" onclick="guestUser()"><i class="fas fa-sign-in-alt"></i>Guest User</button><a href="forgot.php"> Forgot Password ?</a> </button> -->
        </div>

        <div class="card-footer">
            <p>
                No account? <a href="signup.php"> Sign up</a>   
            </p>

            <p class="mb-0">
                <a href="/English/password-reset/">Forgot Password?</a>   
            </p>
        </div>
    </div>
</div>

<div class="container" style="margin-top: 60px;">
<div class="row">
  <div class="col-sm-4" style="text-align: center;">
    <a href="http://www.travpart.com/"><img style="height: 50px;" src="https://www.travpart.com/wp-content/themes/aberration-lite/images/2019logo.png" /></a>
  </div>
  <div class="col-sm-4" style="text-align: center;">
    <a href="#"><img style="height: 50px;" src="http://www.travpart.com/wp-content/uploads/2018/08/C_Users_ilham_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_1b312699-b11a-40bb-805e-0ba4a25a30c4.jpg" /></a>
  </div>
  <div class="col-sm-4" style="text-align: center;">
    <a href="https://tourfrombali.com"><img style="height: 50px;" src="http://www.travpart.com/English/wp-content/uploads/2018/08/tourfrombaliimg.png" />
        <p style="margin: auto 0; display: inline;">  Powered By TourFromBali  </p>
    </a>
  </div>
</div>
   <script>
       function guestUser(){
           
           var text = "";
           var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

         for (var i = 0; i < 5; i++)
              text += possible.charAt(Math.floor(Math.random() * possible.length));
          
          window.location.href = 'guestUserLogin.php?id='+text;
        }
   </script>    
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <!-- Facebook login script -->
    
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '245698579466676',
                cookie     : true,
                xfbml      : true,
                version    : 'v3.1'
            });
            
            FB.AppEvents.logPageView();   

            FB.getLoginStatus(function(response) {
                console.log( response );
                statusChangeCallback(response);
            }); 
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <style>
    .social{text-align:center;width:65px;float:left;background:rgb(255,255,255);border:1px solid rgb(204,204,204);box-shadow:0 2px 4px rgba(0,0,0,0.15), inset 0 0 50px rgba(0,0,0,0.1);border-radius:5px;margin:0 10px 10px 0;padding:17px;}
.google-pluse:hover{background:#DD4B39;color:#FFF;}
.twitter:hover{background:#00acee;color:#FFF;}
.facebook:hover{background:#3b5998;color:#FFF;}
    </style>
</body>
</html>