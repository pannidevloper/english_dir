<?php
include('session.php');
include('header.php');
include('../conn.php');
include('../../wp-config.php');
global $wpdb;
?>
<body>
	<?php include('navbar.php'); ?>
	<div class="container-fluid">
    <div class="row">
		<div class="col-lg-12">
		<h2 style="text-align:center;">My Orders</h2>

		<?php
		if($_SESSION['access']==1)
		{
			$agent_id=$_SESSION['id'];
			$orders=mysqli_query($conn, "SELECT * FROM `orders` WHERE `agent_id` = {$agent_id}");
			echo '<div class="order_list">
					<table width="100%" class="table  table-bordered table-hover" >
						<thead>
							<tr> <th>Booking Code</th> <th>Sales Amount</th> <th>Commission</th> <th></th> <th>Reservation Code</th> </tr>
						</thead>
						<tbody>';
			while($row = mysqli_fetch_array($orders, MYSQL_ASSOC))
			{
				$tour=$wpdb->get_row("SELECT confirm_payment,rev_num FROM `wp_tour` WHERE `id` = ".(int)substr($row['bookingcode'], 4));
				if(!empty($tour->rev_num))
					$rev_num=$tour->rev_num;
				else
					$rev_num='NULL';
				$confirm_payment=$tour->confirm_payment;
				echo "<tr> <td>{$row['bookingcode']}</td> <td>$".$row['total_price']."</td> <td>$".($row['total_price']*0.08)."</td>";
				if($confirm_payment==1)
					echo '<td>Paid</td>';
				else
					echo '<td><input class="confirm_payment" type="button" value="Not Paid" tourid="'.(int)substr($row['bookingcode'], 4).'" /></td>';
				echo '<td>'.$rev_num.'</td>
				</tr>';
			}
			echo '</tbody>
					</table>
				</div>';
			
		}
		else if($_SESSION['access']==2)
		{
			$customer_id=$_SESSION['id'];
			$orders=mysqli_query($conn, "SELECT orders.*, user.uname FROM orders,user WHERE orders.customer_id={$customer_id} AND user.userid=orders.agent_id");
			echo '<div class="order_list">
					<table width="100%" class="table  table-bordered table-hover" >
						<thead>
							<tr> <th>Booking Code</th> <th>Nominal Amount</th> <th>Agent\'s Name</th> </tr>
						</thead>
						<tbody>';
			while($row = mysqli_fetch_array($orders, MYSQL_ASSOC))
			{
				//var_dump($row);
				echo "<tr> <td>{$row['bookingcode']}</td> <td>$".$row['total_price']."</td> <td>{$row['uname']}</td> </tr>";
			}
			echo '</tbody>
					</table>
				</div>';
				//if (empty($row)){
					//echo "<h4>No order found</h4>";
				//}
		}
		else
		{
			echo '<h4 class="no_record">No record available</h4>';
		}
		?>
        

	</div>
	</div>
	</div>
	<?php include('modal.php'); ?>
	<style type="text/css">
		h4.no_record {
    		text-align: center;
    		color: red;
    		padding-top: 72px;
        }
	</style>
</body>
</html>