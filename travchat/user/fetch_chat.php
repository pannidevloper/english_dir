<?php
require_once($_SERVER['DOCUMENT_ROOT']."/English/wp-load.php");
global $wpdb;
session_start();
include('../conn.php');
include('./set_active_status.php');
if (isset($_POST['fetch'])) {
//    $timeZoneName = $_COOKIE['timeZoneName'];
//    date_default_timezone_set($timeZoneName);
    $to = $_COOKIE['to'];
    $from = $_SESSION['id'];
    $previous_date = '00-00-00';
    $sql = "SELECT  c.*,u.uname,u.photo,u.username FROM `chat` c INNER JOIN `user` u ON u.userid = c.c_from WHERE (c.c_to=$to AND c.c_from=$from) OR (c.c_to=$from AND c.c_from=$to)";
	$query = mysqli_query($conn, $sql) or die(mysqli_error());
    mysqli_query($conn, "UPDATE chat SET status=1 WHERE c_to={$from} AND c_from={$to}") or die(mysqli_error());

	$showAlertMessage=false;
	$nowDay=date('Y-m-d',strtotime($wpdb->get_var("SELECT NOW()")));
     while ($row = mysqli_fetch_array($query)) {
		if(!$showAlertMessage && date('Y-m-d',strtotime($row['chat_date']))==$nowDay) {
			$showAlertMessage=true;
		}

        $new_date = $row['chat_date'];
        $previous = strtotime(date('y-m-d', strtotime($previous_date)));
        $new = strtotime(date('y-m-d', strtotime($new_date)));

	$sendday= date('M-d-y', strtotime($new_date));
	$nowDay1=date('M-d-y',strtotime($wpdb->get_var("SELECT NOW()")));
		
        if ($previous != $new) {
                if ($nowDay1==$sendday) {
                echo "<div class='datte'>" . "Today" . "</div>";
                $previous_date = $new_date;
            }else{
                echo "<div class='datte'>" . date('M-d-y', strtotime($new_date)) . "</div>";
                $previous_date = $new_date;
            }
            
        }
        ?>  
        <?php if ($row['type'] == 'attachment'): ?>
            <div>
                <img  src="../<?php
                if (empty($row['photo'])) {
                    echo "upload/profile.jpg";
                } else {
                    echo $row['photo'];
                }
                $attachment_Image = '../image/file.png';
                $attachment_type = explode('/', $row['attachment_type']);
                $type = '';
                if ($attachment_type[0] == 'image') {
                    $type = 'image';
                    $attachment_Image = '../upload/' . $row['message'];
                }
                ?>" style="height:40px; width:40px; position:relative; top:15px; left:10px;border-radius:50% 50%">
                <span style="font-size:10px; position:relative; float:right; top:7px; right:15px;"><i><?php echo date('h:i A', strtotime($row['chat_date'])); ?></i></span><br>
                <span style="font-size:11px; position:relative; top:-20px; left:60px;" ><strong><?php echo $row['uname']; ?></strong></span><br>
                <?php if ($type == 'image'): ?>
                    <div style="cursor:pointer;margin-left:8%;" onclick="downloadAttachment('<?php echo $row['message']; ?>', '<?php echo $row['attachment_type']; ?>');"><img title="click to download" alt="attachment" style="width:128px;height:96px;" src="<?php echo $attachment_Image; ?>"></div>
                <?php else: ?>
                    <div style="cursor:pointer;margin-left:8%;" onclick="downloadAttachment('<?php echo $row['message']; ?>', '<?php echo $row['attachment_type']; ?>');"><img title="click to download" alt="attachment" style="width:40px;height:40px;" src="<?php echo $attachment_Image; ?>"></div>
                <?php endif; ?>
            </div>
        <?php elseif($row['type'] == 'link'): ?>
            <div>

                <img  src="../<?php
                if (empty($row['photo'])) {
                    echo "upload/profile.jpg";
                } else {
                    echo $row['photo'];
                }
                ?>" style="height:40px; width:40px; position:relative; top:15px; left:10px;border-radius:50% 50%">
                <span style="font-size:10px; position:relative; float:right; top:7px; right:15px;" class="rightt"><i><?php echo date('h:i A', strtotime($row['chat_date'])); ?></i></span><br>
                <span style="font-size:11px; position:relative; top:-20px; left:60px;"><strong><?php echo $row['uname']; ?></strong>:<br> <?php echo "<a target='_blank' href='".$row['message']."'>".$row['message']."</a>"; ?></span><br>
            </div>
		  <?php  elseif($row['type'] == 'deleted'):  ?>
           <div>

               <img src="../<?php
                if (empty($row['photo'])) {
                    echo "upload/profile.jpg";
                } else {
                    echo $row['photo'];
                }
                ?>"
                   style="height:40px; width:40px; position:relative; top:15px; left:10px;border-radius:50% 50%" />
                <span style="font-size:10px; position:relative; float:right; top:7px; right:15px;" class="rightt"><i><?php echo date('h:i A', strtotime($row['chat_date'])); ?></i></span><br>
				<span class="msgg" style="font-size:11px; position:relative; top:-20px; left:60px;"><strong><?php echo $row['uname']; ?></strong>:<br> <span class="deleted-message">Deleted</span> </span><br>
            </div>
		  <?php elseif($row['type'] == 'verify_tour_request'):
			$verify_status=$wpdb->get_var("SELECT verify_status FROM verify_tour_request WHERE tour_id='{$row['message']}'");
		  ?>
            <div>
                <img  src="../<?php
                if (empty($row['photo'])) {
                    echo "upload/profile.jpg";
                } else {
                    echo $row['photo'];
                }
                ?>" style="height:40px; width:40px; position:relative; top:15px; left:10px;border-radius:50% 50%">
                <span style="font-size:10px; position:relative; float:right; top:7px; right:15px;" class="rightt"><i><?php echo date('h:i A', strtotime($row['chat_date'])); ?></i></span><br>
                <span style="font-size:11px; position:relative; top:-20px; left:60px;">
					<strong><?php echo $row['uname']; ?></strong>:<br>
					<?php if($row['c_from']==$from): ?>
						<?php if($verify_status==0): ?>
							<span class="message-content">I'd like you to verify this tour package is my request:BALI<?php echo $row['message']; ?></span>
						<?php elseif($verify_status==1): ?>
							<span class="message-content">Congratulation! The travel advisor had just verified it's your request. You just earned a point to play for the voucher game.</span>
						<?php else : ?>
							<span class="message-content">Sorry. This tour package request has already been verified earlier.</span>
						<?php endif; ?>
					<?php elseif($verify_status==0): ?>
						<span class="message-content">I'd like you to verify this tour package is my request:BALI<?php echo $row['message']; ?></span><br>
						<button tourid="<?php echo $row['message']; ?>" class="btn btn-my confirm_request">Yes, it's your request</button>
						<button tourid="<?php echo $row['message']; ?>" class="btn btn-my reject_request">No, it's not your request</button>
					<?php elseif($verify_status==1): ?>
						<span class="message-content">You have confirmed customer request.</span>
					<?php elseif($verify_status==2): ?>
						<span class="message-content">You have rejected customer request.</span>
					<?php endif; ?>
				</span><br>
            </div>
          <?php  else:  ?>
           <div>

                <img  src="../<?php
                if (empty($row['photo'])) {
                    echo "upload/profile.jpg";
                } else {
                    echo $row['photo'];
                }
                ?>" style="height:40px; width:40px; position:relative; top:15px; left:10px;border-radius:50% 50%">
                <span style="font-size:10px; position:relative; float:right; top:0px; right:15px;" class="rightt"><i><?php echo date('h:i A', strtotime($row['chat_date'])); ?></i></span><br>
                <span style="font-size:12px; position:relative; float:right; right:13px;" class="rightt del-css"><button class="delete-message" chatid="<?php echo $row['chatid']; ?>">Delete</button></span>
				<span class="msgg" style="font-size:11px; position:relative; top:-20px; left:60px;"><a href="https://www.travpart.com/English/bio/?user=<?php echo $row['username']; ?>"class="chat_name_link"><strong><?php echo $row['uname']; ?></strong>:<br></a> <div class="msg-text"><span class="message-content"><?php echo $row['message']; ?></span></div> </span><br>
            </div>


        <?php endif; ?>		
        <div style="height:5px;"></div>
        <?php
    }
	$last_activity=$wpdb->get_var("SELECT `last_activity` FROM `sessions` WHERE `user_id` = {$_COOKIE['to']} ORDER BY `sessions`.`id` DESC LIMIT 1");
	$is_active=get_user_status(array('last_activity'=>$last_activity));
	if( !$is_active && $showAlertMessage && $_SESSION['access']!=1) {
	?>
	<div class="response-time-alert-message">
		<p>Thanks for your question. Seller will reply as soon as possible within 24 hours!</p>
	</div>
	<?php
		if($wpdb->get_var("SELECT COUNT(*) FROM `user` WHERE userid='{$_SESSION['id']}' AND username='Guest' AND email IS NULL")>0 ) {
		?>
		<div style="background: antiquewhite;">
			<p>We're sorry, this travel advisor is offline right now. Please leave your email here so that he can reach you back later</p>
			<p>Email: <input id="guestEmail" type="email" style="margin-right: 6px;"><button id="guestEmailSave" class="btn btn-sm btn-success">Save</button></p>
		</div>
		<?php
		}
	}

}
?>

   <style type="text/css">
       .msg-text {
            width: 80%;
            text-align: justify;
        }
        @media (max-width: 600px){
           .msg-text {
                width: 72%;
            } 
            span.del-css {
                margin-top: -23px;
            }
        } 
   </style>     
       