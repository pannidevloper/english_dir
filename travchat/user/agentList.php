
<?php include('session.php'); ?>
<?php include('header.php'); ?>
<?php
$id = $_SESSION['id'];
if ($_SESSION['access'] == 1) {
    header('location:./index.php');
}
?>
<body>
    <?php include('navbar.php'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default" style="height:50px;">
                    <span style="font-size:18px; margin-left:10px; position:relative; top:13px;"><strong><span class="glyphicon glyphicon-user"></span> Agent List</strong></span>

                </div>
                <table width="100%" class="table table-striped table-bordered table-hover" id="userList">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Chat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $mem = array('999999');
                        //fetch all current add members
                        $um = mysqli_query($conn, "select * from `chat_member` where chatroomid='" . $_SESSION['id'] . "'");
                        while ($umrow = mysqli_fetch_array($um)) {
                            $mem[] = $umrow['userid'];
                        }
                        //break it into string form
                        $users = implode($mem, ',');
                        //check for user is agent or customer
                        $sql = "
                            select * from `user` 
                            LEFT JOIN `sessions` ON
                            `sessions`.`id` = (
                                SELECT `id` from `sessions` where `user_id` = `user`.`userid` ORDER BY `id` DESC LIMIT 1
                            )
                            where userid not in (" . $users . ") AND access = 1
                        ";
						
						$sql = "select * from `user` where userid not in (" . $users . ") AND access = 1";

                        $query = mysqli_query($conn, $sql);
                        while ($row = mysqli_fetch_array($query)) {
                            $is_active = false;
                            if( $row['last_activity']) {
                                $last_activity = new DateTime( $row['last_activity'] );
                                $now = new DateTime();
                                $is_active = $last_activity < $now && ( ($now->getTimestamp() - $last_activity->getTimestamp()) <= MAX_INACTIVITY_TIME );
                            }
                            ?>
                            <tr>
                                <td>
                                    <?php echo ucfirst($row['uname']); ?>
                                </td>
                                <td>
                                    <span class="status-indicator <?php echo $is_active ? 'online': '' ; ?>"></span>
                                    <?php echo $is_active ? 'Online': 'Offline' ; ?>
                                </td>
                                <td>
                                    <a href='addnewmember.php?user=<?php echo $row['userid'] ?>'>Message</a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>                     
            </div>
        </div>
    </div>
    <?php include('modal.php'); ?>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
    <script src="../js/dataTables.responsive.js"></script>
</body>
</html>
