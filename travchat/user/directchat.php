<?php
require_once($_SERVER['DOCUMENT_ROOT']."/English/wp-load.php");
global $wpdb;
session_start();

$token=filter_input(INPUT_GET, 'token', FILTER_SANITIZE_STRING);
$chatUserid=filter_input(INPUT_GET, 'chatuserid', FILTER_SANITIZE_NUMBER_INT);

if( !empty($token) && $chatUserid>1 && ($userid=$wpdb->get_var("SELECT userid FROM `user` WHERE token='{$token}' AND username='Guest' AND email IS NOT NULL"))>1 ) {
	$_SESSION['id'] = $userid;
	$_SESSION['access'] = 0;
	$_SESSION['user_name'] = 'Guest';
	setcookie('access', 0, time() + (86400 * 30), "/");
	wp_redirect('./index.php?user='.$chatUserid);
}

?>