<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/English/wp-load.php");
global $wpdb;
session_start();
include('./set_active_status.php');
$to = filter_input(INPUT_POST, 'agent', FILTER_SANITIZE_NUMBER_INT);
$from = intval($_SESSION['id']);
if (!empty($_POST['fetch']) && $to > 1 && $from > 1) {
    $results = $wpdb->get_results("SELECT * FROM `chat` WHERE (c_to=$to AND c_from=$from) OR (c_to=$from AND c_from=$to)", ARRAY_A);
    $wpdb->query($conn, "UPDATE chat SET status=1 WHERE c_to={$from} AND c_from={$to}");
    $agent_avatar = $wpdb->get_var("SELECT photo FROM user WHERE userid={$to}");
    if (empty($agent_avatar))
        $agent_avatar = home_url() . "/travchat/upload/profile.jpg";
    else
        $agent_avatar = home_url() . '/travchat/' . $agent_avatar;

    $showAlertMessage = false;
    $last_message_time = 0;
    $nowDay = date('Y-m-d', strtotime($wpdb->get_var("SELECT NOW()")));
    foreach ($results as $row) {
        if (!$showAlertMessage && date('Y-m-d', strtotime($row['chat_date'])) == $nowDay) {
            $showAlertMessage = true;
        }
        if ($row['userid'] == $to)
            $message_class = 'chat_msg_item_admin';
        else
            $message_class = 'chat_msg_item_user';
        $last_message_time = $row['chat_date'];

        ?>
<?php if ($row['type'] == 'attachment') : ?>

<?php elseif ($row['type'] == 'link') : ?>
<span class="chat_msg_item <?php echo $message_class; ?>">
    <?php if ($row['userid'] == $to) { ?>
    <div class="chat_avatar">
        <img src="<?php echo $agent_avatar; ?>" />
    </div>
    <?php }
                echo "<a target='_blank' href='" . $row['message'] . "'>" . $row['message'] . "</a>"; ?>
</span>

<?php elseif ($row['type'] == 'deleted') :  ?>

<?php elseif ($row['type'] == 'verify_tour_request') : ?>

<?php else :  ?>
<span class="chat_msg_item <?php echo $message_class; ?>">
    <?php if ($row['userid'] == $to) { ?>
    <div class="chat_avatar">
        <img src="<?php echo $agent_avatar; ?>" />
    </div>
    <?php }
                echo $row['message']; ?>
</span>

<?php endif; ?>
<?php
    }

    $last_activity = $wpdb->get_var("SELECT `last_activity` FROM `sessions` WHERE `user_id` = {$_COOKIE['to']} ORDER BY `sessions`.`id` DESC LIMIT 1");
    $is_active = get_user_status(array('last_activity' => $last_activity));
    if ($last_message_time == 0)
        $last_message_time = false;
    else if (strtotime($wpdb->get_var("SELECT NOW()")) - strtotime($last_message_time) >= 10)
        $last_message_time = true;
	else
		$last_message_time = false;
    if (!$is_active && $showAlertMessage && $_SESSION['access'] != 1) {
        ?>
<span class="chat_msg_item chat_msg_item_admin" style=" margin: 4px; max-width: 100%; color: green;">
    Thank you for your message, unfortunately the travel advisor is not online right now. We will send your message to the travel advisor right away.
</span>
<?php
        if ( $last_message_time && $wpdb->get_var("SELECT COUNT(*) FROM `user` WHERE userid='{$_SESSION['id']}' AND username='Guest' AND email IS NULL") > 0) {
            ?>
<span class="chat_msg_item chat_msg_item_admin">
    <div class="chat_avatar">
        <img src="<?php echo $agent_avatar; ?>" />
    </div>Agent typically replies in a few hours. Don't miss their reply.
    <div class="chat_form">
        <br>
        <form class="get-notified">
            <label for="chat_log_email">Get notified by email</label>
            <input id="guestEmail" placeholder="Enter your email" />
            <i id="guestEmailSave" class="zmdi zmdi-chevron-right" style="margin-left:-2px"></i>
        </form>
    </div>
</span>
<?php
        }
    }
}
?>