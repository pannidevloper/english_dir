<?php
$url='https://www.tourfrombali.com/faq/';
$ch=curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 60);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
$data=curl_exec($ch);
if(curl_errno($ch))
{
	echo FALSE;
}
else
{
	preg_match_all('!<div class="faqwd_answer">(.+?).{0,1}</div>!is', $data, $faq_answer);

	preg_match_all('!<span class="faqwd_post_title".*?>(.+?)</span>!is', $data, $faq_title);

	if(!empty($faq_title[1]) && count($faq_answer[1])==count($faq_title[1]))
	{
		$faq='';
		for($i=0; $i<count($faq_title[1]); $i++)
		{
			//echo '<h3>'.strip_tags($faq_title[1][$i]).'</h3>';
			//echo '<p>'.strip_tags($faq_answer[1][$i],'<a>').'</p>';
			$faq.='<li><span class="title">'.strip_tags($faq_title[1][$i]).'</span><span class="answer">'.strip_tags($faq_answer[1][$i],'<a>').'</span></li>';
                        
                       
		}
        
		$faq=str_ireplace('sales agent', 'travel advisor', $faq);
		file_put_contents(__DIR__.'/faq_content.txt', str_replace("\n",'',$faq));
		echo TRUE;
	}
	else
		echo FALSE;
}
curl_close($ch);
?>