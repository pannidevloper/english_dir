<?php include('session.php'); ?>
<?php include('header.php'); ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/English/wp-load.php"); ?>

<?php

//session_destroy();

	global $wpdb;
	global $current_user;
	
	if(is_user_logged_in() ){
		//$user = wp_get_current_user();
	//	echo "<pre>"; print_r($user); exit;
	}
?>

<?php
$id = $_SESSION['id'];
$chatq = mysqli_query($conn, "select * from `chatroom` where chatroomid='$id'");
$chatrow = mysqli_fetch_array($chatq);

$cmem = mysqli_query($conn, "select * from `chat_member` where chatroomid='$id'");

$charges = mysqli_query($conn, "select total_charges from `agent_charges` where agent_user_id='$id'");
$agent_charge = mysqli_fetch_assoc($charges);
$_SESSION['agent_charges'] = $agent_charge['total_charges'];


?>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ4N2X6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->  
  <?php include('navbar.php'); ?>
    <div class="container-fluid">
        <div class="row">
            <!--<div class="col-lg-2">
			<div class="my-order-container">
                
			<div class="">
            <table width="100%" class="table  table-bordered table-hover" >
                <thead>
                <tr><th class="text-center my-ord"><a href="./myorders.php">My Orders</a></th>
                </tr></thead>
                <tbody></tbody>
            </table>
			</div>
			</div>
			</div>-->

			<div class="col-lg-12 my-ch-container">
				<div class="pc-memmber-list">
				<?php include('mychat.php');  ?>
				</div>
				<?php include('room.php'); ?>
				<div class="mobile-memmber-list" style="display:none;">
				<?php include('mychat.php');  ?>
				</div>
			</div>
        </div>
    </div>
    <?php include('room_modal.php'); ?>
    <?php include('out_modal.php'); ?>
    <?php include('modal.php'); ?>

<!--      user avatar popup -->
      <style>
        .trav-rating {
          display: inline-block;
          vertical-align: middle; }
        .trav-rating > span {
          border-radius: 2px;
          background-color: #fc8c14;
          font-size: 14px;
          line-height: 1;
          padding: 2px 4px;
          color: #fff;
          text-align: center; }
        .trav-rating > i {
          line-height: 21px;
          display: inline-block;
          color: #fc8c14;
          margin-left: 5px;
          margin-right: 5px; }
        .trav-rating > small {
          font-size: 14px;
          line-height: 21px;
          color: #666666; }

        .avatar-small {
          width: 50px;
          height: 50px;
          background-position: center center;
          background-size: cover;
          border-radius: 50%; }

        .avatar-popver {
          position: absolute;
          left: -50000rem;
          max-width: 230px;
          position: relative;
          padding-left: 18px; }
        .avatar-popver .arrow {
          position: absolute;
          top: 40px;
          left: 3px;
          display: block;
          width: 0;
          height: 0; }
        .avatar-popver .arrow:before {
          position: absolute;
          top: 0px;
          left: 0px;
          display: block;
          width: 0;
          height: 0;
          content: '';
          border-color: #dcdcdc;
          border-style: solid;
          border-width: 16px 16px 16px 0px;
          border-left-color: transparent;
          border-top-color: transparent;
          border-bottom-color: transparent; }
        .avatar-popver .arrow:after {
          position: absolute;
          top: 1px;
          left: 1px;
          display: block;
          width: 0;
          height: 0;
          content: '';
          border-color: #fff;
          border-style: solid;
          border-width: 15px 15px 15px 0px;
          border-left-color: transparent;
          border-top-color: transparent;
          border-bottom-color: transparent; }
        .avatar-popver .inner {
          border: 1px solid #dcdcdc;
          border-radius: 3px;
          box-shadow: 1.4px 3.7px 13px 0 rgba(0, 0, 0, 0.11);
          background-color: #ffffff; }

        .avatar-popver-top {
          padding: 10px;
          padding-left: 75px;
          position: relative;
          color: #333;
          font-size: 12px; }
        .avatar-popver-top .avatar-small {
          position: absolute;
          top: 12px;
          left: 12px; }
        .avatar-popver-top .username {
          font-size: 14px;
          font-weight: 600;
          margin-bottom: 5px; }

        .avatar-popver-bottom {
          padding: 0;
          border-top: 1px solid #dcdcdc;
          background-color: #f9f9f9;
          text-align: center;
          display: flex; }
        .avatar-popver-bottom a {
          text-decoration: none;
          color: #666666;
          font-weight: 600;
          font-size: 12px;
          display: block;
          flex: 1 1 50%;
          max-width: 50%;
          padding: 5px; }
        .avatar-popver-bottom a:first-child {
          border-right: 1px solid #dcdcdc; }

        .hidden {
          display: none; }

      </style>
      <link rel="stylesheet" id="font-awesome-css" href="//use.fontawesome.com/releases/v5.4.1/css/all.css" type="text/css" media="all">
      <script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
      <script src="https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js"></script>

      <!--avatar popup-->
      <div id="avatar-popver" class="avatar-popver hidden">
        <div class="arrow"></div>
        <div class="inner">
          <div class="avatar-popver-top">
            <div class="avatar-small" style="background-image: url(https://www.travpart.com/Chinese/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg)"></div>
            <div class="detail">
              <div class="username" id="user_name"></div>
              
              <div class="rating-wrap">
                
                <span class="trav-rating"><span>5.0</span><i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></i></span>

              </div>
          
            </div>

          </div>
          <div class="avatar-popver-bottom" id="view_user" >
            
            <a href="#" class="bloc-user"></a>
          </div>
        </div>
      </div>
      <script>
        var popperInstance;
        function showAvatarPopver(reference,popper) {
          popperInstance = new Popper(reference, popper, {
            placement:'right'
          });
          popper.classList.remove('hidden');
          return popperInstance
        }
        var popper = document.getElementById('avatar-popver');
        var ref = document.getElementById('user_details');

        ref.addEventListener('click',function (e) {
          e.stopPropagation();
          e.preventDefault();
          showAvatarPopver(ref,popper);
        })
        document.addEventListener('click',function (e) {
          popper.classList.add('hidden');
        });
      </script>


    


    <script>
        $(document).ready(function () {
			
			$('.dropdown-toggle').dropdown('toggle');

            $('#myChatRoom').DataTable({
                "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
                "bLengthChange": false,
                "bInfo": false,
                "bPaginate": true,
                "bFilter": true,
                "bSort": false,
                "pageLength": 8
            });

            displayChat();

            $(document).on('click', '#send_msg', function () {
                   var access = '<?php echo $_SESSION['access'];   ?>';
                  /* if($('#chat_area').html() == '' && access==1){
                       alert('You cant initiate chat.');
                       return false;
                   } */
                   
                   
                if ($('#chat_msg').val() == "") {
                    alert('Please write message first');
                } else {
                    var msgg = $('#chat_msg').val();
                    
                    $.ajax({
                        type: "POST",
                        url: "send_message.php",
                        data: {
                            msg:msgg
                        },
                        success: function(response){
                            console.log(response);
                            $('#chat_msg').val("");
                            displayChat();
                        }
                    });
                   
                }
            });
			
			$(document).on('click', '.delete-message', function () {
				var chatid=$(this).attr('chatid');
				var delete_message_button=$(this);
                $.ajax({
                  type: "GET",
                  url: "delete_message.php?chatid="+chatid,
                  success: function (data) {
					  console.log(data);
					  if(data==1) {
						  delete_message_button.parent().next().find('.message-content').html('<span class="deleted-message">Deleted</span>');
						  delete_message_button.remove();
					  }
                  }
                });
            });
			
			$(document).on('click', '.confirm_request', function () {
				$(this).parent().find('button').attr('disabled','disabled');
                $.ajax({
                  type: "POST",
                  url: "/English/travchat/user/verify_tour_request.php",
				  data: {
					  tourid: $(this).attr('tourid'),
					  confirm: 1
				  },
				  dataType: "json",
                  success: function (data) {
					  displayChat();
                  }
                });
            });
			
			$(document).on('click', '.reject_request', function () {
				$(this).parent().find('button').attr('disabled','disabled');
                $.ajax({
                  type: "POST",
                  url: "/English/travchat/user/verify_tour_request.php",
				  data: {
					  tourid: $(this).attr('tourid'),
					  confirm: 2
				  },
				  dataType: "json",
                  success: function (data) {
					  displayChat();
                  }
                });
            });
			
			$(document).on('click', '.emoji_item', function (e) {
				e.preventDefault();
				$('#chat_msg').val($('#chat_msg').val()+$(this).text());
			});

            $(document).on('click', '#confirm_leave', function () {
                id = <?php echo $id; ?>;
                $('#leave_room').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $.ajax({
                    type: "POST",
                    url: "leaveroom.php",
                    data: {
                        id: id,
                        leave: 1,
                    },
                    success: function () {
                        window.location.href = 'index.php';
                    }
                });

            });

            $(document).on('click', '#confirm_delete', function () {
                id = <?php echo $id; ?>;
                $('#confirm_delete').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $.ajax({
                    type: "POST",
                    url: "deleteroom.php",
                    data: {
                        id: id,
                        del: 1,
                    },
                    success: function () {
                        window.location.href = 'index.php';
                    }
                });

            });

            $(document).keypress(function (e) {
                if (e.which == 13) {
                    $("#send_msg").click();
                }
            });

            $("#user_details").hover(function () {
                $('.showme').removeClass('hidden');
            }, function () {
                $('.showme').addClass('hidden');
            });

            //
            $(document).on('click', '.delete2', function () {
                var rid = $(this).val();
                $('#delete_room2').modal('show');
                $('.modal-footer #confirm_delete2').val(rid);
            });

            $(document).on('click', '#confirm_delete2', function () {
                var nrid = $(this).val();
                $('#delete_room2').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $.ajax({
                    url: "deleteroom.php",
                    method: "POST",
                    data: {
                        id: nrid,
                        del: 1,
                    },
                    success: function () {
                        window.location.href = 'index.php';
                    }
                });
            });

            $(document).on('click', '.leave2', function () {
                var rid = $(this).val();
                $('#leave_room2').modal('show');
                $('.modal-footer #confirm_leave2').val(rid);
            });

            $(document).on('click', '#confirm_leave2', function () {
                var nrid = $(this).val();
                $('#leave_room2').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $.ajax({
                    url: "leaveroom.php",
                    method: "POST",
                    data: {
                        id: nrid,
                        leave: 1,
                    },
                    success: function () {
                        window.location.href = 'index.php';
                    }
                });
            });
			
			$('#chat_area').on('click', '#guestEmailSave', function () {
                $(this).addClass('disabled');
				$.ajax({
					url: "update_email.php",
                    method: "POST",
                    data: {
                        email: $('#guestEmail').val(),
                    },
                    success: function (status) {
                        if(status==0)
							alert('Saved failed. Try again!');
						else if(status==2)
							alert('The email address has been used.');
						else
							alert('Saved!');
						displayChat();
                    }
				});
            });
			
        });

        var interval = 30000;

        function displayChat() {
            $.ajax({
                url: 'fetch_chat.php',
                type: 'POST',
                async: false,
                data: {

                    fetch: 1
                },
                success: function (response) {
                    $('#chat_area').html(response);
                    $("#chat_area").scrollTop($("#chat_area")[0].scrollHeight);
                }
            });
        }

       setTimeout(displayChat, interval);


        function saveUserId(id,that,uname,country,region,name,timezone,photo, status) {
			$(that).find('.badge').text('0');
			
            $('#time').html('');
			if(country!='')
			{
				var now=new Date();
				var d=now.getHours()>12?((now.getHours()-12)+':'+now.getMinutes()+' PM'):(now.getHours()+':'+now.getMinutes()+' AM');
				$('#chating-with').html( uname + ("<span class='status-indicator " + (status ? 'online' : '') + "'></span>") + ("<span class='status-text'>" +( status ? 'Online' : 'Offline')+"</span>" ));
				$('#time').html(d);
                $('#place').html(region+" ,"+country);
                if(photo!='')
                {
                console.log(region);
                $('#img').attr('src','../'+photo);
                }
			}
			else{}

            
            $('#chating-with').html(uname);
          $('#user_name').html(uname);
          $('#view_user').html("<a href=https://www.travpart.com/English/bio/?user=" +name+"  class='view-profile'>View Profile</a>");
            var row = $('#myChatRoom').find('tr');
            console.log(row);
            for (var i = 0; i < row.length; i++) {
                $(row[i]).removeClass('success');
            }

            $(that).addClass('success');
            document.cookie = "to=" + id;
            takingUserId = id;
            displayChat();
        }
//this function to select user when page loaded
        $(document).ready(function () {

            var user = '<?php echo(isset($_GET['user']) && !empty($_GET['user']) ? $_GET['user'] : ''); ?>';
            if (user != '') {
                $('#' + user).click();
            } else {
                var button = $('#myChatRoom').find('tr')[1];
                $(button).click();
                var timeZoneName = Intl.DateTimeFormat().resolvedOptions().timeZone;
                document.cookie = "timeZoneName=" + timeZoneName;
            }
        });


        function fetchUser(value) {
            $.ajax({
                url: 'changeAddMember.php',
                type: 'POST',
                async: false,
                data: {
                    value: value,
                    fetch: 1
                },
                success: function (response) {
                    $("#user").html(response);
                }
            });
        }

        setInterval(calcTime, 1000);

        offset = '+5.5';
        function calcTime( ) {
            // create Date object for current location
            d = new Date();
            if (getCookie('takingUserId') != '') {
                offset = getCookie('takingUserId');
            }
            // convert to msec
            // add local time zone offset 
            // get UTC time in msec
            utc = d.getTime() + (d.getTimezoneOffset() * 60000);

            // create new Date object for different city
            // using supplied offset
            nd = new Date(utc + (3600000 * offset));

            var time = nd.toLocaleString();
            // return time as a string

            $('#time').html(time.split(',')[1]);
        }



        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function changeTimeZone(value) {
            document.cookie = takingUserId + "=" + value;
            $('#closeTimeZoneModel').click();
        }


        $(document).on('click', '#send_file', function () {

            $.ajax({
                type: "POST",
                url: "send-attachment.php",
                data: {
                    id: id,
                    leave: 1
                },
                success: function () {
                    window.location.href = 'index.php';
                }
            });

        });

        function checkFileAndSend(e) {
            e.preventDefault();
            var formData = new FormData($('#attachment')[0]);
            console.log(formData);
            $.ajax({
                url: 'send-attachment.php',
                type: 'POST',
                data: formData,
                processData: false, // tell jQuery not to process the data
                contentType: false, // tell jQuery not to set contentType
                success: function (data) {
                    console.log(data);
                    var response = JSON.parse(data);
                    if (response.success == 1) {
                        $('#close_send_attachment_model').click();
                        displayChat();
                    }
                }
            });
        }

        function downloadAttachment(address, type) {
            window.location.href = 'download-attachment.php?file=' + address + "&type=" + type;
//            $.ajax({
//                url: 'download-attachment.php?file='+address+"&type="+type,
//                type: 'GET',
//                processData: false, // tell jQuery not to process the data
//                success: function (data) {
//                    console.log(data);
//
//                }
//            });
        }

        function checkFileBeforeUpload() {
            var fileObj = document.getElementById('user_img_file').files[0];
            var type = fileObj.type;
            if (type.split('/')[0] == 'image') {
                $('#user_img_uld').attr('disabled', false);
            } else {
                $('#user_img_uld').attr('disabled', true);
            }

//            var ext = fileObj.name.split('.');
//            var extention = {'csv': 1};
        }
        
        //send link in chat
    function sendLink(e){
        e.preventDefault();
      
        var formData = {
                'link': $('#link').val() //for get email 
            };
        $.ajax({
                url: 'send_link.php',
                type: 'POST',
                data:formData,
                success: function (data) {
                    console.log(data);
                    var response = JSON.parse(data);
                    if (response.success == 1) {
                        $('#close_send_link_model').click();
                        displayChat();
                    }
                }
            });
    }
    
    function paymentPageJquery(e){
        e.preventDefault();
        var form = new FormData($('#payment-form')[0]);
        console.log(form);
        $.ajax({
           type: 'POST',
           url: 'payment.php',
           contentType: false,
           processData: false,
           data: form, 
            success: function(data){
              //console.log(data);
              var response = JSON.parse(data);
              if(response.success == 1){
                 var  $msg = response.data;
                    $.ajax({
                        type: "POST",
                        url: "send_message.php",
                        data: {
                            msg: $msg
                        },
                        success: function (response) {
                            displayChat();
                            $('#create_payment').modal('hide');
                           
                        }
                    });
              }else{
                  $("payment-error").html(response.data);
              }
              
               } 
        });
    }


    </script>	
    <script>
    $(document).ready(function(){
        $('#myChatRoom_filter').parent('div').removeClass('pull-right'); 
    });
    </script>
</body>
</html>