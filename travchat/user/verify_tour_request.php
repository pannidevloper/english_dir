<?php
require_once($_SERVER['DOCUMENT_ROOT']."/English/wp-load.php");
global $wpdb;
session_start();
if (!empty($_POST['bookingcode'])) {
	$bookingcode=filter_input(INPUT_POST, 'bookingcode', FILTER_SANITIZE_STRING);
	$tourid=intval(substr($bookingcode, 4));
    $to = intval($_COOKIE['to']);
	$from = intval($_SESSION['id']);
	
	if ($wpdb->get_var("SELECT COUNT(*) FROM user,wp_users,wp_tourmeta t WHERE user.userid='{$to}' AND user.username=wp_users.user_login AND t.tour_id='{$tourid}' AND t.meta_key='userid' AND t.meta_value=wp_users.ID")!=1) {
		echo json_encode(array('error'=>1, 'error_msg'=>'This tour is not belong to this agent.'));
	}
	else {
		$customer_id=$wpdb->get_var("SELECT customer_id FROM verify_tour_request WHERE tour_id='{$tourid}'");
		if(!empty($customer_id)) {
			if($customer_id==$from) {
				echo json_encode(array('error'=>1, 'error_msg'=>'You have sent the request to verify your tour request.'));
			}
			else {
				echo json_encode(array('error'=>1, 'error_msg'=>'This tour is not your request.'));
			}
		}
		else {
			$wpdb->query("INSERT INTO `verify_tour_request` (`tour_id`, `customer_id`, `verify_status`) VALUES ('{$tourid}', '{$from}', '0')");
			$wpdb->query("INSERT INTO `chat` (`userid`, `message`, `chat_date`, `c_to`, `c_from`, `type`, `status`) VALUES ('{$from}', '{$tourid}', CURRENT_TIMESTAMP, '{$to}', '{$from}', 'verify_tour_request', '1')");
			echo json_encode(array('error'=>0));
		}
	}
}
else if(!empty($_POST['tourid']) && !empty($_POST['confirm'])) {
	$tourid=intval($_POST['tourid']);
	$agent_id = intval($_SESSION['id']);
	$confirm=intval($_POST['confirm'])==1?1:2;
	if($wpdb->get_var("SELECT COUNT(*) FROM user,wp_users,wp_tourmeta t WHERE user.userid='{$agent_id}' AND user.username=wp_users.user_login AND t.tour_id='{$tourid}' AND t.meta_key='userid' AND t.meta_value=wp_users.ID")==1) {
		if($wpdb->get_var("SELECT COUNT(*) FROM verify_tour_request WHERE tour_id='{$tourid}'")>0) {
			$tp_pts_tor_pkg=get_option('_travpart_vo_option')['tp_pts_tor_pkg'];
			$wpdb->query("UPDATE `verify_tour_request` SET verify_status={$confirm} WHERE tour_id='{$tourid}'");
			$wpdb->query("UPDATE wp_usermeta,verify_tour_request,user,wp_users SET wp_usermeta.meta_value=wp_usermeta.meta_value+{$tp_pts_tor_pkg} WHERE verify_tour_request.tour_id='{$tourid}' AND  verify_tour_request.customer_id=user.userid AND user.username=wp_users.user_login AND wp_users.ID=wp_usermeta.user_id AND wp_usermeta.meta_key='game_points' ");
			//save the tour to buyer profile
			$userID=$wpdb->get_var("SELECT ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND user.userid='".intval($_COOKIE['to'])."'");
			$wpdb->query("INSERT INTO `wp_client_tour` (`user_id`, `tour_id`) VALUES ('{$userID}', '{$tourid}')");
			echo json_encode(array('error'=>0));
		}
	}
}
