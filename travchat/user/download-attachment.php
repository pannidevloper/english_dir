<?php

if (isset($_GET['file']) && isset($_GET['type'])) {
    
    $file = "../upload/" . $_GET['file'];
    $type = $_GET['type'];
    header('Content-Description: File Transfer');
    header('Content-Type: ' . $type);
    header('Content-Disposition: attachment; filename=' . $file);
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
    exit;
} else {
    echo "fail to download";
}
?>