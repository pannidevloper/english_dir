<?php
if (php_sapi_name()!='cli') {
    exit;
}
require_once(__DIR__.'/../conn.php');
require_once(__DIR__.'/../../wp-load.php');
global $wpdb;
function sendmail($to, $subject, $message)
{
    $key="Tt3At58P6ZoYJ0qhLvqdYyx21";
    $postdata=array('to'=>$to,
                    'subject'=>$subject,
                    'message'=>$message);
    $url="https://www.tourfrombali.com/api.php?action=sendmail&key={$key}";
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $data=curl_exec($ch);
    if (curl_errno($ch) || $data==false) {
        curl_close($ch);
        return false;
    } else {
        curl_close($ch);
        return true;
    }
}

function generate_email_content($message, $chatUserid, $is_guset=false, $token=null)
{
    if ($is_guset) {
        $reply_link='https://www.travpart.com/English/travchat/user/directchat.php?chatuserid='.$chatUserid.'&token='.$token;
    } else {
        $reply_link='https://www.travpart.com/English/travchat/user/index.php?user='.$chatUserid;
    }
    $email_content='<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting"> 
    <title>You received a message at Travchat</title>

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet">

<style>
html,
body {
    margin: 0 auto !important;
    padding: 0 !important;
    height: 100% !important;
    width: 100% !important;
    background: #f1f1f1;
}

* {
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
}

div[style*="margin: 16px 0"] {
    margin: 0 !important;
}

table,
td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
}

table {
    border-spacing: 0 !important;
    border-collapse: collapse !important;
    table-layout: fixed !important;
    margin: 0 auto !important;
}

img {
    -ms-interpolation-mode:bicubic;
}

a {
    text-decoration: none;
}

*[x-apple-data-detectors],  /* iOS */
.unstyle-auto-detected-links *,
.aBn {
    border-bottom: 0 !important;
    cursor: default !important;
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
}

.a6S {
    display: none !important;
    opacity: 0.01 !important;
}

.im {
    color: inherit !important;
}
.travchatdate{
	color: #999;
	margin-top: -10px;
}
.travchatthumbnail{
	width: 50px;
	height: 50px;
	border: 2px solid #3b898a;
	-webkit-border-radius: 50%;
	-moz-border-radius: 50%;
	border-radius: 50%;
}
.blacktext{
	color: black
}
.newmessage{
	display: inline-block;
	color: white;
	background: #5bbc2e;
	padding: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	margin: 10px;
	float: right;
	clear: both;

}
img.g-img + div {
    display: none !important;
}

@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
    u ~ div .email-container {
        min-width: 320px !important;
    }
}

@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
    u ~ div .email-container {
        min-width: 375px !important;
    }
}

@media only screen and (min-device-width: 414px) {
    u ~ div .email-container {
        min-width: 414px !important;
    }
}
</style>

<style>
.primary{
	background: #f3a333;
}
.bg_white{
	background: #ffffff;
}
.bg_light{
	background: #fafafa;
}
.bg_green{
	background: #3b898a;
}
.bg_dark{
	background: rgba(0,0,0,.8);
}
.email-section{
	padding:2.5em;
}
/*BUTTON*/
.btn{
	padding: 10px 15px;
}
.btn.btn-primary{
	border-radius: 30px;
	background: #3b898a;
	color: #ffffff;
}
h1,h2,h3,h4,h5,h6{
	font-family: "Playfair Display", serif;
	color: #000000;
	margin-top: 0;
}
body{
	font-family: "Montserrat", sans-serif;
	font-weight: 400;
	font-size: 15px;
	line-height: 1.8;
	color: rgba(0,0,0,.4);
}
a{
	color: #f3a333;
}
table{
}
/*LOGO*/
.logo h1{
	margin: 0;
}
.logo h1 a{
	color: #000;
	font-size: 20px;
	font-weight: 700;
	text-transform: uppercase;
	font-family: "Montserrat", sans-serif;
}

.hero{
	position: relative;
}
.hero img{
}
.hero .text{
	color: rgba(255,255,255,.8);
}
.hero .text h2{
	color: #ffffff;
	font-size: 30px;
	margin-bottom: 0;
}
/*HEADING SECTION*/
.heading-section{
}
.heading-section h2{
	color: #000000;
	font-size: 28px;
	margin-top: 0;
	line-height: 1.4;
}
.heading-section .subheading{
	margin-bottom: 20px !important;
	display: inline-block;
	font-size: 13px;
	text-transform: uppercase;
	letter-spacing: 2px;
	color: rgba(0,0,0,.4);
	position: relative;
}
.heading-section .subheading::after{
	position: absolute;
	left: 0;
	right: 0;
	bottom: -10px;
	width: 100%;
	height: 2px;
	background: #3b898a;
	margin: 0 auto;
}
.heading-section-white{
	color: rgba(255,255,255,.8);
}
.heading-section-white h2{
	font-size: 28px;
	font-family: 
	line-height: 1;
	padding-bottom: 0;
}
.heading-section-white h2{
	color: #ffffff;
}
.heading-section-white .subheading{
	margin-bottom: 0;
	display: inline-block;
	font-size: 13px;
	text-transform: uppercase;
	letter-spacing: 2px;
	color: rgba(255,255,255,.4);
}
.icon{
	text-align: center;
}
.icon img{
}

.text-services{
	padding: 10px 10px 0; 
	text-align: center;
}
.text-services h3{
	font-size: 20px;
}

.text-services .meta{
	text-transform: uppercase;
	font-size: 14px;
}

.text-testimony .name{
	margin: 0;
}
.text-testimony .position{
	color: rgba(0,0,0,.3);
}

.img{
	width: 100%;
	height: auto;
	position: relative;
}
.img .icon{
	position: absolute;
	top: 50%;
	left: 0;
	right: 0;
	bottom: 0;
	margin-top: -25px;
}
.img .icon a{
	display: block;
	width: 60px;
	position: absolute;
	top: 0;
	left: 50%;
	margin-left: -25px;
}

.counter-text{
	text-align: center;
}
.counter-text .num{
	display: block;
	color: #ffffff;
	font-size: 34px;
	font-weight: 700;
}
.counter-text .name{
	display: block;
	color: rgba(255,255,255,.9);
	font-size: 13px;
}
/*FOOTER*/
.footer{
	color: rgba(255,255,255,.5);
}
.footer .heading{
	color: #ffffff;
	font-size: 20px;
}
.footer ul{
	margin: 0;
	padding: 0;
}
.footer ul li{
	list-style: none;
	margin-bottom: 10px;
}
.footer ul li a{
	color: rgba(255,255,255,1);
}
@media screen and (max-width: 500px) {
	.icon{
		text-align: left;
	}
	.text-services{
		padding-left: 0;
		padding-right: 20px;
		text-align: left;
	}
}
</style>


</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #222222;">
	<center style="width: 100%; background-color: #f1f1f1;">
    <div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
      &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
    </div>
    <div style="max-width: 600px; margin: 0 auto;" class="email-container">
    	<!-- BEGIN BODY -->
      <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
      	<tr>
          <td class="bg_white logo" style="padding: 1em 2.5em; text-align: center">
            <a href="https://www.travpart.com/English/travchat" target="_blank"><img src="https://www.travpart.com/English/wp-content/uploads/2018/11/travchat.jpg" alt="travchat" width="250"></a>
          </td>
	      </tr><!-- end tr -->
	      <tr>
		      <td class="bg_white">
		        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		          <tr>
		            <td class="bg_dark email-section" style="text-align:center;">
		            	<div class="heading-section heading-section-white">
		              	<h2>You received a message at Travchat</h2>
		            	</div>
		            </td>
		           </tr>
		           <tr>
		             <td class="bg_white email-section">
		            	<div class="heading-section heading-section-white">
		            	'.$message.'	
		            	<hr>              	
		              	<p class="blacktext">To reply to your message, please click the button below.</p>
		              	<p><a href="'.$reply_link.'" class="btn btn-primary">Reply</a></p>
                        <br>
                        <p style="color: black;"><b>Note:</b> This email was sent from an unmonitored address. Please do not respond to this email address.</p>
		            	</div>
		            </td>
		          </tr><!-- end: tr -->
      <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
      	<tr>
          <td valign="middle" class="bg_green footer email-section">
            <table>
            	<tr>
                <td valign="top" width="66.666%" style="padding-top: 20px;">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                      <td style="text-align: left; padding-right: 10px;">
                      	<h3 class="heading">Travchat</h3>
                      	<p>Chat with our travel agents.</p>
                      </td>
                    </tr>
                  </table>
                </td>               
                <td valign="top" width="33.333%" style="padding-top: 20px;">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                      <td style="text-align: left; padding-left: 10px;">
                      	<h3 class="heading">Useful Links</h3>
                      	<ul>
			                <li><a href="https://www.travpart.com/English/about-us/" target="_blank">About Us</a></li>
			                <li><a href="https://www.travpart.com/English/tutorial/" target="_blank">Tutorial</a></li>
			                <li><a href="https://www.travpart.com/blog/" target="_blank">Blog</a></li>
			                <li><a href="https://www.travpart.com/English/travcust/" target="_blank">Create a tour package</a></li>
			              </ul>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr><!-- end: tr -->
        <tr>
        	<td valign="middle" class="bg_green footer email-section">
        		<table>
        		 <tr>
		          	<td><a href="https://play.google.com/store/apps/details?id=com.travpart.english" target="_blank"><img src="https://www.travpart.com/wp-content/uploads/2019/03/download-our-free-app.jpg" alt="download app" style="width: 100%"></a></td>
		          	</tr>
            	<tr>
                <td valign="top" width="100%">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                      <td style="text-align: left; padding-right: 10px;color:white">
                      <hr>
                      	<p>&copy; 2019 Travchat. All Rights Reserved</p>
                      </td>
                    </tr>
                  </table>
                </td>               
              </tr>
            </table>
        	</td>
        </tr>
      </table>
    </div>
  </center>
  <hr />
  <p style="text-align: center;">
            <span style="color: #999999;">
                Jl. Dewi Sartika Jakarta, RT.5/RW.2, Indonesia 13630<br />
                <a href="http://www.travpart.com">www.travpart.com</a>
            </span>
        </p>
        <p style="text-align: center;">
            <span style="color: #999999;">
                You are receiving this email because of your relationship with us.<br />
                <a href="UNSUBSCRIBE_LINK" target="_blank" rel="noopener">Unsubscribe</a> &nbsp;from travpart.<br />
            </span>
        </p>
        <p style="text-align: center;">
            &nbsp;<a href="https://www.travpart.com/English/contact/">Contact us</a>
            &nbsp;&bull; <a href="https://www.travpart.com/English/termsofuse/">Privacy&amp;&nbsp;Policy</a>
        </p>
</body>
</html>';
    return $email_content;
}

/*$unread_time=0; //seconds
$unread_messages=mysqli_query($conn, "SELECT chat.*, user.uname, user.photo FROM chat, user WHERE unix_timestamp()-unix_timestamp(chat_date)>={$unread_time} AND status=0 AND chat.userid=user.userid AND c_to IN(SELECT userid FROM `user` WHERE `email` IS NOT NULL)") or die(mysqli_error());
$k=0;
while($row = mysqli_fetch_array($unread_messages, MYSQL_ASSOC))
{
    $mail_msg[$k]=$row['message'];
    $date_msg[$k]=$row['chat_date'];

    $email_content='<div>
            <img style="float: left;" src="'.(empty($row['photo'])?'http://www.travpart.com/English/travchat/upload/profile.jpg':('http://www.travpart.com/English/travchat/'.$row['photo'])).'" width="64" height="64" />
            <b style="margin-left: 5px;">'.$row['uname'].'</b>';
    for($i=0;$i<=$k;$i++) {
        $email_content.='<span style="margin-left: 5px;">'.$date_msg[$i].'</span></div>
            <div style="padding: 35px 2px;">'.$mail_msg[$i].'</div>';
    }
    $email_content.='<a style="background-color: #4caf50; color: white; padding: 12px; border: none; text-decoration: none;" href="http://travpart.com/English/travchat">reply</a>';

    $email_address=mysqli_fetch_array(mysqli_query($conn, "SELECT email FROM `user` WHERE email IS NOT NULL AND userid={$row['c_to']}"),MYSQL_ASSOC) or die();
    $email_address=$email_address['email'];

    mysqli_query($conn, "UPDATE chat SET status=1 WHERE chatid={$row['chatid']}") or die();
    $k++;
}
sendmail($email_address, 'You have an unread message from Travpart.com', $email_content);*/

$unread_time=10; //seconds
$unread_messages=$wpdb->get_results("SELECT chat.*, user.uname, user.photo FROM chat, user WHERE unix_timestamp()-unix_timestamp(chat_date)>=0 AND unix_timestamp()-unix_timestamp(chat_date)<={$unread_time} AND status=0 AND chat.userid=user.userid AND c_to IN(SELECT userid FROM `user` WHERE `email` IS NOT NULL) ORDER BY c_to,c_from", ARRAY_A);
$last_row='';
while (!empty($last_row) || $row=array_shift($unread_messages)) {
    $chatUser=$wpdb->get_row("SELECT username,email,user_registered FROM `user` WHERE email IS NOT NULL AND userid={$row['c_to']}", ARRAY_A);
    $email_address=$chatUser['email'];
    //mysqli_query($conn, "UPDATE chat SET status=1 WHERE c_to={$row['c_to']} AND c_from={$row['c_from']}");
    $is_guset=false;
    if ($chatUser['username']=='Guest') {
        $is_guset=true;
        $token=md5($email_address .rand(1000, 99999). time());
        $wpdb->query("UPDATE user SET token='{$token}' WHERE userid={$row['c_to']}");
    }
    
    $i=1;
    $current_to=$row['c_to'];
    $current_from=$row['c_from'];
    $message='';
    do {
        if ($current_to!=$row['c_to'] || $current_from!=$row['c_from']) {
            break;
        }
        if ($i%2==1) {
            $message.='<table style="width:100%" class="travchatreceiver">
		            		<tr>
		            			<td style="width: 15%">
									<div class="travchatthumbnail">
										<img src="'.(empty($row['photo'])?'http://www.travpart.com/English/travchat/upload/profile.jpg':('http://www.travpart.com/English/travchat/'.$row['photo'])).'"
										style="height: 50px;width: 50px;position:relative;border-radius:50% 50%;">
									</div>
								</td>
		            			<td>
		            			<div class="subheading" style="text-align:left;color:#3b898a">'.$row['uname'].'</div>
		            			<div class="travchatdate">'.$row['chat_date'].'</div></td>
		            		</tr>
		            		<tr style="background:white;color:black;">
		            			<td colspan="2" style="padding:20px">'.$row['message'].'</td>
		            		</tr>
		            	</table>';
        } else {
            $message.='<table style="width:100%" class="bg_light travchatsender">
		            		<tr class="bg_light">
		            			<td style="width: 15%">
									<div class="travchatthumbnail">
										<img src="'.(empty($row['photo'])?'http://www.travpart.com/English/travchat/upload/profile.jpg':('http://www.travpart.com/English/travchat/'.$row['photo'])).'"
										style="height: 50px;width: 50px;position:relative;border-radius:50% 50%;">
									</div>
								</td>
		            			<td>
		            			<div class="subheading" style="text-align:left;color:#3b898a">'.$row['uname'].'</div>
		            			<div class="travchatdate">'.$row['chat_date'].'</div></td>
		            		</tr>
		            		<tr style="background:white;color:black;">
		            			<td colspan="3" style="padding:20px" class="bg_light">'.$row['message'].'</td>
		            		</tr>
		            	</table>';
        }
        $i++;
    } while ($row=array_shift($unread_messages));
    $last_row=$row;

	$unsubscribe_link=home_url('unsubscribe').'/?qs='.base64_encode($chatUser['email'].'||'.md5('EC'.$chatUser['username'].$chatUser['email'].$chatUser['user_registered']));
	$enable_notification=$wpdb->get_var("SELECT subscribe FROM `unsubscribed_emails` WHERE email='{$chatUser['email']}' AND type=2");

    if (is_null($enable_notification) || $enable_notification==1) {
        if (empty($email_address)) {
            break;
        } elseif ($is_guset) {
            sendmail($email_address, 'You have an unread message from Travpart.com.', str_replace('UNSUBSCRIBE_LINK', $unsubscribe_link, generate_email_content($message, $current_from, $is_guset, $token)));
        } else {
            sendmail($email_address, 'You have an unread message from Travpart.com.', str_replace('UNSUBSCRIBE_LINK', $unsubscribe_link, generate_email_content($message, $current_from)));
        }
    }
}
