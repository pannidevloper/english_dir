<!-- Leave Room -->
<div class="modal fade" id="leave_room" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Leaving Room...</h4></center>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <h3><center>Are you sure?</center></h3>
                    <span style="font-size: 11px;"><center><i>Note: Once you leave the room and you wanted to come back, password is needed for a locked room.</i></center></span>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                <button type="submit" class="btn btn-warning" id="confirm_leave"><span class="glyphicon glyphicon-check"></span> Leave</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Delete Room -->
<div class="modal fade" id="delete_room" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Deleting Room...</h4></center>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <h3><center>Are you sure?</center></h3>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                <button type="submit" class="btn btn-danger" id="confirm_delete"><span class="glyphicon glyphicon-check"></span> Delete</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Add Member -->
<div class="modal fade" id="add_member" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Add Member</h4></center>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="POST" action="addnewmember.php?id=<?php echo $id; ?>">
                        <div class="form-group input-group">
                            <span class="input-group-addon" style="width:150px;">Select:</span>
                            <select style="width:350px;" class="form-control" name="user" id="user">
                                <?php
                                include('../conn.php');
                                $mem = array('999999');
                                //fetch all current add members
                                $um = mysqli_query($conn, "select * from `chat_member` where chatroomid='" . $_SESSION['id'] . "'");
                                while ($umrow = mysqli_fetch_array($um)) {
                                    $mem[] = $umrow['userid'];
                                }
                                //break it into string form
                                $users = implode($mem, ',');
                                //check for user is agent or customer
                                if ($_SESSION['access'] == 1) {
                                    //if agent then fetch information of not added customer
                                    $sql = "select * from `user` where userid not in (" . $users . ") AND access = 2";
                                } else {
                                    //if customer then fetch information of not added agent
                                    $sql = "select * from `user` where userid not in (" . $users . ") AND access = 1";
                                }

                                $u = mysqli_query($conn, $sql);
                                if (mysqli_num_rows($u) < 1) {
                                    ?>
                                    <option value="">No User Available</option>
                                    <?php
                                } else {
                                    while ($urow = mysqli_fetch_array($u)) {
                                        ?>
                                        <option value="<?php echo $urow['userid']; ?>"><?php echo $urow['uname']; ?></option>	
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                </div> 
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-check"></span> Add</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!-- send_attachment model-->
<div class="modal fade" id="send_attachment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close_send_attachment_model">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Send Attachments</h4></center>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <form action="" method="post" id="attachment" enctype="multipart/form-data">
                        <div class="upload-box">
							<h5>Select file to upload.</h5>
                            <div>
                                <input type="file" id="" name="file" accept="image/x-png,image/gif,image/jpeg">
                            </div>
                        </div>
                        <button class="upload-btn" name="send-file" onclick="checkFileAndSend(event)">Send</button>
                    </form>

                </div> 
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- send emoji -->
<div class="modal fade" id="send_emoji" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close_send_emoji_model">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Send emoji</h4></center>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <?php
					$emoji_list=array('😀','😁','😂','😃','😄','😅','😆','😉','😊','😋','😎','😍','😘','😗',
					'😙','😚','😇','😐','😑','😶','😏','😣','😥','😮','😯','😪','😫','😴','😌','😛','😜','😝',
					'😒','😓','😔','😕','😲','😷','😖','😞','😟','😤','😢','😭','😦','😧','😨','😬','😰','😱','😳',
					'😵','😡','😠','💪','👈','👉','☝','👆','👇','✌','✋','👌','👍','👎','✊','👊','👋','👏','👐','✍');
					$line_length=10;
                    echo "<table style='margin-left:auto;margin-right:auto;width:50%;'>";
                    for($i = 0; $i < count($emoji_list)/$line_length; $i++){
                        echo "<tr>";
                        for($j = 0; $j < $line_length; $j++){
							echo '<td><a class="emoji_item" href="#">'.$emoji_list[$i*$line_length+$j].'</a></td>';
                        }
                        echo "</tr>";
                    }
                    echo "</table>";
                    ?>
                </div> 
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!-- Send link model   -->
<div class="modal fade" id="send_link" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close_send_link_model">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Send Link</h4></center>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <form action="" method="post">
                        <div class="input-box">
                            <div>
                                <input type="text" id="link" name="link" placeholder="write your link here.">
                            </div>
                        </div>
                        <button class="input-btn" name="send-file" onclick="sendLink(event)">Send</button>
                    </form>

                </div> 
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Create Payment model   -->
<div class="modal fade" id="create_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        
        <div class="modal-content mx-auto">
            
            <form method="POST" action="payment.php" id="payment-form" class="moddl-form" onsubmit="paymentPageJquery(event);">
            <input type="submit" name="create_payment_page" value="Create a payment page"></br>
			<label>
			<input type="radio" name="percent" value="100" checked >100%</br>
            <input type="radio" name="percent" value="80">80%
			</label>
			<p style="font-weight:700">Total Amount :<input type="text" name="total-value" value="<?php echo @$_SESSION['agent_charges'];?>" required/></p>
			</form>
            <div class="text-center" id="payment-error"></div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!-- FAQ model   -->
<div class="modal fade" id="open_faq" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="">
    <div class="modal-dialog">
        <div class="modal-content mx-auto">

         <div class="form-group has-feedback">
    <label class="control-label" for="inputSuccess2"></label>
    <input type="text" class="form-control" id="inputSuccess2" name="search"/>
   
    <span  class="glyphicon glyphicon-search form-control-feedback" ></span>
    <div id="faqsearchcontent">
    </div>
    
</div>


       </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal -->






<!-- Send link model   -->
<div class="modal fade" id="open_options" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="">
    <div class="modal-dialog">
        <div class="modal-content mx-auto">
              <a target="_blank"  class="btn btn-secondary btn-lg btn-block"  href="#" style=" border-bottom:none">List of my booking codes</a>
              <a target="_blank"  class="btn btn-secondary btn-lg btn-block"  href="http://travpart.com/English/travcust" style=" border-bottom:none">Create a customize journey</a>

		<?php  if(isset($_SESSION['access']) && $_SESSION['access'] == 1):   ?>
            <a href="#create_payment" data-toggle="modal" title="custom charges" class="btn btn-secondary btn-lg btn-block" style=" border-bottom:none">Create a payment page</a> 
			<?php   endif;   ?>
            
            <a href="#open_faq" data-toggle="modal" title="faq" class="btn btn-secondary btn-lg btn-block"   style=" border-bottom:none">Check FAQ</a>
            <a target="_blank" class="btn btn-secondary btn-lg btn-block"  href="" >Check the list of tour page</a>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal -->


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php /*  <link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script> */?>
  


<script>

jQuery(document).ready(function(){
 $( function() {
    $( "#accordion" ).accordion();
  } );


  jQuery('#inputSuccess2').click(function(){
    $('#faqdata').empty();
    $('#faqsearchcontent').empty();

//console.log($('#inputSuccess2').val());

  if(($('#inputSuccess2').val().length)>1)
    {
         
  
	$.ajax('/English/travchat/faq/faq2.php', {
                        method:'post',
                        data: { search:$('#inputSuccess2').val() }
                             
		}).done(function(data) {
                        console.log(data);
                                               
                          $('#faqsearchcontent').append(data);
                                                 
                        
                        });
       

   }
   else
   {
   alert("please enter atleast two words");
   }



});



});


</script>


