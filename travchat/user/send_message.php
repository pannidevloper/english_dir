<?php
include('../conn.php');
require_once($_SERVER['DOCUMENT_ROOT']."/English/wp-load.php");
session_start();

global $wpdb;

$agent=filter_input(INPUT_POST, 'agent', FILTER_SANITIZE_NUMBER_INT);
if(empty($_COOKIE['to']) && empty($agent)) {
	echo json_encode(array('status'=>2, 'error'=>'no user selected'));
}
else if (!empty($_POST['msg'])) {
    $to = intval($_COOKIE['to']);
    $msg = $_POST['msg'];
	
	if($agent>1)
		$to=$agent;
	
	if(!empty($_POST['InitMsg'])) {
		$initMsg=filter_input(INPUT_POST, 'InitMsg', FILTER_SANITIZE_STRING);
		$initMsgId=filter_input(INPUT_POST, 'InitMsgId', FILTER_SANITIZE_NUMBER_INT);
		if($wpdb->get_var("SELECT COUNT(*) FROM `chat` WHERE (c_from='{$_SESSION['id']}' AND c_to='{$to}') OR (c_from='{$to}' AND c_to='{$_SESSION['id']}')")==0) {
			$wpdb->query("INSERT INTO `chat` (message, userid, chat_date,c_to,c_from,type,attachment_type) VALUES ('{$initMsg}' , '{$to}', NOW(),'{$_SESSION['id']}','{$to}','','')");
			if($initMsgId>0) {
				$wpdb->query("UPDATE `wp_chat_messages` SET reply_count=reply_count+1 WHERE id={$initMsgId}");
			}
		}
	}

	if($to>1) {
		$sql ="insert into `chat` (message, userid, chat_date,c_to,c_from,type,attachment_type) values ('$msg' , '" . $_SESSION['id'] . "', NOW(),'$to','" . $_SESSION['id'] . "','','')";
		mysqli_query($conn,$sql);
		echo json_encode(array('status'=>1));
	}
}
?>