<style>

.topnav {
  overflow: hidden;
  background-color: #368A8C;
}

.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
  text-align:right!important;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.active {
  background-color: #5bbc2e!important;
  color: white;
}

.topnav .icon {
  display: none;
}
.navbar-default {
    background-color: white !important;
    border-color: white !important;
    border-bottom: solid 7px #1A6D84 !important;
}

@media screen and (max-width: 600px) {
  .topnav a:not(:first-child) {display: none;}
  .topnav a.icon {
    float: right;
    display: block;
  }
}

@media screen and (max-width: 600px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
  .mobilenone{display:none;}
  //.mobileonly{display:block!important;}
}
</style>

<style>
.panel.panel-default.user_details_mob div.pull-right {
    padding-top: 10px;
}
.input-group-prepend button{
    width: 100%;
}
.input-group-prepend{
    width: 10%;
    background: #fff;
    display: inline-block;
}.lighten-3 i {
    color: #1bba9a !important;
}
.navbar-nav{
    padding-left: 20px;
    padding-top: 4px;
}
.search_hea, .lighten-3 {
    border: 1px solid #1bba9a;
}.lighten-3 {
    border-right: 0px;
    background: none;
}
.py-1 {
    border: 0px !important;
    font-size: 11px !important;
    letter-spacing: 1px !important;
    padding-left: 0px !important;
    font-family: 'Heebo', sans-serif !important;
}
.search_hea{
    width: 84%;
    display: inline-block;
}

.input-group {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-align: stretch;
    -ms-flex-align: stretch;
    align-items: stretch;
    width: 100%;
}

.search_hea {
    border-left: 0px !important;
}
.input-group .form-control, .input-group-addon, .input-group-btn {
    display: table-cell;
}
.input-group .form-control {
    position: relative;
    z-index: 2;
    float: left;
    width: 100%;
    margin-bottom: 0;
}


span.notify {
    position: relative;
    padding: 2px 5px !important;
    color: #fff;
    font-size: 10px;
    border-radius: 50%;
    top: -12px;
    left: -8px;
}
.mobile_header_balouch_show{
display: none;
}
.for_logo_mobile{
display: inline-block;
width: 60%;
}
.custom_image_mobile{
width: 85%;
}
.for_menu_mobile{
width: 35%; 
padding: 15px;
display: inline-block;
}

.English-logo img{
    width: auto;height: 50px;
}
.header_container_fluid{
    padding-left: 15px;
    padding-right: 15px;
    padding-top: 10px;
    padding-bottom: 10px;
}
.navbar_header_logo_sec{
    padding-left: 0px;
    padding-right: 0px;
}
.navbar_header_logo img{
    width: 150px;
}
.desktop_menu {
    margin-left: 13px;
}
.marg_right {
    margin-right: 15px !important;
}
.notify {
    position: absolute;
    background: red;
    color: #fff;
    border: 2px solid yellow;
    font-size: 10px;
    border-radius: 50%;
    top: -12px;
    height: 22px;
    width: 22px;
    right: -12px;
    text-align: center;
    line-height: 20px;
}
li.mega-menu-item.mega-menu-item-type-post_type.mega-menu-item-object-page.mega-align-bottom-left.mega-menu-flyout {
    margin: 0 0px 0 0;
    display: inline-block;
    height: auto;
    vertical-align: middle;
}
a.mega-menu-link {
    text-decoration: none;
}
a.mega-menu-link.loginstyle.headerfont{
    cursor: pointer;
    display: inline;
    letter-spacing: 1px;
}
a.mega-menu-link.loginstyle.headerfont:hover {
    text-shadow: 3px 3px 5px #333;
    border-bottom: 2px solid #5bbc2e!important;
}
.col-mmd-4 a {
    padding: 20px 0px !important;
    text-align: inherit;
}
select.header_curreny.form-control.search_select{
    display: block;
    width: 100%;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.search_select {
    position: relative;
    top: -15px;
    background: none;
}
select.form-control:not([size]):not([multiple]) {
    height: calc(2.25rem + 2px);
}
@media only screen and (max-width: 1024px){
        .mobilenone{
                display: none;
               }     
    .mobile_header_balouch_show{
        display: block !important;
    }
    .mobile_header_balouch_hide{
        display: none !important;
    }
}
.min_height{
    height: 105px;
    overflow: scroll;
}
.mega-menu-link a { 
    color: #000;
    text-decoration: none;
}
span.badge{
    padding:.25em .4em !important;
    background:red !important;
}

#content{
    margin-top: 0px !important;
}
.row_c{
    border-bottom:1px solid #ccc;
}
.notification_div{
    background: #fff;
}
.for_border{
    border-left: 4px solid #22b14c;
    border-right: 4px solid #22b14c;
    border-bottom: 4px solid #22b14c;
}
.profile_s{
    background:#22b14c;
    padding-bottom: 0px !important;
}

.dropdown-item {
    display: block;
    width: 100%;
    padding: .25rem 1.5rem;
    clear: both;
    font-weight: 400;
    color: #212529;
    text-align: inherit;
    white-space: nowrap;
    background-color: transparent;
    border: 0;
}
.input-group-prepend{
    background: #fff;
}
.home_mytime_msg{
    background: #368a8c;
}
.col-mmd-4 img{ 
    border-radius: 100%;
    border: 1px solid #fff;
}
p{
    font-family: 'Heebo', sans-serif;
}
.col-mmd-4 a,.col-mmd-3 a,.col-mmd-6 a{
    color: #fff;
    font-family: 'Heebo', sans-serif;
    font-size: 12px;
    text-transform: uppercase;
}
.space_top{
    margin-top: 10px;
}
.col-mmd-4{
    text-align: center;
    width: 32%;
    display: inline-block;
}
.avatar-25{
    border-radius: 50%;
    border:1px solid #ccc;
    padding: 1px;
}
.col-mmd-3{
    text-align: center;
    width: 18%;
    display: inline-block;
}
.col-mmd-6{
    text-align: center;
    width: 60%;
    display: inline-block;
}
.remove_pad{
    padding: 0px !important;
}
.custom_menu_btn_mobile i{
    color: #1a6d84;position: absolute;font-size: 22px;margin-left: 5px;
}
    .custom_menu_btn_mobile{
        padding: 0px 15px !important;
        background: none !important;
        border:0px !important;
        outline: 0px !important;
        position: relative;
        box-shadow: none !important;
        font-size: 16px !important;
        color: #1a6d84; 
    }               
        @media only screen and (max-width: 1024px){
            .custom_remove_pd{
                padding: 0px !important;
                max-width: 100% !important;
            }
            .search_hea{
                margin: 0px;
            }
            .col-mmd-4 a:visited {
                color: #fff !important;
            }
            .mobile_header_balouch_show{
                display: block !important;
            }
            .mobile_header_balouch_hide{
                display: none !important;
            }
            .mobilenone{
                display: none;
               }
        }
        
        
        
        
        @media only screen and (max-width: 900px) and (min-width: 768px){
            .custom_menu_btn_mobile{
                font-size: 22px !important;
            }
            .custom_menu_btn_mobile i{
                font-size: 30px;
            }
            .for_menu_mobile{
                padding: 44px !important;
            }
            .mobilenone{
                display: none;
               }
        }
        
        @media only screen and (max-width: 1024px) and (min-width: 900px){
            .custom_menu_btn_mobile{
                font-size: 40px !important;
            }
            .custom_menu_btn_mobile i{
                font-size: 53px;
            }
            .for_menu_mobile{
                padding: 53px !important;
            }.mobilenone{
                display: none;
               } 
        }
</style>

 <script>
        jQuery(document).ready(function($) {
            $('.dropdown-toggle').dropdown('toggle');

            $('#myChatRoom').DataTable({
                "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
                "bLengthChange": false,
                "bInfo": false,
                "bPaginate": true,
                "bFilter": false,
                "bSort": false,
                "pageLength": 8
            });

/*
$('#userList').DataTable({
                "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
                "bLengthChange": false,
                "bInfo": false,
                "bPaginate": true,
                "bFilter": true,
                "bSort": false,
                "pageLength": 8
            });
*/

            displayChat();

            $(document).on('click', '#send_msg', function () {
                   var access = '<?php echo $_SESSION['access'];   ?>';
                  /* if($('#chat_area').html() == '' && access==1){
                       alert('You cant initiate chat.');
                       return false;
                   } */
                   
                   
                if ($('#chat_msg').val() == "") {
                   // alert('Please write message first');
                } else {
                    var msgg = $('#chat_msg').val();
                    
                    $.ajax({
                        type: "POST",
                        url: "send_message.php",
                        data: {
                            msg:msgg
                        },
                        dataType: "json",
                        success: function(response){
                            $('#chat_msg').val("");
                            if(!!response && response.status==2) {
                                alert('Please select a travel advisor to chat!');
                            }
                            else {
                                displayChat();
                            }
                        }
                    });
                   
                }
            });
            
            $(document).on('click', '.delete-message', function () {
                var chatid=$(this).attr('chatid');
                var delete_message_button=$(this);
                $.ajax({
                  type: "GET",
                  url: "delete_message.php?chatid="+chatid,
                  success: function (data) {
                      console.log(data);
                      if(data==1) {
                          delete_message_button.parent().next().find('.message-content').html('<span class="deleted-message">Deleted</span>');
                          delete_message_button.remove();
                      }
                  }
                });
            });
            
            $(document).on('click', '.confirm_request', function () {
                $(this).parent().find('button').attr('disabled','disabled');
                $.ajax({
                  type: "POST",
                  url: "/English/travchat/user/verify_tour_request.php",
                  data: {
                      tourid: $(this).attr('tourid'),
                      confirm: 1
                  },
                  dataType: "json",
                  success: function (data) {
                      displayChat();
                  }
                });
            });
            
            $(document).on('click', '.reject_request', function () {
                $(this).parent().find('button').attr('disabled','disabled');
                $.ajax({
                  type: "POST",
                  url: "/English/travchat/user/verify_tour_request.php",
                  data: {
                      tourid: $(this).attr('tourid'),
                      confirm: 2
                  },
                  dataType: "json",
                  success: function (data) {
                      displayChat();
                  }
                });
            });
            
            $(document).on('click', '.emoji_item', function (e) {
                e.preventDefault();
                $('#chat_msg').val($('#chat_msg').val()+$(this).text());
            });

            $(document).on('click', '#confirm_leave', function () {
                id = <?php echo $id; ?>;
                $('#leave_room').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $.ajax({
                    type: "POST",
                    url: "leaveroom.php",
                    data: {
                        id: id,
                        leave: 1,
                    },
                    success: function () {
                        window.location.href = 'index.php';
                    }
                });

            });

            $(document).on('click', '#confirm_delete', function () {
                id = <?php echo $id; ?>;
                $('#confirm_delete').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $.ajax({
                    type: "POST",
                    url: "deleteroom.php",
                    data: {
                        id: id,
                        del: 1,
                    },
                    success: function () {
                        window.location.href = 'index.php';
                    }
                });

            });

            $(document).keypress(function (e) {
                if (e.which == 13) {
                    $("#send_msg").click();
                }
            });

            $("#user_details").hover(function () {
                $('.showme').removeClass('hidden');
            }, function () {
                $('.showme').addClass('hidden');
            });

            //
            $(document).on('click', '.delete2', function () {
                var rid = $(this).val();
                $('#delete_room2').modal('show');
                $('.modal-footer #confirm_delete2').val(rid);
            });

            $(document).on('click', '#confirm_delete2', function () {
                var nrid = $(this).val();
                $('#delete_room2').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $.ajax({
                    url: "deleteroom.php",
                    method: "POST",
                    data: {
                        id: nrid,
                        del: 1,
                    },
                    success: function () {
                        window.location.href = 'index.php';
                    }
                });
            });

            $(document).on('click', '.leave2', function () {
                var rid = $(this).val();
                $('#leave_room2').modal('show');
                $('.modal-footer #confirm_leave2').val(rid);
            });

            $(document).on('click', '#confirm_leave2', function () {
                var nrid = $(this).val();
                $('#leave_room2').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $.ajax({
                    url: "leaveroom.php",
                    method: "POST",
                    data: {
                        id: nrid,
                        leave: 1,
                    },
                    success: function () {
                        window.location.href = 'index.php';
                    }
                });
            });
            
            $('#chat_area').on('click', '#guestEmailSave', function () {
                $(this).addClass('disabled');
                $.ajax({
                    url: "update_email.php",
                    method: "POST",
                    data: {
                        email: $('#guestEmail').val(),
                    },
                    success: function (status) {
                        if(status==0)
                            alert('Saved failed. Try again!');
                        else if(status==2)
                            alert('The email address has been used.');
                        else
                            alert('Saved!');
                        displayChat();
                    }
                });
            });
            
        });

        var interval = 30000;

        function displayChat() {
            $.ajax({
                url: 'fetch_chat.php',
                type: 'POST',
                async: false,
                data: {

                    fetch: 1
                },
                success: function (response) {
                    $('#chat_area').html(response);
                    $("#chat_area").scrollTop($("#chat_area")[0].scrollHeight);
                }
            });
        }

       setTimeout(displayChat, interval);


        function saveUserId(id,that,uname,country,region,name,timezone,photo, status) {
            $(that).find('.badge').text('');
            
            $('#time').html('');
            if(!!photo) {
                $('#user_details img').attr('src','../'+photo);
            }
            else {
                $('#user_details img').attr('src','../image/avatar.png');
            }
            if(country!='')
            {
                var now=new Date();
                var d=now.getHours()>12?((now.getHours()-12)+':'+now.getMinutes()+' PM'):(now.getHours()+':'+now.getMinutes()+' AM');
                $('#chating-with').html( uname + ("<span class='status-indicator " + (status ? 'online' : '') + "'></span>") + ("<span class='status-text'>" +( status ? 'Online' : 'Offline')+"</span>" ));
                $('#time').html(d);
                $('#place').html(region+" ,"+country);
            }
            else{}

            
             //$('#chating-with').html(uname);
          $('#chating-with').html("<a href=https://www.travpart.com/English/bio/?user=" +name+" class='chat_name_link'>"+uname+"</a>");
       if(!!photo) {
            $('#img1').attr('src','../'+photo);
                   }
           else {
            $('#img1').attr('src','../image/avatar.png');
            }
          $('#user_name').html(uname);
          $('#view_user').html("<a href=https://www.travpart.com/English/bio/?user=" +name+"  class='view-profile'>View Profile</a>");
            var row = $('#myChatRoom').find('tr');
            console.log(row);
            for (var i = 0; i < row.length; i++) {
                $(row[i]).removeClass('success');
            }

            $(that).addClass('success');
            document.cookie = "to=" + id;
            takingUserId = id;
            displayChat();
        }
//this function to select user when page loaded
        $(document).ready(function () {

            var user = '<?php echo(isset($_GET['user']) && !empty($_GET['user']) ? $_GET['user'] : ''); ?>';
            if (user != '') {
                $('#' + user).click();
            } else {
                var button = $('#myChatRoom').find('tr')[1];
                $(button).click();
                var timeZoneName = Intl.DateTimeFormat().resolvedOptions().timeZone;
                document.cookie = "timeZoneName=" + timeZoneName;
            }
        });


        function fetchUser(value) {
            $.ajax({
                url: 'changeAddMember.php',
                type: 'POST',
                async: false,
                data: {
                    value: value,
                    fetch: 1
                },
                success: function (response) {
                    $("#user").html(response);
                }
            });
        }

        setInterval(calcTime, 1000);

        offset = '+5.5';
        function calcTime( ) {
            // create Date object for current location
            d = new Date();
            if (getCookie('takingUserId') != '') {
                offset = getCookie('takingUserId');
            }
            // convert to msec
            // add local time zone offset 
            // get UTC time in msec
            utc = d.getTime() + (d.getTimezoneOffset() * 60000);

            // create new Date object for different city
            // using supplied offset
            nd = new Date(utc + (3600000 * offset));

            var time = nd.toLocaleString();
            // return time as a string

            $('#time').html(time.split(',')[1]);
        }



        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function changeTimeZone(value) {
            document.cookie = takingUserId + "=" + value;
            $('#closeTimeZoneModel').click();
        }


        $(document).on('click', '#send_file', function () {

            $.ajax({
                type: "POST",
                url: "send-attachment.php",
                data: {
                    id: id,
                    leave: 1
                },
                success: function () {
                    window.location.href = 'index.php';
                }
            });

        });

        function checkFileAndSend(e) {
            e.preventDefault();
            var formData = new FormData($('#attachment')[0]);
            console.log(formData);
            $.ajax({
                url: 'send-attachment.php',
                type: 'POST',
                data: formData,
                processData: false, // tell jQuery not to process the data
                contentType: false, // tell jQuery not to set contentType
                success: function (data) {
                    console.log(data);
                    var response = JSON.parse(data);
                    if (response.success == 1) {
                        $('#close_send_attachment_model').click();
                        displayChat();
                    }
                }
            });
        }

        function downloadAttachment(address, type) {
            window.location.href = 'download-attachment.php?file=' + address + "&type=" + type;
//            $.ajax({
//                url: 'download-attachment.php?file='+address+"&type="+type,
//                type: 'GET',
//                processData: false, // tell jQuery not to process the data
//                success: function (data) {
//                    console.log(data);
//
//                }
//            });
        }

        function checkFileBeforeUpload() {
            var fileObj = document.getElementById('user_img_file').files[0];
            var type = fileObj.type;
            if (type.split('/')[0] == 'image') {
                $('#user_img_uld').attr('disabled', false);
            } else {
                $('#user_img_uld').attr('disabled', true);
            }

//            var ext = fileObj.name.split('.');
//            var extention = {'csv': 1};
        }
        
        //send link in chat
    function sendLink(e){
        e.preventDefault();
      
        var formData = {
                'link': $('#link').val() //for get email 
            };
        $.ajax({
                url: 'send_link.php',
                type: 'POST',
                data:formData,
                success: function (data) {
                    console.log(data);
                    var response = JSON.parse(data);
                    if (response.success == 1) {
                        $('#close_send_link_model').click();
                        displayChat();
                    }
                }
            });
    }
    
    function paymentPageJquery(e){
        e.preventDefault();
        var form = new FormData($('#payment-form')[0]);
        console.log(form);
        $.ajax({
           type: 'POST',
           url: 'payment.php',
           contentType: false,
           processData: false,
           data: form, 
            success: function(data){
              //console.log(data);
              var response = JSON.parse(data);
              if(response.success == 1){
                 var  $msg = response.data;
                    $.ajax({
                        type: "POST",
                        url: "send_message.php",
                        data: {
                            msg: $msg
                        },
                        success: function (response) {
                            displayChat();
                            $('#create_payment').modal('hide');
                           
                        }
                    });
              }else{
                  $("payment-error").html(response.data);
              }
              
               } 
        });
    }


    </script>   
    <script>
    $(document).ready(function(){
        $('#myChatRoom_filter').parent('div').removeClass('pull-right'); 
    });
    </script>
<script type="text/javascript">
jQuery(document).ready(function($){
    $('.hide_not,.hide_pro').hide();
    $('.lighten-3').click(function(){
        $('.custom_d').toggle();
    })
    $('.custom_menu_btn_mobile').click(function(){
        $(this).find('i').toggleClass('fa-bars fa-times-circle');
        $('.home_mytime_msg').slideToggle();
    })
    $('.not_slide').click(function(){
        $(this).find('i').toggleClass('fa-angle-down fa-angle-up');
        $('.hide_not').slideToggle();
    });
    $('.pro_slide').click(function(){
        $(this).find('i').toggleClass('fa-angle-down fa-angle-up');
        $('.hide_pro').slideToggle();
    });
    
    $('.click_main_search').click(function(){
        var color_c = $(this);
        $('.search_hea').attr('action',color_c.data('url'));
        $('.py-1').attr('name',color_c.data('name'));
        $('.click_main_search').removeClass('for_b_left');
        $('.py-1').attr('placeholder',color_c.data('text'));
        color_c.addClass('for_b_left');
    });
    
});
</script>

<script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>
<div class="mobile_header_balouch_show">
  <div class="row" style="padding: 10px 0px 10px 10px;">
    <div class="for_logo_mobile text-center">
        <?php if (is_user_logged_in()) { ?>
            <a href="https://www.travpart.com/English/timeline/">
               <img class="custom_image_mobile" src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png" alt="travpart">
            </a>
        <?php } else { ?>
            <a href="https://www.travpart.com/">
               <img class="custom_image_mobile" src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png" alt="travpart">
            </a>
        <?php } ?>
    </div> 
    <div class="for_menu_mobile text-center">
        <button class="btn custom_menu_btn_mobile">
            MENU
            <i class="fas fa-bars"></i>
        </button>
    </div>  
  </div>
  
  
  <div class="home_mytime_msg" style="display: none;">
    
    <div class="rows" style="padding-top: 20px;">
        <div class="col-mmd-4">
          <a href="https://www.travpart.com/English/timeline/">
            <i class="fas fa-home" style="font-size: 30px;margin-left:9px"></i>
            <p>Feed</p>
          </a>  
        </div>
        <div class="col-mmd-4">
        <?php $current_user = wp_get_current_user(); ?>
          <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->ID;?>">    
            <i class="fas fa-clock" style="font-size: 30px;"></i>
            
            <p>My Timeline</p>
          </a>  
        </div>
        <div class="col-mmd-4">
          <a href="/English/travchat/">
            <i class="fas fa-envelope" style="font-size: 30px;margin-right:9px"></i>
            <p>Message</p>
          </a>  
        </div>
    </div>
    
    <div class="for_border">
        
        <?php
        $display_name = um_user('display_name');
        $current_user = wp_get_current_user();
        if (is_user_logged_in()) { ?>
        <div class="rows profile_s" style="padding: 5px;">
            <div class="col-mmd-4">
              <a href="#" class="custpm_pr">
                <?php echo um_get_avatar('', wp_get_current_user()->ID, 30); ?> 
              </a>  
            </div>
            <div class="col-mmd-4">
              <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>?profiletab=pages" style="font-size: 9px;line-height: 30px">
                <strong> 
                     <?php echo $display_name; ?>
                </strong>
              </a>  
            </div>
            <div class="col-mmd-4">
              <a class="pro_slide" style="color:#fff;font-weight: bold;font-size: 18px;margin-top: -10px;font-family: 'Heebo', sans-serif;padding: 5px 15px !important">
                V
              </a>
            </div> 
        </div>
        <?php } ?>
        
        <div class="hide_pro">
        <div class="rows "  style="padding-top: 10px;">
            <div class="col-mmd-4">
              <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>?profiletab=pages">
                <i class="fas fa-user-circle" style="font-size: 30px;"></i>
                <p>My Profile</p>
              </a>  
            </div>
            <div class="col-mmd-4">
              <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=referal">   
                <i class="fas fa-undo-alt" style="font-size: 30px;"></i>
                <p>My Refferal</p>
              </a>  
            </div>
            <div class="col-mmd-4">
              <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=settlement">
                <i class="fas fa-calendar-check" style="font-size: 30px;"></i>
                <p>Settlement</p>
              </a>  
            </div>
        </div>
    
    
    <div class="rows ">
        <div class="col-mmd-4">
          <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=transaction">
            <i class="fas fa-dollar"  style="font-size: 30px;"></i>
            <p>Transaction</p>
          </a>  
        </div>
        <div class="col-mmd-4">
          <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=verification">  
            <i class="fas fa-calendar-check" style="font-size: 30px;"></i>
            <p>Verification</p>
          </a>  
        </div>
        <div class="col-mmd-4">
          <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=pages2">
            <i class="fas fa-book-open" style="font-size: 30px;"></i>
            <p>Booking</p>
          </a>  
        </div>
    </div>
   </div>
  </div> 
  <div class="rows " style="padding: 10px 0px 0px 0px">
        <div class="col-mmd-4">
          <a href="/English/about-us/">
            <i class="fas fa-briefcase" style="font-size: 30px;"></i>
            <p>About</p>
          </a>  
        </div>
        <div class="col-mmd-4">
          <a href="https://www.travpart.com/Chinese">
            <i class="fas fa-language" style="font-size: 30px;"></i>
            <p>中文</p>
          </a>  
        </div>
        <div class="col-mmd-4" style="position: absolute;">
          <select class="form-control" style="    margin: 5px 0px;border-radius: 0px;min-height: 35px;">
            <option>USD</option>
            <option>SNG</option>
          </select>
        </div>
    </div>
  <?php
        //$display_name = um_user('display_name');
        //$current_user = wp_get_current_user();
        //if (is_user_logged_in()) { ?>
  
   <?php //} ?>
   
  <?php if (is_user_logged_in()) { ?>
    <a href="https://www.travpart.com/English/logout/?redirect_to=https://www.travpart.com/English/login/" class="btn btn-primary" style="font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;"><i class="fas fa-sign-out-alt"></i> Logout</a>
  <?php }else{ ?>
    <a href="/English/login/" class="btn btn-primary" style="font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;"><i class="fas fa-sign-out-alt"></i> Login</a>
    <?php } ?>
  </div>
  
</div>



<div class="topnav mobileonly" id="myTopnav">
 <a class="navbar-brand logo-contentimage" href="./index.php">
 <img src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png" />
 </a> 
  <a href="#" style="padding-bottom:20px"></a>
  <a href="https://www.travpart.com/English/about-us/">About Us</a>
  <a href="https://www.travpart.com/English/travchat">Message</a>
  <?php
    ob_start();
    wp_nav_menu(array('theme_location' => 'main-menu'));
    $main_menu = ob_get_contents();
    ob_end_clean();
    ?>
    <?php ob_start(); ?>

                <?php if (is_user_logged_in()) { ?>
                 
                    <a href="./logout.php" class="buttype">LOGOUT<i class="fa fa-caret-down" style="display: inline;"></i></a>
                <?php } else { ?>

                    <a href="/English/login/" class="buttype">LOGIN</a>
                    
                <?php } ?>
  <a href="http://travpart.com/English/travchat/user/myorders.php">My Order</a>
  <a href="https://www.travpart.com/English/travcust/">Create a tour package</a>
  <?php if(current_user_can('um_travel-advisor')) { ?>
  <a href="https://www.travpart.com/English/customers-request/">CUSTOMER REQUEST</a>
<?php } ?>
  <a href="https://www.travpart.com/English/tutorial/"><img src="https://www.travpart.com/English/wp-content/themes/bali/tutorial-downloadpdf.png"></a>
  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
</div>
    

<nav class="navbar navbar-default mobilenone">
    <div class="container-fluid header_container_fluid">
        <div class="container">
        <div class="col-md-12">
        <div class="row">
        <div class="col-md-2 navbar_header_logo_sec"> 
            <?php if (is_user_logged_in()) { ?>
                <a class="navbar_header_logo" href="https://www.travpart.com/English/timeline/">
                    <img src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png" />
                </a>
            <?php } else { ?>
                <a class="navbar_header_logo" href="https://www.travpart.com/">
                    <img src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png" />
                </a>
            <?php } ?>
        </div>


        <ul class="col-md-4 nav navbar-nav">
            <div class="input-group md-form form-sm form-1 pl-0">
             <div class="input-group-prepend">
                <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="basic-text1" style="height: 36px;">
                    <i class="fas fa-search text-white" aria-hidden="true"></i>
                    <!--<span class="dropdown_title">People</span>-->
                </button>
                <div class="dropdown-menu" style="width: 100%;">
                  <a style="font-size:12px !important;margin-left:0px; " class="dropdown-item click_main_search for_b_left" data-name="friend" data-url="<?php bloginfo('home'); ?>/people" data-text="Search for People" href="#">People</a>
                  <a style="font-size:12px !important;margin-left:0px; " class="dropdown-item click_main_search" data-name="search" data-url="<?php bloginfo('home'); ?>/toursearch" data-text="Search for Tour Packages" href="#">Tour Packages</a>
                </div>
              </div>
              <form class="search_hea"  action="<?php bloginfo('home'); ?>/people" method="get" style="width: 85%;margin: 0px;">
                  <input class="form-control my-0 py-1" type="text" placeholder="Search for People" aria-label="Search" name="friend">
              </form>
            </div>
            <!-- <li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Lobby</a></li>-->
        </ul>
        <div class="below-logo">
            <!--  <div style="width:70%;">
        <ul class="list-unstyle header-logo">
       <li class="ttl-1-li"><a class="ttl-1" href="http://www.travpart.com/English/travcust" >Customize Travel</a>
        <a class="ttl-1-logo below-logo-fl" href="http://www.travpart.com/English"><img src="https://www.travpart.com/English/wp-content/uploads/2019/08/2019logo.png" /></a>
        </li></ul>
    </div> -->
        </div>
       
<div class="main-menu pull-right mobilenone">
    <?php
    ob_start();
    wp_nav_menu(array('theme_location' => 'main-menu'));
    $main_menu = ob_get_contents();
    ob_end_clean();
    ?>
    <?php ob_start(); ?>

    

    <!--<li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/about-us/">ABOUT</a></li>-->
    <!--<li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/travchat">MESSAGE</a></li>-->
    <!--<li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/contact/">CONTACT</a></li>-->
    <?php if($_SESSION['access']==1) { ?>
    <!--<li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont cqheader" href="https://www.travpart.com/English/customers-request/">CUSTOMER REQUEST</a></li>-->
    <?php } ?>
     <?php if (is_user_logged_in()) { ?>
        <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-link profileicon">
          <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->user_login; ?>">
            <?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?>
          </a>
        </li>
    <?php } ?>
    <div class="dropdown">
        <div class="dropbtn"> <!--  Login start --> 
                <?php
                $display_name = um_user('display_name');
                $current_user = wp_get_current_user();
                if (is_user_logged_in()) {
                    ?>
                 <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">

                    <a style="font-size: 16px;" class="mega-menu-link loginstyle headerfont" href="./logout.php" class="buttype">  
                      <?php echo $display_name; ?>
                    </a>
                <?php } else { ?>
                    <?php if( $_SESSION['user_name']=='Guest'){?>
                    <a class="mega-menu-link" href="guestLogout.php" class="
                    buttype">LOGOUT</a>
              <?php  }
              else
              {?>
                <a class="mega-menu-link" href="/English/login/" class="
                    buttype">LOGIN</a>

          <?php   } ?>

                    <style>
                        .dropdown-content{display:none!important;}
                    </style>
                <?php } ?>
            </li>   
            <!--  Login end --></div>
        <div class="dropdown-content">
            <div class="home_mytime_msg for_text">
                <div class="rows">
<<<<<<< HEAD
                          <div class="col-mmd-4">
                            <a href="https://www.travpart.com/English/timeline/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                              <i class="fas fa-home" style="font-size: 30px;"></i>
                              <p>Feed</p>
                            </a>
                          </div>
                          <div class="col-mmd-4">
                            <?php $current_user = wp_get_current_user(); ?>
                            <a style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; " href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->user_login; ?>">
                              <i class="fas fa-clock" style="font-size: 30px;"></i>

                              <p>My Timeline</p>
                            </a>
                          </div>
                          <div class="col-mmd-4">
                            <a href="/English/travchat/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                              <i class="fas fa-envelope" style="font-size: 30px;"></i>
                              <p>Message</p>
                            </a>
                          </div>
                        </div>
            
                 <div class="rows">
                          <div class="col-mmd-4">
                            <a href="https://www.travpart.com/English/events-list/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                              <i class="fas fa-calendar-alt" style="font-size: 30px;"></i>
                              <p>Event</p>
                            </a>
                          </div>
                          <div class="col-mmd-4">
                            <a href="https://www.travpart.com/English/videos/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                              <i class="fas fa-play" style="font-size: 30px;"></i>
                              <p>Videos</p>
                            </a>
                          </div>
                          <?php if (current_user_can('um_travel-advisor')) { ?>
                            <div class="col-mmd-4">
                              <a href="https://www.travpart.com/English/customers-request/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                                <i class="fas fa-headset" style="font-size: 30px;"></i>
                                <p>Customer</p>
                              </a>
                            </div>
                          <?php }else{ ?>
                            <div class="col-mmd-4">
                              <a href="https://www.travpart.com/English/TravPlan" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                                <i class="fas fa-map-marked-alt" style="font-size: 30px;"></i>
                                <p>Travplan</p>
                              </a>
                            </div>
                          <?php } ?>
                          
                           <?php if (current_user_can('um_travel-advisor')) { ?>
                            <div class="col-mmd-4">
                              <a href="https://www.travpart.com/English/travcust/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                                <i class="fas fa-edit" style="font-size: 30px;"></i>
                                <p>Travcust</p>
                              </a>
                            </div>
                            <div class="col-mmd-4 performance_icon">
                              <a href="" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                                <img src="https://www.travpart.com/English/wp-content/uploads/2020/06/performance_icon.png" style="border: none;width: 50px;height: 36px">
                                <p>Performance</p>
                              </a>
                            </div>
                          <?php } ?>
                          
                        </div>
                
=======
                    <div class="col-mmd-4">
                      <a href="https://www.travpart.com/English/timeline/"style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                        <i class="fas fa-home" style="font-size: 30px;"></i>
                        <p>Feed</p>
                      </a>  
                    </div>
                    <div class="col-mmd-4">
                    <?php $current_user = wp_get_current_user(); ?>
                      <a  style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; " href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->ID;?>">  
                        <i class="fas fa-clock" style="font-size: 30px;"></i>
                        
                        <p>My Timeline</p>
                      </a>  
                    </div>
                    <div class="col-mmd-4">
                      <a href="https://www.travpart.com/English/events-list/"style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                        <i class="fas fa-calendar-alt" style="font-size: 30px;"></i>
                        <p>Event</p>
                      </a>  
                    </div> 
                </div>
                
                
                <div class="rows">
                    
                    <?php 
                    global $user_login, $current_user;
                    get_currentuserinfo();
                    $user_info = get_userdata($current_user->ID);
                    $roles = array (
                    'administrator',
                    'um_travel-advisor',
                    );
                    if (is_user_logged_in() && array_intersect( $roles, $user_info->roles)) {
                    ?>
                    <div class="col-mmd-4">
                      <a href="https://www.travpart.com/English/about-us" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                        <img src="https://www.travpart.com/English/wp-content/themes/bali/images/travel_icon.png" style="width: auto;height:36px;border:0px; border-radius:0px !important;"/>
                        <p>Create service</p>
                      </a>  
                    </div>
                    <?php } ?>
                                        
                    
                    <div class="col-mmd-4">
                      <a href="https://www.travpart.com/English/about-us"style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                        <i class="fas fa-user" style="font-size: 30px;"></i>
                        <p>About us</p>
                      </a>  
                    </div>
                    <?php if(current_user_can('um_travel-advisor')) { ?>
                    <div class="col-mmd-4">
                      <a href="https://www.travpart.com/English/customers-request/"style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                        <i class="fas fa-headset" style="font-size: 30px;"></i>
                        <p>Customer</p>
                      </a>  
                    </div>
                    <?php } ?>
                </div>
                <div class="rows">
                    <?php if (current_user_can('um_travel-advisor')) { ?>
                        <div class="col-mmd-4">
                          <a href="https://www.travpart.com/English/travcust/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                            <i class="fas fa-edit" style="font-size: 30px;"></i>
                            <p>Travcust</p>
                          </a>
                        </div>
                        <div class="col-mmd-4 performance_icon">
                          <a href="./myorders.php" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                            <img src="https://www.travpart.com/English/wp-content/uploads/2020/06/performance_icon.png" style="border: none;width: 50px;height: 36px">
                            <p>Performance</p>
                          </a>
                        </div>
                    <?php } ?>
                </div>
>>>>>>> dc0843cc01520cbbc54a2c676d5dafb0bbcbeced
                <div class="for_border">
                    
                    <?php
                    $display_name = um_user('display_name');
                    $current_user = wp_get_current_user();
                    if (is_user_logged_in()) { ?>
                    <div class="rows profile_s">
                        <div class="col-mmd-4">
                          <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>" style="border: 0px !important;" class="custpm_pr">
                            <?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?>
                          </a>  
                        </div>
                        <div class="col-mmd-4">
                          <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>?profiletab=pages" style="font-size: 12px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize;padding:0px !important; ">
                            <strong> 
                                 <?php echo $display_name; ?>
                            </strong>
                          </a>  
                        </div>
                        <div class="col-mmd-4">
                          <a class="pro_slide" style="cursor: pointer;color:#fff;font-weight: bold;font-size: 18px;font-family: 'Heebo', sans-serif;padding: 5px 15px !important;border: 0px !important;color: #fff !important;">
                            V
                          </a>
                        </div> 
                    </div> 
                    <?php } ?>
                    
                    <div class="hide_pro" style="display: none;">
                    <div class="rows "  style="padding-top: 10px;">
                        <div class="col-mmd-4">
                          <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>?profiletab=pages">
                            <i class="fas fa-user-circle" style="font-size: 30px;"></i>
                            <p>My Profile</p>
                          </a>  
                        </div>
                        <div class="col-mmd-4">
                          <a style="color: #fff !important;border: 0px !important;" href="/English/my-connection/?user=<?php echo um_user('display_name'); ?>"> 
                            <i class="fas fa-undo-alt" style="font-size: 30px;"></i>
                            <p>My Refferal</p>
                          </a>  
                        </div>
                        <div class="col-mmd-4">
                          <a style="color: #fff !important;border: 0px !important;" href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=settlement">
                            <i class="fas fa-calendar-check" style="font-size: 30px;"></i>
                            <p>Settlement</p>
                          </a>  
                        </div>
                    </div>
                
                
                <div class="rows ">
                    <div class="col-mmd-4">
                      <a style="color: #fff !important;border: 0px !important;" href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=transaction">
                        <i class="fas fa-dollar-sign"  style="font-size: 30px;"></i>
                        <p>Transaction</p>
                      </a>  
                    </div>
                    <div class="col-mmd-4">
                      <a style="color: #fff !important;border: 0px !important;" href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=verification">   
                        <i class="fas fa-calendar-check" style="font-size: 30px;"></i>
                        <p>Verification</p>
                      </a>  
                    </div>
                    <div class="col-mmd-4">
                      <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/ticket/">    
                        <i class="fas fa-info-circle" style="font-size: 30px;"></i><br />
                        OPEN TICKETS
                      </a>  
                    </div>
                  
                </div>
                
                <div class="rows ">
                    <div class="col-mmd-4">
                      <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=pages2">
                        <i class="fas fa-book-open" style="font-size: 30px"></i><br />
                        BOOKING
                      </a>  
                    </div>
                  
                </div>
               </div>
              </div>
              
              <div class="rows " style="padding: 10px 0px 0px 0px">
                <!--<div class="col-mmd-4">
                  <a style="color: #fff !important;border: 0px !important;" href="/English/about-us/">
                    <i class="fas fa-briefcase" style="font-size: 30px;"></i>
                    <p>About</p>
                  </a>  
                </div>-->
                <div class="col-mmd-4">
                  <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/Chinese">
                    <i class="fas fa-language" style="font-size: 30px;"></i>
                    <p>中文</p>
                  </a>  
                </div>
                <div class="col-mmd-4">
                  <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/contact/">
                    <i class="fas fa-phone"  style="font-size: 30px;"></i> 
                    <br >
                    CONTACT
                  </a>  
                </div>
                <div class="col-mmd-4">
                  <select class="header_curreny form-control search_select">
                     <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
                     <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
                     <option full_name="USD" syml="USD" value="USD"> USD </option>
                     <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                  </select>
                </div>
            </div> 
             
              <div class="for_border">
                
               
               
              <?php if (is_user_logged_in()) { ?>
                <a  href="https://www.travpart.com/English/logout/?redirect_to=https://www.travpart.com/English/login/" class="btn btn-primary" style="color: #fff !important;border: 0px !important;font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;text-align: left !important;padding-left: 10px !important;"><i class="fas fa-sign-out-alt"></i> Logout</a>
              <?php }else{ ?>
                <a href="/English/login/" class="btn btn-primary" style="color: #fff !important;border: 0px !important;font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;text-align: left !important;padding-left: 10px !important;"><i class="fas fa-sign-out-alt"></i> Login</a>
                <?php } ?>
              </div>
              
            </div>
            <!--  User name end -->  
        </div>
    </div>  
    <li class="mega-menu-link">
        <a style="font-size: 16px;" class="desktop_menu" href="https://www.travpart.com/English/timeline">Feed</a>
     </li>
     <li class="mega-menu-link">
        <a style="font-size: 16px;" class="desktop_menu" href="http://travpart.com/english/people">Find People</a>
     </li>
     <li class="mega-menu-item marg_right mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
        <a  href="https://www.travpart.com/English/connection-request-list" class="custom_ficon desktop_menu"  style="color: #22b14c !important;font-size: 20px !important;position:relative;">
            
         <?php $countreq = getFriendRequestCount(wp_get_current_user()->ID); ?>
            <?php if ($countreq > 0) { ?>
                <i class="fas fa-user-friends" style="color: #1a2947;"></i>
         
                 <div class="notify">
                 <?php echo getFriendRequestCount(wp_get_current_user()->ID); ?>
                 </div>
            <?php } else{ ?>
                <i class="fas fa-user-friends" style="padding-left: 2px;padding-right: 2px"></i>
         <?php  } ?>
        </a>
     </li>
     
     <li class="mega-menu-item marg_right mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
        <a href="https://www.travpart.com/English/travchat/" class="custom_chaticon" style="color: #22b14c !important;font-size: 20px !important;">
         <?php $countmsg = getUnreadMessageCount(wp_get_current_user()->ID); ?>
            <?php if ($countmsg > 0) { ?>
                <i class="fas fa-comment-alt" style="color: #1a2947;"></i>
         
                 <span class="badge" style="position:relative;background: red;color: #fff;font-size: 10px;border-radius: 50%;top: -10px;left: -6px;">
                 <?php echo getUnreadMessageCount(wp_get_current_user()->ID); ?>
                 </span>
            <?php } else{ ?>
                <i class="fas fa-comment-alt" style="padding-left: 2px;padding-right: 2px"></i>
         <?php  } ?>
        </a>
     </li>
     
    <?php 
    if (is_user_logged_in() && (getNotificationcount(wp_get_current_user()->ID)>0)) { ?>
        <li class="mega-menu-item marg_right  mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout notificationicon mobilenone" style="position: relative;">
        <a href="#" class="custom_chaticon" style="color: #22b14c !important;font-size: 20px !important;position:relative">
            <i class="fas fa-bell" style="color: #1a2947;"></i>
            <div class="notify">
                <?php echo getNotificationcount(wp_get_current_user()->ID); ?>
            </div>
        </a>  
            <div class="notificationtooltips" style="display:none;">
                <div class="arrow-up"></div>
                <div id="nheading">
                    <div class="nheading-left">
                        <h6 class="nheading-title">Notifications<span>
                        </span></h6>
                    </div>
                    <div class="nheading-right">
                        <a class="notification-link" href="#">See all</a>
                    </div>
                </div>
                <ul class="notification-list">
                  
                    <?php
                    $notifications=getNotification(wp_get_current_user()->ID);
                    foreach($notifications as $row) {
                    ?>
                    <li class="notification-item" v-for="user of users">
                        <div class="user-content">
                            <p class="user-info" style="color: black"><?php echo $row['content']; ?></p>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </li>
        <?php } else { ?>
        <?php if(is_user_logged_in()) { ?>
            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout notificationicon mobilenone" style="position: relative;">
                <a class="desktop_menu" href="#" style="color: #22b14c !important;font-size: 20px !important;">
                    <!--<img src="https://www.travpart.com/wp-content/uploads/2019/10/notification_black.png" alt="notification" width="25">-->
                    <i class="fas fa-bell" style="padding-left: 2px;padding-right: 2px"></i>
                </a>
            </li>
        <?php } ?>  
    <?php } ?>   
    <!--<li class="mega-menu-link"><a href="./myorders.php"><span>MY ORDERS</span></a></li>-->
<?php 
global $user_login, $current_user;
get_currentuserinfo();
$user_info = get_userdata($current_user->ID);
$roles = array (
'administrator',
'um_travel-advisor',
);

if (is_user_logged_in() && array_intersect( $roles, $user_info->roles)) {
//echo ' <li class="mega-menu-link"><a href="https://www.travpart.com/English/travcust/"><span style="background: #006400 !important;padding:5px;border-radius:4px;color:#fff;">create tour package</span></a></li> ';
} else {
echo '';
}
?>
    <!--<li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link" href="https://www.travpart.com/English/tutorial/" target="_blank" class="mega-menu-link"><img src="https://www.travpart.com/English/wp-content/themes/bali/tutorial-downloadpdf.png" alt="Download Tutorial" style="width:107px;"></a></li>-->
    <?php
    $append_menu_items = ob_get_contents();
    ob_end_clean();

    $main_menu = substr(trim($main_menu), 0, strlen($main_menu) - 11);
    echo $main_menu . $append_menu_items . '</ul></div></div>';
    ?>
      <div class="dropdown mobileonly" style="display: none">
        <div class="dropbtn"> <!--  Login start --> 
                <?php if (is_user_logged_in()) { ?>
                 <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                    <a class="mega-menu-link" href="./logout.php" class="buttype">LOGOUT <i class="fa fa-caret-down" style="display: inline;"></i></a>
                <?php } else { ?>
                    <a class="mega-menu-link" href="/English/login/" class="buttype">LOGIN</a>
                    <style>
                        .dropdown-content{display:none!important;}
                    </style>
                <?php } ?>
            </li>   
            <!--  Login end --></div>
        <div class="dropdown-content">
            <!--  User name start -->
            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                <?php
                $display_name = um_user('display_name');
                $current_user = wp_get_current_user();
                if (is_user_logged_in()) {
                    ?>
                    <strong><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=pages" >
                            <?php echo $display_name; ?></a></strong><br>
                    <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=profile"><i class="fas fa-user-circle"></i> MY PROFILE
                    </a><br>
                    <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=referal"><i class="fas fa-undo-alt"></i> MY REFERRAL
                    </a><br>
                    <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=settlement"><i class="fas fa-calendar-check"></i> SETTLEMENT
                    </a><br>
                    <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=transaction"><i class="fas fa-dollar-sign"></i> TRANSACTION
                    </a><br>
                    <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=verification"><i class="fas fa-calendar-check"></i> VERIFICATION
                    </a><br>
                    <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=pages2"><i class="fas fa-book-open"></i> BOOKING 
                    </a><br>
                    <a class="mega-menu-link" href="https://www.travpart.com/English/ticket/" >
                    <i class="fas fa-book-open"></i> CONTACT
                    </a>
                <?php } else { ?>
                <?php } ?>
            </li>
            <!--  User name end -->  
        </div>
    </div>              
</div>

                    
        <!-- <ul class="nav navbar-nav navbar-right">
         

            <li><a class="ttl-1-myordr" href="./myorders.php">My Orders</a></li>
            <li class="ttl-1-li"><a class="ttl-1" href="http://www.travpart.com/English/travcust">Customize Travel</a>
                <a class="ttl-1-logo below-logo-fl" href="http://www.travpart.com/English/travcust"><img id="logo-imgview"
                        src="http://www.travpart.com/wp-content/uploads/2018/08/C_Users_ilham_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_5d530297-1236-49a1-8d98-13dadb59abc0.jpg" /></a>
            </li>
            <?php if(!empty(@$srow['photo'])){
            ?>
            <li><img src="../<?php echo $srow['photo']; ?>" width="42px;" style="margin: 3px;"></li>
            <?php }?>

            <li><a href="#account" data-toggle="modal"><span class="glyphicon glyphicon-lock"></span>
                    <?php echo @$user; ?></a></li>
            <?php if($_COOKIE['access'] == 0):   ?>
            <li><a href="guestLogout.php"><span class="glyphicon glyphicon-log-out"></span> Out</a></li>
            <?php  endif;  ?>
            <?php  if($_COOKIE['access'] != 0):   ?>
            <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#photo" data-toggle="modal"><span class="glyphicon glyphicon-picture"></span> Update Photo</a></li>
                    <li class="divider"></li>
                    <li><a href="#logout" data-toggle="modal"><span class="glyphicon glyphicon-log-out"></span> LOGOUT</a></li>
                </ul>
            </li>
            <?php endif;   ?>
        </ul> -->
    </div>
    </div>
    </div>
    </div>
</nav>
<script type="text/javascript">
    $(".ttl-1-logo").hover(function () {
        $("#logo-imgview").slideToggle(800);
    });

      //$("li.ttl-2-li").hover(function(){
          //  $(".ttl-2-logo").slideToggle(800);
      //  });
</script>
<style>

.click_main_search {
    outline: 0px;
}

.for_b_left {
    border-left: 3px solid #1bba9a !important;
}
.navbar-brand{
    height: auto !important;
}
    button.searchButton2 {
        height: 35px !important;
        margin: 98px 0 0px 20px !important;
    }

    .below-logo {
        display: flex;
        justify-content: center;
    }

    ul.list-unstyle.header-logo {
        list-style: none;
        padding: 0;
        margin: 0;
    }

    ul.list-unstyle.header-logo li {
        background-color: #f2f2f2;
        display: block;
        padding: 20px;
        width: 47%;
        text-align: center;
        float: left;
        margin: 10px;
    }

    .below-logo a {
        display: block;
        font-weight: 600;
    }

    .below-logo .ttl-1-logo,
    .below-logo .ttl-2-logo {
        display: none;
    }

    .below-logo img {
        width: 100%;

    }

    .ttl-1-logo img {
        position: absolute;
        width: 200px;
        left: 0px;
        top: 40px;
        z-index: 99999;
        display: none;
    }

    ul.list-unstyle.header-logo li {
        position: relative;
    }

    .divider-row {
        border-bottom: 1px solid #000;
    }

    #login_form {
        width: 350px;
        margin: 60px auto 0 auto;
    }

    .btn-facebook {
        background-color: #052940;
    }

    .btn-twitter {
        background-color: #1DA1F2;
    }

    .btn-google {
        background-color: #DB4437;
    }

    ul.list-unstyle.header-logo {

        float: right;
        display: inline-block;
    }

    ul.list-unstyle.header-logo li {
        width: auto;
    }

    a.ttl-1 {
        width: auto;
        display: inline-block;
        float: right;
    }

    a.ttl-1-logo.below-logo-fl {
        border: 1px solid white;
        padding: 20px;
        top: 2px;
        position: relative;
    }

    .logo-contentimage {
        display: inline-block;
        width: 280px;
        text-align: center;
        padding: 0px;
    }
a{
    text-decoration: none;
}
    .logo-contentimage img {
        max-width: 70%;
        height: auto;
        margin: auto;
        margin-top: 10px;
    }

    .dropbtn {
    background-color: transparent;
    color: white;
    border: none;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute!important;
    background-color: #3b898a!important;
    min-width: 330px !important;
    padding:5px 1px!important;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2)!important;
    z-index: 999;
}

.dropdown-content a {
    color: black;
    text-decoration: none;
    display: block;
}

.dropdown:hover .dropdown-content {display: block;}

.mobileonly,.mobilephoneonly{display:none}
.profileicon img{width:25px;height:25px;border-radius:50%;border:1px solid #ccc}
.mega-menu-item{display:inline-block;list-style: none}
.mega-menu-link{
    display:inline-block;
    z-index:9999!important;
    margin: 0 0px 0 0;
    height: auto;
    vertical-align: middle;
}
.mega-menu-link span{padding:4px;color:black;}
#mega-menu-wrap-main-menu #mega-menu-main-menu a.mega-menu-link{color:black!important;line-height:2.5em!important}

.navbar-right{margin-right:0!important;}
ul#mega-menu-main-menu {
    margin-bottom: 0px;
    padding-top: 3px;
}

#mega-toggle-block-1{display:none;}

.navbar-brand img {
    padding-top: 5px !important;
    padding-bottom: 0px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
}


@media only screen and (max-width: 600px) {
.panel-default{margin-top:30px;}
.modal-body{padding:0!important;}
.pull-right{display:none;}
}
</style>