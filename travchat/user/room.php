<?/* <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>*/ ?>

<style type="text/css">
table td{border:none;}
table td{border-bottom:1px solid #ccc;}
.answer{background-color: lightgray;padding: 5px 5px;}
.warning{width:100%;padding:20px;border-top:1px solid #ccc;vertical-align: middle;position: absolute;bottom:0px;}
.popuplist{background: #00cc00!important;color:white;font-size:14px;padding:15px;}
.popuplist a{color:white;text-align: center}
.modal-body{padding:0;}
.navbar-brand img{padding:20px;}
.profilethumbnail{display:inline-block;width:60px;height:60px;-webkit-border-radius: 50%;-moz-border-radius: 50%;border-radius: 50%;position: relative;background:#b0c3c3;}
.profilethumbnail .online{background:#00ff00;width:10px;height:10px;-webkit-border-radius: 50%;-moz-border-radius: 50%;border-radius: 50%;position: absolute;bottom:0;right:9%;}
.profiledetails{display: inline-block;vertical-align: top;margin-left:20px;margin-top:11px;}
.profiledetails .howmanymessages{display: inline-block;margin-left:5px;background:#00cc00;color:white;padding:2px 10px;text-align: center;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;max-width: 35px}
.profiledetails .location{font-weight: bold}
.profiledetails .time{color:#7a7a7a;}
.modal-dialog{width: 700px}

.destinationplan{padding:15px;border:1px solid #d2d2d2; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;background:#f9f9f9;color: #333;text-align: center}
.destinationplan:hover{border:1px solid #00cc00;color: #00cc00;background:#eff9f9;cursor: pointer;vertical-align: middle}
.marginlefttw{margin-left:20px;}
c-prfl-name p{width: auto!important}
.profiledetails {float: right;width: 86%; margin-top: 2px;}
@media only screen and (max-width: 600px){
.modal-dialog{margin:0%!important;width:96%!important;}
.pull-right{display:inline-block!important;}
//.profiledetails{margin-left:0px!important;margin-top:0px!important;}
.profiledetails{margin-left: 0px!important;float: right;margin-top: 6px!important;width: 77%;}
.ui-tooltip{display:none!important;}
.marginlefttw{margin-left:0px!important;}
.panel-default{height:auto!important;min-height:100px;margin-top:0!important;}
.profile-box{
        width: 100% !important;
}
}

#bookingCodeList span {
	display:block;
	margin: 9px;
}
a.chat_name_link {
    color: #333;
}
a.chat_name_link:hover{
    text-decoration: none;
}
a.chat_name_link span:hover {
    color: #333;
}
.panel.panel-default div.pull-right span a.btn-my {
    background-color: #22b14c !important;
    box-shadow: 0 0 0 5px hsl(0, 0%, 87%);
    margin: 1px;
    border-radius: 0px;
}
table#myChatRoom tbody {
    height: 430px !important;
    display: inline-block;
    overflow-y: scroll;
    width: inherit;
}
.profile-box{
        width: 370px;
}
.service_cus_chat_tab_mob{
    display: none;
}
.user_detail_br{
    display: none;
}
.c-prfl-name{
    text-align: left;
}
.chatroom_mob{
    display: none;
}

    @media only screen and (max-width: 600px){
        .service_cus_chat_tab_content{
            display: flex;
            border-bottom: 1px solid #3333;
            margin-left: -16px;
            margin-right: -16px;
            border-top: 1px solid #3333;
        }
        .service_cus_chat_tab_mob{
            display: block;
        }
        .history_tab{
            width: 30.33%;
            text-align: center;
            padding-top: 10px;
            padding-bottom: 10px;
            color: #22b14c;  
        }
        .service_cus_tab{
            width: 39.33%;
            text-align: center;
            padding-top: 10px;
            padding-bottom: 10px;
            color: black;
        }
        .chat_tab{
            width: 30.33%;
            text-align: center;
            padding-top: 10px;
            padding-bottom: 10px;
            color: black;
        }
        .history_tab span.history_tab_text{
            border-bottom: 2px solid #22b14c;
            padding: 5px;
            font-size: 15px;
        }
        .service_cus_tab span.service_cus_tab_text{
            padding: 5px;
            font-size: 15px;
        }
        .chat_tab span.chat_tab_text{
            padding: 5px;
            font-size: 15px;
        }
        .panel.panel-default div.pull-right span a.btn-my {
            display: none;
        }
        .col-lg-8.pad-tb-15.ser_cus_mob_none {
            display: none;
        }
        .panel.panel-default div#chat_area{
            height: 250px !important;
        }
        .trav_chat_msg.input-group input.form-control {
            width: 45% !important;
        }
        .trav_chat_msg.input-group span.input-group-btn .btn {
            padding-left: 10px;
            padding-right: 10px;
        }
        .user_detail_br{
            display: block !important;
        }
        .user_details_mob{
            margin-left: -30px;
            margin-right: -30px;
        }
        .panel.panel-default.user_details_mob span strong span#time {
            margin-left: 52px !important;
        }
        .panel.panel-default.user_details_mob span strong span#user_details img#img {
            top: 12px !important;
        }
        .col-lg-8.pad-tb-15.ser_cus_mob_none div.panel.panel-default.user_details_mob {
            min-height: 70px;
        }
        .panel.panel-default.user_details_mob span.user_details_pos{
            top: 0px !important;

        }
        .trav_chat_msg.input-group span.input-group-btn button.btn.btn-success.btn-my {
            background-color: #22b14c !important;
        }
        .my-ch-container {
            background-color: lightgray;
        }
        .chatroom_mob{
            display: block;
        }
        .mobile-memmber-list div.col-lg-4.bg-grey.pad-tb-15.chatroom_desk{
            display: none;
        }
    }

</style>
<script type="text/javascript">
    $(document).ready(function(){
        $(".service_cus_chat_tab_content div.history_tab").click(function(){
            $(".col-lg-8.pad-tb-15.ser_cus_mob_none").hide();
            $(".mobile-memmber-list div.col-lg-4.bg-grey.pad-tb-15.chatroom_mob").show();
            $(".mobile-memmber-list div.col-lg-4.bg-grey.pad-tb-15.chatroom_desk").hide();
            $(".history_tab span.history_tab_text").css({"color":"#22b14c","border-bottom": "2px solid #22b14c"});
            $(".chat_tab span.chat_tab_text").css({"color":"black","border-bottom":"none"});
            $(".service_cus_tab span.service_cus_tab_text").css({"color":"black","border-bottom":"none"});
        });
        $(".service_cus_chat_tab_content div.chat_tab").click(function(){
            $(".col-lg-8.pad-tb-15.ser_cus_mob_none").show();
            $(".mobile-memmber-list div.col-lg-4.bg-grey.pad-tb-15").hide();
            $(".chat_tab span.chat_tab_text").css({"color":"#22b14c","border-bottom": "2px solid #22b14c"});
            $(".history_tab span.history_tab_text").css({"color":"black","border-bottom":"none"});
            $(".service_cus_tab span.service_cus_tab_text").css({"color":"black","border-bottom":"none"});
        });
        $(".service_cus_chat_tab_content div.service_cus_tab").click(function(){
            $(".col-lg-8.pad-tb-15.ser_cus_mob_none").hide();
            $(".mobile-memmber-list div.col-lg-4.bg-grey.pad-tb-15.chatroom_desk").show();
            $(".mobile-memmber-list div.col-lg-4.bg-grey.pad-tb-15.chatroom_mob").hide();
            $(".chat_tab span.chat_tab_text").css({"color":"black","border-bottom": "none"});
            $(".history_tab span.history_tab_text").css({"color":"black","border-bottom":"none"});
            $(".service_cus_tab span.service_cus_tab_text").css({"color":"#22b14c","border-bottom":"2px solid #22b14c"});
        });
    });
</script>
<div class="service_cus_chat_tab_mob">
    <div class="service_cus_chat_tab_content">
        <div class="history_tab">
            <span class="history_tab_text">History</span>
        </div>
        <div class="service_cus_tab">
            <span class="service_cus_tab_text"><?php echo ($_SESSION['access'] != 1)?'Service Providers':'Customer List'; ?></span>
        </div>
        <div class="chat_tab">
            <span class="chat_tab_text">Chat</span>
        </div>
    </div>
</div>
<div class="col-lg-8 pad-tb-15 ser_cus_mob_none">

<div class="panel panel-default user_details_mob" style="height:50px;">

<span class="user_details_pos" style="font-size:18px; margin-left:10px; position:relative; top:13px;"><strong>

<span  id="user_details">

<!-- <span class="glyphicon glyphicon-user"></span> -->
<img id="img" src="../image/avatar.png" style="height: 40px;width: 40px;position: relative;top:-9px;left:0px;border-radius: 50% 50%;"/>

<!--<span class="badge"><?php echo mysqli_num_rows($cmem); ?></span>-->

 
</span><span id="chating-with"> </span><br class="user_detail_br">

<span style="margin-left:10px;color:#CD853F" id="time"></span>
<span style="margin-left:10px;color:#CD853F" id="place"></span>


</strong>



</span>

<div class="showme hidden" style="position: absolute; left:-120px; top:20px;"></div>

            <div class="pull-right" style="margin-right:10px; margin-top:-4px;">

                <span data-toggle="modal" data-target="#myAgentlist">
                    <a data-toggle="tooltip" data-placement="left" title="Click here to speak to a travel advisor." class="btn btn-my"><?php echo ($_SESSION['access'] != 1)?'Service Providers':'Customer List'; ?></a>
                </span>
            </div>

    </div>

    <div>

        <div class="panel panel-default" style="height: 430px;position:relative">

            <div style="height:10px;"></div>

            <span style="margin-left:10px;font-weight:bold;color:#368A8C;">Welcome to Chatroom</span><br>

            <span style="font-size:10px; margin-left:15px;"><i>Note: Avoid using foul language and hate speech to avoid banning of account</i></span>

            <div style="height:10px;"></div>

            <div id="chat_area" style="margin-left:10px; height:366px; overflow-y:scroll;">

            <div class="warning"><img src="https://www.travpart.com/English/wp-content/uploads/2018/11/exclamation-mark.png" alt="warning" width="30"> <span style="margin-left:10px">Never accept or ask for direct payments. Doing so may get your account restricted.</span></div>

            </div>

        </div>

        <div class="modal fade" id="myAgentlist" tabindex="-1" role="dialog" aria-labelledby="myAgentlistLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
          <div style="margin-left:20px;display:inline-block">
            <h4 id="show_all_customer"><span class="glyphicon glyphicon-user"></span> <?php echo ($_SESSION['access']==1)?'Customer List':'Agent List'; ?></h4>
        </div>
        <input type="text" id="myInput" onkeyup="myFunction1()" placeholder="Search for names.." title="Type in a name">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin:9px"><span aria-hidden="true">Close</span></button>       
          </div>
          <div class="modal-body">
		   <?php if($_SESSION['access']==1) { ?>
           <table style="width: 100%">
            <tr>
                <!-- <td style="width: 33.3%" class="popuplist"><a href="#"><button class="listbutton"><strong><span class="glyphicon glyphicon-user"></span> <?php echo ($_SESSION['access'] == 2)?'Agent List':'Customer List'; ?></strong></button></a></td> -->
                <td style="width: 50%" class="popuplist"><a id="show_recent_customer" class="marginlefttw"><strong>Most Recent Customer</strong></a></td>
                <td class="popuplist" style="text-align: right"><a href="https://www.travpart.com/English/customers-request" target="_blank"><strong>Check all customer's request</strong></a></td>
            </tr>
		   </table>
		   <?php } ?>
        <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12" style="padding:0">
                <table width="100%" class="table table-hover" id="userList">
                    <tbody>
                        <?php
						if($_SESSION['access']==1){
						//Customer List
						$sql = "SELECT * FROM `user`
								LEFT JOIN `sessions` ON `sessions`.`id` = (SELECT `id` from `sessions` where `user_id` = `user`.`userid` ORDER BY `id` DESC LIMIT 1)
								WHERE userid not in (select userid from `chat_member` where chatroomid='{$_SESSION['id']}')
								AND access = 2 AND activated=1 ";
						
                        $query = mysqli_query($conn, $sql);
                        while ($row = mysqli_fetch_array($query)) {
                            $is_active = false;
							$is_recent=((time()-strtotime($row['last_activity']))<=24*60*60);
                            if( $row['last_activity']) {
                                $last_activity = new DateTime( $row['last_activity'] );
                                $now = new DateTime();
                                $is_active = $last_activity < $now && ( ($now->getTimestamp() - $last_activity->getTimestamp()) <= MAX_INACTIVITY_TIME );
                            }
							if($_SESSION['access']==1) {
								$cas_data=mysqli_fetch_array(mysqli_query($conn,"SELECT csa_id,csa_destination_plan FROM `contact_sales_agent` WHERE `csa_user_name`='{$row['username']}' AND DATE_SUB(CURDATE(), INTERVAL 1 MONTH)<=DATE(csa_created) ORDER BY `csa_id` DESC LIMIT 1"));
								$destination_plan='DESTINATION PLAN';
								$csa_id=$cas_data['csa_id'];
							}
							if(empty($destination_plan))
								$destination_plan='';
							?>
                            <tr <?php echo $is_recent?'class="is_recent"':''; ?>>
                                <td style="padding-left:30px">
									<a href="addnewmember.php?user=<?php echo $row['userid'] ?>">
                                    <div class="profilethumbnail">
                                        <div class="<?php echo $is_active ? 'online': 'offline' ; ?>"></div>
                                    </div>
                                    <div class="profiledetails">
                                    <?php echo htmlspecialchars((empty($row['uname'])?$row['username']:$row['uname'])); ?><br>
                                    <span class="location"><i class="fas fa-map-marker-alt"></i> <?php echo (empty($row['region'])||empty($row['country']))?'':($row['region'].', '.$row['country']); ?></span> - <span class="time"><?php echo empty($row['last_activity'])?' ':date('m/d G:ia e',strtotime($row['last_activity']));?></span>
                                    </div>
									</a>
                                </td>
								<?php if(empty($csa_id)) { ?>
                                <td style="vertical-align: middle;text-align: center">
                                </td>
								<?php } else { ?>
								<td style="vertical-align: middle;text-align: center">
                                    <a href="/English/customers-request-details/?csa_id=<?php echo $csa_id; ?>" target=_blank class="destinationplan"><?php echo $destination_plan; ?></a>
                                </td>
								<?php } ?>
                            </tr>
                            <?php
                        }
						}
						else {
						//Agent List
                        /*$sql = "SELECT * FROM `user`
								LEFT JOIN `sessions` ON `sessions`.`id` = (SELECT `id` from `sessions` where `user_id` = `user`.`userid` ORDER BY `id` DESC LIMIT 1)
								WHERE userid not in (select userid from `chat_member` where chatroomid='{$_SESSION['id']}')
									AND access = 1 AND activated=1 ";*/
						$sql="SELECT
								(SELECT user.userid FROM wp_users,user WHERE access = '1' AND activated=1 AND wp_users.user_login=user.username AND wp_users.ID=meta_value) AS userid,
								MAX(score)
							FROM `wp_tour`,`wp_tourmeta`
							WHERE
								meta_key='userid' AND wp_tour.id=wp_tourmeta.tour_id
								AND wp_tour.id IN(
									SELECT meta_value FROM `wp_posts`,`wp_postmeta` WHERE `meta_key`='tour_id' AND wp_posts.ID=wp_postmeta.post_id AND wp_posts.post_status='publish'
								)
							GROUP BY meta_value
							ORDER BY `MAX(score)` DESC";
                        
                        $query = mysqli_query($conn, $sql);
                        while ($userRow = mysqli_fetch_assoc($query)) {
							$getUserDetailSQL="SELECT * FROM `user`
											LEFT JOIN `sessions` ON `sessions`.`id` = (SELECT `id` from `sessions` where `user_id` = `user`.`userid` ORDER BY `id` DESC LIMIT 1)
											WHERE userid not in (select userid from `chat_member` where chatroomid='{$_SESSION['id']}')
											AND userid='{$userRow['userid']}'";
							if(!empty($userRow['userid']) && ($row=mysqli_fetch_assoc(mysqli_query($conn, $getUserDetailSQL))) ) {
                            $is_active = false;
							$is_recent=((time()-strtotime($row['last_activity']))<=24*60*60);
                            if( $row['last_activity']) {
                                $last_activity = new DateTime( $row['last_activity'] );
                                $now = new DateTime();
                                $is_active = $last_activity < $now && ( ($now->getTimestamp() - $last_activity->getTimestamp()) <= MAX_INACTIVITY_TIME );
                            }
							?>
                            <tr <?php echo $is_recent?'class="is_recent"':''; ?>>
                                <td style="padding-left:30px">
									<a href="addnewmember.php?user=<?php echo $row['userid'] ?>">
                                    <div class="profilethumbnail"> <img id="agentlistimg" src="<?php 
                                        $ph = $row['photo']; 
                                        if($ph){
                                            echo "../" . $row['photo']; 
                                            }else{
                                                echo '../image/avatar.png';
                                            }
                                        
                                        ?>" alt="Avatar" width="60" hight="40">
                                        <div class="<?php echo $is_active ? 'online': 'offline' ; ?>"></div>
                                    </div>
                                    <div class="profiledetails">
                                    <?php echo htmlspecialchars((empty($row['uname'])?$row['username']:$row['uname'])); ?><br>
                                    <span class="location"><i class="fas fa-map-marker-alt"></i> <?php echo (empty($row['region'])||empty($row['country']))?'':($row['region'].', '.$row['country']); ?></span> - <span class="time"><?php echo empty($row['last_activity'])?' ':date('m/d G:ia e',strtotime($row['last_activity']));?></span>
                                    </div>
									</a>
                                </td>
								<td style="vertical-align: middle;text-align: center"></td>
                            </tr>
                            <?php
							}
                        }
						}
                        ?>
                    </tbody>
                </table>                     
            </div>
        </div>
    </div>
            
          </div>

        </div>
      </div>
    </div>

<?php /*

<div class="input-group">

<input type="text" class="form-control" placeholder="Type message..." id="chat_msg">

<span class="input-group-btn">
<a href="#open_options" data-toggle="modal" class="btn btn-my" title="more option"> <span class="glyphicon glyphicon-plus"></span></a>
<!-- <a href="#send_link" data-toggle="modal" class="btn btn-primary" title="send link"> <span class="glyphicon glyphicon-link"></span></a>-->
<a href="#send_attachment" data-toggle="modal" class="btn btn-my" title="send attachments"> <span class="glyphicon glyphicon-paperclip"></span></a>

<button class="btn btn-success btn-my" type="submit" id="send_msg" value="<?php echo $id; ?>">

<span class="glyphicon glyphicon-comment"></span> Send

</button>

</span>

</div>

*/ ?>
<?php
$bookingCodeListContent='';
if($_SESSION['access']==1) {
	$bookingCodeListConn=mysqli_query($conn, "SELECT wp_tour.id FROM wp_tour,wp_users,user,wp_tourmeta
		WHERE user.userid={$_SESSION['id']} AND wp_users.user_login=user.username
		  AND wp_tour.id=wp_tourmeta.`tour_id` AND wp_tourmeta.meta_key='userid' AND wp_tourmeta.meta_value=wp_users.id");
	while($row=mysqli_fetch_assoc($bookingCodeListConn)) {
		$bookingCodeListContent.='<span class="bookingCodeItem" url="https://www.travpart.com/English/tour-details/?bookingcode=BALI'.$row['id'].'" target=_blank>BALI'.$row['id'].'</span>';
	}
}

$filecontents=file_get_contents('faq_content.txt');
$newfilecontents=str_replace("'","\'",$filecontents);
?>
<script>
    var $j = jQuery;
    var faq_content_list = '<?php echo $newfilecontents; ?>';

    var faq_content = '<div id="faq-content" class="faq-content" style="height:300px; width:220px; overflow:auto; display:none;"> \
<a class="close closeop">×</a> \
<div class="faq_search"><input type="text" class="faq_search_input" placeholder="Search for frequently asked questions" /></div> \
<div class="faq_question"><ul> \
' + faq_content_list + '</ul></div>';


    var user_type = '<?php echo $_SESSION['access']; ?>';


    $j(function() {

        if (user_type == 1) {
            $j("[data-toggle='popover']").popover({
                html: 'true',
                content: '<div id="options" style="height:300px; width:220px;"> \
          <button id="bookingCodeList_button" class="btn btn-my">List of my booking codes</button><hr class="popover-hr"> \
          <a href="/English/travcust" target=_blank class="btn btn-my">Create a customized Journey</a><hr class="popover-hr"> \
          <button id="generate_paylink" class="btn btn-my">Create a payment link</button><hr class="popover-hr"> \
          <button id="faq_button" class="btn btn-my">Check FAQ</button><hr class="popover-hr"> \
		  <a href="/English/toursearch" target=_blank class="btn btn-my">Check the list of tour page</a><hr class="popover-hr"> </div> \
		  <div id="bookingCodeList" style="height:300px;width:220px;overflow: scroll;display:none;"> <h4>Booking Code</h4> <?php echo $bookingCodeListContent; ?></div>\
          <div id="generate_paylink_content" style="display:none;height:300px; width:220px;"><h4>Create a payment link</h4> Total tour cost:<br/>$<input id="total_price" type="text"/> <hr class="popover-hr"> <button class="btn btn-my total_pay" discount="1.0">100% payment</button> <hr class="popover-hr"> <button class="btn btn-my total_pay" discount="0.8">80% payment</button> </div> \
          <div id="search_price_box_form" style="height:300px; width:220px; display:none;" > \
    <form id="destinations_trip_types" target=_blank method="get" action="https://www.tourfrombali.com/tag/tour-page/"> \
    <p id="heading">Discover the perfect journey</p> \
    <input type="hidden" name="s" value=""> \
    <div class="search_price_box_field"> \
        <select id="destinations" name="destination"><option value="">All destinations</option><option value="Bali">Bali</option><option value="Sumatra">Sumatra</option><option value="Komodo Island">Komodo Island</option><option value="Remote Island">Remote Island</option><option value="East Java">East Java</option><option value="Central Java">Central Java</option><option value="West Java">West Java</option><option value="Lombok Island">Lombok Island</option><option value="Papua (Irian Jaya) Island">Papua (Irian Jaya) Island</option></select></div><div class="search_price_box_field"><select id="trip-type" name="trip-type"><option value="">Types</option><option value="Culture Exchange">Culture Exchange</option><option value="Photography">Photography</option><option value="Island Survival">Island Survival</option><option value="Hiking">Hiking</option><option value="Water Activities">Water Activities</option><option value="Animal">Animal</option><option value="Sky Activities">Sky Activities</option><option value="MICE">MICE</option><option value="Wedding">Wedding</option><option value="Spa">Spa</option><option value="Boat">Boat</option><option value="Rent Cars">Rent Cars</option><option value="Honeymoon">Honeymoon</option><option value="Culture Tours">Culture Tours</option><option value="1 Day Trip">1 Day Trip</option><option value="Temples">Temples</option></select></div><div class="search_price_box_field"><select id="days" name="days"><option value="">Days</option><option value="1 day">1 day</option><option value="2 days">2 days</option><option value="3 days">3 days</option><option value="4 days">4 days</option><option value="5 days">5 days</option><option value="6 days">6 days</option><option value="7 days">7 days</option></select> \
    </div> \
        <div class="search_price_box_field price-range"> \
            <input type="hidden" class="search-box-range-input" name="search-box-range" value="0,7538" /> \
        </div> \
    <input type="submit" value="Search" /> \
    </form></div>' + faq_content,
            });
        } else {
            $j("[data-toggle='popover']").popover({
                html: 'true',
                content: '<div id="options" style="height:300px; width:220px;">\
                <a class="close closeop">×</a> \
          <button id="faq_button" class="btn btn-my">Check FAQ</button><hr class="popover-hr"> \
		  <button id="get_game_point" class="btn btn-my">Get game points</button><hr class="popover-hr"> \
          <a href="/English/toursearch" target=_blank class="btn btn-my">Check the list of tour page</a><hr class="popover-hr"> </div> \
		  <div id="verify_tour_request_box" style="height: 300px; width: 220px; display:none;"><a class="close closeop">×</a><h4>Verify tour request</h4> <p>Verify if this was your tour package request to earn vouchers game points</p>BOOKING Code: <input id="verify_bookingcode" type="text"> <hr class="popover-hr"> <button id="send_verify_request" class="btn btn-my">Confirm</button> <p id="verify_error" style="color: red; font-size: 18px; margin: 12px 3px; display: none;"></p> </div> \
          <div id="search_price_box_form" style="height:300px; width:220px; display:none;" > \
    <form id="destinations_trip_types" target=_blank method="get" action="https://www.tourfrombali.com/tag/tour-page/"> \
    <p id="heading">Discover the perfect journey</p> \
    <input type="hidden" name="s" value=""> \
    <div class="search_price_box_field"> \
        <select id="destinations" name="destination"><option value="">All destinations</option><option value="Bali">Bali</option><option value="Sumatra">Sumatra</option><option value="Komodo Island">Komodo Island</option><option value="Remote Island">Remote Island</option><option value="East Java">East Java</option><option value="Central Java">Central Java</option><option value="West Java">West Java</option><option value="Lombok Island">Lombok Island</option><option value="Papua (Irian Jaya) Island">Papua (Irian Jaya) Island</option></select></div><div class="search_price_box_field"><select id="trip-type" name="trip-type"><option value="">Types</option><option value="Culture Exchange">Culture Exchange</option><option value="Photography">Photography</option><option value="Island Survival">Island Survival</option><option value="Hiking">Hiking</option><option value="Water Activities">Water Activities</option><option value="Animal">Animal</option><option value="Sky Activities">Sky Activities</option><option value="MICE">MICE</option><option value="Wedding">Wedding</option><option value="Spa">Spa</option><option value="Boat">Boat</option><option value="Rent Cars">Rent Cars</option><option value="Honeymoon">Honeymoon</option><option value="Culture Tours">Culture Tours</option><option value="1 Day Trip">1 Day Trip</option><option value="Temples">Temples</option></select></div><div class="search_price_box_field"><select id="days" name="days"><option value="">Days</option><option value="1 day">1 day</option><option value="2 days">2 days</option><option value="3 days">3 days</option><option value="4 days">4 days</option><option value="5 days">5 days</option><option value="6 days">6 days</option><option value="7 days">7 days</option></select> \
    </div> \
        <div class="search_price_box_field price-range"> \
            <input type="hidden" class="search-box-range-input" name="search-box-range" value="0,7538" /> \
        </div> \
    <input type="submit" value="Search" /> \
    </form></div>' + faq_content,
            });

        }

        $j("[data-toggle='popover']").on('shown.bs.popover', function() {
            $j('#bookingCodeList_button').click(function() {
                $j('#options').hide();
                $j('#open_search_box').hide();
                $j('#generate_paylink_content').hide();
                $j('#bookingCodeList').show();
            });
	    $j('a.close.closeop').click(function() {
               $("#options_btn").click();
            });
            $j('#generate_paylink').click(function() {
                $j('#options').hide();
                $j('#open_search_box').hide();
                $j('#bookingCodeList').hide();
                $j('#generate_paylink_content').show();
            });
            $j('#open_search_box').click(function() {
                $j('#options').hide();
                $j('#generate_paylink_content').hide();
                $j('#bookingCodeList').hide();
                $j('#search_price_box_form').show();
            });
            $j('#get_game_point').click(function() {
                $j('#options').hide();
                $j('#generate_paylink_content').hide();
                $j('#bookingCodeList').hide();
                $j('#search_price_box_form').hide();
                $j('#verify_tour_request_box').show();
            });

            $j('.bookingCodeItem').click(function() {
                $('#chat_msg').val('<a target="_blank" href="' + $(this).attr('url') + '">' + $(this).text() + '</a>');
                $('#options_btn').click();
                $('#chat_msg').focus();
                $('#send_msg').click();
            });

            $j('#send_mail').click(function() {
                $.ajax({
                    type: "POST",
                    url: '/English/travchat/user/send_message_mail.php',
                    data: 1,
                    success: function(data) {
                        console.log('mail sent');

                    }
                });
            });
			
			$j('#send_verify_request').click(function() {
				$(this).attr('disabled', 'disabled');
				$.ajax({
                    type: "POST",
                    url: '/English/travchat/user/verify_tour_request.php',
                    data: {
						bookingcode:$('#verify_bookingcode').val()
                    },
					dataType: 'json',
                    success: function(data) {
                        if(data.error!=0) {
							$('#verify_error').text(data.error_msg);
							$('#verify_error').show();
						}
						else {
							$('#options_btn').click();
						}
						$('#send_verify_request').removeAttr('disabled');
                    }
                });
			});


            $j('#faq_button').click(function() {

                $('#options').hide();
                //$('#open_search_box').hide();
                //$('#generate_paylink_content').hide();
                $('#faq-content').show();
            });

            jQuery(".search-box-range-input").jRange({
                from: 0,
                to: 7538,
                step: 1,
                format: "$%s",
                width: 150,
                showLabels: true,
                isRange: true,
                theme: "theme-blue",
                snap: true
            });


            $j('.faq_question .answer').css('display', 'none');
            $j('.faq_search>input').keyup(function(e) {
                last = e.timeStamp;
                setTimeout(function() {
                    if (last - e.timeStamp === 0) {
                        console.log($('.faq_search_input').val());
                        //var search_text=$('.faq_search_input').val().toLowerCase();
                        var search_text = $('.faq_search_input').val();

                        $j('.faq_question li').hide();
                        $j('.faq_question span.title').each(function() {
                            //if($j(this).text().indexOf(search_text)!=-1)
                            if ($j(this).text().toLowerCase().indexOf(search_text) >= 0) {
                                $j(this).parent().show();
                            } else if ($j(this).text().indexOf(search_text) != -1) {
                                $j(this).parent().show();

                            }

                        });
                    }
                }, 360);
            });
            $j('.faq_question .title').click(function() {
                if ($j(this).next().css('display') == 'none')
                    $j(this).next().css('display', 'block');
                else
                    $j(this).next().css('display', 'none');
            });




            $j('.total_pay').click(function() {
                if ($('#total_price').val() == '') {
                    $('#total_price').css('border-color', 'red');
                    $('#total_price').focus();
                } else {
                    $('#total_price').css('border-color', '');

                    var total_dollar = $('#total_price').val() * $(this).attr('discount');
                    var dataString = {
                        'hotels': '',
                        'tickets': '',
                        'number_of_adult': 1,
                        'number_of_child': 0,
                        'number_of_room': 0,
                        'number_of_extra_bed': 0,
                        'cs_addnightroom': 0,
                        'cs_addnightroomprice': '',
                        'post_id': 123,
                        'start_date': 0,
                        'end_date': 0,
                        'total': 0,
                        'total_dollar': total_dollar,
                        'hiddenTripDetails': 0,
                        'hiddenTaxAndServiceRP': 0
                    };
                    $.ajax({
                        type: "POST",
                        url: '/English/wp-content/themes/bali/addrequest.php',
                        data: dataString,
                        success: function(data) {
                            var trimdata = jQuery.trim(data);
                            //console.log(trimdata);
                            $('#chat_msg').val('Payment page link:<a target="_blank" href="http://www.travpart.com/English/confirm-payment/' + trimdata + '">http://www.travpart.com/English/confirm-payment/' + trimdata + '</a>');
                            $('#options_btn').click();
                            $('#chat_msg').focus();
                            //Add order
                            var orderdata = {
                                'tour_id': data,
                                'total_price': total_dollar
                            };
                            $.ajax({
                                type: "POST",
                                url: '/English/travchat/user/addorder.php',
                                data: orderdata,
                                success: function(data) {
                                    console.log("Add order success");
                                }
                            });
                        }
                    });
                }
            });
        });


    });


    jQuery(document).ready(function() {
        // executes when HTML-Document is loaded and DOM is ready
        console.log("document is ready");

        /*jQuery('.btn[href^=#]').click(function(e){
        e.preventDefault();
        var href = jQuery(this).attr('href');
        jQuery(href).modal('toggle');
        });*/
    });

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(function() {
        $('#show_all_customer').click(function() {
            $('#userList tr').show();
        });
        $('#show_recent_customer').click(function() {
            $('#userList tr').hide();
            $('.is_recent').show();
        });
    });
</script>

<style>
.popover-hr{
margin-top:11px;
margin-bottom:11px;
}
.profilethumbnail {
    width: auto;
    height: auto;
}
img#agentlistimg {
    height: 53px;
    border-radius: 50%;
}

a.close.closeop {
    opacity: inherit;
    color: red;
}

#myInput {
    background-image: url(https://www.travpart.com/English/travchat/image/search.png);
    background-position: 10px 12px;
    background-repeat: no-repeat;
    width: 50%;
    font-size: 16px;
    padding: 14px 20px 12px 41px;
    border: 1px solid #ddd;
    margin-bottom: 12px;
    background-size: 27px;
    margin-left: 46px;
}
.trav_chat_msg.input-group input.form-control {
    width: 79%;
}
</style>


<div class="trav_chat_msg input-group">

<input type="text" class="form-control" placeholder="Type message..." id="chat_msg">

<span class="input-group-btn">
<!--a href="#open_options" data-toggle="modal" class="btn btn-my" title="more option"> <span class="glyphicon glyphicon-plus"></span></a-->
<?php /*if($_SESSION['access'] == 1) { */ ?>
<!--Added the Popup For the + symbol on 12-02-2020 Starts Here-->
    <a id="options_btn" class="btn btn-my"
data-container="body" data-toggle="popover" title="Tour Details" data-placement="top"><span class="glyphicon glyphicon-plus"></span></a>
<!--Added the Popup For the + symbol on 12-02-2020 Ends Here-->
<?php /* } */?>
<!-- <a href="#send_link" data-toggle="modal" class="btn btn-primary" title="send link"><span class="glyphicon glyphicon-link"></span></a>-->
<a href="#send_emoji" data-toggle="modal" class="btn btn-my" title="Emoji" style="padding: 0px; border: 0px;">
<span style="font-size: 24px;">☺</span>
</a>
<a href="#send_attachment" data-toggle="modal" class="btn btn-my" title="send attachments"> <span class="glyphicon glyphicon-paperclip"></span></a>

<button class="btn btn-success btn-my" type="submit" id="send_msg" value="<?php echo $id; ?>">

<span class="glyphicon glyphicon-comment"></span> Send

</button>

</span>

</div>





</div>          

</div>
<script>
$(document).ready(function(){
/*$('.modal-dialog .modal-content a[href="#create_payment"]').click(function(){     
$(this).closest("#open_options").css("display" , "none");
$(".modal-backdrop.in").hide();
$("body").css("padding-right","0px");
})

$('.modal-dialog .modal-content a[href="#open_faq"]').click(function(){     
$(this).closest("#open_options").css("display" , "none");
$(".modal-backdrop.in").hide();
$("body").css("padding-right","0px");
})*/


});


</script>