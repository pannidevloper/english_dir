<!DOCTYPE html>
<html>
    <head>

        <title>Travchat</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://www.travpart.com/Chinese/wp-content/themes/bali/js/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="/English/wp-content/themes/bali/jquery-range/jquery.range.js"></script>
		<script src="../js/jquery.dataTables.min.js"></script>
		<script src="../js/dataTables.bootstrap.min.js"></script>
		<script src="../js/dataTables.responsive.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        
        <link href="../css/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/dataTables.responsive.css" rel="stylesheet">
        <link href="../css/style.css?v2" rel="stylesheet">
        <link href="/English/wp-content/themes/bali/jquery-range/jquery.range.css" rel="stylesheet">
        <link rel="icon" href="http://www.travpart.com/wp-content/themes/aberration-lite/images/travpart_icon_lnA_icon.ico" />
	<link rel='stylesheet' id='fontawesome-css'  href='//use.fontawesome.com/releases/v5.6.1/css/all.css' type='text/css' media='all' />
        <style>
        @media only screen and (max-width: 600px) {
.logo-contentimage{top:-15px!important;}
.modal-dialog{margin:50% auto!important;width:90%!important;display:block;transform:translate(-50%);}
}
</style>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '324809911559064');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=324809911559064&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/e34f1763-afe4-4f4e-9fc6-df81af96c04f.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQ4N2X6');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ4N2X6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-65718700-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-65718700-6');
</script>
<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/e34f1763-afe4-4f4e-9fc6-df81af96c04f.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>
<script>
    $(document).ready(function() {
      $('.autocompletion-block input[name="search"]').on('keyup', function(e) {
        var that = $(this);
        last = e.timeStamp;
        setTimeout(function() {
          var keyword = that.val();
          if (last - e.timeStamp === 0) {
            $.getJSON(
              '/English/travchat/api/?action=w_search_users', {
                keyword: keyword
              },
              function(data) {
                $('.autocompletion-block ul>li').remove();
                $.each(data.users, function(i, v) {
                  $('.autocompletion-block ul').append('<li><a href="addnewmember.php?user=' + v
                    .userid + '">' + v.name + '</a></li>');

                });
                $('.autocompletion-block .autocompletion-list').show();
                $('.autocompletion-block input[name="search"]').css('border-color', '');
              });
          }
        }, 1000);
      });
    });
</script>
    </head>