<?php
/**
 * Define our maximum inactivity time before considering our user offline
 * 
 * @var MAX_INACTIVITY_TIME 
 */
define( "MAX_INACTIVITY_TIME", 1800 );

/**
 * For every request we make, we update the user's last activity.
 * 
 * @param int user_id The user id
 * 
 * @return void
 */
function update_user_last_activity( $user_id ) {
    $conn = get_connection();

    $date = date('Y-m-d H:i:s', time());

    $sql = "
        UPDATE `sessions` SET last_activity = '{$date}' WHERE `sessions`.`user_id` = {$user_id};
    ";

    $result = mysqli_query( $conn, $sql );

    return $result;
}

/**
 * Gets the correct connection where the user was retrived
 * 
 * @return mixed|boolean
 */
function get_connection() {
    include ('../conn.php'); 

    /*if( !isset($_SESSION['conn'])) return false ;

    if( $_SESSION['conn'] !== 1 ) {
        return $conn2;
    }*/

    return $conn;
}

function get_user_status( $user ) {
    if( !isset( $user['last_activity']) && !$user['last_activity'] ) {
        return false;
    }

    $is_active = false;
    $last_activity = new DateTime( $user['last_activity'] );
    $now = new DateTime();
    $is_active = $last_activity < $now && ( ($now->getTimestamp() - $last_activity->getTimestamp()) <= MAX_INACTIVITY_TIME );

    return $is_active;
}

update_user_last_activity( $_SESSION['id'] );