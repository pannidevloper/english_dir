<?php
require_once($_SERVER['DOCUMENT_ROOT']."/English/wp-load.php");
include('session.php');
if ($_SESSION['access']==0 && empty($_GET)) {
    wp_redirect('./index.php?'.rand(1, 9));
}
include('header.php');
?>

<?php

///session_destroy();

    global $wpdb;
    global $current_user;
    
    if (is_user_logged_in()) {
        //$user = wp_get_current_user();
    //	echo "<pre>"; print_r($user); exit;
    }
?>

<?php
$id = $_SESSION['id'];
$chatq = mysqli_query($conn, "select * from `chatroom` where chatroomid='$id'");
$chatrow = mysqli_fetch_array($chatq);

$cmem = mysqli_query($conn, "select * from `chat_member` where chatroomid='$id'");

$charges = mysqli_query($conn, "select total_charges from `agent_charges` where agent_user_id='$id'");
$agent_charge = mysqli_fetch_assoc($charges);
$_SESSION['agent_charges'] = $agent_charge['total_charges'];


?>

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ4N2X6" height="0" width="0"
      style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <?php include('navbar.php'); ?>
  <div class="container-fluid">
    <div class="row">
      <!--<div class="col-lg-2">
			<div class="my-order-container">
                
			<div class="">
            <table width="100%" class="table  table-bordered table-hover" >
                <thead>
                <tr><th class="text-center my-ord"><a href="./myorders.php">My Orders</a></th>
                </tr></thead>
                <tbody></tbody>
            </table>
			</div>
			</div>
			</div>-->

      <div class="col-lg-12 my-ch-container">
        <div class="pc-memmber-list">
          <?php include('mychat_desk.php');  ?>
        </div>
        <?php include('room.php'); ?>
        <div class="mobile-memmber-list" style="display:none;">
          <?php include('mychat_mobile.php');  ?>
        </div>
      </div>
    </div>
  </div>
  <?php include('room_modal.php'); ?>
  <?php include('out_modal.php'); ?>
  <?php include('modal.php'); ?>

  <!--      user avatar popup -->
  <style>
    .trav-rating {
      display: inline-block;
      vertical-align: middle;
    }

    .trav-rating>span {
      border-radius: 2px;
      background-color: #fc8c14;
      font-size: 14px;
      line-height: 1;
      padding: 2px 4px;
      color: #fff;
      text-align: center;
    }

    .trav-rating>i {
      line-height: 21px;
      display: inline-block;
      color: #fc8c14;
      margin-left: 5px;
      margin-right: 5px;
    }

    .trav-rating>small {
      font-size: 14px;
      line-height: 21px;
      color: #666666;
    }

    .avatar-small {
      width: 50px;
      height: 50px;
      background-position: center center;
      background-size: cover;
      border-radius: 50%;
    }

    .avatar-popver {
      position: absolute;
      left: -50000rem;
      max-width: 230px;
      position: relative;
      padding-left: 18px;
    }

    .avatar-popver .arrow {
      position: absolute;
      top: 40px;
      left: 3px;
      display: block;
      width: 0;
      height: 0;
    }

    .avatar-popver .arrow:before {
      position: absolute;
      top: 0px;
      left: 0px;
      display: block;
      width: 0;
      height: 0;
      content: '';
      border-color: #dcdcdc;
      border-style: solid;
      border-width: 16px 16px 16px 0px;
      border-left-color: transparent;
      border-top-color: transparent;
      border-bottom-color: transparent;
    }

    .avatar-popver .arrow:after {
      position: absolute;
      top: 1px;
      left: 1px;
      display: block;
      width: 0;
      height: 0;
      content: '';
      border-color: #fff;
      border-style: solid;
      border-width: 15px 15px 15px 0px;
      border-left-color: transparent;
      border-top-color: transparent;
      border-bottom-color: transparent;
    }

    .avatar-popver .inner {
      border: 1px solid #dcdcdc;
      border-radius: 3px;
      box-shadow: 1.4px 3.7px 13px 0 rgba(0, 0, 0, 0.11);
      background-color: #ffffff;
    }

    .avatar-popver-top {
      padding: 10px;
      padding-left: 75px;
      position: relative;
      color: #333;
      font-size: 12px;
    }

    .avatar-popver-top .avatar-small {
      position: absolute;
      top: 12px;
      left: 12px;
    }

    .avatar-popver-top .username {
      font-size: 14px;
      font-weight: 600;
      margin-bottom: 5px;
    }

    .avatar-popver-bottom {
      padding: 0;
      border-top: 1px solid #dcdcdc;
      background-color: #f9f9f9;
      text-align: center;
      display: flex;
    }

    .avatar-popver-bottom a {
      text-decoration: none;
      color: #666666;
      font-weight: 600;
      font-size: 12px;
      display: block;
      flex: 1 1 50%;
      max-width: 50%;
      padding: 5px;
    }

    .avatar-popver-bottom a:first-child {
      border-right: 1px solid #dcdcdc;
    }

    .hidden {
      display: none;
    }
  </style>
  <link rel="stylesheet" id="font-awesome-css" href="//use.fontawesome.com/releases/v5.4.1/css/all.css" type="text/css"
    media="all">
  <script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
  <script src="https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js"></script>

  <!--avatar popup-->
  <div id="avatar-popver" class="avatar-popver hidden">
    <div class="arrow"></div>
    <div class="inner">
      <div class="avatar-popver-top">
        <div class="avatar-small"><img id="img1" src="../image/avatar.png"
            style="height: 40px;width: 40px;position: relative;left:0px;border-radius: 50% 50%;" /></div>
        <div class="detail">
          <div class="username" id="user_name"></div>

          <div class="rating-wrap">

            <span class="trav-rating"><span>5.0</span><i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                  class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></i></span>

          </div>

        </div>

      </div>
      <div class="avatar-popver-bottom" id="view_user">

        <a href="#" class="bloc-user"></a>
      </div>
    </div>
  </div>
  <script>
    var popperInstance;

    function showAvatarPopver(reference, popper) {
      popperInstance = new Popper(reference, popper, {
        placement: 'right'
      });
      popper.classList.remove('hidden');
      return popperInstance
    }
    var popper = document.getElementById('avatar-popver');
    var ref = document.getElementById('user_details');

    ref.addEventListener('click', function(e) {
      e.stopPropagation();
      e.preventDefault();
      showAvatarPopver(ref, popper);
    })
    document.addEventListener('click', function(e) {
      popper.classList.add('hidden');
    });
  </script>






</body>

</html>