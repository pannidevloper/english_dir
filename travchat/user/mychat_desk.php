<div class="col-lg-4 bg-grey pad-tb-15 chatroom_desk">
	<div class="">
		<div class="view-filter">
			<span class="autocompletion-block">
				<input name="search" type="text" value="" placeholder="Search" />
				<ul class="autocompletion-list"></ul>
			</span>

			<table width="100%" class="table table-striped table-bordered" id="myChatRoom">
				<thead>
					<th class="ch-title">
						<?php
                        if ($_SESSION['access']==1) {
                            echo "Users you have contacted";
                        } elseif ($_SESSION['access']==2) {
                            echo "Agents you have contacted";
                        } else {
                            echo "Members";
                        }
                        ?>
					</th>

				</thead>
				<tbody>
					<?php
                    $id = $_SESSION['id'];
                    
                    $firstReplyUser=empty($_GET['user'])?'':intval($_GET['user']);
                    //Get the order of unread message count
                    $unreadOrders=$wpdb->get_results("SELECT userid,COUNT(status) AS unread FROM `chat` WHERE c_to='{$id}' AND status=0 GROUP BY userid ORDER BY unread ASC", ARRAY_A);
                    $unreadOrderA=array();
                    foreach ($unreadOrders as $row) {
                        if ($row['userid']!=$firstReplyUser) {
                            $unreadOrderA[]=$row['userid'];
                        }
                    }
                    //Get user list order
                    $userOrders=$wpdb->get_results("SELECT userid,MAX(chat_date) AS last_message_date FROM `chat` WHERE c_to='{$id}' GROUP BY userid ORDER BY last_message_date ASC", ARRAY_A);
                    $userOrderA=array();
                    foreach ($userOrders as $row) {
                        if ($row['userid']!=$firstReplyUser) {
                            $userOrderA[]=$row['userid'];
                        }
                    }
                    if (!empty($firstReplyUser)) {
                        $unreadOrderA[]=$firstReplyUser;
                    }
                    $userField=implode(',', array_merge(array_diff($userOrderA, $unreadOrderA), $unreadOrderA));
                    if (!empty($userField)) {
                        $userField=" ORDER BY FIELD(cm.userid,{$userField}) DESC";
                    } else {
                        $userField='';
                    }
                    
                    $my = $wpdb->get_results("SELECT * FROM chat_member cm INNER JOIN user u ON u.userid = cm.userid LEFT JOIN sessions s ON s.id=(SELECT id FROM `sessions` WHERE sessions.user_id=u.userid ORDER BY id DESC LIMIT 1) WHERE chatroomid = '$id' {$userField}", ARRAY_A);
                    $row = 1;
            
                    foreach ($my as $myrow) {
                        $unread_message_count=$wpdb->get_var("SELECT COUNT(*) FROM chat WHERE c_to={$id} AND c_from={$myrow['userid']} AND status=0");
                        
                        //Get last message user sent
                        $last_message_row=$wpdb->get_row("SELECT * FROM `chat` WHERE (c_to={$id} OR c_to={$myrow['userid']}) AND (c_from={$myrow['userid']} OR c_from={$id}) AND type!='deleted' ORDER BY chat_date DESC LIMIT 1", ARRAY_A);
                        $last_message='';
                        if (!empty($last_message_row)) {
                            if ($last_message_row['type']=='attachment') {
                                $last_message='Attachment';
                            } else {
                                $last_message=$last_message_row['message'];
                            }
                            
                            $last_message=mb_strimwidth(trim(strip_tags($last_message)), 0, 70, "...");
                        }
                        
                        if ($_SESSION['id'] != $myrow['userid']) {
                            if ($row == 1) {
                                setcookie("to", $myrow['userid']);
                                $row++;
                            }
                            $is_active = get_user_status($myrow); ?>
					<tr id='<?php echo $myrow['userid']; ?>'
						onclick="saveUserId('<?php echo $myrow['userid']; ?>', this, '<?php echo $myrow['uname']; ?>', '<?php echo $myrow['country']; ?>','<?php echo $myrow['region']; ?>', '<?php echo $myrow['username']; ?>', '<?php echo $myrow['timezone']; ?>','<?php echo $myrow['photo']; ?>', <?php echo $is_active ?>);">
						<td <?php if ($unread_message_count>0) {
                                echo 'style="background: burlywood;"';
                            } ?>
							>
							<div class="profile-box">
								<?php
                                if (!empty($myrow['username']) && $myrow['username']!='Guest') {
                                    $user_photo = um_get_user_avatar_url($wpdb->get_var("SELECT ID FROM wp_users WHERE user_login='{$myrow['username']}'"), 80);
								}
								else {
									$user_photo = '../image/avatar.png';
								}
								?>
								<div class="c-prfl-details">
									<div class="c-prfl-img">
										<img src="<?php echo $user_photo; ?>"
											alt="Avatar" style="border-radius:50% 50%;width:40px;height:40px">
										<span
											class="status-indicator <?php echo $is_active ? 'online' : '' ?>"></span>
									</div>
									<div class="c-prfl-name">
										<?php echo $myrow['uname']; ?>
										<!--<p><?php //echo $last_message;?>
										</p>-->
										<p class="remove_mar" style="margin: 0px;"><?php echo $myrow['country'].','.$myrow['region']; ?>
										</p>
									</div>
									<br clear="all" />
								</div>
								<div class="c-prfl-details-right">
									<?php
                                    $before1days =  date('Y-m-d', strtotime("-2 days"));
                                    $today = date('Y-m-d');
                                    $coming_d =  $last_message_row['chat_date'];
									$time = strtotime($coming_d);
									$coming = date('Y-m-d', $time);
								 
									$show='';
									if ($coming<$today && $coming>$before1days) {
										$show =  "yesterday";
									} elseif ($today==$coming) {
										$show =  date('g:i A', $time);
									} else {
										 $show =   $coming;
									} ?>
									<div class="c-prfl-date">
										<?php// echo empty($last_message_row['chat_date'])?'':date('g:iA', strtotime($last_message_row['chat_date']));?>
										<?php echo $show; ?>
									</div>
									<div class="c-prfl-count"><a href="#"><span class="badge"><?php echo ($unread_message_count>=0) ? '' : $unread_message_count; ?></span></a>
									</div>
								</div>
								<br clear="all" />
							</div>
						</td>

					</tr>
					<?php
                        }
                    }
                    ?>
				</tbody>
			</table>
		</div>
	</div>
</div>