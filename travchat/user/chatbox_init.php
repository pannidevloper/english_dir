<?php
require_once($_SERVER['DOCUMENT_ROOT']."/English/wp-load.php");
global $wpdb;
session_start();

$agent=filter_input(INPUT_POST, 'agent', FILTER_SANITIZE_NUMBER_INT);

if(!empty($agent) && $agent>1) {
	if(empty($_SESSION['id']) && empty($_SESSION['access'])) {
		$wpdb->insert('user', array('uname'=>'Guest', 'username'=>'Guest', 'access'=>'0'));
		$_SESSION['id']=$wpdb->insert_id;
		$_SESSION['access']=0;
		$_SESSION['user_name']='Guest';
		setcookie('access', 0, time() + (86400 * 30), "/");

		$date = date('Y-m-d H:i:s', time());
		$wpdb->query("INSERT INTO `sessions` (user_id, ip_address, last_activity) VALUES ({$_SESSION['id']}, '{$_SERVER["REMOTE_ADDR"]}', '{$date}')");
	}
	if( $_SESSION['id']>1 && ($_SESSION['access']==0 || $_SESSION['access']==2) ) {
		if($wpdb->get_var("SELECT COUNT(*) FROM chat_member WHERE userid='{$_SESSION['id']}' AND chatroomid='{$agent}'")==0) {
			$wpdb->query("INSERT INTO `chat_member` (userid, chatroomid) VALUES ('{$_SESSION['id']}','{$agent}')");
			$wpdb->query("INSERT INTO `chat_member` (userid, chatroomid) VALUES ('{$agent}','{$_SESSION['id']}')");
		}
	}
	
}
