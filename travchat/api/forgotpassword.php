<?php
defined('BASEPATH') or exit('No direct script access allowed');

//Old code
/* if (isTheseParametersAvailable(array('email'))) {

      $email = empty($_REQUEST['email']) ? '' : check_input($_REQUEST['email']);

      $activated = password_hash($password . time(), PASSWORD_DEFAULT);

      $res = mysqli_query($conn, "SELECT count(*),username FROM `user` WHERE email='{$email}' and activated=1");

      while ($row = mysqli_fetch_row($res)) {
      $num = $row[0];
      $username = $row[1];
      }

      if ($num > 0) {
      mysqli_query($conn, "update `user` set activated='{$activated}' where email='{$email}'");

      $mail_content = 'Hello,<br>Welcome to Travchat! Please click on the link below to Reset your password<br>
      <div style="display:block;margin:22px 0px;">
      <p>Your username : ' . $username . '</p></br>
      <a style="background-color: #4caf50; color: white; padding: 12px; border: none; text-decoration: none;" href="http://www.travpart.com/English/travchat/activatepass.php?token=' . $activated . '&status=1">Reset Password</a>
      </div>
      <br>Or copy the link to the browser to open:<a href="http://www.travpart.com/English/travchat/activatepass.php?token=' . $activated . '&status=1">http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '</a>
      <br><br>- Travchat team';

      sendmailtosuer($email, 'Travchat - Please reset your password', $mail_content);

      http_response_code(200);
      $response['msg'] = 'Please check your email, a link to reset your password is sent to your email !';
      } else {
      http_response_code(401);
      $response['msg'] = 'Your mail id does not exist, please register.';
      }
      } else {
      http_response_code(401);
      $response['msg'] = 'Required parameter missing';
      } */
if (empty($_POST['email'])) {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
} else {
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $user_existed = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE email='{$email}'") > 0;
    $wp_user_existed = $wpdb->get_var("SELECT COUNT(*) FROM wp_users WHERE user_email='{$email}'") > 0;

    if ($user_existed && !$wp_user_existed) {
        $user = $wpdb->get_row("SELECT username,uname,access FROM user WHERE email='{$email}'", ARRAY_A);
        $password = md5(time());
        $role = exlog_map_role($user['access']);
        $userdata = array(
            'user_login' => $user['username'],
            'first_name' => '',
            'last_name'  => $user['uname'],
            'user_pass'  => $password,
            'role'       => $role,
            'user_email' => $email,
        );
        $new_user_id = wp_insert_user($userdata);
        update_user_meta($new_user_id, 'access', $user['access']);
        wp_update_user(array('ID' => $new_user_id, 'nickname' => $user['uname'], 'display_name' => $user['uname']));
    }

    $postdata = array(
        '_um_password_reset' => '1',
        'username_b' => check_input($_POST['email']),
        'form_id' => 'um_password_id',
        'timestamp' => time()
    );
    $url = "https://www.travpart.com/English/password-reset/";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $data = curl_exec($ch);

    if (curl_errno($ch)) {
        http_response_code(401);
        $response['msg'] = 'System is busy. Please try again.';
    } else if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
        http_response_code(401);
        $response['msg'] = 'Your mail id does not exist, please register.';
    } else if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 302) {
        http_response_code(200);
        $response['msg'] = 'Please check your email, a link to reset your password is sent to your email !';
    } else {
        http_response_code(401);
        $response['msg'] = 'System is busy. Please try again.';
    }
    curl_close($ch);
}
