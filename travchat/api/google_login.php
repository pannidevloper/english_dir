<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('access_token'))) {
    $google_userinfo_api = 'https://www.googleapis.com/oauth2/v2/userinfo';
    $access_token = filter_input(INPUT_POST, 'access_token', FILTER_SANITIZE_STRING);

    $headers = array(
        'Authorization: Bearer ' . $access_token,
    );

    $authorization = curl_init($google_userinfo_api);
    curl_setopt($authorization, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($authorization, CURLOPT_TIMEOUT, 60);
    curl_setopt($authorization, CURLOPT_HTTPHEADER, $headers);
    $google_response = json_decode(curl_exec($authorization), true);
    if (!empty($google_response['email'])) {
        $is_login = false;
        $passkey = $google_response['id'] . time();
        $token = md5($passkey);
        $row = $wpdb->get_row("SELECT wp_users.ID as wp_user_id, user.* from `wp_users`,`user` WHERE email='{$google_response['email']}' AND user.username=wp_users.user_login", ARRAY_A);

        if (empty($row)) {
            if (!empty($google_response['name'])) {
                $username = str_replace(' ', '', $google_response['name']);
            } else {
                $username = str_replace('@gmail.com', '', $google_response['email']);
            }

            while (username_exists($username) || $wpdb->get_var("SELECT COUNT(*) from user where username = '{$username}'") > 0) {
                $username = $username.rand(1, 9);
            }

            $wpdb->insert('user', array(
                'uname' => empty($google_response['name']) ? $username : $google_response['name'],
                'username' => $username,
                'email' => $google_response['email'],
                'password' => $token,
                'token' => $token,
                'access' => 2,
                'activated' => 1,
                'photo' => empty($google_response['picture']) ? '' : ('upload/' . $photo_name),
                'phone' => ''
            ));
            wp_authenticate($username, $passkey);
            $row = $wpdb->get_row("SELECT wp_users.ID as wp_user_id, user.* from `wp_users`,`user` WHERE email='{$google_response['email']}' AND user.username=wp_users.user_login", ARRAY_A);
            $is_login = true;
        } else {
            $ban_suspend = get_user_meta($row['wp_user_id'], 'ban_suspend', true);
            if ($ban_suspend == '1') {
                http_response_code(401);
                $response['msg'] = 'This account is no longger active';
            } elseif ($ban_suspend == '2') {
                http_response_code(401);
                $response['msg'] = 'Your account is currently restricted by our Trust & Safety team';
            } else {
                mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");
                $is_login = true;
            }
        }


        if ($is_login) {
            //save profile photo
            if (!empty($google_response['picture'])) {
                $profile_photo = file_get_contents($google_response['picture']);
                um_upload_profile_photo($profile_photo, $row['wp_user_id']);
            }

            $device_token =   to_get_user_device_token_from_wp_id_for_api($row['wp_user_id']);

            $profile_completeness=  get_user_meta($row['wp_user_id'], 'profile_completeness', true);
            $profile_completeness_value =  $profile_completeness*100;
            if (empty($profile_completeness) || $profile_completeness_value<50) {
                $msg2="<a href='https://www.travpart.com/English/user/?profiletab=profile'>Your profile is still incomplete, finish your profile now so your friends can find you</a>";

                $wpdb->insert('notification', array('wp_user_id'=>$row['wp_user_id'], 'content'=>$msg2,'identifier'=>'incomplete_profile'));
            }
            if ($device_token) {
                if ($profile_completeness_value<50) {
                    notification_for_user_form_api($device_token, "Your profile is still incomplete, finish your profile now so your friends can find you", 'incomplete_profile');
                }
            }

            $user = array(
            'userid' => $row['userid'],
            'username' => $row['username'],
            'email' => $row['email'],
            'phone' => $row['phone'],
            'uname' => $row['uname'],
            'photo' => (empty($row['photo']) ? um_get_user_avatar_url($row['wp_user_id'], 80) : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
            'country' => $row['country'],
            'region' => $row['region'],
            'timezone' => $row['timezone'],
            'access' => $row['access'],
            'token' => $token
            );

            http_response_code(200);
            $response['msg'] = $user;
        }
    } else {
        http_response_code(401);
        $response['msg'] = 'Wrong token';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
