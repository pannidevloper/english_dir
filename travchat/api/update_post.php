<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Content-Type: application/json');

$update_post = function() {
  global $wpdb;

	$post_id = filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_STRING);
	$post_text = filter_input(INPUT_POST, 'post_text', FILTER_SANITIZE_STRING);

  $postid = $_POST['postid'];
  $post_text = $_POST['posttext'];

  $kv_edited_post = array(
      'ID'           => $post_id,
      'post_content' => $post_text
  );
  return wp_update_post($kv_edited_post);
};

if (isTheseParametersAvailable(array('username', 'token', 'post_id', 'post_text'))) {
	http_response_code(200);

	$response = array(
		'data' => get_post($update_post())
	);
} else {
	http_response_code(401);
	$response['msg'] = 'Required parameter missing';
}
