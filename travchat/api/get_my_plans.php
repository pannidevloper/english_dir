<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('user_name', 'token'))) {

    $today_date = date("d/m/Y H:i:s");
    $today_date =date ("d-m-Y");

    $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$user_name}' AND token='{$token}'");

    if (!empty($userid)) {

        $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");

        $list = $wpdb->get_results("SELECT * FROM wp_user_plans WHERE user_id = '{$wp_userid}'  ORDER BY id DESC");

        if ($list) {

           foreach ($list as $plan) {


            $date = explode('/', $plan->end_date);
            $converted_Date  = date('d-m-Y', strtotime(implode('-', array_reverse($date))));

            if ( strtotime( $today_date )> strtotime( $converted_Date )){
            continue;
            }

                $plans[] = array(

                    'start_date' => $plan->start_date,
                    'end_Date' => $plan->end_date,
                    'days' => $plan->no_of_days,
                    'location' => $plan->location,
                    'plan_id' => $plan->id,
                    'plan_id' => $plan->user_id,
                    'coordiantes' => $plan->lat . "," . $plan->lng,
                    'given_name'=>$plan->given_name,
                    'notes'=>$plan->notes
                );
            }

            http_response_code(200);
            $response['msg'] = 'succes';
            $response['list'] = $plans;
        } else {

            http_response_code(200);
            $response['msg'] = 'You have no plans';
        }
    } else {

        http_response_code(200);
        $response['msg'] = 'username  not exist!';
    }
} else {

    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}
