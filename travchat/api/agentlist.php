<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    if (!empty($_POST['kw'])) {
        $kw =  filter_input(INPUT_POST, 'kw', FILTER_SANITIZE_STRING);
    }

    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);
    if (!empty($user)) {
        $userid = $user['userid'];
        $mem = array('999999');
        $um = mysqli_query($conn, "select * from `chat_member` where chatroomid='" . $userid . "'");

        while ($umrow = mysqli_fetch_array($um)) {
            $mem[] = $umrow['userid'];
        }
        $users = implode($mem, ',');

        $sql = " select * from `user` 
                    LEFT JOIN `sessions` ON
                        `sessions`.`id` = (
                            SELECT `id` from `sessions` where `user_id` = `user`.`userid` ORDER BY `id` DESC LIMIT 1
                        )
                        where  uname !='' AND userid NOT IN ('343','220') AND access = 1
                    ";
        if (isset($kw)) {
            $sql .= " AND username LIKE '%{$kw}%'";
        }
        $sql .= "ORDER BY seller_responsiveness ASC";
        $query = mysqli_query($conn, $sql);
        $tempArr = array();

        $index = 0;

        while ($row = mysqli_fetch_array($query)) {
            $index++;
            $is_active = false;
            if ($row['last_activity']) {
                $last_activity = new DateTime($row['last_activity']);
                $now = new DateTime();
                $is_active = $last_activity < $now && (($now->getTimestamp() - $last_activity->getTimestamp()) <= MAX_INACTIVITY_TIME);
            }
            $username = $row['username'];
            $author = get_user_by('slug', $username);
            if (!empty($author)) {
                $args = array(
                    'post_type'  => 'post',
                    'author'     => $author->ID,
                    'status' =>   'publish'
                );
                $wp_posts = get_posts($args);

                $avatar = um_get_user_avatar_data($author->ID);
                if (!empty($avatar['url'])) {
                    $agent_avatar = $avatar['url'];
                }

                $verified = (get_user_meta($author->ID, 'verification_status', true) == 2);

                if (count($wp_posts) > 0) {
                    $tempArr[] = array(
                        'userid' => $row['userid'],
                        'name' => (empty($row['uname']) ? $row['username'] : $row['uname']),
                        'photo' => (empty($agent_avatar) ? '' : ($agent_avatar)),
                        'phone' => $row['phone'],
                        'country' => $row['country'],
                        'region' => $row['region'],
                        'verified' => $verified,
                        'online' => $row['is_active'],
                        'last_activity' => $row['last_activity'],
                        'hours' => $row['seller_responsiveness']
                    );
                }
            }
        }

        http_response_code(200);
        $response['msg'] = "agentlist retrived successfully";
        $response['agentlist'] = $tempArr;
    } else {
        http_response_code(401);
        $response['msg'] = "username does not exists";
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
