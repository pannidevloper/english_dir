<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);
    if (empty($user)) {
        http_response_code(401);
        $response['msg'] = 'check username';
    } else {
        //Get the order of unread message count
        $unreadOrders = $wpdb->get_results("SELECT userid,COUNT(status) AS unread FROM `chat` WHERE c_to='{$user['userid']}' AND status=0 GROUP BY userid ORDER BY unread ASC", ARRAY_A);
        $unreadOrderA = array();
        foreach ($unreadOrders as $row) {
            $unreadOrderA[] = $row['userid'];
        }
        //Get user list order
        $userOrders = $wpdb->get_results("SELECT userid,MAX(chat_date) AS last_message_date FROM `chat` WHERE c_to='{$user['userid']}' GROUP BY userid ORDER BY last_message_date ASC", ARRAY_A);
        $userOrderA = array();
        foreach ($userOrders as $row) {
            $userOrderA[] = $row['userid'];
        }
        $userField = implode(',', array_merge(array_diff($userOrderA, $unreadOrderA), $unreadOrderA));
        if (!empty($userField))
            $userField = " ORDER BY FIELD(userid,{$userField}) DESC";
        else
            $userField = '';
        $my = mysqli_query($conn, "SELECT * FROM user WHERE userid IN(SELECT distinct c_to FROM chat WHERE c_from='{$user['userid']}')
        OR userid IN(SELECT distinct c_from FROM chat WHERE c_to='{$user['userid']}')  {$userField}");
        $chat_members = array();
        while ($myrow = mysqli_fetch_array($my)) {
            $unread_message_count = mysqli_fetch_array(mysqli_query($conn, "SELECT COUNT(*) FROM chat WHERE c_to={$user['userid']} AND c_from={$myrow['userid']} AND status=0"))[0];
            //Get last message user sent
            $last_message_row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM `chat` WHERE (c_to={$user['userid']} OR c_to={$myrow['userid']}) AND (c_from={$myrow['userid']} OR c_from={$user['userid']}) AND type!='deleted' ORDER BY chat_date DESC LIMIT 1"));
            $last_message = '';
            if (!empty($last_message_row)) {
                if ($last_message_row['type'] == 'attachment')
                    $last_message = 'Attachment';
                else
                    $last_message = $last_message_row['message'];

                $last_message = mb_strimwidth(trim(strip_tags($last_message)), 0, 70, "...");
            }
            $wp_userid = $wpdb->get_var("SELECT ID FROM wp_users WHERE wp_users.user_login='{$myrow['username']}'");
            if(empty($wp_userid)) {
                $avatar=plugins_url().'/ultimate-member/assets/img/default_avatar.jpg';
            }
            else {
                $avatar=um_get_user_avatar_url($wp_userid, 80);
            }
            array_push($chat_members, array(
                'userid' => $myrow['userid'],
                'uname' => $myrow['username'],
                'photo' => $avatar,
                'country' => $myrow['country'],
                'region' => $myrow['region'],
                'timezone' => $myrow['timezone'],
                'unread' => $unread_message_count,
                'last_message' => $last_message,
                'last_message_date' => $last_message_row['chat_date'],
                'online' => $myrow['is_active']
            ));
        }

        $response['msg'] = "success";
        $response['chatmembers'] = $chat_members;
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
