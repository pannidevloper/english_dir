<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);
    $visiblity = filter_input(INPUT_POST, 'location_visiable', FILTER_SANITIZE_STRING);

    $wp_userid = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($wp_userid)) {
        if ($status == "on"  || $status == "off") {
            $updated = update_user_meta($wp_userid, 'location_tracker', $status);
        }

        if ($visiblity == 'family' || $visiblity == 'family_friend' || $visiblity == 'friend' || $visiblity == 'anyone' || $visiblity == 'specific_person') {
            update_user_meta($wp_userid, 'location_visiable', $visiblity);
        }

        $family_member=get_user_meta($wp_userid, 'family_member', true);
        if(empty($family_member)) {
            $family_member=array();
        }
        else {
            $family_member=explode(',', $family_member);
        }

        $specific_person=get_user_meta($wp_userid, 'specific_person', true);
        if(empty($specific_person)) {
            $specific_person=array();
        }
        else {
            $specific_person=explode(',', $specific_person);
        }

        $tracker=get_user_meta($wp_userid, 'location_tracker', true);
        $visiable=get_user_meta($wp_userid, 'location_visiable', true);

        $response['status'] = array(
            'tracker' => empty($tracker)?'off':$tracker,
            'visiable' => empty($visiable)?'family':$visiable,
            'family_member'=> $family_member,
            'specific_person'=> $specific_person,
        );
        http_response_code(200);
    } else {
        http_response_code(401);
        $response['msg'] = 'No User Found';
    }
} else {

    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}
