<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token', 'event_location'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $event_name = filter_input(INPUT_POST, 'event_name', FILTER_SANITIZE_STRING);
    $event_type = filter_input(INPUT_POST, 'event_type', FILTER_SANITIZE_STRING);
    $event_invited_people = $_POST['event_invited_people'];
    $event_location = filter_input(INPUT_POST, 'event_location', FILTER_SANITIZE_STRING);
    $start_date = filter_input(INPUT_POST, 'start_date', FILTER_SANITIZE_STRING);
    $end_date = filter_input(INPUT_POST, 'end_date', FILTER_SANITIZE_STRING);
    $event_tour_package = filter_input(INPUT_POST, 'event_tour_package', FILTER_SANITIZE_NUMBER_INT);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_ID) {
        $new_post = array(
            'post_title'   => $event_name,
            'post_type'      => 'eventpost',
            'post_status'    => 'publish',
            'post_author' => $wp_user_ID
        );

        $new_post_id = wp_insert_post($new_post, true);

        if (is_wp_error($new_post_id)) {
            http_response_code(401);
            $response['msg'] = $new_post_id->get_error_message();
            exit(json_encode($response));
        }

        $event_type = $event_type == 'public' ? 'public' : 'private';
        update_post_meta($new_post_id, 'event_type', $event_type);

        if (!empty($_FILES['event_location_image'])) {
            $attachment_id = media_handle_upload('event_location_image', $new_post_id, array('post_author' => $wp_user_ID));
            if (is_wp_error($attachment_id)) {
                http_response_code(401);
                $response['msg'] = 'File is invaild';
                exit;
            } else {
                update_post_meta($new_post_id, 'event_location_image_id', $attachment_id);
            }
        }

        if (!empty($event_invited_people)) {
            update_post_meta($new_post_id, 'event_invited_people', $event_invited_people);
            foreach ($event_invited_people as $p) {
                if (intval($p) > 0)
                    $wpdb->insert('notification', array('wp_user_id' => intval($p), 'content' => um_get_display_name($wp_user_ID) . " invited you to their event"));
            }
        }

        if (!empty($event_location)) {
            update_post_meta($new_post_id, 'event_location', $event_location);
        }

        if (!empty($start_date)) {
            update_post_meta($new_post_id, 'start_date', strtotime($start_date));
        }

        if (!empty($end_date)) {
            update_post_meta($new_post_id, 'end_date', strtotime($end_date));
        }

        if (!empty($event_tour_package)) {
            update_post_meta($new_post_id, 'event_tour_package', $event_tour_package);
            $hotelinfo = $wpdb->get_row("SELECT img,location FROM wp_hotel WHERE tour_id='{$event_tour_package}' LIMIT 1");
            if (!empty($hotelinfo->img)) {
                if (is_array(unserialize($hotelinfo->img))) {
                    $hotelimg = unserialize($hotelinfo->img)[0];
                } else
                    $hotelimg = $hotelinfo->img;
            }
            if (!empty($hotelimg)) {
                update_post_meta($new_post_id, 'tour_img', $hotelimg);
            }
        }

        http_response_code(200);
        $response['msg'] = 'Posted successfully';
    }
    // if user not exist
    else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
