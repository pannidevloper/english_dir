<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token','user_id'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $userid = filter_input(INPUT_POST, 'user_id', FILTER_VALIDATE_INT, array('options' => array('default' => 0, 'min_range' => 0)));
 

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($wp_user_ID)) {

	$avg_rating = $wpdb->get_var("SELECT AVG(rating) FROM `social_reviews` WHERE user_id='{$userid}'");

	$count_rows = $wpdb->get_var("SELECT COUNT(*) FROM `social_reviews` WHERE user_id='{$userid}'");

    $social_reviews = $wpdb->get_results("SELECT creator_id,review,rating FROM `social_reviews` WHERE user_id='{$userid}'", ARRAY_A);

	if ($avg_rating == 0) {
	$avg_rating = 0;
	}
	 http_response_code(200);
	 $response['msg'] = "retrived";
	  $response['no_of_ratings'] = $count_rows;
     $response['rating'] = $avg_rating;
     $response['reviews'] = $social_reviews;
       

    } else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
