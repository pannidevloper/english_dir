<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_ID) {

        $args = array(
            'post_type'  => 'shortpost',
            'author'     =>  $wp_user_ID,
        );
        $short_posts = get_posts($args);

        if ($short_posts) {


            foreach ($short_posts as $single_post) {


                $postslist[] = array(

                    'post_id' => $single_post->ID,
                    'post_content' => $single_post->post_content,
                    'time' => $single_post->post_modified,
                    'feeling' => get_post_meta($single_post->ID, 'feeling', true)
                );
            }

            http_response_code(200);
            $response['success'] = 1;
            $response['msg'] = "posts retrived";
            $response['data'] = $postslist;
        } else {
            http_response_code(200);
            $response['success'] = 0;
            $response['msg'] = 'No Posts';
        }
    } else {

        http_response_code(400);
        $response['success'] = 0;
        $response['msg'] = 'No user found';
    }
} else {

    http_response_code(401);
    $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
}
