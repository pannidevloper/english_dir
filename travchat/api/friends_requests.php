<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_ID) {

        $connections = $wpdb->get_results(
            "SELECT * FROM friends_requests WHERE  user2 = '{$wp_user_ID}' AND status=0 "
        );

        foreach ($connections as $value) {

            $Data = $wpdb->get_row("SELECT * FROM `wp_users` WHERE ID = '$value->user1'");

            $requests[] = array(

                'user_name' => $Data->user_login,
                'user_id' => $value->user1,
                'img' => esc_url(get_avatar_url($value->user1)),
                'request_id'=>$value->id
            );
        }
        http_response_code(200);
        $response['succes'] = 1;
        $response['msg'] = 'retrived list';
        $response['data'] = $requests;
    } else {
        http_response_code(401);
        $response['succes'] = 0;
        $response['msg'] = 'No user exist';
    }
} else {

    http_response_code(401);
    $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
}
