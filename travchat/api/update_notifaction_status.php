<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('id'))) {

    $notification_id = check_input($_REQUEST['id']);
    $wpdb->update(
        'notification',
        array(
            'read_status' => 1
        ),
        array('ID' => $notification_id),
        array(
            '%d'    // value2
        ),
        array('%d')
    );
    http_response_code(200);
    $response['msg'] = 'status updated';
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
