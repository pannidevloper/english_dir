<?php
set_time_limit(0);
function uniqueAssocArray($array, $uniqueKey)
{
    $unique = array();

    foreach ($array as $value) {
        $unique[$value[$uniqueKey]] = $value;
    }

    $data = array_values($unique);

    return $data;
}
defined('BASEPATH') or exit('No direct script access allowed');

$postdata = json_decode(file_get_contents('php://input'), true);

if (!empty($postdata['username']) && !empty($postdata['token']) && !empty($postdata['contact_list'])) {
    $username = filter_var($postdata['username'], FILTER_SANITIZE_STRING);
    $token = filter_var($postdata['token'], FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {
        $contact_list_array = $postdata['contact_list'];
        $contact_list_array = uniqueAssocArray($contact_list_array, 'phoneNumber');
        $start_time = microtime(true);
        $phone_sql = "";
        $phones = array();

        for ($i = 0; $i < count($contact_list_array); $i++) {
            $phone = $contact_list_array[$i]['phoneNumber'];
            $contact_name = $contact_list_array[$i]['name'];
            $phone = str_replace(' ', '', $phone);

            // some saved numbers also have - in that so remove
            $phone = str_replace(array('+','-'), '', $phone);
            $save_phone_no = $phone;
            to_save_phone_number ($wp_user_ID,$save_phone_no,$contact_name);
            $phone = substr($phone, -7);

            if (strlen($phone) > 5) {
                $phone_sql .= "OR LOCATE('{$phone}',`phone`) ";
                $phones[] = $save_phone_no;
            }
        }

        $check_phones = $wpdb->get_results("SELECT wp_users.ID,user.region,user.phone FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND (" . trim($phone_sql, 'OR') . ")");

        foreach ($check_phones as $check_phone) {

            if ($check_phone) {

                if (!empty($check_phone->phone) and isset($check_phone->phone)) {
                    // calculations for status,either they have sent friend request before/blocked
                    $user_phone = $check_phone->phone;
                    $user_phone = str_replace(' ', '', $user_phone);
                    // some saved numbers also have - in that so remove that also
                    $user_phone = str_replace(array('+','-'), "", $user_phone);

                    if (in_array($user_phone, $phones)) {
                        $Data = $wpdb->get_row("SELECT status FROM `friends_requests` WHERE user1 = '$wp_user_ID' AND user2 = '{$check_phone->ID}'");
                        if (empty($Data)) {

                            $reverse_check = $wpdb->get_row("SELECT status FROM `friends_requests` WHERE user2 = '$wp_user_ID' AND user1 = '{$check_phone->ID}'");

                            if (!empty($reverse_check) and isset($reverse_check)) {

                                $button_text = "Requested to connect with you";
                                $button_class = "pending_button";
                                //var_dump($reverse_check);
                                if ($reverse_check->status == 1) {

                                    $button_text = "Connected";
                                    $button_class = "connected_button";
                                }
                            } else {
                                $button_text = 'Connect';
                                $button_class = "connect_button";
                            }
                        } else {
                            if ($Data->status == 0) {

                                $button_text = "Connection Request sent";
                                $button_class = "requestsent_button";
                            } elseif ($Data->status == 1) {

                                $button_text = "Connected";
                                $button_class = "connected_button";
                            } elseif ($Data->status == 2) {

                                $button_text = "Blocked";
                                $button_class = "blocked_button";
                            }
                        }

                        if ($check_phone->ID != $wp_user_ID and empty($Data)) {
                if( get_avatar_url( $value->ID) =="http://www.travpart.com/English/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg" || get_avatar_url( $value->ID) =="https://www.travpart.com/English/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg")
                    continue;
                            $contacts[] = array(
                                'userid' => $check_phone->ID,
                                'username' => um_get_display_name($check_phone->ID),
                                'hometown' => get_user_meta($check_phone->ID,'hometown',true),
                                'photo' => um_get_user_avatar_url($check_phone->ID),
                                'status' => $button_text,
                                'phone' => $check_phone->phone,
                                'phone1' => $check_phone->phone
                            );
                        }
                    }
                }
            }
        }
        $contacts = uniqueAssocArray($contacts, 'userid');
        http_response_code(200);
        $response['msg'] =  'success';
        $response['list'] =  $contacts;
    } else {
        http_response_code(401);
        $response['msg'] = 'No user found';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}


function to_save_phone_number($wp_user_ID, $phone, $contact_name)
{

    global $wpdb;
    $phone_check_from_db_for_user = $wpdb->get_var("SELECT id FROM `user_contacts` WHERE  wp_user_id='{$wp_user_ID}' AND contact='{$phone}'");

    if (empty($phone_check_from_db_for_user)) {

        $sql = $wpdb->prepare("INSERT INTO `user_contacts` (`wp_user_id`, `contact_name`,`contact`) values (%d,%s, %s)", $wp_user_ID, $contact_name, $phone);

        $wpdb->query($sql);
    }
}
