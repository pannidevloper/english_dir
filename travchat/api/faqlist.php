<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {

    $username = $_REQUEST['username'];
    $token = check_input($_REQUEST['token']);

    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);
    if (!empty($user)) {

        $url = 'https://www.tourfrombali.com/faq/';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $data = curl_exec($ch);
        if (curl_errno($ch)) {
            echo FALSE;
        } else {
            preg_match_all('!<div class="faqwd_answer">(.+?).{0,1}</div>!is', $data, $faq_answer);
            preg_match_all('!<span class="faqwd_post_title".*?>(.+?)</span>!is', $data, $faq_title);

            if (!empty($faq_title[1]) && count($faq_answer[1]) == count($faq_title[1])) {
                $faq = '';
                $temp = array();
                for ($i = 0; $i < count($faq_title[1]); $i++) {

                    $title = strip_tags($faq_title[1][$i]);
                    $answer = strip_tags($faq_answer[1][$i]);

                    array_push($temp, array("title" => $title, "answer" => $answer));
                }

                http_response_code(200);
                $response['msg'] = "success";
                $response['faqlist'] = $temp;
            } else {
                http_response_code(401);
            }
        }

        curl_close($ch);
    } else {
        http_response_code(401);
        $response['msg'] = 'Incorrect Username';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
