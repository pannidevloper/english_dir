<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (isTheseParametersAvailable(array('username', 'token', 'comment_id'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $comment_id = filter_input(INPUT_POST, 'comment_id', FILTER_SANITIZE_STRING);


$wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

   if ($wp_user_ID) {

    $comment_user_id = $wpdb->get_var("SELECT user_id FROM `user_feed_comments` WHERE  id='{$comment_id}'");

    if( $comment_user_id ){

        if($comment_user_id==$wp_user_ID){

             $wpdb->delete( 'user_feed_comments', array( 'id' => $comment_id ) );

             http_response_code(200);
             $response['success'] = 0;
             $response['msg'] = 'Comment deleted successfully';

        }
        else{

             http_response_code(401);
             $response['success'] = 0;
             $response['msg'] = 'You can not delete other user comment';

        }

    }
    else{

        http_response_code(401);
        $response['success'] = 0;
        $response['msg'] = 'Comment Not exist!';

    }
   
   }
	else{
		http_response_code(401);
		$response['success'] = 0;
        $response['msg'] = 'User does not exist';

	}

}
else{
	 http_response_code(401);
	 $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
}