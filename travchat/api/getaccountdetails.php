<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username'))) {

    $myq = mysqli_query($conn, "select * from `user` where username='" . $_REQUEST['username'] . "'");

    if ($myqrow = mysqli_fetch_array($myq)) {
        $arr = array(
            "userid" => $myqrow['userid'],
            "username" => $myqrow['username'],
            "email" => $myqrow['email'],
            "password" => $myqrow['password'],
            "uname" => $myqrow['uname'],
            "photo" => (empty($myqrow['photo']) ? '' : ("https://www.travpart.com/English/travchat/{$myqrow['photo']}")),
            "access" => $myqrow['access'],
            "activated" => $myqrow['activated'],
            "country" => $myqrow['country'],
            "region" => $myqrow['region'],
            "timezone" => $myqrow['timezone'],
            "user_unique_room_id" => $myqrow['user_unique_room_id'],
            "userid_travcust" => $myqrow['userid_travcust'],
            "phone" => $myqrow['phone'],
            "online_time" => $myqrow['online_time'],
            "is_active" => $myqrow['is_active'],
        );
        http_response_code(200);
        $response['userdata'] = $arr;
    } else {
        http_response_code(401);
        $response['msg'] = "ERROR";
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
