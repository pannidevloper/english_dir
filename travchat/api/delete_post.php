<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Content-Type: application/json');

if (isTheseParametersAvailable(array('username', 'token', 'post_id'))) {
	http_response_code(200);

	  $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
	  $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
	  $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
	$post_id = filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT);
	$post = get_post($post_id);

	if($post->post_author == $wp_user_ID) {
		$response = array(
			'data' => wp_delete_post($post_id)
		);	
	}
} else {
	http_response_code(401);
	$response['msg'] = 'Required parameter missing';
}
