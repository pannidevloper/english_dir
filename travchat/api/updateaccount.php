<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'name', 'password', 'email', 'phone', 'token'))) {
    $token = check_input($_REQUEST['token']);
    $username = check_input($_REQUEST['username']);

    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);

    if (empty($user)) {
        http_response_code(401);
        $response['msg'] = "username not found";
        // echo json_encode(array("error_msg" => "username not found"));
    } else {
        $id = $user['userid'];

        $update_items = '';

        if (!empty($_POST['password']))
            $update_items .= "password='" . md5($_POST['password']) . "',";
        if (!empty($_POST['name']))
            $update_items .= "uname='{$_POST['name']}',";
        if (!empty($_POST['email']))
            $update_items .= "email='{$_POST['email']}',";
        if (!empty($_POST['phone']))
            $update_items .= "phone='{$_POST['phone']}',";
        $update_items = rtrim($update_items, ',');


        if (mysqli_query($conn, "update `user` set {$update_items} where userid='{$user['userid']}'")) {
            http_response_code(200);
            $response['msg'] = 'Account updated';
        } else {

            http_response_code(401);
            $response['msg'] = 'Error happened';
        }
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
