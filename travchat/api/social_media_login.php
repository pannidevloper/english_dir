<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isset($_REQUEST['username']) && isset($_REQUEST['useremail'])) {

    $useremail = check_input($_REQUEST['useremail']);
    $username = str_replace(' ', '', check_input($_REQUEST['username']));
    $avatar_url = filter_input(INPUT_POST, 'avatar_url', FILTER_VALIDATE_URL);

    //check for user,is the user already there
    $user_count = mysqli_query($conn, "SELECT count(*) FROM `user` WHERE email='{$useremail}'");
    $num = mysqli_fetch_row($user_count)[0];

    // check either this user also have account in our wp users table

    $user_count_in_wp_users = mysqli_query($conn, "SELECT count(*) FROM `wp_users` WHERE user_email='{$useremail}'");
    $num_in_wp_users = mysqli_fetch_row($user_count_in_wp_users)[0];

    if ($num > 0 && $num_in_wp_users > 0) {

        $response = user_login_for_social($username, $useremail, $avatar_url);
    } elseif ($num_in_wp_users > 0 && $num < 1) {

        $nameforusrmail = $username;
        $fusername = $username;
        $email = $useremail;
        $fpassword = md5(time());
        $access = 2;
        $activated = 1;
        $location = "";
        $phone = "";
        //$token ="";

        // INSERT USER IN USER TABLE
        mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated,photo,phone) values ('$nameforusrmail', '$fusername','$email', '$fpassword', '$access','$activated','$location','$phone')");


        $response = user_login_for_social($username, $useremail, $avatar_url);
    } elseif ($num > 0 && $num_in_wp_users < 1) {

        $website = "http://Travpart.com/English";
        $userdata = array(
            'user_login' =>  $username,
            'user_url'   =>  $website,
            'user_email' => $useremail,
            'user_pass'  =>  md5(time()) // When creating an user, `user_pass` is expected.
        );

        $user_id = wp_insert_user($userdata);
        http_response_code(400);
        //LOGIN NOW
        $response = user_login_for_social($username, $useremail, $avatar_url);
    }
    // else of 1st user count
    elseif ($num_in_wp_users < 1 && $num < 1) {

        $nameforusrmail = $username;
        $fusername = $username;
        $email = $useremail;
        $fpassword = md5(time());
        $access = 2;
        $activated = 1;
        $location = "";
        $phone = "";


        // INSERT USER IN USER TABLE
        mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated,photo,phone) values ('$nameforusrmail', '$fusername','$email', '$fpassword', '$access','$activated','$location','$phone')");


        $website = "http://Travpart.com/English";
        $userdata = array(
            'user_login' =>  $username,
            'user_url'   =>  $website,
            'user_email' => $useremail,
            'user_pass'  =>  md5(time()) // When creating an user, `user_pass` is expected.
        );

        $user_id = wp_insert_user($userdata);
        http_response_code(200);
        //LOGIN NOW
        $response = user_login_for_social($username, $useremail, $avatar_url);
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}
