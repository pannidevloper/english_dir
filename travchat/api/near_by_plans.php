    <?php
    defined('BASEPATH') or exit('No direct script access allowed');

    if (isset($_REQUEST['lat']) && isset($_REQUEST['lng'])) {

    $today_date = date("d/m/Y H:i:s");
    $today_date =date ("d-m-Y");

    $lat = check_input($_REQUEST['lat']);
    $lng = check_input($_REQUEST['lng']);
    $miles = 50;

    $all_plans = $wpdb->get_results("SELECT * FROM `wp_user_plans`");

    foreach ($all_plans as $plan) {

    $date = explode('/', $plan->end_date);
    $converted_Date  = date('d-m-Y', strtotime(implode('-', array_reverse($date))));

    if ( strtotime( $today_date )> strtotime( $converted_Date )){
    continue;
    }


    $calculated_miles = API_User_distance($lat, $lng, $plan->lat, $plan->lng, "M");

    if ($calculated_miles < $miles && $calculated_miles != 0) {

    // $near_plans_array[] = $plan->id;

    $start_date_time = explode(" ", $plan->start_date);
    if (is_null($start_date_time[1])) {

    $start_date_time[1] = "00:00:00";
    }


    $end_date_time = explode(" ", $plan->end_date);

    if (is_null($end_date_time[1])) {
    $end_date_time[1] = "00:00:00";
    }

    $plans_list[] = array(

    'start_date' => $start_date_time[0],
    'end_Date' => $end_date_time[0],
    'days' => $plan->no_of_days,
    'location' => $plan->location,
    'plan_id' => $plan->id,
    'user_id' => $plan->user_id,
    'friend_name' =>  um_get_display_name($plan->user_id),
    'avatar' => um_get_user_avatar_url($plan->user_id, 80),
    'coordiantes' => $plan->lat . "," . $plan->lng,
    //   'mut_friends_count' => count($match),
    //  'mut_friends_ids' => $match,
    'start_time' => $start_date_time[1],
    'end_time' => $end_date_time[1],
    'miles' => $calculated_miles
    );
    }
    $response['msg'] = "successfully";
    $response['list'] = $plans_list;
    }
    } //end of if
    else {
    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
    }
