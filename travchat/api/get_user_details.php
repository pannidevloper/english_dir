<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    $career = $wpdb->get_row("SELECT job_title,company,location FROM `wp_career` WHERE userid='{$wp_user_ID}' ORDER BY end_date DESC LIMIT 1");
    $education = $wpdb->get_row("SELECT school,degree,area FROM `wp_education` WHERE userid='{$wp_user_ID}' ORDER BY enddate DESC LIMIT 1");

    $age = 0;
    if (!empty(get_user_meta($wp_user_ID, 'date_of_birth', true))) {
        $date_of_birth = strtotime(get_user_meta($wp_user_ID, 'date_of_birth', true));
        if ($data_of_birth < time()) {
            $age = date('Y') - date('Y', $date_of_birth);
            if (date('n') < date('n', $date_of_birth)) {
                $age--;
            } elseif (date('n') == date('n', $date_of_birth) && date('j') < date('j', $date_of_birth)) {
                $age--;
            }
        }
    }

    http_response_code(200);
    $response = array(
        'career' => $career,
        'education' => $education,
        'age' => $age
    );

} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
