<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token')) && !empty($_FILES['photo_or_video'])) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_ID) {
        $attachment_id = media_handle_upload('photo_or_video', 0, array('post_author' => $wp_user_ID));
        if (is_wp_error($attachment_id)) {
            http_response_code(401);
            $response['msg'] = 'File is invaild';
        } else {
            http_response_code(200);
            $response['attachment_id'] = $attachment_id;
            $response['url'] = wp_get_attachment_url($attachment_id);
        }
    } else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
