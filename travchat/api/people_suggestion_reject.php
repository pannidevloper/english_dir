<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token', 'userid'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $userid = filter_input(INPUT_POST, 'userid', FILTER_VALIDATE_INT, array('options' => array('default' => 0, 'min_range' => 0)));

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($wp_user_ID)) {
        if ($wpdb->get_var("SELECT COUNT(*) FROM friends_requests WHERE user1={$wp_user_ID} AND user2={$userid}") == 0) {
            $wpdb->insert('friends_requests', array('user1' => $wp_user_ID, 'user2' => $userid, 'status' => 2, 'action_by_user' => $wp_user_ID));
        } else {
            $wpdb->update('friends_requests', array('status' => 2), array('user1' => $wp_user_ID, 'user2' => $userid, 'action_by_user' => $wp_user_ID));
        }
        http_response_code(200);
        $response['msg'] = 'Success';
    } else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
