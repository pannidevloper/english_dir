<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $user = $wpdb->get_row("SELECT wp_users.ID,user.access FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($user->ID)) {
        $response['feelings'] = array(
            '😀happy',
            '😥sad',
            '😍loved',
            '😡angry',
        );

        $friends = $wpdb->get_results("SELECT ID FROM `wp_users` WHERE ID IN(
                    SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}')
                    OR ID IN(
                        SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}')", ARRAY_A);
        foreach ($friends as &$friend) {
            $friend['name'] = um_get_display_name($friend['ID']);
        }
        $response['tag_friends'] = $friends;

        if ($user->access == 1) {
            $response['tour_packages'] = $wpdb->get_results("SELECT post_title, meta_value as tour_id FROM `wp_posts`,`wp_postmeta`
                    WHERE post_type='post' AND post_status='publish' AND `meta_key`='tour_id'
                    AND wp_posts.ID=wp_postmeta.post_id AND post_author='{$user->ID}'");
        }

        http_response_code(200);
        $response['msg'] = 'success';
    } else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
