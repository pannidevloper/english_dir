<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token', 'email', 'name', 'question'))) {

    $token = check_input($_REQUEST['token']);
    $username = check_input($_REQUEST['username']);

    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);
    if (empty($user)) {
        http_response_code(401);
        $response['msg'] = 'user not found';
    } else {
        $csa_email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $csa_name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
        $csa_message = filter_input(INPUT_POST, 'question', FILTER_SANITIZE_STRING);
        $csa_destination = filter_input(INPUT_POST, 'destination', FILTER_SANITIZE_STRING);
        $csa_subject = 'Quote request';
        $csa_question_cat = 'Ask a support';

        $chatUserId = $user['userid'];

        $csa_agents = $wpdb->get_results("SELECT username,email FROM `user` LEFT JOIN `sessions` ON `sessions`.`id` = (
            SELECT `id` from `sessions` where `user_id` = `user`.`userid` ORDER BY `id` DESC LIMIT 1)
            WHERE access=1 AND activated=1 ORDER BY last_activity DESC LIMIT 10", ARRAY_A);
        $csa_agent_name = $csa_agents[0]['username'];

        $agentInfo = $wpdb->get_row("SELECT `userid`,`email` FROM `user` WHERE `username`='{$csa_agent_name}'");
        $csa_agent_email = $agentInfo->email;
        $agentUserid = $agentInfo->userid;

        $toAgentMailBody = <<<EOD
Hi {$csa_agent_name},

You have received an inquiry from {$csa_name} about {$csa_destination}. Below is full description about his/her query.
            
{$csa_message}

Thanks,

The Travpart Team
EOD;

        $wpdb->insert(
            'contact_sales_agent',
            array(
                'csa_user_name' => $csa_name,
                'csa_user_email' => $csa_email,
                'csa_subject' => $csa_subject,
                'csa_destination_plan' => $csa_destination,
                'csa_agent_email' => $csa_agent_email,
                'csa_message' => $csa_message,
                'csa_question_cat' => $csa_question_cat,
                'csa_created' => date('Y-m-d H:i:s'),
            ),
            array(
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s'
            )
        );

        if ($wpdb->insert_id) {

            $wpdb->query("insert into `chat` (message, userid, chat_date,c_to,c_from,type,attachment_type) values ('" . trim($toAgentMailBody) . "' , '{$chatUserId}', NOW(),'{$agentUserid}','{$chatUserId}','','')");

            include(get_template_directory() . "/potential-sales-agents-email-content.php");
            wp_mail($csa_agent_email, 'A travel customer for you from travpart.com', str_replace('[name]', $csa_agent_name, $potential_sale_agent_email_content));
            $potential_sale_agents = $wpdb->get_results("SELECT `name`,`email`,`create_time` FROM `wp_potential_sale_agent` WHERE location LIKE '{$csa_destination_plan}'");
            foreach ($potential_sale_agents as $t) {
                $lasted_email_send_time = $wpdb->get_var("SELECT `csa_created` FROM `contact_sales_agent` WHERE `csa_created`>'{$t->create_time}' ORDER BY `csa_created` ASC LIMIT 1");
                $create_time = strtotime($t->create_time);
                if (!empty($lasted_email_send_time) && $create_time < strtotime($lasted_email_send_time)) {
                    if ((time() - strtotime($lasted_email_send_time)) > 15 * 24 * 60 * 60)
                        continue;
                }
                wp_mail($t->email, 'A travel customer for you from travpart.com', str_replace('[name]', $t->name, $potential_sale_agent_email_content));
            }
            foreach ($csa_agents as $key => $t) {
                if ($key == 0)
                    continue;
                wp_mail($t['email'], 'A travel customer for you from travpart.com', str_replace('[name]', $t['username'], $potential_sale_agent_email_content));
            }
            http_response_code(200);
            $response['msg'] = 'success';
        } else {
            http_response_code(401);
            $response['msg'] = 'System error. Try again later';
        }
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
