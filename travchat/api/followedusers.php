<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

    if (!empty($userid)) {
        $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$username}'");
        $users = $wpdb->get_results("SELECT wp_users.ID,wp_users.user_login FROM `social_follow`,`wp_users`
                WHERE social_follow.sf_agent_id=wp_users.ID AND `sf_user_id` = {$wp_userid}", ARRAY_A);
        foreach ($users as &$user) {
            $user['timeline_url'] = home_url('/my-time-line/?user=' . $user['ID']);
            $user['photo'] = um_get_user_avatar_data($user['ID'])['url'];
        }
        $response['users'] = $users;
        http_response_code(200);
        $response['msg'] = 'success';
    } else {
        http_response_code(401);
        $response['msg'] = 'Incorrect token';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
