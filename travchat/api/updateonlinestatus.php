<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = check_input($_REQUEST['username']);
    $token = check_input($_REQUEST['token']);
    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);
    if (empty($user)) {
        http_response_code(401);
        echo json_encode(array("error_msg" => "ERROR"));
    } else {
        if (mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP() WHERE userid={$user['userid']}")) {
            http_response_code(200);
            echo json_encode(array("msg" => "update completed"));
        } else {
            http_response_code(401);
            echo json_encode(array("error_msg" => "Update failed"));
        }
    }
} else {
    http_response_code(401);
    echo json_encode(array("error_msg" => "add username"));
}
