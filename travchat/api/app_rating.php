<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $rating = filter_input(INPUT_POST, 'app_rating', FILTER_SANITIZE_STRING);
    $app_comment = filter_input(INPUT_POST, 'app_comment', FILTER_SANITIZE_STRING);

    $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

    if (!empty($userid) AND $rating<=5) {
        $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$username}'");

        $check_for_rating = $wpdb->get_var("SELECT COUNT(*) FROM `wp_app_rating` WHERE wp_user_ID='{$wp_userid}'");

        if ($check_for_rating > 0) {

            $wpdb->update(
            'wp_app_rating', array(
            'rating'=>$rating,
            'comment'=>$app_comment,

            ), array('wp_user_ID' => $wp_userid), array(
            '%f',
            '%s',

            ), array('%d')
            );

            http_response_code(200);
            $response['rated'] = 1;
            $response['msg'] = 'Rating Updated';
        } else {

            $sql = $wpdb->prepare("INSERT INTO `wp_app_rating` (`wp_user_ID`, `rating`,`comment`) values (%d, %f,%s)", $wp_userid, $rating, $app_comment);
            $wpdb->query($sql);
            http_response_code(200);
            $response['rated'] = 1;
            $response['msg'] = 'Rated Successfully';
        }
    } 

    elseif( $rating>5){
         http_response_code(401);
        $response['msg'] = 'Rating value can not be more than 5';
    }

    else {
        http_response_code(401);
        $response['msg'] = 'Incorrect token';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
