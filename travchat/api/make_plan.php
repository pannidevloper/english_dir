<?php
defined('BASEPATH') or exit('No direct script access allowed');

function get_friends_with_login_user_id($wp_userid, $msgg, $identifier)
{
    global $wpdb;
    $friends = $wpdb->get_results("SELECT ID FROM `wp_users` WHERE  ID IN(
                            SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_userid}')
                            OR ID IN(
                                SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_userid}')");
    foreach ($friends as $f) {
        $wpdb->insert('notification', array('wp_user_id' => $f->ID, 'content' => um_get_display_name($wp_userid) . " " . $msgg,'identifier'=>$identifier));

        $msg =  um_get_display_name($wp_userid) . " " . $msgg;
        $token =  to_get_user_device_token_from_wp_id_for_api($f->ID);

        if ($token) {
            notification_for_user_form_api($token, $msg, 'make_plan');
        }
    }
}

if (isTheseParametersAvailable(array('user_name', 'token', 'plan_start_date', 'plan_end_date', 'days', 'location', 'lat', 'lng'))) {

    $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $plan_start_date = filter_input(INPUT_POST, 'plan_start_date', FILTER_SANITIZE_STRING);
    $plan_end_date = filter_input(INPUT_POST, 'plan_end_date', FILTER_SANITIZE_STRING);
    $days = filter_input(INPUT_POST, 'days', FILTER_SANITIZE_STRING);
    $location = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING);
    $lat = filter_input(INPUT_POST, 'lat', FILTER_SANITIZE_STRING);
    $lng = filter_input(INPUT_POST, 'lng', FILTER_SANITIZE_STRING);
    $given_name = filter_input(INPUT_POST, 'given_name', FILTER_SANITIZE_STRING);
    $notes = filter_input(INPUT_POST, 'notes', FILTER_SANITIZE_STRING);
    // $time = filter_input(INPUT_POST, 'time', FILTER_SANITIZE_STRING);

    //user verifaction
    $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$user_name}' AND token='{$token}'");

    if (!empty($userid)) {

        $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");

        //insert into database here

        $result =    $wpdb->insert(
            'wp_user_plans',
            array(
                'user_id' => $wp_userid,
                'user_name' => $user_name,
                'start_date' => $plan_start_date,
                'end_Date' => $plan_end_date,
                'no_of_days' => $days,
                'location' => $location,
                'lat' => $lat,
                'lng' => $lng,
                 'name'=>$given_name,
                 'notes'=>$notes
            ),
            array(
                '%d',
                '%s',
                '%s',
                '%s',
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s'
            )
        );

        if ($result == 1) {
            get_friends_with_login_user_id($wp_userid, ' created a  Travel plan', 'make_plan');
            http_response_code(200);

            $response['msg'] = "Successfully added";
        } else {
            http_response_code(200);
            $response['msg'] = "Some error occured!";
        }
    } else {

        http_response_code(401);
        $response['msg'] = 'Wrong Username and password/Token';
    }
} else {

    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}
