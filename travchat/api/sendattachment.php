<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'member_userid', 'token')) || empty($_FILES)) {

    $tmp_name = $_FILES['file']['tmp_name'];
    $file_name = $_FILES['file']['name'];
    $size = $_FILES['file']['size'];
    //$type = $_FILES['file']['type'];
    $type = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $tmp_name);
    $error = 0;
    $token = check_input($_POST['token']);
    $user_name = check_input($_POST['username']);


    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$user_name}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);

    if (!empty($user)) {
        $from = $user['userid'];
        $to = check_input($_POST['member_userid']);

        if (mysqli_fetch_array(mysqli_query($conn, "SELECT COUNT(*) FROM chat_member WHERE (userid={$from} AND chatroomid={$to}) OR (userid={$to} AND chatroomid={$from})"))[0] <= 0) {
            mysqli_query($conn, "insert into chat_member (userid, chatroomid) values ('{$from}','{$to}')");
            mysqli_query($conn, "insert into chat_member (userid, chatroomid) values ('{$to}','{$from}')");
        }

        $folder = "../upload/" . $user_name;

        if (!is_dir($folder)) {
            mkdir($folder, 0700, true);
        }

        $upload_file_full_path = $folder . "/" . $file_name;

        if (move_uploaded_file($tmp_name, $upload_file_full_path)) {

            $msg = $user_name . "/" . $file_name;

            $sql = "insert into `chat` (message, userid, chat_date,c_to,c_from,type,attachment_type) values ('" . $msg . "' , '" . $from . "', NOW(),'" . $to . "','" . $from . "','attachment','" . $type . "')";

            $result = mysqli_query($conn, $sql);

            if (explode('/', $type)[0] == 'image')
                $message_content = 'https://www.travpart.com/English/travchat/upload/' . $msg;
            else
                $message_content = "https://www.travpart.com/English/travchat/user/download-attachment.php?file=" . rawurlencode($msg) . "&type={$type}";

            if ($result) {
                http_response_code(200);
                // $response['error'] = false;
                $response['msg'] = 'Successfully send';
                $response['messages'] = array('date' => date('Y-m-d H:i:s'), 'uname' => $user_name, 'member_userid' => $to, 'type' => $type, 'message' => $message_content, 'unread' => 0);
            } else {
                http_response_code(401);
                // $response['error'] = true;
                $response['msg'] = 'Error in sending file';
            }
        } else {
            http_response_code(401);
            // $response['error'] = true;
            $response['msg'] = 'Error in sending file';
        }
    } else {

        http_response_code(401);
        // $response['error'] = true;
        $response['msg'] = 'Error in user';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
