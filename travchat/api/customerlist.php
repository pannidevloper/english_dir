<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $token = check_input($_REQUEST['token']);
    $username = $_REQUEST['username'];

    if (isset($_REQUEST['kw'])) {
        $kw =  $_REQUEST['kw'];
    }

    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}' AND access = 1 ");
    $user = mysqli_fetch_assoc($res);
    if (!empty($user)) {

        $userid = $user['userid'];
        //  $mem = array('999999');
        $um = mysqli_query($conn, "select * from `chat_member` where chatroomid='" . $userid . "'");

        while ($umrow = mysqli_fetch_array($um)) {
            $mem[] = $umrow['userid'];
        }
        $users = implode($mem, ',');

        $sql = " select * from `user`  where userid in (" . $users . ") AND userid NOT IN ('2','51','37','510') AND access = 2
                    ";
        if (isset($kw)) {
            $sql .= " AND username LIKE '%{$kw}%'";
        }
        $query = mysqli_query($conn, $sql);
        $tempArr = array();

        $index = 0;

        while ($row = mysqli_fetch_array($query)) {
            $index++;
            $is_active = false;
            if ($row['last_activity']) {
                $last_activity = new DateTime($row['last_activity']);
                $now = new DateTime();
                $is_active = $last_activity < $now && (($now->getTimestamp() - $last_activity->getTimestamp()) <= MAX_INACTIVITY_TIME);
            }

            $tempArr[] = array(
                'userid' => $row['userid'], 'name' => $row['uname'],
                'photo' => (empty($row['photo']) ? '' : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
                'phone' => $row['phone'], 'country' => $row['country'], 'region' => $row['region'], 'online' => $row['is_active'], 'last_activity' => $row['last_activity']
            );
        }

        http_response_code(200);
        $response['msg'] = "customer-list retrived successfully";
        $response['customerlist'] = $tempArr;
    } else {
        http_response_code(401);
        $response['msg'] = "username does not exists";
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
