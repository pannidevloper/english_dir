<?php
include('../conn.php');
require_once($_SERVER['DOCUMENT_ROOT'] . "/English/wp-load.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/English/wp-admin/includes/image.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/English/wp-admin/includes/file.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/English/wp-admin/includes/media.php');
require_once(get_stylesheet_directory() . '/geoip/geoip2.phar');
global $wpdb;

define('BASEPATH', dirname(__FILE__) . DIRECTORY_SEPARATOR);

$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
$response = array();
if (!empty($action)) {
    $path = BASEPATH . wp_basename(strtolower($action)) . '.php';
    if (file_exists($path)) {
        include($path);

        //update user location
        if (!empty($_REQUEST['username']) && !empty($_REQUEST['token'])) {
            $username = htmlspecialchars($_REQUEST['username']);
            $token = htmlspecialchars($_REQUEST['token']);

            if ($wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'") > 0) {
                if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else if ($_SERVER['REMOTE_ADDR'] != '') {
                    $ip = $_SERVER['REMOTE_ADDR'];
                } else {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                }

                if (filter_var($ip, FILTER_VALIDATE_IP, array('flags' => FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE))) {
                    $geoip = new GeoIp2\Database\Reader(get_stylesheet_directory() . '/geoip/GeoLite2-City.mmdb');
                    $geoipRecord = $geoip->city($ip);
                    if (!empty($geoipRecord->country->name)) {
                        if ($wpdb->get_var("SELECT region FROM `user` WHERE username='{$username}'") != $geoipRecord->city->name)
                            $wpdb->update(
                                'user',
                                array('country' => $geoipRecord->country->name, 'region' => $geoipRecord->city->name, 'timezone' => $geoipRecord->location->timeZone),
                                array('username' => $username)
                            );
                    }
                }
            }
        }
    } else {
        http_response_code(401);
        $response['msg'] = 'Invalid API Call';
    }
}

echo json_encode($response);


//general functions
function isTheseParametersAvailable($params)
{
    foreach ($params as $param) {
        if (!isset($_POST[$param])) {
            return false;
        }
    }

    return true;
}

function check_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
}

function sendmailtosuer($to, $subject, $message)
{
    $key = "Tt3At58P6ZoYJ0qhLvqdYyx21";
    $postdata = array(
        'to' => $to,
        'subject' => $subject,
        'message' => $message
    );
    $url = "https://www.tourfrombali.com/api.php?action=sendmail&key={$key}";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $data = curl_exec($ch);
    if (curl_errno($ch) || $data == FALSE) {
        curl_close($ch);
        return FALSE;
    } else {
        curl_close($ch);
        return TRUE;
    }
}

// Added on 17 Feb for sending mobile notifactions
function to_get_user_device_token_from_wp_id_for_api($tour_creator_id)
{
    global $wpdb;

    $get_user_data = $wpdb->get_row("SELECT * FROM `wp_users` where  ID= " . $tour_creator_id);
    $user_email = $get_user_data->user_email;

    if (!empty($user_email)) {

        $get_user_data_tabl2 = $wpdb->get_row("SELECT * FROM `user` where  email = '{$user_email}'");
        $user_id = $get_user_data_tabl2->userid;

        if (!empty($user_id)) {

            $get_user_data_tabl3 = $wpdb->get_row("SELECT * FROM `sessions` where  user_id = " . $user_id);
            $user_device_token = $get_user_data_tabl3->device_token;

            return $user_device_token;
        }
    }
}

// added push up notifaction new method by farhan
function notification_for_user_form_api($id, $msgg, $identifier)
{
    // API access key from Google API's Console
    define('API_ACCESS_KEY', 'AIzaSyCUhKe-YxeR_gBYlGnhiuxzphZPYMUPXos');

    $registrationIds = array($id);

    // prep the bundle
    $msg = array(
        'message'   => $msgg,
        'title'     => 'TravPart notification',
        //'subtitle'  => 'This is a subtitle. subtitle',
        // 'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
        'vibrate'   => 1,
        'sound'     => 1,
        'largeIcon' => 'large_icon',
        'smallIcon' => 'small_icon',
        'url' => 'google.com',
        'identifier' => $identifier
    );

    $fields = array(
        'registration_ids'  => $registrationIds,
        'data'          => $msg
    );

    $headers = array(
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);

    //echo $result;
}

function API_User_distance($lat1, $lon1, $lat2, $lon2, $unit)
{
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
    } else {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}

function user_login_for_social($username, $useremail, $avatar_url = null)
{

    global $wpdb;
    global $conn;
    $row = $wpdb->get_row("SELECT wp_users.ID as wp_user_id, user.* from `wp_users`,`user` WHERE email ='{$useremail}' AND user.username=wp_users.user_login", ARRAY_A);

    //update profile photo
    if (!empty($avatar_url) && !empty($row['wp_user_id'])) {
        $profile_photo = file_get_contents($avatar_url);
        if (!empty($profile_photo)) {
            um_upload_profile_photo($profile_photo, $row['wp_user_id']);
        }
    }

    $token = md5($username);
    $userid = (int) $row['userid'];
    $wpdb->update('user', array('token' => $token), array('userid' => $userid));
    mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");
    if ($row) {

        //notifaction

        $profile_completeness =  get_user_meta($row['wp_user_id'], 'profile_completeness', true);
        $profile_completeness_value =  $profile_completeness * 100;

        $device_token =   to_get_user_device_token_from_wp_id_for_api($row['wp_user_id']);
        if ($profile_completeness_value < 50) {
            $msg2 = "<a href='https://www.travpart.com/English/user/?profiletab=profile'>Your profile is still incomplete, finish your profile now so your friends can find you</a>";

            $wpdb->insert('notification', array('wp_user_id' => $row['wp_user_id'], 'content' => $msg2, 'identifier' => 'incomplete_profile'));

            if ($device_token) {

                notification_for_user_form_api($device_token, "Your profile is still incomplete, finish your profile now so your friends can find you", 'incomplete_profile');
            }
        }

        // sessions


        $ip_address = $_SERVER['REMOTE_ADDR'];
        $now = date("Y-m-d H:i:s");
        $user_id = $row['userid'];
        $user_id = (int) $user_id;
        $wp_user_id = $row['wp_user_id'];
        //   $session_check =  mysqli_query($conn2, "SELECT * from  `sessions`   WHERE user_id='{$user_id}'");

        $session_check = $wpdb->get_var("SELECT user_id FROM `sessions` WHERE  user_id='{$user_id}'");

        if (empty($session_check)) {
            $sessions_query  = "INSERT INTO `sessions` (user_id, ip_address,last_activity,wp_user_id) VALUES ( {$user_id}, '{$ip_address}','{$now}','{$wp_user_id}')";
        } else {
            $sessions_query = "UPDATE `sessions` SET last_activity  = '{$now}',wp_user_id='{$wp_user_id}' WHERE `user_id` = {$user_id}";
        }

        $result = mysqli_query($conn, $sessions_query);
        $user = array(
            'userid' => $row['userid'],
            'username' => $row['username'],
            'email' => $row['email'],
            'phone' => $row['phone'],
            'uname' => $row['uname'],
            'photo' => (empty($row['photo']) ? um_get_user_avatar_url($row['wp_user_id'], 80) : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
            'country' => $row['country'],
            'region' => $row['region'],
            'timezone' => $row['timezone'],
            'access' => $row['access'],
            'token' => $token
        );
    } else {
        $user = "no user";
    }

    return $user;
}

function um_upload_profile_photo($image, $wp_user_id)
{
    rmdir(UM()->uploader()->get_upload_base_dir() . $wp_user_id);
    $path = UM()->uploader()->get_upload_user_base_dir($wp_user_id, true) . DIRECTORY_SEPARATOR . "profile_photo.jpg";
    file_put_contents($path, $image);
    update_user_meta($wp_user_id, 'profile_photo', wp_basename($path));
    UM()->user()->remove_cache($wp_user_id);
}
