<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('user_name', 'token'))) {

    $today_date = date("d/m/Y H:i:s");
    $today_date =date ("d-m-Y");
    $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);

    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$user_name}' AND token='{$token}'");

    if (!empty($userid)) {

        $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");

        $plans = $wpdb->get_results("SELECT * FROM `wp_user_plans` WHERE   user_id IN(
                    SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_userid}')
                    OR user_id IN(
                        SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_userid}')");

        if ($plans) {

            // 1st user get friends
         // friends of 1st
        $connections = $wpdb->get_results(
        "SELECT * FROM friends_requests WHERE  (user2 = '{$wp_userid}' OR  user1 = '{$wp_userid}') AND status=1" );
        foreach ($connections as $p1) {

        if ($p1->user1 == $wp_userid){
        $friends_list1[] = $p1->user2;
        }
        else{
        $friends_list1[] = $p1->user1;
        }
        }
        if (($key = array_search($wp_userid,$friends_list1)) !== false) {
        unset($friends_list1[$key]);
        }
        



        foreach ($plans as $plan) {

        $date = explode('/', $plan->end_date);
        $converted_Date  = date('d-m-Y', strtotime(implode('-', array_reverse($date))));

        if ( strtotime( $today_date )> strtotime( $converted_Date )){
        continue;
        }

        if($plan)
        // 2nd user get friends

        $connections2 = $wpdb->get_results(
        "SELECT * FROM friends_requests WHERE  (user2 = '{$plan->user_id}' OR  user1 = '{$plan->user_id}') AND status=1" );
        foreach ($connections2 as $p2) {

        if ($p2->user1 == $plan->user_id){
        $friends_list2[] = $p2->user2;
        }
        else{
        $friends_list2[] = $p2->user1;
        }
        }
            


        $match = array_intersect($friends_list1, $friends_list2);
        $match= array_unique ($match);
        $start_date_time = explode(" ", $plan->start_date);
        if (is_null($start_date_time[1])) {

        $start_date_time[1] = "00:00:00";
        }


        $end_date_time = explode(" ", $plan->end_date);

        if (is_null($end_date_time[1])) {
        $end_date_time[1] = "00:00:00";
        }

        foreach ($match as $match_id) {

        $wp_user_name= $wpdb->get_var("SELECT user_login FROM `wp_users` WHERE ID='{$match_id}'");

        $user_data[] =array(
        'user_id'=>$match_id,
        'username'=>um_get_display_name($match_id),
        'avatar'=> um_get_user_avatar_url($match_id, 80)
        );

        }

        //check user status either joined or not


        $already_join = $wpdb->get_var("SELECT wp_user_id FROM `wp_join_plan` WHERE plan_id='{$plan->id}' AND wp_user_id ='{$wp_userid}'"); 
        if($already_join){

        $status='Joined';

        }else{
        $status='Join';
        }


        $plans_list[] = array(
        'start_date' => $start_date_time[0],
        'end_Date' => $end_date_time[0],
        'days' => $plan->no_of_days,
        'location' => $plan->location,
        'plan_id' => $plan->id,
        'user_id' => $plan->user_id,
        'friend_name' => um_get_display_name($plan->user_id),
        'avatar' => um_get_user_avatar_url($plan->user_id, 80),
        'coordiantes' => $plan->lat . "," . $plan->lng,
        'mut_friends_count' => count($match),
        'mut_friends_data' => $user_data,
        'start_time' => $start_date_time[1],
        'end_time' => $end_date_time[1],
         'join_status'=>$status,
         'given_name'=>$plan->given_name,
          'notes'=>$plan->notes
        // 'd'=>$wp_user_id
        );

                unset($friends_list2);
               
            }

            http_response_code(200);
            $response['msg'] = "successfully ";
            $response['k'] = $plans_list;
        } else {

            http_response_code(200);
            $response['msg'] = "No plans available ";
        }
    } else {

        http_response_code(200);
        $response['msg'] = "No user exist";
    }
} else {


    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}
