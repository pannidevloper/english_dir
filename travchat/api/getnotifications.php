<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
    if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {
        $notificationslist = $wpdb->get_results("SELECT * FROM notification WHERE wp_user_id='{$wp_user_ID}' AND read_status=0 ORDER BY id DESC");

        foreach ($notificationslist as $single) {
            /*
    $url = $single->content;
        $notifications[] = array(
            'id'=>$single->id,
            'content'=>$single->content,

        );
        */

            $string = $single->content;

            preg_match('/\>(.*)<\/a>/', $string, $matches);
            $noti_text = $matches[1];
            preg_match('/href=(["\'])([^\1]*)\1/i', $string, $m);

            $list[] =
                array(
                    'id' => $single->id,
                    'text' => !empty($noti_text) ? $noti_text : $string,
                    'url' => $m[2],
                    'time' => $single->create_time,
                    'status' => 0,
                    'identifier' => $single->identifier
                );
        }
        http_response_code(200);
        $response['msg'] = "notifications retrived successfully";
        $response['notifications'] = $list;
    } else {
        http_response_code(401);
        $response['msg'] = "username does not exists";
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
