<?php
defined('BASEPATH') or exit('No direct script access allowed');

$options = get_option('_travpart_vo_option');
$list = $wpdb->get_results("SELECT wp_users.ID,wp_users.user_nicename,wp_coupons.* FROM wp_coupons,wp_users WHERE wp_coupons.user_id=wp_users.ID AND DATE_SUB(CURDATE(), INTERVAL {$options['tp_coupon_display_kept_day']} DAY)<=DATE(get_time) ORDER BY wp_coupons.id DESC LIMIT 3", ARRAY_A);
$result = array();
foreach ($list as $row) {
    $tmp['username'] = $row['user_nicename'];
    $tmp['avatar'] = um_get_user_avatar_url($row['ID'], 80);
    $tmp['discount'] = ($row['discount'] == '%' ? $row['figure'] . $row['discount'] : $row['discount'] . $row['figure']);
    $tmp['type'] = $row['type'];
    $result[] = $tmp;
}
if (!empty($result)) {
    http_response_code(200);
    $response['result'] = $result;
} else {
    http_response_code(401);
    $response['msg'] = 'There is not any voucher';
}
