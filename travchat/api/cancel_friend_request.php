<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token', 'receiver_user_id'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $receiver_user_id =  filter_input(INPUT_POST, 'receiver_user_id', FILTER_SANITIZE_STRING);
    // check either receiver user id exist

    $user = get_userdata($receiver_user_id);



    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_ID and !empty($receiver_user_id) and $user != false) {

        $check_request_status = $wpdb->get_var("SELECT COUNT(*) FROM friends_requests WHERE user1 = '{$wp_user_ID}' AND user2 = '{$receiver_user_id}'");

        // $sql = $wpdb->prepare("INSERT INTO `friends_requests` (`user1`, `user2`,`action_by_user`) values (%d, %d,%d)", $wp_user_ID,$receiver_user_id,$wp_user_ID);

        if ($check_request_status > 0) {

            $wpdb->query(
                "DELETE  FROM friends_requests
    WHERE user1 = '{$wp_user_ID}' AND user2 ='{$receiver_user_id}'"

            );
            http_response_code(200);
            $response['success'] = 1;
            $response['msg'] = "Request cancled";
        } else {

            http_response_code(401);
            $response['success'] = 0;
            $response['msg'] = 'You have no connection request with user';
        }
    } else {

        http_response_code(401);
        $response['success'] = 0;
        $response['msg'] = 'Some error';
    }
} else {

    http_response_code(401);
    $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
}
