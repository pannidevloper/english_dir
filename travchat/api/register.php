<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('name', 'username', 'password', 'confirm_password', 'email', 'phone', 'signup_as'))) {

    $username = check_input($_REQUEST['username']);
    $pw1 = check_input($_REQUEST["password"]);
    $pw2 = check_input($_REQUEST["confirm_password"]);

    $nameforusrmail = $_REQUEST['name'];
    $emailforusrmail = $_REQUEST['email'];
    $phoneforusrmail = $_REQUEST['phone'];

    if (!preg_match("/^[a-zA-Z0-9_]*$/", $username)) {
        http_response_code(401);
        $response['msg'] = 'Invalid username';
    } else if (username_exists($username) || $wpdb->get_var("SELECT COUNT(*) from user where username = '{$username}'") > 0) {
        http_response_code(401);
        $response['msg'] = 'The username has been registered';
    } else if (!preg_match('!^.*(?=.{8,})(?=.*\d)(?=.*[A-Z]{1,})(?=.*[a-z]{1,}).*$!', $pw1)) {
        http_response_code(401);
        $response['msg'] = 'Invaild password. the password must have 8 characters or longer and include upper and lowercase letters and numbers.';
    } else if ($pw1 != $pw2) {
        http_response_code(401);
        $response['msg'] = 'password does not match';
    } else if (empty($_POST['email'])) {
        http_response_code(401);
        $response['msg'] = 'The email can\'t be empty';
    } else if (!is_email($_POST['email'])) {
        http_response_code(401);
        $response['msg'] = 'The email address is invaild';
    } else if (empty($_POST['phone'])) {
        http_response_code(401);
        $response['msg'] = 'The phone number can\'t be empty';
    } else {
        $fusername = $username;
        $email = empty($_REQUEST['email']) ? '' : check_input($_REQUEST['email']);
        $phone = empty($_REQUEST['phone']) ? '' : check_input($_REQUEST['tel-ext'] . check_input($_REQUEST['phone']));

        $res=$wpdb->get_row("SELECT username FROM `user` WHERE email='{$email}'", ARRAY_A);

        $numForPhone=$wpdb->get_var("SELECT count(*) FROM `user` WHERE phone='{$phone}'");

        if (!empty($res['username']) && $res['username']!='Guest') {
            http_response_code(401);
            $response['msg'] = 'Your e-mail id has been already registered.';
        } else if ($numForPhone > 0) {
            http_response_code(401);
            $response['msg'] = 'Your Phone has been already registered.';
        } else {

            $password = check_input($_REQUEST["password"]);
            $fpassword = md5($password);
            $fname = check_input($_REQUEST["username"]);
            $access = $_REQUEST['signup_as'];

            if ($access == 'Customer') {
                $access = 2;
            } else if ($access == 'Agent') {
                $access = 1;
            } else {
                $access = 2;
            }


            $activated = time() . rand(1, 99999999999999999999999999999999999999999) . time();
            $fileInfo = PATHINFO($_FILE["photo"]);

            if (empty($_FILE["photo"])) {
                $location = '';
            } else {
                // if ($fileInfo['extension'] == "jpg" OR $fileInfo['extension'] == "png") {
                $newFilename = $fileInfo['filename'] . "_" . time() . "." . $fileInfo['extension'];

                move_uploaded_file($_FILES["image"]["tmp_name"], "../upload/" . $newFilename);

                $location = "upload/" . $newFilename;
                // } else {
                // }
            }


            if (!empty($res['username']) && $res['username']=='Guest') {
                $wpdb->query("UPDATE `user` SET uname='{$nameforusrmail}',username='{$fusername}',password='{$fpassword}',access='{$access}',activated='{$activated}',photo='{$location}',phone='{$phone}' WHERE email='{$email}'");
            } else {
                $wpdb->query("insert into `user` (uname, username,email,password,access,activated,photo,phone) values ('$nameforusrmail', '$fusername','$email', '$fpassword', '$access','$activated','$location','$phone')");
            }

            if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_potential_sale_agent` WHERE email='{$email}'") >= 1) {
                $wpdb->delete("wp_potential_sale_agent", array('email' => $email));
            }
            
            $mail_content = '<!DOCTYPE html><html><head><style type="text/css">
            .header_activation_div{padding: 40px;margin-left: 200px;margin-right: 200px;background-color: #27CD59;text-align: center;padding-bottom:0px;}
            .header_activation_inner_div{margin-left: 200px;margin-right: 200px;background-color: #EFEEEA;padding-bottom: 20px;}
            .header_activation_bottom_div{padding: 20px;margin-left:50px;margin-right:50px;background-color: #ffffff;}
            .header_activation_inner_inner_div{margin-left: 90px;margin-right: 90px;padding: 20px;background-color: #FFFFFF;padding-top:0px;}
            @media only screen and (max-width: 600px) {.header_activation_div{margin-left: 10px;margin-right: 10px;}.header_activation_inner_div{margin-left: 10px;margin-right: 10px;}.header_activation_bottom_div{margin-left:0px;margin-right:0px;}.header_activation_inner_inner_div{margin-left: 40px;margin-right: 40px;}}
            </style></head><body><div><div class="header_activation_div">
            <img src="https://www.travpart.com/wp-content/uploads/2020/06/travpart_logo_mail_new.jpg" style="width: 60px;height: 60px;margin-bottom:10px;">
            <div class="header_activation_bottom_div"></div></div>
            <div class="header_activation_inner_div">
            <div class="header_activation_inner_inner_div">
            <p style="text-align: center;font-size: 15px;margin-bottom: 5px;margin-top:0px;">We are glad you are here,</p>
              <h3 style="text-align: center;margin-top: 5px;">'.$nameforusrmail.'</h3>
              <div style="text-align: center;padding-top: 20px;">
              <a style="background-color: #007C89;color:white;padding:12px;border:none;text-decoration:none;" href="http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '">Activate Account</a></div>
              <p style="text-align: center;padding-top: 10px;">Or copy the link to the browser to open:</p>
              <div style="text-align: center;"><a href="http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '">http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '</a></div>
              <p style="text-align: center;">If you did not register in travpart,please ignore</p>
              <p style="margin-top: 50px;text-align: center;color: gray;margin-bottom: 5px;">© Travpart, All Rights Reserved</p>
              <p style="text-align: center;color: gray;margin-top: 5px;">Jakarta,13630 Indonesia</p>
              <div style="text-align: center;"><a href="https://www.travpart.com/English/contact/" style="text-decoration: none;"><span>Contact Us</span></a>
              <span>  -  </span><a href="https://www.travpart.com/English/termsofuse/" style="text-decoration: none;"><span>Terms of Use</span></a></div>
              </div></div>
            </div></body></html>';

            sendmailtosuer($email, 'Travchat - Please activate your account', $mail_content);

            $num2=$wpdb->get_var("SELECT count(*) FROM `user` WHERE email='{$email}' and activated=1");
            if ($num2 > 0) {
                http_response_code(200);
                $response['msg'] = 'Sign up successful. You may login now!';
            } else {
                http_response_code(200);
                $response['msg'] = 'You need to activate the link in your e-mail !';
            }
        }
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
