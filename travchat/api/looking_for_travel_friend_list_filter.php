<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $distance = filter_input(INPUT_POST, 'distance', FILTER_VALIDATE_INT, array('options' => array('default' => 0, 'min_range' => 0)));
    $hometown = filter_input(INPUT_POST, 'hometown', FILTER_SANITIZE_STRING);
    $age_min = filter_input(INPUT_POST, 'age_min', FILTER_VALIDATE_INT, array('options' => array('default' => 17, 'min_range' => 17, 'max_range' => 65)));
    $age_max = filter_input(INPUT_POST, 'age_max', FILTER_VALIDATE_INT, array('options' => array('default' => 65, 'min_range' => 17, 'max_range' => 65)));
    $gender = filter_input(INPUT_POST, 'gender', FILTER_SANITIZE_STRING);
    $gender = (strtolower($gender) == 'female') ? 'Female' : 'Male';
    $marital_status = strtolower(filter_input(INPUT_POST, 'marital_status', FILTER_SANITIZE_STRING));
    $marital_status = empty($marital_status) ? 'single' : $marital_status;
    $star_min = filter_input(INPUT_POST, 'star_min', FILTER_VALIDATE_INT, array('options' => array('default' => 0, 'min_range' => 0, 'max_range' => 5)));

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($wp_user_ID)) {

        $lat = 0.0;
        $lng = 0.0;

        $user_location = $wpdb->get_row("SELECT lat, lng FROM user_location WHERE ID = {$wp_user_ID}");
        if (!empty($user_location)) {
            $lat = $user_location->lat;
            $lng = $user_location->lng;
        }

        $baseQuery = "SELECT wp_users.ID, user_login as username, (6371 * ACOS(COS(RADIANS({$lat})) * COS(RADIANS(lat)) * 
                        COS(RADIANS(lng) - RADIANS({$lng})) + SIN(RADIANS({$lat})) * SIN(RADIANS(lat)))) AS distance
                        FROM wp_users LEFT JOIN user_location ON user_location.ID = wp_users.ID
                        WHERE wp_users.ID!={$wp_user_ID}
                        AND wp_users.ID IN(
                            SELECT wp_posts.post_author FROM `wp_posts`,`wp_postmeta`
                            WHERE wp_posts.ID=wp_postmeta.post_id AND meta_key='lookingfriend' AND meta_value=1
                        )";
        $filterQuery = "";

        if (!empty($hometown))
            $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'hometown' AND `meta_value` LIKE '%{$hometown}%')";

        $filterQuery .= " AND wp_users.ID IN(
                        SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'date_of_birth'
                        AND TIMESTAMPDIFF(YEAR, meta_value, CURDATE()) BETWEEN {$age_min} AND {$age_max}
                    )";

        $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'gender' AND `meta_value`='{$gender}')";
        $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'marital_status' AND `meta_value`='{$marital_status}')";
        $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `social_reviews` GROUP BY user_id HAVING(AVG(rating)>={$star_min}))";

        if ($distance > 0)
            $filterQuery .= " HAVING(distance<{$distance})";

        $query = $baseQuery . $filterQuery . " ORDER BY distance";

        $list = $wpdb->get_results($query, ARRAY_A);
        foreach ($list as &$row) {
            $row['name'] = um_get_display_name($row['ID']);
            $row['avatar'] = um_get_user_avatar_url($row['ID'], 'original');
            $connect_status = $wpdb->get_var("SELECT status FROM `friends_requests` WHERE (user1='{$wp_user_ID}' AND user2='{$row['ID']}') OR (user1='{$row['ID']}' AND user2='{$wp_user_ID}')");
            $row['connect_status'] = is_null($connect_status) ? 'unconnected' : ($connect_status == 0 ? 'sent' : 'connected');
            $row['hometown'] = get_user_meta($row['ID'], 'hometown', true);
            $row['current_city'] = get_user_meta($row['ID'], 'current_city', true);
            $row['edu_level'] = get_user_meta($row['ID'], 'edu_level', true);
        }
        if (!empty($list)) {
            http_response_code(200);
            $response['list'] = $list;
        } else {
            http_response_code(401);
            $response['msg'] = 'There is not any user who is looking for travel friends.';
        }
    } else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
