<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token', 'rejected_user_id'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $rejected_user_id = filter_input(INPUT_POST, 'rejected_user_id', FILTER_VALIDATE_INT, array('options' => array('default' => 1, 'min_range' => 1)));

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($wp_user_ID)) {
        $wpdb->insert(
            'match_reject',
            array('rejected_user_id' => $rejected_user_id, 'user_id' => $wp_user_ID)
        );
        http_response_code(200);
        $response['msg'] = 'success';
    } else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
