<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    // $kw = filter_input(INPUT_POST, 'kw', FILTER_SANITIZE_STRING);


    $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

    if (!empty($userid)) {

        $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$username}'");
        $friends = $wpdb->get_results("SELECT ID,user_nicename FROM `wp_users` WHERE  ID IN(
                    SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_userid}')
                    OR ID IN(
                        SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_userid}')");

        foreach ($friends as $value) {

            $location = $wpdb->get_row("SELECT * FROM `user_location` WHERE ID='{$value->ID}'");

            if ($location) {
                $lat = $location->lat;
                $Lng = $location->lng;
            }

            $list[] = array(

                'userid' => $value->ID,
                'username' => $value->user_nicename,
                'hometown' => get_user_meta($value->ID, 'hometown', true),
                'photo' => esc_url(get_avatar_url($value->ID)),
                'coordiantes' => $lat . "," . $Lng

            );
        }
        $response['msg'] = "retrived";
        $response['list'] = $list;
    } else {

        http_response_code(401);
        $response['msg'] = 'User name or token wrong';
    }
} else {

    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}
