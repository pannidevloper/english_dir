<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_ID) {
        $count = $wpdb->get_var("SELECT COUNT(*) FROM short_posts_hide_from_timeline WHERE user_id='{$wp_user_ID}'");
        http_response_code(200);
        $response['success'] = true;
        $response['count'] = $count;
    } else {
        http_response_code(400);
        $response['success'] = false;
        $response['msg'] = 'No user found';
    }
} else {
    http_response_code(401);
    $response['success'] = false;
    $response['msg'] = 'Required parameter missing';
}
