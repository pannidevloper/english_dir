<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (isTheseParametersAvailable(array('username','token','plan_id'))) {

	$plan_id = filter_input(INPUT_POST, 'plan_id', FILTER_SANITIZE_STRING);
	$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);


	$wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

	if ($wp_user_ID) {

		$get_plan = $wpdb->get_var("SELECT user_id FROM `wp_user_plans` WHERE id='{$plan_id}'"); 

		if($get_plan && $get_plan!=$wp_user_ID){

		$already_join = $wpdb->get_var("SELECT wp_user_id FROM `wp_join_plan` WHERE plan_id='{$plan_id}'"); 

		if($already_join){

		http_response_code(401);
        $response['msg'] = 'You have already Joined this Plan!';
		}
		else{

		$join_plan = $wpdb->prepare("INSERT INTO `wp_join_plan` (`plan_id`,`wp_user_id`) values (%d, %d)", $plan_id,$wp_user_ID);

        $wpdb->query($join_plan);

        $wpdb->insert('notification', array('wp_user_id' => $get_plan, 'content' => um_get_display_name($wp_user_ID) . " Joined your plan",'identifier'=>'Joined_plan_'.$plan_id));

        $token =  to_get_user_device_token_from_wp_id_for_api( $get_plan );

        if ($token) {
        	$msg =um_get_display_name($wp_user_ID) . " Joined your plan";
          notification_for_user_form_api($token, $msg, 'Joined_plan_'.$plan_id);
        }

        http_response_code(200);
        $response['msg'] = 'Successfully Joind';

		}

		}
		elseif($get_plan==$wp_user_ID){

		http_response_code(401);
        $response['msg'] = 'You can not join your plan';

		}
		else{

	    http_response_code(401);
        $response['msg'] = 'No Plan Found';

		}

	}
	else{
		 http_response_code(401);
        $response['msg'] = 'User does not exist';
	}

	
}
else{
	http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}