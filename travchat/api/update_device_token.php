<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('user_id', 'device_token'))) {


    $user_id = $_POST['user_id'];
    $device_token =  $_POST['device_token'];
    $query = " UPDATE `sessions` SET device_token = '{$device_token}' WHERE `user_id` = {$user_id}  ";
    $result = mysqli_query($conn, $query);
    http_response_code(200);
    $response['msg'] = "Updated";
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
