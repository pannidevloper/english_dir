<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    /// $search_term = check_input($_REQUEST['search']);
    $wp_user = $wpdb->get_row("SELECT wp_users.ID, wp_users.user_login FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
    $wp_user_ID = $wp_user->ID;

    if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {
        $friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE ID IN(
                SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_user_ID}')
                OR ID IN(
                    SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_user_ID}')");

        foreach ($friends as $f) {

            $check_for_location = $wpdb->get_row("SELECT * FROM `user_location` WHERE ID='{$f->ID}'");
            if ($check_for_location > 0) {
                $lat = $check_for_location->lat;
                $lng = $check_for_location->lng;
            } else {
                $lat = "Not found";
                $Lng = "Not found";
            }

            $location_tracker=get_user_meta($f->ID, 'location_tracker', true);
            $location_visiable=get_user_meta($f->ID, 'location_visiable', true);
            $family_member=get_user_meta($f->ID, 'family_member', true);
            $specific_person=get_user_meta($f->ID, 'specific_person', true);

            $location_status='off';
            if($location_tracker=='on') {
                if($location_visiable == 'family_friend' || $location_visiable == 'friend' || $location_visiable == 'anyone') {
                    $location_status='on';
                }
                else if($location_visiable == 'family') {
                    if(in_array($wp_user->user_login, explode(',',$family_member))) {
                        $location_status='on';
                    }
                }
                else if($location_visiable == 'specific_person') {
                    if(in_array($wp_user->user_login, explode(',',$specific_person))) {
                        $location_status='on';
                    }
                }
            }

            $list[] = array(
                'user_id' => $f->ID,
                'username' => um_get_display_name($f->ID),
                'education' => get_user_meta($f->ID, 'edu_level', true),
                'avator' =>   um_get_user_avatar_url($f->ID),
                'coordiantes' => $lat . "," . $lng,
                'home_town'=> get_user_meta( $f->ID, 'hometown' , true ),
                'status' => $location_status,
                'visiblity' => $location_visiable,

            );
        }

        $response['msg'] = "Successfully";
        $response['list'] = $list;
    } else {

        http_response_code(200);
        $response['msg'] = "No user exist";
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}
