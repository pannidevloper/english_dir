<?php
defined('BASEPATH') or exit('No direct script access allowed');

function search_kw_store($kw, $user = null, $token = null)
{
    global $wpdb;
    $get_search = $wpdb->get_row("SELECT id,count FROM wp_search_keywords WHERE kw LIKE '{$kw}' ");
    if (empty($get_search)) {
        $wpdb->insert(
            $wpdb->prefix . 'search_keywords',
            array(
                'kw' => $kw,
                'count' => 1
            ),
            array(
                '%s',
                '%d'
            )
        );
        $kw_id = $wpdb->insert_id;
    } else {
        $next_count = $get_search->count + 1;
        $wpdb->update(
            $wpdb->prefix . 'search_keywords',
            array(
                'count' => $next_count
            ),
            array('kw' => $kw),
            array(
                '%d'    // value2
            ),
            array('%s')
        );
        $kw_id = $get_search->id;
    }
    if (!empty($user) && !empty($token)) {
        $user_id = $wpdb->get_var("SELECT wp_users.ID FROM `user`,`wp_users` WHERE user.username='{$user}' AND user.token='{$token}' AND wp_users.user_login=user.username");
        if ($user_id > 0 && ($wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}user_trace
            WHERE user_id={$user_id} AND type='keyword' AND value='{$kw_id}'") == 0)) {
            $wpdb->insert(
                $wpdb->prefix . 'user_trace',
                array('user_id' => $user_id, 'type' => 'keyword', 'value' => $kw_id)
            );
        }
    }
}

$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
$token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);


$currency_usd = $wpdb->get_var("SELECT option_value FROM tourfrom_balieng2.wp_options WHERE option_name='_cs_currency_USD'");

// Site Url
$site_url = $wpdb->get_var("SELECT option_value FROM tourfrom_balieng2.wp_options WHERE option_name='siteurl'");
$search_price_range = check_input($_REQUEST['range']);
$search_days = check_input($_REQUEST['days']);
$search_date = check_input($_REQUEST['date_range']);
if (!empty($search_date)) {

    search_kw_store($search_date);

    $search_date_range = explode('--', $search_date);
    $date_lower_date = $search_date_range[0];
    $date_upper_date = $search_date_range[1];
}


if (!empty($search_days)) {
    search_kw_store($search_days);
}

if (!empty($search_price_range)) {
    search_kw_store($search_price_range);

    $search_price_range = explode('-', $search_price_range);
    $lower_limit = $search_price_range[0];

    $lower_limit = ceil($lower_limit / $currency_usd);

    $upper_limit = $search_price_range[1];
    $upper_limit = ceil($upper_limit / $currency_usd);
}

if (isTheseParametersAvailable(array('kw'))) {

    $search_term = check_input($_REQUEST['kw']);
    search_kw_store($search_term, $username, $token);
    $array_of_ids_in_search = array();

    //  selects all   starting with "term":
    $get_search = $wpdb->get_results("SELECT ID FROM tourfrom_balieng2.wp_posts WHERE  post_title LIKE '{$search_term}%'   AND post_status ='publish'");

    $row_count = $wpdb->num_rows;

    if ($row_count == 0 || $row_count == 1) {


        $get_search = $wpdb->get_results("SELECT ID FROM tourfrom_balieng2.wp_posts WHERE  post_title LIKE '%{$search_term}%' AND post_status ='publish' ");
        $row_count = $wpdb->num_rows;
    }

    if ($row_count == 0 || $row_count == 1) {


        $get_search = $wpdb->get_results("SELECT ID FROM tourfrom_balieng2.wp_posts WHERE  post_content LIKE '%{$search_term}%' OR post_title LIKE '%{$search_term}%' AND  post_status ='publish' ");
        $row_count = $wpdb->num_rows;
    }

    foreach ($get_search as $value) {
        // Store all posts ids
        $array_of_ids_in_search[] = $value->ID;
    }


    // main query

    $sql = "SELECT wp_tour.*,wp_tour_sale.number_of_people,wp_tour_sale.username,wp_tourmeta.meta_value
            FROM tourfrom_balieng2.wp_tour,tourfrom_balieng2.wp_tour_sale,tourfrom_balieng2.wp_tourmeta
            WHERE wp_tour.id=wp_tour_sale.tour_id
                AND wp_tourmeta.tour_id=wp_tour_sale.tour_id
                AND wp_tourmeta.meta_key='bookingdata'
                AND username!='agent2' AND username!='Lix'";

    if (!empty($search_price_range)) {

        $sql .= " AND total>={$lower_limit}";
        if ($upper_limit != 0)
            $sql .= " AND total<={$upper_limit}";
    }

    $sql .= " ORDER BY wp_tour.score  DESC";

    $data = $wpdb->get_results($sql);


    foreach ($data as $t) {

        // Getting Post Status
        $post_status = $wpdb->get_row("SELECT post_status FROM tourfrom_balieng2.wp_posts,tourfrom_balieng2.wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';")->post_status;

        if ($post_status != 'publish')
            continue;

        // Get the Posts data,like title and ID for further use if post is published
        $postdata = $wpdb->get_row("SELECT  ID,post_title FROM tourfrom_balieng2.wp_posts,tourfrom_balieng2.wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';");

        $post_ID = $postdata->ID;

        // Filter the search Term
        if (!empty($search_term)) {
            if (!in_array($post_ID, $array_of_ids_in_search))
                continue;
        }

        $images = array();

        // Get the Images from the post content

        /* $post_content=$wpdb->get_row("SELECT post_content FROM tourfrom_balieng2.wp_postmeta, tourfrom_balieng2.wp_posts WHERE wp_posts.ID=wp_postmeta.post_id AND `meta_key`='tour_id' AND `meta_value`='{$t->id}'")->post_content;
            */
        if (preg_match('!<img.*?src="(.*?)"!', $post_content, $post_image_match))
            $images[] = $post_image_match[1];

        $agentrow = $wpdb->get_row("SELECT userid,uname FROM tourfrom_balieng2.user WHERE username='{$t->username}'");

        $agent_id = $agentrow->userid;

        $agent_name = $agentrow->uname;

        if (empty($agent_name))

            $agent_name = $t->username;

        $bookingdata = unserialize($t->meta_value);

        $hotelinfo = $wpdb->get_row("SELECT img,ratingUrl,location,start_date,end_date FROM tourfrom_balieng2.wp_hotel WHERE tour_id='{$t->id}'");


        if (!empty($search_days)) {

            $bookingdata = unserialize($t->meta_value);

            $days_array = array(
                $bookingdata['tourlist_date'], $bookingdata['place_date'], $bookingdata['eaidate'],
                $bookingdata['boat_date'], $bookingdata['spa_date'], $bookingdata['meal_date'],
                $bookingdata['car_date'], $bookingdata['popupguide_date'], $bookingdata['spotdate'], $bookingdata['restaurant_date']
            );
            $manualitems = $wpdb->get_results("SELECT meta_value FROM tourfrom_balieng2.`wp_tourmeta` where meta_key='manualitem' and tour_id=" . $t->id);
            foreach ($manualitems as $manualItem) {
                $days_array[] = unserialize($manualItem->meta_value)['date'] . ',';
            }
            $days = max(array_map(function ($value) {
                return max(explode(',', $value));
            }, $days_array));

            if (intval($days) <= 0)
                $days = 1;
            if (!empty($search_days) && $days != $search_days)
                continue;
        }
        if (!empty($search_date)) {

            if (strtotime($hotelinfo->start_date) != strtotime($date_lower_date) and strtotime($hotelinfo->end_date) != strtotime($date_upper_date))
                continue;
        }


        if (!empty($hotelinfo->img)) {

            if (is_array(unserialize($hotelinfo->img))) {
                foreach (unserialize($hotelinfo->img) as $hotelImgItem)
                    $images[] = $hotelImgItem;
            } else
                $images[] = $hotelinfo->img;
        }
        $post_content = $wpdb->get_row("SELECT post_content FROM tourfrom_balieng2.wp_postmeta, tourfrom_balieng2.wp_posts WHERE wp_posts.ID=wp_postmeta.post_id AND `meta_key`='tour_id' AND `meta_value`='{$t->id}'")->post_content;
        if (preg_match('!<img.*?src="(.*?)"!', $post_content, $post_image_match))
            $images[] = $post_image_match[1];

        if (empty($images[0]))
            $images[0] = "https://i.travelapi.com/hotels/1000000/910000/903000/902911/26251f24_y.jpg";

        $cost = ceil((float) $t->total * $currency_usd);


        /// Filter for Days
        /*
            $days_array=array($bookingdata['tourlist_date'],$bookingdata['place_date'],$bookingdata['eaidate'],
            $bookingdata['boat_date'],$bookingdata['spa_date'],$bookingdata['meal_date'],
            $bookingdata['car_date'],$bookingdata['popupguide_date'],$bookingdata['spotdate'],$bookingdata['restaurant_date']);
            */
        /*
            $manualitems = $wpdb->get_results("SELECT meta_value FROM tourfrom_balieng2.`wp_tourmeta` where meta_key='manualitem' and tour_id=" . $t->id);
            foreach($manualitems as $manualItem) {
                $days_array[]=unserialize($manualItem->meta_value)['date'].',';
            }
            $days=max(array_map(function($value) { return max(explode(',', $value)); }, $days_array));

            if (intval($days) <= 0)
                $days = 1;
            */
        $data11[] = $t->id;
        $post_link = $site_url . "/?p=" . $post_ID;
        $data1[] = array('post_title' => $postdata->post_title, 'post_id' => $post_ID, 'bookingcode' => $t->id, 'location' => $hotelinfo->location, 'image' => $images[0], 'cost' => $cost, 'post_link' => $post_link);
    }



    if (empty($data1)) :
        http_response_code(401);
        $response['msg'] = 'NO data Found!';
    else :
        http_response_code(200);
        $response['data'] =  $data1;

    endif;
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
