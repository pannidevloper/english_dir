<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!empty($_GET['account'])) {
    $account = filter_input(INPUT_GET, 'account', FILTER_SANITIZE_STRING);
    if (is_email($account)) {
        $username_or_email = "email='$account'";
    } else {
        $username_or_email = "username='$account'";
    }
    if ($wpdb->get_var("SELECT COUNT(*) FROM `user` WHERE {$username_or_email} AND activated!='1'") == 1) {
        $userinfo = $wpdb->get_row("SELECT username,email,uname,phone,activated FROM `user` WHERE {$username_or_email} AND activated!='1'");
        $mail_content = 'Hello, ' . $userinfo->uname . ' <br>Welcome to Travchat! Please click on the link below to activate your account<br>
                            <div style="display:block;margin:22px 0px;">
                            <a style="background-color: #4caf50; color: white; padding: 12px; border: none; text-decoration: none;" href="http://www.travpart.com/English/travchat/activate.php?token=' . $userinfo->activated . '&time='.time().'">Activation</a>
                            </div>
                            <br>Or copy the link to the browser to open:<a href="http://www.travpart.com/English/travchat/activate.php?token=' . $userinfo->activated . '">http://www.travpart.com/English/travchat/activate.php?token=' . $userinfo->activated . '</a>
                            <div>
                                <p> Your E-mail is: ' . $userinfo->email . '</p>
                            <p> Your User Name is: ' . $userinfo->username . '</p>
                                <p> Your Phone is: ' . $userinfo->phone . '</p>
                            </div>
                            
                            <br><br>- Travchat team';

        sendmailtosuer($userinfo->email, 'Travchat - Please activate your account', $mail_content);
        http_response_code(200);
        $response['msg'] = "success";
    } else {
        http_response_code(401);
        $response['msg'] = 'The account has been activated';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Invalid parameters';
}
