<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token', 'tour_id', 'type'))) {

    $user_name = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $sc_tour_id = filter_input(INPUT_POST, 'tour_id', FILTER_SANITIZE_STRING);
    $sc_type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING);


    $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$user_name}' AND token='{$token}'");

    if (!empty($userid)) {

        $sc_user_id = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");
        //CHECK either before like
        $duplicateDataCheck = $wpdb->get_results("SELECT * FROM `social_connect` WHERE sc_tour_id = '$sc_tour_id' AND sc_user_id = '$sc_user_id'", ARRAY_A);
        $sc_created = date('Y-m-d H:i:s');
        if (!empty($duplicateDataCheck)) {
            /// get db row id
            $duplicateDataCheck = $duplicateDataCheck[0];
            $sc_id = isset($duplicateDataCheck['sc_id']) ? $duplicateDataCheck['sc_id'] : '';
            $r =  $wpdb->update(
                'social_connect',
                array(
                    'sc_tour_id' => $sc_tour_id,
                    'sc_type' => $sc_type,
                    'sc_user_id' => $sc_user_id,
                    'sc_created' => $sc_created,
                ),
                array('sc_id' => $sc_id),
                array(
                    '%d',
                    '%s',
                    '%d',
                    '%s',
                ),
                array('%d')
            );
        } else {

            $wpdb->insert(
                'social_connect',
                array(
                    'sc_tour_id' => $sc_tour_id,
                    'sc_type' => $sc_type,
                    'sc_user_id' => $sc_user_id,
                    'sc_created' => $sc_created,
                ),
                array(
                    '%d',
                    '%s',
                    '%d',
                    '%s',
                )
            );
        }
        http_response_code(200);
        $response['msg'] = "Success";
    } else {
        http_response_code(200);
        $response['msg'] = "No user exist";
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}
