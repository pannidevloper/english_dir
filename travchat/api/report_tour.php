<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {


    $username = check_input($_REQUEST['username']);
    $token = check_input($_REQUEST['token']);
    $tour_id = check_input($_REQUEST['tour_id']);
    $report_comment = check_input($_REQUEST['report_msg']);

    $wp_user_data = $wpdb->get_results("SELECT wp_users.user_email,wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_data) {
        foreach ($wp_user_data as $value) {

            $sf_user_id = $value->ID;
            $email = $value->user_email;
        }

        $tourData = $wpdb->get_results("SELECT * FROM `reported_tours` WHERE reported_tour_id = '$tour_id' AND report_by_user_id = '$sf_user_id'", ARRAY_A);
        if ($tourData) {
            $data['status'] = 1;
            $response['msg'] =  "You already Reported that Tour";
        } else {

            $wpdb->insert(
                'reported_tours',
                array(
                    'reported_tour_id' => $tour_id,
                    'report_by_user_id' => $sf_user_id,
                    'report_by_user_email' => $email,
                    'report_text' => $report_comment,

                ),
                array(
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                )
            );



            // send email

            $to = 'tourfrombali@gmail.com';
            $subject = 'Tour Reported';
            $body =   $email . " Reported the Tour with id: " . $tour_id . " Please Review the issue";
            $headers = array('Content-Type: text/html; charset=UTF-8');

            sendmailtosuer('tourfrombali@gmail.com', $subject, $body);

            $response['status'] = 1;
            $response['msg'] =  'You Reported Tour Successfully,Thank you !';
        }
    } else {
        http_response_code(401);
        $response['msg'] = 'No user Exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
