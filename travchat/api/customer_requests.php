<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {


    $username = check_input($_REQUEST['username']);
    $token = check_input($_REQUEST['token']);

    $wp_user_email = $wpdb->get_var("SELECT wp_users.user_email FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    $request_list = $wpdb->get_results("SELECT contact_sales_agent.*,user.userid  FROM `contact_sales_agent`,`user`  WHERE  `csa_user_name`=`user`.`username` AND csa_agent_email ='{$wp_user_email}'");
    if (!empty($request_list)) {

        foreach ($request_list as $single) {


            $list[] =
                array(
                    'id' => $single->csa_id,
                    'user_id' => $single->userid,
                    'username' => $single->csa_user_name,
                    'subject' => $single->csa_subject,
                    'plan' => $single->csa_destination_plan,
                    'msg' => $single->csa_message,
                    'cat' => $single->csa_question_cat,
                    'created' => $single->csa_created
                );
        }
        http_response_code(200);
        $response['msg'] = $list;
    } else {
        http_response_code(200);
        $response['msg'] = 'No data';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
