<?php
defined('BASEPATH') or exit('No direct script access allowed');

$app_version = get_option('app_version');

http_response_code(200);
$response = array(
    'version' => $app_version['version'],
    'force_update' => $app_version['force_update'],
    'download_link' => 'https://play.google.com/store/apps/details?id=com.travpart.english'
);
