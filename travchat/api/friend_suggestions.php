<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {

        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

        $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

        $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
        $user_id = $wp_user_ID;
        $edu_level = get_user_meta( $user_id, 'edu_level' , true );
        $religion = get_user_meta( $user_id, 'religion' , true );
        $hometown = get_user_meta( $user_id, 'hometown' , true );
        $current_city = get_user_meta( $user_id, 'current_city' , true );

        $user_education = $wpdb->get_results("SELECT school,degree FROM wp_education 
        WHERE  userid  ='$wp_user_ID' ");


    if ($wp_user_ID) {

        $query = "SELECT kw FROM user_friends_search_keywords WHERE user_id = {$wp_user_ID}";
        $searchs = $wpdb->get_results($query);
        $search_ids = array();

             foreach($searchs as $row){

                $user = get_user_by('login',$row->kw);

                if( $user) {

                $search_ids[] = $user->ID;

                }
           }

        // add education/school

                foreach ($user_education as $edu) {
                $same_school = $wpdb->get_row("SELECT userid FROM wp_education 
                WHERE  school Like '%{$edu->school}%' OR  degree Like '%{$edu->degree}%' AND userid!=$user_id");

                $realted_users_id[] = $same_school->userid;

                }

   
           $search_ids = array_merge ( $search_ids,$realted_users_id);



                //next

                if(empty ($search_ids)){


                $args = 

                array(
                'meta_query' => array(
                'relation' => 'OR',
                array(
                'key'     => 'edu_level',
                'value'   => $edu_level,
                'compare' => '='
                ),
                array(
                'key'     => 'hometown',
                'value'   => $hometown,
                'compare' => '='
                ),
                array(
                'key'     => 'current_city',
                'value'   => $current_city,
                'compare' => 'LIKE'
                )
                ),

                'exclude' => array($wp_user_ID),
                 );

                }
                else {

                $args = 

                array (
                'exclude' => array($wp_user_ID),
                'include'=>$search_ids,
                  );


                }

    // Main Query to fetch results


        if(function_exists('front_end_suggestions')){
        $suggestions = front_end_suggestions ($wp_user_ID);

        }else{
        $wp_user_query = new WP_User_Query( $args );

        $suggestions = $wp_user_query->get_results();

        }

        if(empty($suggestions) || count ($suggestions) <5 ){

        $wp_user_query = new WP_User_Query( $args );

        $suggestions = $wp_user_query->get_results();
        }

         foreach ($suggestions as $value) {


            
           // calculations for status,either they have sent friend request before/blocked
            $Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user1 = '$wp_user_ID' AND user2 = '{$value->ID}'");
            if(empty($Data)){

            $reverse_check = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '$wp_user_ID' AND user1 = '{$value->ID}'");

            if(!empty($reverse_check) AND isset($reverse_check)){

            $button_text ="Requested to connect with you";
            $button_class="pending_button";
            //var_dump($reverse_check);
            if($reverse_check->status==1){

                $button_text ="Connected";
                $button_class="connected_button";
            }

            }
            else{
            $button_text = 'Connect';
            $button_class="connect_button";
            }

            }
            else{
            if($Data->status == "0"){

            $button_text = "Connection Request sent";
            $button_class="requestsent_button";
            }
            elseif( $Data->status=="1"){

            $button_text = "Connected";
            $button_class="connected_button";
            }
            elseif( $Data->status=="2"){

            $button_text = "Blocked";
            $button_class="blocked_button";
            }
            }
        
    if( $value->ID!=$wp_user_ID AND empty($Data)){

    if( get_avatar_url( $value->ID) =="http://www.travpart.com/English/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg" || get_avatar_url( $value->ID) =="https://www.travpart.com/English/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg")
                    continue;

        $user = get_user_by('id',  $value->ID);
        $username = $user->user_login;
        $list[] = array(
                    'userid'=> $value->ID,
                    'username' => um_get_display_name($value->ID),
                    'hometown' => $value->region,
                    'photo' => um_get_user_avatar_url($value->ID),
                    'status' => $button_text,
                    'user_login_name' => $username,
                    
                   // 'list_username' => $contact_list_array[$i]['name']
                );
         }
    }

    
        http_response_code(200);
      //  $response['success'] = "success";
        $response['result'] =$list;
       
    } else {

        http_response_code(400);
        $response['success'] = 0;
        $response['msg'] = 'No user found';
    }
} else {

    http_response_code(401);
    $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
}
