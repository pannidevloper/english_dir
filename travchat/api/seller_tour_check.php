<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = check_input($_REQUEST['username']);
    $token = check_input($_REQUEST['token']);
    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
    if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {

        $sql = "SELECT  COUNT(*)
    FROM tourfrom_balieng2.wp_tour,tourfrom_balieng2.wp_tour_sale,tourfrom_balieng2.wp_tourmeta
    WHERE
    wp_tour.id=wp_tour_sale.tour_id
    AND wp_tourmeta.tour_id=wp_tour_sale.tour_id
    AND wp_tourmeta.meta_key='bookingdata'
    AND username ='$username'";
        $data = $wpdb->get_var($sql);
        http_response_code(200);
        $response['no_of_tours'] = $data;
        $response['msg'] = "success";
    } else {
        http_response_code(401);
        $response['msg'] = 'Invalid login';
    }
} else {

    http_response_code(401);
    $response['msg'] = 'Invalid parameters';
}
