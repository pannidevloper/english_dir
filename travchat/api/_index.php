<?php

include('../conn.php');
require_once("../../wp-load.php");

$response = array();

if (isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'register':

            if (isTheseParametersAvailable(array('name', 'username', 'password', 'confirm_password', 'email', 'phone', 'signup_as'))) {

                $username = check_input($_REQUEST['username']);
                $pw1 = check_input($_REQUEST["password"]);
                $pw2 = check_input($_REQUEST["confirm_password"]);

                $nameforusrmail = $_REQUEST['name'];
                $emailforusrmail = $_REQUEST['email'];
                $phoneforusrmail = $_REQUEST['phone'];


                if (!preg_match("/^[a-zA-Z0-9_]*$/", $username)) {
                    $response['error'] = 401;
                    $response['message'] = 'Invalid username';
                } else if ($pw1 != $pw2) {
                    $response['error'] = 401;
                    $response['message'] = 'password does not match';
                } else {
                    $fusername = $username;
                    $email = empty($_REQUEST['email']) ? '' : check_input($_REQUEST['email']);
                    $phone = empty($_REQUEST['phone']) ? '' : check_input($_REQUEST['tel-ext'] . check_input($_REQUEST['phone']));

                    $res = mysqli_query($conn, "SELECT count(*) FROM `user` WHERE email='{$email}'");
                    $num = mysqli_fetch_row($res)[0];

                    $resforphone = mysqli_query($conn, "SELECT count(*) FROM `user` WHERE phone='{$phone}'");
                    $numForPhone = mysqli_fetch_row($resforphone)[0];

                    if ($num > 0) {
                        $response['status'] = 401;
                        $response['msg'] = 'Your mail id has been already registered.';
                    } else if ($numForPhone > 0) {
                        $response['status'] = 401;
                        $response['msg'] = 'Your Phone has been already registered.';
                    } else {

                        $password = check_input($_REQUEST["password"]);
                        $fpassword = md5($password);
                        $fname = check_input($_REQUEST["username"]);
                        $access = $_REQUEST['signup_as'];

                        if ($access == 2) {
                            $access_text = 'Customer';
                        } else if ($access == 1) {
                            $access_text = 'Sales_Agent';
                        } else {
                            $access_text = 'Guest';
                        }

                        $activated = password_hash($password . time(), PASSWORD_DEFAULT);
                        $fileInfo = PATHINFO($_FILE["photo"]);

                        if (empty($_FILE["photo"])) {
                            $location = '';
                        } else {
                            if ($fileInfo['extension'] == "jpg" OR $fileInfo['extension'] == "png") {
                                $newFilename = $fileInfo['filename'] . "_" . time() . "." . $fileInfo['extension'];
                                move_uploaded_file($_FILES["image"]["tmp_name"], "upload/" . $newFilename);
                                $location = "upload/" . $newFilename;
                            } else {
                                
                            }
                        }

                        global $wpdb;

                        $wpdb->insert("wp_users", array(
                            "user_login" => $fusername,
                            "user_pass" => $fpassword,
                            "user_nicename" => $fname,
                            "user_email" => $email,
                            "user_url" => 'NULL',
                            "user_registered" => time(),
                            "user_activation_key" => '',
                            "user_status" => '0',
                            "display_name" => $fname
                        ));

                        $user = get_user_by('email', $email);
                        $user_id = $user->ID;

                        if ($access == 2) {
                            $u = new WP_User($user_id);
                            $u->set_role('um_clients');

                            $wpdb->insert("wp_usermeta", array(
                                "umeta_id" => 'NULL',
                                "user_id" => $user_id,
                                "meta_key" => 'wp_user_level',
                                "meta_value" => '10'
                            ));
                        } else {
                            $u = new WP_User($user_id);
                            $u->set_role('um_sales-agent');

                            $wpdb->insert("wp_usermeta", array(
                                "umeta_id" => 'NULL',
                                "user_id" => $user_id,
                                "meta_key" => 'wp_user_level',
                                "meta_value" => '5'
                            ));
                        }


                        mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated,photo,phone) values ('$fname', '$fusername','$email', '$fpassword', '$access','$activated','$location','$phone')");

                        $mail_content = 'Hello, ' . $nameforusrmail . ' <br>Welcome to Travchat! Please click on the link below to activate your account<br>
       								<div style="display:block;margin:22px 0px;">
       								<a style="background-color: #4caf50; color: white; padding: 12px; border: none; text-decoration: none;" href="http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '">Activation</a>
       								</div>
       								<br>Or copy the link to the browser to open:<a href="http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '">http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '</a>
       								<div>
       						    		<p> Your Mail is: ' . $emailforusrmail . '</p>
       						   		<p> Your User Name is: ' . $fusername . '</p>
       						    		<p> Your Phone is: ' . $phoneforusrmail . '</p>
       								</div>
       								
       								<br><br>- Travchat team';

                        sendmailtosuer($email, 'Travchat - Please activate your account', $mail_content);

                        $res2 = mysqli_query($conn, "SELECT count(*) FROM `user` WHERE email='{$email}' and activated=1");
                        $num2 = mysqli_fetch_row($res2)[0];
                        if ($num2 > 0) {
                            $response['status'] = 200;
                            $response['msg'] = 'Sign up successful. You may login now!';
                        } else {
                            $response['status'] = 200;
                            $response['msg'] = 'You need to activate the link in your mail !';
                        }
                    }
                }
            }

            break;

        case 'login':

            if (isTheseParametersAvailable(array('username', 'password'))) {

                $username = check_input($_REQUEST['username']);
                $now = date('Y-m-d H:i:s', time());

                if (!preg_match("/^[a-zA-Z0-9_]*$/", $username)) {
                    $response['status'] = 401;
                    $response['msg'] = 'Invalid username or password';
                } else {

                    $fusername = $username;
                    $password = check_input($_REQUEST["password"]);
                    $fpassword = md5($password);

                    $query = mysqli_query($conn, "select * from `user` where username='$fusername' and password='$fpassword' and activated=1");

                    if (mysqli_num_rows($query) == 0) {

                        $query2 = mysqli_query($conn2, "select * from `wp_users` where user_login='$fusername' and user_pass='$fpassword'");

                        if (mysqli_num_rows($query2) == 0) {
                            $response['status'] = 401;
                            $response['msg'] = 'Invalid username or password or account not activated';
                        } else {

                            $row2 = mysqli_fetch_array($query2);

                            $user = array(
                                'id' => $row2['ID'],
                                'username' => $row2['user_login'],
                                'email' => $row2['user_email'],
                                'display_name' => $row2['display_name']
                            );

                            if (mysqli_num_rows($result) === 0) {
                                $sessions_query = "
								        INSERT INTO `sessions` (user_id, ip_address, last_activity) VALUES ( {$row2['id']}, '{$_SERVER["REMOTE_ADDR"]}', {$now})
								    ";
                            } else {
                                $sessions_query = "
								        UPDATE `sessions` SET last_activity = now() WHERE `sessions`.`user_id` = {$row2['id']}
								    ";
                            }

                            $result = mysqli_query($conn, $sessions_query);

                            $response['status'] = 200;
                            $response['msg'] = 'Login successfull';
                            $response['user'] = $user;
                        }
                    } else {

                        $row = mysqli_fetch_array($query);
                        $user = array(
                            'id' => $row['userid'],
                            'username' => $row['username'],
                            'email' => $row['email'],
                            'photo' => $row['photo'],
                            'uname' => $row['uname'],
                            'access' => $row['access'],
                            'phone' => $row['phone'],
                            'country' => $row['country'],
                            'region' => $row['region'],
                            'timezone' => $row['timezone'],
                        );

                        $response['status'] = 200;
                        $response['msg'] = 'Login successfull';
                        $response['user'] = $user;
                    }
                }
            }
            break;

        case 'forgotpassword':

            if (isTheseParametersAvailable(array('email'))) {

                $email = empty($_REQUEST['email']) ? '' : check_input($_REQUEST['email']);

                $activated = password_hash($password . time(), PASSWORD_DEFAULT);

                $res = mysqli_query($conn, "SELECT count(*),username FROM `user` WHERE email='{$email}' and activated=1");

                while ($row = mysqli_fetch_row($res)) {
                    $num = $row[0];
                    $username = $row[1];
                }

                if ($num > 0) {
                    mysqli_query($conn, "update `user` set activated='{$activated}' where email='{$email}'");

                    $mail_content = 'Hello,<br>Welcome to Travchat! Please click on the link below to Reset your password<br>
				<div style="display:block;margin:22px 0px;">
		                <p>Your username : ' . $username . '</p></br>
				<a style="background-color: #4caf50; color: white; padding: 12px; border: none; text-decoration: none;" href="http://www.travpart.com/English/travchat/activatepass.php?token=' . $activated . '&status=1">Reset Password</a>
				</div>
				<br>Or copy the link to the browser to open:<a href="http://www.travpart.com/English/travchat/activatepass.php?token=' . $activated . '&status=1">http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '</a>
				<br><br>- Travchat team';

                    sendmailtosuer($email, 'Travchat - Please reset your password', $mail_content);

                    $response['status'] = 200;
                    $response['msg'] = 'You need to activate the link in your mail !';
                } else {
                    $response['status'] = 401;
                    $response['msg'] = 'Your mail id does not exist, please register.';
                }
            }

            break;


        case 'orderlist':

            // if(isTheseParametersAvailable(array('access', 'agentid'))){
            // 	if($_REQUEST['access'] == 1){
            // 		$temp = array();
            // 		$agent_id=$_REQUEST['agentid'];
            // 		$orders=mysqli_query($conn, "SELECT * FROM `orders` WHERE `agent_id` = {$agent_id}");
            // 		while($row = mysqli_fetch_array($orders, MYSQL_ASSOC)){
            // 			$bookcode = $row['bookingcode'];
            // 			$saleamt = $row['total_price'];
            // 			$commission = $row['total_price']*0.08;
            // 			$temp[$bookcode]=array('saleamt' => $saleamt, 'commission' => $commission);
            // 		}
            // 		$response['status'] = 200;
            // 		$response['data'] = $temp;
            // 	}else if($_REQUEST['access'] == 2){
            // 		$customer_id=$_REQUEST['agentid'];
            // 		$temp = array();
            // 		$orders=mysqli_query($conn, "SELECT orders.*, user.uname FROM orders,user WHERE orders.customer_id={$customer_id} AND user.userid=orders.agent_id");
            // 		while($row = mysqli_fetch_array($orders, MYSQL_ASSOC))
            // 		{
            // 			$temp[$row['bookingcode']]=array('nominalamt'=>$row['total_price'], 'agentname'=>$row['uname']);
            // 		}
            // 		$response['status'] = 200;
            // 		$response['data'] = $temp;
            // 	}else{
            // 		$response['status'] = 200;
            // 		$response['message'] = 'Provide access code';
            // 	}
            // }


            include('../../wp-config.php');

            if (empty($_REQUEST['username'])) {
                http_response_code(401);
                echo json_encode(array("error_msg" => "provide username"));
            } else {
                $username = check_input($_POST['username']);
                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}'");
                $user = mysqli_fetch_assoc($res);
                if (empty($user)) {
                    http_response_code(401);
                    echo json_encode(array("error_msg" => "No user found"));
                } else {
                    if ($user['access'] == 1) {
                        $agent_id = $user['userid'];
                        $orders = mysqli_query($conn, "SELECT * FROM `orders` WHERE `agent_id` = {$agent_id}");
                        $agent_name = $user['uname'];
                        $orderlist = array();
                        while ($row = mysqli_fetch_array($orders, MYSQL_ASSOC)) {
                            $confirm_payment = $wpdb->get_row("SELECT confirm_payment FROM `wp_tour` WHERE `id` = " . (int) substr($row['bookingcode'], 4))->confirm_payment;

                            if ($confirm_payment == 1)
                                $confirm_payment = TRUE;
                            else
                                $confirm_payment = FALSE;
                            array_push($orderlist, array('agentname' => $agent_name,
                                'bookingcode' => $row['bookingcode'],
                                'total_price' => round($row['total_price'], 2),
                                'commission' => round($row['total_price'] * 0.08, 2),
                                'confirm_payment' => $confirm_payment,
                                'invoice_link' => (($confirm_payment == TRUE) ? ('https://www.travpart.com/Chinese/wp-content/themes/bali/api.php?action=getinvoice&agent_name=' . $agent_name . '&tourid=' . (int) substr($row['bookingcode'], 4)) : '')
                            ));
                        }
                        http_response_code(200);
                        echo json_encode(array('msg' => 'success', 'orderlist' => $orderlist));
                    }
                }
            }

            break;



        case 'agentlist':

            if (isTheseParametersAvailable(array('username'))) {

                $username = $_REQUEST['username'];

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}'");
                $user = mysqli_fetch_assoc($res);
                if (!empty($user)) {

                    $userid = $user['userid'];
                    $mem = array('999999');
                    $um = mysqli_query($conn, "select * from `chat_member` where chatroomid='" . $userid . "'");

                    while ($umrow = mysqli_fetch_array($um)) {
                        $mem[] = $umrow['userid'];
                    }
                    $users = implode($mem, ',');

                    $sql = " select * from `user` 
	                        LEFT JOIN `sessions` ON
	                            `sessions`.`id` = (
	                                SELECT `id` from `sessions` where `user_id` = `user`.`userid` ORDER BY `id` DESC LIMIT 1
	                            )
	                            where userid not in (" . $users . ") AND access = 1
	                        ";
                    $query = mysqli_query($conn, $sql);
                    $tempArr = array();

                    $index = 0;

                    while ($row = mysqli_fetch_array($query)) {
                        $index++;
                        $is_active = false;
                        if ($row['last_activity']) {
                            $last_activity = new DateTime($row['last_activity']);
                            $now = new DateTime();
                            $is_active = $last_activity < $now && ( ($now->getTimestamp() - $last_activity->getTimestamp()) <= MAX_INACTIVITY_TIME );
                        }

                        $tempArr[$index] = array('userid' => $row['userid'], 'username' => $row['uname'], 'isactive' => $is_active, 'phone' => $row['phone'], 'country' => $row['country'], 'photo' => $row['photo']);
                    }

                    $response['status'] = 200;
                    $response['agentlist'] = $tempArr;
                }
            }

            break;


        case 'faqlist':

            if (isTheseParametersAvailable(array('username'))) {

                $username = $_REQUEST['username'];

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}'");
                $user = mysqli_fetch_assoc($res);
                if (!empty($user)) {

                    $url = 'https://www.tourfrombali.com/faq/';
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                    $data = curl_exec($ch);
                    if (curl_errno($ch)) {
                        echo FALSE;
                    } else {
                        preg_match_all('!<div class="faqwd_answer">(.+?).{0,1}</div>!is', $data, $faq_answer);
                        preg_match_all('!<span class="faqwd_post_title".*?>(.+?)</span>!is', $data, $faq_title);

                        if (!empty($faq_title[1]) && count($faq_answer[1]) == count($faq_title[1])) {
                            $faq = '';
                            $temp = array();
                            for ($i = 0; $i < count($faq_title[1]); $i++) {

                                $title = strip_tags($faq_title[1][$i]);
                                $answer = strip_tags($faq_answer[1][$i]);

                                array_push($temp, array("title" => $title, "answer" => $answer));
                            }

                            $response['status'] = 200;
                            $response['faq'] = $temp;
                        } else {
                            $response['status'] = 401;
                        }
                    }

                    curl_close($ch);
                } else {
                    $response['status'] = 401;
                    $response['msg'] = 'Incorrect Username';
                }
            } else {
                $response['status'] = 401;
                $response['msg'] = 'Send username';
            }

            break;


        case 'sendmessage':

            if (isTheseParametersAvailable(array('username', 'member_userid', 'msg'))) {

                $msg = $_REQUEST['msg'];
                $username = check_input($_REQUEST['username']);

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}'");
                $user = mysqli_fetch_assoc($res);
                if (empty($user)) {
                    http_response_code(401);
                    echo json_encode(array("error_msg" => "no user found"));
                } else {
                    $from = $user['userid'];
                    $to = $_REQUEST['member_userid'];

                    $sql = "insert into `chat` (message, userid, chat_date,c_to,c_from,type,attachment_type) values ('{$msg}' , '" . $from . "', NOW(),'{$to}','" . $from . "','','')";

                    if (mysqli_query($conn, $sql)) {
                        $response['status'] = 200;
                        $response['msg'] = 'Sent';
                    }
                }
            }

            break;


        case 'chatmember':

            if (isTheseParametersAvailable(array('username'))) {

                $username = $_REQUEST['username'];

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}'");
                $user = mysqli_fetch_assoc($res);
                if (empty($user)) {
                    http_response_code(401);
                    echo json_encode(array("error_msg" => "check username"));
                } else {

                    $id = $user['userid'];
                    $tempArr2 = array();

                    $me = mysqli_query($conn, "select * from `chat_member` left join chatroom on chatroom.chatroomid=chat_member.chatroomid where chat_member.userid='" . $id . "' order by chatroom.date_created desc");

                    $numme = mysqli_num_rows($me);

                    $query = "
	                        SELECT * FROM `chat_member` cm 
	                        INNER JOIN user u ON u.userid = cm.userid 
	                        LEFT JOIN `sessions` s ON
	                        s.id = (
	                            SELECT id 
	                            from `sessions`
	                            WHERE `sessions`.`user_id` = u.userid
	                            LIMIT 1
	                            ORDER BY id DESC
	                        )
	                        WHERE chatroomid = '" . $id . "'
	                    ";

                    $my = mysqli_query($conn, $query);

                    while ($myrow = mysqli_fetch_array($my)) {

                        $arr = array(
                            "userid" => $myrow['user_id'],
                            "uname" => $myrow['username'],
                            "photo" => $myrow['photo'],
                            "country" => $myrow['country'],
                            "region" => $myrow['region'],
                            "timezone" => $myrow['timezone'],
                            "last_message_date" => $myrow['last_activity'],
                            "online" => $myrow['is_active']
                        );
                        array_push($tempArr2, $arr);
                    }

                    $response['data'] = $tempArr2;
                }
            }

            break;


        case 'chatmessage':

            if (isTheseParametersAvailable(array('username', 'member_userid'))) {

                $username = $_REQUEST['username'];

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}'");
                $user = mysqli_fetch_assoc($res);
                if (empty($user)) {
                    http_response_code(401);
                    echo json_encode(array("error_msg" => "Username not available"));
                } else {

                    $from = $user['userid'];
                    $to = $_REQUEST['member_userid'];

                    $sql = "SELECT  c.*,u.uname,u.photo FROM `chat` c INNER JOIN `user` u ON u.userid = c.c_from WHERE c.c_to IN ($to,$from) AND c.c_from IN ($to,$from)";

                    $query = mysqli_query($conn, $sql) or die(mysqli_error());

                    mysqli_query($conn, "UPDATE chat SET status=1 WHERE c_to={$from} AND c_from={$to}") or die(mysqli_error());

                    $tempArr = array();

                    while ($row = mysqli_fetch_array($query)) {
                        $arr = array(
                            'date' => $row['chat_date'],
                            'uname' => $row['uname'],
                            'type' => $row['type'],
                            'message' => $row['message'],
                            'unread' => $row['status']
                        );
                        array_push($tempArr, $arr);
                    }

                    $response['status'] = 200;
                    $response['messages'] = $tempArr;
                }
            }

            break;


        case 'newmessage':

            if (isTheseParametersAvailable(array('username'))) {

                $username = check_input($_REQUEST['username']);
                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}'");
                $user = mysqli_fetch_assoc($res);

                if (empty($user)) {
                    http_response_code(401);
                    echo json_encode(array("error_msg" => "username not found"));
                } else {
                    $to = $user['userid'];

                    $query = mysqli_query($conn, "SELECT  c.*,u.uname,u.photo FROM chat c INNER JOIN user u ON u.userid = c.c_from WHERE c.c_to={$to} AND status=1");

                    $messages = array();

                    while ($row = mysqli_fetch_array($query)) {

                        if ($row['type'] == 'attachment') {
                            if (explode('/', $row['attachment_type'])[0] == 'image')
                                $message_content = 'https://www.travpart.com/Chinese/travchat/upload/' . $row['message'];
                            else
                                $message_content = "https://www.travpart.com/Chinese/travchat/user/download-attachment.php?file=" . rawurlencode($row['message']) . "&type={$row['attachment_type']}";
                            array_push($messages, array('member_userid' => $row['c_from'],
                                'date' => $row['chat_date'],
                                'uname' => $row['uname'],
                                'type' => $row['type'],
                                'message' => $message_content,
                                'unread' => (($row['status'] == 0) ? TRUE : FALSE)
                            ));
                        }
                        else {
                            array_push($messages, array('member_userid' => $row['c_from'],
                                'date' => $row['chat_date'],
                                'uname' => $row['uname'],
                                'type' => $row['type'],
                                'message' => $row['message'],
                                'unread' => (($row['status'] == 0) ? TRUE : FALSE)
                            ));
                        }
                    }
                    mysqli_query($conn, "UPDATE chat SET status=1 WHERE c_to={$to}");

                    http_response_code(200);
                    echo json_encode(array('msg' => 'Success', 'messages' => $messages));
                }
            }
            break;


        case 'updateaccount':

            if (isTheseParametersAvailable(array('username', 'name', 'password', 'email', 'phone'))) {

                $username = check_input($_REQUEST['username']);
                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}'");
                $user = mysqli_fetch_assoc($res);

                if (empty($user)) {
                    http_response_code(401);
                    echo json_encode(array("error_msg" => "username not found"));
                } else {
                    $id = $user['userid'];
                    $name = $_REQUEST['name'];
                    $password = md5($_REQUEST['password']);
                    $username = $_REQUEST['username'];
                    $email = $_REQUEST['email'];
                    $phone = $_REQUEST['phone'];

                    if (mysqli_query($conn, "update `user` set username='$username', password='$password', uname='$name',email='$email',phone='$phone' where userid='" . $id . "'")) {
                        $response['status'] = 200;
                        $response['status'] = 'Account updated';
                    } else {

                        $response['status'] = 401;
                        $response['status'] = 'Error happened';
                    }
                }
            }

            break;


        case 'getaccountdetails':

            if (isTheseParametersAvailable(array('username'))) {

                $myq = mysqli_query($conn, "select * from `user` where username='" . $_REQUEST['username'] . "'");

                if ($myqrow = mysqli_fetch_array($myq)) {
                    $arr = array(
                        "userid" => $myqrow['userid'],
                        "username" => $myqrow['username'],
                        "email" => $myqrow['email'],
                        "password" => $myqrow['password'],
                        "uname" => $myqrow['uname'],
                        "photo" => $myqrow['photo'],
                        "access" => $myqrow['access'],
                        "activated" => $myqrow['activated'],
                        "country" => $myqrow['country'],
                        "region" => $myqrow['region'],
                        "timezone" => $myqrow['timezone'],
                        "user_unique_room_id" => $myqrow['user_unique_room_id'],
                        "userid_travcust" => $myqrow['userid_travcust'],
                        "phone" => $myqrow['phone'],
                        "is_active" => $myqrow['is_active'],
                    );
                    $response['status'] = 200;
                    $response['userdata'] = $arr;
                } else {
                    $response['status'] = 401;
                    $response['msg'] = "ERROR";
                }
            }

            break;


        case 'sendattachment':

            if (isTheseParametersAvailable(array('username', 'from', 'to'))) {

                $tmp_name = $_FILES['file']['tmp_name'];
                $file_name = $_FILES['file']['name'];
                $size = $_FILES['file']['size'];
                $type = $_FILES['file']['type'];
                $error = 0;
                $user_name = $_REQUEST['user_name'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];


                if ($error == 0) {

                    $folder = "../upload/" . $user_name;

                    if (!is_dir($folder)) {
                        mkdir($folder, 0700, true);
                    }

                    $upload_file_full_path = $folder . "/" . $file_name;

                    if (move_uploaded_file($tmp_name, $upload_file_full_path)) {

                        $msg = $user_name . "/" . $file_name;

                        $sql = "insert into `chat` (message, userid, chat_date,c_to,c_from,type,attachment_type) values ('" . $msg . "' , '" . $from . "', NOW(),'" . $to . "','" . $from . "','attachment','" . $type . "')";

                        $result = mysqli_query($conn, $sql);

                        if ($result) {

                            $response['error'] = false;
                            $response['message'] = 'Successfully send';
                        } else {

                            $response['error'] = true;
                            $response['message'] = 'Error in sending file1';
                        }
                    } else {

                        $response['error'] = true;
                        $response['message'] = 'Error in sending file2';
                    }
                } else {

                    $response['error'] = true;
                    $response['message'] = 'Error in file';
                }
            }

            break;


        case 'updatephoto':

            if (isTheseParametersAvailable(array('username'))) {

                $username = check_input($_REQUEST['username']);

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}'");
                $user = mysqli_fetch_assoc($res);

                if (empty($user)) {
                    http_response_code(401);
                    echo json_encode(array("error_msg" => "username not found"));
                } else {


                    $fileInfo = PATHINFO($_FILES["photo"]["name"]);

                    if (empty($_FILES["photo"]["name"])) {

                        $location = $srow['photo'];
                        $response['status'] = 401;
                        $response['message'] = 'Uploaded photo is empty!';
                    } else {

                        $userid = $user['userid'];

                        if ($fileInfo['extension'] == "jpg" OR $fileInfo['extension'] == "png") {

                            $newFilename = $fileInfo['filename'] . "_" . time() . "." . $fileInfo['extension'];
                            move_uploaded_file($_FILES["image"]["tmp_name"], "../upload/" . $newFilename);
                            $location = "upload/" . $newFilename;

                            mysqli_query($conn, "update `user` set photo='$location' where userid='" . $userid . "'");

                            $response['status'] = 200;
                            $response['message'] = 'Photo updated successfully!';
                        } else {

                            $response['error'] = 401;
                            $response['message'] = 'Photo not updated. Please upload JPG or PNG files only!';
                        }
                    }
                }
            } else {

                $response['status'] = 401;
                $response['message'] = 'Invalid Operation Called';
            }


            break;


        // case 'updateonlinestatus':
        // 	if(isTheseParametersAvailable(array('username'))){
        // 		$username=check_input($_REQUEST['username']);
        // 		$res=mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}'");
        // 		$user=mysqli_fetch_assoc($res);
        // 		if(empty($user))
        // 		{
        // 			http_response_code(401);
        // 			echo json_encode(array("error_msg"=>"ERROR"));
        // 		}
        // 		else
        // 		{
        // 			if(mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP() WHERE userid={$user['userid']}"))
        // 			{
        // 				http_response_code(200);
        // 				echo json_encode(array("msg"=>"update completed"));
        // 			}
        // 			else
        // 			{
        // 				http_response_code(401);
        // 				echo json_encode(array("error_msg"=>"Update failed"));
        // 			}
        // 		}
        // 	}else {
        // 		http_response_code(401);
        // 		echo json_encode(array("error_msg"=>"add username"));
        // 	}
        // break;


        default:

            $response['error'] = true;
            $response['message'] = 'Invalid Operation Called';
    }
} else {
    $response['error'] = true;
    $response['message'] = 'Invalid API Call';
}

echo json_encode($response);

// FUNCTIONS

function isTheseParametersAvailable($params) {
    foreach ($params as $param) {
        if (!isset($_REQUEST[$param])) {
            return false;
        }
    }

    return true;
}

function check_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
}

function sendmailtosuer($to, $subject, $message) {
    $key = "Tt3At58P6ZoYJ0qhLvqdYyx21";
    $postdata = array('to' => $to,
        'subject' => $subject,
        'message' => $message);
    $url = "https://www.tourfrombali.com/api.php?action=sendmail&key={$key}";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $data = curl_exec($ch);
    if (curl_errno($ch) || $data == FALSE) {
        curl_close($ch);
        return FALSE;
    } else {
        curl_close($ch);
        return TRUE;
    }
}

?>