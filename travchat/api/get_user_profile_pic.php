<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $wp_user = $wpdb->get_row("SELECT wp_users.ID, wp_users.user_login FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
    $wp_user_ID = $wp_user->ID;

    if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {
        if(!isset($id)){
            $id = $wp_user_ID;
        }
        $response['msg'] = "Successfully";
        $response['avator'] =  um_get_user_avatar_url($id);
       
        
    } else {

        http_response_code(200);
        $response['msg'] = "No user exist";
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}
