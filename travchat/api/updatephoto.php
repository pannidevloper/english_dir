<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username'))) {
    $token = check_input($_REQUEST['token']);
    $username = check_input($_REQUEST['username']);

    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);

    if (empty($user)) {
        http_response_code(401);
        $response['msg'] = "username not found";
        // echo json_encode(array("error_msg" => "username not found"));
    } else {


        $fileInfo = PATHINFO($_FILES["photo"]["name"]);
        $image_format = array('jpg', 'png', 'gif', 'bmp', 'webp', 'jpeg');

        if (empty($_FILES["photo"]["name"])) {

            $location = $srow['photo'];
            http_response_code(401);
            $response['msg'] = 'Uploaded photo is empty!';
        } else {

            $userid = $user['userid'];

            // if ($fileInfo['extension'] == "jpg" OR $fileInfo['extension'] == "png") {
            if (in_array($fileInfo['extension'], $image_format)) {

                $newFilename = $fileInfo['filename'] . "_" . time() . "." . $fileInfo['extension'];

                move_uploaded_file($_FILES["photo"]["tmp_name"], "../upload/" . $newFilename);

                $location = "upload/" . $newFilename;
                if (mysqli_query($conn, "update `user` set photo='$location' where userid='" . $userid . "'")) {
                    http_response_code(200);
                    $response['msg'] = 'Photo updated successfully!';
                }
            } else {
                http_response_code(401);
                $response['msg'] = "Photo not updated. We don't support {$fileInfo['extension']} file!";
            }
        }
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
