<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('feed_id'))) {

	$feed_id = filter_input(INPUT_POST, 'feed_id', FILTER_SANITIZE_STRING);
	$comments = $wpdb->get_results("SELECT user_feed_comments.id,user_feed_comments.comment,user_feed_comments.user_id,wp_users.user_login FROM `user_feed_comments`,`wp_users` WHERE wp_users.ID=user_id AND `feed_id` = '{$feed_id}' ORDER BY user_feed_comments.id DESC");

		if ( $comments ){

	   http_response_code(200);
		$response['succes'] = 1;
		$response['msg'] = 'Successfully retrieved';
		$response['comments'] = $comments;
	}
	else{
		http_response_code(200);
		$response['success'] = 0;
		$response['msg'] = 'No comments found';

	}

 }
 else{
       http_response_code(401);
       $response['msg'] = 'Required parameter missing';
   }