<?php
defined('BASEPATH') or exit('No direct script access allowed');

$currencyList = get_option('currency_list');
if (!empty($currencyList)) {
    http_response_code(200);
    $response['currencies'] = $currencyList;
    $response['msg'] = 'success';
} else {
    http_response_code(401);
    $response['msg'] = 'There is not any currency';
}
