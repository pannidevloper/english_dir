<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $token = check_input($_REQUEST['token']);
    $username = check_input($_REQUEST['username']);
    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);

    if (empty($user)) {
        http_response_code(401);
        $response['msg'] = "username not found";
        // echo json_encode(array("error_msg" => "username not found"));
    } else {
        $to = $user['userid'];

        $query = mysqli_query($conn, "SELECT  c.*,u.uname,u.photo FROM chat c INNER JOIN user u ON u.userid = c.c_from WHERE c.c_to={$to} AND status=1");

        $messages = array();

        while ($row = mysqli_fetch_array($query)) {

            if ($row['type'] == 'attachment') {
                if (explode('/', $row['attachment_type'])[0] == 'image')
                    $message_content = 'https://www.travpart.com/English/travchat/upload/' . $row['message'];
                else
                    $message_content = "https://www.travpart.com/English/travchat/user/download-attachment.php?file=" . rawurlencode($row['message']) . "&type={$row['attachment_type']}";
                array_push($messages, array(
                    'member_userid' => $row['c_from'],
                    'date' => $row['chat_date'],
                    'uname' => $row['uname'],
                    'type' => $row['type'],
                    'message' => $message_content,
                    'unread' => (($row['status'] == 0) ? TRUE : FALSE)
                ));
            } else {
                array_push($messages, array(
                    'member_userid' => $row['c_from'],
                    'date' => $row['chat_date'],
                    'uname' => $row['uname'],
                    'type' => $row['type'],
                    'message' => $row['message'],
                    'unread' => (($row['status'] == 0) ? TRUE : FALSE)
                ));
            }
        }
        mysqli_query($conn, "UPDATE chat SET status=1 WHERE c_to={$to}");

        http_response_code(200);
        $response['msg'] = 'success';
        $response['messages'] = $messages;
        // echo json_encode(array('msg' => 'Success', 'messages' => $messages));
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
