<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token','request_id'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $request_type = 1;
    $request_id = filter_input(INPUT_POST, 'request_id', FILTER_SANITIZE_STRING);
    /// $search_term = check_input($_REQUEST['search']);
    $wp_user = $wpdb->get_row("SELECT wp_users.ID, wp_users.user_login FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
    $wp_user_ID = $wp_user->ID;

    if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {

    $reciver_id = $wpdb->get_row("SELECT * FROM `friends_requests` where id='{$request_id}'");

    if( $reciver_id ) {
    $wpdb->update(
                    'friends_requests', array(
                'status' => $request_type,
                'action_by_user' => $wp_user_ID,
            
                    ), array('id' => $request_id), array(
                '%d',
                '%d',
                    ), array('%d')
            );
    http_response_code(200);
    $response['msg'] = "Friend Request Accepted";

    $msg2 ="<a href='https://www.travpart.com/English/timeline'>".um_get_display_name($reciver_id->user2)." accepted your friend's request. Post something on his/her timeline.</a>";
    $wpdb->insert('notification', array('wp_user_id'=>$reciver_id->user1, 'content'=>$msg2,'identifier'=>'friend_request_confirmed_'.$username));

     $wpdb->insert('notification', array('wp_user_id'=>$reciver_id->user2, 'content'=>'You are  connected with '. um_get_display_name($reciver_id->user1),'identifier'=>'friends_now'));

     if(!empty($token)){

    $msg = um_get_display_name($reciver_id->user2)." accepted your friend's request. Post something on his/her timeline.";

    $token =to_get_user_device_token_from_wp_id_for_api($reciver_id->user1);

    if($token){
    // sendtomobilefunction($msg,$token);
    notification_for_user_form_api($token,$msg,'friend_request_confirmed');

}
    } 
        
}
		else{

		http_response_code(200);
		$response['msg'] = "No Request exist ";
}

    	}
    	else{
    	 http_response_code(200);
        $response['msg'] = "No user exist";
    	}
    }
    else{
    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
    }