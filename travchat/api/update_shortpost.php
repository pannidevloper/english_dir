<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token','post_id'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $post_id = filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_STRING);

    $post_new_content = filter_input(INPUT_POST, 'post_new_content', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_ID) {

        $post = get_post($post_id);

        if($post->post_author == $wp_user_ID) {

            $updated_post = array(
            'ID'           => $post_id,
            'post_content' => $post_new_content
        );
        $post_id_return = wp_update_post($updated_post);
        if($post_id_return){
            $msg ="successfully updated";
        }
         http_response_code(200);
         $response['success'] = 1;
         $response['msg'] = $msg;
        }
        else{
          http_response_code(400);
         $response['success'] = 0;
         $response['msg'] = 'Post Author Not matched';

        }

       
    }
    else {

    http_response_code(400);
    $response['success'] = 0;
    $response['msg'] = 'No user found';
    }
    } else {

    http_response_code(401);
    $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
    }
