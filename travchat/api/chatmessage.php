<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'member_userid'))) {

    $username = $_REQUEST['username'];
    $token = check_input($_REQUEST['token']);
    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);
    if (empty($user)) {
        http_response_code(401);
        $response['msg'] = "Username not available";
    } else {

        $from = $user['userid'];
        $to = $_REQUEST['member_userid'];

        $sql = "SELECT  c.*,u.uname,u.photo FROM `chat` c INNER JOIN `user` u ON u.userid = c.c_from WHERE c.c_to IN ($to,$from) AND c.c_from IN ($to,$from)";

        $query = mysqli_query($conn, $sql) or die(mysqli_error());

        mysqli_query($conn, "UPDATE chat SET status=1 WHERE c_to={$from} AND c_from={$to}") or die(mysqli_error());

        $tempArr = array();

        while ($row = mysqli_fetch_array($query)) {

            if ($row['type'] == 'attachment') {

                $message_content = "https://www.travpart.com/English/travchat/upload/" . rawurlencode($row['message']);
                $arr = array(
                    'date' => $row['chat_date'],
                    'uname' => $row['uname'],
                    'member_userid' => $row['userid'],
                    'photo' => (empty($row['photo']) ? '' : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
                    'type' => $row['type'],
                    'message' => $message_content,
                    'unread' => $row['status']
                );
                array_push($tempArr, $arr);
            } else {
                $arr = array(
                    'date' => $row['chat_date'],
                    'uname' => $row['uname'],
                    'member_userid' => $row['userid'],
                    'photo' => (empty($row['photo']) ? '' : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
                    'type' => $row['type'],
                    'message' => $row['message'],
                    'unread' => $row['status']
                );
                array_push($tempArr, $arr);
            }
        }

        http_response_code(200);
        $response['msg'] = "success";
        $response['messages'] = $tempArr;
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
