<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('user_name'))) {

    $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);

    $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");

    if ($wp_userid) {

        $location = $wpdb->get_row("SELECT * FROM `user_location` WHERE ID='{$wp_userid}'");

        if ($location) {
            $lat = $location->lat;
            $Lng = $location->lng;
        }
        http_response_code(200);
        $response['msg'] = "location Found";
        $response['coordiantes'] = $lat . "," . $Lng;
    } else {
        http_response_code(401);
        $response['msg'] = 'No location coordiantes';
    }
} else {

    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}
