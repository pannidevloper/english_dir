<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token', 'post_content'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $location = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING);
    $lat = filter_input(INPUT_POST, 'lat', FILTER_SANITIZE_NUMBER_FLOAT);
    $lng = filter_input(INPUT_POST, 'lng', FILTER_SANITIZE_NUMBER_FLOAT);
    $content = filter_input(INPUT_POST, 'post_content', FILTER_SANITIZE_STRING);
    $feeling = filter_input(INPUT_POST, 'feeling', FILTER_SANITIZE_STRING);
    $tour_id = filter_input(INPUT_POST, 'tourpackage', FILTER_SANITIZE_NUMBER_INT);
    $tag_user_id = filter_input(INPUT_POST, 'tagfriend', FILTER_SANITIZE_NUMBER_INT);
    $lookingfriend = filter_input(INPUT_POST, 'lookingfriend', FILTER_SANITIZE_NUMBER_INT);
    $startdate = filter_input(INPUT_POST, 'startdate', FILTER_SANITIZE_STRING);
    $enddate = filter_input(INPUT_POST, 'enddate', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_ID) {
        $post_content = wp_rel_nofollow($content);
        $location = htmlspecialchars($location);
        $new_post = array(
            'post_content'   => wp_kses_post($post_content),
            'post_type'      => 'shortpost',
            'post_status'    => 'publish',
            'post_author' => $wp_user_ID
        );

        if (!empty($lookingfriend) && $lookingfriend >= 1) {
            if (empty($location) || empty($lat) || empty($lng)) {
                http_response_code(401);
                $response['msg'] = "Location is required";
                exit(json_encode($response));
            }
            if (empty($startdate)) {
                http_response_code(401);
                $response['msg'] = "Start date is required";
                exit(json_encode($response));
            }
            if (empty($enddate)) {
                http_response_code(401);
                $response['msg'] = "End date is required";
                exit(json_encode($response));
            }
        }

        $new_post_id = wp_insert_post($new_post, true);

        if (is_wp_error($new_post_id)) {
            http_response_code(401);
            $response['msg'] = $new_post_id->get_error_message();
            exit(json_encode($response));
        }

        if (!empty($_FILES['photo_or_video'])) {
            $attachment_id = media_handle_upload('photo_or_video', $new_post_id, array('post_author' => $wp_user_ID));
            if (is_wp_error($attachment_id)) {
                http_response_code(401);
                $response['msg'] = 'File is invaild';
                exit(json_encode($response));
            } else {
                update_post_meta($new_post_id, 'attachment', $attachment_id);
            }
        } else if (!empty($_POST['attachment'])) {
            foreach ($_POST['attachment'] as $i) {
                add_post_meta($new_post_id, 'attachment', intval($i));
            }
        }

        if (!empty($location)) {
            update_post_meta($new_post_id, 'location', $location);
        }

        if (!empty($startdate)) {
            update_post_meta($new_post_id, 'startdate', $startdate);
        }

        if (!empty($enddate)) {
            update_post_meta($new_post_id, 'enddate', $enddate);
        }

        if (!empty($feeling)) {
            update_post_meta($new_post_id, 'feeling', base64_encode($feeling));
        }

        if (!empty($tag_user_id)) {
            update_post_meta($new_post_id, 'tag_user_id', $tag_user_id);
        }

        if ($lookingfriend >= 1) {
            update_post_meta($new_post_id, 'lookingfriend', $lookingfriend);
            $friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE ID IN(
                    SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_user_ID}')
                    OR ID IN(
                        SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_user_ID}')");
            foreach ($friends as $f) {
                $wpdb->insert('notification', array('wp_user_id' => $f->ID, 'content' => um_get_display_name($wp_user_ID) . " is looking for a travel friend"));
            }
        }

        if (!empty($tour_id)) {
            $tour = $wpdb->get_row("SELECT ID,post_title FROM `wp_posts`,`wp_postmeta`
                    WHERE post_type='post' AND post_status='publish' AND wp_posts.ID=wp_postmeta.post_id
                    AND `meta_key`='tour_id' AND meta_value='{$tour_id}'");
            update_post_meta($new_post_id, 'tour_post', $tour->ID);
            update_post_meta($new_post_id, 'tour_title', $tour->post_title);

            $hotelinfo = $wpdb->get_row("SELECT img,location FROM wp_hotel WHERE tour_id='{$tour_id}' LIMIT 1");
            if (!empty($hotelinfo->img)) {
                if (is_array(unserialize($hotelinfo->img))) {
                    $hotelimg = unserialize($hotelinfo->img)[0];
                } else
                    $hotelimg = $hotelinfo->img;
            }
            if (!empty($hotelimg)) {
                update_post_meta($new_post_id, 'tour_img', $hotelimg);
            }
        }

        if (!empty($lookingfriend) && $lookingfriend>=1) {
            $wpdb->insert('wp_user_plans',
                array(
                    'user_id' => $wp_user_ID,
                    'user_name' => $username,
                    'start_date' => date('d/m/Y',strtotime($startdate)),
                    'end_Date' => date('d/m/Y',strtotime($enddate)),
                    'no_of_days' => ceil(abs((strtotime($enddate)-strtotime($startdate))/86400)),
                    'location' => $location,
                    'lat' => $lat,
                    'lng' => $lng
                )
            );
        }

        http_response_code(200);
        $response['msg'] = 'Posted successfully';
    }
    // if user not exist
    else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
