<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token', 'keyword'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $keyword = filter_input(INPUT_POST, 'keyword', FILTER_SANITIZE_STRING);

    $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

    if (!empty($userid)) {
        $msgList = $wpdb->get_results("SELECT chat.userid,user.username,chatid,message,chat_date,c_to,c_from FROM `chat`,`user`
                WHERE `message` LIKE '%{$keyword}%' AND (`c_from` = '{$userid}' OR `c_to` = '{$userid}') AND chat.userid=user.userid AND type!='deleted' ORDER BY `chatid` DESC");
        $response['msgResults'] = $msgList;

        $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
        $userList = $wpdb->get_results("SELECT ID,userid,username,uname,photo,country,region FROM `user`,wp_users
        WHERE `username` LIKE '%{$keyword}%' AND username=user_login
        AND (ID IN(SELECT user1 FROM friends_requests WHERE user2='{$wp_user_ID}' AND status=1)
            OR ID IN(SELECT user2 FROM friends_requests WHERE user1='{$wp_user_ID}' AND status=1)
        )", ARRAY_A);
        foreach ($userList as &$user) {
            $avatar = um_get_user_avatar_data($user['ID']);
            $user = array(
                'userid' => $user['userid'],
                'name' => (empty($user['uname']) ? $user['username'] : $user['uname']),
                'photo' => (empty($avatar['url']) ? '' : ($avatar['url'])),
                'country' => $user['country'],
                'region' => $user['region']
            );
        }
        $response['userResults'] = $userList;

        http_response_code(200);
        $response['msg'] = 'success';
    } else {
        http_response_code(401);
        $response['msg'] = 'Incorrect token';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
