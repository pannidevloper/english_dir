<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

    if (!empty($userid)) {
        $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$username}'");
        $pageList = $wpdb->get_results("SELECT ID,post_title,post_author,post_type,post_content FROM `wp_posts` WHERE ID IN (
                SELECT post_id FROM `blog_social_connect` WHERE sc_type=0 AND sc_user_id='{$wp_userid}'
                ) OR ID IN (
                SELECT post_id FROM `wp_postmeta`,social_connect WHERE meta_key='tour_id' AND sc_type=0 AND sc_tour_id=meta_value AND sc_user_id='{$wp_userid}'
                ) AND post_status='publish'
                ORDER BY ID DESC", ARRAY_A);
        foreach ($pageList as &$page) {
            $page['url'] = get_permalink($page['ID']);
            if ($page['post_type'] == 'post') {
                $tour_id = get_post_meta($page['ID'], 'tour_id', true);
                $hotelinfo = $wpdb->get_row("SELECT img,location FROM wp_hotel WHERE tour_id='{$tour_id}' LIMIT 1");
                if (!empty($hotelinfo->img)) {
                    if (is_array(unserialize($hotelinfo->img))) {
                        $page['img'] = unserialize($hotelinfo->img)[0];
                    } else
                        $page['img'] = $hotelinfo->img;
                }
            } else {
                $page['img'] = get_template_directory_uri() . '/assets/img/rome.jpg';
                if (preg_match('!<img.*?src="(.*?)"!', $page['post_content'], $post_image_match)) {
                    $page['img'] = $post_image_match[1];
                }
            }
            unset($page['post_content']);
        }
        http_response_code(200);
        $response['msg'] = 'success';
        $response['pages'] = $pageList;
    } else {
        http_response_code(401);
        $response['msg'] = 'Incorrect token';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
