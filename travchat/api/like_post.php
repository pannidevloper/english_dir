<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Content-Type: application/json');

$update_like = function($feed_id,$username,$token) {
    global $wpdb;

    $res = [];


    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

	$Data = $wpdb->get_row("SELECT * FROM `user_feed_likes` WHERE user_id = '$wp_user_ID' AND feed_id = '$feed_id'");
	if($Data){
		$feed_id_get = $Data->id;
		$table = 'user_feed_likes';
		$wpdb->delete( $table, array( 'ID' => $feed_id_get ) ); 

		$res['status'] = 1;
		$res['message'] =  "Successfully unliked the post" ;
	} else{

		$wpdb->insert(
			'user_feed_likes',
			array(
				'feed_id' => $feed_id,
				'user_id' => $wp_user_ID,

			),
			array(
				'%s',
				'%s',
			)
		);

//get the post username from post id

		$author_id = get_post_field( 'post_author', $feed_id );

/// User Notifaction process

		$msg = "<a href='https://www.travpart.com/English/shortpost/$feed_id'>".um_get_display_name($wp_user_ID) . " Liked your post </a>";
		$wpdb->insert('notification', array('wp_user_id'=>$author_id, 'content'=>$msg,'identifier'=>'some_one_liked_the_post_'.$feed_id));

    // get  user Token
		$token= to_get_user_device_token_from_wp_id($author_id);


		if(!empty($token)){

			$msg = um_get_display_name($wp_user_ID)." Liked your post";

			notification_for_user( $token,$msg,'some_one_liked_the_post'.$feed_id);
		} 


		$res['status'] = 1;
		$res['message'] = "Successfully liked the post";

	}
	return $res;
};

if (isTheseParametersAvailable(array('username', 'token','feed_id'))) {
	http_response_code(200);

	$feed_id = filter_input(INPUT_POST, 'feed_id', FILTER_SANITIZE_NUMBER_INT);
	 $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
	$like = $update_like($feed_id,$username,$token);
	$response = array(
		'data' => $like
	);
} else {
	http_response_code(401);
	$response['msg'] = 'Required parameter missing';
}
