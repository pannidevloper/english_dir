<?php
defined('BASEPATH') or exit('No direct script access allowed');

function sendtomobile($message, $id)
{
    $url = 'https://fcm.googleapis.com/fcm/send';

    $fields = array(
        'registration_ids' => array(
            $id
        ),
        'data' => array(
            "message" => $message
        )
    );


    $headers = array(
        'Authorization: key=' . "AIzaSyCUhKe-YxeR_gBYlGnhiuxzphZPYMUPXos",
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS,  json_encode($fields));

    $result = curl_exec($ch);
    // print_r( $result );
    curl_close($ch);
}

if (isTheseParametersAvailable(array('username', 'member_userid', 'msg'))) {

    $msg = $_REQUEST['msg'];
    $token = check_input($_REQUEST['token']);
    $username = check_input($_REQUEST['username']);

    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);
    if (empty($user)) {
        http_response_code(401);
        $response['msg'] = 'user not found';
        // echo json_encode(array("error_msg" => "no user found"));
    } else {
        if (mysqli_fetch_array(mysqli_query($conn, "SELECT COUNT(*) FROM chat_member WHERE (userid={$user['userid']} AND chatroomid={$_REQUEST['member_userid']}) OR (userid={$_POST['member_userid']} AND chatroomid={$user['userid']})"))[0] <= 0); {
            mysqli_query($conn, "insert into chat_member (userid, chatroomid) values ('{$user['userid']}','{$_REQUEST['member_userid']}')");
            mysqli_query($conn, "insert into chat_member (userid, chatroomid) values ('{$_REQUEST['member_userid']}','{$user['userid']}')");
        }

        $from = $user['userid'];
        $to = $_REQUEST['member_userid'];

        $sql = "insert into `chat` (message, userid, chat_date,c_to,c_from,type,attachment_type) values ('{$msg}' , '" . $from . "', NOW(),'{$to}','" . $from . "','','')";

        if (mysqli_query($conn, $sql)) {
            http_response_code(200);
            $res =  mysqli_query($conn2, "SELECT * from  `sessions`   WHERE user_id='{$to}'");
            if (mysqli_num_rows($res) != 0) {

                $user = mysqli_fetch_assoc($res);
                $send_key = $user['device_token'];
                if ( $send_key) {
                notification_for_user_form_api ( $send_key,$username.' sent you a message ','chat_msg_'.$from);
            }
              //  sendtomobile($msg, $send_key);
            }
            $response['msg'] = 'Sent';
            $response['messages'] = array('date' => date('Y-m-d H:i:s'), 'uname' => $username, 'member_userid' => $to, 'type' => '', 'message' => $msg, 'unread' => 0);
        }
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
