<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isset($_REQUEST['tour'])) {
    $tour_id = $_REQUEST['tour'];
    $totalComment = $wpdb->get_var("SELECT COUNT(*) FROM `social_comments` WHERE scm_tour_id = '$tour_id'");

    $totalLikes = $wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE sc_type = 0 AND sc_tour_id = '$tour_id'");

    $totalDislikes = $wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE sc_type = 1 AND sc_tour_id = '$tour_id'");

    //get share count
    $share_count = $wpdb->get_var("SELECT `meta_value` FROM `wp_tourmeta` WHERE `tour_id`='{$tour_id}' AND `meta_key`='sharecount'");
    if (empty($share_count)) {
        $share_count = 0;
    }

    $data = array();
    array_push($data, array(
        'comment' => $totalComment,
        'likes' => $totalLikes,
        'dislikes' => $totalDislikes,
        'share' => $share_count

    ));
    http_response_code(200);
    //  echo json_encode(array('msg' => 'tour statices.', 'list' => $data));
    $response['result'] = $data;
} else {
    http_response_code(401);
    $response['msg'] = "empty tour id";
}
