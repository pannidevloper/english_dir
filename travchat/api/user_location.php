<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token', 'lat', 'lng'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $lat = filter_input(INPUT_POST, 'lat', FILTER_SANITIZE_STRING);
    $lng = filter_input(INPUT_POST, 'lng', FILTER_SANITIZE_STRING);

    $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

    if (!empty($userid)) {
        $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$username}'");

        $check_for_location = $wpdb->get_var("SELECT COUNT(*) FROM `user_location` WHERE ID='{$wp_userid}'");

        if ($check_for_location > 0) {


            $wpdb->update(
                'user_location',
                array(
                    'lat' => $lat,
                    'lng' => $lng
                ),
                array('ID' => $wp_userid),
                array(
                    '%f',   // value1
                    '%f'    // value2
                ),
                array('%d')
            );
            http_response_code(200);
            $response['msg'] = 'location Data Updated Successfully';
        } else {

            $sql = $wpdb->prepare("INSERT INTO `user_location` (`ID`, `lat`,`lng`) values (%d, %f,%f)", $wp_userid, $lat, $lng);
            $wpdb->query($sql);
            http_response_code(200);
            $response['msg'] = 'location Data Addeded Successfully';
        }
    } else {
        http_response_code(401);
        $response['msg'] = 'Incorrect token';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
