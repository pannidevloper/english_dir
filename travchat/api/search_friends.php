<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $kw = filter_input(INPUT_POST, 'kw', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($wp_user_ID)) {
        $friends = $wpdb->get_results("SELECT ID,user_login as username FROM `wp_users` WHERE user_login LIKE '%{$kw}%' AND (ID IN(
            SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_user_ID}')
            OR ID IN(
                SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_user_ID}'))", ARRAY_A);
        http_response_code(200);
        $response['list'] = $friends;
    } else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
