<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'password'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $now = date('Y-m-d H:i:s', time());

    if (is_email($username)) {
        $username_or_email = "email='$username'";
    } else {
        $username_or_email = "username='$username'";
    }

    if (!is_email($username) && !preg_match("/^[a-zA-Z0-9_]*$/", $username)) {
        http_response_code(401);
        $response['msg'] = 'Invalid username or email address';
    } elseif ($wpdb->get_var("SELECT COUNT(*) from `user` WHERE {$username_or_email}") == 0) {
        http_response_code(401);
        $account_type = is_email($username) ? 'email address' : 'username';
        $response['msg'] = "Sorry, we can't find an account with that {$account_type}";
    } elseif ($wpdb->get_var("SELECT activated from `user` WHERE {$username_or_email}") != '1') {
        http_response_code(401);
        $resent_link = home_url() . "/travchat/api/?action=resent_activate_email&account=" . $username;
        $response['msg'] = 'Please activate your username from the activation email we sent to you or <a href="' . $resent_link . '">click here</a> to resend the activation email';
    } elseif (!empty($ban_suspend=$wpdb->get_var("SELECT wp_usermeta.meta_value FROM `user`,`wp_users`,`wp_usermeta` WHERE {$username_or_email} AND user.username=wp_users.user_login AND wp_usermeta.user_id=wp_users.ID AND wp_usermeta.meta_key='ban_suspend'"))) {
        http_response_code(401);
        if ($ban_suspend == 1) {
            $response['msg'] = "This account is no longger active";
        } elseif ($ban_suspend == 2) {
            $response['msg'] = "Your account is currently restricted by our Trust & Safety team";
        }
    } else {
        $fusername = $username;
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
        $fpassword = md5($password);

        $auth = $wpdb->get_var("SELECT COUNT(*) from `user` WHERE {$username_or_email} and password='$fpassword' and activated=1");
        $wp_user = wp_authenticate($username, $password);

        if ($auth == 0 && is_wp_error(($wp_user))) {
            http_response_code(401);
            $response['msg'] = 'Invalid password';
        } else {
            $token = md5($password . time());

            $row = $wpdb->get_row("SELECT wp_users.ID as wp_user_id, user.* from `wp_users`,`user` WHERE {$username_or_email} AND user.username=wp_users.user_login", ARRAY_A);

            mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");
            $wp_user_id = $row['wp_user_id'];
            $user = array(
                'userid' => $row['userid'],
                'wp_user_id'=>$wp_user_id,
                'username' => $row['username'],
                'email' => $row['email'],
                'phone' => $row['phone'],
                'uname' => $row['uname'],
                'photo' => (empty($row['photo']) ? um_get_user_avatar_url($row['wp_user_id'], 80) : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
                'country' => $row['country'],
                'region' => $row['region'],
                'timezone' => $row['timezone'],
                'access' => $row['access'],
                'token' => $token
            );

            $device_token =   to_get_user_device_token_from_wp_id_for_api($row['wp_user_id']);

            $profile_completeness =  get_user_meta($row['wp_user_id'], 'profile_completeness', true);
            $profile_completeness_value =  $profile_completeness * 100;
            if ($profile_completeness_value < 50) {
                $msg2 = "<a href='https://www.travpart.com/English/user/?profiletab=profile'>Your profile is still incomplete, finish your profile now so your friends can find you</a>";

                $wpdb->insert('notification', array('wp_user_id' => $row['wp_user_id'], 'content' => $msg2, 'identifier' => 'incomplete_profile'));
            }
            if ($device_token) {
                if ($profile_completeness_value < 50) {
                    notification_for_user_form_api($device_token, "Your profile is still incomplete, finish your profile now so your friends can find you", 'incomplete_profile');
                }
            }

            $user_id = $row['userid'];
            $user_id = (int) $user_id;

            $ip_address = $_SERVER['REMOTE_ADDR'];
            $now = date("Y-m-d H:i:s");

            $session_check =  mysqli_query($conn2, "SELECT * from  `sessions`   WHERE user_id='{$user_id}'");

            if (mysqli_num_rows($session_check) == 0) {
                $sessions_query  = "INSERT INTO `sessions` (user_id, ip_address,last_activity,wp_user_id) VALUES ( {$user_id}, '{$ip_address}','{$now}','{$wp_user_id}')";
            } else {
                $sessions_query = "UPDATE `sessions` SET last_activity  = '{$now}',wp_user_id='{$wp_user_id}' WHERE `user_id` = {$user_id}";
            }

            $result = mysqli_query($conn, $sessions_query);

            if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_users` WHERE `user_login`='{$username}'") == 0) {
                wp_authenticate($username, $password);
            }

            http_response_code(200);
            $response = $user;
        }
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
