<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
$appid = '2250832561603290';
$appsecret = 'a5af813973b2edc719991dc56aabc8a9';

//https://www.facebook.com/v6.0/dialog/oauth?client_id=2250832561603290&redirect_uri=https%3A%2F%2Fwww.travpart.com%2FEnglish%2Ftravchat%2Fapi%2F%3Faction%3Dfacebook_login&state=%7Bst%3Dtravpart2020%2Cds%3D0202trapvart%7D

$code=$_GET['code'];

$access_token=file_get_contents("https://graph.facebook.com/v6.0/oauth/access_token?client_id={$appid}&redirect_uri=https%3A%2F%2Fwww.travpart.com%2FEnglish%2Ftravchat%2Fapi%2F%3Faction%3Dfacebook_login&client_secret={$appsecret}&code={$code}");
$access_token=json_decode($access_token,true)['access_token'];
*/

if (!empty($_GET['access_token'])) {
    $access_token = filter_input(INPUT_GET, 'access_token', FILTER_SANITIZE_STRING);

    $fb_response = json_decode(file_get_contents("https://graph.facebook.com/v6.0/me?fields=id%2Cname%2Cshort_name%2Cemail&access_token={$access_token}"), true);

    if (!empty($fb_response['email'])) {
        $is_login = false;
        $passkey = $fb_response['id'] . time();
        $token = md5($passkey);
        $row = $wpdb->get_row("SELECT wp_users.ID as wp_user_id, user.* from `wp_users`,`user` WHERE email='{$fb_response['email']}' AND user.username=wp_users.user_login", ARRAY_A);

        if (empty($row) && !empty($fb_response['name'])) {
            $username = str_replace(' ', '', $fb_response['name']);

            while (username_exists($username) || $wpdb->get_var("SELECT COUNT(*) from user where username = '{$username}'") > 0) {
                $username = $username . rand(1, 9);
            }

            $wpdb->insert('user', array(
                'uname' => $fb_response['name'],
                'username' => $username,
                'email' => $fb_response['email'],
                'password' => $token,
                'token' => $token,
                'access' => 2,
                'activated' => 1,
                'photo' => '',
                'phone' => ''
            ));
            wp_authenticate($username, $passkey);
            $row = $wpdb->get_row("SELECT wp_users.ID as wp_user_id, user.* from `wp_users`,`user` WHERE email='{$fb_response['email']}' AND user.username=wp_users.user_login", ARRAY_A);
            $is_login = true;
        } else {
            $ban_suspend = get_user_meta($row['wp_user_id'], 'ban_suspend', true);
            if ($ban_suspend == '1') {
                http_response_code(401);
                $response['msg'] = 'This account is no longger active';
            } elseif ($ban_suspend == '2') {
                http_response_code(401);
                $response['msg'] = 'Your account is currently restricted by our Trust & Safety team';
            } else {
                mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");
                $is_login = true;
            }
        }

        if ($is_login) {
            //save profile photo
            if (!empty($fb_response['id'])) {
                $profile_photo = file_get_contents("http://graph.facebook.com/{$fb_response['id']}/picture?type=large");
                um_upload_profile_photo($profile_photo, $row['wp_user_id']);
            }

            $device_token = to_get_user_device_token_from_wp_id_for_api($row['wp_user_id']);

            $profile_completeness =  get_user_meta($row['wp_user_id'], 'profile_completeness', true);
            $profile_completeness_value =  $profile_completeness * 100;
            if (empty($profile_completeness) || $profile_completeness_value < 50) {
                $msg2 = "<a href='https://www.travpart.com/English/user/?profiletab=profile'>Your profile is still incomplete, finish your profile now so your friends can find you</a>";
                $wpdb->insert('notification', array('wp_user_id' => $row['wp_user_id'], 'content' => $msg2, 'identifier' => 'incomplete_profile'));
            }
            if ($device_token) {
                if ($profile_completeness_value < 50) {
                    notification_for_user_form_api($device_token, "Your profile is still incomplete, finish your profile now so your friends can find you", 'incomplete_profile');
                }
            }

            $user = array(
            'userid' => $row['userid'],
            'username' => $row['username'],
            'email' => $row['email'],
            'phone' => $row['phone'],
            'uname' => $row['uname'],
            'photo' => (empty($row['photo']) ? um_get_user_avatar_url($row['wp_user_id'], 80) : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
            'country' => $row['country'],
            'region' => $row['region'],
            'timezone' => $row['timezone'],
            'access' => $row['access'],
            'token' => $token
            );

            http_response_code(200);
            $response['msg'] = $user;
        }
    } else {
        http_response_code(401);
        $response['msg'] = 'Wrong token';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
