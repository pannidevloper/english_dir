<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = check_input($_REQUEST['username']);
    $token = check_input($_REQUEST['token']);
    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
    if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {

        $message_count =  $wpdb->get_var("SELECT COUNT(*) FROM `chat`,`user` WHERE `c_to`=user.userid AND user.username='{$username}' AND `status`=0");
        $response['no_of_messages'] = $message_count;
        $response['msg'] = "success";
    } else {
        http_response_code(401);
        $response['msg'] = 'Invalid login';
    }
} else {

    http_response_code(401);
    $response['msg'] = 'Invalid parameters';
}
