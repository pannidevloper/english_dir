<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token','user_id'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $userid = filter_input(INPUT_POST, 'user_id', FILTER_VALIDATE_INT, array('options' => array('default' => 0, 'min_range' => 0)));
    $item_per_page = 8;
    $page = filter_input(INPUT_POST, 'page', FILTER_VALIDATE_INT, array('options' => array('default' => 1, 'min_range' => 1)));
    $limit = ($page - 1) * $item_per_page . ',' . $item_per_page;

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($wp_user_ID)) {
        $user_photos = $wpdb->get_results("SELECT guid as url,post_mime_type as type FROM `wp_posts`
        WHERE wp_posts.post_author='{$userid}' AND post_status='inherit'
        AND `post_type` = 'attachment' ORDER BY post_date DESC LIMIT {$limit}", ARRAY_A);

        if (!empty($user_photos)) {
            http_response_code(200);
            $response['list'] = $user_photos;
        } else {
            http_response_code(401);
            $response['msg'] = 'no images';
        }
    } else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
