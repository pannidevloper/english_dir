<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Content-Type: application/json');

$get_timeline = function() {
    global $wpdb;

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    $friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE ID IN(
        SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_user_ID}')
        OR ID IN(
            SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_user_ID}')");

    $tourPackages = $wpdb->get_results("SELECT ID,post_title, meta_value as tour_id FROM `wp_posts`,`wp_postmeta`
            WHERE post_type='post' AND post_status='publish' AND `meta_key`='tour_id'
            AND wp_posts.ID=wp_postmeta.post_id AND post_author='{$wp_user_ID}'");

    $friends_ids_query = $wpdb->get_results("SELECT ID FROM `wp_users` WHERE ID IN(
        SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_user_ID}')
        OR ID IN(
            SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_user_ID}')");
    foreach ($friends_ids_query as $id) {

        $friends_ids[] = $id->ID;
    }

    $List = trim(implode(',', $friends_ids) . ',' . $wp_user_ID, ',');
    $pageNum = filter_input(INPUT_GET, 'pageNum', FILTER_SANITIZE_NUMBER_INT);
    $pageNum = ($pageNum > 0) ? ($pageNum - 1) : 0;
    $post_per_page = 15;
    $postOffset = $pageNum * $post_per_page;
    $posts = $wpdb->get_results("SELECT wp_posts.ID,post_author,user_login,post_title,post_date,post_type,post_content
                                            FROM `wp_posts`,`wp_users` WHERE
                                            ((post_author IN ($List) AND (`post_type`='shortpost' OR `post_type`='eventpost')) OR `post_type`='blog')
                                            AND post_status='publish' AND post_author=wp_users.ID
                                            AND wp_posts.ID NOT IN(SELECT short_post_id FROM `short_posts_hide_from_timeline` WHERE user_id='{$wp_user_ID}') 
                                            ORDER BY post_date DESC LIMIT {$postOffset},{$post_per_page}");
    
    $get_looking_friend = function($post_id) {
        $lookingfriend = get_post_meta($post_id, 'lookingfriend', true);
        switch ($lookingfriend) {
            case 1:
                $lookingfriend_value = 'Looking For A Travel Friend';
                break;
            case 2:
                $lookingfriend_value = 'Want to Hangout';
                break;
            case 3:
                $lookingfriend_value = 'Going for Party';
                break;
            case 4:
                $lookingfriend_value = 'Going for Sport';
                break;
            default:
                $lookingfriend_value = '';
        }
        return $lookingfriend_value;
    };

    // process wp posts start from here
    $data = [];
    foreach($posts as $post) {
        $liked = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes`
            WHERE user_id = '{$wp_user_ID}' AND feed_id = '$post->ID'");
        $like_count = $wpdb->get_var("SELECT COUNT(*)
            FROM `user_feed_likes`
            WHERE feed_id = '$post->ID'");

        $comments = [];
        $post_image = null;
        $lookingfriend_value = null;
        $attachment_url = null;
        $video_thumbnail_url = null;
        $location = null;
        $tour_post = null;
        $tour_title = null;
        $event_name = null; // used by post type eventpost
        $event_location = null; // used by post type eventpost
        $start_date = null; // used by post type eventpost
        $event_tour_package = null; // used by post type eventpost
        $tour_img = null; // used by post type eventpost
        $going_count = null; // used by post type eventpost
        $going_status = null; // used by post type eventpost

        if ($post->post_type == 'blog') {
            $post_image = get_template_directory_uri() . '/assets/img/rome.jpg';
            if ($thum_url = get_the_post_thumbnail_url($post->ID)) {
                $post_image = $thum_url;
            } else if (preg_match('!<img.*?src="(.*?)"!', $post->post_content, $post_image_match)) {
                $post_image = $post_image_match[1];
            }

            $comments = $wpdb->get_results("SELECT scm_text comment,scm_user_id user_id,user_login
                FROM `blog_social_comments`,`wp_users`
                WHERE wp_users.ID=scm_user_id AND `post_id` = '{$post->ID}'
                ORDER BY scm_created DESC");
        } else if($post->post_type == 'shortpost') {
            $comments = $wpdb->get_results("SELECT comment,user_id,user_login
                FROM `user_feed_comments`,`wp_users`
                WHERE wp_users.ID=user_id AND `feed_id` = '{$post->ID}'
                ORDER BY user_feed_comments.id DESC");
            $lookingfriend_value = $get_looking_friend($post->ID);

            $attachment = get_post_thumbnail_id($post->ID);
            $attachment_url = '';
            if (empty($attachment)) {
                $attachment = get_post_meta($post->ID, 'attachment', true);
            }
            if (!empty($attachment)) {
                if (wp_attachment_is('image', $attachment)) {
                    $attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
                } else {
                    $attachment_url = wp_get_attachment_url($attachment);
                }
            } else if (!empty(get_post_meta($post->ID, 'tour_img', true))) {
                $attachment_url = get_post_meta($post->ID, 'tour_img', true);
            }

            $video_thumbnail_url = wp_get_attachment_url(get_post_meta($attachment, '_kgflashmediaplayer-poster-id', true));
            $location = get_post_meta($post->ID, 'location', true);
            $tour_post = get_post_meta($post->ID, 'tour_post', true);
            $tour_title = get_post_meta($post->ID, 'tour_title', true);
        } else if($post->post_type == 'eventpost') {
            $comments = $wpdb->get_results("SELECT comment,user_id,user_login
                FROM `user_feed_comments`,`wp_users`
                WHERE wp_users.ID=user_id AND `feed_id` = '{$post->ID}'
                ORDER BY user_feed_comments.id DESC");

            if (get_post_meta($post->ID, 'event_type', true) == 'private') {
                $event_invited_people = get_post_meta($post->ID, 'event_invited_people', true);
                if ($wp_user_ID != $post->post_author && !in_array($wp_user_ID, $event_invited_people)) {
                    continue;
                }
            }

            $event_name = get_post_meta($post->ID, 'event_name', true);
            $event_location = get_post_meta($post->ID, 'event_location', true);
            $attachment_url = get_template_directory_uri() . '/assets/img/rome.jpg';
            $attachment = get_post_meta($post->ID, 'event_location_image_id', true);
            if (!empty($attachment)) {
                $attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
            }
            $start_date = get_post_meta($post->ID, 'start_date', true);
            $event_tour_package = get_post_meta($post->ID, 'event_tour_package', true);
            $tour_img = get_post_meta($post->ID, 'tour_img', true);
            if (empty($tour_img)) {
                $tour_img = 'https://i.travelapi.com/hotels/25000000/24160000/24157900/24157865/e01875da_y.jpg';
            }
            $going_count = $wpdb->get_var("SELECT COUNT(*) FROM `event_going` WHERE post_id = '{$post->ID}' AND status=1");
            $going_status = $wpdb->get_var("SELECT status FROM `event_going` WHERE post_id='{$post->ID}' AND user_id='{$current_user->ID}'");
        }

        foreach($comments as $comment) {
            $comment->avatar_url = get_avatar_url($comment->user_id, 50);
            $comment->display_name = um_get_display_name($comment->user_id);
        }

        $post_author_name = get_the_author_meta( 'display_name' , $post->post_author);
        if(!$post_author_name)
            $post_author_name = get_the_author_meta( 'user_nicename' , $post->post_author);

        $data[] = [
            'ID' => $post->ID,
            'post_title' => $post->post_title,
            'post_date' => $post->post_date,
            'post_type' => $post->post_type,
            'post_content' => $post->post_content,
            'post_content_short' => mb_strimwidth(strip_tags($post->post_content), 0, 230, "..."),
            'liked' => $liked,
            'like_count' => $like_count,
            'comments' => $comments,
            'created_at_text' => human_time_diff(strtotime($post->post_date), current_time('timestamp')),
            'post_author_name' => $post_author_name,
            'avatar' => um_get_avatar_url(um_get_avatar('', $post->post_author, 52)),
            'post_link'=> get_permalink( $post->ID ),
            'meta' => [
                'post_image' => $post_image,
                'lookingfriend_value' => $lookingfriend_value,
                'attachment_url' => $attachment_url,
                'video_thumbnail_url' => $video_thumbnail_url,
                'location' => $location,
                'tour_post' => $tour_post,
                'tour_title' => $tour_title,
                'tour_img' => $tour_img,
                'event_name' => $event_name,
                'event_location' => $event_location,
                'start_date' => $start_date,
                'event_tour_package' => $event_tour_package,
                'going_count' => $going_count,
                'going_status' => $going_status,
            ]
        ];
    }
    return $data;
};

$get_page_num = function() {
    $pageNum = filter_input(INPUT_GET, 'pageNum', FILTER_SANITIZE_NUMBER_INT);
    $pageNum = ($pageNum > 0) ? ($pageNum - 1) : 0;
    return $pageNum;
};

if (isTheseParametersAvailable(array('username', 'token'))) {
    http_response_code(200);

    $data = $get_timeline();
    $response = array(
        'data' => $data,
        'meta' => [
            'size' => count($data),
            'page_num' => $get_page_num()
        ]
    );

} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
