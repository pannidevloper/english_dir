<?php
defined('BASEPATH') or exit('No direct script access allowed');

$data = $wpdb->get_results("SELECT * FROM `wp_covid19_data` ");

if ($data):
    foreach ($data as $single_user) {

    	$user_info = get_userdata($single_user->wp_user_id);

    	if($single_user->lat && $single_user->lng){
    		$lat = $single_user->lat;
    		$lng= $single_user->lng;
    	}
    	else{
    		$lat ='Not found';
    		$lng='Not found';
    	}

    	 $list[] = array(
                'user_id' => $single_user->wp_user_id,
                'username' => $user_info->user_login,
                'avator' =>   um_get_user_avatar_url($single_user->wp_user_id, 'original'),
                'coordiantes' => $lat. ",".$lng,
                'update_date'=> date('m/d/Y', $single_user->datetime)

            );

    }

http_response_code(200);
$response['msg'] = "Successfully";
 $response['list'] = $list;

else:
http_response_code(401);
$response['msg'] = "No Data";

endif;