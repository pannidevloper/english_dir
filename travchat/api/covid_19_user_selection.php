<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (isTheseParametersAvailable(array('username', 'token','user_option','lat','lng'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $user_option = filter_input(INPUT_POST, 'user_option', FILTER_SANITIZE_STRING);

    $lat = filter_input(INPUT_POST, 'lat', FILTER_SANITIZE_STRING);

    $lng = filter_input(INPUT_POST, 'lng', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

     $date = date('m/d/Y h:i:s a', time());
     $timestamp = strtotime($date);
		if ($wp_user_ID) {

			//check either user selected
		$already = $wpdb->get_var("SELECT id FROM `wp_covid19_data` WHERE  wp_user_id='{$wp_user_ID}'");

		if($already){


			$wpdb->update(
			'wp_covid19_data', array(
			'datetime'=>$timestamp,
			'lat'=>$lat,
			'lng'=>$lng,


			), array('id' => $already), array(
			'%s',
			'%s',
			'%s',

			), array('%d')
			);

		http_response_code(200);
		$response['succes'] = 1;
		$response['msg'] = 'User Data updated';

		}
		else{

		$sql = $wpdb->prepare("INSERT INTO `wp_covid19_data` (`wp_user_id`, `user_option`,`datetime`,`lat`,`lng`) values (%d, %s,%s,%s,%s)", $wp_user_ID, $user_option,$timestamp,$lat,$lng);
           $wpdb->query($sql);
		http_response_code(200);
		$response['succes'] = 1;
		$response['msg'] = 'User Data Recorded';
		}

       // $wpdb->query($sql);

		}
		else {

		http_response_code(401);
		$response['succes'] = 0;
		$response['msg'] = 'No user exist';

		}
    }

    else{

    http_response_code(401);
    $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
    }