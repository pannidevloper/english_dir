<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Content-Type: application/json');

$get_my_timeline = function() {
    global $wpdb;

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user`
        WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
    $user = get_user_by('id', $wp_user_ID);

    $tour_packages = [];

    $friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE ID IN(
        SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_user_ID}')
        OR ID IN(
            SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_user_ID}')");

    $fb_sharing_option = get_user_meta( $wp_user_ID, 'fb_sharing_option' , true );
    if(!empty ($fb_sharing_option)){
        $fb_sharing_option = $fb_sharing_option;
    } else{
        $fb_sharing_option = 0;
    }

    if (user_can($user, 'um_travel-advisor') || user_can($user, 'administrator')) {
        $tour_packages = $wpdb->get_results("SELECT ID,post_title, meta_value as tour_id FROM `wp_posts`,`wp_postmeta`
        WHERE post_type='post' AND post_status='publish' AND `meta_key`='tour_id'
        AND wp_posts.ID=wp_postmeta.post_id AND post_author='{$wp_user_ID}'");
    }

    $ratings = $wpdb->get_row("SELECT SUM(CASE WHEN rating=5 THEN 1 ELSE 0 END) as star5,
                                  SUM(CASE WHEN rating=4 THEN 1 ELSE 0 END) as star4,
                                  SUM(CASE WHEN rating=3 THEN 1 ELSE 0 END) as star3,
                                  SUM(CASE WHEN rating=2 THEN 1 ELSE 0 END) as star2,
                                  SUM(CASE WHEN rating=1 THEN 1 ELSE 0 END) as star1
                                FROM social_reviews WHERE user_id='{$wp_user_ID}'", ARRAY_A);

    $rating_people_count = array_sum($ratings);
    $avg_rating = $wpdb->get_var("SELECT AVG(rating) FROM `social_reviews` WHERE user_id='{$wp_user_ID}'");

    $user_photos = $wpdb->get_results("SELECT guid,post_mime_type,ID FROM `wp_posts` WHERE `post_author` = '{$user->ID}'
                                        AND post_status='inherit' AND `post_type` = 'attachment'
                                        ORDER BY `post_date` DESC LIMIT 6");

    $user_location = $wpdb->get_row("SELECT region,country FROM `user` WHERE username='{$user->user_login}'");
    if (!empty($user_location)) {
        $user_location = trim($user_location->region . ', ' . $user_location->country, ',');
    }

    if ($rating_people_count == 0) {
        $avg_rating = 0;
    }

    $location_visiable = get_user_meta($user->ID, 'location_visiable', true);
    if ($location_visiable == 'anyone' || $user->ID == wp_get_current_user()->ID) {
        $location_visiable = true;
    } else if (empty($location_visiable) || $location_visiable == 'family') {
        if (strpos(get_user_meta($user->ID, 'family_member', true), wp_get_current_user()->user_login) !== false) {
            $location_visiable = true;
        } else {
            $location_visiable = false;
        }
    } else if ($location_visiable == 'friend') {
        $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}' AND user2=" . wp_get_current_user()->ID);
        if ($is_friend == 0) {
            $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}' AND user1=" . wp_get_current_user()->ID);
        }
        if ($is_friend > 0) {
            $location_visiable = true;
        } else {
            $location_visiable = false;
        }
    } else if ($location_visiable == 'family_friend') {
        if (strpos(get_user_meta($user->ID, 'family_member', true), wp_get_current_user()->user_login) !== false) {
            $location_visiable = true;
        } else {
            $location_visiable = false;
        }
        if (!$location_visiable) {
            $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}' AND user2=" . wp_get_current_user()->ID);
            if ($is_friend == 0) {
                $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}' AND user1=" . wp_get_current_user()->ID);
            }
            if ($is_friend > 0) {
                $location_visiable = true;
            } else {
                $location_visiable = false;
            }
        }
    } else {
        $location_visiable = false;
    }

    $user_live_location = $wpdb->get_row("SELECT lat,lng FROM `user_location` WHERE ID='{$user->ID}'");

    if ($user_live_location) {

        $latitude = $user_live_location->lat;
        $longitude = $user_live_location->lng;

        $map_url = "https://www.travpart.com/English/map-custom/?lat=" . $latitude . "&&lng=" . $longitude;
    }

    $fill_meta = function($posts) {
    	foreach($posts as $post) {
		    $post_author_name = get_the_author_meta( 'display_name' , $post->post_author);
		    if(!$post_author_name)
		    	$post_author_name = get_the_author_meta( 'user_nicename' , $post->post_author);

    		$post->meta = [
    			'created_at_text' => human_time_diff(strtotime($post->post_date), current_time('timestamp')),
    			'post_author_name' => $post_author_name,
    		];
    	}
    	return $posts;
    };

    $args = array(
        'post_type'  => 'shortpost',
        'author'     =>  $wp_user_ID,
    );
    $posts = get_posts($args);
    $posts = $fill_meta($posts);

    $res->friends = $friends;
    $res->posts = $posts;
    $res->meta = [
        'fb_sharing_option' => $fb_sharing_option,
        'tour_packages' => $tour_packages,
        'ratings' => $ratings,
        'avg_rating' => $avg_rating,
        'user_photos' => $user_photos,
        'user_location' => $user_location,
        'location_visiable' => $location_visiable,
        'user_live_location' => $user_live_location,
        'latitude' => $latitude,
        'longitude' => $longitude,
        'map_url' => $map_url,
    ];

    return $res;
};

if (isTheseParametersAvailable(array('username', 'token'))) {
    http_response_code(200);
    $response['success'] = 1;
    $response['data'] = $get_my_timeline();
} else {

    http_response_code(401);
    $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
}
