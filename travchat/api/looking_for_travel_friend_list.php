<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $location = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING);
    $location_filter = '';
    $item_per_page = 10;
    $page = filter_input(INPUT_POST, 'page', FILTER_VALIDATE_INT, array('options' => array('default' => 1, 'min_range' => 1)));
    $limit = ($page - 1) * $item_per_page . ',' . $item_per_page;

    //filter option
    $hometown = filter_input(INPUT_POST, 'hometown', FILTER_SANITIZE_STRING);
    $age_min = filter_input(INPUT_POST, 'age_min', FILTER_VALIDATE_INT, array('options' => array('default' => 18, 'min_range' => 18, 'max_range' => 65)));
    $age_max = filter_input(INPUT_POST, 'age_max', FILTER_VALIDATE_INT, array('options' => array('default' => 65, 'min_range' => 18, 'max_range' => 65)));
    $gender = filter_input(INPUT_POST, 'gender', FILTER_SANITIZE_STRING);
    $gender_v = (strtolower($gender) == 'female') ? 'Female' : 'Male';
    $marital_status = strtolower(filter_input(INPUT_POST, 'marital_status', FILTER_SANITIZE_STRING));
    $star_min = filter_input(INPUT_POST, 'star_min', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 5)));
    $religion = filter_input(INPUT_POST, 'religion', FILTER_SANITIZE_STRING);

    $filterQuery = '';

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($wp_user_ID)) {
        if (!empty($location)) {
            $location_filter = "AND post_id IN (
                        SELECT post_id FROM `wp_postmeta` WHERE `meta_key`='location' AND `meta_value` LIKE '%{$location}%'
                      )";
        }

        if (!empty($hometown)) {
            $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'hometown' AND `meta_value` LIKE '%{$hometown}%')";
        }
        
        if($age_min>18 || $age_max<65) {
            $filterQuery .= " AND wp_users.ID IN(
                SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'date_of_birth'
                AND TIMESTAMPDIFF(YEAR, meta_value, CURDATE()) BETWEEN {$age_min} AND {$age_max}
            )";
        }

        if(!empty($gender)) {
            $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'gender' AND `meta_value`='{$gender_v}')";
        }

        if(!empty($marital_status)) {
            $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'marital_status' AND `meta_value`='{$marital_status}')";
        }
        
        if(!empty($star_min)) {
            $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `social_reviews` GROUP BY user_id HAVING(AVG(rating)>={$star_min}))";
        }

        if(!empty($religion)) {
            $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'religion' AND `meta_value`='{$religion}')";
        }

        $list = $wpdb->get_results("SELECT wp_users.ID,wp_users.user_login as username,user.country,user.region,wp_posts.ID as post_id, wp_postmeta.meta_value as type
        FROM `wp_posts`,`wp_postmeta`,`wp_users`,`user`
        WHERE wp_users.user_login=user.username AND wp_users.ID=wp_posts.post_author
        AND wp_posts.ID=wp_postmeta.post_id AND meta_key='lookingfriend'
        AND wp_posts.ID IN (
            SELECT MAX(wp_posts.ID) FROM `wp_posts`,`wp_postmeta` WHERE wp_posts.post_status='publish'
            AND wp_posts.ID=wp_postmeta.post_id AND meta_key='lookingfriend' AND wp_posts.ID IN (
                SELECT post_id FROM `wp_postmeta` WHERE meta_key='enddate' AND STR_TO_DATE(meta_value, '%m/%d/%Y')>=NOW()
            ) GROUP BY wp_posts.post_author
        )
        {$location_filter}
        AND wp_users.ID NOT IN(
            SELECT rejected_user_id FROM `match_reject` WHERE user_id={$wp_user_ID}
        )
        AND wp_users.ID NOT IN(
            SELECT user2 FROM `friends_requests` WHERE status=0 AND user1={$wp_user_ID}
        )
        {$filterQuery}
        ORDER BY wp_posts.ID DESC LIMIT {$limit}", ARRAY_A);
        foreach ($list as &$row) {
            $wp_userid = $row['ID'];
            $check_for_location = $wpdb->get_row("SELECT * FROM `user_location` WHERE ID='{$wp_userid}'");
            if ($check_for_location > 0) {
                $lat = $check_for_location->lat;
                $lng = $check_for_location->lng;
            }

            $age = null;
            $date_of_birth = get_user_meta($row['ID'], 'date_of_birth', true);
            if (!empty($date_of_birth)) {
                $date_of_birth = strtotime($date_of_birth);
                if ($data_of_birth < time()) {
                    $age = date('Y') - date('Y', $date_of_birth);
                    if (date('n') < date('n', $date_of_birth)) {
                        $age--;
                    } else if (date('n') == date('n', $date_of_birth) && date('j') < date('j', $date_of_birth)) {
                        $age--;
                    }
                }
            }

            $row['name'] = um_get_display_name($row['ID']);
            $row['avatar'] = um_get_user_avatar_url($row['ID'], 'original');
            $row['start_date'] = date('d M Y',strtotime(get_post_meta($row['post_id'], 'startdate', true)));
            $row['end_date'] = date('d M Y',strtotime(get_post_meta($row['post_id'], 'enddate', true)));
            $row['plan_to_go'] = get_post_meta($row['post_id'], 'location', true);
            $connect_sent = $wpdb->get_var("SELECT status FROM `friends_requests` WHERE user1='{$wp_user_ID}' AND user2='{$row['ID']}'");
            $connect_received = $wpdb->get_var("SELECT status FROM `friends_requests` WHERE user1='{$row['ID']}' AND user2='{$wp_user_ID}'");
            if($connect_sent==1 || $connect_received==1) {
                $connect_status = 'connected';
            }
            else if($connect_sent==='0') {
                $connect_status = 'sent';
            }
            else if($connect_received==='0') {
                $connect_status = 'received';
            }
            else {
                $connect_status = 'unconnected';
            }
            $row['connect_status'] = $connect_status;
            $row['hometown'] = get_user_meta($row['ID'], 'hometown', true);
            $row['current_city'] = get_user_meta($row['ID'], 'current_city', true);
            $row['age'] = $age;
            $row['edu_level'] = get_user_meta($row['ID'], 'edu_level', true);
            $row['job'] = $wpdb->get_var("SELECT job_title FROM `wp_career` WHERE userid={$row['ID']} ORDER BY end_date DESC LIMIT 1");
            $row['school'] = $wpdb->get_var("SELECT school FROM `wp_education` WHERE userid={$row['ID']} ORDER BY enddate DESC LIMIT 1");
            $row['coordiantes'] = empty($lat) ? '' : ($lat . "," . $lng);
        }
        if (!empty($list)) {
            http_response_code(200);
            $response['list'] = $list;
        } else {
            http_response_code(401);
            $response['msg'] = 'There is not any user who is looking for travel friends.';
        }
    } else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
