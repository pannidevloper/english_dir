<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

   $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($wp_user_ID)) {

    $query = $wpdb->prepare("SELECT user1 FROM `friends_requests` WHERE status=0 AND user2=%d", $wp_user_ID);
	$rows = $wpdb->get_results($query, ARRAY_A);

	foreach ($rows as $row) {
		$results[] = array(
			'userid' => $row['user1'],
			'username' => get_user_by('id', $row['user1'])->user_login,
			'name' => um_get_display_name($row['user1']),
			'hometown' => get_user_meta($row['user1'], 'hometown', true),
			'photo' => um_get_user_avatar_url($row['user1']),
		);
	}
	 http_response_code(200);
      //  $response['success'] = "success";
        $response['result'] =$results;
    	
     }
     	else {
        http_response_code(401);
        $response['msg'] = 'No user found';
     }
} 
else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}