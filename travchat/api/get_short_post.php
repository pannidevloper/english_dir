<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Content-Type: application/json');

$get_short_post = function($post_id) {
    global $wpdb;

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user`
        WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    $post = $wpdb->get_row("SELECT wp_posts.ID,post_author,user_login,post_title,post_date,post_type,post_content
        FROM `wp_posts`,`wp_users` WHERE wp_posts.ID='{$post_id}' AND post_author=wp_users.ID");
    $liked = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE user_id = '{$wp_user_ID}' AND feed_id = '{$post_id}'");
    $like_count = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE feed_id = '{$post_id}'");

    $comments = $wpdb->get_results("SELECT comment,user_id,user_login FROM `user_feed_comments`,`wp_users` WHERE wp_users.ID=user_id AND `feed_id` = '{$post->ID}' ORDER BY user_feed_comments.id DESC");

    $attachment = get_post_thumbnail_id($value->ID);
    $attachment_url = '';
    if (empty($attachment)) {
        $attachment = get_post_meta($post->ID, 'attachment', true);
    }
    if (!empty($attachment)) {
        if (wp_attachment_is('image', $attachment)) {
            $attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
        } else {
            $attachment_url = wp_get_attachment_url($attachment);
        }
    } else if (!empty(get_post_meta($post->ID, 'tour_img', true))) {
        $attachment_url = get_post_meta($post->ID, 'tour_img', true);
    }
    $tour_post_id = get_post_meta($post->ID, 'tour_post', true);

    // short post view counter
    function setshortPostViews($post_id) {
        $count_key = 'post_views_count';
        $count = get_post_meta($post_id, $count_key, true);
        if($count==''){
            $count = 0;
            delete_post_meta($post_id, $count_key);
            add_post_meta($post_id, $count_key, '0');
        }else{
            $count++;
            update_post_meta($post_id, $count_key, $count);
        }
    }
    setshortPostViews($post_id);
    $author_id = get_post_field('post_author', $post->ID);

    if(function_exists('visitor_profile_logs') AND is_user_logged_in()) {
        visitor_profile_logs ('short_post_'.$post_id,$author_id,$wp_user_ID );
    }

    $res = $post;
    $res->meta = [
        'attachment_url' => $attachment_url,
        'liked' => $liked,
        'like_count' => $like_count,
    ];
    $res->comments = $comments;

    return $res;
};

if (isTheseParametersAvailable(array('username', 'token'))) {
    $post_id = filter_input(INPUT_GET, 'post_id', FILTER_SANITIZE_STRING);

    http_response_code(200);
    $response['success'] = 1;
    $response['data'] = $get_short_post($post_id);
} else {

    http_response_code(401);
    $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
}
