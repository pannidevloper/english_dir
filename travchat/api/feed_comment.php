<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (isTheseParametersAvailable(array('username', 'token', 'feed_id','comment'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $feed_id = filter_input(INPUT_POST, 'feed_id', FILTER_SANITIZE_STRING);
    $feed_comment = filter_input(INPUT_POST, 'comment', FILTER_SANITIZE_STRING);

$wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

   if ($wp_user_ID) {
   if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_posts` WHERE `ID` = '{$feed_id}'") < 1 || $feed_comment=='') {
        
        http_response_code(401);
        $response['success'] = 0;
        $response['msg'] = 'Feed Post Not Exist!';
       
    } 
    else {

    	$wpdb->insert(
            'user_feed_comments',
            array(
                'feed_id' => $feed_id,
                'user_id' => $wp_user_ID,
                'comment' => $feed_comment
            )
        );

         http_response_code(200);
          $response['success'] = 1;
         $response['msg'] = 'Successfully comment posted!';


    }
}
	else{
		http_response_code(401);
		$response['success'] = 0;
        $response['msg'] = 'User does not exist';

	}

}
else{
	 http_response_code(401);
	 $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
}