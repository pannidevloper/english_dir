<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('user_name', 'token','user_id'))) {

    $today_date = date("d/m/Y H:i:s");
    $today_date =date ("d-m-Y");

    $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $wp_user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);

    $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$user_name}' AND token='{$token}'");

    if (!empty($userid)) {

        $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");

        $list = $wpdb->get_results("SELECT * FROM wp_user_plans WHERE user_id = '{$wp_user_id}'  ORDER BY id DESC");

        if ($list) {

        // 1st user get friends
        $mut1 = "

        SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_userid}'

        UNION

        SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_userid}'";

        $rt1 = $wpdb->get_results($mut1);

        foreach ($rt1 as $value) {

        if ($value->user2 == $wp_userid)
        continue;
        $friends_list1[] = $value->user2;
        }
        }

        if ($list) {

           foreach ($list as $plan) {

            
            
            $mut2 = "

            SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$plan->user_id}'

            UNION

            SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$plan->user_id}' ";
            $rt2 = $wpdb->get_results($mut2);

            foreach ($rt2 as $value) {


            $friends_list2[] = $value->user2;

            }
            


            $match = array_intersect($friends_list1, $friends_list2);

            foreach ($match as $match_id) {

            $wp_user_name= $wpdb->get_var("SELECT user_login FROM `wp_users` WHERE ID='{$match_id}'");

            $user_data =array(
            'user_id'=>$match_id,
            'username'=>$wp_user_name,
            'avatar'=> um_get_user_avatar_url($match_id, 80)
            );

            }


            $date = explode('/', $plan->end_date);
            $converted_Date  = date('d-m-Y', strtotime(implode('-', array_reverse($date))));

            if ( strtotime( $today_date )> strtotime( $converted_Date )){
            continue;
            }

            // starting in days code
        $dt1 = explode('/', $plan->start_date);
        $converted_Date  = date('d-m-Y', strtotime(implode('-', array_reverse($dt1))));
        $dt1 = strtotime($converted_Date);
        $dt2 =   strtotime($today_date);
          $days = ($dt1 - $dt2) / 60 / 60 / 24;

                $plans[] = array(

                    'start_date' => $plan->start_date,
                    'end_Date' => $plan->end_date,
                    'days' => $plan->no_of_days,
                    'location' => $plan->location,
                    'plan_id' => $plan->id,
                    'user_id' => $plan->user_id,
                     'friend_name' => um_get_display_name($plan->user_id),
                     'avatar' => um_get_user_avatar_url($plan->user_id, 80),
                     'starting_in_days' => $days . "days",
                     'coordiantes' => $plan->lat . "," . $plan->lng,
                     'mut_friends_count' => count($match),
                      'mut_friends_data' => $user_data,
                      'given_name'=>$plan->given_name,
                       'notes'=>$plan->notes
                );
            }

            http_response_code(200);
            $response['msg'] = 'succes';
            $response['list'] = $plans;
        } else {

            http_response_code(200);
            $response['msg'] = 'User have no plans';
        }
    } else {

        http_response_code(200);
        $response['msg'] = 'username  not exist!';
    }
} else {

    http_response_code(401);
    $response['msg'] = 'Required Parameter missing';
}
