<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token', 'receiver_user_id'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $receiver_user_id =  filter_input(INPUT_POST, 'receiver_user_id', FILTER_SANITIZE_STRING);
    // check either receiver user id exist

    $user = get_userdata($receiver_user_id);

$current_user = wp_get_current_user();

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_ID and !empty($receiver_user_id) and $user != false) {

        $sql = $wpdb->prepare("INSERT INTO `friends_requests` (`user1`, `user2`,`action_by_user`) values (%d, %d,%d)", $wp_user_ID, $receiver_user_id, $wp_user_ID);

        $wpdb->query($sql);

        $msg2 ="<a href='https://www.travpart.com/English/connection-request-list/'>".um_get_display_name($current_user->ID)." sent you friend Request</a>";
                $wpdb->insert('notification', array('wp_user_id'=>$receiver_user_id, 'content'=>$msg2,'identifier'=>'friend_request_received_'.$current_user->user_login));

        $token =  to_get_user_device_token_from_wp_id_for_api($receiver_user_id);

        if ($token) {
            $msg =  um_get_display_name( $wp_user_ID ). " Sent you friend request";
            $identifier = "friends_request_received_".$current_user->user_login;
            notification_for_user_form_api($token, $msg, $identifier);
        }

        http_response_code(200);
        $response['success'] = 1;
        $response['msg'] = "friend request sent successfully";
    } else {

        http_response_code(401);
        $response['success'] = 0;
        $response['msg'] = 'Some error occured';
    }
} else {

    http_response_code(401);
    $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
}
