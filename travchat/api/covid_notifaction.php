<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (isTheseParametersAvailable(array('ID'))) {
 $ID = filter_input(INPUT_POST, 'ID', FILTER_SANITIZE_STRING);
$res = mysqli_query($conn, "SELECT lat,lng FROM `user_location` WHERE ID='{$ID}'");

		if (mysqli_num_rows($res)>0){

		$user = mysqli_fetch_assoc( $res );
		http_response_code( 200 );
		$response['msg'] = 'Success';
		$response['list'] = $user;
		}
		else{
		http_response_code(401);
		$response['msg'] = 'User location not found';
		}
}
else{
	 http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}