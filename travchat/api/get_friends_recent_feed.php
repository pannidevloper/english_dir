<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_ID) {
        $friends_feed = $wpdb->get_results("SELECT wp_posts.ID,post_author,user_login as username,post_date
        FROM `wp_posts`,`wp_users` WHERE
        post_author=wp_users.ID
        AND post_date IN (
            SELECT MAX(post_date)
            FROM `wp_posts`,`wp_postmeta` WHERE
            (post_author IN (SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_user_ID}')
            OR post_author IN(SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_user_ID}'))
            AND wp_posts.ID=wp_postmeta.post_id AND meta_key='attachment'
            AND meta_value IN (SELECT ID FROM `wp_posts` WHERE post_mime_type LIKE '%image%')
            AND `post_type`='shortpost' AND post_status='publish'
            GROUP by post_author)
        ORDER BY post_date DESC LIMIT 5", ARRAY_A);
        foreach ($friends_feed as &$row) {
            $row['user_photo'] = um_get_user_avatar_url($row['post_author']);
            $row['photo'] = wp_get_attachment_url(get_post_meta($row['ID'], 'attachment', true));
            unset($row['ID']);
            unset($row['post_author']);
            unset($row['attachment_id']);
        }
        http_response_code(200);
        $response['feeds'] = $friends_feed;
    } else {
        http_response_code(401);
        $response['msg'] = 'User does not exist';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
