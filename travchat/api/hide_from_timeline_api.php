<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token','post_id'))) {

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $post_id = filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_STRING);



    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($wp_user_ID) {


       $already_exist = $wpdb->get_row("SELECT * FROM short_posts_hide_from_timeline WHERE short_post_id='{$post_id}' AND user_id='{$wp_user_ID}'");

       if(empty($already_exist)){

          $r=  $wpdb->insert(
            'short_posts_hide_from_timeline',
            array(
            'short_post_id' => $post_id,
            'user_id' => $wp_user_ID
            ),
            array(
            '%d',
            '%d'
            )
            );

        http_response_code(200);
        $response['success'] = 1;
        $response['msg'] = 'successfully hidden from timeline';

    }
    else{
    http_response_code(400);
    $response['success'] = 0;
    $response['msg'] = 'Already Hidden from timeline';
       }


      

       
    }
    else {

    http_response_code(400);
    $response['success'] = 0;
    $response['msg'] = 'No user found';
    }
    } else {

    http_response_code(401);
    $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
    }
