<?php
defined('BASEPATH') or exit('No direct script access allowed');

function to_get_user_lat_lng($userid)
{
    global $wpdb;
    $value = $wpdb->get_row("SELECT * FROM `user_location` WHERE ID='{$userid}'");

    if ($value) {

        $coordiantes = $value->lat . "," . $value->lng;
    } else {
        $coordiantes = "No coordiantes found";
    }

    return $coordiantes;
}

if (isTheseParametersAvailable(array('userid', 'miles', 'lat', 'lng'))) {

    $userid = filter_input(INPUT_POST, 'userid', FILTER_SANITIZE_STRING);
    $miles = filter_input(INPUT_POST, 'miles', FILTER_SANITIZE_STRING);
    $user_lat = filter_input(INPUT_POST, 'lat', FILTER_SANITIZE_STRING);;
    $user_lng = filter_input(INPUT_POST, 'lng', FILTER_SANITIZE_STRING);;

    $all_users = $wpdb->get_results("SELECT * FROM `user_location`");

    foreach ($all_users as $user) {


        $calculated_miles = API_User_distance($user_lat, $user_lng, $user->lat, $user->lng, "M");

        if ($calculated_miles < $miles && $calculated_miles != 0) {

            $near_friends_array[] = $user->ID;
        }
    } // end of foreach


    // now we have friend ids,lets get info from user table
    if ($near_friends_array) {
        foreach ($near_friends_array as $friend_id) {

            $value = $wpdb->get_row("SELECT * FROM `wp_users` WHERE ID='{$friend_id}'");
            $Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user1 = '$user_id' AND user2 = '{$value->ID}'");

            if (empty($Data)) {

                $reverse_check = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '$user_id' AND user1 = '{$value->ID}'");

                if (!empty($reverse_check) and isset($reverse_check)) {

                    $button_text = "Connected";
                } else {
                    $button_text = 'Connect';
                }

                // end of if
            } else {

                if ($Data->status == "0") {

                    $button_text = "Pending";
                } elseif ($Data->status == "1") {

                    $button_text = "Connected";
                } elseif ($Data->status == "2") {

                    $button_text = "Blocked";
                }
            }




            $list[] = array(

                'userid' => $value->ID,
                'username' => $value->user_nicename,
                'hometown' => get_user_meta($value->ID, 'hometown', true),
                'photo' => um_get_user_avatar_url($value->ID),
                'status' => $button_text,
                'coordiantes' => to_get_user_lat_lng($value->ID)
                //   'miles'=>$calculated_miles

            );
        }
        http_response_code(200);
        $response['success'] = 1;
        $response['msg'] = 'suggestions';
        $response['result'] = $list;
    } else {
        http_response_code(401);
        $response['success'] = 0;
        $response['msg'] = 'No near friends';
    }
} else {
    http_response_code(401);
    $response['success'] = 0;
    $response['msg'] = 'Required parameter missing';
}
