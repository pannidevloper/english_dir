<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('name', 'email', 'photo', 'connector'))) {

    $user_name = $_REQUEST['name'];
    $user_email = $_REQUEST['email'];
    $user_avt = $_REQUEST['photo'];
    $connector = $_REQUEST['connector'];
    $pass  = $_REQUEST['pass'];

    if (empty($pass)) {
        $pass = wp_generate_password();
        $password = md5($pass);
    }


    if ($wpdb->get_var("SELECT COUNT(*) from wp_users where user_email = '{$user_email}'") and $wpdb->get_var("SELECT COUNT(*) from user where email = '{$user_email}'")) {

        $query2 = mysqli_query($conn, "SELECT * FROM `user` WHERE email='{$user_email}'");
        $row = mysqli_fetch_array($query2);
        $userid = $row['userid'];

        if ($wpdb->get_var("SELECT COUNT(*) from social_login_data where email = '{$user_email}'")) {
            mysqli_query($conn, "UPDATE social_login_data SET image='{$user_avt}' WHERE email='{$user_email}'");
        } else {

            mysqli_query($conn, "insert into `social_login_data` (user_id,email,name,connector,image) values ('$userid','$user_email','$user_nicename','$connector','$user_avt')");
        }

        $token = md5($pass . time());
        mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");

        $user = array(
            'userid' => $row['userid'],
            'username' => $row['username'],
            'email' => $row['email'],
            'phone' => $row['phone'],
            'uname' => $row['uname'],
            'photo' => $user_avt,
            'country' => $row['country'],
            'region' => $row['region'],
            'timezone' => $row['timezone'],
            'access' => $row['access'],
            'token' => $token
        );
        $device_token =   to_get_user_device_token_from_wp_id_for_api ( $row['wp_user_id'] );

         $profile_completeness=  get_user_meta($row['wp_user_id'],'profile_completeness',true);
         $profile_completeness_value =  $profile_completeness*100;
         if(empty( $profile_completeness ) || $profile_completeness_value<50){
          

           $msg2="<a href='https://www.travpart.com/English/user/?profiletab=profile'>Your profile is still incomplete, finish your profile now so your friends can find you</a>";

         $wpdb->insert('notification', array('wp_user_id'=>$row['wp_user_id'], 'content'=>$msg2,'identifier'=>'incomplete_profile'));
      }
         if($device_token) {
          
            if($profile_completeness_value<50){
            notification_for_user_form_api ( $device_token,"Your profile is still incomplete, finish your profile now so your friends can find you",'incomplete_profile' );
        }

         }
        http_response_code(200);
        $response = $user;

        /*
            //mysqli_query($conn, "UPDATE wp_users SET user_pass='{$passmd5}' WHERE user_email='{$user_email}'");
            $creds = array(
                'user_login'    => $user_name,
                'user_password' => $pass
            );
            $user = wp_signon( $creds, false );
            if ( is_wp_error( $user ) ) {
                $response['msg']=  $user->get_error_message();
            }
            else
            {
                $response['msg'] =$user;
            }
            */
    } else {
        $userdata = array(
            'user_login'  =>  $user_name,
            'user_nicename' => $user_name,
            'user_email' => $user_email,
            'display_name' => $user_name,
            'user_pass'   =>  $pass  // When creating a new user, `user_pass` is expected.
        );


        $user_id = wp_insert_user($userdata);
        wp_update_user(array('ID' => $user_id, 'role' => 'um_clients'));


        $access = 2;
        $activated = 1;
        mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated) values ('$user_name', '$user_name','$user_email', '$password', '$access','$activated')");
        $query2 = mysqli_query($conn, "SELECT * FROM `user` WHERE email='{$user_email}'");
        $row = mysqli_fetch_array($query2);
        $userid = $row['userid'];


        mysqli_query($conn, "insert into `social_login_data` (user_id,email,name,connector,image) values ('$userid','$user_email','$user_nicename','$connector','$user_avt')");

        $token = md5($pass . time());
        mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");
        $user = array(
            'userid' => $row['userid'],
            'username' => $row['username'],
            'email' => $row['email'],
            'phone' => $row['phone'],
            'uname' => $row['uname'],
            'photo' => $user_avt,
            'country' => $row['country'],
            'region' => $row['region'],
            'timezone' => $row['timezone'],
            'access' => $row['access'],
            'token' => $token
        );
        $device_token =   to_get_user_device_token_from_wp_id_for_api ( $row['wp_user_id'] );

         $profile_completeness=  get_user_meta($row['wp_user_id'],'profile_completeness',true);
         $profile_completeness_value =  $profile_completeness*100;
          if(empty( $profile_completeness ) || $profile_completeness_value<50){
          


           $msg2="<a href='https://www.travpart.com/English/user/?profiletab=profile'>Your profile is still incomplete, finish your profile now so your friends can find you</a>";

         $wpdb->insert('notification', array('wp_user_id'=>$row['wp_user_id'], 'content'=>$msg2,'identifier'=>'incomplete_profile'));
      }
         if($device_token) {
          
            if($profile_completeness_value<50){
            notification_for_user_form_api ( $device_token,"Your profile is still incomplete, finish your profile now so your friends can find you",'incomplete_profile' );
        }

         }
        http_response_code(200);
        $response = $user;

        /*
            $creds = array(
                'user_login'    => $user_name,
                'user_password' => $pass
            );
            $user = wp_signon( $creds, false );
            if ( is_wp_error( $user ) ) {
                $response['msg']=  $user->get_error_message();
            }
            else
            {
                $response['msg'] =$user;
            }
            */
    }
}
