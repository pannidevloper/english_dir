<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) { 
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if (!empty($wp_user_ID)) {
        $user= get_user_by('id', $wp_user_ID);
        $userResults=array();
        $avatar = um_get_user_avatar_data($user->ID);
            $userResults[] = array(
                'userid' => $user->ID,
                'username' => $user->user_login,
                'name' => um_get_display_name($user->ID),
                'photo' => um_get_user_avatar_url($user->ID),
                'hometown' => get_user_meta($user->ID, 'hometown', true),
                'profile' => home_url('user/' . $user->user_login)
            );
        
        $response['userResults'] = $userResults;

        http_response_code(200);
        $response['msg'] = 'success';
    } else {
        http_response_code(401);
        $response['msg'] = 'Incorrect token';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
