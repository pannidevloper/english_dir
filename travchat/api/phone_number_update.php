<?php
function validate_phone_number($phone)
{
     // Allow +, - and . in phone number
     $filtered_phone_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
     // Remove "-" from number
     $phone_to_check = str_replace("-", "", $filtered_phone_number);

     // Check the lenght of number
     // This can be customized if you want phone number from a specific country
     if (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14) {
        return false;
     } else {
       return true;
     }
}
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token', 'number'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $number = filter_input(INPUT_POST, 'number', FILTER_SANITIZE_STRING);

    $_user_ID = $wpdb->get_var("SELECT user.userid FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

    if ($_user_ID) {

    	$result = validate_phone_number ($number);

    	if($result){
    		$success = mysqli_query($conn, "UPDATE `user` SET phone= '{$number}' WHERE userid={$_user_ID}");
			if($success):
				http_response_code(200);
                $response['success'] = 1;
				$response['msg'] = 'Number Updated';
			else:
				http_response_code(401);
                $response['success'] = 0;
                 $response['msg'] = 'Some error occured';
             endif;

    	}
    	else{

    	http_response_code(401);
        $response['success'] = 0;
        $response['msg'] = 'Wrong number Formate';

    	}

    	/*
    	mysqli_query($conn, "UPDATE `user` SET phone= '{$number}' WHERE userid={$_user_ID}");
    	http_response_code(200);
        $response['msg'] = 'updated';
        */
    }
    else{
    	 http_response_code(401);
         $response['success'] = 0;
        $response['msg'] = 'User does not exist';
    }

}
else{
	http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}