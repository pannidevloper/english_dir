<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (is_user_logged_in()) {
    $keyword = filter_input(INPUT_GET, 'keyword', FILTER_SANITIZE_STRING);
    $current_user = wp_get_current_user();
    $userList=get_users(
        array('search' =>'*'.$keyword.'*',
        'search_columns'=>array('ID','user_login','user_nicename','display_name'),
        'exclude'=>array($current_user->ID),
        'number'=>10)
    );
    $userResults=array();
    foreach ($userList as $user) {
        $user_id = $wpdb->get_var("SELECT userid FROM user WHERE username='{$user->user_login}' LIMIT 1");
        $userResults[] = array(
            'userid' => $user_id,
            'username' => $user->user_login,
            'name' => um_get_display_name($user->ID)
        );
    }
    $response['users'] = $userResults;
}
