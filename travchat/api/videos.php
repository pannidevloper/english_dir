<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Content-Type: application/json');

function get_videos($wp_user_ID, $pageNum)
{
    global $wpdb;
    
    $pageNum = ($pageNum > 0) ? ($pageNum - 1) : 0;
    $post_per_page = 15;
    $postOffset = $pageNum * $post_per_page;
    $videoPosts = $wpdb->get_results("SELECT wp_posts.ID,post_author,user_login,post_title,post_date,post_content
        FROM `wp_posts`,`wp_postmeta`,`wp_users`
        WHERE wp_posts.ID=wp_postmeta.post_id
            AND post_author=wp_users.ID
            AND `meta_key`='attachment'
            AND post_type='shortpost'
            AND post_status='publish'
          AND meta_value IN (SELECT ID FROM `wp_posts` WHERE `post_mime_type` LIKE '%video%')
          AND wp_posts.ID NOT IN(SELECT short_post_id FROM `short_posts_hide_from_timeline` WHERE user_id='{$wp_user_ID}') 
        ORDER BY post_date DESC LIMIT {$postOffset},{$post_per_page}");

    foreach ($videoPosts as $post) {
        $post->name = um_get_display_name($post->post_author);
        $post->avatar = um_get_user_avatar_url($post->post_author, 'original');
        $post->short_post_link = get_permalink($post->ID);
        $liked = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE user_id = '{$wp_user_ID}' AND feed_id = '$post->ID'");
        $like_count = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE feed_id = '$post->ID'");

        $attachment = get_post_thumbnail_id($value->ID);
        $attachment_url = '';
        if (empty($attachment)) {
            $attachment = get_post_meta($post->ID, 'attachment', true);
        }
        if (!empty($attachment)) {
            if (wp_attachment_is('image', $attachment)) {
                $attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
            } else {
                $attachment_url = wp_get_attachment_url($attachment);
            }
        } elseif (!empty(get_post_meta($post->ID, 'tour_img', true))) {
            $attachment_url = get_post_meta($post->ID, 'tour_img', true);
        }
        $tour_post_id = get_post_meta($post->ID, 'tour_post', true);

        $comments = $wpdb->get_results("SELECT comment,user_id,user_login FROM `user_feed_comments`,`wp_users` WHERE wp_users.ID=user_id AND `feed_id` = '{$post->ID}' ORDER BY user_feed_comments.id DESC");
        foreach ($comments as $comment) {
            $comment->avatar_url = get_avatar_url($comment->user_id, 50);
            $comment->display_name = um_get_display_name($comment->user_id);
        }

        $post->created_at_text = human_time_diff(strtotime($post->post_date), current_time('timestamp'));

        $post->meta = [
            'liked' => $liked,
            'like_count' => $like_count,
            'attachment_url' => $attachment_url,
            'location' => get_post_meta($post->ID, 'location', true),
            'tour_post_id' => $tour_post_id,
            'tour_title' => get_post_meta($post->ID, 'tour_title', true),
            'tour_post_link'=> get_permalink( $tour_post_id )
        ];

        $post->comments = $comments;
    }

    return $videoPosts;
};

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
    $pageNum = filter_input(INPUT_POST, 'pageNum', FILTER_SANITIZE_NUMBER_INT);
    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
    
    if ($wp_user_ID > 0) {
        http_response_code(200);
        $response = array(
            'data' => get_videos($wp_user_ID, $pageNum)
        );
    }
    else {
        http_response_code(401);
        $response['msg'] = 'Token is not correct';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
