<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING); 

 $user_id = $wpdb->get_var("SELECT userid FROM `user` WHERE  username='{$username}' AND token='{$token}'");


$ip_address = $_SERVER['REMOTE_ADDR'];
 $now = date("Y-m-d H:i:s");
 if( $user_id){
$session_check =  mysqli_query($conn2, "SELECT * from  `sessions`   WHERE user_id='{$user_id}'");

if (mysqli_num_rows($session_check) == 0) {

	http_response_code(401);
	$response['success'] = 0;
    $response['msg'] = 'User Login recode not found';

}
else
{
	$sessions_query = "UPDATE `sessions` SET last_activity  = '{$now}' WHERE `user_id` = {$user_id}";
	$result = mysqli_query($conn, $sessions_query);

	http_response_code(200);
	$response['success'] = 1;
    $response['msg'] = 'Session time Updated';
}


}

else
{
	http_response_code(200);
	$response['success'] = 0;
    $response['msg'] = 'User not found';
}
}
else
{

	http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}