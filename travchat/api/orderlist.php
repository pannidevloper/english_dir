<?php
defined('BASEPATH') or exit('No direct script access allowed');

// if(isTheseParametersAvailable(array('access', 'agentid'))){
//  if($_REQUEST['access'] == 1){
//      $temp = array();
//      $agent_id=$_REQUEST['agentid'];
//      $orders=mysqli_query($conn, "SELECT * FROM `orders` WHERE `agent_id` = {$agent_id}");
//      while($row = mysqli_fetch_array($orders, MYSQL_ASSOC)){
//          $bookcode = $row['bookingcode'];
//          $saleamt = $row['total_price'];
//          $commission = $row['total_price']*0.08;
//          $temp[$bookcode]=array('saleamt' => $saleamt, 'commission' => $commission);
//      }
//      http_response_code(200);
//      $response['data'] = $temp;
//  }else if($_REQUEST['access'] == 2){
//      $customer_id=$_REQUEST['agentid'];
//      $temp = array();
//      $orders=mysqli_query($conn, "SELECT orders.*, user.uname FROM orders,user WHERE orders.customer_id={$customer_id} AND user.userid=orders.agent_id");
//      while($row = mysqli_fetch_array($orders, MYSQL_ASSOC))
//      {
//          $temp[$row['bookingcode']]=array('nominalamt'=>$row['total_price'], 'agentname'=>$row['uname']);
//      }
//      http_response_code(200);
//      $response['data'] = $temp;
//  }else{
//      http_response_code(200);
//      $response['message'] = 'Provide access code';
//  }
// }

if (empty($_REQUEST['username'])) {
    http_response_code(401);
    $response['msg'] = "provide username";
    // echo json_encode(array("msg" => "provide username"));
} else {
    $username = check_input($_REQUEST['username']);
    $token = check_input($_REQUEST['token']);
    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $res1 = mysqli_query($conn, "SELECT * FROM `wp_users` WHERE user_nicename='{$username}'");
    $user = mysqli_fetch_assoc($res);
    $user1 = mysqli_fetch_assoc($res1);
    /* echo "<pre>";
          print_r($user);
          exit; */
    if (empty($user)) {
        http_response_code(401);
        $response['msg'] = "No user found";
        // echo json_encode(array("error_msg" => "No user found"));
    } else {
        if ($user['access'] == 1) {
            $agent_id = $user1['ID'];



            $orders = mysqli_query($conn, "SELECT t.id tour_id,t.confirm_payment,t.total,p.post_id,p.meta_key FROM (wp_tour t LEFT JOIN wp_postmeta p
                                    ON (t.id=p.meta_value AND p.meta_key='tour_id')) INNER JOIN wp_tourmeta m
                                    ON (t.id=m.`tour_id` AND m.meta_key='userid' AND m.meta_value='{$agent_id}')");

            // Added on 2 May 2019
            //$orders = mysqli_query($conn, "SELECT * FROM `orders` WHERE `agent_id` = {$agent_id}");
            $agent_name = $username;
            $orderlist = array();
            // while ($row = mysqli_fetch_array($orders, MYSQL_ASSOC)) {
            while ($row = mysqli_fetch_assoc($orders)) {

                $confirm_payment = $wpdb->get_row("SELECT confirm_payment FROM `wp_tour` WHERE `id` = " . $row["tour_id"]);

                $af_ratting = 0;
                $ac_ratting = 0;
                $rattingDataByTour = $wpdb->get_results('SELECT * FROM agent_feedback WHERE af_tour_id = ' . $row["tour_id"], ARRAY_A);
                if (!empty($rattingDataByTour)) {
                    $af_ratting = $rattingDataByTour[0]['af_ratting'];
                }

                $rattingDataByTourForMine = $wpdb->get_results('SELECT * FROM agent_client_feedback WHERE = ' . $row["tour_id"], ARRAY_A);
                if (!empty($rattingDataByTourForMine)) {
                    $ac_ratting = $rattingDataByTour[0]['ac_ratting'];
                }

                if ($confirm_payment == 1)
                    $confirm_payment = TRUE;
                else
                    $confirm_payment = FALSE;
                array_push($orderlist, array(
                    'agentname' => $agent_name,
                    'bookingcode' => 'BALI' . $row['tour_id'],
                    'total_price' => intval($row['total']),
                    'commission' => round($row['total'] * 0.08, 2),
                    'confirm_payment' => $row['confirm_payment'],
                    'invoice_link' => (($row['confirm_payment'] == 1) ? ('https://www.travpart.com/English/wp-content/themes/bali/api.php?action=getinvoice&agent_name=' . $agent_name . '&tourid=' . (int) $row['tour_id']) : ''),
                    'FeedbackFromSellers' => $af_ratting,
                    'FeedbackToSellers' => $ac_ratting
                ));
            }
            http_response_code(200);
            // echo json_encode(array('msg' => 'success', 'orderlist' => $orderlist));
            $response['msg'] = "success";
            $response['orderlist'] = $orderlist;
        } else if ($user['access'] == 2) {
            $customer_id = $user['userid'];

            $orders = mysqli_query($conn, "SELECT orders.*, user.uname FROM orders,user WHERE orders.customer_id={$customer_id} AND user.userid=orders.agent_id");

            //  $orders = mysqli_query($conn, "SELECT * FROM orders WHERE `customer_id`= customer_id={$customer_id}"); 


            $orderlist = array();
            while ($row = mysqli_fetch_array($orders)) {
                array_push($orderlist, array(
                    'agentname' => $row['uname'],
                    'bookingcode' => $row['bookingcode'],
                    'total_price' => round($row['total_price'], 2),
                    'commission' => NULL,
                    'confirm_payment' => NULL,
                    'invoice_link' => NULL
                ));
            }

            http_response_code(200);
            $response['msg'] = "success";
            $response['orderlist'] = $orderlist;
        } else {
            http_response_code(401);
            $response['msg'] = "user does not have access permission";
        }
    }
}
