<?php
defined('BASEPATH') or exit('No direct script access allowed');

// sort by days
function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
        }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

$currencyName = filter_input(INPUT_GET, 'currency', FILTER_SANITIZE_STRING);
$currencyList = get_option('currency_list');
if (is_array($currencyList[$currencyName])) {
    $currency = $currencyList[$currencyName]['rate'];
} else if(is_array($currencyList['usd'])) {
    $currencyName = 'usd';
    $currency = $currencyList[$currencyName]['rate'];
} else {
    $currencyName = 'idr';
    $currency = '1';
}

// sort by score
$popularity_order = check_input($_REQUEST['popularity_order']);

// sort by cost
$price_order = $_REQUEST['price_order'];

$duration_order = $_REQUEST['duration_order'];

// Site Url
$site_url = $wpdb->get_var("SELECT option_value FROM tourfrom_balieng2.wp_options WHERE option_name='siteurl'");

$sql = "SELECT wp_tour.*,wp_tour_sale.number_of_people,wp_tour_sale.netprice,wp_tour_sale.username,wp_tourmeta.meta_value
    FROM tourfrom_balieng2.wp_tour,tourfrom_balieng2.wp_tour_sale,tourfrom_balieng2.wp_tourmeta
    WHERE
    wp_tour.id=wp_tour_sale.tour_id
    AND wp_tourmeta.tour_id=wp_tour_sale.tour_id
    AND wp_tourmeta.meta_key='bookingdata'
    AND username!='agent2' AND username!='Lix' AND total!=0";

if (!empty($popularity_order) and $popularity_order == 'htl') {
    $sql .= ' ORDER BY wp_tour.score DESC';
} elseif (!empty($popularity_order) and $popularity_order == 'lth') {
    $sql .= ' ORDER BY wp_tour.score ASC';
} elseif (!empty($price_order) and $price_order == 'htl') {
    $sql .= ' ORDER BY wp_tour.total DESC';
} elseif (!empty($price_order) and $price_order == 'lth') {
    $sql .= ' ORDER BY wp_tour.total ASC';
} else {
    $sql .= ' ORDER BY wp_tour.score DESC';
}

$data = $wpdb->get_results($sql);
foreach ($data as $t) {
    $post_status = $wpdb->get_row("SELECT post_status FROM tourfrom_balieng2.wp_posts,tourfrom_balieng2.wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';")->post_status;

    if ($post_status != 'publish')
        continue;

    $postdata = $wpdb->get_row("SELECT  ID,post_title FROM tourfrom_balieng2.wp_posts,tourfrom_balieng2.wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';");
    $post_ID = $postdata->ID;
    $post_title = $postdata->post_title;
    $cost = ceil((float) $t->total * $currency);

    $hotelinfo = $wpdb->get_row("SELECT img,ratingUrl,location,start_date FROM tourfrom_balieng2.wp_hotel WHERE tour_id='{$t->id}'");

    if (!empty($hotelinfo->img)) {
        if (is_array(unserialize($hotelinfo->img))) {
            foreach (unserialize($hotelinfo->img) as $hotelImgItem)
                $images[] = $hotelImgItem;
        } else
            $images[] = $hotelinfo->img;
    }
    $post_content = $wpdb->get_row("SELECT post_content FROM tourfrom_balieng2.wp_postmeta, tourfrom_balieng2.wp_posts WHERE wp_posts.ID=wp_postmeta.post_id AND `meta_key`='tour_id' AND `meta_value`='{$t->id}'")->post_content;

    if (preg_match('!<img.*?src="(.*?)"!', $post_content, $post_image_match))
        $images[] = $post_image_match[1];

    if (empty($post_image_match[1]))
        $post_image_match[1] = "https://i.travelapi.com/hotels/1000000/910000/903000/902911/26251f24_y.jpg";

    $agent_name = $t->username;
    //days
    $days_array = array(
        $bookingdata['tourlist_date'], $bookingdata['place_date'], $bookingdata['eaidate'],
        $bookingdata['boat_date'], $bookingdata['spa_date'], $bookingdata['meal_date'],
        $bookingdata['car_date'], $bookingdata['popupguide_date'], $bookingdata['spotdate'], $bookingdata['restaurant_date']
    );
    $manualitems = $wpdb->get_results("SELECT meta_value FROM tourfrom_balieng2.`wp_tourmeta` where meta_key='manualitem' and tour_id=" . $t->id);
    foreach ($manualitems as $manualItem) {
        $days_array[] = unserialize($manualItem->meta_value)['date'] . ',';
    }
    $days = max(array_map(function ($value) {
        return max(explode(',', $value));
    }, $days_array));
    if (intval($days) <= 0)
        $days = 1;
    $post_link = $site_url . "/?p=" . $post_ID;
    $data1[] = array(
        'post_title' => htmlspecialchars_decode($postdata->post_title),
        'post_id' => $post_ID,
        'bookingcode' => $t->id,
        'location' => $hotelinfo->location,
        'image' => $post_image_match[1],
        'cost' =>  strtoupper($currencyName)." ".$cost,
        'seller' => $agent_name,
        'post_link' => $post_link,
        'days' => $days
  //  'currency'=> strtoupper($currencyName).'-'.$currencyList[$currencyName]['symbol']
    );
}

if (isset($duration_order) and $duration_order == 'lth') {
    $data1 = array_orderby($data1, 'days', SORT_ASC);
} elseif (isset($duration_order) and $duration_order == 'htl') {
    $data1 = array_orderby($data1, 'days', SORT_DESC);
}

http_response_code(200);
$response['data'] = $data1;
