<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {
    $username = check_input($_REQUEST['username']);
    $token = check_input($_REQUEST['token']);
    $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
    if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {
        $notifications_count = $wpdb->get_var("SELECT COUNT(*) FROM notification WHERE wp_user_id='{$wp_user_ID}' AND read_status=0 AND content LIKE '%Check out%'");
        http_response_code(200);
        $response['count'] = $notifications_count;
        $response['msg'] = 'success';
    } else {
        http_response_code(401);
        $response['msg'] = 'No user found';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
