<?php
include('../conn.php');
require_once($_SERVER['DOCUMENT_ROOT'] . "/English/wp-load.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/English/wp-admin/includes/image.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/English/wp-admin/includes/file.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/English/wp-admin/includes/media.php');
global $wpdb;

//

 function get_friends_with_login_user_id($wp_userid,$msgg,$identifier){

    global $wpdb;
    $friends = $wpdb->get_results("SELECT ID FROM `wp_users` WHERE  ID IN(
                            SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_userid}')
                            OR ID IN(
                                SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_userid}')");
    foreach ($friends as $f) {
        $wpdb->insert('notification', array('wp_user_id' => $f->ID, 'content' => um_get_display_name($wp_userid) ." ". $msgg));

    $msg =  um_get_display_name($wp_userid). " " . $msgg;
      $token =  to_get_user_device_token_from_wp_id_for_api($f->ID);

            if($token){
                notification_for_user_form_api($token,$msg,$identifier);
                 }
             }
         }

// Added on 17 Feb for sending mobile notifactions
function to_get_user_device_token_from_wp_id_for_api($tour_creator_id){
global $wpdb;

$get_user_data =$wpdb->get_row("SELECT * FROM `wp_users` where  ID= " . $tour_creator_id);
$user_email = $get_user_data->user_email;

if(!empty($user_email)) {

$get_user_data_tabl2 = $wpdb->get_row("SELECT * FROM `user` where  email = '{$user_email}'");
$user_id = $get_user_data_tabl2->userid;

if(!empty($user_id)) {

$get_user_data_tabl3 =$wpdb->get_row("SELECT * FROM `sessions` where  user_id = " . $user_id);
$user_device_token = $get_user_data_tabl3->device_token;

return $user_device_token;
}
}
}

// added push up notifaction new method by farhan
function notification_for_user_form_api($id,$msgg,$identifier){

// API access key from Google API's Console
define( 'API_ACCESS_KEY', 'AIzaSyCUhKe-YxeR_gBYlGnhiuxzphZPYMUPXos' );

$registrationIds = array ( $id );

// prep the bundle
$msg = array
(
'message'   => $msgg,
'title'     => 'TravPart notification',
//'subtitle'  => 'This is a subtitle. subtitle',
// 'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
'vibrate'   => 1,
'sound'     => 1,
'largeIcon' => 'large_icon',
'smallIcon' => 'small_icon',
'url'=>'google.com',
'identifier'=>$identifier
);

$fields = array
(
'registration_ids'  => $registrationIds,
'data'          => $msg
);

$headers = array
(
'Authorization: key=' . API_ACCESS_KEY,
'Content-Type: application/json'
);

$ch = curl_init();

curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
$result = curl_exec($ch );
curl_close( $ch );

//echo $result;

}           

function to_get_user_lat_lng($userid){
global $wpdb;
$value = $wpdb->get_row("SELECT * FROM `user_location` WHERE ID='{$userid}'");

    if($value){

    $coordiantes = $value->lat.",".$value->lng;
    }
    else{
    $coordiantes ="No coordiantes found";
    }

return $coordiantes;

}
// calcluate distance b/w users


function API_User_distance($lat1, $lon1, $lat2, $lon2, $unit)
{
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
    } else {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}

// sort by days
function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
        }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

$response = array();

function sendtomobile($message, $id)
{



    $url = 'https://fcm.googleapis.com/fcm/send';



    $fields = array(
        'registration_ids' => array(
            $id
        ),
        'data' => array(
            "message" => $message
        )
    );


    $headers = array(
        'Authorization: key=' . "AIzaSyCUhKe-YxeR_gBYlGnhiuxzphZPYMUPXos",
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS,  json_encode($fields));

    $result = curl_exec($ch);
    // print_r( $result );
    curl_close($ch);
}

if (isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'register':

            if (isTheseParametersAvailable(array('name', 'username', 'password', 'confirm_password', 'email', 'phone', 'signup_as'))) {

                $username = check_input($_REQUEST['username']);
                $pw1 = check_input($_REQUEST["password"]);
                $pw2 = check_input($_REQUEST["confirm_password"]);

                $nameforusrmail = $_REQUEST['name'];
                $emailforusrmail = $_REQUEST['email'];
                $phoneforusrmail = $_REQUEST['phone'];

                if (!preg_match("/^[a-zA-Z0-9_]*$/", $username)) {
                    http_response_code(401);
                    $response['msg'] = 'Invalid username';
                } else if (username_exists($username) || $wpdb->get_var("SELECT COUNT(*) from user where username = '{$username}'") > 0) {
                    http_response_code(401);
                    $response['msg'] = 'The username has been registered';
                } else if (!preg_match('!^.*(?=.{8,})(?=.*\d)(?=.*[A-Z]{1,})(?=.*[a-z]{1,}).*$!', $pw1)) {
                    http_response_code(401);
                    $response['msg'] = 'Invaild password. the password must have 8 characters or longer and include upper and lowercase letters and numbers.';
                } else if ($pw1 != $pw2) {
                    http_response_code(401);
                    $response['msg'] = 'password does not match';
                } else if (empty($_POST['email'])) {
                    http_response_code(401);
                    $response['msg'] = 'The email can\'t be empty';
                } else if (!is_email($_POST['email'])) {
                    http_response_code(401);
                    $response['msg'] = 'The email address is invaild';
                } else if (empty($_POST['phone'])) {
                    http_response_code(401);
                    $response['msg'] = 'The phone number can\'t be empty';
                } else {
                    $fusername = $username;
                    $email = empty($_REQUEST['email']) ? '' : check_input($_REQUEST['email']);
                    $phone = empty($_REQUEST['phone']) ? '' : check_input($_REQUEST['tel-ext'] . check_input($_REQUEST['phone']));

                    $res = mysqli_query($conn, "SELECT count(*) FROM `user` WHERE email='{$email}'");
                    $num = mysqli_fetch_row($res)[0];

                    $resforphone = mysqli_query($conn, "SELECT count(*) FROM `user` WHERE phone='{$phone}'");
                    $numForPhone = mysqli_fetch_row($resforphone)[0];

                    if ($num > 0) {
                        http_response_code(401);
                        $response['msg'] = 'Your mail id has been already registered.';
                    } else if ($numForPhone > 0) {
                        http_response_code(401);
                        $response['msg'] = 'Your Phone has been already registered.';
                    } else {

                        $password = check_input($_REQUEST["password"]);
                        $fpassword = md5($password);
                        $fname = check_input($_REQUEST["username"]);
                        $access = $_REQUEST['signup_as'];

                        if ($access == 'Customer') {
                            $access = 2;
                        } else if ($access == 'Agent') {
                            $access = 1;
                        } else {
                            $access = 2;
                        }

                        /*
                        if ($access == 2) {
                            $access_text = 'Customer';
                        } else if ($access == 1) {
                            $access_text = 'Sales_Agent';
                        }
                        // else {
                        //     $access_text = 'Guest';
                        // }
                        */


                        $activated = time() . rand(1, 99999999999999999999999999999999999999999) . time();
                        $fileInfo = PATHINFO($_FILE["photo"]);

                        if (empty($_FILE["photo"])) {
                            $location = '';
                        } else {
                            // if ($fileInfo['extension'] == "jpg" OR $fileInfo['extension'] == "png") {
                            $newFilename = $fileInfo['filename'] . "_" . time() . "." . $fileInfo['extension'];

                            move_uploaded_file($_FILES["image"]["tmp_name"], "../upload/" . $newFilename);

                            $location = "upload/" . $newFilename;
                            // } else {
                            // }
                        }

                        /*global $wpdb;

                        $wpdb->insert("wp_users", array(
                            "user_login" => $fusername,
                            "user_pass" => $fpassword,
                            "user_nicename" => $nameforusrmail,
                            "user_email" => $email,
                            "user_url" => 'NULL',
                            "user_registered" => time(),
                            "user_activation_key" => '',
                            "user_status" => '0',
                            "display_name" => $nameforusrmail
                        ));

                        $user = get_user_by('email', $email);
                        $user_id = $user->ID;

                        if ($access == 2) {
                            $u = new WP_User($user_id);
                            $u->set_role('um_clients');

                            $wpdb->insert("wp_usermeta", array(
                                "umeta_id" => 'NULL',
                                "user_id" => $user_id,
                                "meta_key" => 'wp_user_level',
                                "meta_value" => '10'
                            ));
                        } else {
                            $u = new WP_User($user_id);
                            $u->set_role('um_sales-agent');

                            $wpdb->insert("wp_usermeta", array(
                                "umeta_id" => 'NULL',
                                "user_id" => $user_id,
                                "meta_key" => 'wp_user_level',
                                "meta_value" => '5'
                            ));
                        }*/


                        mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated,photo,phone) values ('$nameforusrmail', '$fusername','$email', '$fpassword', '$access','$activated','$location','$phone')");

                        if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_potential_sale_agent` WHERE email='{$email}'") >= 1) {
                            $wpdb->delete("wp_potential_sale_agent", array('email' => $email));
                        }

                        $mail_content = 'Hello, ' . $nameforusrmail . ' <br>Welcome to Travchat! Please click on the link below to activate your account<br>
                                    <div style="display:block;margin:22px 0px;">
                                    <a style="background-color: #4caf50; color: white; padding: 12px; border: none; text-decoration: none;" href="http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '">Activation</a>
                                    </div>
                                    <br>Or copy the link to the browser to open:<a href="http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '">http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '</a>
                                    <div>
                                        <p> Your Mail is: ' . $emailforusrmail . '</p>
                                    <p> Your User Name is: ' . $fusername . '</p>
                                        <p> Your Phone is: ' . $phoneforusrmail . '</p>
                                    </div>
                                    
                                    <br><br>- Travchat team';

                        sendmailtosuer($email, 'Travchat - Please activate your account', $mail_content);

                        $res2 = mysqli_query($conn, "SELECT count(*) FROM `user` WHERE email='{$email}' and activated=1");
                        $num2 = mysqli_fetch_row($res2)[0];
                        if ($num2 > 0) {
                            http_response_code(200);
                            $response['msg'] = 'Sign up successful. You may login now!';
                        } else {
                            http_response_code(200);
                            $response['msg'] = 'You need to activate the link in your mail !';
                        }
                    }
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;



        case 'login':
            if (isTheseParametersAvailable(array('username', 'password'))) {

                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $now = date('Y-m-d H:i:s', time());

                if (!is_email($username) && !preg_match("/^[a-zA-Z0-9_]*$/", $username)) {
                    http_response_code(401);
                    $response['msg'] = 'Invalid username or password';
                } else {

                    $fusername = $username;
                    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
                    $fpassword = md5($password);

                    if (is_email($username)) {
                        $username_or_email = "email='$fusername'";
                    } else {
                        $username_or_email = "username='$fusername'";
                    }

                    $auth = $wpdb->get_var("SELECT COUNT(*) from `user` WHERE {$username_or_email} and password='$fpassword' and activated=1");

                    if ($auth == 0 && is_wp_error(($wp_user = wp_authenticate($username, $password)))) {
                        http_response_code(401);
                        $response['msg'] = 'Invalid username or password';
                    } else {
                        if (!empty($wp_user)) {
                            $wpdb->insert(
                                'user',
                                array(
                                    'uname' => $wp_user->user_nicename,
                                    'username' => $wp_user->user_login,
                                    'email' => $wp_user->user_email,
                                    'phone' => '',
                                    'password' => $fpassword,
                                    'access' => $wp_user->has_cap('um_travel-advisor') ? '1' : '2',
                                    'activated' => '1'
                                )
                            );
                        }

                        $token = md5($password . time());

                        $row = $wpdb->get_row("SELECT wp_users.ID as wp_user_id, user.* from `wp_users`,`user` WHERE {$username_or_email} AND user.username=wp_users.user_login", ARRAY_A);

                        mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");

                        $user = array(
                            'userid' => $row['userid'],
                            'username' => $row['username'],
                            'email' => $row['email'],
                            'phone' => $row['phone'],
                            'uname' => $row['uname'],
                            'photo' => (empty($row['photo']) ? um_get_user_avatar_url($row['wp_user_id'], 80) : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
                            'country' => $row['country'],
                            'region' => $row['region'],
                            'timezone' => $row['timezone'],
                            'access' => $row['access'],
                            'token' => $token
                        );
                        $user_id = $row['userid'];
                        $user_id = (int) $user_id;

                        $ip_address = $_SERVER['REMOTE_ADDR'];
                        $now = date("Y-m-d H:i:s");

                        $session_check =  mysqli_query($conn2, "SELECT * from  `sessions`   WHERE user_id='{$user_id}'");

                        if (mysqli_num_rows($session_check) == 0) {
                            $sessions_query  = "INSERT INTO `sessions` (user_id, ip_address,last_activity) VALUES ( {$user_id}, '{$ip_address}','{$now}')";
                        } else {
                            $sessions_query = "UPDATE `sessions` SET last_activity  = '{$now}' WHERE `user_id` = {$user_id}";
                        }

                        $result = mysqli_query($conn, $sessions_query);

                        if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_users` WHERE `user_login`='{$username}'") == 0) {
                            wp_authenticate($username, $password);
                        }

                        http_response_code(200);
                        $response = $user;
                    }
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;
        
        case 'google_login':
            if (isTheseParametersAvailable(array('access_token'))) {
                $google_userinfo_api = 'https://www.googleapis.com/oauth2/v2/userinfo';
                $access_token = filter_input(INPUT_POST, 'access_token', FILTER_SANITIZE_STRING);

                $headers = array(
                    'Authorization: Bearer ' . $access_token,
                );

                $authorization = curl_init($google_userinfo_api);
                curl_setopt($authorization, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($authorization, CURLOPT_TIMEOUT, 60);
                curl_setopt($authorization, CURLOPT_HTTPHEADER, $headers);
                $google_response = json_decode(curl_exec($authorization), true);
                if (!empty($google_response['email'])) {

                    $passkey = $google_response['id'] . time();
                    $token = md5($passkey);
                    $row = $wpdb->get_row("SELECT wp_users.ID as wp_user_id, user.* from `wp_users`,`user` WHERE email='{$google_response['email']}' AND user.username=wp_users.user_login", ARRAY_A);

                    if (empty($row)) {
                        if (!empty($google_response['name'])) {
                            $username = str_replace(' ', '', $google_response['name']);
                        } else {
                            $username = str_replace('@gmail.com', '', $google_response['email']);
                        }
                        //save profile photo
                        if (!empty($google_response['picture'])) {
                            $profile_photo = file_get_contents($google_response['picture']);
                            $photo_name = $username . time() . '.jpg';
                            file_put_contents('../upload/' . $photo_name, $profile_photo);
                        }
                        $wpdb->insert('user', array(
                            'uname' => empty($google_response['name']) ? $username : $google_response['name'],
                            'username' => $username,
                            'email' => $google_response['email'],
                            'password' => $token,
                            'token' => $token,
                            'access' => 2,
                            'activated' => 1,
                            'photo' => empty($google_response['picture']) ? '' : ('upload/' . $photo_name),
                            'phone' => ''
                        ));
                        wp_authenticate($username, $passkey);
                        $row = $wpdb->get_row("SELECT wp_users.ID as wp_user_id, user.* from `wp_users`,`user` WHERE email='{$google_response['email']}' AND user.username=wp_users.user_login", ARRAY_A);
                    } else {
                        mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");
                    }

                    $user = array(
                        'userid' => $row['userid'],
                        'username' => $row['username'],
                        'email' => $row['email'],
                        'phone' => $row['phone'],
                        'uname' => $row['uname'],
                        'photo' => (empty($row['photo']) ? um_get_user_avatar_url($row['wp_user_id'], 80) : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
                        'country' => $row['country'],
                        'region' => $row['region'],
                        'timezone' => $row['timezone'],
                        'access' => $row['access'],
                        'token' => $token
                    );

                    http_response_code(200);
                    $response['msg'] = $user;
                } else {
                    http_response_code(401);
                    $response['msg'] = 'Wrong token';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;
        
        case 'forgotpassword':
            //Old code
            /* if (isTheseParametersAvailable(array('email'))) {

              $email = empty($_REQUEST['email']) ? '' : check_input($_REQUEST['email']);

              $activated = password_hash($password . time(), PASSWORD_DEFAULT);

              $res = mysqli_query($conn, "SELECT count(*),username FROM `user` WHERE email='{$email}' and activated=1");

              while ($row = mysqli_fetch_row($res)) {
              $num = $row[0];
              $username = $row[1];
              }

              if ($num > 0) {
              mysqli_query($conn, "update `user` set activated='{$activated}' where email='{$email}'");

              $mail_content = 'Hello,<br>Welcome to Travchat! Please click on the link below to Reset your password<br>
              <div style="display:block;margin:22px 0px;">
              <p>Your username : ' . $username . '</p></br>
              <a style="background-color: #4caf50; color: white; padding: 12px; border: none; text-decoration: none;" href="http://www.travpart.com/English/travchat/activatepass.php?token=' . $activated . '&status=1">Reset Password</a>
              </div>
              <br>Or copy the link to the browser to open:<a href="http://www.travpart.com/English/travchat/activatepass.php?token=' . $activated . '&status=1">http://www.travpart.com/English/travchat/activate.php?token=' . $activated . '</a>
              <br><br>- Travchat team';

              sendmailtosuer($email, 'Travchat - Please reset your password', $mail_content);

              http_response_code(200);
              $response['msg'] = 'Please check your email, a link to reset your password is sent to your email !';
              } else {
              http_response_code(401);
              $response['msg'] = 'Your mail id does not exist, please register.';
              }
              } else {
              http_response_code(401);
              $response['msg'] = 'Required parameter missing';
              } */
            if (empty($_POST['email'])) {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            } else {
                $postdata = array(
                    '_um_password_reset' => '1',
                    'username_b' => check_input($_POST['email']),
                    'form_id' => 'um_password_id',
                    'timestamp' => time()
                );
                $url = "https://www.travpart.com/English/password-reset/";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                $data = curl_exec($ch);

                if (curl_errno($ch)) {
                    http_response_code(401);
                    $response['msg'] = 'System is busy. Please try again.';
                } else if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
                    http_response_code(401);
                    $response['msg'] = 'Your mail id does not exist, please register.';
                } else if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 302) {
                    http_response_code(200);
                    $response['msg'] = 'Please check your email, a link to reset your password is sent to your email !';
                } else {
                    http_response_code(401);
                    $response['msg'] = 'System is busy. Please try again.';
                }
                curl_close($ch);
            }
            break;


        case 'orderlist':


            // if(isTheseParametersAvailable(array('access', 'agentid'))){
            //  if($_REQUEST['access'] == 1){
            //      $temp = array();
            //      $agent_id=$_REQUEST['agentid'];
            //      $orders=mysqli_query($conn, "SELECT * FROM `orders` WHERE `agent_id` = {$agent_id}");
            //      while($row = mysqli_fetch_array($orders, MYSQL_ASSOC)){
            //          $bookcode = $row['bookingcode'];
            //          $saleamt = $row['total_price'];
            //          $commission = $row['total_price']*0.08;
            //          $temp[$bookcode]=array('saleamt' => $saleamt, 'commission' => $commission);
            //      }
            //      http_response_code(200);
            //      $response['data'] = $temp;
            //  }else if($_REQUEST['access'] == 2){
            //      $customer_id=$_REQUEST['agentid'];
            //      $temp = array();
            //      $orders=mysqli_query($conn, "SELECT orders.*, user.uname FROM orders,user WHERE orders.customer_id={$customer_id} AND user.userid=orders.agent_id");
            //      while($row = mysqli_fetch_array($orders, MYSQL_ASSOC))
            //      {
            //          $temp[$row['bookingcode']]=array('nominalamt'=>$row['total_price'], 'agentname'=>$row['uname']);
            //      }
            //      http_response_code(200);
            //      $response['data'] = $temp;
            //  }else{
            //      http_response_code(200);
            //      $response['message'] = 'Provide access code';
            //  }
            // }


            include('../../wp-config.php');

            if (empty($_REQUEST['username'])) {
                http_response_code(401);
                $response['msg'] = "provide username";
                // echo json_encode(array("msg" => "provide username"));
            } else {
                $username = check_input($_REQUEST['username']);
                $token = check_input($_REQUEST['token']);
                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $res1 = mysqli_query($conn, "SELECT * FROM `wp_users` WHERE user_nicename='{$username}'");
                $user = mysqli_fetch_assoc($res);
                $user1 = mysqli_fetch_assoc($res1);
                /* echo "<pre>";
                  print_r($user);
                  exit; */
                if (empty($user)) {
                    http_response_code(401);
                    $response['msg'] = "No user found";
                    // echo json_encode(array("error_msg" => "No user found"));
                } else {
                    if ($user['access'] == 1) {
                        $agent_id = $user1['ID'];



                        $orders = mysqli_query($conn, "SELECT t.id tour_id,t.confirm_payment,t.total,p.post_id,p.meta_key FROM (wp_tour t LEFT JOIN wp_postmeta p
                                            ON (t.id=p.meta_value AND p.meta_key='tour_id')) INNER JOIN wp_tourmeta m
                                            ON (t.id=m.`tour_id` AND m.meta_key='userid' AND m.meta_value='{$agent_id}')");

                        // Added on 2 May 2019
                        //$orders = mysqli_query($conn, "SELECT * FROM `orders` WHERE `agent_id` = {$agent_id}");
                        $agent_name = $username;
                        $orderlist = array();
                        // while ($row = mysqli_fetch_array($orders, MYSQL_ASSOC)) {
                        while ($row = mysqli_fetch_assoc($orders)) {

                            $confirm_payment = $wpdb->get_row("SELECT confirm_payment FROM `wp_tour` WHERE `id` = " . $row["tour_id"]);

                            $af_ratting = 0;
                            $ac_ratting = 0;
                            $rattingDataByTour = $wpdb->get_results('SELECT * FROM agent_feedback WHERE af_tour_id = ' . $row["tour_id"], ARRAY_A);
                            if (!empty($rattingDataByTour)) {
                                $af_ratting = $rattingDataByTour[0]['af_ratting'];
                            }

                            $rattingDataByTourForMine = $wpdb->get_results('SELECT * FROM agent_client_feedback WHERE = ' . $row["tour_id"], ARRAY_A);
                            if (!empty($rattingDataByTourForMine)) {
                                $ac_ratting = $rattingDataByTour[0]['ac_ratting'];
                            }

                            if ($confirm_payment == 1)
                                $confirm_payment = TRUE;
                            else
                                $confirm_payment = FALSE;
                            array_push($orderlist, array(
                                'agentname' => $agent_name,
                                'bookingcode' => 'BALI' . $row['tour_id'],
                                'total_price' => intval($row['total']),
                                'commission' => round($row['total'] * 0.08, 2),
                                'confirm_payment' => $row['confirm_payment'],
                                'invoice_link' => (($row['confirm_payment'] == 1) ? ('https://www.travpart.com/English/wp-content/themes/bali/api.php?action=getinvoice&agent_name=' . $agent_name . '&tourid=' . (int) $row['tour_id']) : ''),
                                'FeedbackFromSellers' => $af_ratting,
                                'FeedbackToSellers' => $ac_ratting
                            ));
                        }
                        http_response_code(200);
                        // echo json_encode(array('msg' => 'success', 'orderlist' => $orderlist));
                        $response['msg'] = "success";
                        $response['orderlist'] = $orderlist;
                    } else if ($user['access'] == 2) {
                        $customer_id = $user['userid'];

                        $orders = mysqli_query($conn, "SELECT orders.*, user.uname FROM orders,user WHERE orders.customer_id={$customer_id} AND user.userid=orders.agent_id");

                        //  $orders = mysqli_query($conn, "SELECT * FROM orders WHERE `customer_id`= customer_id={$customer_id}"); 


                        $orderlist = array();
                        while ($row = mysqli_fetch_array($orders)) {
                            array_push($orderlist, array(
                                'agentname' => $row['uname'],
                                'bookingcode' => $row['bookingcode'],
                                'total_price' => round($row['total_price'], 2),
                                'commission' => NULL,
                                'confirm_payment' => NULL,
                                'invoice_link' => NULL
                            ));
                        }

                        http_response_code(200);
                        $response['msg'] = "success";
                        $response['orderlist'] = $orderlist;
                    } else {
                        http_response_code(401);
                        $response['msg'] = "user does not have access permission";
                    }
                }
            }

            break;

        case 'get_tour_statics':
            if (isset($_REQUEST['tour'])) {
                $tour_id = $_REQUEST['tour'];
                $totalComment = $wpdb->get_var("SELECT COUNT(*) FROM `social_comments` WHERE scm_tour_id = '$tour_id'");

                $totalLikes = $wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE sc_type = 0 AND sc_tour_id = '$tour_id'");

                $totalDislikes = $wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE sc_type = 1 AND sc_tour_id = '$tour_id'");

                //get share count
                $share_count = $wpdb->get_var("SELECT `meta_value` FROM `wp_tourmeta` WHERE `tour_id`='{$tour_id}' AND `meta_key`='sharecount'");
                if (empty($share_count)) {
                    $share_count = 0;
                }

                $data = array();
                array_push($data, array(
                    'comment' => $totalComment,
                    'likes' => $totalLikes,
                    'dislikes' => $totalDislikes,
                    'share' => $share_count

                ));
                http_response_code(200);
                //  echo json_encode(array('msg' => 'tour statices.', 'list' => $data));
                $response['result'] = $data;
            } else {
                http_response_code(401);
                $response['msg'] = "empty tour id";
            }
            break;


        case 'agentlist':

            if (isTheseParametersAvailable(array('username', 'token'))) {
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                if (!empty($_POST['kw'])) {
                    $kw =  filter_input(INPUT_POST, 'kw', FILTER_SANITIZE_STRING);
                }

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);
                if (!empty($user)) {
                    $userid = $user['userid'];
                    $mem = array('999999');
                    $um = mysqli_query($conn, "select * from `chat_member` where chatroomid='" . $userid . "'");

                    while ($umrow = mysqli_fetch_array($um)) {
                        $mem[] = $umrow['userid'];
                    }
                    $users = implode($mem, ',');

                    $sql = " select * from `user` 
                            LEFT JOIN `sessions` ON
                                `sessions`.`id` = (
                                    SELECT `id` from `sessions` where `user_id` = `user`.`userid` ORDER BY `id` DESC LIMIT 1
                                )
                                where  uname !='' AND userid NOT IN ('343','220') AND access = 1
                            ";
                    if (isset($kw)) {
                        $sql .= " AND username LIKE '%{$kw}%'";
                    }
                    $sql .= "ORDER BY seller_responsiveness ASC";
                    $query = mysqli_query($conn, $sql);
                    $tempArr = array();

                    $index = 0;

                    while ($row = mysqli_fetch_array($query)) {
                        $index++;
                        $is_active = false;
                        if ($row['last_activity']) {
                            $last_activity = new DateTime($row['last_activity']);
                            $now = new DateTime();
                            $is_active = $last_activity < $now && (($now->getTimestamp() - $last_activity->getTimestamp()) <= MAX_INACTIVITY_TIME);
                        }
                        $username = $row['username'];
                        $author = get_user_by('slug', $username);
                        if (!empty($author)) {
                            $args = array(
                                'post_type'  => 'post',
                                'author'     => $author->ID,
                                'status' =>   'publish'
                            );
                            $wp_posts = get_posts($args);

                            $avatar = um_get_user_avatar_data($author->ID);
                            if (!empty($avatar['url'])) {
                                $agent_avatar = $avatar['url'];
                            }

                            $verified = (get_user_meta($author->ID, 'verification_status', true) == 2);

                            if (count($wp_posts) > 0) {
                                $tempArr[] = array(
                                    'userid' => $row['userid'],
                                    'name' => (empty($row['uname']) ? $row['username'] : $row['uname']),
                                    'photo' => (empty($agent_avatar) ? '' : ($agent_avatar)),
                                    'phone' => $row['phone'],
                                    'country' => $row['country'],
                                    'region' => $row['region'],
                                    'verified' => $verified,
                                    'online' => $row['is_active'],
                                    'last_activity' => $row['last_activity'],
                                    'hours' => $row['seller_responsiveness']
                                );
                            }
                        }
                    }

                    http_response_code(200);
                    $response['msg'] = "agentlist retrived successfully";
                    $response['agentlist'] = $tempArr;
                } else {
                    http_response_code(401);
                    $response['msg'] = "username does not exists";
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;


            // added on April 12,2019 by farhan
        case 'customerlist':

            if (isTheseParametersAvailable(array('username', 'token'))) {
                $token = check_input($_REQUEST['token']);
                $username = $_REQUEST['username'];

                if (isset($_REQUEST['kw'])) {
                    $kw =  $_REQUEST['kw'];
                }

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}' AND access = 1 ");
                $user = mysqli_fetch_assoc($res);
                if (!empty($user)) {

                    $userid = $user['userid'];
                    //  $mem = array('999999');
                    $um = mysqli_query($conn, "select * from `chat_member` where chatroomid='" . $userid . "'");

                    while ($umrow = mysqli_fetch_array($um)) {
                        $mem[] = $umrow['userid'];
                    }
                    $users = implode($mem, ',');

                    $sql = " select * from `user`  where userid in (" . $users . ") AND userid NOT IN ('2','51','37','510') AND access = 2
                            ";
                    if (isset($kw)) {
                        $sql .= " AND username LIKE '%{$kw}%'";
                    }
                    $query = mysqli_query($conn, $sql);
                    $tempArr = array();

                    $index = 0;

                    while ($row = mysqli_fetch_array($query)) {
                        $index++;
                        $is_active = false;
                        if ($row['last_activity']) {
                            $last_activity = new DateTime($row['last_activity']);
                            $now = new DateTime();
                            $is_active = $last_activity < $now && (($now->getTimestamp() - $last_activity->getTimestamp()) <= MAX_INACTIVITY_TIME);
                        }

                        $tempArr[] = array(
                            'userid' => $row['userid'], 'name' => $row['uname'],
                            'photo' => (empty($row['photo']) ? '' : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
                            'phone' => $row['phone'], 'country' => $row['country'], 'region' => $row['region'], 'online' => $row['is_active'], 'last_activity' => $row['last_activity']
                        );
                    }

                    http_response_code(200);
                    $response['msg'] = "customer-list retrived successfully";
                    $response['customerlist'] = $tempArr;
                } else {
                    http_response_code(401);
                    $response['msg'] = "username does not exists";
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;

            // end of customerlist api
        case 'faqlist':

            if (isTheseParametersAvailable(array('username', 'token'))) {

                $username = $_REQUEST['username'];
                $token = check_input($_REQUEST['token']);

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);
                if (!empty($user)) {

                    $url = 'https://www.tourfrombali.com/faq/';
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                    $data = curl_exec($ch);
                    if (curl_errno($ch)) {
                        echo FALSE;
                    } else {
                        preg_match_all('!<div class="faqwd_answer">(.+?).{0,1}</div>!is', $data, $faq_answer);
                        preg_match_all('!<span class="faqwd_post_title".*?>(.+?)</span>!is', $data, $faq_title);

                        if (!empty($faq_title[1]) && count($faq_answer[1]) == count($faq_title[1])) {
                            $faq = '';
                            $temp = array();
                            for ($i = 0; $i < count($faq_title[1]); $i++) {

                                $title = strip_tags($faq_title[1][$i]);
                                $answer = strip_tags($faq_answer[1][$i]);

                                array_push($temp, array("title" => $title, "answer" => $answer));
                            }

                            http_response_code(200);
                            $response['msg'] = "success";
                            $response['faqlist'] = $temp;
                        } else {
                            http_response_code(401);
                        }
                    }

                    curl_close($ch);
                } else {
                    http_response_code(401);
                    $response['msg'] = 'Incorrect Username';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;


        case 'sendmessage':

            if (isTheseParametersAvailable(array('username', 'member_userid', 'msg'))) {

                $msg = $_REQUEST['msg'];
                $token = check_input($_REQUEST['token']);
                $username = check_input($_REQUEST['username']);

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);
                if (empty($user)) {
                    http_response_code(401);
                    $response['msg'] = 'user not found';
                    // echo json_encode(array("error_msg" => "no user found"));
                } else {
                    if (mysqli_fetch_array(mysqli_query($conn, "SELECT COUNT(*) FROM chat_member WHERE (userid={$user['userid']} AND chatroomid={$_REQUEST['member_userid']}) OR (userid={$_POST['member_userid']} AND chatroomid={$user['userid']})"))[0] <= 0); {
                        mysqli_query($conn, "insert into chat_member (userid, chatroomid) values ('{$user['userid']}','{$_REQUEST['member_userid']}')");
                        mysqli_query($conn, "insert into chat_member (userid, chatroomid) values ('{$_REQUEST['member_userid']}','{$user['userid']}')");
                    }

                    $from = $user['userid'];
                    $to = $_REQUEST['member_userid'];

                    $sql = "insert into `chat` (message, userid, chat_date,c_to,c_from,type,attachment_type) values ('{$msg}' , '" . $from . "', NOW(),'{$to}','" . $from . "','','')";

                    if (mysqli_query($conn, $sql)) {
                        http_response_code(200);
                        $res =  mysqli_query($conn2, "SELECT * from  `sessions`   WHERE user_id='{$to}'");
                        if (mysqli_num_rows($res) != 0) {

                            $user = mysqli_fetch_assoc($res);
                            $send_key = $user['device_token'];
                            sendtomobile($msg, $send_key);
                        }
                        $response['msg'] = 'Sent';
                        $response['messages'] = array('date' => date('Y-m-d H:i:s'), 'uname' => $username, 'member_userid' => $to, 'type' => '', 'message' => $msg, 'unread' => 0);
                    }
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;


        case 'chatmember':
            if (isTheseParametersAvailable(array('username', 'token'))) {

                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);
                if (empty($user)) {
                    http_response_code(401);
                    $response['msg'] = 'check username';
                } else {
                    //Get the order of unread message count
                    $unreadOrders = $wpdb->get_results("SELECT userid,COUNT(status) AS unread FROM `chat` WHERE c_to='{$user['userid']}' AND status=0 GROUP BY userid ORDER BY unread ASC", ARRAY_A);
                    $unreadOrderA = array();
                    foreach ($unreadOrders as $row) {
                        $unreadOrderA[] = $row['userid'];
                    }
                    //Get user list order
                    $userOrders = $wpdb->get_results("SELECT userid,MAX(chat_date) AS last_message_date FROM `chat` WHERE c_to='{$user['userid']}' GROUP BY userid ORDER BY last_message_date ASC", ARRAY_A);
                    $userOrderA = array();
                    foreach ($userOrders as $row) {
                        $userOrderA[] = $row['userid'];
                    }
                    $userField = implode(',', array_merge(array_diff($userOrderA, $unreadOrderA), $unreadOrderA));
                    if (!empty($userField))
                        $userField = " ORDER BY FIELD(cm.userid,{$userField}) DESC";
                    else
                        $userField = '';
                    $my = mysqli_query($conn, "SELECT * FROM chat_member cm INNER JOIN user u ON u.userid = cm.userid LEFT JOIN sessions s ON s.id=(SELECT id FROM `sessions` WHERE sessions.user_id=u.userid ORDER BY id DESC LIMIT 1) WHERE chatroomid = '{$user['userid']}' {$userField}");
                    $chat_members = array();
                    while ($myrow = mysqli_fetch_array($my)) {
                        $unread_message_count = mysqli_fetch_array(mysqli_query($conn, "SELECT COUNT(*) FROM chat WHERE c_to={$user['userid']} AND c_from={$myrow['userid']} AND status=0"))[0];
                        //Get last message user sent
                        $last_message_row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM `chat` WHERE (c_to={$user['userid']} OR c_to={$myrow['userid']}) AND (c_from={$myrow['userid']} OR c_from={$user['userid']}) AND type!='deleted' ORDER BY chat_date DESC LIMIT 1"));
                        $last_message = '';
                        if (!empty($last_message_row)) {
                            if ($last_message_row['type'] == 'attachment')
                                $last_message = 'Attachment';
                            else
                                $last_message = $last_message_row['message'];

                            $last_message = mb_strimwidth(trim(strip_tags($last_message)), 0, 70, "...");
                        }
                        array_push($chat_members, array(
                            'userid' => $myrow['userid'],
                            'uname' => $myrow['username'],
                            'photo' => (empty($myrow['photo']) ? '' : ("https://www.travpart.com/English/travchat/{$myrow['photo']}")),
                            'country' => $myrow['country'],
                            'region' => $myrow['region'],
                            'timezone' => $myrow['timezone'],
                            'unread' => $unread_message_count,
                            'last_message' => $last_message,
                            'last_message_date' => $last_message_row['chat_date'],
                            'online' => $myrow['is_active']
                        ));
                    }

                    $response['msg'] = "success";
                    $response['chatmembers'] = $chat_members;
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;


        case 'chatmessage':

            if (isTheseParametersAvailable(array('username', 'member_userid'))) {

                $username = $_REQUEST['username'];
                $token = check_input($_REQUEST['token']);
                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);
                if (empty($user)) {
                    http_response_code(401);
                    $response['msg'] = "Username not available";
                } else {

                    $from = $user['userid'];
                    $to = $_REQUEST['member_userid'];

                    $sql = "SELECT  c.*,u.uname,u.photo FROM `chat` c INNER JOIN `user` u ON u.userid = c.c_from WHERE c.c_to IN ($to,$from) AND c.c_from IN ($to,$from)";

                    $query = mysqli_query($conn, $sql) or die(mysqli_error());

                    mysqli_query($conn, "UPDATE chat SET status=1 WHERE c_to={$from} AND c_from={$to}") or die(mysqli_error());

                    $tempArr = array();

                    while ($row = mysqli_fetch_array($query)) {

                        if ($row['type'] == 'attachment') {

                            $message_content = "https://www.travpart.com/English/travchat/upload/" . rawurlencode($row['message']);
                            $arr = array(
                                'date' => $row['chat_date'],
                                'uname' => $row['uname'],
                                'member_userid' => $row['userid'],
                                'photo' => (empty($row['photo']) ? '' : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
                                'type' => $row['type'],
                                'message' => $message_content,
                                'unread' => $row['status']
                            );
                            array_push($tempArr, $arr);
                        } else {
                            $arr = array(
                                'date' => $row['chat_date'],
                                'uname' => $row['uname'],
                                'member_userid' => $row['userid'],
                                'photo' => (empty($row['photo']) ? '' : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
                                'type' => $row['type'],
                                'message' => $row['message'],
                                'unread' => $row['status']
                            );
                            array_push($tempArr, $arr);
                        }
                    }

                    http_response_code(200);
                    $response['msg'] = "success";
                    $response['messages'] = $tempArr;
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;


        case 'newmessage':

            if (isTheseParametersAvailable(array('username', 'token'))) {
                $token = check_input($_REQUEST['token']);
                $username = check_input($_REQUEST['username']);
                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);

                if (empty($user)) {
                    http_response_code(401);
                    $response['msg'] = "username not found";
                    // echo json_encode(array("error_msg" => "username not found"));
                } else {
                    $to = $user['userid'];

                    $query = mysqli_query($conn, "SELECT  c.*,u.uname,u.photo FROM chat c INNER JOIN user u ON u.userid = c.c_from WHERE c.c_to={$to} AND status=1");

                    $messages = array();

                    while ($row = mysqli_fetch_array($query)) {

                        if ($row['type'] == 'attachment') {
                            if (explode('/', $row['attachment_type'])[0] == 'image')
                                $message_content = 'https://www.travpart.com/English/travchat/upload/' . $row['message'];
                            else
                                $message_content = "https://www.travpart.com/English/travchat/user/download-attachment.php?file=" . rawurlencode($row['message']) . "&type={$row['attachment_type']}";
                            array_push($messages, array(
                                'member_userid' => $row['c_from'],
                                'date' => $row['chat_date'],
                                'uname' => $row['uname'],
                                'type' => $row['type'],
                                'message' => $message_content,
                                'unread' => (($row['status'] == 0) ? TRUE : FALSE)
                            ));
                        } else {
                            array_push($messages, array(
                                'member_userid' => $row['c_from'],
                                'date' => $row['chat_date'],
                                'uname' => $row['uname'],
                                'type' => $row['type'],
                                'message' => $row['message'],
                                'unread' => (($row['status'] == 0) ? TRUE : FALSE)
                            ));
                        }
                    }
                    mysqli_query($conn, "UPDATE chat SET status=1 WHERE c_to={$to}");

                    http_response_code(200);
                    $response['msg'] = 'success';
                    $response['messages'] = $messages;
                    // echo json_encode(array('msg' => 'Success', 'messages' => $messages));
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;


        case 'updateaccount':

            if (isTheseParametersAvailable(array('username', 'name', 'password', 'email', 'phone', 'token'))) {
                $token = check_input($_REQUEST['token']);
                $username = check_input($_REQUEST['username']);

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);

                if (empty($user)) {
                    http_response_code(401);
                    $response['msg'] = "username not found";
                    // echo json_encode(array("error_msg" => "username not found"));
                } else {
                    $id = $user['userid'];

                    $update_items = '';

                    if (!empty($_POST['password']))
                        $update_items .= "password='" . md5($_POST['password']) . "',";
                    if (!empty($_POST['name']))
                        $update_items .= "uname='{$_POST['name']}',";
                    if (!empty($_POST['email']))
                        $update_items .= "email='{$_POST['email']}',";
                    if (!empty($_POST['phone']))
                        $update_items .= "phone='{$_POST['phone']}',";
                    $update_items = rtrim($update_items, ',');


                    if (mysqli_query($conn, "update `user` set {$update_items} where userid='{$user['userid']}'")) {
                        http_response_code(200);
                        $response['msg'] = 'Account updated';
                    } else {

                        http_response_code(401);
                        $response['msg'] = 'Error happened';
                    }
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;


        case 'getaccountdetails':

            if (isTheseParametersAvailable(array('username'))) {

                $myq = mysqli_query($conn, "select * from `user` where username='" . $_REQUEST['username'] . "'");

                if ($myqrow = mysqli_fetch_array($myq)) {
                    $arr = array(
                        "userid" => $myqrow['userid'],
                        "username" => $myqrow['username'],
                        "email" => $myqrow['email'],
                        "password" => $myqrow['password'],
                        "uname" => $myqrow['uname'],
                        "photo" => (empty($myqrow['photo']) ? '' : ("https://www.travpart.com/English/travchat/{$myqrow['photo']}")),
                        "access" => $myqrow['access'],
                        "activated" => $myqrow['activated'],
                        "country" => $myqrow['country'],
                        "region" => $myqrow['region'],
                        "timezone" => $myqrow['timezone'],
                        "user_unique_room_id" => $myqrow['user_unique_room_id'],
                        "userid_travcust" => $myqrow['userid_travcust'],
                        "phone" => $myqrow['phone'],
                        "online_time" => $myqrow['online_time'],
                        "is_active" => $myqrow['is_active'],
                    );
                    http_response_code(200);
                    $response['userdata'] = $arr;
                } else {
                    http_response_code(401);
                    $response['msg'] = "ERROR";
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;


        case 'sendattachment':

            if (isTheseParametersAvailable(array('username', 'member_userid', 'token')) || empty($_FILES)) {

                $tmp_name = $_FILES['file']['tmp_name'];
                $file_name = $_FILES['file']['name'];
                $size = $_FILES['file']['size'];
                //$type = $_FILES['file']['type'];
                $type = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $tmp_name);
                $error = 0;
                $token = check_input($_POST['token']);
                $user_name = check_input($_POST['username']);


                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$user_name}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);

                if (!empty($user)) {
                    $from = $user['userid'];
                    $to = check_input($_POST['member_userid']);

                    if (mysqli_fetch_array(mysqli_query($conn, "SELECT COUNT(*) FROM chat_member WHERE (userid={$from} AND chatroomid={$to}) OR (userid={$to} AND chatroomid={$from})"))[0] <= 0) {
                        mysqli_query($conn, "insert into chat_member (userid, chatroomid) values ('{$from}','{$to}')");
                        mysqli_query($conn, "insert into chat_member (userid, chatroomid) values ('{$to}','{$from}')");
                    }

                    $folder = "../upload/" . $user_name;

                    if (!is_dir($folder)) {
                        mkdir($folder, 0700, true);
                    }

                    $upload_file_full_path = $folder . "/" . $file_name;

                    if (move_uploaded_file($tmp_name, $upload_file_full_path)) {

                        $msg = $user_name . "/" . $file_name;

                        $sql = "insert into `chat` (message, userid, chat_date,c_to,c_from,type,attachment_type) values ('" . $msg . "' , '" . $from . "', NOW(),'" . $to . "','" . $from . "','attachment','" . $type . "')";

                        $result = mysqli_query($conn, $sql);

                        if (explode('/', $type)[0] == 'image')
                            $message_content = 'https://www.travpart.com/English/travchat/upload/' . $msg;
                        else
                            $message_content = "https://www.travpart.com/English/travchat/user/download-attachment.php?file=" . rawurlencode($msg) . "&type={$type}";

                        if ($result) {
                            http_response_code(200);
                            // $response['error'] = false;
                            $response['msg'] = 'Successfully send';
                            $response['messages'] = array('date' => date('Y-m-d H:i:s'), 'uname' => $user_name, 'member_userid' => $to, 'type' => $type, 'message' => $message_content, 'unread' => 0);
                        } else {
                            http_response_code(401);
                            // $response['error'] = true;
                            $response['msg'] = 'Error in sending file';
                        }
                    } else {
                        http_response_code(401);
                        // $response['error'] = true;
                        $response['msg'] = 'Error in sending file';
                    }
                } else {

                    http_response_code(401);
                    // $response['error'] = true;
                    $response['msg'] = 'Error in user';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;

        case 'updatephoto':

            if (isTheseParametersAvailable(array('username'))) {
                $token = check_input($_REQUEST['token']);
                $username = check_input($_REQUEST['username']);

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);

                if (empty($user)) {
                    http_response_code(401);
                    $response['msg'] = "username not found";
                    // echo json_encode(array("error_msg" => "username not found"));
                } else {


                    $fileInfo = PATHINFO($_FILES["photo"]["name"]);
                    $image_format = array('jpg', 'png', 'gif', 'bmp', 'webp', 'jpeg');

                    if (empty($_FILES["photo"]["name"])) {

                        $location = $srow['photo'];
                        http_response_code(401);
                        $response['msg'] = 'Uploaded photo is empty!';
                    } else {

                        $userid = $user['userid'];

                        // if ($fileInfo['extension'] == "jpg" OR $fileInfo['extension'] == "png") {
                        if (in_array($fileInfo['extension'], $image_format)) {

                            $newFilename = $fileInfo['filename'] . "_" . time() . "." . $fileInfo['extension'];

                            move_uploaded_file($_FILES["photo"]["tmp_name"], "../upload/" . $newFilename);

                            $location = "upload/" . $newFilename;
                            if (mysqli_query($conn, "update `user` set photo='$location' where userid='" . $userid . "'")) {
                                http_response_code(200);
                                $response['msg'] = 'Photo updated successfully!';
                            }
                        } else {
                            http_response_code(401);
                            $response['msg'] = "Photo not updated. We don't support {$fileInfo['extension']} file!";
                        }
                    }
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }


            break;
            // Added 2nd login api (15 April 2019)

        case 'social_media_login__expired':

            if (isset($_REQUEST['username']) && isset($_REQUEST['useremail'])) {

                $useremail = check_input($_REQUEST['useremail']);
                $photo = $_REQUEST['photo'];
                $res = mysqli_query($conn, "SELECT count(*) FROM `user` WHERE email='{$useremail}'");
                $num = mysqli_fetch_row($res)[0];

                if ($num > 0) {
                    http_response_code(200);
                    //  $response['msg'] = 'This Email already exist ,Please Try Login with your password ';  
                    $query2 = mysqli_query($conn, "SELECT * FROM `user` WHERE email='{$useremail}'");
                    $row = mysqli_fetch_array($query2);
                    $token = md5($password . time());
                    mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");
                    if (!empty($photo)) {
                        mysqli_query($conn, "UPDATE `user` SET photo = '{$photo}' WHERE userid={$row['userid']}");
                    } else {
                        $photo = $row['photo'];
                    }

                    $user = array(
                        'userid' => $row['userid'],
                        'username' => $row['username'],
                        'email' => $row['email'],
                        'phone' => $row['phone'],
                        'uname' => $row['uname'],
                        //  'photo' => (empty($row['photo']) ? '' : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
                        'photo' => $photo,
                        'country' => $row['country'],
                        'region' => $row['region'],
                        'timezone' => $row['timezone'],
                        'access' => $row['access'],
                        'token' => $token
                    );
                    http_response_code(200);
                    $response = $user;
                } else {

                    $useremail = check_input($_REQUEST['useremail']);

                    $nameforusrmail = check_input($_REQUEST['username']);
                    $fusername = check_input($_REQUEST['username']);
                    $email = check_input($_REQUEST['useremail']);
                    $fpassword = rand(99, 99999);
                    $fpassword = md5($fpassword);
                    $access = 2;
                    $activated = 1;
                    if (!empty($photo)) {
                        mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated,photo) values ('$nameforusrmail', '$fusername','$email', '$fpassword', '$access','$activated','$photo')");
                    } else {
                        mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated) values ('$nameforusrmail', '$fusername','$email', '$fpassword', '$access','$activated')");
                    }

                    $userdata = array(
                        'user_login'  =>  $fusername,
                        'user_nicename' => $fusername,
                        'user_email' => $email,
                        'display_name' => $fusername,
                        'user_pass'   =>  NULL  // When creating a new user, `user_pass` is expected.
                    );

                    $user_id = wp_insert_user($userdata);
                    wp_update_user(array('ID' => $user_id, 'role' => 'um_clients'));

                    $query2 = mysqli_query($conn, "select * from `user` where email='$email' and password='$fpassword' and activated=1");

                    $row = mysqli_fetch_array($query2);
                    $token = md5($password . time());
                    mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");

                    $user = array(
                        'userid' => $row['userid'],
                        'username' => $row['username'],
                        'email' => $row['email'],
                        'phone' => $row['phone'],
                        'uname' => $row['uname'],
                        'photo' => (empty($row['photo']) ? '' : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
                        'country' => $row['country'],
                        'region' => $row['region'],
                        'timezone' => $row['timezone'],
                        'access' => $row['access'],
                        'token' => $token
                    );

                    http_response_code(200);
                    $response = $user;
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;



        case 'updateonlinestatus':

            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = check_input($_REQUEST['username']);
                $token = check_input($_REQUEST['token']);
                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);
                if (empty($user)) {
                    http_response_code(401);
                    echo json_encode(array("error_msg" => "ERROR"));
                } else {
                    if (mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP() WHERE userid={$user['userid']}")) {
                        http_response_code(200);
                        echo json_encode(array("msg" => "update completed"));
                    } else {
                        http_response_code(401);
                        echo json_encode(array("error_msg" => "Update failed"));
                    }
                }
            } else {
                http_response_code(401);
                echo json_encode(array("error_msg" => "add username"));
            }
            break;



            // Tour Search API
        case 'tour_search':

            $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);


            $currency_usd = $wpdb->get_var("SELECT option_value FROM tourfrom_balieng2.wp_options WHERE option_name='_cs_currency_USD'");

            // Site Url
            $site_url = $wpdb->get_var("SELECT option_value FROM tourfrom_balieng2.wp_options WHERE option_name='siteurl'");
            $search_price_range = check_input($_REQUEST['range']);
            $search_days = check_input($_REQUEST['days']);
            $search_date = check_input($_REQUEST['date_range']);
            if (!empty($search_date)) {

                search_kw_store($search_date);

                $search_date_range = explode('--', $search_date);
                $date_lower_date = $search_date_range[0];
                $date_upper_date = $search_date_range[1];
            }


            if (!empty($search_days)) {
                search_kw_store($search_days);
            }

            if (!empty($search_price_range)) {
                search_kw_store($search_price_range);

                $search_price_range = explode('-', $search_price_range);
                $lower_limit = $search_price_range[0];

                $lower_limit = ceil($lower_limit / $currency_usd);

                $upper_limit = $search_price_range[1];
                $upper_limit = ceil($upper_limit / $currency_usd);
            }

            if (isTheseParametersAvailable(array('kw'))) {

                $search_term = check_input($_REQUEST['kw']);
                search_kw_store($search_term, $username, $token);
                $array_of_ids_in_search = array();

                //  selects all   starting with "term":
                $get_search = $wpdb->get_results("SELECT ID FROM tourfrom_balieng2.wp_posts WHERE  post_title LIKE '{$search_term}%'   AND post_status ='publish'");

                $row_count = $wpdb->num_rows;

                if ($row_count == 0 || $row_count == 1) {


                    $get_search = $wpdb->get_results("SELECT ID FROM tourfrom_balieng2.wp_posts WHERE  post_title LIKE '%{$search_term}%' AND post_status ='publish' ");
                    $row_count = $wpdb->num_rows;
                }

                if ($row_count == 0 || $row_count == 1) {


                    $get_search = $wpdb->get_results("SELECT ID FROM tourfrom_balieng2.wp_posts WHERE  post_content LIKE '%{$search_term}%' OR post_title LIKE '%{$search_term}%' AND  post_status ='publish' ");
                    $row_count = $wpdb->num_rows;
                }

                foreach ($get_search as $value) {
                    // Store all posts ids
                    $array_of_ids_in_search[] = $value->ID;
                }


                // main query

                $sql = "SELECT wp_tour.*,wp_tour_sale.number_of_people,wp_tour_sale.username,wp_tourmeta.meta_value
        FROM tourfrom_balieng2.wp_tour,tourfrom_balieng2.wp_tour_sale,tourfrom_balieng2.wp_tourmeta
        WHERE
        wp_tour.id=wp_tour_sale.tour_id
        AND wp_tourmeta.tour_id=wp_tour_sale.tour_id
        AND wp_tourmeta.meta_key='bookingdata'
        AND username!='agent2' AND username!='Lix'";

                if (!empty($search_price_range)) {

                    $sql .= " AND total>={$lower_limit}";
                    if ($upper_limit != 0)
                        $sql .= " AND total<={$upper_limit}";
                }

                $sql .= " ORDER BY wp_tour.score  DESC";

                $data = $wpdb->get_results($sql);


                foreach ($data as $t) {

                    // Getting Post Status
                    $post_status = $wpdb->get_row("SELECT post_status FROM tourfrom_balieng2.wp_posts,tourfrom_balieng2.wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';")->post_status;

                    if ($post_status != 'publish')
                        continue;

                    // Get the Posts data,like title and ID for further use if post is published
                    $postdata = $wpdb->get_row("SELECT  ID,post_title FROM tourfrom_balieng2.wp_posts,tourfrom_balieng2.wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';");

                    $post_ID = $postdata->ID;

                    // Filter the search Term
                    if (!empty($search_term)) {
                        if (!in_array($post_ID, $array_of_ids_in_search))
                            continue;
                    }

                    $images = array();

                    // Get the Images from the post content

                    /* $post_content=$wpdb->get_row("SELECT post_content FROM tourfrom_balieng2.wp_postmeta, tourfrom_balieng2.wp_posts WHERE wp_posts.ID=wp_postmeta.post_id AND `meta_key`='tour_id' AND `meta_value`='{$t->id}'")->post_content;
        */
                    if (preg_match('!<img.*?src="(.*?)"!', $post_content, $post_image_match))
                        $images[] = $post_image_match[1];

                    $agentrow = $wpdb->get_row("SELECT userid,uname FROM tourfrom_balieng2.user WHERE username='{$t->username}'");

                    $agent_id = $agentrow->userid;

                    $agent_name = $agentrow->uname;

                    if (empty($agent_name))

                        $agent_name = $t->username;

                    $bookingdata = unserialize($t->meta_value);

                    $hotelinfo = $wpdb->get_row("SELECT img,ratingUrl,location,start_date,end_date FROM tourfrom_balieng2.wp_hotel WHERE tour_id='{$t->id}'");


                    if (!empty($search_days)) {

                        $bookingdata = unserialize($t->meta_value);

                        $days_array = array(
                            $bookingdata['tourlist_date'], $bookingdata['place_date'], $bookingdata['eaidate'],
                            $bookingdata['boat_date'], $bookingdata['spa_date'], $bookingdata['meal_date'],
                            $bookingdata['car_date'], $bookingdata['popupguide_date'], $bookingdata['spotdate'], $bookingdata['restaurant_date']
                        );
                        $manualitems = $wpdb->get_results("SELECT meta_value FROM tourfrom_balieng2.`wp_tourmeta` where meta_key='manualitem' and tour_id=" . $t->id);
                        foreach ($manualitems as $manualItem) {
                            $days_array[] = unserialize($manualItem->meta_value)['date'] . ',';
                        }
                        $days = max(array_map(function ($value) {
                            return max(explode(',', $value));
                        }, $days_array));

                        if (intval($days) <= 0)
                            $days = 1;
                        if (!empty($search_days) && $days != $search_days)
                            continue;
                    }
                    if (!empty($search_date)) {

                        if (strtotime($hotelinfo->start_date) != strtotime($date_lower_date) and strtotime($hotelinfo->end_date) != strtotime($date_upper_date))
                            continue;
                    }


                    if (!empty($hotelinfo->img)) {

                        if (is_array(unserialize($hotelinfo->img))) {
                            foreach (unserialize($hotelinfo->img) as $hotelImgItem)
                                $images[] = $hotelImgItem;
                        } else
                            $images[] = $hotelinfo->img;
                    }
                    $post_content = $wpdb->get_row("SELECT post_content FROM tourfrom_balieng2.wp_postmeta, tourfrom_balieng2.wp_posts WHERE wp_posts.ID=wp_postmeta.post_id AND `meta_key`='tour_id' AND `meta_value`='{$t->id}'")->post_content;
                    if (preg_match('!<img.*?src="(.*?)"!', $post_content, $post_image_match))
                        $images[] = $post_image_match[1];

                    if (empty($images[0]))
                        $images[0] = "https://i.travelapi.com/hotels/1000000/910000/903000/902911/26251f24_y.jpg";

                    $cost = ceil((float) $t->total * $currency_usd);


                    /// Filter for Days
                    /*
        $days_array=array($bookingdata['tourlist_date'],$bookingdata['place_date'],$bookingdata['eaidate'],
        $bookingdata['boat_date'],$bookingdata['spa_date'],$bookingdata['meal_date'],
        $bookingdata['car_date'],$bookingdata['popupguide_date'],$bookingdata['spotdate'],$bookingdata['restaurant_date']);
        */
                    /*
        $manualitems = $wpdb->get_results("SELECT meta_value FROM tourfrom_balieng2.`wp_tourmeta` where meta_key='manualitem' and tour_id=" . $t->id);
        foreach($manualitems as $manualItem) {
        $days_array[]=unserialize($manualItem->meta_value)['date'].',';
        }
        $days=max(array_map(function($value) { return max(explode(',', $value)); }, $days_array));

        if (intval($days) <= 0)
        $days = 1;
        */
                    $data11[] = $t->id;
                    $post_link = $site_url . "/?p=" . $post_ID;
                    $data1[] = array('post_title' => $postdata->post_title, 'post_id' => $post_ID, 'bookingcode' => $t->id, 'location' => $hotelinfo->location, 'image' => $images[0], 'cost' => $cost, 'post_link' => $post_link);
                }



                if (empty($data1)) :
                    http_response_code(401);
                    $response['msg'] = 'NO data Found!';
                else :
                    http_response_code(200);
                    $response['data'] =  $data1;

                endif;
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }


            break;


            // Tour list added to api
        case 'tourlist':
            $currencyName = filter_input(INPUT_GET, 'currency', FILTER_SANITIZE_STRING);
            $currencyList = get_option('currency_list');
            if (is_array($currencyList[$currencyName])) {
                $currency = $currencyList[$currencyName]['rate'];
            } else {
                $currencyName = 'idr';
                $currency = 1;
            }

            // sort by score
            $popularity_order = check_input($_REQUEST['popularity_order']);

            // sort by cost
            $price_order = $_REQUEST['price_order'];

            $duration_order = $_REQUEST['duration_order'];

            // Site Url
            $site_url = $wpdb->get_var("SELECT option_value FROM tourfrom_balieng2.wp_options WHERE option_name='siteurl'");

            $sql = "SELECT wp_tour.*,wp_tour_sale.number_of_people,wp_tour_sale.netprice,wp_tour_sale.username,wp_tourmeta.meta_value
            FROM tourfrom_balieng2.wp_tour,tourfrom_balieng2.wp_tour_sale,tourfrom_balieng2.wp_tourmeta
            WHERE
            wp_tour.id=wp_tour_sale.tour_id
            AND wp_tourmeta.tour_id=wp_tour_sale.tour_id
            AND wp_tourmeta.meta_key='bookingdata'
            AND username!='agent2' AND username!='Lix' AND total!=0";

            if (!empty($popularity_order) and $popularity_order == 'htl') {
                $sql .= ' ORDER BY wp_tour.score DESC';
            } elseif (!empty($popularity_order) and $popularity_order == 'lth') {
                $sql .= ' ORDER BY wp_tour.score ASC';
            } elseif (!empty($price_order) and $price_order == 'htl') {
                $sql .= ' ORDER BY wp_tour.total DESC';
            } elseif (!empty($price_order) and $price_order == 'lth') {
                $sql .= ' ORDER BY wp_tour.total ASC';
            } else {
                $sql .= ' ORDER BY wp_tour.score DESC';
            }

            $data = $wpdb->get_results($sql);
            foreach ($data as $t) {
                $post_status = $wpdb->get_row("SELECT post_status FROM tourfrom_balieng2.wp_posts,tourfrom_balieng2.wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';")->post_status;

                if ($post_status != 'publish')
                    continue;

                $postdata = $wpdb->get_row("SELECT  ID,post_title FROM tourfrom_balieng2.wp_posts,tourfrom_balieng2.wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';");
                $post_ID = $postdata->ID;
                $post_title = $postdata->post_title;
                $cost = ceil((float) $t->total * $currency);

                $hotelinfo = $wpdb->get_row("SELECT img,ratingUrl,location,start_date FROM tourfrom_balieng2.wp_hotel WHERE tour_id='{$t->id}'");

                if (!empty($hotelinfo->img)) {
                    if (is_array(unserialize($hotelinfo->img))) {
                        foreach (unserialize($hotelinfo->img) as $hotelImgItem)
                            $images[] = $hotelImgItem;
                    } else
                        $images[] = $hotelinfo->img;
                }
                $post_content = $wpdb->get_row("SELECT post_content FROM tourfrom_balieng2.wp_postmeta, tourfrom_balieng2.wp_posts WHERE wp_posts.ID=wp_postmeta.post_id AND `meta_key`='tour_id' AND `meta_value`='{$t->id}'")->post_content;

                if (preg_match('!<img.*?src="(.*?)"!', $post_content, $post_image_match))
                    $images[] = $post_image_match[1];

                if (empty($post_image_match[1]))
                    $post_image_match[1] = "https://i.travelapi.com/hotels/1000000/910000/903000/902911/26251f24_y.jpg";

                $agent_name = $t->username;
                //days
                $days_array = array(
                    $bookingdata['tourlist_date'], $bookingdata['place_date'], $bookingdata['eaidate'],
                    $bookingdata['boat_date'], $bookingdata['spa_date'], $bookingdata['meal_date'],
                    $bookingdata['car_date'], $bookingdata['popupguide_date'], $bookingdata['spotdate'], $bookingdata['restaurant_date']
                );
                $manualitems = $wpdb->get_results("SELECT meta_value FROM tourfrom_balieng2.`wp_tourmeta` where meta_key='manualitem' and tour_id=" . $t->id);
                foreach ($manualitems as $manualItem) {
                    $days_array[] = unserialize($manualItem->meta_value)['date'] . ',';
                }
                $days = max(array_map(function ($value) {
                    return max(explode(',', $value));
                }, $days_array));
                if (intval($days) <= 0)
                    $days = 1;
                $post_link = $site_url . "/?p=" . $post_ID;
                $data1[] = array(
                    'post_title' => htmlspecialchars_decode($postdata->post_title),
                    'post_id' => $post_ID,
                    'bookingcode' => $t->id,
                    'location' => $hotelinfo->location,
                    'image' => $post_image_match[1],
                    'cost' => strtoupper($currencyName) . ' ' . $cost . ' ' . $currencyList[$currencyName]['symbol'],
                    'seller' => $agent_name,
                    'post_link' => $post_link,
                    'days' => $days
                );
            }

            if (isset($duration_order) and $duration_order == 'lth') {
                $data1 = array_orderby($data1, 'days', SORT_ASC);
            } elseif (isset($duration_order) and $duration_order == 'htl') {
                $data1 = array_orderby($data1, 'days', SORT_DESC);
            }

            http_response_code(200);
            $response['data'] = $data1;
            break;


            // Update device token for firebase

        case 'update_device_token':
            if (isTheseParametersAvailable(array('user_id', 'device_token'))) {


                $user_id = $_POST['user_id'];
                $device_token =  $_POST['device_token'];
                $query = " UPDATE `sessions` SET device_token = '{$device_token}' WHERE `user_id` = {$user_id}  ";
                $result = mysqli_query($conn, $query);
                http_response_code(200);
                $response['msg'] = "Updated";
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;

        case 'social':
            if (isTheseParametersAvailable(array('name', 'email', 'photo', 'connector'))) {

                $user_name = $_REQUEST['name'];
                $user_email = $_REQUEST['email'];
                $user_avt = $_REQUEST['photo'];
                $connector = $_REQUEST['connector'];
                $pass  = $_REQUEST['pass'];

                if (empty($pass)) {
                    $pass = wp_generate_password();
                    $password = md5($pass);
                }


                if ($wpdb->get_var("SELECT COUNT(*) from wp_users where user_email = '{$user_email}'") and $wpdb->get_var("SELECT COUNT(*) from user where email = '{$user_email}'")) {

                    $query2 = mysqli_query($conn, "SELECT * FROM `user` WHERE email='{$user_email}'");
                    $row = mysqli_fetch_array($query2);
                    $userid = $row['userid'];

                    if ($wpdb->get_var("SELECT COUNT(*) from social_login_data where email = '{$user_email}'")) {
                        mysqli_query($conn, "UPDATE social_login_data SET image='{$user_avt}' WHERE email='{$user_email}'");
                    } else {

                        mysqli_query($conn, "insert into `social_login_data` (user_id,email,name,connector,image) values ('$userid','$user_email','$user_nicename','$connector','$user_avt')");
                    }

                    $token = md5($pass . time());
                    mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");

                    $user = array(
                        'userid' => $row['userid'],
                        'username' => $row['username'],
                        'email' => $row['email'],
                        'phone' => $row['phone'],
                        'uname' => $row['uname'],
                        'photo' => $user_avt,
                        'country' => $row['country'],
                        'region' => $row['region'],
                        'timezone' => $row['timezone'],
                        'access' => $row['access'],
                        'token' => $token
                    );
                    http_response_code(200);
                    $response = $user;

                    /*
       //mysqli_query($conn, "UPDATE wp_users SET user_pass='{$passmd5}' WHERE user_email='{$user_email}'");


         $creds = array(
        'user_login'    => $user_name,
        'user_password' => $pass
    );

    $user = wp_signon( $creds, false );
 
    if ( is_wp_error( $user ) ) {
       $response['msg']=  $user->get_error_message();
    }
    else
    {
            $response['msg'] =$user;

    }
    */
                } else {
                    $userdata = array(
                        'user_login'  =>  $user_name,
                        'user_nicename' => $user_name,
                        'user_email' => $user_email,
                        'display_name' => $user_name,
                        'user_pass'   =>  $pass  // When creating a new user, `user_pass` is expected.
                    );


                    $user_id = wp_insert_user($userdata);
                    wp_update_user(array('ID' => $user_id, 'role' => 'um_clients'));


                    $access = 2;
                    $activated = 1;
                    mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated) values ('$user_name', '$user_name','$user_email', '$password', '$access','$activated')");
                    $query2 = mysqli_query($conn, "SELECT * FROM `user` WHERE email='{$user_email}'");
                    $row = mysqli_fetch_array($query2);
                    $userid = $row['userid'];


                    mysqli_query($conn, "insert into `social_login_data` (user_id,email,name,connector,image) values ('$userid','$user_email','$user_nicename','$connector','$user_avt')");

                    $token = md5($pass . time());
                    mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");
                    $user = array(
                        'userid' => $row['userid'],
                        'username' => $row['username'],
                        'email' => $row['email'],
                        'phone' => $row['phone'],
                        'uname' => $row['uname'],
                        'photo' => $user_avt,
                        'country' => $row['country'],
                        'region' => $row['region'],
                        'timezone' => $row['timezone'],
                        'access' => $row['access'],
                        'token' => $token
                    );
                    http_response_code(200);
                    $response = $user;

                    /*
         $creds = array(
        'user_login'    => $user_name,
        'user_password' => $pass
    );

    $user = wp_signon( $creds, false );
 
    if ( is_wp_error( $user ) ) {
       $response['msg']=  $user->get_error_message();
    }
    else
    {
            $response['msg'] =$user;

    }
    */
                }
            }
            break;


        case 'voucher':
            $options = get_option('_travpart_vo_option');
            $list = $wpdb->get_results("SELECT wp_users.ID,wp_users.user_nicename,wp_coupons.* FROM wp_coupons,wp_users WHERE wp_coupons.user_id=wp_users.ID AND DATE_SUB(CURDATE(), INTERVAL {$options['tp_coupon_display_kept_day']} DAY)<=DATE(get_time) ORDER BY wp_coupons.id DESC LIMIT 3", ARRAY_A);
            $result = array();
            foreach ($list as $row) {
                $tmp['username'] = $row['user_nicename'];
                $tmp['avatar'] = um_get_user_avatar_url($row['ID'], 80);
                $tmp['discount'] = ($row['discount'] == '%' ? $row['figure'] . $row['discount'] : $row['discount'] . $row['figure']);
                $tmp['type'] = $row['type'];
                $result[] = $tmp;
            }
            if (!empty($result)) {
                http_response_code(200);
                $response['result'] = $result;
            } else {
                http_response_code(401);
                $response['msg'] = 'There is not any voucher';
            }
            break;

        case 'asksupport':

            if (isTheseParametersAvailable(array('username', 'token', 'email', 'name', 'question'))) {

                $token = check_input($_REQUEST['token']);
                $username = check_input($_REQUEST['username']);

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);
                if (empty($user)) {
                    http_response_code(401);
                    $response['msg'] = 'user not found';
                } else {
                    $csa_email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
                    $csa_name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
                    $csa_message = filter_input(INPUT_POST, 'question', FILTER_SANITIZE_STRING);
                    $csa_destination = filter_input(INPUT_POST, 'destination', FILTER_SANITIZE_STRING);
                    $csa_subject = 'Quote request';
                    $csa_question_cat = 'Ask a support';

                    $chatUserId = $user['userid'];

                    $csa_agents = $wpdb->get_results("SELECT username,email FROM `user` LEFT JOIN `sessions` ON `sessions`.`id` = (
                    SELECT `id` from `sessions` where `user_id` = `user`.`userid` ORDER BY `id` DESC LIMIT 1)
                    WHERE access=1 AND activated=1 ORDER BY last_activity DESC LIMIT 10", ARRAY_A);
                    $csa_agent_name = $csa_agents[0]['username'];

                    $agentInfo = $wpdb->get_row("SELECT `userid`,`email` FROM `user` WHERE `username`='{$csa_agent_name}'");
                    $csa_agent_email = $agentInfo->email;
                    $agentUserid = $agentInfo->userid;

                    $toAgentMailBody = <<<EOD
Hi {$csa_agent_name},
You have received an inquiry from {$csa_name} about {$csa_destination}. Below is full description about his/her query.

{$csa_message}

Thanks,

The Travpart Team
EOD;

                    $wpdb->insert(
                        'contact_sales_agent',
                        array(
                            'csa_user_name' => $csa_name,
                            'csa_user_email' => $csa_email,
                            'csa_subject' => $csa_subject,
                            'csa_destination_plan' => $csa_destination,
                            'csa_agent_email' => $csa_agent_email,
                            'csa_message' => $csa_message,
                            'csa_question_cat' => $csa_question_cat,
                            'csa_created' => date('Y-m-d H:i:s'),
                        ),
                        array(
                            '%s',
                            '%s',
                            '%s',
                            '%s',
                            '%s',
                            '%s',
                            '%s',
                            '%s'
                        )
                    );

                    if ($wpdb->insert_id) {

                        $wpdb->query("insert into `chat` (message, userid, chat_date,c_to,c_from,type,attachment_type) values ('" . trim($toAgentMailBody) . "' , '{$chatUserId}', NOW(),'{$agentUserid}','{$chatUserId}','','')");

                        include(get_template_directory() . "/potential-sales-agents-email-content.php");
                        wp_mail($csa_agent_email, 'A travel customer for you from travpart.com', str_replace('[name]', $csa_agent_name, $potential_sale_agent_email_content));
                        $potential_sale_agents = $wpdb->get_results("SELECT `name`,`email`,`create_time` FROM `wp_potential_sale_agent` WHERE location LIKE '{$csa_destination_plan}'");
                        foreach ($potential_sale_agents as $t) {
                            $lasted_email_send_time = $wpdb->get_var("SELECT `csa_created` FROM `contact_sales_agent` WHERE `csa_created`>'{$t->create_time}' ORDER BY `csa_created` ASC LIMIT 1");
                            $create_time = strtotime($t->create_time);
                            if (!empty($lasted_email_send_time) && $create_time < strtotime($lasted_email_send_time)) {
                                if ((time() - strtotime($lasted_email_send_time)) > 15 * 24 * 60 * 60)
                                    continue;
                            }
                            wp_mail($t->email, 'A travel customer for you from travpart.com', str_replace('[name]', $t->name, $potential_sale_agent_email_content));
                        }
                        foreach ($csa_agents as $key => $t) {
                            if ($key == 0)
                                continue;
                            wp_mail($t['email'], 'A travel customer for you from travpart.com', str_replace('[name]', $t['username'], $potential_sale_agent_email_content));
                        }
                        http_response_code(200);
                        $response['msg'] = 'success';
                    } else {
                        http_response_code(401);
                        $response['msg'] = 'System error. Try again later';
                    }
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;

        case 'potentialbuyer':

            if (isTheseParametersAvailable(array('username', 'token'))) {

                $token = check_input($_REQUEST['token']);
                $username = check_input($_REQUEST['username']);

                $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
                $user = mysqli_fetch_assoc($res);
                if (empty($user)) {
                    http_response_code(401);
                    $response['msg'] = 'user not found';
                } else {
                    $userid = $user['userid'];
                    $list = $wpdb->get_results("SELECT user.userid,user.username FROM `user`,`wp_users`
                    WHERE user.username=wp_users.user_login AND user.access=2
                        AND wp_users.ID IN (
                            SELECT wp_user_trace.user_id FROM `wp_users`,`user`,`wp_posts`,`wp_user_trace`
                                WHERE wp_users.user_login=user.username AND user.userid={$userid}
                            AND wp_posts.post_author=wp_users.ID AND wp_posts.post_type='post'
                            AND wp_user_trace.type='page' AND wp_user_trace.value=wp_posts.ID
                        )");
                    http_response_code(200);
                    $response['list'] = $list;
                    $response['msg'] = 'success';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;

            // api end point for showing unread messages in app

        case 'unseen_notifaction_count':

            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = check_input($_REQUEST['username']);
                $token = check_input($_REQUEST['token']);
                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
                if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {
                    $notifications_count = $wpdb->get_var("SELECT COUNT(*) FROM notification WHERE wp_user_id='{$wp_user_ID}' AND read_status=0");
                    http_response_code(200);
                    $response['count'] = $notifications_count;
                    $response['msg'] = 'success';
                } else {
                    http_response_code(401);
                    $response['msg'] = 'No user found';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;
            // Update status
        case 'update_notifaction_status':
            if (isTheseParametersAvailable(array('id'))) {

                $notification_id = check_input($_REQUEST['id']);
                $wpdb->update(
                    'notification',
                    array(
                        'read_status' => 1
                    ),
                    array('ID' => $notification_id),
                    array(
                        '%d'    // value2
                    ),
                    array('%d')
                );
                http_response_code(200);
                $response['msg'] = 'status updated';
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;
        case 'seller_tour_check':

            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = check_input($_REQUEST['username']);
                $token = check_input($_REQUEST['token']);
                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
                if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {

                    $sql = "SELECT  COUNT(*)
            FROM tourfrom_balieng2.wp_tour,tourfrom_balieng2.wp_tour_sale,tourfrom_balieng2.wp_tourmeta
            WHERE
            wp_tour.id=wp_tour_sale.tour_id
            AND wp_tourmeta.tour_id=wp_tour_sale.tour_id
            AND wp_tourmeta.meta_key='bookingdata'
            AND username ='$username'";
                    $data = $wpdb->get_var($sql);
                    http_response_code(200);
                    $response['no_of_tours'] = $data;
                    $response['msg'] = "success";
                } else {
                    http_response_code(401);
                    $response['msg'] = 'Invalid login';
                }
            } else {

                http_response_code(401);
                $response['msg'] = 'Invalid parameters';
            }

            break;

        case 'unread_messages_count':

            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = check_input($_REQUEST['username']);
                $token = check_input($_REQUEST['token']);
                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
                if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {

                    $message_count =  $wpdb->get_var("SELECT COUNT(*) FROM `chat`,`user` WHERE `c_to`=user.userid AND user.username='{$username}' AND `status`=0");
                    $response['no_of_messages'] = $message_count;
                    $response['msg'] = "success";
                } else {
                    http_response_code(401);
                    $response['msg'] = 'Invalid login';
                }
            } else {

                http_response_code(401);
                $response['msg'] = 'Invalid parameters';
            }

            break;

        case 'buyer_to_seller_chat_check':

            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = check_input($_REQUEST['username']);
                $token = check_input($_REQUEST['token']);
                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
                if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {

                    $message_count =  $wpdb->get_var("SELECT COUNT(*) FROM `chat`,`user` WHERE `c_from`='{$wp_user_ID}' AND user.username='{$username}'");
                    $response['no_of_chats_to_sellers'] = $message_count;
                    $response['msg'] = "success";
                } else {
                    http_response_code(401);
                    $response['msg'] = 'Invalid login';
                }
            } else {

                http_response_code(401);
                $response['msg'] = 'Invalid parameters';
            }

            break;

        case 'unseen_blog_notifaction_count':

            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = check_input($_REQUEST['username']);
                $token = check_input($_REQUEST['token']);
                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
                if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {
                    $notifications_count = $wpdb->get_var("SELECT COUNT(*) FROM notification WHERE wp_user_id='{$wp_user_ID}' AND read_status=0 AND content LIKE '%Check out%'");
                    http_response_code(200);
                    $response['count'] = $notifications_count;
                    $response['msg'] = 'success';
                } else {
                    http_response_code(401);
                    $response['msg'] = 'No user found';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;;
        case 'customer_requests':

            if (isTheseParametersAvailable(array('username', 'token'))) {


                $username = check_input($_REQUEST['username']);
                $token = check_input($_REQUEST['token']);

                $wp_user_email = $wpdb->get_var("SELECT wp_users.user_email FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                $request_list = $wpdb->get_results("SELECT contact_sales_agent.*,user.userid  FROM `contact_sales_agent`,`user`  WHERE  `csa_user_name`=`user`.`username` AND csa_agent_email ='{$wp_user_email}'");
                if (!empty($request_list)) {

                    foreach ($request_list as $single) {


                        $list[] =
                            array(
                                'id' => $single->csa_id,
                                'user_id' => $single->userid,
                                'username' => $single->csa_user_name,
                                'subject' => $single->csa_subject,
                                'plan' => $single->csa_destination_plan,
                                'msg' => $single->csa_message,
                                'cat' => $single->csa_question_cat,
                                'created' => $single->csa_created
                            );
                    }
                    http_response_code(200);
                    $response['msg'] = $list;
                } else {
                    http_response_code(200);
                    $response['msg'] = 'No data';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;

        case 'currencies':
            $currencyList = get_option('currency_list');
            if (!empty($currencyList)) {
                http_response_code(200);
                $response['currencies'] = $currencyList;
                $response['msg'] = 'success';
            } else {
                http_response_code(401);
                $response['msg'] = 'There is not any currency';
            }
            break;

        case 'Report_tour':
            if (isTheseParametersAvailable(array('username', 'token'))) {


                $username = check_input($_REQUEST['username']);
                $token = check_input($_REQUEST['token']);
                $tour_id = check_input($_REQUEST['tour_id']);
                $report_comment = check_input($_REQUEST['report_msg']);

                $wp_user_data = $wpdb->get_results("SELECT wp_users.user_email,wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if ($wp_user_data) {
                    foreach ($wp_user_data as $value) {

                        $sf_user_id = $value->ID;
                        $email = $value->user_email;
                    }

                    $tourData = $wpdb->get_results("SELECT * FROM `reported_tours` WHERE reported_tour_id = '$tour_id' AND report_by_user_id = '$sf_user_id'", ARRAY_A);
                    if ($tourData) {
                        $data['status'] = 1;
                        $response['msg'] =  "You already Reported that Tour";
                    } else {

                        $wpdb->insert(
                            'reported_tours',
                            array(
                                'reported_tour_id' => $tour_id,
                                'report_by_user_id' => $sf_user_id,
                                'report_by_user_email' => $email,
                                'report_text' => $report_comment,

                            ),
                            array(
                                '%s',
                                '%s',
                                '%s',
                                '%s',
                            )
                        );



                        // send email

                        $to = 'tourfrombali@gmail.com';
                        $subject = 'Tour Reported';
                        $body =   $email . " Reported the Tour with id: " . $tour_id . " Please Review the issue";
                        $headers = array('Content-Type: text/html; charset=UTF-8');

                        sendmailtosuer('tourfrombali@gmail.com', $subject, $body);

                        $response['status'] = 1;
                        $response['msg'] =  'You Reported Tour Successfully,Thank you !';
                    }
                } else {
                    http_response_code(401);
                    $response['msg'] = 'No user Exist';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;

        case 'getnotifications':
            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
                if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {
                    $notificationslist = $wpdb->get_results("SELECT * FROM notification WHERE wp_user_id='{$wp_user_ID}' AND read_status=0 ORDER BY id DESC");

                    foreach ($notificationslist as $single) {
                        /*
            $url = $single->content;
                $notifications[] = array(
                    'id'=>$single->id,
                    'content'=>$single->content,

                );
                */

                        $string = $single->content;

                        preg_match('/\>(.*)<\/a>/', $string, $matches);
                        $noti_text = $matches[1];
                        preg_match('/href=(["\'])([^\1]*)\1/i', $string, $m);

                        $list[] =
                            array(
                                'id' => $single->id,
                                'text' => !empty($noti_text) ? $noti_text : $string,
                                'url' => $m[2],
                                'time' => $single->create_time,
                                'status' => 0
                            );
                    }
                    http_response_code(200);
                    $response['msg'] = "notifications retrived successfully";
                    $response['notifications'] = $list;
                } else {
                    http_response_code(401);
                    $response['msg'] = "username does not exists";
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;

            // send friend request api end point
        case 'send_friend_request':

            if (isTheseParametersAvailable(array('username', 'token', 'receiver_user_id'))) {

                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $receiver_user_id =  filter_input(INPUT_POST, 'receiver_user_id', FILTER_SANITIZE_STRING);
                // check either receiver user id exist

                $user = get_userdata($receiver_user_id);



                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if ($wp_user_ID and !empty($receiver_user_id) and $user != false) {

                    $sql = $wpdb->prepare("INSERT INTO `friends_requests` (`user1`, `user2`,`action_by_user`) values (%d, %d,%d)", $wp_user_ID, $receiver_user_id, $wp_user_ID);

                    $wpdb->query($sql);

                $token =  to_get_user_device_token_from_wp_id_for_api($receiver_user_id);

                if($token){
                $msg =  "Sent you friend request";
                $identifier="friends_request_received";
                notification_for_user_form_api($token,$msg,$identifier);
                }

                    http_response_code(200);
                    $response['success'] = 1;
                    $response['msg'] = "friend request sent successfully";
                } else {

                    http_response_code(401);
                    $response['success'] = 0;
                    $response['msg'] = 'Some error occured';
                }
            } else {

                http_response_code(401);
                $response['success'] = 0;
                $response['msg'] = 'Required parameter missing';
            }

            break;

            // cancle friend request api end point
        case 'cancel_friend_request':

            if (isTheseParametersAvailable(array('username', 'token', 'receiver_user_id'))) {

                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $receiver_user_id =  filter_input(INPUT_POST, 'receiver_user_id', FILTER_SANITIZE_STRING);
                // check either receiver user id exist

                $user = get_userdata($receiver_user_id);



                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if ($wp_user_ID and !empty($receiver_user_id) and $user != false) {

                    $check_request_status = $wpdb->get_var("SELECT COUNT(*) FROM friends_requests WHERE user1 = '{$wp_user_ID}' AND user2 = '{$receiver_user_id}'");

                    // $sql = $wpdb->prepare("INSERT INTO `friends_requests` (`user1`, `user2`,`action_by_user`) values (%d, %d,%d)", $wp_user_ID,$receiver_user_id,$wp_user_ID);

                    if ($check_request_status > 0) {

                        $wpdb->query(
                            "DELETE  FROM friends_requests
            WHERE user1 = '{$wp_user_ID}' AND user2 ='{$receiver_user_id}'"

                        );
                        http_response_code(200);
                        $response['success'] = 1;
                        $response['msg'] = "Request cancled";
                    } else {

                        http_response_code(401);
                        $response['success'] = 0;
                        $response['msg'] = 'You have no connection request with user';
                    }
                } else {

                    http_response_code(401);
                    $response['success'] = 0;
                    $response['msg'] = 'Some error';
                }
            } else {

                http_response_code(401);
                $response['success'] = 0;
                $response['msg'] = 'Required parameter missing';
            }

            break;

        case 'friends_requests';

            if (isTheseParametersAvailable(array('username', 'token'))) {

                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if ($wp_user_ID) {

                    $connections = $wpdb->get_results(
                        "SELECT * FROM friends_requests WHERE  user2 = '{$wp_user_ID}' AND status=0 "
                    );

                    foreach ($connections as $value) {

                        $Data = $wpdb->get_row("SELECT * FROM `wp_users` WHERE ID = '$value->user1'");

                        $requests[] = array(

                            'user_name' => $Data->user_login,
                            'user_id' => $value->user1,
                            'img' => esc_url(get_avatar_url($value->user1))
                        );
                    }
                    http_response_code(200);
                    $response['succes'] = 1;
                    $response['msg'] = 'retrived list';
                    $response['data'] = $requests;
                } else {
                    http_response_code(401);
                    $response['succes'] = 0;
                    $response['msg'] = 'No user exist';
                }
            } else {

                http_response_code(401);
                $response['success'] = 0;
                $response['msg'] = 'Required parameter missing';
            }

            break;

        case 'friend_suggestions':

            if (isTheseParametersAvailable(array('username', 'token'))) {

                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");
                $user_id = $wp_user_ID;
                $edu_level = get_user_meta($user_id, 'edu_level', true);
                $religion = get_user_meta($user_id, 'religion', true);
                $hometown = get_user_meta($user_id, 'hometown', true);
                $hobby = get_user_meta($user_id, 'hobby', true);


                if ($wp_user_ID) {

                    $suggestions = $wpdb->get_results("SELECT * FROM $wpdb->users
        LEFT JOIN $wpdb->usermeta ON $wpdb->users.ID = $wpdb->usermeta.user_id
        WHERE  (meta_key = 'edu_level'AND meta_value = '$edu_level' AND user_id!='$user_id') OR (meta_key = 'religion'AND meta_value = '$religion' AND user_id!='$user_id') OR (meta_key = 'hometown'AND meta_value = '$hometown' AND user_id!='$user_id') OR (meta_key = 'hobby'AND meta_value LIKE '%{$hobby}%' AND user_id!='$user_id')");

                    if ($suggestions) {

                        //if data
                        foreach ($suggestions as $value) {


                            $Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user1 = '$user_id' AND user2 = '{$value->ID}'");

                            if (empty($Data)) {

                                $reverse_check = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '$user_id' AND user1 = '{$value->ID}'");

                                if (!empty($reverse_check) and isset($reverse_check)) {

                                    $button_text = "Connected";
                                } else {
                                    $button_text = 'Connect';
                                }

                                // end of if
                            } else {

                                if ($Data->status == "0") {

                                    $button_text = "Pending";
                                } elseif ($Data->status == "1") {

                                    $button_text = "Connected";
                                } elseif ($Data->status == "2") {

                                    $button_text = "Blocked";
                                }
                            }
                            $same_religion  = get_user_meta($value->ID, 'religion', true);
                            $same_hometown  = get_user_meta($value->ID, 'hometown', true);

                            $list[] = array(
                                'username' => $value->user_nicename,
                                'user_id' => $value->ID,
                                'img' => esc_url(get_avatar_url($value->ID)),
                                'religion' => $same_religion,
                                'hometown' => $same_hometown,
                                'status' => $button_text

                            );
                        }
                        http_response_code(200);
                        $response['success'] = 1;
                        $response['msg'] = 'suggestions';
                        $response['suggestion_list'] = $list;
                    } else {

                        http_response_code(200);
                        $response['success'] = 1;
                        $response['msg'] = 'No suggestions';
                    }
                } else {

                    http_response_code(400);
                    $response['success'] = 0;
                    $response['msg'] = 'No user found';
                }
            } else {

                http_response_code(401);
                $response['success'] = 0;
                $response['msg'] = 'Required parameter missing';
            }

            break;

        case 'get_short_posts':

            if (isTheseParametersAvailable(array('username', 'token'))) {

                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if ($wp_user_ID) {

                    $args = array(
                        'post_type'  => 'shortpost',
                        'author'     =>  $wp_user_ID,
                    );
                    $short_posts = get_posts($args);

                    if ($short_posts) {


                        foreach ($short_posts as $single_post) {


                            $postslist[] = array(

                                'post_id' => $single_post->ID,
                                'post_content' => $single_post->post_content,
                                'time' => $single_post->post_modified,
                                'feeling' => get_post_meta($single_post->ID, 'feeling', true)
                            );
                        }

                        http_response_code(200);
                        $response['success'] = 1;
                        $response['msg'] = "posts retrived";
                        $response['data'] = $postslist;
                    } else {
                        http_response_code(200);
                        $response['success'] = 0;
                        $response['msg'] = 'No Posts';
                    }
                } else {

                    http_response_code(400);
                    $response['success'] = 0;
                    $response['msg'] = 'No user found';
                }
            } else {

                http_response_code(401);
                $response['success'] = 0;
                $response['msg'] = 'Required parameter missing';
            }
            break;
            // Feed like facebook posting on profile
        case 'feed_post':
            if (isTheseParametersAvailable(array('username', 'token', 'post_content'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $location = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING);
                $content = filter_input(INPUT_POST, 'post_content', FILTER_SANITIZE_STRING);
                $feeling = filter_input(INPUT_POST, 'feeling', FILTER_SANITIZE_STRING);
                $tour_id = filter_input(INPUT_POST, 'tourpackage', FILTER_SANITIZE_NUMBER_INT);
                $tag_user_id = filter_input(INPUT_POST, 'tagfriend', FILTER_SANITIZE_NUMBER_INT);
                $lookingfriend = filter_input(INPUT_POST, 'lookingfriend', FILTER_SANITIZE_NUMBER_INT);

                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if ($wp_user_ID) {
                    $post_content = wp_rel_nofollow($content);
                    $location = htmlspecialchars($location);
                    $new_post = array(
                        'post_content'   => wp_kses_post($post_content),
                        'post_type'      => 'shortpost',
                        'post_status'    => 'publish',
                        'post_author' => $wp_user_ID
                    );

                    $new_post_id = wp_insert_post($new_post, true);

                    if (is_wp_error($new_post_id)) {
                        http_response_code(401);
                        $response['msg'] = $new_post_id->get_error_message();
                        break;
                    }

                    if (!empty($_FILES['photo_or_video'])) {
                        $attachment_id = media_handle_upload('photo_or_video', $new_post_id, array('post_author' => $wp_user_ID));
                        if (is_wp_error($attachment_id)) {
                            http_response_code(401);
                            $response['msg'] = 'File is invaild';
                            break;
                        } else {
                            update_post_meta($new_post_id, 'attachment', $attachment_id);
                        }
                    }

                    if (!empty($location)) {
                        update_post_meta($new_post_id, 'location', $location);
                    }

                    if (!empty($feeling)) {
                        update_post_meta($new_post_id, 'feeling', base64_encode($feeling));
                    }

                    if (!empty($tag_user_id)) {
                        update_post_meta($new_post_id, 'tag_user_id', $tag_user_id);
                    }

                    if ($lookingfriend == 1) {
                        update_post_meta($new_post_id, 'lookingfriend', 1);
                        $friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE ID IN(
                            SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_user_ID}')
                            OR ID IN(
                                SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_user_ID}')");
                        foreach ($friends as $f) {
                            $wpdb->insert('notification', array('wp_user_id' => $f->ID, 'content' => um_get_display_name($wp_user_ID) . " is looking for a travel friend"));
                        }
                    }

                    if (!empty($tour_id)) {
                        $tour = $wpdb->get_row("SELECT ID,post_title FROM `wp_posts`,`wp_postmeta`
                            WHERE post_type='post' AND post_status='publish' AND wp_posts.ID=wp_postmeta.post_id
                            AND `meta_key`='tour_id' AND meta_value='{$tour_id}'");
                        update_post_meta($new_post_id, 'tour_post', $tour->ID);
                        update_post_meta($new_post_id, 'tour_title', $tour->post_title);

                        $hotelinfo = $wpdb->get_row("SELECT img,location FROM wp_hotel WHERE tour_id='{$tour_id}' LIMIT 1");
                        if (!empty($hotelinfo->img)) {
                            if (is_array(unserialize($hotelinfo->img))) {
                                $hotelimg = unserialize($hotelinfo->img)[0];
                            } else
                                $hotelimg = $hotelinfo->img;
                        }
                        if (!empty($hotelimg)) {
                            update_post_meta($new_post_id, 'tour_img', $hotelimg);
                        }
                    }

                    http_response_code(200);
                    $response['msg'] = 'Posted successfully';
                }
                // if user not exist
                else {
                    http_response_code(401);
                    $response['msg'] = 'User does not exist';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;

        case 'create_event_post':
            if (isTheseParametersAvailable(array('username', 'token', 'event_location'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $event_name = filter_input(INPUT_POST, 'event_name', FILTER_SANITIZE_STRING);
                $event_type = filter_input(INPUT_POST, 'event_type', FILTER_SANITIZE_STRING);
                $event_invited_people = $_POST['event_invited_people'];
                $event_location = filter_input(INPUT_POST, 'event_location', FILTER_SANITIZE_STRING);
                $start_date = filter_input(INPUT_POST, 'start_date', FILTER_SANITIZE_STRING);
                $end_date = filter_input(INPUT_POST, 'end_date', FILTER_SANITIZE_STRING);
                $event_tour_package = filter_input(INPUT_POST, 'event_tour_package', FILTER_SANITIZE_NUMBER_INT);

                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if ($wp_user_ID) {
                    $new_post = array(
                        'post_title'   => $event_name,
                        'post_type'      => 'eventpost',
                        'post_status'    => 'publish',
                        'post_author' => $wp_user_ID
                    );

                    $new_post_id = wp_insert_post($new_post, true);

                    if (is_wp_error($new_post_id)) {
                        http_response_code(401);
                        $response['msg'] = $new_post_id->get_error_message();
                        break;
                    }

                    $event_type = $event_type == 'public' ? 'public' : 'private';
                    update_post_meta($new_post_id, 'event_type', $event_type);

                    if (!empty($_FILES['event_location_image'])) {
                        $attachment_id = media_handle_upload('event_location_image', $new_post_id, array('post_author' => $wp_user_ID));
                        if (is_wp_error($attachment_id)) {
                            http_response_code(401);
                            $response['msg'] = 'File is invaild';
                            break;
                        } else {
                            update_post_meta($new_post_id, 'event_location_image_id', $attachment_id);
                        }
                    }

                    if (!empty($event_invited_people)) {
                        update_post_meta($new_post_id, 'event_invited_people', $event_invited_people);
                        foreach ($event_invited_people as $p) {
                            if (intval($p) > 0)
                                $wpdb->insert('notification', array('wp_user_id' => intval($p), 'content' => um_get_display_name($wp_user_ID) . " invited you to their event"));
                        }
                    }

                    if (!empty($event_location)) {
                        update_post_meta($new_post_id, 'event_location', $event_location);
                    }

                    if (!empty($start_date)) {
                        update_post_meta($new_post_id, 'start_date', strtotime($start_date));
                    }

                    if (!empty($end_date)) {
                        update_post_meta($new_post_id, 'end_date', strtotime($end_date));
                    }

                    if (!empty($event_tour_package)) {
                        update_post_meta($new_post_id, 'event_tour_package', $event_tour_package);
                        $hotelinfo = $wpdb->get_row("SELECT img,location FROM wp_hotel WHERE tour_id='{$event_tour_package}' LIMIT 1");
                        if (!empty($hotelinfo->img)) {
                            if (is_array(unserialize($hotelinfo->img))) {
                                $hotelimg = unserialize($hotelinfo->img)[0];
                            } else
                                $hotelimg = $hotelinfo->img;
                        }
                        if (!empty($hotelimg)) {
                            update_post_meta($new_post_id, 'tour_img', $hotelimg);
                        }
                    }

                    http_response_code(200);
                    $response['msg'] = 'Posted successfully';
                }
                // if user not exist
                else {
                    http_response_code(401);
                    $response['msg'] = 'User does not exist';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;

        case 'search_users':
            if (isTheseParametersAvailable(array('username', 'token', 'keyword'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $keyword = filter_input(INPUT_POST, 'keyword', FILTER_SANITIZE_STRING);

                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

                if (!empty($userid)) {
                    $userList = $wpdb->get_results("SELECT ID,userid,username,uname,photo,wp_usermeta.meta_value as hometown FROM `user`,wp_users
                        LEFT JOIN wp_usermeta ON wp_users.ID=wp_usermeta.user_id AND wp_usermeta.meta_key='hometown'
                        WHERE (`username` LIKE '%{$keyword}%' OR userid='{$keyword}') AND username=user_login", ARRAY_A);
                    foreach ($userList as &$user) {
                        $avatar = um_get_user_avatar_data($user['ID']);
                        $user = array(
                            'userid' => $user['userid'],
                            'name' => um_get_display_name($user['ID']),
                            'photo' => (empty($avatar['url']) ? '' : ($avatar['url'])),
                            'hometown' => $user['hometown'],
                            'profile' => home_url('user/' . $user['username'])
                        );
                    }
                    $response['userResults'] = $userList;

                    http_response_code(200);
                    $response['msg'] = 'success';
                } else {
                    http_response_code(401);
                    $response['msg'] = 'Incorrect token';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;

        case 'interestedpage':
            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

                if (!empty($userid)) {
                    $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$username}'");
                    $pageList = $wpdb->get_results("SELECT ID,post_title,post_author,post_type,post_content FROM `wp_posts` WHERE ID IN (
                        SELECT post_id FROM `blog_social_connect` WHERE sc_type=0 AND sc_user_id='{$wp_userid}'
                        ) OR ID IN (
                        SELECT post_id FROM `wp_postmeta`,social_connect WHERE meta_key='tour_id' AND sc_type=0 AND sc_tour_id=meta_value AND sc_user_id='{$wp_userid}'
                        ) AND post_status='publish'
                        ORDER BY ID DESC", ARRAY_A);
                    foreach ($pageList as &$page) {
                        $page['url'] = get_permalink($page['ID']);
                        if ($page['post_type'] == 'post') {
                            $tour_id = get_post_meta($page['ID'], 'tour_id', true);
                            $hotelinfo = $wpdb->get_row("SELECT img,location FROM wp_hotel WHERE tour_id='{$tour_id}' LIMIT 1");
                            if (!empty($hotelinfo->img)) {
                                if (is_array(unserialize($hotelinfo->img))) {
                                    $page['img'] = unserialize($hotelinfo->img)[0];
                                } else
                                    $page['img'] = $hotelinfo->img;
                            }
                        } else {
                            $page['img'] = get_template_directory_uri() . '/assets/img/rome.jpg';
                            if (preg_match('!<img.*?src="(.*?)"!', $page['post_content'], $post_image_match)) {
                                $page['img'] = $post_image_match[1];
                            }
                        }
                        unset($page['post_content']);
                    }
                    http_response_code(200);
                    $response['msg'] = 'success';
                    $response['pages'] = $pageList;
                } else {
                    http_response_code(401);
                    $response['msg'] = 'Incorrect token';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;

        case 'search_message_user':
            if (isTheseParametersAvailable(array('username', 'token', 'keyword'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $keyword = filter_input(INPUT_POST, 'keyword', FILTER_SANITIZE_STRING);

                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

                if (!empty($userid)) {
                    $msgList = $wpdb->get_results("SELECT chat.userid,user.username,chatid,message,chat_date,c_to,c_from FROM `chat`,`user`
                        WHERE `message` LIKE '%{$keyword}%' AND (`c_from` = '{$userid}' OR `c_to` = '{$userid}') AND chat.userid=user.userid AND type!='deleted' ORDER BY `chatid` DESC");
                    $response['msgResults'] = $msgList;

                    $userList = $wpdb->get_results("SELECT ID,userid,username,uname,photo,country,region FROM `user`,wp_users WHERE `username` LIKE '%{$keyword}%' AND username=user_login", ARRAY_A);
                    foreach ($userList as &$user) {
                        $avatar = um_get_user_avatar_data($user['ID']);
                        $user = array(
                            'userid' => $user['userid'],
                            'name' => (empty($user['uname']) ? $user['username'] : $user['uname']),
                            'photo' => (empty($avatar['url']) ? '' : ($avatar['url'])),
                            'country' => $user['country'],
                            'region' => $user['region']
                        );
                    }
                    $response['userResults'] = $userList;

                    http_response_code(200);
                    $response['msg'] = 'success';
                } else {
                    http_response_code(401);
                    $response['msg'] = 'Incorrect token';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;

        case 'followedusers':
            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

                if (!empty($userid)) {
                    $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$username}'");
                    $users = $wpdb->get_results("SELECT wp_users.ID,wp_users.user_login FROM `social_follow`,`wp_users`
                        WHERE social_follow.sf_agent_id=wp_users.ID AND `sf_user_id` = {$wp_userid}", ARRAY_A);
                    foreach ($users as &$user) {
                        $user['timeline_url'] = home_url('/my-time-line/?user=' . $user['ID']);
                        $user['photo'] = um_get_user_avatar_data($user['ID'])['url'];
                    }
                    $response['users'] = $users;
                    http_response_code(200);
                    $response['msg'] = 'success';
                } else {
                    http_response_code(401);
                    $response['msg'] = 'Incorrect token';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;

        case 'get_feed_post_data':
            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $user = $wpdb->get_row("SELECT wp_users.ID,user.access FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if (!empty($user->ID)) {
                    $response['feelings'] = array(
                        '😀happy',
                        '😥sad',
                        '😍loved',
                        '😡angry',
                    );

                    $friends = $wpdb->get_results("SELECT ID FROM `wp_users` WHERE ID IN(
                            SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}')
                            OR ID IN(
                                SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}')", ARRAY_A);
                    foreach ($friends as &$friend) {
                        $friend['name'] = um_get_display_name($friend['ID']);
                    }
                    $response['tag_friends'] = $friends;

                    if ($user->access == 1) {
                        $response['tour_packages'] = $wpdb->get_results("SELECT post_title, meta_value as tour_id FROM `wp_posts`,`wp_postmeta`
                            WHERE post_type='post' AND post_status='publish' AND `meta_key`='tour_id'
                            AND wp_posts.ID=wp_postmeta.post_id AND post_author='{$user->ID}'");
                    }

                    http_response_code(200);
                    $response['msg'] = 'success';
                } else {
                    http_response_code(401);
                    $response['msg'] = 'User does not exist';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;

        case 'get_event_post_data':
            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $user = $wpdb->get_row("SELECT wp_users.ID,user.access FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if (!empty($user->ID)) {

                    $friends = $wpdb->get_results("SELECT ID FROM `wp_users` WHERE ID IN(
                                SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}')
                                OR ID IN(
                                    SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}')", ARRAY_A);
                    foreach ($friends as &$friend) {
                        $friend['name'] = um_get_display_name($friend['ID']);
                    }
                    $response['invite_friends'] = $friends;

                    if ($user->access == 1) {
                        $response['tour_packages'] = $wpdb->get_results("SELECT post_title, meta_value as tour_id FROM `wp_posts`,`wp_postmeta`
                                WHERE post_type='post' AND post_status='publish' AND `meta_key`='tour_id'
                                AND wp_posts.ID=wp_postmeta.post_id AND post_author='{$user->ID}'");
                    }

                    http_response_code(200);
                    $response['msg'] = 'success';
                } else {
                    http_response_code(401);
                    $response['msg'] = 'User does not exist';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;

            // App Rating...
        case 'app_rating':
            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $rating = filter_input(INPUT_POST, 'app_rating', FILTER_SANITIZE_STRING);
                $app_comment = filter_input(INPUT_POST, 'app_comment', FILTER_SANITIZE_STRING);

                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

                if (!empty($userid)) {
                    $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$username}'");

                    $check_for_rating = $wpdb->get_var("SELECT COUNT(*) FROM `wp_app_rating` WHERE wp_user_ID='{$wp_userid}'");

                    if ($check_for_rating > 0) {


                        http_response_code(200);
                        $response['rated'] = 1;
                        $response['msg'] = 'You already Rated';
                    } else {

                        $sql = $wpdb->prepare("INSERT INTO `wp_app_rating` (`wp_user_ID`, `rating`,`comment`) values (%d, %d,%s)", $wp_userid, $rating, $app_comment);
                        $wpdb->query($sql);
                        http_response_code(200);
                        $response['rated'] = 0;
                        $response['msg'] = 'Rated Successfully';
                    }
                } else {
                    http_response_code(401);
                    $response['msg'] = 'Incorrect token';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;

            // API FOR Distance between users

        case 'user_distance':

            if (isTheseParametersAvailable(array('userid', 'miles', 'lat', 'lng'))) {

                $userid = filter_input(INPUT_POST, 'userid', FILTER_SANITIZE_STRING);
                $miles = filter_input(INPUT_POST, 'miles', FILTER_SANITIZE_STRING);
                $user_lat = filter_input(INPUT_POST, 'lat', FILTER_SANITIZE_STRING);;
                $user_lng = filter_input(INPUT_POST, 'lng', FILTER_SANITIZE_STRING);;

                $all_users = $wpdb->get_results("SELECT * FROM `user_location`");

                foreach ($all_users as $user) {


            $calculated_miles = API_User_distance($user_lat, $user_lng, $user->lat, $user->lng, "M");

                if ($calculated_miles < $miles && $calculated_miles != 0) {

                $near_friends_array[] = $user->ID;

                }
                } // end of foreach


                // now we have friend ids,lets get info from user table
                if ($near_friends_array) {
                    foreach ($near_friends_array as $friend_id) {

                        $value = $wpdb->get_row("SELECT * FROM `wp_users` WHERE ID='{$friend_id}'");
                        $Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user1 = '$user_id' AND user2 = '{$value->ID}'");

                        if (empty($Data)) {

                            $reverse_check = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '$user_id' AND user1 = '{$value->ID}'");

                            if (!empty($reverse_check) and isset($reverse_check)) {

                                $button_text = "Connected";
                            } else {
                                $button_text = 'Connect';
                            }

                            // end of if
                        } else {

                            if ($Data->status == "0") {

                                $button_text = "Pending";
                            } elseif ($Data->status == "1") {

                                $button_text = "Connected";
                            } elseif ($Data->status == "2") {

                                $button_text = "Blocked";
                            }
                        }




                        $list[] = array(

                            'userid' => $value->ID,
                            'username' => $value->user_nicename,
                            'hometown' => get_user_meta($value->ID, 'hometown', true),
                            'photo' => um_get_user_avatar_url($value->ID),
                            'status' => $button_text,
                            'coordiantes'=> to_get_user_lat_lng($value->ID)
                         //   'miles'=>$calculated_miles

                        );
                    }
                    http_response_code(200);
                    $response['success'] = 1;
                    $response['msg'] = 'suggestions';
                    $response['result'] = $list;
                } else {
                    http_response_code(401);
                    $response['success'] = 0;
                    $response['msg'] = 'No near friends';
                }
            } else {
                http_response_code(401);
                $response['success'] = 0;
                $response['msg'] = 'Required parameter missing';
            }

            break;



            // storing user location from app

        case 'user_location':
            if (isTheseParametersAvailable(array('username', 'token', 'lat', 'lng'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $lat = filter_input(INPUT_POST, 'lat', FILTER_SANITIZE_STRING);
                $lng = filter_input(INPUT_POST, 'lng', FILTER_SANITIZE_STRING);

                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

                if (!empty($userid)) {
                    $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$username}'");

                    $check_for_location = $wpdb->get_var("SELECT COUNT(*) FROM `user_location` WHERE ID='{$wp_userid}'");

                    if ($check_for_location > 0) {


                        $wpdb->update(
                            'user_location',
                            array(
                                'lat' => $lat,
                                'lng' => $lng
                            ),
                            array('ID' => $wp_userid),
                            array(
                                '%f',   // value1
                                '%f'    // value2
                            ),
                            array('%d')
                        );
                        http_response_code(200);
                        $response['msg'] = 'location Data Updated Successfully';
                    } else {

                        $sql = $wpdb->prepare("INSERT INTO `user_location` (`ID`, `lat`,`lng`) values (%d, %f,%f)", $wp_userid, $lat, $lng);
                        $wpdb->query($sql);
                        http_response_code(200);
                        $response['msg'] = 'location Data Addeded Successfully';
                    }
                } else {
                    http_response_code(401);
                    $response['msg'] = 'Incorrect token';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }

            break;


        case 'looking_for_travel_friend_list':
            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $location = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING);
                $location_filter = '';
                $item_per_page = 10;
                $page = filter_input(INPUT_POST, 'page', FILTER_VALIDATE_INT, array('options' => array('default' => 1, 'min_range' => 1)));
                $limit = ($page - 1) * $item_per_page . ',' . $item_per_page;

                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if (!empty($wp_user_ID)) {
                    if (!empty($location)) {
                        $location_filter = "AND post_id IN (
                                SELECT post_id FROM `wp_postmeta` WHERE `meta_key`='location' AND `meta_value` LIKE '%{$location}%'
                              )";
                    }
                    $list = $wpdb->get_results("SELECT wp_users.ID,wp_users.user_login as username,user.country,user.region,wp_posts.ID as post_id
                        FROM `wp_posts`,`wp_postmeta`,`wp_users`,`user`
                          WHERE wp_users.user_login=user.username AND wp_users.ID=wp_posts.post_author AND wp_posts.post_status='publish'
                              AND wp_posts.ID=wp_postmeta.post_id AND meta_key='lookingfriend' AND meta_value=1
                              {$location_filter}
                              AND post_id NOT IN(
                                  SELECT post_id FROM `match_reject` WHERE user_id={$wp_user_ID}
                                )
                              AND wp_users.ID NOT IN(
                                  SELECT user2 FROM `friends_requests` WHERE status=0 AND user1={$wp_user_ID}
                                )
                          ORDER BY wp_posts.ID DESC LIMIT {$limit}", ARRAY_A);
                    foreach ($list as &$row) {
                        $wp_userid = $row['ID'];
                        $check_for_location = $wpdb->get_row("SELECT * FROM `user_location` WHERE ID='{$wp_userid}'");
                        if ($check_for_location > 0) {
                            $lat = $check_for_location->lat;
                            $lng = $check_for_location->lng;
                        }

                        $row['name'] = um_get_display_name($row['ID']);
                        $row['avatar'] = um_get_user_avatar_url($row['ID'], 'original');
                        $row['plan_to_go'] = get_post_meta($row['post_id'], 'location', true);
                        $connect_status = $wpdb->get_var("SELECT status FROM `friends_requests` WHERE (user1='{$wp_user_ID}' AND user2='{$row['ID']}') OR (user1='{$row['ID']}' AND user2='{$wp_user_ID}')");
                        $row['connect_status'] = is_null($connect_status) ? 'unconnected' : ($connect_status == 0 ? 'sent' : 'connected');
                        $row['hometown'] = get_user_meta($row['ID'], 'hometown', true);
                        $row['current_city'] = get_user_meta($row['ID'], 'current_city', true);
                        $row['edu_level'] = get_user_meta($row['ID'], 'edu_level', true);
                        $row['coordiantes'] = empty($lat) ? '' : ($lat . "," . $lng);
                    }
                    if (!empty($list)) {
                        http_response_code(200);
                        $response['list'] = $list;
                    } else {
                        http_response_code(401);
                        $response['msg'] = 'There is not any user who is looking for travel friends.';
                    }
                } else {
                    http_response_code(401);
                    $response['msg'] = 'User does not exist';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;


        case 'looking_for_travel_friend_list_filter':
            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $distance = filter_input(INPUT_POST, 'distance', FILTER_VALIDATE_INT, array('options' => array('default' => 0, 'min_range' => 0)));
                $hometown = filter_input(INPUT_POST, 'hometown', FILTER_SANITIZE_STRING);
                $age_min = filter_input(INPUT_POST, 'age_min', FILTER_VALIDATE_INT, array('options' => array('default' => 17, 'min_range' => 17, 'max_range' => 65)));
                $age_max = filter_input(INPUT_POST, 'age_max', FILTER_VALIDATE_INT, array('options' => array('default' => 65, 'min_range' => 17, 'max_range' => 65)));
                $gender = filter_input(INPUT_POST, 'gender', FILTER_SANITIZE_STRING);
                $gender = (strtolower($gender) == 'female') ? 'Female' : 'Male';
                $marital_status = strtolower(filter_input(INPUT_POST, 'marital_status', FILTER_SANITIZE_STRING));
                $marital_status = empty($marital_status) ? 'single' : $marital_status;
                $star_min = filter_input(INPUT_POST, 'star_min', FILTER_VALIDATE_INT, array('options' => array('default' => 0, 'min_range' => 0, 'max_range' => 5)));

                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if (!empty($wp_user_ID)) {

                    $lat = 0.0;
                    $lng = 0.0;

                    $user_location = $wpdb->get_row("SELECT lat, lng FROM user_location WHERE ID = {$wp_user_ID}");
                    if (!empty($user_location)) {
                        $lat = $user_location->lat;
                        $lng = $user_location->lng;
                    }

                    $baseQuery = "SELECT wp_users.ID, user_login as username, (6371 * ACOS(COS(RADIANS({$lat})) * COS(RADIANS(lat)) * 
                                COS(RADIANS(lng) - RADIANS({$lng})) + SIN(RADIANS({$lat})) * SIN(RADIANS(lat)))) AS distance
                                FROM wp_users LEFT JOIN user_location ON user_location.ID = wp_users.ID
                                WHERE wp_users.ID!={$wp_user_ID}
                                AND wp_users.ID IN(
                                    SELECT wp_posts.post_author FROM `wp_posts`,`wp_postmeta`
                                    WHERE wp_posts.ID=wp_postmeta.post_id AND meta_key='lookingfriend' AND meta_value=1
                                )";
                    $filterQuery = "";

                    if (!empty($hometown))
                        $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'hometown' AND `meta_value` LIKE '%{$hometown}%')";

                    $filterQuery .= " AND wp_users.ID IN(
                                SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'date_of_birth'
                                AND TIMESTAMPDIFF(YEAR, meta_value, CURDATE()) BETWEEN {$age_min} AND {$age_max}
                            )";

                    $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'gender' AND `meta_value`='{$gender}')";
                    $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'marital_status' AND `meta_value`='{$marital_status}')";
                    $filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `social_reviews` GROUP BY user_id HAVING(AVG(rating)>={$star_min}))";

                    if ($distance > 0)
                        $filterQuery .= " HAVING(distance<{$distance})";

                    $query = $baseQuery . $filterQuery . " ORDER BY distance";

                    $list = $wpdb->get_results($query, ARRAY_A);
                    foreach ($list as &$row) {
                        $row['name'] = um_get_display_name($row['ID']);
                        $row['avatar'] = um_get_user_avatar_url($row['ID'], 'original');
                        $connect_status = $wpdb->get_var("SELECT status FROM `friends_requests` WHERE (user1='{$wp_user_ID}' AND user2='{$row['ID']}') OR (user1='{$row['ID']}' AND user2='{$wp_user_ID}')");
                        $row['connect_status'] = is_null($connect_status) ? 'unconnected' : ($connect_status == 0 ? 'sent' : 'connected');
                        $row['hometown'] = get_user_meta($row['ID'], 'hometown', true);
                        $row['current_city'] = get_user_meta($row['ID'], 'current_city', true);
                        $row['edu_level'] = get_user_meta($row['ID'], 'edu_level', true);
                    }
                    if (!empty($list)) {
                        http_response_code(200);
                        $response['list'] = $list;
                    } else {
                        http_response_code(401);
                        $response['msg'] = 'There is not any user who is looking for travel friends.';
                    }
                } else {
                    http_response_code(401);
                    $response['msg'] = 'User does not exist';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;

        case 'match_reject':
            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $post_id = filter_input(INPUT_POST, 'post_id', FILTER_VALIDATE_INT, array('options' => array('default' => 1, 'min_range' => 1)));

                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if (!empty($wp_user_ID)) {
                    $wpdb->insert(
                        'match_reject',
                        array('post_id' => $post_id, 'user_id' => $wp_user_ID)
                    );
                    http_response_code(200);
                    $response['msg'] = 'success';
                } else {
                    http_response_code(401);
                    $response['msg'] = 'User does not exist';
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required parameter missing';
            }
            break;


            /// api for friend lat/lng

        case 'get_friends_with_location':
            if (isTheseParametersAvailable(array('username', 'token'))) {
                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                // $kw = filter_input(INPUT_POST, 'kw', FILTER_SANITIZE_STRING);


                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$username}' AND token='{$token}'");

                if (!empty($userid)) {

                    $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$username}'");
                    $friends = $wpdb->get_results("SELECT ID,user_nicename FROM `wp_users` WHERE  ID IN(
                            SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_userid}')
                            OR ID IN(
                                SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_userid}')");

                    foreach ($friends as $value) {

                        $location = $wpdb->get_row("SELECT * FROM `user_location` WHERE ID='{$value->ID}'");

                        if ($location) {
                            $lat = $location->lat;
                            $Lng = $location->lng;
                        }

                        $list[] = array(

                            'userid' => $value->ID,
                            'username' => $value->user_nicename,
                            'hometown' => get_user_meta($value->ID, 'hometown', true),
                            'photo' => esc_url(get_avatar_url($value->ID)),
                            'coordiantes' => $lat . "," . $Lng

                        );
                    }
                    $response['msg'] = "retrived";
                    $response['list'] = $list;
                } else {

                    http_response_code(401);
                    $response['msg'] = 'User name or token wrong';
                }
            } else {

                http_response_code(401);
                $response['msg'] = 'Required Parameter missing';
            }

            break;

        case 'get_location_by_user_name':
            if (isTheseParametersAvailable(array('user_name'))) {

                $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);

                $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");

                if ($wp_userid) {

                    $location = $wpdb->get_row("SELECT * FROM `user_location` WHERE ID='{$wp_userid}'");

                    if ($location) {
                        $lat = $location->lat;
                        $Lng = $location->lng;
                    }
                    http_response_code(200);
                    $response['msg'] = "location Found";
                    $response['coordiantes'] = $lat . "," . $Lng;
                } else {
                    http_response_code(401);
                    $response['msg'] = 'No location coordiantes';
                }
            } else {

                http_response_code(401);
                $response['msg'] = 'Required Parameter missing';
            }

            break;

            // make plan

        case 'make_plan':
            if (isTheseParametersAvailable(array('user_name', 'token', 'plan_start_date', 'plan_end_date', 'days', 'location', 'lat', 'lng'))) {

                $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $plan_start_date = filter_input(INPUT_POST, 'plan_start_date', FILTER_SANITIZE_STRING);
                $plan_end_date = filter_input(INPUT_POST, 'plan_end_date', FILTER_SANITIZE_STRING);
                $days = filter_input(INPUT_POST, 'days', FILTER_SANITIZE_STRING);
                $location = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING);
                $lat = filter_input(INPUT_POST, 'lat', FILTER_SANITIZE_STRING);
                $lng = filter_input(INPUT_POST, 'lng', FILTER_SANITIZE_STRING);
                // $time = filter_input(INPUT_POST, 'time', FILTER_SANITIZE_STRING);

                //user verifaction
                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$user_name}' AND token='{$token}'");

                if (!empty($userid)) {

                    $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");

                    //insert into database here

                    $result =    $wpdb->insert(
                        'wp_user_plans',
                        array(
                            'user_id' => $wp_userid,
                            'user_name' => $user_name,
                            'start_date' => $plan_start_date,
                            'end_Date' => $plan_end_date,
                            'no_of_days' => $days,
                            'location' => $location,
                            'lat' => $lat,
                            'lng' => $lng
                        ),
                        array(
                            '%d',
                            '%s',
                            '%s',
                            '%s',
                            '%d',
                            '%s',
                            '%s',
                            '%s'
                        )
                    );

                    if ($result == 1) {
                        get_friends_with_login_user_id ($wp_userid,'is just created a plan','make_plan');
                        http_response_code(200);

                        $response['msg'] = "Successfully added";
                    } else {
                        http_response_code(200);
                        $response['msg'] = "Some error occured!";
                    }
                } else {

                    http_response_code(401);
                    $response['msg'] = 'Wrong Username and password/Token';
                }
            } else {

                http_response_code(401);
                $response['msg'] = 'Required Parameter missing';
            }
            break;

            // get plans for user

        case 'get_my_plans':

            if (isTheseParametersAvailable(array('user_name', 'token'))) {

                $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$user_name}' AND token='{$token}'");

                if (!empty($userid)) {

                    $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");

                    $list = $wpdb->get_results("SELECT * FROM wp_user_plans WHERE user_id = '{$wp_userid}'  ORDER BY id DESC");

                    if ($list) {

                        foreach ($list as $plan) {

                            $plans[] = array(

                                'start_date' => $plan->start_date,
                                'end_Date' => $plan->end_date,
                                'days' => $plan->no_of_days,
                                'location' => $plan->location,
                                'plan_id' => $plan->id,
                                'coordiantes' => $plan->lat . "," . $plan->lng
                            );
                        }

                        http_response_code(200);
                        $response['msg'] = 'succes';
                        $response['list'] = $plans;
                    } else {

                        http_response_code(200);
                        $response['msg'] = 'You have no plans';
                    }
                } else {

                    http_response_code(200);
                    $response['msg'] = 'username  not exist!';
                }
            } else {

                http_response_code(401);
                $response['msg'] = 'Required Parameter missing';
            }

            break;

        case 'get_friends_plan':

            if (isTheseParametersAvailable(array('user_name', 'token'))) {

                $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);

                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$user_name}' AND token='{$token}'");

                if (!empty($userid)) {

                    $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");

                    $plans = $wpdb->get_results("SELECT * FROM `wp_user_plans` WHERE  user_id IN(
                            SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_userid}')
                            OR user_id IN(
                                SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_userid}')");

                    if ($plans) {

                        // 1st user get friends
                        $mut1 = "
             
                SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_userid}'

                UNION

                SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_userid}'";

                        $rt1 = $wpdb->get_results($mut1);

                        foreach ($rt1 as $value) {

                            if ($value->user2 == $wp_userid)
                                continue;
                            $friends_list1[] = $value->user2;
                        }



                        foreach ($plans as $plan) {

                            // 2nd user get friends
                            $mut2 = "
             
                SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$plan->user_id}'

                UNION

                SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$plan->user_id}' ";
                            $rt2 = $wpdb->get_results($mut2);

                            foreach ($rt2 as $value) {


                                $friends_list2[] = $value->user2;
                            }
                            $match = array_intersect($friends_list1, $friends_list2);
                            $start_date_time = explode(" ", $plan->start_date);
                            if (is_null($start_date_time[1])) {

                                $start_date_time[1] = "00:00:00";
                            }


                            $end_date_time = explode(" ", $plan->end_date);

                            if (is_null($end_date_time[1])) {
                                $end_date_time[1] = "00:00:00";
                            }
                            $plans_list[] = array(
                                'start_date' => $start_date_time[0],
                                'end_Date' => $end_date_time[0],
                                'days' => $plan->no_of_days,
                                'location' => $plan->location,
                                'plan_id' => $plan->id,
                                'user_id' => $plan->user_id,
                                'friend_name' => $plan->user_name,
                                'avatar' => um_get_user_avatar_url($plan->user_id, 80),
                                'coordiantes' => $plan->lat . "," . $plan->lng,
                                'mut_friends_count' => count($match),
                                'mut_friends_ids' => $match,
                                'start_time' => $start_date_time[1],
                                'end_time' => $end_date_time[1]
                            );
                            unset($friends_list2);
                        }

                        http_response_code(200);
                        $response['msg'] = "successfully ";
                        $response['k'] = $plans_list;
                        // $response['login_user'] = $friends_list1;
                        //$response['other_user'] = $friends_list2;
                        //$response['mtch'] = $match;
                    } else {

                        http_response_code(200);
                        $response['msg'] = "No plans available ";
                    }
                } else {

                    http_response_code(200);
                    $response['msg'] = "No user exist";
                }
            } else {


                http_response_code(401);
                $response['msg'] = 'Required Parameter missing';
            }

            break;

        case 'get_friends_plan_by_calander':

            $today_date = date("d/m/Y H:i:s");

            if (isTheseParametersAvailable(array('user_name', 'token'))) {

                $user_name = filter_input(INPUT_POST, 'user_name', FILTER_SANITIZE_STRING);

                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

                $start_date = filter_input(INPUT_POST, 'start_date', FILTER_SANITIZE_STRING);

                $end_date = filter_input(INPUT_POST, 'end_date', FILTER_SANITIZE_STRING);

                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$user_name}' AND token='{$token}'");

                if (!empty($userid)) {

                    $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");

                    $plans = $wpdb->get_results("SELECT * FROM `wp_user_plans` WHERE   user_id IN(
                            SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_userid}')
                            OR id IN(
                                SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_userid}')");

                    if (isset($start_date)) {

                        $plans = $wpdb->get_results("SELECT * FROM `wp_user_plans` WHERE start_date >= '{$start_date}' OR end_date<=  '{$end_date}'  AND user_id IN(
                            SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_userid}')
                            OR id IN(
                                SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_userid}')");
                    }

                    if ($plans) {

                        foreach ($plans as $plan) {

                            if ($plan->start_date < $today_date) {

                                continue;
                            }


                            $start_date_time = explode(" ", $plan->start_date);
                            if (is_null($start_date_time[1])) {

                                $start_date_time[1] = "00:00:00";
                            }


                            $end_date_time = explode(" ", $plan->end_date);

                            if (is_null($end_date_time[1])) {
                                $end_date_time[1] = "00:00:00";
                            }

                            $dt1 = strtotime($plan->start_date);
                            $dt2 =   strtotime($today_date);

                            $days = ($dt1 - $dt2) / 60 / 60 / 24;

                            $plans_list[] = array(

                                'start_date' => $start_date_time[0],
                                'end_Date' => $end_date_time[0],
                                'days' => $plan->no_of_days,
                                'location' => $plan->location,
                                'plan_id' => $plan->id,
                                'friend_name' => $plan->user_name,
                                'starting_in_days' => $days . "days",
                                'coordiantes' => $plan->lat . "," . $plan->lng,
                                'start_time' => $start_date_time[1],
                                'end_time' => $end_date_time[1]
                            );
                        }

                        http_response_code(200);
                        $response['msg'] = "successfully ";
                        $response['list'] = $plans_list;
                        //  $response['s'] = $today_date;

                    } else {

                        http_response_code(200);
                        $response['msg'] = "No plans available ";
                    }
                } else {

                    http_response_code(200);
                    $response['msg'] = "No user exist";
                }
            } else {


                http_response_code(401);
                $response['msg'] = 'Required Parameter missing';
            }

            break;

            // tour like api required by nitish .22/01/2020
        case 'tour_like_api':
            if (isTheseParametersAvailable(array('username', 'token', 'tour_id', 'type'))) {

                $user_name = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $sc_tour_id = filter_input(INPUT_POST, 'tour_id', FILTER_SANITIZE_STRING);
                $sc_type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING);


                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$user_name}' AND token='{$token}'");

                if (!empty($userid)) {

                    $sc_user_id = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");
                    //CHECK either before like
                    $duplicateDataCheck = $wpdb->get_results("SELECT * FROM `social_connect` WHERE sc_tour_id = '$sc_tour_id' AND sc_user_id = '$sc_user_id'", ARRAY_A);
                    $sc_created = date('Y-m-d H:i:s');
                    if (!empty($duplicateDataCheck)) {
                        /// get db row id
                        $duplicateDataCheck = $duplicateDataCheck[0];
                        $sc_id = isset($duplicateDataCheck['sc_id']) ? $duplicateDataCheck['sc_id'] : '';
                        $r =  $wpdb->update(
                            'social_connect',
                            array(
                                'sc_tour_id' => $sc_tour_id,
                                'sc_type' => $sc_type,
                                'sc_user_id' => $sc_user_id,
                                'sc_created' => $sc_created,
                            ),
                            array('sc_id' => $sc_id),
                            array(
                                '%d',
                                '%s',
                                '%d',
                                '%s',
                            ),
                            array('%d')
                        );
                    } else {

                        $wpdb->insert(
                            'social_connect',
                            array(
                                'sc_tour_id' => $sc_tour_id,
                                'sc_type' => $sc_type,
                                'sc_user_id' => $sc_user_id,
                                'sc_created' => $sc_created,
                            ),
                            array(
                                '%d',
                                '%s',
                                '%d',
                                '%s',
                            )
                        );
                    }
                    http_response_code(200);
                    $response['msg'] = "Success";
                } else {
                    http_response_code(200);
                    $response['msg'] = "No user exist";
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required Parameter missing';
            }
            break;


            //show friends
        case 'show_friends':

            if (isTheseParametersAvailable(array('username', 'token'))) {

                $username = check_input($_REQUEST['username']);
                $token = check_input($_REQUEST['token']);
                /// $search_term = check_input($_REQUEST['search']);
                $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$username}' AND token='{$token}'");

                if (!empty($wp_user_ID) && intval($wp_user_ID) > 1) {
                    $friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE ID IN(
                        SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$wp_user_ID}')
                        OR ID IN(
                            SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$wp_user_ID}')");

                    foreach ($friends as $f) {

                        $check_for_location = $wpdb->get_row("SELECT * FROM `user_location` WHERE ID='{$f->ID}'");
                        if ($check_for_location > 0) {
                            $lat = $check_for_location->lat;
                            $lng = $check_for_location->lng;
                        } else {
                            $lat = "Not found";
                            $Lng = "Not found";
                        }

                        $list[] = array(
                            'user_id' => $f->ID,
                            'username' => $f->user_login,
                            'education' => get_user_meta($f->ID, 'edu_level', true),
                            'avator' =>   um_get_user_avatar_url($f->ID, 'original'),
                            'coordiantes' => $lat . "," . $lng,
                            'status' => get_user_meta($f->ID, 'location_tracker', true),
                            'visiblity' => get_user_meta($f->ID, 'location_visiable', true)

                        );
                    }

                    $response['msg'] = "Successfully";
                    $response['list'] = $list;
                } else {

                    http_response_code(200);
                    $response['msg'] = "No user exist";
                }
            } else {

                http_response_code(401);
                $response['msg'] = 'Required Parameter missing';
            }


            break;

        // Near By Plans
        case 'near_by_plans':

        if (isset($_REQUEST['lat']) && isset($_REQUEST['lng'])) {

               $lat = check_input($_REQUEST['lat']);
               $lng = check_input($_REQUEST['lng']);
               $miles =20;

             $all_plans = $wpdb->get_results("SELECT * FROM `wp_user_plans`");

                foreach ($all_plans as $plan) {


                $calculated_miles = API_User_distance($lat, $lng, $plan->lat, $plan->lng, "M");

                if ($calculated_miles < $miles && $calculated_miles != 0) {

               // $near_plans_array[] = $plan->id;

                    $start_date_time = explode(" ", $plan->start_date);
                    if (is_null($start_date_time[1])) {

                    $start_date_time[1] = "00:00:00";
                    }


                    $end_date_time = explode(" ", $plan->end_date);

                    if (is_null($end_date_time[1])) {
                    $end_date_time[1] = "00:00:00";
                    }

                $plans_list[] = array(

                                'start_date' => $start_date_time[0],
                                'end_Date' => $end_date_time[0],
                                'days' => $plan->no_of_days,
                                'location' => $plan->location,
                                'plan_id' => $plan->id,
                                'user_id' => $plan->user_id,
                                'friend_name' => $plan->user_name,
                                'coordiantes' => $plan->lat . "," . $plan->lng,
                             //   'mut_friends_count' => count($match),
                              //  'mut_friends_ids' => $match,
                                'start_time' => $start_date_time[1],
                                'end_time' => $end_date_time[1],
                                'miles'=>$calculated_miles
                            );
                }
                $response['msg'] = "successfully";
                 $response['list'] = $plans_list;
                }

        }//end of if
        else{

               http_response_code(401);
                $response['msg'] = 'Required Parameter missing';

        }



        break;

    
      //social media updated method

        case 'social_media_login':
            if (isset($_REQUEST['username']) && isset($_REQUEST['useremail'])) {

                $useremail = check_input($_REQUEST['useremail']);
                $username = str_replace(' ','', check_input($_REQUEST['username']));




                //check for user,is the user already there
                $user_count = mysqli_query($conn, "SELECT count(*) FROM `user` WHERE email='{$useremail}'");
                $num = mysqli_fetch_row($user_count)[0];

                // check either this user also have account in our wp users table

                $user_count_in_wp_users = mysqli_query($conn, "SELECT count(*) FROM `wp_users` WHERE user_email='{$useremail}'");
                $num_in_wp_users = mysqli_fetch_row($user_count_in_wp_users)[0];

                if ($num > 0 && $num_in_wp_users > 0) {

                    $response = user_login_for_social($username, $useremail);
                } elseif ($num_in_wp_users > 0 && $num < 1) {

                    $nameforusrmail = $username;
                    $fusername = $username;
                    $email = $useremail;
                    $fpassword = md5(time());
                    $access = 2;
                    $activated = 1;
                    $location = "";
                    $phone = "";
                    //$token ="";

                    // INSERT USER IN USER TABLE
                    mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated,photo,phone) values ('$nameforusrmail', '$fusername','$email', '$fpassword', '$access','$activated','$location','$phone')");


                    $response = user_login_for_social($username, $useremail);
                } elseif ($num > 0 && $num_in_wp_users < 1) {

                    $website = "http://Travpart.com/English";
                    $userdata = array(
                        'user_login' =>  $username,
                        'user_url'   =>  $website,
                        'user_email' => $useremail,
                        'user_pass'  =>  md5(time()) // When creating an user, `user_pass` is expected.
                    );

                    $user_id = wp_insert_user($userdata);
                    http_response_code(400);
                    //LOGIN NOW
                    $response = user_login_for_social($username, $useremail);
                }
                // else of 1st user count
                elseif ($num_in_wp_users < 1 && $num < 1) {

                    $nameforusrmail = $username;
                    $fusername = $username;
                    $email = $useremail;
                    $fpassword = md5(time());
                    $access = 2;
                    $activated = 1;
                    $location = "";
                    $phone = "";


                    // INSERT USER IN USER TABLE
                    mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated,photo,phone) values ('$nameforusrmail', '$fusername','$email', '$fpassword', '$access','$activated','$location','$phone')");


                    $website = "http://Travpart.com/English";
                    $userdata = array(
                        'user_login' =>  $username,
                        'user_url'   =>  $website,
                        'user_email' => $useremail,
                        'user_pass'  =>  md5(time()) // When creating an user, `user_pass` is expected.
                    );

                    $user_id = wp_insert_user($userdata);
                    http_response_code(200);
                    //LOGIN NOW
                    $response = user_login_for_social($username, $useremail);
                }
            } else {
                http_response_code(401);
                $response['msg'] = 'Required Parameter missing';
            }


            break;

            case 'location_tracker_status':



            if (isTheseParametersAvailable(array('username', 'token'))) {

                $user_name = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
                $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
                $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);
                $visiblity = filter_input(INPUT_POST, 'location_visiable', FILTER_SANITIZE_STRING);

                $userid = $wpdb->get_var("SELECT userid FROM `user` WHERE username='{$user_name}' AND token='{$token}'");

                if (!empty($userid)) {

                    $wp_userid = $wpdb->get_var("SELECT ID FROM `wp_users` WHERE user_login='{$user_name}'");
                if( $status=="on"  || $status=="off" ){


                    $updated = update_user_meta( $wp_userid, 'location_tracker', $status );

                    $msg = "Location Tracker is ".$status;
                    // http_response_code(200);
                      //$response['msg'] = 'Tracker Info Updated';
    
                }
                    else{
                    // http_response_code(401);
                    $msg = ' Tracker Value is Invalid /On and off only';

                    }

                if( $visiblity=='family' || $visiblity =='family_friend' || $visiblity=='friend' || $visiblity=='anyone'){

                    // who can look
                    $updated = update_user_meta( $wp_userid, 'location_visiable', $visiblity );

                    $msg = $msg. ".Visiblity Info Updated";

                }
                else{

                    $msg = $msg.'. visilbity values are invaild';

                }

                 http_response_code(200);
                $response['msg'] = $msg;
               
                }
                else{

                http_response_code(401);
                $response['msg'] = 'No User Found';

                }
            }
                else{

                 http_response_code(401);
                $response['msg'] = 'Required Parameter missing';

                }

            break;

        default:
            // $response['error'] = true;
            http_response_code(401);
            $response['msg'] = 'Invalid Operation Called';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Invalid API Call';
    // $response['message'] = 'Invalid API Call';
}

echo json_encode($response);

// FUNCTIONS


function user_login_for_social($username, $useremail)
{

    global $wpdb;
    global $conn;
    $row = $wpdb->get_row("SELECT wp_users.ID as wp_user_id, user.* from `wp_users`,`user` WHERE email ='{$useremail}' AND user.username=wp_users.user_login", ARRAY_A);

    $token = md5($username);
    $userid = (int) $row['userid'];
    $wpdb->update('user', array('token' => $token), array('userid' => $userid));
    mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP(), token='{$token}' WHERE userid={$row['userid']}");
    if ($row) {
        $user = array(
            'userid' => $row['userid'],
            'username' => $row['username'],
            'email' => $row['email'],
            'phone' => $row['phone'],
            'uname' => $row['uname'],
            'photo' => (empty($row['photo']) ? um_get_user_avatar_url($row['wp_user_id'], 80) : ("https://www.travpart.com/English/travchat/{$row['photo']}")),
            'country' => $row['country'],
            'region' => $row['region'],
            'timezone' => $row['timezone'],
            'access' => $row['access'],
            'token' => $token
        );
    } else {
        $user = "no user";
    }

    return $user;
}

function isTheseParametersAvailable($params)
{
    foreach ($params as $param) {
        if (!isset($_POST[$param])) {
            return false;
        }
    }

    return true;
}

function check_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
}


function sendmailtosuer($to, $subject, $message)
{
    $key = "Tt3At58P6ZoYJ0qhLvqdYyx21";
    $postdata = array(
        'to' => $to,
        'subject' => $subject,
        'message' => $message
    );
    $url = "https://www.tourfrombali.com/api.php?action=sendmail&key={$key}";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $data = curl_exec($ch);
    if (curl_errno($ch) || $data == FALSE) {
        curl_close($ch);
        return FALSE;
    } else {
        curl_close($ch);
        return TRUE;
    }
}

function search_kw_store($kw, $user = null, $token = null)
{
    global $wpdb;
    $get_search = $wpdb->get_row("SELECT id,count FROM wp_search_keywords WHERE kw LIKE '{$kw}' ");
    if (empty($get_search)) {
        $wpdb->insert(
            $wpdb->prefix . 'search_keywords',
            array(
                'kw' => $kw,
                'count' => 1
            ),
            array(
                '%s',
                '%d'
            )
        );
        $kw_id = $wpdb->insert_id;
    } else {
        $next_count = $get_search->count + 1;
        $wpdb->update(
            $wpdb->prefix . 'search_keywords',
            array(
                'count' => $next_count
            ),
            array('kw' => $kw),
            array(
                '%d'    // value2
            ),
            array('%s')
        );
        $kw_id = $get_search->id;
    }
    if (!empty($user) && !empty($token)) {
        $user_id = $wpdb->get_var("SELECT wp_users.ID FROM `user`,`wp_users` WHERE user.username='{$user}' AND user.token='{$token}' AND wp_users.user_login=user.username");
        if ($user_id > 0 && ($wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}user_trace
            WHERE user_id={$user_id} AND type='keyword' AND value='{$kw_id}'") == 0)) {
            $wpdb->insert(
                $wpdb->prefix . 'user_trace',
                array('user_id' => $user_id, 'type' => 'keyword', 'value' => $kw_id)
            );
        }
    }
}
