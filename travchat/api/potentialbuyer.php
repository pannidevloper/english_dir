<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isTheseParametersAvailable(array('username', 'token'))) {

    $token = check_input($_REQUEST['token']);
    $username = check_input($_REQUEST['username']);

    $res = mysqli_query($conn, "SELECT * FROM `user` WHERE username='{$username}' AND token='{$token}'");
    $user = mysqli_fetch_assoc($res);
    if (empty($user)) {
        http_response_code(401);
        $response['msg'] = 'user not found';
    } else {
        $userid = $user['userid'];
        $list = $wpdb->get_results("SELECT user.userid,user.username FROM `user`,`wp_users`
            WHERE user.username=wp_users.user_login AND user.access=2
                AND wp_users.ID IN (
                    SELECT wp_user_trace.user_id FROM `wp_users`,`user`,`wp_posts`,`wp_user_trace`
                        WHERE wp_users.user_login=user.username AND user.userid={$userid}
                    AND wp_posts.post_author=wp_users.ID AND wp_posts.post_type='post'
                    AND wp_user_trace.type='page' AND wp_user_trace.value=wp_posts.ID
                )");
        http_response_code(200);
        $response['list'] = $list;
        $response['msg'] = 'success';
    }
} else {
    http_response_code(401);
    $response['msg'] = 'Required parameter missing';
}
