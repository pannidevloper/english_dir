
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content text-center" style="text-align: center;">
    <span class="close">&times;</span>
    <img style="width: auto;height: 150px;" src="https://www.travpart.com/English/wp-content/themes/bali/images/2019-logo.png"/>
    <h2>World's class premium travel</h2>
  </div>

</div>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 50%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>
<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/English/wp-load.php");
include('conn.php');
session_start();

if ($_GET['action'] == "open_agent" && !empty($_GET['userid'])) {
    agent_login(intval($_GET['userid']), $conn);
}

function agent_login($userid, $conn) {
    $nsql = "SELECT username,password from user where userid_travcust='$userid'";
    $nquery = mysqli_query($conn, $nsql);
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    $nrow = mysqli_fetch_array($nquery);
    if (mysqli_num_rows($nquery) == 0) {
        echo 0;
    } else {
        echo 1;
        $fusername = $nrow['username'];
        $fpassword = $nrow['password'];
    }
}

function check_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = check_input($_POST['username']);
    $now = date('Y-m-d H:i:s', time());

    if (!preg_match("/^[a-zA-Z0-9_]*$/", $username)) {
        $_SESSION['msg'] = "Username should not contain space and special characters!";
        header('location: index.php');
    } else {

        $fusername = $username;

        $password = check_input($_POST["password"]);
        $fpassword = md5($password);
        $password2 = $_POST["password"];

        $query = mysqli_query($conn, "select * from `user` where username='$fusername' and password='$fpassword'");



        if (mysqli_num_rows($query) == 0) {
            $creds = array();
            $creds['user_login'] = $fusername;
            $creds['user_password'] = $_POST["password"];
            $creds['remember'] = true;
            //var_dump($creds);
            $user = wp_signon($creds);
            if (is_wp_error($user)) {
                //echo $user->get_error_message();
                $_SESSION['msg'] = "Login Failed, Invalid Inputs!";
                header('location: index.php');
            } else {
                mysqli_query($conn, "insert into `user` (uname, username, email, phone, password, access, activated) values ('$fusername', '$fusername', '{$user->user_email}', '', '$fpassword', '2', '1')");
                $query = mysqli_query($conn, "select * from `user` where username='$fusername' and password='$fpassword' and activated=1");
                $row = mysqli_fetch_array($query);
                $_SESSION['id'] = $row['userid'];
                $_SESSION['access'] = $row['access'];
                $_SESSION['user_name'] = $row['uname'];
                $_SESSION['conn'] = 2;
                setcookie('access', $row['access'], time() + (86400 * 30), "/");
                if (!empty($_SERVER['REMOTE_ADDR'])) {
                    $url = "http://www.geoplugin.net/json.gp?ip={$_SERVER['REMOTE_ADDR']}";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                    $data = curl_exec($ch);
                    if (curl_errno($ch) || empty($data)) {
                        curl_close($ch);
                    } else {
                        curl_close($ch);
                        $data = json_decode($data, TRUE);
                        if (!empty($data['geoplugin_countryName']) && !empty($data['geoplugin_regionName']) && !empty($data['geoplugin_timezone'])) {
                            mysqli_query($conn, "UPDATE `user` SET `country` = '{$data['geoplugin_countryName']}', `region` = '{$data['geoplugin_regionName']}', `timezone` = '{$data['geoplugin_timezone']}' WHERE `user`.`userid` = {$row['userid']}");
                        }
                    }
                }
                //mysqli_query($conn, "UPDATE `user` SET online_time=CURRENT_TIMESTAMP() WHERE userid={$row['userid']}");
                $query = "SELECT * FROM `sessions` WHERE `sessions`.`user_id` = {$row['userid']} AND `sessions`.`ip_address` = '{$_SERVER['REMOTE_ADDR']}'";
                $result = mysqli_query($conn, $query);
                if (mysqli_num_rows($result) === 0) {
                    $sessions_query = "INSERT INTO `sessions` (user_id, ip_address, last_activity) VALUES ( {$row['userid']}, '{$_SERVER["REMOTE_ADDR"]}', 'NOW()' )";
                } else {
                    $sessions_query = "UPDATE `sessions` SET last_activity = 'now()' WHERE `sessions`.`user_id` = {$row['userid']}";
                }
                $result = mysqli_query($conn, $sessions_query);

                /*echo "<script>
            window.alert('Login Success, Welcome User!');
            window.location.href = 'user/';
          </script>";*/
          ?>
          <script>
                  // Get the modal
          var modal = document.getElementById("myModal");

          // Get the button that opens the modal
          var btn = document.getElementById("myBtn");

          // Get the <span> element that closes the modal
          var span = document.getElementsByClassName("close")[0];

          // When the user clicks the button, open the modal 
          //btn.onclick = function() {
            modal.style.display = "block";
          //}

          // When the user clicks on <span> (x), close the modal
          span.onclick = function() {
            modal.style.display = "none";
          }

          // When the user clicks anywhere outside of the modal, close it
          window.onclick = function(event) {
            if (event.target == modal) {
              modal.style.display = "none";
            }
          }
                    //window.alert('Login Success, Welcome User!');
                    window.location.href = "user/";
                </script>
          <?php
          
            }

            /*
              //echo $fusername;
              //echo $password;
              //die();
              $check = wp_authenticate_username_password( NULL, $fusername , $password );
              if(is_wp_error( $check ))
              {
              echo 'Wrong';
              }
              else
              {

              $query2= mysqli_query($conn2,"select ID,user_login,user_pass from `wp_users` where user_login='$fusername'");

              }





              if (mysqli_num_rows($query2) == 0) {
              $_SESSION['msg'] = "Login Failed, Invalid Inputs!";
              header('location: index.php');
              }else{
              $row2 = mysqli_fetch_array($query2);
              $_SESSION['id'] = $row2['ID'];
              $_SESSION['access'] = 1;
              $_SESSION['user_name'] = $row2['user_login'];
              $_SESSION['conn'] = 2;
              setcookie('access', $row2['access'],time() + (86400 * 30), "/");

              $sessions_query = "
              SELECT * FROM `sessions` WHERE `sessions`.`user_id` = {$_SESSION['id']} AND `sessions`.`ip_address` = '{$_SERVER['REMOTE_ADDR']}'
              ";
              $result = mysqli_query( $conn, $sessions_query );
              if( mysqli_num_rows($result ) === 0 ) {
              $sessions_query = "
              INSERT INTO `sessions` (user_id, ip_address, last_activity) VALUES ( {$_SESSION['id']}, '{$_SERVER["REMOTE_ADDR"]}', {$now})
              ";

              } else {
              $sessions_query = "
              UPDATE `sessions` SET last_activity = now() WHERE `sessions`.`user_id` = {$_SESSION['id']}
              ";
              }





              ?>
              <script>
              window.alert('Login Success , Welcome User from travchat!');
              window.location.href = 'user/';
              </script>
              <?php


              } */
        } else {
            $row = mysqli_fetch_array($query);

            if ($row['activated'] != 1) {
                $_SESSION['msg'] = "Please check the email and activate your account.";
                header('location: index.php');
            } else if ($row['access'] == 4) {
                $_SESSION['msg'] = "Invalid username.";
                header('location: index.php');
            } else {

                //set Login status of wordpress
                $creds = array();
                $creds['user_login'] = $fusername;
                $creds['user_password'] = $_POST["password"];
                $creds['remember'] = true;
                $user = wp_signon($creds);

                $_SESSION['id'] = $row['userid'];
                $_SESSION['access'] = $row['access'];
                $_SESSION['user_name'] = $row['uname'];

                setcookie('access', $row['access'], time() + (86400 * 30), "/");

                if (!empty($_SERVER['REMOTE_ADDR'])) {
                    $url = "http://www.geoplugin.net/json.gp?ip={$_SERVER['REMOTE_ADDR']}";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                    $data = curl_exec($ch);
                    if (curl_errno($ch) || empty($data)) {
                        curl_close($ch);
                    } else {
                        curl_close($ch);
                        //var_dump(json_decode($data, TRUE));
                        $data = json_decode($data, TRUE);
                        if (!empty($data['geoplugin_countryName']) && !empty($data['geoplugin_regionName']) && !empty($data['geoplugin_timezone'])) {
                            mysqli_query($conn, "UPDATE `user` SET `country` = '{$data['geoplugin_countryName']}', `region` = '{$data['geoplugin_regionName']}', `timezone` = '{$data['geoplugin_timezone']}' WHERE `user`.`userid` = {$row['userid']}");
                        }
                    }
                }
                $query = "
                SELECT * FROM `sessions` WHERE `sessions`.`user_id` = {$_SESSION['id']} AND `sessions`.`ip_address` = '{$_SERVER['REMOTE_ADDR']}'
            ";

                $result = mysqli_query($conn, $query);


                if (mysqli_num_rows($result) === 0) {
                    $sessions_query = "
                    INSERT INTO `sessions` (user_id, ip_address, last_activity) VALUES ( {$_SESSION['id']}, '{$_SERVER["REMOTE_ADDR"]}', '{$now}' )
                ";
                } else {
                    $sessions_query = "
                    UPDATE `sessions` SET last_activity = '{$now}' WHERE `sessions`.`user_id` = {$_SESSION['id']}
                ";
                }

                $result = mysqli_query($conn, $sessions_query);

                $_SESSION['conn'] = 1;
                if(isset($_SESSION['user_to_connect']))
                {
                  $href = "user/addnewmember.php?user=".$_SESSION['user_to_connect'];
                unset($_SESSION['user_to_connect']);
                }
                else
                {
                  $href="user/";
                }
                ?>
                <script>
                  // Get the modal
          var modal = document.getElementById("myModal");

          // Get the button that opens the modal
          var btn = document.getElementById("myBtn");

          // Get the <span> element that closes the modal
          var span = document.getElementsByClassName("close")[0];

          // When the user clicks the button, open the modal 
          //btn.onclick = function() {
            modal.style.display = "block";
          //}

          // When the user clicks on <span> (x), close the modal
          span.onclick = function() {
            modal.style.display = "none";
          }

          // When the user clicks anywhere outside of the modal, close it
          window.onclick = function(event) {
            if (event.target == modal) {
              modal.style.display = "none";
            }
          }
                    //window.alert('Login Success, Welcome User!');
                    window.location.href = "<?php echo $href;  ?>";
                </script>
                <?php

            }
        }
    }
}
?>



