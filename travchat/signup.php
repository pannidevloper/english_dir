<?php
if(!empty($_GET['code']))
{
	setcookie('inviteby', htmlspecialchars($_GET['code']), time()+60*60*24*30, '/English/');
	setcookie('inviteby', htmlspecialchars($_GET['code']), time()+60*60*24*30, '/english/');
}
?>
<!DOCTYPE html>
<html>
    <head>
		<title>Travchat</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, height=device-height, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Invite Friends &amp; Earn Cash!" />
		<meta property="og:description" content="Send a $5 credit to your friends, and earn even more for yourself when they join" />
		<meta property="og:url" content="https://www.travpart.com/English/travchat/signup.php?code=lal" />
		<meta property="og:site_name" content="Travpart - be part of us" />
		<meta property="article:section" content="Uncategorized" />
		<meta property="og:image" content="https://www.travpart.com/English/wp-content/uploads/2018/10/user-profiletab-referal-head-en.jpg" />
		
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
        <style>
            #signup_form{
                width:550px;
                height:auto;
                position:relative;
                top:50px;
                margin: auto;
                padding: auto;
            }
        </style>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQ4N2X6');</script>
<!-- End Google Tag Manager -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-65718700-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-65718700-6');
</script>
<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/e34f1763-afe4-4f4e-9fc6-df81af96c04f.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>
    </head>
    <body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ4N2X6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <div class="container">
            <div id="signup_form" class="well">
                <h2><center><span class="glyphicon glyphicon-user"></span> Sign Up</center></h2>
                <hr>
                <form method="POST" action="register.php" enctype="multipart/form-data">
                    <input type="hidden" name="name" class="form-control" required>
                    <div style="height: 10px;"></div>
                    Firstname: <input type="text" name="firstname" class="form-control" required>
                    <div style="height: 10px;"></div>
                    Lastname: <input type="text" name="lastname" class="form-control" required>
                    <div style="height: 10px;"></div>
                    Username: <input type="text" name="username" class="form-control" required>
                    <div style="height: 10px;"></div>		
                    Password: <input type="password" name="password" class="form-control" required>
                    <div style="height: 10px;"></div>		
                    Enter Password Again: <input type="password" name="password2" class="form-control" required>
                    <div style="height: 10px;"></div>
                    Email: <input type="text" name="email" class="form-control" required>                    
                    <div style="height: 10px;"></div>
                    Signup As<select name="signup_as" class="form-control" required> 
                        <option value="2">Buyer</option>
                        <option value="1">Travel Advisor</option>
                    </select>
                    <div style="height: 10px;"></div>

                    <div style="height: 10px;"></div>
                    <div id="phone-input">
                        <label for="phone">Phone:</label>
                        <div class="row">
                            <div class="col-xs-5">
                                <?php include('./components/country-phone-numbers.php'); ?>
                            </div>
                            <div class="col-xs-7">
                                <input type="tel" name="phone" class="form-control" id="phone">
                            </div>
                        </div>
                    </div>

                    <div style="height: 10px;"></div>
                    Photo:<input id="user_img_file" type="file" style="width:311px;" class="form-control" name="image">

                    <div style="height: 10px;"></div>
                    <button type="submit" class="btn btn-primary">Sign Up</button> <a href="index.php"> Back to Login</a>
                </form>
                <div style="height: 15px;"></div>
                <div style="color: red; font-size: 15px;">
                    <center>
                        <?php
                        session_start();
                        if (isset($_SESSION['sign_msg'])) {
                            echo $_SESSION['sign_msg'];
                            unset($_SESSION['sign_msg']);
                        }
                        ?>
                    </center>
                </div>
            </div>
        </div>
    </body>
</html>