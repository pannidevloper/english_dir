<?php
get_header();
if(have_posts()) {
	while(have_posts()) {
		the_post(); ?>
        
        <h1><span><?php the_title(); ?></span></h1>
		<hr>
		<?php the_content();
		
	}
} else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>