<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, height=device-height, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />



<title><?php wp_title(''); echo ' | '; bloginfo('name');  ?></title>
 
  
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link   async="async"   rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/mobile.css"/>
<link href="<?php bloginfo('template_url'); ?>/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/bxslider.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/jquery-range/jquery.range.css" type="text/css">
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/notification.css" rel="stylesheet" type="text/css">
<link rel="icon" href="http://www.travpart.com/wp-content/themes/aberration-lite/images/travpart_icon_lnA_icon.ico" />
<link  async="async"  href="<?php bloginfo('template_url'); ?>/css/style_custom.css?v=<? time() ?>" rel="stylesheet" type="text/css"/>
<link href="/English/wp-content/themes/bali/css/utilities.css?v=<? time() ?>" rel="stylesheet" type="text/css">

 <style>
  	.mobileappbg{
		display: none;
	}
	@media(max-width:600px){
		.mobileappbg{
			display: block;
		}
	}
  </style>	
	
	<style>
		.list_location{
			width:100%;
			position: relative;
		}
		.custom_btn {
		    position: absolute;
		    top: 15px;
		    font-size: 15px !important;
		    right: 14px;
		    padding: 5px 15px !important;
		    font-weight: bold;
		    background: #ec7979;
		    color: #fff;
		    border-radius: 5px;
		}
		.search_select{
			background: none;
		}li#mega-menu-item-19934 {
		    display: none !important;
		}
		#login_pageid .elementor-invisible {
		    visibility: inherit;
		}
		.input-group.md-form.form-sm.form-1.pl-0{
			padding-top: 4px;
		}
	</style>
	
	<?php if(!is_user_logged_in()){ ?>
	<style>
		.main-menu{
			padding-top:5px;
		}
	</style>
	<?php } ?>
<style>
.performance_icon a:hover{
  text-shadow: 5px 5px 10px #383838;
  cursor: pointer;
}
.input-group-prepend .dropdown-toggle:after {
		display: inline-block;
		vertical-align: middle;
		font-weight: 900;
	}
.d_height .col-md-8{
	padding: 0px !important;
}
.search_select{
	position: relative;
	top: -3px;
}
span.notify {
	    position: relative;
	    background: red;
	    color: #fff;
	    font-size: 10px;
	    border-radius: 50%;
	    top: -10px;
	    left: -6px;
}
#mega-menu-wrap-main-menu{
	padding-top:3px !important;
}
.search_hea,.lighten-3{
	border: 1px solid #1bba9a;
	
}
.search_hea{
	width:200px;
	border-left: 0px !important;
}
.py-1{
	border:0px !important;
	font-size: 11px !important;
	letter-spacing: 1px !important;
	padding-left: 0px !important;
	font-family: 'Heebo', sans-serif !important;
}
.click_main_search{
	outline: 0px;
}
	.desktop_menu{
		font-size: 16px !important;
		margin-left: 13px !important;
	}
#h-t a{
	
	font-family: 'Heebo', sans-serif;			
}
.loginstyle {
	font-size: 16px;
}
.lighten-3 i{		
	color: #1bba9a !important;
}
.dropdown-toggle::after{
	color: #1bba9a !important;
}
.lighten-3{
	border-right: 0px;
	background: none;
}
picture.notify_img img {
    width: 25px;
}

@media screen and (max-width: 600px) {
	.for_logo_mobile{
		padding: 10px 0px !important;
	}
	.tour_info #AddAnotherTravelItemManually{
		margin:0 10px !important;
	}
	.total-tour-submit-wrapper{
		position: inherit !important;
	}
	.space_top_trav{
		margin-top: 60px;
	}
	.datetext {
	    color: #000 !important;
	    float: left;
	    font-size: 16px;
	    background: #fff;
	    width: 20%;
	    display: block !important;
	    border: 3px solid #368a8c !important;
	    border-right: 0px !important;
	    margin-top: 10px;
	    padding: 10px 0px;
	    text-align: center;
	}
	#hotel2 input, #hotel3 input {
	    margin-top: 0px !important;
	    width: 80% !important;
	    margin-right: 0px !important;
	    border-radius: 0px;
	    margin-bottom: 10px !important;
	}
	#coachMarkMenu{
		margin:0px !important;
		display: inline-block !important;
		width: 48% !important;
		top: 0px  !important;
		position: inherit !important;
	}
	button#coachMarkMenu {
	    margin-top: 60px !important;
	}
	.btn-collapse{
		margin: 0px !important;
		display: inline-block;
	}
	.btn-collapse i{
		margin-right: 6px;
	}
	.btn-collapse.expend i{
		margin-right: 0px;
	}
	.text-right {
	    display: contents;
	}
	#h-r{
		position: inherit !important;
		width: 100% !important;
		padding-top: 10px;
	}
	.custom_best_travel  .elementor-heading-title{font-size: 13px !important;}
	.custom_about_size_about  .elementor-heading-title{font-size: 13px !important;}
	.custom_paragrap_content .elementor-text-editor{
		line-height: 1.4em;
		font-size: 12px; 
	}
	
	.elementor-element-2c53917 {
	    display: none;
	}
	#h-t{
		display: none !important;
	}
}
.mobilemenucolor{
	padding: 0px;
}

 .home_mytime_msg {
    background: #368a8c;
}

.dropdown:hover .sdropdown-content {
   display:block !important;  
}
.sdropdown-content{
    position: absolute;
    z-index: 10000;
    display: none;
}


.col-mmd-4 {
    text-align: center;
    width: 32%;
    display: inline-block;
}
.for_text a{
	text-align: center !important;
	padding: 10px 0px !important;
}
.for_c{
	color: #fff !important;	
}
.col-mmmd-4{
				text-align: center;
				width: 32%;
			}
			.col-mmmd-4 a{
				color: #fff;
				font-family: 'Heebo', sans-serif;
				font-size: 12px;
				text-transform: uppercase;
			}
			.menu_mob_dis{
				display: flex;
			}
			@media(max-width: 600px){

				.col-mmmd-4 a:visited {
					color: #fff !important;
				}
			}

 .travcustmenutable td{width: 32.5%;display: inline-block;border: none;}
 .mobiletravcustli span{width: 100%;padding:10px;}
 #h-t{
	margin-top: 38px;
	margin-bottom: 5px;
	border-bottom: solid 7px #1A6D84;
	padding-bottom: 10px;
}


 .apac-btn{
 	float: right;
    margin-top: 15px;
 }

/* Tooltip Css*/
.tool-tip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.tool-tip .tooltiptext {
    visibility: hidden;
    width: 224px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    top: 117%;
    left: 9%;
    margin-left: -60px;
    font-size: 14px;
    font-weight: normal;
    transition: all 0.5s cubic-bezier(0.68, -0.55, 0.27, 1.55);
}

.tool-tip .tooltiptext::after {
  content: "";
  position: absolute;
  bottom: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: transparent transparent black transparent;
}

.tool-tip:hover .tooltiptext {
  visibility: visible;
}

/*End Tooltip css*/

 @media only screen and (max-width: 600px) {
 .loginstyle{text-align: center!important;background:#286d80;}
}

/* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 601px) and (max-width: 1024px) {
  
}

/* 
  ##Device = Tablets, Ipads (landscape)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 601px) and (max-width: 1024px) and (orientation: landscape) {
  
  
}

/* 
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 600px) {
/*#h-r{
	display: inline-block!important;
}  

.apac-btn{
	position: relative;
    bottom: 33px;
    right: 22px;
    margin-top: 0;
    float: inherit;
}*/
  
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
#h-r{
	display: inline-block!important;
}  

/*.apac-btn{
	position: relative;
    bottom: 33px;
    right: 22px;
    margin-top: 0;
    float: inherit;
} */ 
  
}


@media (min-width: 320px) and (max-width: 480px) {
	#carPickupDate,#carPickupTime{
		top: 109px !important;
	}
	#carReturnDate,#carReturnTime{
		    top: 161px !important;
	}
	
.wrap2{
	width: 100% !important;
	padding: 22px !important;
	min-height:418px !important;
}
.travcust-pg-box .wrap2{
	margin-left:0px !important;
}


}
 </style> 

  
<?php 
if(function_exists('bp_is_user_profile')) :
  if( bp_is_user_profile() || bp_is_profile_edit() ) :
?>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDG6aCrxwhxdbMCHZX8YrGYRfkMTrZfvks&libraries=places"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/gmaps.js" type="text/javascript"></script>
<?php endif;
endif; ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bxslider.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/jquery-range/jquery.range.js"></script>
<script src="<?php bloginfo('template_url'); ?>/layer/layer.js" type="text/javascript"></script>

<link href="<?php bloginfo('template_url'); ?>/css/colorbox.css" rel="stylesheet" type="text/css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
  
<script src="<?php bloginfo('template_url'); ?>/js/jquery.colorbox.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/js/currency.js" type="text/javascript"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-65718700-6"></script>

<script src="<?php bloginfo('template_url'); ?>/js/travcust_custom.js" type="text/javascript"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-65718700-6');
</script>
<script  type="text/javascript">
			jQuery(document).ready(function($){
				
				$(document).on('click','.carLocationFromItem',function(){
					$('.getLocationCarsTo').val($(this).text());	
				});
				
				$(document).on('click','#recommendplace .item .col-md-2',function(){
					$('#location_img').attr('src',$(this).find('img').attr('src'));
				});	
				
				$('.hide_not,.hide_pro').hide();
				$('.lighten-3').click(function(){
					$('.custom_d').toggle();
				})
				$('.custom_menu_btn_mobile').click(function(){
					$(this).find('i').toggleClass('fa-bars fa-times-circle');
					$('.home_mytime_msg').slideToggle();
				})
				$('.not_slide').click(function(){
					$(this).find('i').toggleClass('fa-angle-down fa-angle-up');
					$('.hide_not').slideToggle();
				});
				$('.pro_slide').click(function(){
					
					$(this).find('i').toggleClass('fa-angle-down fa-angle-up');
					$('.hide_pro').slideToggle();
				
				});
				
				$('.click_main_search').click(function(){
            		var color_c = $(this);
            		$('.search_hea').attr('action',color_c.data('url'));
            		$('.py-1').attr('name',color_c.data('name'));
            		$('.click_main_search').removeClass('for_b_left');
            		$('.py-1').attr('placeholder',color_c.data('text'));
            		color_c.addClass('for_b_left');
            	});
				
			});
		</script>
        
        <script>
      $(document).ready(function(){
           
        var width=$(document).width();  

		if(width<=600){
			$('#hotel2 .datetext').text('From');
			$('#hotel3 .datetext').text('To');
		}

        if(width>=992){
          width=992;
        }else if(width>=768 && width<=992){
        
          width=768;
        }else{
          width=300;
        }
        //$(".contact_us").colorbox({iframe:true, innerWidth:width, innerHeight:490});
      });
    </script>
    
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65718700-3', 'auto');
  ga('send', 'pageview');
</script>

<script type="text/javascript">

var pdfdoc = new jsPDF();
var specialElementHandlers = {
  '#ignoreContent': function (element, renderer) {
    return true;
  }
};

$(document).on('click', '#pdfweb', function () {
    console.log('hi');
    pdfdoc.fromHTML($('.marvelapp-1a').html(), 10, 10, {
      'width': 110,
      'elementHandlers': specialElementHandlers
    });
    pdfdoc.save('First.pdf');
});

$(function(){
	$('.notificationicon').click(function() {
		$('.notificationtooltips').toggle();
	});
});

</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQ4N2X6');</script>
<!-- End Google Tag Manager -->
<?php wp_head(); ?>

         
        <style>
        @media (max-width: 640px) {
        	 #mega-menu-wrap-main-menu{
			  	padding-top	:0px !important 
			  }
			  .English-logo img {
				    height: 40px !important;
			  }
        }
        </style>
        
		<?php if(is_user_logged_in()){ ?>
			<style>
					.input-group.md-form.form-sm.form-1.pl-0{
						left: 0px !important;
					}
					.English-logo img{
						height: 49px;
						margin-top: 6px;
					}
			</style>	 
		<?php } ?>
<?php if($_GET['device']=='app'){ ?>

<style>
	
	@media screen and (max-width: 600px) {
		.space_top_trav{
		    margin-top:0px !important;
		}
	}
</style>
<?php } ?>	
</head>
<body id="<?php if(is_home() || is_front_page()) { echo 'hp'; } ?>" <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ4N2X6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php $url = site_url(); ?>

<div id="header" style="background: white;<?php echo ($_GET['device']=='app')?'display:none;':''; ?>">
<div class="container-fluid" style="background:white;padding-top:10px;">
  <div class="mobile_header_balouch_show">
	  <div class="row">
	  	<div class="for_logo_mobile text-center">
	  		<?php if (is_user_logged_in()) { ?>
		  		<a href="https://www.travpart.com/English/timeline/">
					<img class="custom_image_mobile" src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png-logo.webp" alt="travcust">
	  	  		</a>
	  	  	<?php } else { ?>
	  	  		<a href="https://www.travpart.com/">
					<img class="custom_image_mobile" src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png-logo.webp" alt="travcust">
	  	  		</a>
	  	  	<?php } ?>
		</div> 
		<div class="for_menu_mobile text-center">
			<button class="btn custom_menu_btn_mobile">
				MENU
				<i class="fas fa-bars"></i>
			</button>
		</div>	
	  </div>
	  
	  
	  <div class="home_mytime_msg" style="display: none;">
		<div style="padding: 10px;">
		  	<div class="input-group md-form form-sm form-1 pl-0">
             <div class="input-group-prepend">
                <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="height: 36px;">
                	<i class="fas fa-search text-white" aria-hidden="true"></i>
                	<!--<span class="dropdown_title">People</span>-->
                </button>
            	<div class="dropdown-menu custom_d" style="width: 100%;">
			      <a class="dropdown-item click_main_search for_b_left" data-name="friend" data-url="<?php bloginfo('home'); ?>/people" data-text="Search for People" href="#">People</a>
			      <a class="dropdown-item click_main_search" data-name="search" data-url="<?php bloginfo('home'); ?>/toursearch" data-text="Search for Tour Packages" href="#">Tour Packages</a>
			    </div>
              </div>                         
              <form class="search_hea" action="<?php bloginfo('home'); ?>/people" method="get" style="width: 86% !important;background: #fff">
                  <input class="form-control my-0 py-1" type="text" placeholder="Search for People" aria-label="Search" name="friend">
              </form>
            </div>
        </div>
	  	<div class="rows menu_mob_dis">
	  		<div class="col-mmmd-4">
	  		  <a href="https://www.travpart.com/English/timeline/">
		  		<i class="fas fa-home" style="font-size: 30px;margin-left:9px"></i>
		  		<p>Feed</p>
		  	  </a>	
		  	</div>
		  	<div class="col-mmmd-4">
		  	<?php $current_user = wp_get_current_user(); ?>
		  	  <a  href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->user_login;?>">	
		  		<i class="fas fa-clock" style="font-size: 30px;"></i>
		  		
		  		<p>My Timeline</p>
		  	  </a>	
		  	</div>
		  	<div class="col-mmmd-4">
		  	  <a href="/English/travchat/">
		  	 	<i class="fas fa-envelope" style="font-size: 30px;margin-right:9px"></i>
		  		<p>Message</p>
		  	  </a>	
		  	</div>
	  	</div>
	  	<div class="rows menu_mob_dis">
	  		<div class="col-mmmd-4">
	  			<a href="https://www.travpart.com/English/people/">
		  			<i class="fas fa-user-plus" style="font-size: 30px;margin-left:9px"></i>
			  		<p>Find People</p>
			  	</a>
	  		</div>
	  		<div class="col-mmmd-4">
	  			<a href="https://www.travpart.com/English/connection-request-list/">
		  			<i class="fas fa-user-friends" style="font-size: 30px;"></i>
		  			<?php if(is_user_logged_in()) { ?>
                     <span class="badge" style="position:relative;background: red;color: #fff;font-size: 12px;top: -10px;padding-top: 5px;padding-bottom: 5px;padding-left: 2px;padding-right: 2px;">
                     <?php echo getFriendRequestCount(wp_get_current_user()->ID); ?>
                     </span>
					 <?php } ?>
			  		<p>Connection Request</p>
			  	</a>
	  		</div>
	  	</div>
	  	
	  	<div class="for_border">
		 	
		  	<?php
            $display_name = um_user('display_name');
            $current_user = wp_get_current_user();
            if (is_user_logged_in()) { ?>
		  	<div class="rows profile_s" style="padding: 5px;">
		  		<div class="col-mmd-2">
		  		  <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>" class="custpm_pr">
			  		<?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?>
			  	  </a>	
			  	</div>
			  	<div class="col-mmd-8">
			  	  <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>">
			  	  	<strong> 
                         <?php echo $display_name; ?>
			  	  	</strong>
			  	  </a>	
			  	</div>
			  	<div class="col-mmd-2">
			  	  <a class="pro_slide" style="color:#fff;font-weight: bold;font-size: 18px;margin-top: -10px;font-family: 'Heebo', sans-serif;padding: 5px 15px !important">
			  	  	V
			  	  </a>
			  	</div> 
		  	</div>
		  	<?php } ?>
		  	
		  <div class="hide_pro">
		  
		  	<div class="rows "  style="padding-top: 10px;">
		  		<div class="col-mmd-4">
		  		  <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>">
			  		<i class="fas fa-user-circle" style="font-size: 30px;"></i>
			  		<p>My Profile</p>
			  	  </a>	
			  	</div>
			  	<div class="col-mmd-4">
			  	  <a href="/English/my-connection/?user=<?php echo um_user('display_name'); ?>">	
			  		<i class="fas fa-undo-alt" style="font-size: 30px;"></i>
			  		<p>My Refferal</p>
			  	  </a>	
			  	</div>
			  	<div class="col-mmd-4">
			  	  <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=settlement">
			  	 	<i class="fas fa-calendar-check" style="font-size: 30px;"></i>
			  		<p>Settlement</p>
			  	  </a>	
			  	</div>
		  	</div>
	  	
	  	
	  	<div class="rows ">
	  		<div class="col-mmd-4">
	  		  <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=transaction">
		  		<i class="fas fa-dollar"  style="font-size: 30px;"></i>
		  		<p>Transaction</p>
		  	  </a>	
		  	</div>
		  	<div class="col-mmd-4">
		  	  <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=verification">	
		  		<i class="fas fa-calendar-check" style="font-size: 30px;"></i>
		  		<p>Verification</p>
		  	  </a>	
		  	</div>
		  	<div class="col-mmd-4">
		  	  <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=bookingCodeList">
		  	 	<i class="fas fa-book-open" style="font-size: 30px;"></i>
		  		<p>Booking</p>
		  	  </a>	
		  	</div>
	  	</div>
	   </div>
	  </div> 
	  <div class="rows " style="padding: 10px 0px 0px 0px">
	  		<div class="col-mmd-4">
	  		  <a href="/English/about-us/">
		  		<i class="fas fa-briefcase" style="font-size: 30px;"></i>
		  		<p>About</p>
		  	  </a>	
		  	</div>
		  	<div class="col-mmd-4">
		  	  <a href="https://www.travpart.com/Chinese">
		  		<i class="fas fa-language" style="font-size: 30px;"></i>
		  		<p>中文</p>
		  	  </a>	
		  	</div>
		  	<div class="col-mmd-4" style="position: absolute;">
		  	  <select class="header_curreny form-control search_select" style="margin: 5px 0px;border-radius: 0px;min-height: 35px;">
                 <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
                 <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
                 <option full_name="USD" syml="USD" value="USD"> USD </option>
                 <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
              </select>
		  	</div>
	  	</div>
	 
	   
	  <?php if (is_user_logged_in()) { ?>
	  	<a href="https://www.travpart.com/English/logout/?redirect_to=https://www.travpart.com/English/login/" class="btn btn-primary" style="font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;"><i class="fas fa-sign-out-alt"></i> Logout</a>
	  <?php }else{ ?>
	  	<a href="/English/login/" class="btn btn-primary" style="font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;"><i class="fas fa-sign-out-alt"></i> Login</a>
		<?php } ?>
	  </div>
	  
 </div>
 </div>
 </div>
  
  
  <div class="container-fluid ">    
      <div class="row">
      		
	<div id="h-t" class="col-md-12 mobilemenucolor" style="background:white;width:100%;background-size:100%; <?php echo ($_GET['device']=='app')?'display:none;':''; ?>">
		<div class="row mobile_header_balouch_hide">
					<div class="col-md-2 col-xs-12	">
						<?php if (is_user_logged_in()) { ?>
						<a href="https://www.travpart.com/English/timeline/">
							<img src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png-logo.webp" alt="Travcust" width="150" class="mobilenone English-logo">
						</a>
						<?php }else{ ?>
						<a href="https://www.travpart.com/">
							<img src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png-logo.webp" alt="Travcust" width="150" class="mobilenone English-logo">
						</a>
						<?php } ?>
					</div>
					<div class="col-md-4 col-xs-6search" style="padding-top: 1px;padding-left: 30px;">
						<div class="input-group md-form form-sm form-1 pl-0">
                         <div class="input-group-prepend">
                            <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="basic-text1" style="height: 36px;">
                            	<i class="fas fa-search text-white" aria-hidden="true"></i>
                            	<!--<span class="dropdown_title">People</span>-->
                            </button>
                        	<div class="dropdown-menu" style="width: 100%;">
						      <a style="font-size:12px !important;margin-left:0px; " class="dropdown-item click_main_search for_b_left" data-name="friend" data-url="<?php bloginfo('home'); ?>/people" data-text="Search for People" href="#">People</a>
						      <a style="font-size:12px !important;margin-left:0px; " class="dropdown-item click_main_search" data-name="search" data-url="<?php bloginfo('home'); ?>/toursearch" data-text="Search for Tour Packages" href="#">Tour Packages</a>
						    </div>
                          </div>
                          <form class="search_hea"  action="<?php bloginfo('home'); ?>/people" method="get" style="width: 90%;margin: 0px;">
                              <input class="form-control my-0 py-1" type="text" placeholder="Search for People" aria-label="Search" name="friend">
                          </form>
                        </div>
					</div>
					<div class="col-md-6 col-xs-6">
					
						<div class="mobilephoneonly ">
                    <a href="https://www.travpart.com/English/">
                    <img src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png" alt="travpart" width="150" style="margin-left:-10px"></a></div>
                    <div class="main-menu pull-right">
                        <?php
                        ob_start();
                        wp_nav_menu(array('theme_location' => 'main-menu'));
                        $main_menu = ob_get_contents();
                        ob_end_clean();
                        $unread_message_count=getUnreadMessageCount(wp_get_current_user()->user_login);
                        ?>
                        <?php ob_start(); ?>
                        <!--
                        <li class="mobilenone nomobileview mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/about-us/">ABOUT</a></li>
                        <li class="mobilenone nomobileview mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="<?php echo $chat_link  ?>">MESSAGE<?php
                           /* if($unread_message_count>0) {
                                if($unread_message_count>99)
                                    $unread_message_display='99+';
                                else
                                    $unread_message_display=$unread_message_count;
                                echo "<span>{$unread_message_display}</span>";
                            }*/
                            ?></a></li>  -->                              
                        <?php if(current_user_can('um_travel-advisor')) { ?>
                        <!--<li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                        <a class="mega-menu-link headerfont cqheader" href="https://www.travpart.com/English/customers-request/">CUSTOMER REQUEST</a></li>-->
                        <?php } ?>
                        <?php if(is_user_logged_in()) { ?>
                        <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-link profileicon">
                          <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->user_login;?>">	
                        	<?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?>
                          </a>	
                        </li>
                        <?php } ?>
                        <div class="dropdown">
                           
                            <div class="dropbtn"> <!--  Login start --> 

                                <?php
                                $display_name = um_user('display_name');
                                 if (is_user_logged_in()) { ?>

                                    <li class="mega-menu-item mega-menu-item-type-post_type  mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout" style="background:white;text-align: center!important;padding:0px">
                                        <a style="font-size: 16px;" class="mega-menu-link for_h loginstyle headerfont" href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->user_login;?>" >
                                        <?php echo $display_name; ?></a>
                                        <!--<a class="mega-menu-link loginstyle" href="<?php echo wp_logout_url(get_permalink()); ?>" class="buttype">LOGOUT <i class="fa fa-caret-down" style="display: inline;"></i></a>-->
                                    <?php } else { ?>
                                        <!--<a class="mega-menu-link loginstyle headerfont" href="/English/login/" class="buttype" style="background:white;text-align: center!important;padding:10px;display:block">LOGIN</a>-->
                                        <style>
                                            .dropdown-content{min-width: 300px !important;display:none!important;}
                                        </style>
                                    <?php } ?>
                                </li>   
                                <!--  Login end -->
                            </div>
                            <div class="sdropdown-content mobilenone" style="padding: 0px !important;min-width: 330px !important;">
                                 
                                <!--  User name start -->
                                <?php //if($_GET['debug']==2){ ?>
                                 <style>
                                	.custom_dd{
										display:none !important;
									}
                                </style>
                                <div class="home_mytime_msg for_text" style="padding-top: 10px;">
								
							  
							  	<div class="rows">
							  		<div class="col-mmd-4">
							  		  <a href="https://www.travpart.com/English/timeline/"style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
								  		<i class="fas fa-home" style="font-size: 30px;"></i>
								  		<p>Feed</p>
								  	  </a>	
								  	</div>
								  	<div class="col-mmd-4">
								  	<?php $current_user = wp_get_current_user(); ?>
								  	  <a  style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; " href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->user_login;?>">	
								  		<i class="fas fa-clock" style="font-size: 30px;"></i>
								  		
								  		<p>My Timeline</p>
								  	  </a>	
								  	</div>
								  	<div class="col-mmd-4">
								  	  <a href="/English/travchat/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
								 	  	 	<i class="fas fa-envelope" style="font-size: 30px;"></i>
								  		<p>Message</p>
								  	  </a>	
								  	</div>
							  	</div>
							  	
							  	
							  	<div class="rows">
							  		<div class="col-mmd-4">
							  		  <a href="https://www.travpart.com/English/events-list/"style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
								  		<i class="fas fa-calendar-alt" style="font-size: 30px;"></i>
								  		<p>Event</p>
								  	  </a>	
								  	</div> 
								  	<div class="col-mmd-4">
							  		  <a href="https://www.travpart.com/English/about-us"style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
								  		<i class="fas fa-user" style="font-size: 30px;"></i>
								  		<p>About us</p>
								  	  </a>
								  	</div>
								  	<?php if(current_user_can('um_travel-advisor')) { ?>
								  	<div class="col-mmd-4">
							  		  <a href="https://www.travpart.com/English/customers-request/"style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
								  		<i class="fas fa-headset" style="font-size: 30px;"></i>
								  		<p>Customer</p>
								  	  </a>	
								  	</div>
								  	<?php } ?>
								</div>
								<div class="rows">
									<?php if (current_user_can('um_travel-advisor')) { ?>
			                            <div class="col-mmd-4">
			                              <a href="https://www.travpart.com/English/travcust/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
			                                <i class="fas fa-edit" style="font-size: 30px;"></i>
			                                <p>Travcust</p>
			                              </a>
			                            </div>
			                            <div class="col-mmd-4 performance_icon">
			                              <a href="" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
			                                <img src="https://www.travpart.com/English/wp-content/uploads/2020/06/performance_icon.png" style="border: none;width: 50px;height: 36px">
			                                <p>Performance</p>
			                              </a>
			                            </div>
			                        <?php } ?>
								</div>

							  	<div class="for_border">
								 	
								  	<?php
		                            $display_name = um_user('display_name');
		                            $current_user = wp_get_current_user();
		                            if (is_user_logged_in()) { ?>
								  	<div class="rows profile_s" style="padding: 5px;">
								  		<div class="col-mmd-4">
								  		  <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>" style="border: 0px !important;" class="custpm_pr">
									  		<?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?>
									  	  </a>	 
									  	</div>
									  	<div class="col-mmd-4">
									  	  <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>" style="font-size: 12px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
									  	  	<strong> 
		                                         <?php echo $display_name; ?>
									  	  	</strong>
									  	  </a>	
									  	</div>
									  	<div class="col-mmd-4">
									  	  <a class="pro_slide" style="cursor: pointer;color:#fff;font-weight: bold;font-size: 18px;font-family: 'Heebo', sans-serif;padding: 5px 15px !important;border: 0px !important;color: #fff !important;">
									  	  	V
									  	  </a>
									  	</div> 
								  	</div> 
								  	<?php } ?>
								  	
								  	<div class="hide_pro">
								  	<div class="rows "  style="padding-top: 10px;">
								  		<div class="col-mmd-4">
								  		  <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>">
									  		<i class="fas fa-user-circle" style="font-size: 30px;"></i>
									  		<p>My Profile</p>
									  	  </a>	
									  	</div>
									  	<div class="col-mmd-4">
									  	  <a style="color: #fff !important;border: 0px !important;" href="/English/my-connection/?user=<?php echo um_user('display_name'); ?>">	
									  		<i class="fas fa-undo-alt" style="font-size: 30px;"></i>
									  		<p>My Refferal</p>
									  	  </a>	
									  	</div>
									  	<div class="col-mmd-4">
									  	  <a style="color: #fff !important;border: 0px !important;" href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=settlement">
									  	 	<i class="fas fa-calendar-check" style="font-size: 30px;"></i>
									  		<p>Settlement</p>
									  	  </a>	
									  	</div>
								  	</div>
							  	
							  	
							  	<div class="rows ">
							  		<div class="col-mmd-4">
							  		  <a style="color: #fff !important;border: 0px !important;" href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=transaction">
								  		<i class="fas fa-dollar"  style="font-size: 30px;">&#36;</i>
								  		<p>Transaction</p>
								  	  </a>	
								  	</div>
								  	<div class="col-mmd-4">
								  	  <a style="color: #fff !important;border: 0px !important;" href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=verification">	
								  		<i class="fas fa-calendar-check" style="font-size: 30px;"></i>
								  		<p>Verification</p>
								  	  </a>	
								  	</div>
								  	<div class="col-mmd-4">
								  	  <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/ticket/">	
								  		<i class="fas fa-info-circle" style="font-size: 30px;"></i><br />
								  		OPEN TICKETS
								  	  </a>	
								  	</div>
								  
							  	</div>
							  	
							  	<div class="rows ">
							  		<div class="col-mmd-4">
								  	  <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=bookingCodeList">
								  	 	<i class="fas fa-book-open" style="font-size: 30px"></i><br />
								  		BOOKING
								  	  </a>	
								  	</div>
								  
							  	</div>
							   </div>
							  </div>
							  
							  <div class="rows " style="padding: 10px 0px 0px 0px">
						  		<!--<div class="col-mmd-4">
						  		  <a style="color: #fff !important;border: 0px !important;" href="/English/about-us/">
							  		<i class="fas fa-briefcase" style="font-size: 30px;"></i>
							  		<p>About</p>
							  	  </a>	
							  	</div>-->
							  	<div class="col-mmd-4">
							  	  <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/Chinese">
							  		<i class="fas fa-language" style="font-size: 30px;"></i>
							  		<p>中文</p>
							  	  </a>	
							  	</div>
						  		<div class="col-mmd-4">
							  	  <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/contact/">
							  	 	<i class="fas fa-phone"  style="font-size: 30px;"></i> 
							  	 	<br >
							  	 	CONTACT
							  	  </a>	
							  	</div>
							  	<div class="col-mmd-4" style="position: absolute;">
							  	  <select class="header_curreny form-control search_select" style="margin: 5px 0px;border-radius: 0px;min-height: 35px;">
                                     <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
                                     <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
                                     <option full_name="USD" syml="USD" value="USD"> USD </option>
                                     <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                                  </select>
							  	</div>
						  	</div> 
							 
							  <div class="for_border">
							  	
							   
							   
							  <?php if (is_user_logged_in()) { ?>
							  	<a  href="https://www.travpart.com/English/logout/?redirect_to=https://www.travpart.com/English/login/" class="btn btn-primary" style="color: #fff !important;border: 0px !important;font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;text-align: left !important;padding-left: 10px !important;"><i class="fas fa-sign-out-alt"></i> Logout</a>
							  <?php }else{ ?>
							  	<a href="/English/login/" class="btn btn-primary" style="color: #fff !important;border: 0px !important;font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;text-align: left !important;padding-left: 10px !important;"><i class="fas fa-sign-out-alt"></i> Login</a>
								<?php } ?>
							  </div>
							  
							</div>
                               
                                <?php //} ?>
                              
                                <li class="mega-menu-item custom_dd mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                    <?php
                                    $display_name = um_user('display_name');
                                    $current_user = wp_get_current_user();
                                    if (is_user_logged_in()) {
                                        ?>
                                        <strong><p style="margin-left: 30px; color: black!important;line-height: 2.5em!important;margin-bottom:0px !important;" class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>" >
                                                <?php echo $display_name; ?></p></strong><br>
                                        <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=profile"><i class="fas fa-user-circle"></i> MY PROFILE
                                        </a><br>
                                        <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=referal"><i class="fas fa-undo-alt"></i> REFERRAL 
                                        </a><br>
                                        <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=settlement"><i class="fas fa-calendar-check"></i> SETTLEMENT
                                        </a><br>
                                        <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=transaction"><i class="fas fa-dollar-sign"></i> TRANSACTION
                                        </a><br>
                                        <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=verification" ><i class="fas fa-calendar-check"></i> VERIFICATION
                                        </a><br>
                                        <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=bookingCodeList"><i class="fas fa-book-open"></i> BOOKING
                                        </a><br>
                                        <a class="mega-menu-link" href="https://www.travpart.com/English/ticket/" ><i class="fas fa-info-circle"></i> OPEN TICKETS</a><br>
                                        <a class="mega-menu-link" href="https://www.travpart.com/English/contact/" ><i class="fas fa-phone"></i> CONTACT
                                        </a>
                                    <?php } else { ?>
                                    <?php } ?>
                                </li>
                                <!--  User name end -->  
                            </div>
                            <div class="mobileonly">
                            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                <table style="width:100%;z-index:99999" class="mobilemenutable">
                                    <tr style="background:#3b898a!important">
                                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/" style="margin-left:9px"><i class="fas fa-home" style="font-size: 30px;margin-left:9px"></i><div style="margin-left:9px">HOME
                                        </a></div></td>
                                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/about-us/" style="margin-left:22px"><i class="fas fa-briefcase" style="font-size: 30px;margin-left:9px"></i><div style="margin-left:9px">ABOUT 
                                        </a></div>
                             </td>
                                        <td><a class="mega-menu-link mega-menu-msglink" href="<?php echo $chat_link  ?>" style="margin-left:9px"><i class="fas fa-envelope" style="font-size: 30px;margin-right:9px"></i><div style="margin-right:9px">MESSAGE
                                        <?php
                                        if($unread_message_count>0) {
                                            echo "<span>{$unread_message_display}</span>";
                                        }
                                        ?>
                                        </a></div></td>
                                    </tr>
                                </table>
                                
                                <!--  User name start -->
                                 <?php
                                    $display_name = um_user('display_name');
                                    $current_user = wp_get_current_user();
                                    if (is_user_logged_in()) {
                                        ?>
                                        
                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                    <table style="width:90%" class="mobilemenutable">
                                    <tr>
                                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=profile"><i class="fas fa-user-circle" style="font-size: 30px;"></i><br>
                                        MY PROFILE
                                        </a><br></td>
                                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=referal"><i class="fas fa-undo-alt" style="font-size: 30px;"></i><br>MY REFERRAL
                                        </a></td>
                                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=settlement"><i class="fas fa-calendar-check" style="font-size: 30px;"></i><br> SETTLEMENT
                                        </a></td>
                                    </tr>
                                    <tr>                                                
                                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=transaction"><i class="fas fa-dollar-sign" style="font-size: 30px;"></i><br> TRANSACTION
                                        </a></td>
                                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=verification"><i class="fas fa-calendar-check" style="font-size: 30px;"></i><br> VERTIFICATION
                                        </a></td>
                                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=bookingCodeList"><i class="fas fa-book-open" style="font-size: 30px;"></i><br> BOOKING
                                        </a></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a class="mega-menu-link" href="https://www.travpart.com/English/ticket/" ><i class="fas fa-phone" style="font-size: 30px;"></i><br> CONTACT
                                        </a></td>
                                        <td>
                                            <a class="mega-menu-link" href="https://www.travpart.com/Chinese"><i class="fas fa-language" style="font-size: 30px;"></i><br>中文</a></li>
                                        </td>
                                        <td>
                                            <a class="mega-menu-link" href="http://www.travpart.com/tutorial/Partner_s_Guidance.pdf" target="_blank" class="mega-menu-link"><i class="fas fa-file-pdf" style="font-size: 30px;color:#e63c60"></i><br> TUTORIAL</li>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="color:white;text-align: left;font-size:15px">Change Currency:</td>
                                        <td>
                                            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                                <select class="header_curreny search_select" style="padding:0px;width:70px;height:40px;background-image:none;display:block;font-size:20px">

                                                    <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
                                                    <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
                                                    <option full_name="USD" syml="USD" value="USD"> USD </option>
                                                    <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                                                </select>
                                            </li>
                                        </td>
                                    </tr><?php if(current_user_can('um_travel-advisor')) { ?>
                                    <tr>
                                        <td colspan="3" style="width: 100%">                                                
                                        <p class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                        <a class="mega-menu-link headerfont cqheader" href="https://www.travpart.com/English/customers-request/">CUSTOMER REQUEST</a></p>
                                        </td> 
                                    </tr> <?php } ?>
                                    
                                    <tr>
                                        <td colspan="3">
                                        <a href="https://www.travpart.com/mobileapp/"><div style="border:1px solid white;background:#5bbc2e;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;padding-bottom:10px;color:white;text-align: left;font-size:18px"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/android.png" alt="Download our app" class="androidicon">
                                        Download our mobile app</div>
                                        </a></td> 
                                    </tr>
                                </table>
                                  <?php } else { ?>
                                    <?php } ?>
                                    </li>
                            </div>
                        </div>
                        <?php if (!is_user_logged_in()) { ?>
                         <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                         	<a class="desktop_menu" href="https://www.travpart.com/English/login">LOGIN</a>
                         </li> 
                         <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                            <a class="desktop_menu" href="https://www.travpart.com/English/register2/">REGISTER</a>
                        </li>
                        <li class="mega-menu-link">
                            <a href="https://www.travpart.com/English/contact/"><span style="margin-left: 10px;padding:5px;background: #37a000 !important;">Contact us</span></a>
                        </li>
                        <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                            <a class="desktop_menu" href="https://www.travpart.com/Chinese">
                            	<img src="/English/wp-content/themes/bali//images/flg-cn.png" alt="中文" class="mobilenotshow"> 中文
                            </a>
                        </li>
                         <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                     	  	
						  	<select class="header_curreny form-control search_select" style="background: none;margin:0px 0px 0px 8px;">

                                <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
                                <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
                                <option full_name="USD" syml="USD" value="USD"> USD </option>
                                <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                            </select>
						  	
                         </li>
                         
                         <?php } ?>
                         
                         <?php if(is_user_logged_in()) { ?>
                         <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                         	<a class="desktop_menu" href="https://www.travpart.com/English/timeline">Feed</a>
                         </li>
                         <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                         	<a class="desktop_menu" href="http://travpart.com/english/people">Find People</a>
                         </li>
                         <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                         	<a  href="https://www.travpart.com/English/connection-request-list" class="custom_ficon desktop_menu"  style="color: #22b14c !important;font-size: 20px !important;">
                             <?php $countreq = getFriendRequestCount(wp_get_current_user()->ID); ?>
                                <?php if ($countreq > 0) { ?>
                                    <i class="fas fa-user-friends" style="color: #1a2947;"></i>
                             
                                     <span class="badge" style="position:relative;background: red;color: #fff;font-size: 10px;border-radius: 50%;top: -10px;left: -6px;">
                                     <?php echo getFriendRequestCount(wp_get_current_user()->ID); ?>
                                     </span>
                                <?php } else{ ?>
                                    <i class="fas fa-user-friends" style="padding-left: 2px;padding-right: 2px"></i>
                             <?php  } ?>
                         	</a>
                         </li>
						<?php //} ?>
                         
						<?php //if(is_user_logged_in()) { ?>
                         <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                         	<a href="https://www.travpart.com/English/travchat/" class="custom_chaticon" style="color: #22b14c !important;font-size: 20px !important;">
                             <?php $countmsg = getUnreadMessageCount(wp_get_current_user()->ID); ?>
                                <?php if ($countmsg > 0) { ?>
                                    <i class="fas fa-comment-alt" style="color: #1a2947;"></i>
                             
                                     <span class="badge" style="position:relative;background: red;color: #fff;font-size: 10px;border-radius: 50%;top: -10px;left: -6px;">
                                     <?php echo getUnreadMessageCount(wp_get_current_user()->ID); ?>
                                     </span>
                                <?php } else{ ?>
                                    <i class="fas fa-comment-alt" style="padding-left: 2px;padding-right: 2px"></i>
                             <?php  } ?>
                         	</a>
                         </li> 
						<?php } ?>
                        <?php 
                        if (is_user_logged_in() && (getNotificationcount(wp_get_current_user()->ID)>0)) { ?>
                            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout notificationicon mobilenone" style="position: relative;">
                            <a href="#" class="custom_chaticon" style="color: #22b14c !important;font-size: 20px !important;">
                         		<i class="fas fa-bell" style="color: #1a2947;"></i>
                                <span class="notify" style="background: red !important;border-radius: 50%; padding: 3px 5px !important;">
                                	<?php echo getNotificationcount(wp_get_current_user()->ID); ?>
                                </span>
                            </a> 
                                <div class="notificationtooltips" style="display:none;">
                                    <div class="arrow-up"></div>
                                    <div id="nheading">
                                        <div class="nheading-left">
                                            <h6 class="nheading-title">Notifications<span>
                                            </span></h6>
                                        </div>
                                        <div class="nheading-right">
                                            <a class="notification-link" href="#">See all</a>
                                        </div>
                                    </div>
                                    <ul class="notification-list">
                                        
                                        <?php /*<li class="notification-item" v-for="user of users">
                                            <!-- <div class="img-left">
                                              <img class="user-photo" alt="User Photo" v-bind:src="user.picture.thumbnail" />
                                            </div> -->
                                            <div class="user-content">
                                                <p class="user-info">You got a new follower.</p>
                                                <p class="time">1 hour ago</p>
                                            </div>
                                        </li>*/?>
                                        <?php
                                        $notifications=getNotification(wp_get_current_user()->ID);
                                        foreach($notifications as $row) {
                                        ?>
                                        <li class="notification-item" v-for="user of users">
                                            <div class="user-content">
                                                <p class="user-info" style="color: black"><?php echo $row['content']; ?></p>
                                            </div>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </li>                                                                      

                        <?php } else { ?>
                            <?php if(is_user_logged_in()) { ?>
                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout notificationicon mobilenone" style="position: relative;">
                                    <a class="desktop_menu" href="#" style="color: #22b14c !important;font-size: 20px !important;">
	                                    <!--<img src="https://www.travpart.com/wp-content/uploads/2019/10/notification_black.png" alt="notification" width="25">-->
	                                    <i class="fas fa-bell" style="padding-left: 2px;padding-right: 2px"></i>
                                    </a>
                                </li>
                            <?php } ?>  
                        <?php } ?>  
                        <?php 
	                    global $user_login, $current_user;
	                    get_currentuserinfo();
	                    $user_info = get_userdata($current_user->ID);
	                    $roles = array (
	                        'administrator',
	                        'um_travel-advisor',
	                    );

	                if (is_user_logged_in() && array_intersect( $roles, $user_info->roles)) {

	                //echo '<li class="mega-menu-link changecolor mobiletravcustli"><a href="https://www.travpart.com/English/travcust/"><span style="padding:5px">Create a tour package</span></a></li>';

	                } else {

	                //echo '<li class="mega-menu-link changecolor mobiletravcustli"><a href="https://www.travpart.com/English/contact/"><span style="padding:5px">Contact us</span></a></li>';

	                }
	                ?>
                        <!--<li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                            <select class="header_curreny search_select" style="padding:0px;height:20px;background-image:none;">

                                <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
                                <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
                                <option full_name="USD" syml="USD" value="USD"> USD </option>
                                <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                            </select>
                        </li>-->
                        
                         
                        <?php 
                            global $user_login, $current_user;
                            get_currentuserinfo();
                            $user_info = get_userdata($current_user->ID);
                            $roles = array (
                                'administrator',
                                'um_travel-advisor',
                            );

                        if (is_user_logged_in() && array_intersect( $roles, $user_info->roles)) {

                        //echo '<li class="mega-menu-link changecolor mobiletravcustli"><a href="https://www.travpart.com/English/travcust/"><span style="padding:5px" class="mobiletravcust">Create a tour package</span></a></li>';

                        //} else {

                        //echo '<li class="mega-menu-link changecolor mobiletravcustli"><a href="https://www.travpart.com/English/contact/"><span style="padding:5px" class="mobiletravcust"></span></a></li>';

                        }
                        ?>

                        <?php
                        $append_menu_items = ob_get_contents();
                        ob_end_clean();

                        $main_menu = substr(trim($main_menu), 0, strlen($main_menu) - 11);
                        if($unread_message_count>0) {
                            if($unread_message_count>99)
                                $unread_message_count='99+';
                            $main_menu = str_replace('Message', "Message <span>{$unread_message_count}</span>", $main_menu);
                        }
                        echo $main_menu . $append_menu_items . '</ul></div></div>';
                        ?>                                
                    </div>
							
      		
            <!--div id="h-t-1" class="col-md-12" style="display: none;background:white;width:100%;background-size:100%;">
                <div class="main-menu pull-right" style="margin:7px;margin-top: 0; <?php echo ($_GET['device']=='app')?'display:none;':''; ?>">
                <?php
                ob_start();
                wp_nav_menu( array( 'theme_location' => 'main-menu' ) );
                $main_menu = ob_get_contents();
                ob_end_clean();
				$unread_message_count=getUnreadMessageCount(wp_get_current_user()->user_login);
                ?>
                <?php ob_start(); ?>
                  <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/about-us/">ABOUT</a></li>
                  <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/travchat">MESSAGE<?php
					if($unread_message_count>0) {
						if($unread_message_count>99)
							$unread_message_display='99+';
						else
							$unread_message_display=$unread_message_count;
						echo "<span>{$unread_message_display}</span>";
					}
					?></a></li>
				  <?php if(current_user_can('um_travel-advisor')) { ?>
                  <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont cqheader" href="https://www.travpart.com/English/customers-request/">CUSTOMER REQUEST</a></li>
				  <?php } ?>
				  <?php if(is_user_logged_in()) { ?>
				  <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-link profileicon"><?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?></li>
				  <?php } ?>
				<div class="dropdown">
          <div class="dropbtn"> 
             <li class="mobilelogin mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                <?php                
                if(is_user_logged_in()) { ?>
                <a class="mega-menu-link loginstyle headerfont" href="<?php echo wp_logout_url(get_permalink()); ?>" class="buttype">LOGOUT <i class="fa fa-caret-down" style="display: inline;"></i></a>
                <?php } else { ?>
                <a class="mega-menu-link loginstyle headerfont" href="/English/login/" class="buttype">LOGIN <i class="fa fa-caret-down" style="display: inline;"></i></a>
                <style>
                .dropdown-content{display:none!important;}
                </style>
                <?php } ?>
              </li>   
               </div>
          <div class="dropdown-content mobilenone">
            
           <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
            <?php 
            $display_name = um_user('display_name');
            if(is_user_logged_in()) { ?>
            <strong><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>"" >
            <?php echo $display_name;?></a></strong><br>
            <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=profile"" ><i class="fas fa-user-circle"></i> PROFILE
            </a><br>
            <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=referal"" ><i class="fas fa-undo-alt"></i> REFERRAL
            </a><br>
            <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=settlement"" ><i class="fas fa-calendar-check"></i> SETTLEMENT
            </a><br>
            <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=transaction"" ><i class="fas fa-dollar-sign"></i> TRANSACTION
            </a><br>
             <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=verification"" ><i class="fas fa-calendar-check"></i> VERIFICATION
            </a><br>
             <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=bookingCodeList"" ><i class="fas fa-book-open"></i> BOOKING
            </a><br>
            <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/contact/" ><i class="fas fa-phone"></i> CONTACT
            </a>           
            <?php } else { ?>
            <?php } ?>
          </li>
           
          </div>
          <div class="mobilemenuonly">
           <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
          <table style="width:90%" class="mobilemenutable travcustmenutable">
              <tr style="background:#3b898a!important;border:none">
                  <td style="background:#3f8888;"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/" style="margin-left:9px"><i class="fas fa-home" style="font-size: 30px;margin-left:9px"></i><div style="margin-left:9px">HOME
                  </a></div></td>
                  <td style="background:#3f8888;"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/about-us/" style="margin-left:22px"><i class="fas fa-briefcase" style="font-size: 30px;margin-left:9px"></i><div style="margin-left:9px">ABOUT
                  </a></div></td>
                  <td style="background:#3f8888;"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/travchat" style="margin-left:9px"><i class="fas fa-envelope" style="font-size: 30px;margin-right:9px"></i><div style="margin-right:9px">MESSAGE
				  <?php
				  if($unread_message_count>0) {
					echo "<span>{$unread_message_display}</span>";
				  }
				  ?>
                  </a></div></td>
              </tr>
          </table>
          </a>

          
           <?php
                                            $display_name = um_user('display_name');
                                            if (is_user_logged_in()) {
                                                ?>
                                        <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                            <table style="width:90%" class="mobilemenutable travcustmenutable">
                                            <tr>
                                                <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=profile"" ><i class="fas fa-user-circle" style="font-size: 30px;"></i><br>
                                                PROFILE
                                                </a><br></td>
                                                <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=referal"" ><i class="fas fa-undo-alt" style="font-size: 30px;"></i><br> REFERRAL
                                                </a></td>
                                                <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=settlement"" ><i class="fas fa-calendar-check" style="font-size: 30px;"></i><br> SETTLEMENT
                                                </a></td>
                                            </tr>
                                            <tr>                                                
                                                <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=transaction"" ><i class="fas fa-dollar-sign" style="font-size: 30px;"></i><br> TRANSACTION
                                                </a></td>
                                                <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=verification"" ><i class="fas fa-calendar-check" style="font-size: 30px;"></i><br> VERTIFICATION
                                                </a></td>
                                                <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=bookingCodeList"" ><i class="fas fa-book-open" style="font-size: 30px;"></i><br> BOOKING
                                                </a></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="mega-menu-link" href="https://www.travpart.com/English/ticket/" ><i class="fas fa-phone" style="font-size: 30px;"></i><br> CONTACT
                                                </a></td>
                                                <td>
                                                    <a class="mega-menu-link" href="https://www.travpart.com/Chinese"><i class="fas fa-language" style="font-size: 30px;"></i><br>中文</a></li>
                                                </td>
                                                <td>
                                                    <a class="mega-menu-link" href="http://www.travpart.com/tutorial/Partner_s_Guidance.pdf" target="_blank" class="mega-menu-link"><i class="fas fa-file-pdf" style="font-size: 30px;color:#e63c60"></i><br> TUTORIAL</li>
                                                </td>
                                            </tr>
                                            <tr>
                                              <td></td>
                                              <td></td>
                                              <td><li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                              <select class="header_curreny search_select" style="padding:0px;height:20px;background-image:none;display:block">
                                              <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
                                              <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
                                              <option full_name="USD" syml="USD" value="USD"> USD </option>
                                              <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                                            </select>
                                            </li></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" style="width: 100%">
                                                <?php if(current_user_can('um_travel-advisor')) { ?>
                                                <p class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont cqheader" href="https://www.travpart.com/English/customers-request/">CUSTOMER REQUEST</a></p>
                                        <?php } ?></td> 
                                            </tr>
                                            <tr>
                                                <td colspan="3" style="width: 100%">
                                                <a href="https://www.travpart.com/English/wp-content/themes/bali/App/travpart_english.apk"><div style="border:1px solid white;background:#5bbc2e;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;padding:10px"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/android.png" alt="Download our app" class="androidicon">
                                                Download our mobile app ★★★★★</div>
                                                </a></td> 
                                            </tr>
                                        </table>
                                          <?php } else { ?>
                                            <?php } ?>
                                            </li>
          

          </div>
        </div>
                <?php 
                if(is_user_logged_in()) { ?>
                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout notificationicon mobilenone" style="position: relative;"><a class="mega-menu-link" href="#">
                <img src="https://www.travpart.com/wp-content/uploads/2019/10/notification_black.png" alt="notification" width="25">
                <span style="background-color: red !important"><?php
                                                        echo getNotificationcount(wp_get_current_user()->ID);

                                                        ?>

                                                    </span></a>

                <div class="notificationtooltips" style="display:none;">
                <div class="arrow-up"></div>
                <div id="nheading">
                    <div class="nheading-left">
                      <h6 class="nheading-title">Notifications</h6>
                    </div>
                    <div class="nheading-right">
                      <a class="notification-link" href="#">See all</a>
                    </div>
                  </div>
                  <ul class="notification-list">
                    <?php
					$notifications=getNotification(wp_get_current_user()->ID);
					foreach($notifications as $row) {
					?>
					<li class="notification-item" v-for="user of users">
						<div class="user-content">
							<p class="user-info"><?php echo $row['content']; ?></p>
						</div>
					</li>
					<?php } ?>
                  </ul>
                </div>
                </li>                                                                      

                 <?php } else { ?>
                    <?php } ?>  
                <li  class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link" href="https://www.travpart.com/Chinese"><img src="<?php bloginfo('template_url'); ?>/images/flg-cn.png" alt="中文" class="mobilenotshow"> 中文</a></li>
                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">

                    <select class="header_curreny search_select" style="padding:0px;height:20px;background-image:none;">

						<option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
						<option full_name="RMB" syml="元"  value="CNY"> RMB </option>
						<option full_name="USD" syml="USD" value="USD"> USD </option>
						<option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
					</select>
                </li>          
                  
                  <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="http://www.travpart.com/tutorial/Partner_s_Guidance.pdf" target="_blank">HOW IT WORKS</a></li>
                <?php 
                    global $user_login, $current_user;
                    get_currentuserinfo();
                    $user_info = get_userdata($current_user->ID);
                    $roles = array (
                        'administrator',
                        'um_travel-advisor',
                    );

                if (is_user_logged_in() && array_intersect( $roles, $user_info->roles)) {

                echo '<li class="mega-menu-link changecolor mobiletravcustli"><a href="https://www.travpart.com/English/travcust/"><span style="padding:5px">Create a tour package</span></a></li>';

                } else {

                //echo '<li class="mega-menu-link changecolor mobiletravcustli"><a href="https://www.travpart.com/English/contact/"><span style="padding:5px">Contact us</span></a></li>';

                }
                ?>
                <?php
                $append_menu_items = ob_get_contents();
                ob_end_clean();

                $main_menu = substr(trim($main_menu),0,strlen($main_menu)-11);
				if($unread_message_count>0) {
					if($unread_message_count>99)
						$unread_message_count='99+';
					$main_menu = str_replace('Message', "Message <span>{$unread_message_count}</span>", $main_menu);
				}
                echo $main_menu .$append_menu_items.'</ul></div></div>';
                ?>
            </div>-->
           
        </div>
    </div>
        </div>
        </div>
      <div class="container-fluid space_top_trav" style="background:white;padding-top:7px">
        <div class="container" style="background:white;height:80px">
      <div class="row"> 
          <div class="col-md-3">
              <div id="logo" style="margin-top:0px">
              	 <a href="#"  style="max-width: 100%;padding: 10px;margin-top: 0px;"><img src="https://www.travpart.com/English/wp-content/uploads/2018/11/logo2.png" alt="" style="height:100%"></a>
              </div>

              <div class="mobileonly">
    <div style="margin-top:90px;margin-left:-24px">
      
        <a class="btn btn-primary firstbackbtn" style="width:145px;display:block;border:3px solid #226d82!important; background:#4ec3b7!important;cursor:pointer;margin-bottom:10px"  data-toggle="tooltip" title="delete and/or add more travel item"><span>Modify Package</span> </a>
      
        <div class="tooltip" style="z-index: 1">
        <a href="<?php echo empty($tour_id)?'#':(site_url().'/tour-details/?bookingcode=BALI'.$tour_id); ?>" <?php echo empty($tour_id)?'data-toggle="tooltip" ':'data-toggle="tooltip"'; ?> class="btn btn-primary firstbackbtn">
        <span><?php echo empty($tour_id)?'':("(Booking Code BALI{$tour_id})"); ?> <img src="https://www.travpart.com/English/wp-content/uploads/2018/12/search.png" alt="Check my Journey" width="20"></span><!--<span class="tooltiptext">Check my journey</span>-->
        </a>
        </div>
         
        <div class="tooltip" style="z-index: 1;margin-left:20px">
        <a href="<?php echo get_template_directory_uri().'/newtour.php'; ?>" class="btn btn-primary firstbackbtn" style="text-decoration: none">
        <img src="https://www.travpart.com/English/wp-content/uploads/2018/11/add-list.png" alt="Create a new tour package" width="20">  <!--<span class="tooltiptext">Create a new tour package</span>--> </a>
        </div>
  </div>
</div>
        </div>

            <div id="h-r" class="col-md-9 col-xs-12">
            	<div class="apac-btn">
            		<!-- <a class="apac_btn" href="" data-toggle="tooltip" title="" target="_blank" data-placement="left">APAC</a> -->

<a href="https://www.dropbox.com/sh/ahxj94uxvqqwzs4/AACZiZ4dh63AcJ4ULlF4KPxza?dl=0." target="_blank" class="tool-tip apac_btn">APAC
  <span class="tooltiptext">Just a reference link for tours to APAC destination. It's fine if you create tours other than APAC destination</span>
</a>


           	 	</div>
      </div>
        </div>
    </div>
</div>
  
<?php 
  
  /*if( is_home() || is_front_page() || is_page('14047')) {
        echo '<div id="search_price_box" class="container">';
            echo '<div id="search_price_box_form">';
                echo search_price_box();
               // echo search_nontour_box(); 
                
            echo '</div>';
        echo '</div>';
  }
  if(is_home() || is_front_page())
  {
echo '<div id="search_price_box" class="container">';
            echo '<div id="search_price_box_form2">';
                //echo search_price_box();
               echo search_nontour_box(); 
                
            echo '</div>';
        echo '</div>'; 
}*/
?>
  
<?php
/*
if(is_home() || is_front_page()) {
   putRevSlider( 'homepage' );
}*/

?>
<div id="content">
  <div class="container">
      <div class="row">
          <div class="col-md-12">
            
            <?php if(!is_home() && !is_front_page()) { ?>
            <div class="row">
                <div class="col-md-8">
                    <?php getBcrumbs(); ?>
              </div>
              
            </div>
            <?php } ?>
            
  <style>
    button.searchButton2 {     height: 35px !important; margin: 98px 0 0px 20px !important;}
  </style>
