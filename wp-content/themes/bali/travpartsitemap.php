<?php
	/*
	Template Name: travpartsitemap
	*/
	get_header();

 
 
 $string="17-Mile Drive Tours
Acadia National Park Tours
Air Force Memorial Tours
Alcatraz Island Tours
Alte Oper Tours
American Falls Tours
Amish Village Tours
Amsterdam Tours
Amsterdam Canals Tours
Amsterdam Red Light District Tours
Anaheim Tours
Antelope Canyon Tours
Aquarium of the Pacific Tours
Arc de Triomphe Tours
Arch of Constantine Tours
Arches National Park Tours
Arezzo Tours
Arlington National Cemetery Tours
Art Deco District Tours
Athabasca Falls Tours
Athabasca Glacier Tours
Atomium Tours
Ausable Chasm Tours
Avignon Tours
Balboa Park Tours
Banff Tours
Banff Gondola Tours
Banff Hot Springs Tours
Banff National Park Tours
Barcelona Tours
Barcelona Gothic Quarter Tours
Barcelona Olympic Park Tours
Barrio Santa Cruz Tours
Barstow Tours
Basilica Cathedral of Our Lady of the Pillar Tours
Bath Tours
Bath Abbey Tours
Bay Area Tours
Bay of Fundy Tours
Beacon Hill Park Tours
Bear Country USA Tours
Beethoven-Haus Tours
Belem Tower Tours
Beverly Hills Tours
Black Forest Tours
Boldt Castle Tours
Bonn Tours
Boston Tours
Boston Common Tours
Boston Harbor Tours
Boston's John Hancock Tower Tours
Bow Falls Tours
Bow Lake Tours
Bow Valley Tours
Bratislava Tours
Bridal Veil Falls Tours
Bridalveil Fall Tours
Bridge of Sighs Tours
Bright Angel Lodge Tours
British Columbia Parliament Buildings Tours
Brooklyn Bridge Tours
Bruce Peninsula Tours
Brussels Tours
Bryce Canyon City Tours
Bryce Canyon National Park Tours
Buckingham Palace Tours
Buda Castle Tours
Budapest Tours
Butchart Gardens Tours
CN Tower Tours
Calgary Tours
Cambridge Tours
Cambridge, MA Tours
Canada Place Tours
Canadian Museum of History Tours
Canadian Pacific Railway's Last Spike Tours
Canadian Parliament Buildings Tours
Canadian Rockies Tours
Canadian War Museum Tours
Cannery Row Tours
Cannes Tours
Canyon Sainte-Anne Tours
Capilano Suspension Bridge Tours
Capitol Hill Tours
Casa Loma Tours
Casper Tours
Cathedral of Santa Maria del Fiore Tours
Cathedral-Basilica of Notre-Dame de Quebec Tours
Cave of the Winds Tours
Central Park Tours
Champs-Elysees Tours
Chapel Bridge Tours
Charging Bull Tours
Charles Bridge Tours
Charlottetown Tours
Chateau Frontenac Tours
Chattanooga Tours
Chicago Tours
Chihuly Glass Garden Tours
Chinatown Vancouver Tours
Chinatown, Montreal Tours
Chinatown, SF Tours
Chinatown, Toronto Tours
Chinatown, Victoria Tours
Citadelle of Quebec Tours
Cody Tours
Cologne Tours
Cologne Cathedral Tours
Colosseum Tours
Columbia Ice Explorer Tours
Columbia Icefield Tours
Confederation Bridge Tours
Congress Hall, Philadelphia Tours
Corning Tours
Corning Museum of Glass Tours
Coster Diamonds Factory Tours
Crater Lake National Park Tours
Crazy Horse Memorial Tours
Dam Square Tours
Danube River Tours
De Catherina Hoeve (The Cheese Farm) Tours
Death Valley National Park Tours
Denver Tours
Desert Hills Premium Outlets Tours
Desert View Watchtower Tours
Devils Tower National Monument Tours
Disney California Adventure Tours
Disney World - Magic Kingdom Tours
Disney's Animal Kingdom Tours
Disney's Blizzard Beach Tours
Disney's Hollywood Studios Tours
Disney's Typhoon Lagoon Tours
Disneyland Park Tours
Dolly Parton's Dixie Stampede Tours
Downtown LA Tours
Eagle Point Tours
Edinburgh Tours
Edinburgh Castle Tours
Eiffel Tower Tours
El Capitan Tours
Elisabeth Bridge Tours
Emerald Lake Tours
Empire State Building Tours
Engelberg Tours
Epcot Center Tours
Ethel M. Chocolate Factory and Botanical Cactus Gardens Tours
Everglades National Park Tours
Felton Tours
Fifth Avenue Tours
Finger Lakes Tours
Fisherman's Bastion Tours
Fisherman's Wharf Tours
Florence Tours
Flowerpot Island Tours
Fort Lauderdale Tours
Fragonard Perfume Factory, Grasse Tours
Frankfurt Tours
Fredericton Tours
Fremont Street Experience Tours
Fresno Tours
Galeries Lafayette Department Store Tours
Galleria Vittorio Emanuele II Tours
Gastown Tours
Gatlinburg Tours
Gellert Hill Tours
Genoa Tours
Gillette Tours
Giotto's Campanile Tours
Glacier Skywalk Tours
Glen Canyon Dam Tours
Goat Island Tours
Golden Tours
Golden Gate Bridge Tours
Golden Gate Park Tours
Golden Hall Tours
Golden Lane Tours
Gran Via Tours
Granada Tours
Grand Canal Tours
Grand Canyon Tours
Grand Canyon National Park Tours
Grand Canyon Skywalk Tours
Grand Canyon South Rim Tours
Grand Canyon West Rim Tours
Grand Canyon of the Yellowstone Tours
Grand Place Tours
Grand Teton National Park Tours
Granville Island Tours
Great Salt Lake Tours
Great Smoky Mountains Tours
Green Gables House Tours
Gretna Green Tours
Grindelwald Tours
Ground Zero Tours
Guano Point Tours
Half Dome Tours
Halifax Tours
Hanauma Bay Tours
Hartland Covered Bridge Tours
Harvard Square Tours
Harvard University Tours
Heart Island Tours
Hemingway House and Museum Tours
Heroes' Square Tours
Hershey's Chocolate World Tours
Hofburg Palace Tours
Hollywood Tours
Hollywood Boulevard Tours
Hollywood Walk of Fame Tours
Honolulu Tours
Hoover Dam Tours
Hopewell Rocks Tours
Horseshoe Bend Tours
Horseshoe Falls Tours
Hotel National des Invalides Tours
Houses of Parliament Tours
Hradčany Quarter Tours
Hualapai Ranch Tours
Hudson River Tours
Hungarian Parliament Building Tours
Icefields Parkway Tours
Independence Hall Tours
Independence National Historical Park Tours
Independence Rock Tours
Interlaken Tours
International Spy Museum Tours
Intrepid Sea, Air & Space Museum Tours
Iolani Palace Tours
Island of Oahu Tours
Islands of Adventure Tours
Jackson Tours
Jackson Hole Tours
Jasper Tours
Jasper National Park Tours
Johnston Canyon Tours
Journey Behind the Falls Tours
Jungfrau Tours
Jungfraujoch Tours
Kamloops Tours
Kanab, UT Tours
Karl Marx House Tours
Kelowna Tours
Kennedy Space Center Tours
Keukenhof Gardens Tours
Key West Tours
King Kamehameha Statue Tours
King's College Tours
Kingman Tours
Kingston Tours
Kingston City Hall Tours
Koblenz Tours
Korean War Veterans Memorial Tours
LAX Airport Tours
La Rambla Tours
La Scala Tours
Lake District National Park Tours
Lake Louise Tours
Lake Mead Tours
Lake Minnewanka Tours
Lake Ontario Tours
Lake Powell Tours
Lake Tahoe Tours
Lake Titisee Tours
Lake Windermere Tours
Lake Zurich Tours
Lancaster Tours
Las Vegas Tours
Las Vegas Bellagio Fountains Tours
Las Vegas Strip Tours
Leaning Tower of Pisa Tours
Leavenworth Tours
Liberty Bell Tours
Library of Congress Tours
Lincoln Memorial Tours
Lion Monument Tours
Lisbon Tours
Little Havana Tours
Lombard Street Tours
London Tours
Lookout Mountain Tours
Los Angeles Tours
Louvre Museum Tours
Lucerne Tours
Lucerne Water Tower Tours
Luna Island Tours
Luray Caverns Tours
Luxembourg Tours
Madame Tussauds Hollywood Tours
Madame Tussauds New York Tours
Madame Tussauds Washington D.C. Tours
Madrid Tours
Madrid Plaza Mayor Tours
Magnetic Hill Tours
Maid of the Mist Tours
Maligne Canyon Tours
Maligne Lake Tours
Manchester Tours
Manneken Pis Tours
Marienplatz Tours
Maritime Museum of the Atlantic Tours
Market Street Tours
Martha's Vineyard Tours
Massachusetts Institute of Technology (MIT) Tours
Mather Point Tours
Matthias Church Tours
Melbourne Tours
Metropolitan Museum of Art Tours
Miami Tours
Michelangelo Square Tours
Midtown Manhattan Tours
Milan Tours
Milan Cathedral Tours
Modesto Tours
Mojave Desert Tours
Monaco Tours
Mondsee Abbey Tours
Monterey Bay Tours
Monterey Bay Aquarium Tours
Montmorency Falls Tours
Montparnasse Tower Tours
Montreal Tours
Montreal Biodome Tours
Montreal City Hall Tours
Montreal Olympic Park Tours
Montreal Olympic Stadium Tours
Montreal Olympic Tower Tours
Montserrat Tours
Monument Valley Tours
Moraine Lake Tours
Moulin Rouge Tours
Mount Rainier National Park Tours
Mount Robson Tours
Mount Royal Tours
Mount Titlis Tours
Mt Rushmore Tours
Mud Volcano Tours
Munich Tours
Murano Tours
N/A Tours
NYC Trinity Church Tours
Napa Valley Tours
National Museum of Natural History Tours
National War Memorial of Canada Tours
Natural Bridge Tours
Natural Bridge, Canada Tours
Navajo Nation Tours
Naval Base San Diego Tours
Neues Rathaus (New Town Hall) Tours
New England Aquarium Tours
New Haven Tours
New York Tours
New York Stock Exchange Tours
Newport, RI Tours
Niagara Falls Tours
Niagara Falls Illumination Tours
Niagara Falls, NY Tours
Niagara Falls, ON Tours
Niagara Power Vista Tours
Niagara River Gorge Tours
Niagara-on-the-Lake Tours
Nice Tours
Notre Dame Basilica of Montreal Tours
Oak Creek Canyon Tours
Okanagan Lake Tours
Old Faithful Tours
Old Fort Niagara Tours
Old Montreal Tours
Old Quebec City Tours
Olympic National Park Tours
One Liberty Observation Deck Tours
One World Observatory Tours
One World Trade Center Tours
Ontario Legislative Building Tours
Orlando Tours
Ottawa Tours
Oue Skyspace Tours
Our Lady of Reims Cathedral Tours
Page, AZ Tours
Palace of Fine Arts Tours
Palace of Versailles Tours
Palais des Festivals Tours
Palffy Palace Tours
Palm Springs Tours
Pantheon Tours
Paris Tours
Parliament Hill Tours
Parque del Buen Retiro Tours
Peace Tower Tours
Peak 2 Peak Gondola Tours
Pearl Harbor Tours
Pebble Beach Tours
Peyto Lake Tours
Philadelphia Tours
Piazza del Duomo Tours
Piazza della Signoria Tours
Pigeon Forge Tours
Pike Place Market Tours
Pisa Tours
Pisa Baptistery Tours
Pisa Cathedral Tours
Place de la Concorde Tours
Place de la Constitution Tours
Plaza de Espana (Madrid) Tours
Plaza de Oriente Tours
Plaza de Toros de Las Ventas Tours
Plimoth Plantation Tours
Pocatello Tours
Pont Adolphe Tours
Ponte Vecchio, Florence Tours
Porta Nigra Tours
Portland Tours
Portland Head Light Tours
Potomac River Tours
Prague Tours
Prague Astronomical Clock Tours
Prague Castle Tours
Prague Old Town Square Tours
Prince Edward Island Tours
Prince's Palace of Monaco Tours
Princeton Tours
Princeton University Tours
Provo Tours
Pulteney Bridge Tours
Quebec Tours
Quebec Capital Observatory Tours
Quebec Ice Hotel Tours
Quebec Parliament Building Tours
Queen's College Tours
Queen's University of Kingston Tours
Quincy Market Tours
Rainbow Bridge Tours
Rathaus-Glockenspiel Tours
Red Rocks Park Tours
Redwood National and State Parks Tours
Reims Tours
Reno Tours
Revelstoke Tours
Rhine Falls Tours
Rhine River Tours
Rhine Valley Tours
Rideau Hall Tours
Ripley's Aquarium of Canada Tours
Ripley's Believe It or Not Tours
River Seine Tours
River Thames Tours
Rivière-du-Loup Tours
Roaring Camp Railroads Tours
Rock City Gardens Tours
Rockefeller Center Tours
Rocky Mountain National Park Tours
Rodeo Drive Tours
Rogers Pass Tours
Rome Tours
Romer Platz Tours
Route 66 Tours
Royal Military College Tours
Ruby Falls Tours
Rudesheim Tours
Sacramento Tours
Saint Joseph's Oratory of Mount Royal Tours
Salem Witch Museum Tours
Salmon Arm Tours
Salt Lake City Tours
Salt Lake Temple Tours
San Diego Tours
San Francisco Tours
San Francisco Bay Tours
San Francisco City Hall Tours
San Francisco Civic Center Tours
Sanremo Tours
Santa Barbara Tours
Santa Maria de Montserrat Tours
Santa Monica Tours
Santa Monica Pier Tours
Santa Monica State Beach Tours
Sausalito Tours
Schaffhausen Tours
Schonbrunn Palace Tours
SeaWorld Orlando Tours
SeaWorld San Diego Tours
Seattle Tours
Secret Caverns Tours
Sedona Tours
Seven Mile Bridge Tours
Seville Cathedral Tours
Sforza Castle Tours
Shakespeare's Birthplace Tours
Shediac Tours
Silicon Valley Tours
Silicon Valley Apple Park Tours
Skylon Tower Tours
Smithsonian Institution Tours
Smithsonian National Air and Space Museum Tours
Snoqualmie Falls Tours
Solvang Tours
Solvang City Tours
South Beach Tours
Southernmost Point Monument Tours
Space Needle Tours
Spanish Steps Tours
Spanish Village (Poble Espanyol) Tours
St. George Tours
St. John Tours
St. Mark's Basilica Tours
St. Mark's Campanile Tours
St. Mark's Square Tours
St. Peter's Basilica Tours
St. Peter's Square Tours
St. Stephen's Cathedral Tours
St. Vitus Cathedral Tours
Stanford University Tours
Stanley Park Tours
Statue of Liberty Tours
Stonehenge Tours
Stratford Upon Avon Tours
Sulphur Mountain Tours
Sunmore Ginseng Factory Tours
Sunset Strip Tours
Supreme Court Tours
Swiss Alps Tours
TCL Chinese Theatre Tours
Tanger Outlets Barstow Tours
Temple Square Tours
The Breakers Tours
The Lido Tours
The Lion of Venice Tours
The Museum of Modern Art Tours
The National September 11 Memorial & Museum Tours
The Pentagon Tours
The Presidio of San Francisco Tours
The Royal Mile Tours
Thomas Jefferson Memorial Tours
Thousand Islands Tours
Thunderbird Park Tours
Tidal Basin Tours
Times Square Tours
Toronto Tours
Toronto City Hall Tours
Toronto Islands Tours
Trevi Fountain Tours
Trier Tours
Trinity Church Tours
Tsawwassen Tours
Twin Peaks Tours
USS Arizona Memorial Tours
Union Square San Francisco Tours
United Nations Building Tours
United States Capitol Tours
Universal Studios Florida Tours
Universal Studios Hollywood Tours
University of California Berkeley Tours
University of Toronto Tours
Upper Spiral Tunnel Tours
Utah State Capitol Tours
Vaduz Tours
Valemount Tours
Vancouver Tours
Vancouver Island Tours
Vatican City Tours
Venice Tours
Vernon Tours
Versailles Tours
Victoria Tours
Victoria Harbour Tours
Vienna Tours
Vienna State Opera Tours
Vietnam Veterans Memorial Tours
Waikiki Beach Tours
Wall Street Tours
Walt Disney Concert Hall Tours
Washington Monument Tours
Washington, DC Tours
Watkins Glen Park Tours
Watkins Glen State Park Tours
West Point Military Academy Tours
West Yellowstone Tours
Westminster Abbey Tours
Whirlpool State Park Tours
Whistler Tours
Whistler Village Tours
White House Tours
Woodbury Common Premium Outlets Tours
Wooden Shoe Workshop & Museum Tours
World Trade Center Tours
World War II Memorial Tours
Yale University Tours
Yavapai Point Tours
Yellowstone Lake Tours
Yellowstone National Park Tours
Yellowstone River Tours
Yoho National Park Tours
York Minster Tours
York, England Tours
Yorkdale Shopping Centre Tours
Yosemite Falls Tours
Yosemite National Park Tours
Zaanse Schans Tours
Zaragoza Tours
Zion National Park Tours
Zurich Tours";
 $string = trim( preg_replace('~[\r\n]+~', ',', $string));
 //echo $string;
 $data = explode(',',$string);
//print_r($data);
?> 

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/sitemap.css?v11" />

 
<div class="main_sitemap"> 
  <h1>
	<strong>Travpart Sitemap</strong>
  </h1> 
  
    <div class="tab">
	  <button class="tablinks active" onclick="openCity(event, 'London')">Vacation Packages</button>
	  <button class="tablinks" onclick="openCity(event, 'detail_')">Tour Details</button>
	  <button class="tablinks" onclick="openCity(event, 'webheirarchy')">Web's Hierarchy</button>
	</div>

	<div id="London" class="tabcontent">
	  <h3>Vacation Page</h3>
	  
	  <div class="pages_list">
	   	<div class="sitemap_links">
	   	
		   	 <div class="sitemap_link_row">
	   			<?php $a=1; foreach($data as $va){ $a++;?>
					<div class="sitemap_link_col">
						<a href="#" target="_blank">
							<?php echo $va; ?>
						</a>
					</div>
					<?php if($a>4){ ?>
							</div>
							<div class="sitemap_link_row">
					 <?php	$a=1;} ?>
				<?php } ?>
			</div>
			
		</div>
		<div class="fclear"></div>
	    </div>
	    
	    <nav aria-label="...">
		  <nav aria-label="Page navigation">
			  <ul class="pagination">
			    <li><a href="#">1</a></li>
			    <li><a href="#">2</a></li>
			    <li><a href="#">3</a></li>
			    <li><a href="#">4</a></li>
			    <li><a href="#">5</a></li>
			  </ul>
			</nav>
		</nav>
	    
	  </div>
	  
	</div>

	<div id="detail_" class="tabcontent">
	  <h3>Tour Details</h3>
	</div>

	<div id="webheirarchy" class="tabcontent">
	  <h3>Web Heirarchy</h3>
	</div>

  	
</div>

 
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

</script>
  
 
   
<?php 
 get_footer();
?>