<?php
/* Template Name: Home Page */
get_header();
if(have_posts()) {
	while(have_posts()) {
		the_post();
		the_content();
	}
} else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>



