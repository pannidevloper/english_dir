<style type="text/css">
    #search_price_box{
        position: fixed;
        left: 0;
        top: 32%;
        width: 491px;
        padding: 0px 0px 0px 0px !important;
        display: none;
    }

    #findDealsBtnCustom{
        position: fixed;
        top: 36%;
        left: 0;
        padding: 20px 0px;
        width: 40px;
    }
</style>
<?php
    get_header();
?>

<Button id="findDealsBtnCustom"> F<br>i<br>n<br>d<br><br> D<br>e<br>a<br>l<br>s<br> </Button>

<?php
echo '<div id="search_price_box" class="container">';
            echo '<div id="search_price_box_form">';
            echo'<i class="fa fa-close closebtn" style="font-size:36px;float:right;"></i>';
?>

<?php echo search_price_box(); ?>
            
<script type="text/javascript">

$(document).ready(function () {
    if(window.location.href.indexOf("/tag/") > -1  && window.location.href.indexOf("/?s=") > -1) {
       // jQuery("#search_price_box").hide();
       jQuery("#findDealsBtnCustom").on('click',function(){
        jQuery(this).hide();
        jQuery("#search_price_box").show();
       })

    }
    jQuery(".closebtn").on('click',function(){
        jQuery("#search_price_box").hide();
        jQuery("#findDealsBtnCustom").show(); 
    });
});

</script>
<?php
    echo '</div>';
    echo '</div>';
?> 



<?php
 
$destination = isset($_GET['destination']) ? trim($_GET['destination']) : '';
$trip_type = isset($_GET['trip-type']) ? trim($_GET['trip-type']) : '';
$search_box_range = isset($_GET['search-box-range']) ? trim($_GET['search-box-range']) : '';

?>

<?php
if( $destination != '' || $trip_type != '' || $search_box_range != '') {
if(have_posts()) {
//    echo '<div class="row">';
	while(have_posts()) {
		the_post(); ?>
        <div class="destination_trip_type">
                <?php if ( has_post_thumbnail() ) {  ?>
                    <div class="featured_image">
                        <?php the_post_thumbnail('custom-size'); ?>
                    </div>
                <?php } ?>
                <div class="tour_content">
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

					<?php if(get_post_meta(get_the_ID(), '_cs_logo', true)) { ?>
                  	<img width="90" height="90" style="float:right; position:relative; top:80px; left:80px; " class="attachment-custom-size size-custom-size wp-post-image lazy" src="<?php echo wp_get_attachment_url(get_post_meta(get_the_ID(), '_cs_logo', true)); ?>" />
                    <?php } ?>
					
                    <?php $meta = get_post_meta( get_the_ID(), '_cs_adultprice' );  ?>
                    <?php if( isset( $meta[0] ) && trim($meta[0]) != '' ) {  $price = trim($meta[0]); ?>
                        <p><span class="adult_span result-1s"> Adult <span class="price_span"> $<?php echo round($price * get_option("_cs_currency_USD")); ?> </span> / person </span></p>
                    <?php } ?>
                    <?php $meta = get_post_meta( get_the_ID(), '_cs_childrenprice' );  ?>
                    <?php if( isset( $meta[0] ) && trim($meta[0]) != '' ) {  $price = trim($meta[0]); ?>
                        <p><span class="child_span result-1s"> Children <span class="price_span"> $<?php echo round($price * get_option("_cs_currency_USD")); ?> </span> / person  </span></p>
                    <?php } ?>
                    <?php //$price = get_( get_the_ID() ); ?>
                    <a class="btn btn-default" href="<?php the_permalink(); ?>">Book Now</a>
                </div>
                <div class="itinerary">
                    <?php
                        if( trim(get_post_meta($post->ID,'_cs_overview_content', true))!="") {
                            $the_content = strip_shortcodes(get_post_meta($post->ID,'_cs_overview_content', true));
                            $the_content = preg_replace("/<img[^>]+\>/i", "", $the_content);
                            echo wp_trim_words($the_content, 20);
                        }
                    ?>
                    <br />
                </div>
                <div class="clear"></div>
                <hr />
            </div><?php
        }
	kriesi_pagination();
}
else {
	get_template_part('tpl-pg404', 'pg404');
}
}
get_footer();
?>