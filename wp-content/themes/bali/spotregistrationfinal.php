<?php
//error_reporting(E_ALL);
//ini_set("display_errors", "On");

session_start();
require("dbconnect.php");
/* Template Name: Bucketlistpage */
get_header();

global $wpdb;

$has_hotel = false;
$has_vehicles = false;
$has_activity = false;

$eainums = array();
$notecontents = array();
$nindex = 0;
$guide_total_price = 0;

function unescape($str) {
    $i = 0;
    do {
        $str = unescapecall($str);
        $i++;
    } while ((stripos($str, '%25') !== FALSE || stripos($str, '%2C') !== FALSE) && $i < 8);
    return $str;
}

function unescapecall($str) {
    $ret = '';
    $len = strlen($str);
    for ($i = 0; $i < $len; $i ++) {
        if ($str[$i] == '%' && $str[$i + 1] == 'u') {
            $val = hexdec(substr($str, $i + 2, 4));
            if ($val < 0x7f)
                $ret .= chr($val);
            else
            if ($val < 0x800)
                $ret .= chr(0xc0 | ($val >> 6)) .
                        chr(0x80 | ($val & 0x3f));
            else
                $ret .= chr(0xe0 | ($val >> 12)) .
                        chr(0x80 | (($val >> 6) & 0x3f)) .
                        chr(0x80 | ($val & 0x3f));
            $i += 5;
        } else
        if ($str[$i] == '%') {
            $ret .= urldecode(substr($str, $i, 3));
            $i += 2;
        } else
            $ret .= $str[$i];
    }
    return $ret;
}
?>
<style>
.date_d{
	border-left:1px solid #ccc;
	border-right:1px solid #ccc
}
    /* Popup box BEGIN */
    .custom_comment_hide{
		display: none;
	}
    .hover_bkgr_fricc{
        background:rgba(0,0,0,.4);
        cursor:pointer;
        display:none;
        height:100%;
        position:fixed;
        text-align:center;
        top:0;
        width:100%;
        z-index:10000;
        left:0;
    }
    .hover_bkgr_fricc .helper{
        display:inline-block;
        height:100%;
        vertical-align:middle;
    }
    .hover_bkgr_fricc > div {
        background-color: #fff;
        box-shadow: 10px 10px 60px #555;
        display: inline-block;
        height: auto;
        max-width: 551px;
        min-height: 100px;
        vertical-align: middle;
        width: 60%;
        position: relative;
        border-radius: 8px;
        padding: 15px 5%;
    }
    a,b,p,div,button,input,h1,h2,h3,h4,h5,h6{
		    font-family: 'Heebo', sans-serif;
	}
    .popupCloseButton {
        background-color: #fff;
        border: 3px solid #999;
        border-radius: 50px;
        cursor: pointer;
        display: inline-block;
        font-family: arial;
        font-weight: bold;
        position: absolute;
        top: -20px;
        right: -20px;
        font-size: 25px;
        line-height: 30px;
        width: 30px;
        height: 30px;
        text-align: center;
    }
    .popupCloseButton:hover {
        background-color: #ccc;
    }
    .trigger_popup_fricc {
        cursor: pointer;
        font-size: 20px;
        margin: 20px;
        display: inline-block;
        font-weight: bold;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .backtoformcustom a{margin-right: 0!important;padding:17px!important;}

    .leaflet-control-attribution {
        display:none;
    }


    /* change update 21/02*/

    .radiorow {
        padding: 0px!important;
    }
    .social_connect_form label
    {
        display: contents!important;
        font-weight: 400;
    }
    .col-md-5.radio-col {
        margin-top: 10px;
    }
    button.btn.btn-primary.btn-follow {
        background-color: #ff6633!important;
        outline:none;
        box-shadow: 0 1px 5px 0 #ff6633!important;
        /* margin-bottom: 5px; */
    }
    label.radio-label {
        margin-left: 12px;
        cursor: pointer
    }
    .commentbox_wrapper
    {
        padding-right: 30px;
    }
    .row.commentbox {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.12);
        padding: 8px;
        margin-top: 15px;
        border-left: 3px solid #1ba695;
    }
    .c-name p {
        color: #ff6633;
        font-size: 18px;
    }
    .c-date {
        color: #777777;
        text-align: right;
        font-style: italic;
        font-size: 14px;
    }
    form#commentForm textarea {
        height: 100px;
        width: 400px;
        margin-bottom: 25px;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.12);
    }
    input#r1,input#r2{width:auto;}
    .col-md-4.radio-col {
        margin-top: 9px;
    }


    .radio-col input[type=radio],.radio-col input[type=radio]:hover{
        opacity: 0!important;
    }

    /*.radio-col input[type=radio] + label.radio-label:before,*/
    .radio-col input[type=radio] + label.radio-label span.radio:before
    {
        content: '\f164';
        font-size: 14px;
        left: -8px;
        position: relative;
        font-family: 'FontAwesome';

    }
    .radio-col input[type=radio] + label.radio-label span.radio1:before
    {
        content: '\f165';
        font-size: 14px;
        left: -8px;
        position: relative;
        font-family: 'FontAwesome';

    }

    .btn-un-follow{
        background-color: #e5e5e5 !important;
        color: #000 !important;
    }

  /* cost-box style */
    .cost-box {
      background: #99d9ea;
      border-radius: 5px;
      font-size: 16px;
      color: #333;
      min-height: 350px;
      display: flex;
      flex-direction: column;
      justify-content: space-between; }
    .cost-box .bd {
      padding: 30px;
      display: flex;
      justify-content: flex-end;
      text-align: right; }
    .cost-box .bd .cost-label {
      padding-right: 10px; }
    .cost-box .bd .cost-label > div {
      margin-bottom: 10px; }
    .cost-box .bd .cost-value {
      font-weight: bold; }
    .cost-box .bd .cost-value > div {
      margin-bottom: 10px; }
    .cost-box footer {
      display: flex;
      font-size: 12px; }
    .cost-box footer > div {
      flex: 1 1 auto; }
    .cost-box .cost-action {
      display: flex;
      align-items: center;
      justify-content: center;
      background: #c8bfe7;
      cursor: pointer;
      padding: 14px 5px; }
    .cost-box .cost-action:first-child {
      border-bottom-left-radius: 5px; }
    .cost-box .cost-more {
      display: flex;
      align-items: center;
      justify-content: center;
      background: #23b04c;
      cursor: pointer;
      font-size: 22px;
      padding: 14px 5px;
      border-bottom-right-radius: 5px; }
    .cost-box .cost-action-label {
      color: #333; 
      font-weight: 100;
    }
    .cost-action-value {
        font-weight: 100;
    }
    span#openRedeemMyCoupon img {
        width: 30px;
        height: 24px;
        margin-right: 10px;
    }
    .btn-blue {
        border: solid 1px #22b14c !important;
        background-color: #fff !important;
        color: #22b14c !important;
    }
    .btn-green {
        border: solid 1px #22b14c !important;
        background-color: #22b14c !important;
        color: #fff !important;
    }
    .marvel-shareoption-view.pdfcrowd-remove {
        display: none;
    }
    @media (min-width: 768px) {
      .cost-box footer {
        font-size: 14px; }
      .cost-box .cost-action .fa, .cost-box .cost-action .fas, .cost-box .cost-action .fab {
        font-size: 24px;
        margin-right: 5px; }
      .cost-box .cost-more .fa, .cost-box .cost-more .fas, .cost-box .cost-more .fab {
        margin-left: 5px; } }

  .steps-widget {
    position: fixed;
    top: 168px;
    right: 100px;
    background: #fff;
    z-index: 99998;
    width: 200px;

    border-radius: 5px;
    box-shadow: 0px 1px 6.7px 0.4px rgba(10, 2, 5, 0.2);
    padding-top: 40px;
  }
  .steps-widget-close {
    position: absolute;
    right: 10px;
    top: 10px;
    color: #666;
    cursor: pointer;
    color: #666!important;
  }
  @media (max-width: 600px) {
    .tr_dtl_com_mob{
        display: block !important;
    } 
    .tr_dtl_com_desk{
        display: none !important;
    } 
    .cost-action-value {
        text-align: end;
        margin-bottom: -5px;
    }  
    .tr_dtl_com_mob {
        text-align: center;
    } 
    sup {
        font-size: 9px;
        vertical-align: super;
    } 
    .cost-box .cost-action-label {
        font-size: 14px;
    } 
    .cost-more a {
        font-size: 18px;
    }
    table.mobileappbg {
        margin-top: 0px;
    }
    table.mobileappbg tbody tr{
        border: none;
    }
    table.mobileappbg tbody tr td{
        padding-left: 0%;
        padding: 0px;
    }
    .flex_row_dinn_arr{
        width: 100% !important;
    }
    .customer_sat {
        background: #99d9ea !important;
    }
    .customer_sat h1{
        padding-top: 0px !important;
    }
    .booking_code_desk{
        display: none;
    }
    .booking_code_mob{
        display: block !important;
    }
    .display_table {
        width: 10% !important;
    }
    .display_table_mob {
        width: 10%;
        position: relative;
        display: table-cell;
        vertical-align: middle;
    }
    .booking_code_mob.order-sm-last.ml-sm-auto.mb-3.mb-sm-0 {
        margin-bottom: 0px !important;
    }
    .container.for_remove_ad {
        margin-top: 61px;
    }
    .book_mar_mob{
        margin-bottom: 0px !important;
    }
    .booking-code h2 span {
        font-size: 13px !important;
    }
    .mobileonly.mobileapp {
        display: none;
    }
    .v2-foot.text-right.p-3.pr-sm-5 {
        background-color: #99d9ea !important;
        background-image: none;
        color: black !important;
        padding: 10px !important;
        text-align: center !important;
        font-size: 30px;
    }
    .v2-foot.text-right.p-3.pr-sm-5 strong span span {
        color: black !important;
    }
 }
 @media (min-width: 180px) and (max-width: 320px) {
    .cost-more i.fa.fa-angle-down {
        margin-left: 80px;
    }
    .cost-more a{
        font-size: 13px;
    }
 }
 @media (min-width: 321px) and (max-width: 360px) {
    .cost-more i.fa.fa-angle-down {
        margin-left: 110px;
    }
 }
  @media (max-width: 1200px) {
    .steps-widget {

      display: none;
    }
  }

  @media (max-width: 768px) {
    .steps-widget {
      top:80px;
      display: none;
    }
  }

  .steps-item {
    padding-left: 23px;
    padding-bottom: 40px;
    position: relative; }

  .steps-item-num {
    border-radius: 50%;
    width: 25px;
    height: 25px;
    line-height: 25px;
    text-align: center;
    border: solid 1px #e5e5e5;
    background-color: #ffffff;
    font-weight: bold;
    color: #999999;
    position: absolute;
    left: -12px; }

  .steps-item-title {
    font-size: 14px;
    font-weight: bold;
    letter-spacing: -0.4px;
    text-align: left;
    color: #666666; }

  .steps-item-des {
    font-size: 16px;
    letter-spacing: -0.4px;
    text-align: left;
    color: #666666; }

  .active .steps-item-num {
    background-color: #66cccc;
    color: #fff;
    border-color: #66cccc; }

  .active .steps-item-title {
    color: #66cccc; }

  .active .steps-item-des {
    color: #1a6d84; }
  
  .modal-body input {
    width:auto;
  }
  .bg_white{
  	width: 63px;
  	height: 30px;
  	background: white;
  	vertical-align: middle;
  	display: inherit;
  	text-align: center;
  	position: absolute;
    right: 7px;
    top: 18px;
    font-size: 25px;
    color: #dad4d3;
    font-weight: bold;
  }
.display_table {
    width: 30%;
    position: relative;
    display: table-cell;
    vertical-align: middle;
}

.display_table_1 {
    width: 10%;
    position: relative;
    display: table-cell;
    vertical-align: middle;
}
.border-radius.bg-blue-0.p-1.pr-2.pl-2.font-xl{
    background-color: #7f7f7f !important;
}
.border-radius.bg-blue-0.p-1.pr-2.pl-2.font-xl i {
    color: #000;
}
.d-flex.bg-blue-3.text-white.align-items-center.border-radius.v2-header.mb-2 {
    background-color: #c8bfe7 !important;
}
#AddAnotherTravelItemManually div.border-radius.bg-blue-0.p-1.pr-2.pl-2.font-xl {
    background-color: #7f7f7f !important;
}
#AddAnotherTravelItemManually.d-flex.bg-blue-3.text-white.align-items-center.border-radius.v2-header.mb-2 {
    background-color: #c8bfe7 !important;
}
#AddAnotherTravelItemManually button#addManualItem {
    background-color: #c8bfe7 !important;
}
.font-lg.p-2.pr-3.pl-3.mr-auto {
    color: #333;
}
.btn.p-2.pr-3.pl-3.btn-none.text-white i {
    color: #333;
}
.v2-foot.text-right.p-3.pr-sm-5 {
    background-color: #99d9ea !important;
    background-image: none;
    color: #333;
}
.v2-foot.text-right.p-3.pr-sm-5 strong span span {
    color: #333;
}
.font-lg1{
	font-size: 19px;
	color: black;
    text-align: left;
}
.flex_row_dinn_arr{
    width: 35%;
}
.booking_code_mob{
        display: none;
    }
.custom_row_tour_details{
	background: #ececec;
	margin-bottom: 20px;
}

  button.button.media-button.button-primary.button-large.media-button-select{
    right: 15px;
    position: relative;
  }

.hotel_map,.map_div,.date_d{
	display: inline-block;
	padding: 0px 15px;
	vertical-align: middle;
}
.hotel_map h1 i{	
	color: #333;
}
.hotel_map p i,.date_d i{
	color: #16b9aabf
}
.hotel_map p,.date_d{
	font-size: 14px;
}
.tr_dtl_com_mob{
    display: none;
}
</style>
<?php if ($_GET['device'] == 'app') { ?>
    <style type="text/css">
        .container.for_remove_ad {
            margin-top: 0px;
        }
    </style>
<?php } ?>
<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/tableresponsive.css"/>
<link href="<?php bloginfo('template_url'); ?>/css/tour.css" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/travcust.css?v=50" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maps.api.sygic.com/js/leaflet/1.0.3/leaflet.css" />
<link rel="stylesheet" href="https://maps.api.sygic.com/js/sygic/1.2.0/leaflet.sygic.css" />

<script src="https://maps.api.sygic.com/js/leaflet/1.0.3/leaflet.js"></script>
<script src="https://maps.api.sygic.com/js/leaflet.sygic-1.1.0.js"></script>

<script src="<?php bloginfo('template_url'); ?>/js/bootstrap-notify.js"></script>

<script src="https://www.travpart.com/English/wp-content/themes/bali/js/sss.min.js"></script>
<link rel="stylesheet" href="https://www.travpart.com/English/wp-content/themes/bali/css/sssilder.css" type="text/css" media="all">
<script>jQuery(function($) {$('.slider').sss();});</script>


<input id="cs_RMB" type="hidden" value="<?php echo get_option('_cs_currency_RMD'); ?>" />
<input id="cs_USD" type="hidden" value="<?php echo get_option('_cs_currency_USD'); ?>" />
<input id="cs_AUD" type="hidden" value="<?php echo get_option('_cs_currency_AUD'); ?>" />

<script>
    jQuery(document).ready(function () {
    	
    	 jQuery(document).on('click','.custom_btn',function(){
      		var id = $(this).data('id');
      		var optional_location_d = getCookie('optional_location');			
			var op = JSON.parse(optional_location_d);
      		//console.log(op);
      		var data = $.grep(op, function(e){ 
			 return e.id !== id; 
			});
			 setCookie('optional_location',JSON.stringify(data));
      		$('.location_number').html(data.length);
      		$('.div_d').empty();
			$.each(data,function(key,val){
				$('.div_d').append('<div class="list_location"><div class="map_div"><a href="https://maps.google.com/maps?q='+val.custom_lat+','+val.custom_lng+'&hl=es;z=5&amp;output=embed" style="color:#0000FF;text-align:left" target="_blank"><iframe width="200" height="120" frameborder="0"scrolling="no" margin height="0"marginwidth="0" src="https://maps.google.com/maps?q='+val.custom_lat+','+val.custom_lng+'&hl=es&z=5&amp;output=embed"></iframe></a></div><div class="hotel_map"><h1><i class="fas fa-utensils fa-fw"></i>'+val.searchitemname+'</h1><p><i class="fas fa-map-pin"></i> '+val.spotnames+' </p></div><div class="date_d"><i class="fas fa-calendar-week"></i>'+val.spotdate+' <i class="fa fa-clock"></i><br >'+val.spottime+'</div><div class="close_btn"><span class="btn btn-default custom_btn" data-id="'+val.id+'">X</span></div></div>')
			});
      })
    	
    	//alert(getCookie('spotname')+" "+getCookie('spotdate')+" "+getCookie('spottime'))
    	if( getCookie('spotname')!='' &&  getCookie('spotdate')!='' && getCookie('spottime')!='') {
    		var optional_location_d = getCookie('optional_location');
				 
			var op = JSON.parse(optional_location_d);
			$('.location_number').html(op.length);	
			$.each(op,function(key,val){
				console.log(val);
				$('.div_d').append('<div class="list_location"><div class="map_div"><a href="https://maps.google.com/maps?q='+val.custom_lat+','+val.custom_lng+'&hl=es;z=5&amp;output=embed" style="color:#0000FF;text-align:left" target="_blank"><iframe width="200" height="120" frameborder="0"scrolling="no" margin height="0"marginwidth="0" src="https://maps.google.com/maps?q='+val.custom_lat+','+val.custom_lng+'&hl=es&z=5&amp;output=embed"></iframe></a></div><div class="hotel_map"><h1><i class="fas fa-utensils fa-fw"></i>'+val.searchitemname+'</h1><p><i class="fas fa-map-pin"></i> '+val.spotnames+' </p></div><div class="date_d"><i class="fas fa-calendar-week"></i> '+val.spotdate+'<i class="fa fa-clock"></i><br >'+val.spottime+'</div><div class="close_btn"><span class="btn btn-default custom_btn" data-id="'+val.id+'">X</span></div></div>')
			});
    	}else{
			$('.optional_location').remove();
		}
    	
        $("#header .container-fluid:first >div").removeClass('container').addClass('container-fluid');
        $(".header_curreny").change(function () {

            var full_name = $(this).find("option:selected").attr("full_name");
            var syml = $(this).find("option:selected").attr("syml");

            var curreny = $(this).val();

            if (curreny == "IDR") {

                $(".change_price").each(function () {
                    $(this).html(syml + "<span class='price_span'>" + $(this).attr("or_pic") + " </span>");

                });

            } else {
                $(".change_price").each(function () {
                    var amount = $(this).attr("or_pic");
                    if (syml == '元')
                    {
                        $(this).html("<span class='price_span'>" + Math.round($('#cs_RMB').val() * amount) + " </span>" + syml);
                    } else
                    {
                        $(this).html(syml + "<span class='price_span'>" + ($('#cs_' + curreny).val() * Math.round(amount)).toFixed(2) + " </span>");
                    }
                });
            }
        });
        setTimeout('$(".header_curreny").change()', 800);
    });

    function setHotelCookie(c_name, value, expiredays)
    {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + expiredays);
        document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : "; expires=" + exdate.toGMTString()) + ";domain=www.travpart.com;path=/English/";
        document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : "; expires=" + exdate.toGMTString()) + ";domain=www.travpart.com;path=/hotel-and-flight-bookings/";
    }
    function setCookie(c_name, value, expiredays)
    {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + expiredays);
        document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : "; expires=" + exdate.toGMTString()) + ";domain=www.travpart.com;path=/English/";
    }
    function getCookie(name) {
        var strCookie = document.cookie;
        var arrCookie = strCookie.split("; ");
        for (var i = 0; i < arrCookie.length; i++) {
            var arr = arrCookie[i].split("=");
            if (arr[0] == name)
                return unescape(arr[1]);
        }
        return "";
    }

    function update_total_price() {
        var total_price = 0;
        var discountp = 0;

        $('.v2-foot .change_price').each(function () {
            total_price += parseInt($(this).attr('or_pic'));
        });

    $('.total-cost .change_price').attr('or_pic', total_price);
        $('.total_price .change_price').attr('or_pic', total_price);
        $('.discount .change_price').attr('or_pic', parseInt(total_price * discountp));
        $('.total_discount .change_price').attr('or_pic', parseInt(total_price * (1 - discountp)));
        $('.tax .change_price').attr('or_pic', parseInt(total_price * 0.01));
        $('.net_price .change_price').attr('or_pic', parseInt(total_price * (1 - discountp) + total_price * 0.01));

    }
</script>


<?php
if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $userid = $current_user->ID;
    $usernm = $current_user->user_login;
    $user_roles = $current_user->roles;
    $user_role = array_shift($user_roles);
    $userstatus = 1;
} else {
    // wp_loginout();
    $userid = "guest";
    $usernm = "guest";
    $userstatus = 0;
}

/*
  if(empty($_GET['bookingcode']))
  {
  $place_img=$_POST['place_img'];
  $place_name=$_POST['place_name'];
  $place_date=$_POST['place_date'];
  $place_time=$_POST['place_time'];
  $tourlist_img=$_POST['tourlist_img'];
  $tourlist_name=$_POST['tourlist_name'];
  $tourlist_price=$_POST['tourlist_price'];
  $tourlist_date=$_POST['tourlist_date'];
  $tourlist_time=$_POST['tourlist_time'];
  $meal_type=$_POST['meal_type'];
  $meal_date=$_POST['meal_date'];
  $meal_time=$_POST['meal_time'];
  $car_type=$_POST['car_type'];
  $car_date=$_POST['car_date'];
  $car_time=$_POST['car_time'];
  $popupguide_type=$_POST['popupguide_type'];
  $popupguide_date=$_POST['popupguide_date'];
  $popupguide_time=$_POST['popupguide_time'];
  $spa_type=$_POST['spa_type'];
  $spa_date=$_POST['spa_date'];
  $spa_time=$_POST['spa_time'];
  $boat_type=$_POST['boat_type'];
  $boat_date=$_POST['boat_date'];
  $boat_time=$_POST['boat_time'];
  $spotname = $_POST['spottitle'];
  $spotdate = $_POST['bookingdate'];
  $spottime = $_POST['bookingtime'];
  $spotlat = $_POST['spotlat'];
  $spotlong = $_POST['spotlong'];
  $dist=$_POST['spotdist'];
  $image=$_POST['spotimage'];

  $total_price=0;

  $ExtraActivity=unserialize(get_option('_cs_extraactivity'));
  if(!empty($ExtraActivity))
  {
  $i=0;
  foreach($ExtraActivity as $t)
  {
  if($_POST['eainum_'.$i]>0)
  {
  //$eainums[]=(int)$_POST['eainum_'.$i];
  $eaiimgs[]=$_POST['eaiimg_'.$i];
  $eaiitems[]=$_POST['eaiitem_'.$i];
  $eaiprices[]=$_POST['eaiprice_'.$i];
  $eaitotals[]=$eaiprices[$i]*$eainums[$i];
  }
  $eainums[]=(int)$_POST['eainum_'.$i];
  $eaidates[]=$_POST['eaidate_'.$i];
  $eaitimes[]=$_POST['eaitime_'.$i];
  $i++;
  }
  }





  $tour_id=empty($_COOKIE['tour_id'])?'':$_COOKIE['tour_id'];
  $hotel_name=empty($_COOKIE['hotel_name'])?'':$_COOKIE['hotel_name'];
  $hotel_img=empty($_COOKIE['hotel_img'])?'':$_COOKIE['hotel_img'];
  $hotel_price=empty($_COOKIE['hotel_price'])?'':$_COOKIE['hotel_price'];

  $_SESSION['hotel_price']=$hotel_price;

  $flight_company=0;
  $flight_name=0;
  $flight_price=0;
  $total_price=0;
  $total_pricewithdiscount=0;
  if(empty($tour_id))
  {
  $tour_id=mt_rand();
  }
  $bookingcode="BALI" . $tour_id;
  $ltaprice=0;
  static $delete_day_status;


  $bookingdata=compact('tourlist_img','tourlist_name','tourlist_price','tourlist_date','tourlist_time',
  'place_img','place_name','place_date','place_time','eaidates','eaitimes',
  'boat_type','boat_date','boat_time','spa_type','spa_date','spa_time','meal_type','meal_date','meal_time',
  'car_type','car_date','car_time','popupguide_type','popupguide_date','popupguide_time',
  'eaiitems', 'eainums', 'spotname', 'spotdate', 'spottime', 'spotlat', 'spotlong',
  'dist', 'image','tour_id', 'hotel_name', 'hotel_img', 'hotel_price' );
  if(!empty($tour_id) && !empty($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and  tour_id=".(int)$tour_id)->meta_value))
  {
  $wpdb->query("UPDATE `wp_tourmeta` SET `meta_value` = '".serialize($bookingdata)."' where meta_key='bookingdata' and tour_id=".(int)$tour_id);
  }
  else
  {
  $wpdb->query("INSERT INTO `wp_tourmeta` (`meta_id`, `tour_id`, `meta_key`, `meta_value`) VALUES (NULL, '".$tour_id."', 'bookingdata', '".serialize($bookingdata)."')");
  }

  } */

if (!empty($_GET['bookingcode']))
    $tour_id = intval(substr($_GET['bookingcode'], 4));
else if (!empty($_COOKIE['tour_id']))
    $tour_id = $_COOKIE['tour_id'];
else
    $tour_id = '';


$can_modify_item = false;
$tp_cpn_valdty=intval(get_option('_travpart_vo_option')['tp_cpn_valdty']);
if (!empty($tour_id) && is_user_logged_in() && current_user_can('um_travel-advisor')) {
    $current_user = wp_get_current_user();
  //Bind the user with booking code or check the right to modify item
    if ($wpdb->get_var("SELECT count(*) FROM `wp_tourmeta` where meta_key='userid' and  tour_id=" . (int) $tour_id) < 1) {
        $wpdb->query("INSERT INTO `wp_tourmeta` (`meta_id`, `tour_id`, `meta_key`, `meta_value`) VALUES (NULL, '{$tour_id}', 'userid', '{$current_user->ID}')");
        $can_modify_item = true;
    } else if ($wpdb->get_var("SELECT count(*) FROM `wp_tourmeta` where meta_key='userid' and meta_value={$current_user->ID} and tour_id=" . (int) $tour_id) == 1) {
        $can_modify_item = true;
    }
  
  //Get coupon
  $coupon=$wpdb->get_row("SELECT * FROM `wp_coupons` WHERE used=0 AND `user_id` = {$current_user->ID} AND DATE_SUB(CURDATE(), INTERVAL {$tp_cpn_valdty} DAY)<=date(get_time) AND `tour_id` = ".(int)$tour_id);
}

$creater = $wpdb->get_row("SELECT meta_value FROM `wp_tourmeta` where meta_key='userid' and tour_id=" . (int) $tour_id);

if (!empty($_GET['bookingcode']) && $can_modify_item) {
    easy_setcookie('tour_id', substr($_GET['bookingcode'], 4));
}

$total_price = 0;
$total_pricewithdiscount = 0;
$ltaprice = 0;

//New method to save tour
if (!empty($_COOKIE['update']) && $_COOKIE['update'] == 1 && $can_modify_item) {
//    echo 'Tour update';

    if (!empty($_COOKIE['meal_type']) && !empty($_COOKIE['meal_date']) && !empty($_COOKIE['meal_time'])) {
        $meal_type = unescape(htmlspecialchars($_COOKIE['meal_type']));
        $meal_date = unescape(htmlspecialchars($_COOKIE['meal_date']));
        $meal_time = unescape(htmlspecialchars($_COOKIE['meal_time']));
    } else {
        $meal_type = '';
        $meal_date = '';
        $meal_time = '';
    }

    if (!empty($_COOKIE['restaurant_name']) && !empty($_COOKIE['restaurant_local']) && !empty($_COOKIE['restaurant_price']) && !empty($_COOKIE['restaurant_diners']) && !empty($_COOKIE['restaurant_date']) && !empty($_COOKIE['restaurant_time'])) {
        $restaurant_name = unescape(htmlspecialchars($_COOKIE['restaurant_name']));
        $restaurant_local = unescape(htmlspecialchars($_COOKIE['restaurant_local']));
        $restaurant_img = unescape(htmlspecialchars($_COOKIE['restaurant_img']));
        $restaurant_price = unescape(htmlspecialchars($_COOKIE['restaurant_price']));
        $restaurant_diners = unescape(htmlspecialchars($_COOKIE['restaurant_diners']));
        $restaurant_date = unescape(htmlspecialchars($_COOKIE['restaurant_date']));
        $restaurant_time = unescape(htmlspecialchars($_COOKIE['restaurant_time']));
    } else {
        $restaurant_name = '';
        $restaurant_local = '';
        $restaurant_img = '';
        $restaurant_price = '';
        $restaurant_diners = '';
        $restaurant_date = '';
        $restaurant_time = '';
    }

    if (!empty($_COOKIE['car_type']) && !empty($_COOKIE['car_date']) && !empty($_COOKIE['car_time'])) {
        $car_type = unescape(htmlspecialchars($_COOKIE['car_type']));
        $car_date = unescape(htmlspecialchars($_COOKIE['car_date']));
        $car_time = unescape(htmlspecialchars($_COOKIE['car_time']));
    } else {
        $car_type = '';
        $car_date = '';
        $car_time = '';
    }
	
    if (!empty($_COOKIE['popupguide_type']) && !empty($_COOKIE['popupguide_date']) && !empty($_COOKIE['popupguide_time'])) {
        $popupguide_type = unescape(htmlspecialchars($_COOKIE['popupguide_type']));
        $popupguide_date = unescape(htmlspecialchars($_COOKIE['popupguide_date']));
        $popupguide_time = unescape(htmlspecialchars($_COOKIE['popupguide_time']));
    } else {
        $popupguide_type = '';
        $popupguide_date = '';
        $popupguide_time = '';
    }

    if (!empty($_COOKIE['spa_type']) && !empty($_COOKIE['spa_date']) && !empty($_COOKIE['spa_time'])) {
        $spa_type = unescape(htmlspecialchars($_COOKIE['spa_type']));
        $spa_date = unescape(htmlspecialchars($_COOKIE['spa_date']));
        $spa_time = unescape(htmlspecialchars($_COOKIE['spa_time']));
    } else {
        $spa_type = '';
        $spa_date = '';
        $spa_time = '';
    }

    if (!empty($_COOKIE['boat_type']) && !empty($_COOKIE['boat_date']) && !empty($_COOKIE['boat_time'])) {
        $boat_type = unescape(htmlspecialchars($_COOKIE['boat_type']));
        $boat_date = unescape(htmlspecialchars($_COOKIE['boat_date']));
        $boat_time = unescape(htmlspecialchars($_COOKIE['boat_time']));
    } else {
        $boat_type = '';
        $boat_date = '';
        $boat_time = '';
    }

    if (!empty($_COOKIE['place_img']) && !empty($_COOKIE['place_name']) && !empty($_COOKIE['place_date']) && !empty($_COOKIE['place_time'])) {
        $place_img = $_COOKIE['place_img'];
        $place_name = unescape(htmlspecialchars($_COOKIE['place_name']));
        $place_date = unescape(htmlspecialchars($_COOKIE['place_date']));
        $place_time = unescape(htmlspecialchars($_COOKIE['place_time']));
    } else {
        $place_img = '';
        $place_name = '';
        $place_date = '';
        $place_time = '';
    }

    if (!empty($_COOKIE['tourlist_img']) && !empty($_COOKIE['tourlist_name']) && !empty($_COOKIE['tourlist_price']) && !empty($_COOKIE['tourlist_date']) && !empty($_COOKIE['tourlist_time'])) {
        $tourlist_img = unescape($_COOKIE['tourlist_img']);
        $tourlist_name = unescape(htmlspecialchars($_COOKIE['tourlist_name']));
        $tourlist_price = unescape(htmlspecialchars($_COOKIE['tourlist_price']));
        $tourlist_date = unescape(htmlspecialchars($_COOKIE['tourlist_date']));
        $tourlist_time = unescape(htmlspecialchars($_COOKIE['tourlist_time']));
    } else {
        $tourlist_img = '';
        $tourlist_name = '';
        $tourlist_price = '';
        $tourlist_date = '';
        $tourlist_time = '';
    }

    if (!empty($_COOKIE['spotname']) && !empty($_COOKIE['spotdate']) && !empty($_COOKIE['spottime'])) {
        $spotname = unescape(htmlspecialchars($_COOKIE['spotname']));
        $spotdate = unescape(htmlspecialchars($_COOKIE['spotdate']));
        $spottime = unescape(htmlspecialchars($_COOKIE['spottime']));
        $spotlat = '';
        $spotlong = '';
    } else {
        $spotname = '';
        $spotdate = '';
        $spottime = '';
        $spotlat = '';
        $spotlong = '';
    }

    if (!empty($_COOKIE['eaiitem']) && !empty($_COOKIE['eainum']) && !empty($_COOKIE['eaidate']) && !empty($_COOKIE['eaitime'])) {
        $eaiitem = unescape(htmlspecialchars($_COOKIE['eaiitem']));
        $eainum = unescape(htmlspecialchars($_COOKIE['eainum']));
        $eaidate = unescape(htmlspecialchars($_COOKIE['eaidate']));
        $eaitime = unescape(htmlspecialchars($_COOKIE['eaitime']));
    } else {
        $eaiitem = '';
        $eainum = '';
        $eaidate = '';
        $eaitime = '';
    }

    setcookie('update', '0', (time() + 86400 * 30), '/English/', 'www.travpart.com');

    $bookingdata = compact('tourlist_img', 'tourlist_name', 'tourlist_price', 'tourlist_date', 'tourlist_time', 'place_img', 'place_name', 'place_date', 'place_time', 'eaidate', 'eaitime', 'boat_type', 'boat_date', 'boat_time', 'spa_type', 'spa_date', 'spa_time', 'meal_type', 'meal_date', 'meal_time', 'car_type', 'car_date', 'car_time', 'popupguide_type', 'popupguide_date', 'popupguide_time', 'eaiitem', 'eainum', 'spotname', 'spotdate', 'spottime',  'tour_id', 'restaurant_name', 'restaurant_local', 'restaurant_img', 'restaurant_price', 'restaurant_diners', 'restaurant_diners', 'restaurant_date', 'restaurant_time');
    if (!empty($tour_id) && !empty($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and  tour_id=" . (int) $tour_id)->meta_value)) {
        $wpdb->query("UPDATE `wp_tourmeta` SET `meta_value` = '" . serialize($bookingdata) . "' where meta_key='bookingdata' and tour_id=" . (int) $tour_id);
    } else {
        $wpdb->query("INSERT INTO `wp_tourmeta` (`meta_id`, `tour_id`, `meta_key`, `meta_value`) VALUES (NULL, '" . $tour_id . "', 'bookingdata', '" . serialize($bookingdata) . "')");
    }
} else if (!empty($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=" . (int) substr($_GET['bookingcode'], 4))->meta_value)) {
    extract(unserialize($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=" . (int) substr($_GET['bookingcode'], 4))->meta_value), EXTR_OVERWRITE);
} else if (!empty($_GET['bookingcode'])) {
    $tour_id = intval(substr($_GET['bookingcode'], 4));
}

if (!empty($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='notecontents' and tour_id=" . (int) $tour_id)->meta_value))
    $notecontents = unserialize($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='notecontents' and tour_id=" . (int) $tour_id)->meta_value);

//Set cookie for the user can modify item
function easy_setcookie($key, $value) {
    setcookie($key, $value, (time() + 86400 * 30), '/English/', 'www.travpart.com');
}

if ($can_modify_item) {
    if ((empty($_COOKIE['meal_type']) && !empty($meal_type) && !empty($meal_date) && !empty($meal_time)) || $_COOKIE['meal_type'] != $meal_type) {
        easy_setcookie('meal_type', $meal_type);
        easy_setcookie('meal_date', $meal_date);
        easy_setcookie('meal_time', $meal_time);
    }

    if ((empty($_COOKIE['restaurant_name']) && !empty($restaurant_name) && !empty($restaurant_diners) && !empty($restaurant_date) && !empty($restaurant_time)) || $_COOKIE['restaurant_name'] != $restaurant_name) {
        easy_setcookie('restaurant_name', $restaurant_name);
        easy_setcookie('restaurant_local', $restaurant_local);
        easy_setcookie('restaurant_img', $restaurant_img);
        easy_setcookie('restaurant_price', $restaurant_price);
        easy_setcookie('restaurant_diners', $restaurant_diners);
        easy_setcookie('restaurant_date', $restaurant_date);
        easy_setcookie('restaurant_time', $restaurant_time);
    }

    if ((empty($_COOKIE['car_type']) && !empty($car_type) && !empty($car_date) && !empty($car_time)) || $_COOKIE['car_type'] != $car_type) {
        easy_setcookie('car_type', $car_type);
        easy_setcookie('car_date', $car_date);
        easy_setcookie('car_time', $car_time);
    }

    if ((empty($_COOKIE['popupguide_type']) && !empty($popupguide_type) && !empty($popupguide_date) && !empty($popupguide_time)) || $_COOKIE['popupguide_type'] != $popupguide_type) {
        easy_setcookie('popupguide_type', $popupguide_type);
        easy_setcookie('popupguide_date', $popupguide_date);
        easy_setcookie('popupguide_time', $popupguide_time);
    }

    if ((empty($_COOKIE['spa_type']) && empty($spa_type) && !empty($spa_date) && !empty($spa_time)) || $_COOKIE['spa_type'] != $spa_type) {
        easy_setcookie('spa_type', $spa_type);
        easy_setcookie('spa_date', $spa_date);
        easy_setcookie('spa_time', $spa_time);
    }

    if ((empty($_COOKIE['boat_type']) && !empty($boat_type) && !empty($boat_date) && !empty($boat_time)) || $_COOKIE['boat_type'] != $boat_type) {
        easy_setcookie('boat_type', $boat_type);
        easy_setcookie('boat_date', $boat_date);
        easy_setcookie('boat_time', $boat_time);
    }

    if ((empty($_COOKIE['place_name']) && !empty($place_img) && !empty($place_name) && !empty($place_date) && !empty($place_time)) || $_COOKIE['place_name'] != $place_name) {
        easy_setcookie('place_img', $place_img);
        easy_setcookie('place_name', $place_name);
        easy_setcookie('place_date', $place_date);
        easy_setcookie('place_time', $place_time);
    }

    if ((empty($_COOKIE['tourlist_name']) && !empty($tourlist_img) && !empty($tourlist_name) && !empty($tourlist_price) && !empty($_COOKIE['tourlist_date']) && !empty($_COOKIE['tourlist_time'])) || $_COOKIE['tourlist_name'] != $tourlist_name) {
        easy_setcookie('tourlist_img', $tourlist_img);
        easy_setcookie('tourlist_name', $tourlist_name);
        easy_setcookie('tourlist_price', $tourlist_price);
        easy_setcookie('tourlist_date', $tourlist_date);
        easy_setcookie('tourlist_time', $tourlist_time);
    }

    if ((empty($_COOKIE['spotname']) && !empty($spotname) && !empty($spotdate) && !empty($spottime)  || $_COOKIE['spotname'] != $spotname)) {
        easy_setcookie('spotname', $spotname);
        easy_setcookie('spotdate', $spotdate);
        easy_setcookie('spottime', $spottime);
        
    }

    if ((empty($_COOKIE['eaiitem']) && !empty($eaiitem) && !empty($eainum) && !empty($eaidate) && !empty($eaitime)) || $_COOKIE['eaiitem'] != $eaiitem) {
        easy_setcookie('eaiitem', $eaiitem);
        easy_setcookie('eainum', $eainum);
        easy_setcookie('eaidate', $eaidate);
        easy_setcookie('eaitime', $eaitime);
    }
}


$locationnames = explode(':,', $spotname);
array_walk($locationnames, 'trim_value');
$_SESSION['locationnames'] = $locationnames;

$count = count($locationnames);
easy_setcookie('location_number', $count);


/*
$locationlats = explode(':,', $spotlat);
array_walk($locationlats, 'trim_value');



$locationlongs = explode(':,', $spotlong);
array_walk($locationlongs, 'trim_value');
*/
$locationdates = explode(':,', $spotdate);
array_walk($locationdates, 'trim_value');
/* foreach($locationdates as &$t)
  {
  $t=empty($t)?'':explode('/',$t);
  $t=empty($t)?'':($t[2].'-'.$t[0].'-'.$t[1]);
  } */

$locationtimes = explode(':,', $spottime);
array_walk($locationtimes, 'trim_value');

function trim_value(&$value) {
    $value = trim($value, ":,");
}

function nl2br_save_html($string) {
    if (!preg_match("#</.*>#", $string)) // avoid looping if no tags in the string.
        return nl2br($string);

    $string = str_replace(array("\r\n", "\r", "\n"), "\n", $string);

    $lines = explode("\n", $string);
    $output = '';
    foreach ($lines as $line) {
        $line = rtrim($line);
        if (!preg_match("#</?[^/<>]*>$#", $line)) // See if the line finished with has an html opening or closing tag
            $line .= '<br />';
        $output .= $line . "\n";
    }

    return $output;
}

$pricelist = compact('tourlist_img', 'tourlist_name', 'tourlist_price', 'tourlist_date', 'tourlist_time', 'place_img', 'place_name', 'place_date', 'place_time', 'eaidates', 'eaitimes', 'boat_type', 'boat_date', 'boat_time', 'spa_type', 'spa_date', 'spa_time', 'meal_type', 'meal_date', 'meal_time', 'car_type', 'car_date', 'car_time', 'popupguide_type', 'popupguide_date', 'popupguide_time', 'eaiitems', 'eainums', 'spotname', 'spotdate', 'spottime', 'dist', 'image', 'tour_id', 'hotel_name', 'hotel_img', 'hotel_price');



$connection = mysqli_connect('localhost', $username, $password, $database);

// Check connection
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}


//echo $hotel_img;

$ready = '';
$fail = '';

for ($i = 0; $i < $count; $i++) {
    if (!empty($locationnames[$i]) && !empty($locationdates[$i]) && !empty($locationtimes[$i])) {
        $data[$i] = array($locationnames[$i],
            $locationdates[$i],
            $locationtimes[$i],
            $dist,
            $tour_id,
            $hotel_img,
            $hotel_name,
            $hotel_price,
            $flight_company,
            $flight_name,
            $flight_price,
            $total_price,
            $total_pricewithdiscount,
            $bookingcode,
            $userid,
            serialize($pricelist));
    }
}


$data = array_filter($data);
//echo "<pre>";
//print_r($data);
//echo "tourimage" . $image;


/*
$stmt = $connection->prepare(
        "INSERT INTO `touristspots`(
    `name`,
    `date`,
    `time`,
    `dist`,
    `tourid`,
    `hotelimg`,
    `hotelname`,
    `hotelprice`,
    `flightcompany`,
    `flightname`,
    `flightprice`,
    `totalprice`,
    `totalpricewithdiscount`,
    `bookingcode`,
    `userid`,
    `pricelist`
    )
    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

//print_r($stmt);
// Check if prepare() failed.
if (false === $stmt) {
    echo 'prepare() failed: ' . htmlspecialchars($stmt->error);
    trigger_error($connection->error, E_USER_ERROR);
}


$connection->query("START TRANSACTION");


foreach ($data as $row) {
// Bind parameters. Types: s = string, i = integer, d = double,  b = blob
    $bind = $stmt->bind_param("sssdddisssssssssss", $row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7], $row[8], $row[9], $row[10], $row[11], $row[12], $row[13], $row[14], $row[15], $row[16], $row[17]
    );
    // print_r($bind);   
    // Check if bind_param() failed.
    if (false === $bind) {
        echo 'bind_param() failed: ' . htmlspecialchars($stmt->error);
    }

    $exec = $stmt->execute();


    if (!$stmt->execute()) {
        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
    }

    // Check if execute() failed.
    if (false === $exec) {
        $fail .= sprintf("%s will not be inserted because execute() failed: %s<br/>", $row[0], htmlspecialchars($stmt->error));
    } else {
        $ready .= sprintf("%s will be inserted in database.<br />", $row[0]);
    }
}

//printf("rows inserted: %d\n", $stmt->affected_rows);
// Close the prepared statement
$stmt->close();

$commit = $connection->query('COMMIT');

if (false === $commit) {
    echo "Transaction commit failed<br />";
}
*/

//getting bookingusercode from logged in user
$bookingusercode = $_GET['bookingusercode'];

if (!empty($bookingusercode) && ($userid != "guest")) {
    $userdata = "SELECT pricelist FROM `finalitinerary` where bookingcode='{$tour_id}'";
    $retuserdata = mysqli_query($connection, $userdata);
    while ($finalserial = mysqli_fetch_array($retuserdata, MYSQL_ASSOC)) {
        $unserial = $finalserial;
    }
    foreach ($unserial as $t) {
        //echo $t;
        //die();
        $ExtraActivity = unserialize($t);
    }
    $eainums = $ExtraActivity['eainums'];
    $eaiimgs = $ExtraActivity['eaiimgs'];
    $eaiitems = $ExtraActivity['eaiitems'];
    $eaiprices = $ExtraActivity['eaiprices'];
    $eaidates = $ExtraActivity['eaidates'];
    $eaitimes = $ExtraActivity['eaitimes'];
    $place_img = $_POST['place_img'];
    $place_name = $ExtraActivity['place_name'];
    $place_date = $ExtraActivity['place_date'];
    $place_time = $ExtraActivity['place_time'];
    $tourlist_img = $ExtraActivity['tourlist_img'];
    $tourlist_name = $ExtraActivity['tourlist_name'];
    $tourlist_price = $ExtraActivity['tourlist_price'];
    $tourlist_date = $ExtraActivity['tourlist_date'];
    $tourlist_time = $ExtraActivity['tourlist_time'];
    $meal_type = $ExtraActivity['meal_type'];
    $meal_date = $ExtraActivity['meal_date'];
    $meal_time = $ExtraActivity['meal_time'];
    $car_type = $ExtraActivity['car_type'];
    $car_date = $ExtraActivity['car_date'];
    $car_time = $ExtraActivity['car_time'];
    $popupguide_type = $ExtraActivity['popupguide_type'];
    $popupguide_date = $ExtraActivity['popupguide_date'];
    $popupguide_time = $ExtraActivity['popupguide_time'];
    $spa_type = $ExtraActivity['spa_type'];
    $spa_date = $ExtraActivity['spa_date'];
    $spa_time = $ExtraActivity['spa_time'];
    $boat_type = $ExtraActivity['boat_type'];
    $boat_date = $ExtraActivity['boat_date'];
    $boat_time = $ExtraActivity['boat_time'];
}


/*
if (!empty($bookingusercode) && ($userid != "guest")) {
    $sql2 = "SELECT * from `finalitinerary` where bookingcode='" . $bookingusercode . "' and userid='" . $userid . "' and name<>'' group by `date`";
    $retval2 = mysqli_query($connection, $sql2);
    if (!$retval2) {
        die('Could not get data: ' . mysqli_error());
    }
} else {
    $sql2 = "SELECT * from `touristspots` group by `date`";
    $retval2 = mysqli_query($connection, $sql2);
    if (!$retval2) {
       die('Could not get data:' . mysqli_error());
    }
}*/
?>

<div class="marvelapp-grp">
    <!--  <div class="">-->
    <div class="col-xs-12 col-md-12 col-sm-12 marvelapp-1a">
        <link href="/English/wp-content/themes/bali/css/utilities.css" rel="stylesheet" type="text/css">
        <link href="/English/wp-content/themes/bali/css/tour-details.css?1" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

        <div class="backtoform">

            <div class="d-flex flex-column flex-sm-row mb-4 book_mar_mob">

                <div class=" booking_code_desk order-sm-last ml-sm-auto mb-3 mb-sm-0">
                    <?php
                    if (isset($_SERVER['HTTP_REFERER'])) {
                        $a = $_SERVER['HTTP_REFERER'];
                    }
                    ?>
                    <!-- <a href="<?php //echo home_url().'/travcust';             ?>"><span>Travcust</span></a> >
                <span>Tourist details</span>
      </div> -->


                    <div class="booking-code text-white">
                        <?php
                        if (!empty($bookingusercode) && ($userid != "guest")) {
                            $lunch_per_serve = "Lunch Per Serve";
                            $tourguide_per_day = "Tour Guide Per Day";
                            $insurance_per_person = "Insurance Per Person";
                            $_6seats = "6 Seats";
                            $_12seats = "12 Seats";
                            $_15seats = "15 Seats";
                            $_16seats = "16 Seats";
                            $_19seats = "19 Seats";
                            $_28seats = "28 Seats";
                            $_29seats = "29 Seats";
                            $_31seats = "31 Seats";
                            $_35seats = "35 Seats";
                            $_45seats = "45 Seats";
                            $_59seats = "59 Seats";
                            $halfday = "Half Day";
                            $baliisland = "Bali Island";
                            $balitootherisland = "Bali to other Island";
                            $javaisland = "Java Island";
                            ?>
                            <h2 class="text-white">Booking Code:<span><?php echo $bookingusercode; ?></span></h2>
                        <?php } else { ?>
                            <h2 class="text-white">Booking Code:<span class="text-white p-0">BALI<?php echo $tour_id; ?></span></h2>
                        <?php } ?>
                    </div>
                </div>
                  <style>
                .step-header {
                  border-radius: 3px;
                  border: solid 1px #dcdcdc;
                  background-color: #ffffff;
                  padding: 9px 10px;
                  padding-left: 58px;
                  position: relative;
                  cursor: pointer;
          display: none;
                }

                .step-header-title {
                  font-size: 18px;
                  font-weight: bold;
                  letter-spacing: -0.5px;
                  text-align: left;
                  color: #1a6d84; }

                .step-header-des {
                  font-size: 16px;
                  letter-spacing: -0.5px;
                  color: #666666; }

                .step-header-num {
                  position: absolute;
                  left: 10px;
                  top: 0;
                  background: url("https://www.travpart.com/English/wp-content/themes/bali/images/step-header-num.png") center center no-repeat;
                  width: 39px;
                  height: 46px;
                  background-size: contain;
                  display: flex;
                  align-items: center;
                  justify-content: center;
                  font-size: 24px;
                  color: #fff;
                  font-weight: bold; 
                  }
.backtoform{
	padding-top: 10px;
}
.customer_sat h1{
    font-size: 33px !important;
    text-align: center;
    padding-top: 10px;
    margin: 0px;
    font-family: 'Heebo', sans-serif;
    font-weight: bold;
}
.customer_sat{
	background: #ececec;
}
.bg-org-0{
	background: #66cccc !important;	
}
.bg-org-1 {
    background: #22b14c !important;
}
              </style>
                  <div class="flex-fill mr-sm-3 customer_sat">
                  	<h1>CUSTOMER SATISFACTION AGREEMENT</h1>
                    <div class="step-header">
                      <div class="step-header-num">2</div>
                      <div class="step-header-title">Step 2</div>
                      <div class="step-header-des">This is the page where you can recheck your travel price detail</div>
                    </div>
                  </div>
            </div>

            <?php
            $show_top_button = false;
            if (is_user_logged_in()) {
                $current_user = wp_get_current_user();
                $urow = $wpdb->get_row("SELECT userid,access FROM `user` WHERE `username`='{$current_user->user_login}'");
                if ((!empty($urow->access) && $urow->access == 1) || current_user_can('um_travel-advisor'))
                    $show_top_button = true;
            }
            ?>
      <?php if ($show_top_button) { ?>
            <div class="topbarmenu-outer" style="position: fixed; top: 5px; left:130px;z-index: 199999;margin-left: 160px;">
                <div class="topbarmenu">
                    
                        <?php
                        // tooltip conflict /English/wp-content/plugins/miniorange-login-openid
                        ?>
                        <a href="<?php echo site_url(); ?>/travcust" class="btn btn-primary firstbackbtn"  data-toggle="tooltip-p" title="delete and/or add more travel item"><span>Modify this tour package</span> </a>

                        <div style="display: inline;">
                            <a  style="border:3px solid #226d82!important; background:#4ec3b7!important;cursor:auto;"  <?php echo empty($tour_id) ? 'data-toggle="tooltip-p" title="You have not added anything yet"' : 'data-toggle="tooltip-p" title="Check my journey"'; ?>  class="btn btn-primary firstbackbtn">
                                <span><?php echo empty($tour_id) ? '' : ("(Booking Code BALI{$tour_id})"); ?> <img src="https://www.travpart.com/English/wp-content/uploads/2018/12/search.png" alt="Check my Journey" width="20"></span><!--<span class="tooltiptext">Check my journey</span>-->
                            </a>
                        </div>

                        <div  style="display: inline;">
                            <a href="<?php echo get_template_directory_uri() . '/newtour.php'; ?>" data-toggle="tooltip-p" title="Create a new tour package" class="btn btn-primary firstbackbtn" style="text-decoration: none">
                                <img src="https://www.travpart.com/English/wp-content/uploads/2018/11/add-list.png" width="20">  <!--<span class="tooltiptext">Create a new tour package</span>--> </a>
                        </div>
                </div>
            </div>
      <?php } ?>
      
      <?php if($show_top_button) { ?>
      <div class="steps-widget">
        <a class="steps-widget-close"><i class="fa fa-times"></i> </a>
        <div class="steps-item">
          <div class="steps-item-num">1</div>
          <div class="steps-item-title">Step 1</div>
          <div class="steps-item-des">This is the page where you add your travel items to the cart</div>
        </div>
        <div class="steps-item active">
          <div class="steps-item-num">2</div>
          <div class="steps-item-title">Step 2</div>
          <div class="steps-item-des">This is the page where you can recheck your travel price detail</div>
        </div>
        <div class="steps-item">
          <div class="steps-item-num">3</div>
          <div class="steps-item-title">Step 3</div>
          <div class="steps-item-des">This is the page where you can write your itinerary schedule and add pictures/videos</div>
        </div>
      </div>
      <script>
      jQuery('.step-header').show();
      jQuery('.steps-widget-close').on('click',function (e) {
        e.preventDefault();
        jQuery('.steps-widget').hide();
      });
      jQuery('.step-header').on('click',function (e) {
        e.preventDefault();
        jQuery('.steps-widget').show();
      });
      </script>
      <?php } ?>


            <div class="v2-tour-page">

        
<script>
	jQuery(function ($) {
    		// Restricts input for the given textbox to the given inputFilter.
			function setInputFilter(textbox, inputFilter) {
			  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
			    textbox.addEventListener(event, function() {
			      if (inputFilter(this.value)) {
			        this.oldValue = this.value;
			        this.oldSelectionStart = this.selectionStart;
			        this.oldSelectionEnd = this.selectionEnd;
			      } else if (this.hasOwnProperty("oldValue")) {
			        this.value = this.oldValue;
			        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
			      } else {
			        this.value = "";
			      }
			    });
			  });
			}
			
			setInputFilter(document.getElementById("bg_white"), function(value) {return /^-?\d*$/.test(value); });               	
                    	$('.bg_white').keyup(function(e){
                    		var keyCode = e.which ? e.which : e.keyCode;
                    		var value_p = Math.abs(parseInt( $('.bg_white').val()));
							//alert(value_p)                    		
                    		if(isNaN(value_p))
                    		{
                    			//alert('if');
							}else{
								if(value_p!='' || value_p!=null){
		                    		$('.data-tour-peoples').text(value_p);
									$('#tour-package-peoples,.target-people').show();
									$('.custom_row_tour_details').hide();
									
								}else{
									$('.data-tour-peoples').text('');
									$('#tour-package-peoples,.target-people').hide();
								}
							}
						
                    	});
                    	
                    	$('.open-tour-day-popup1').click(function(){
                    		$('#tour-package-peoples,.target-people').hide();
							$('.custom_row_tour_details').show();
                    	});
                    	
                    	
                    	$('.open-tour-people-popup').on('click', function (e) {
                            e.preventDefault();
                            $('#travcust-peoples-popup').show();
                        });
                    	
                        var savedPopupTemplate = '<div id="travcust-saved-popup" class="cm-popup saved-popup" style="display: none;"><div class="cm-popup-content"><div class="cm-popup-inner"> <i class="cm-close" onclick="jQuery(\'#travcust-saved-popup\').hide();"><i class="far fa-times-circle"></i></i><div class="cm-popup-head"> <img src="https://www.travpart.com/English/wp-content/themes/bali/images/saved-popup-img.png" alt=""></div></div></div></div>';
                        $('body').append(savedPopupTemplate);
                        $('.saveitinerary').on('click', function (e) {
                            e.preventDefault();
                            $('#travcust-saved-popup').show();
			              var data = {
			                'action': 'saveitinerary',
			                'tour_id': <?php echo $tour_id; ?>
			              };
			              
			              jQuery.post('<?php echo admin_url("admin-ajax.php"); ?>', data, function (response) {
			                if (response.error_msg != undefined && response.error_msg.length > 1) {
			                  console.log("save tour to profile tab success");
			                }}, 'json');
                        });

                        var peoplePopupTemplate = '<div id="travcust-peoples-popup" class="cm-popup" style="display: none;"><div class="cm-popup-content"><div class="cm-popup-inner"> <i class="cm-close" onclick="jQuery(\'#travcust-peoples-popup\').hide();"><i class="far fa-times-circle"></i></i><div class="cm-popup-head"> <img src="https://www.travpart.com/English/wp-content/themes/bali/images/journey-people-l.png" alt=""></div><div class="cm-popup-title-blue">please enter the number of passengers</div><div class="cm-popup-form"> <input id="inputTourPackagePeoples" class="form-control" type="number" min="1" max="30" value="" placeholder="only type numbers"> <button class="btn bg-org-0">OK</button></div></div></div></div>';
                        $('body').append(peoplePopupTemplate);
                        

                        $('.btn,cm-close', '#travcust-peoples-popup').on('click', function (e) {
                            e.preventDefault();
                            $('#travcust-peoples-popup').hide();
                            var peoples = Math.abs(parseInt($('#inputTourPackagePeoples').val()));
                            $('#inputTourPackagePeoples').val(peoples)

                            if (peoples) {
                                $('#tour-package-no-people').hide();
                                $('.data-tour-peoples').text(peoples);
                                $('.bg_white ').text(peoples);
                                $('#tour-package-peoples,.target-people').show();
                            } else {
                                $('#tour-package-no-people').show();
                                $('.data-tour-peoples').text('');
                                $('#tour-package-peoples,.target-people').hide();
                            }
                        });
                        $('.v2-header').on('click', function (e) {
                            $(this).next('.v2-box').toggle();
                        });

                    });
                </script>
                
                <div class="custom_row_tour_details ">
                	<div class="display_table_1">
						<img style="width: auto;height: 65px;" src="https://www.travpart.com/English/wp-content/themes/bali/images/journey-people-l.png" alt="">
                	</div>
                	<div class="display_table text-center">
                		<div class="font-lg1">Please enter the number of Passengers</div>
                	</div>
                	<div class="display_table">
                		<input type="text" value="1" id="bg_white" class="bg_white oopen-tour-people-popup" />
                	</div>
                    <div class="display_table_mob">
                        <div class="booking_code_mob order-sm-last ml-sm-auto mb-3 mb-sm-0">
                    <?php
                    if (isset($_SERVER['HTTP_REFERER'])) {
                        $a = $_SERVER['HTTP_REFERER'];
                    }
                    ?>
                    <!-- <a href="<?php //echo home_url().'/travcust';             ?>"><span>Travcust</span></a> >
                <span>Tourist details</span>
      </div> -->


                    <div class="booking-code text-white">
                        <?php
                        if (!empty($bookingusercode) && ($userid != "guest")) {
                            $lunch_per_serve = "Lunch Per Serve";
                            $tourguide_per_day = "Tour Guide Per Day";
                            $insurance_per_person = "Insurance Per Person";
                            $_6seats = "6 Seats";
                            $_12seats = "12 Seats";
                            $_15seats = "15 Seats";
                            $_16seats = "16 Seats";
                            $_19seats = "19 Seats";
                            $_28seats = "28 Seats";
                            $_29seats = "29 Seats";
                            $_31seats = "31 Seats";
                            $_35seats = "35 Seats";
                            $_45seats = "45 Seats";
                            $_59seats = "59 Seats";
                            $halfday = "Half Day";
                            $baliisland = "Bali Island";
                            $balitootherisland = "Bali to other Island";
                            $javaisland = "Java Island";
                            ?>
                            <h2 class="text-white">Booking Code:</br><span><?php echo $bookingusercode; ?></span></h2>
                        <?php } else { ?>
                            <h2 class="text-white">Booking Code:</br><span class="text-white p-0">BALI<?php echo $tour_id; ?></span></h2>
                        <?php } ?>
                    </div>
                </div>
                    </div>
                </div>

                <!--<div id="tour-package-no-people" class="text-white tour-package-no open-tour-people-popup mb-4">
                    <img src="https://www.travpart.com/English/wp-content/themes/bali/images/journey-people-l.png" alt="" width="242">
                    <div class="font-lg p-2 mr-auto">please enter the number of passengers</div>
                </div>-->

                <div id="tour-package-peoples" class="text-white  mb-2 open-tour-people-popup1" style="display:none;">
                    <div class="d-flex bg-org-0  text-white align-items-center border-radius">
                        <div class="border-radius bg-org-1 p-1 pr-2 pl-2 font-xl"><i class="fas fa-clock fa-fw"></i> </div>
                        <div class="font-lg p-2 pr-3 pl-3 mr-auto">You Created: <strong><span class="data-tour-peoples">2</span> passengers</strong></div>
                        <button class="btn p-2 pr-3 pl-3 btn-none text-white open-tour-day-popup1"><i class="fa fa-edit font-lg"></i></button>
                    </div>
                </div>


                <!--Place-->
                <?php
//var_dump($locationdates, $locationtimes, $locationnames);
                // demo data
                /*if(empty($locationnames[0])) {
                    $locationnames[0]= 'demo location names';
                    $locationdates[0] = '3';
                    $locationtimes[0] = '15:00 00';
                }*/
                for ($i = 0; !empty($locationnames[$i]) && !empty($locationdates[$i]) && !empty($locationtimes[$i]); $i++) {
                    ?>
                    <div class="v2-box mb-3">
                        <div class="d-flex flex-column flex-sm-row align-items-center">
                            <div class="mb-2 box-grid-50">
                                <img src="/English/wp-content/uploads/2019/06/map.png" width="538" alt="" class="img-responsive">
                            </div>
                            <div class="box-grid-50  pl-sm-3">
                                <h4 class="v2-title"><?php echo $locationnames[$i]; ?></h4>
                                <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i> <?php echo $locationnames[$i]; ?><!-- – <a href="#"> Displayed on the map</a> (100 m away from central area) --></p>
                                <p class="mb-3">
                                    <?php if ($can_modify_item) { ?>
                                        <textarea class="notecontent" rows="4" cols="50" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                    <?php } else { ?>
                                        <?php if (!empty($notecontents[$nindex])) echo 'Note: ' . $notecontents[$nindex]; ?>
                                        <?php
                                    }
                                    $nindex++;
                                    ?>
                                </p>
                                <p class="mb-2"><i class="fa fa-calendar-alt mr-1 text-blue-0"></i><?php
                                    //echo $locationdates[$i] . ' ' . $locationtimes[$i];
                                     echo is_numeric($locationdates[$i])?('Day '.$locationdates[$i]):$locationdates[$i]; ?></p>
                                <!--div class="d-flex d-row">
                                    <div class="mb-2 mr-3"><i class="fa fa-user mr-1 text-blue-0"></i>2 adults </div>
                                    <div class="mb-2"><i class="fa fa-female mr-1 text-blue-0"></i>  1 children</div>
                                </div>
                                <div><i class="fas fa-clock text-blue-0"></i> 3 days, 2 night</div-->


                            </div>
                        </div>
                    </div>
                <?php } ?>


                <!--Hotel-->
				<?php
                        $tourinfos = $wpdb->get_results("SELECT * FROM `wp_hotel` WHERE `tour_id` = " . (int) $tour_id);
                        $hotel_total_price = 0; 
                		if(count($tourinfos)>0){
				?>
                        
                <div class="d-flex bg-blue-3  text-white align-items-center border-radius v2-header mb-2 holel_tour_tab">
                    <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl"><i class="fa fa-hotel  fa-fw"></i> </div>
                    <div class="font-lg p-2 pr-3 pl-3 mr-auto">Hotel</div>
                    <button onclick="clickCounter()" id="btnHotel" class="btn p-2 pr-3 pl-3 btn-none text-white l"><i class="fa fa-minus font-lg hotel_icon"></i></button>
                </div>
                <div class="v2-box mb-3 hotel_item_box">

                    <div class="v2-list">
                        
                       <?php foreach ($tourinfos as $tourinfo) {
                            $has_hotel = true;
                            $night = round((strtotime($tourinfo->end_date) - strtotime($tourinfo->start_date)) / 86400);
                            $hotel_total_price += $tourinfo->total;
                            $total_price += round($tourinfo->total / get_option("_cs_currency_USD") / get_option('person_price_ration'));
                            ?>
                            <div class="v2-list-item d-flex flex-column flex-sm-row">
                                <div class="v2-list-left mb-3 mr-sm-3">
                                    <div class="thumb-wrap">
                    <div class="slider">
                    <?php if(unserialize($tourinfo->img)) {
                      $hotel_imgs=unserialize($tourinfo->img);
                      if (!empty($tourinfo->regionid)) { ?>
                                            <a target=_blank href="https://www.travpart.com/hotel-and-flight-bookings/?tour_id=<?php echo $tour_id; ?>&travcust=1#/hotel-information/<?php echo $tourinfo->regionid; ?>/?fn=hotelInfo&checkIn=<?php echo date('m-d-Y', strtotime($tourinfo->start_date)); ?>&checkOut=<?php echo date('m-d-Y', strtotime($tourinfo->end_date)); ?>&language=en_US&currency=USD&hotelType=1&rooms=1&adults=1&childs=0&is_custom=0">
                                                <img src="<?php echo $hotel_imgs[0]; ?>" width="180" alt="" class="img-responsive" />
                                            </a>
                                        <?php } else { ?>
                                            <img src="<?php echo $hotel_imgs[0]; ?>" width="180" alt="" class="img-responsive" />
                                        <?php }
                      for($i=1; $i<count($hotel_imgs); $i++) { ?>
                      <img src="<?php echo $hotel_imgs[$i]; ?>" width="180" alt="" class="img-responsive" />
                    <?php }
                      } else {
                        if (!empty($tourinfo->regionid)) { ?>
                                            <a target=_blank href="https://www.travpart.com/hotel-and-flight-bookings/?tour_id=<?php echo $tour_id; ?>&travcust=1#/hotel-information/<?php echo $tourinfo->regionid; ?>/?fn=hotelInfo&checkIn=<?php echo date('m-d-Y', strtotime($tourinfo->start_date)); ?>&checkOut=<?php echo date('m-d-Y', strtotime($tourinfo->end_date)); ?>&language=en_US&currency=USD&hotelType=1&rooms=1&adults=1&childs=0&is_custom=0">
                                                <img src="<?php echo $tourinfo->img; ?>" width="180" alt="" class="img-responsive" />
                                            </a>
                                        <?php } else { ?>
                                            <img src="<?php echo $tourinfo->img; ?>" width="180" alt="" class="img-responsive" />
                                        <?php }
                      } ?>
                    </div>
                                        <!-- <div class="slider">
                                        <img src="https://www.ing.be/assets/nuid/images/767/expatmagazine-767.jpg" /><img src="https://www.ing.be/Assets/nuid/images/767/personal-banking-767.jpg" /><img src="https://www.ing.be/Assets/nuid/images/767/alltransferagreement-767.jpg" /><div><img src="https://www.ing.be/Assets/nuid/images/767/prepare-arrival-767.jpg" /></div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="v2-list-right flex-grow-1">
                                    <div class="d-flex flex-row">
                    <?php if($can_modify_item) { ?>
                                        <div class="v2-actions order-last">
                                            <a item="<?php echo $tourinfo->id; ?>" class="hotel-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                            <!--a href="#" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a-->
                                        </div>
                    <?php } ?>
                                        <div class="mr-auto">
                                            <h4 class="v2-title mt-0 mb-3"><i class="fa fa-landmark text-gray-0"></i>
                                                <?php if (!empty($tourinfo->regionid)) { ?>
                                                    <a target=_blank href="https://www.travpart.com/hotel-and-flight-bookings/?tour_id=<?php echo $tour_id; ?>&travcust=1#/hotel-information/<?php echo $tourinfo->regionid; ?>/?fn=hotelInfo&checkIn=<?php echo date('m-d-Y', strtotime($tourinfo->start_date)); ?>&checkOut=<?php echo date('m-d-Y', strtotime($tourinfo->end_date)); ?>&language=en_US&currency=USD&hotelType=1&rooms=1&adults=1&childs=0&is_custom=0">
                                                        <?php echo $tourinfo->name; ?>
                                                    </a>
                                                    <?php
                                                } else {
                                                    echo $tourinfo->name;
                                                }
                                                ?>
                                            </h4>
    <!--p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i>  Bali Island Kuta – <a href="#">Displayed on the map</a>  (100 m away from central area) </p-->
                                        </div>
                                    </div>
                                    <!--p class="mb-4">The Besavana Phuket hotel is located in old Phuket Town. This three star hotel offers free WiFi, 90 meters away from Thai Hua Museum Museum, and is Chinpracha away from Chinpracha....</p-->
                                    <div class="v2-tb font-xsmall">
                                        <div class="v2-tb-row p-1">
                                            <div class="d-flex flex-row ">
                                            <?php if($can_modify_item) { ?>
                                                <div class="p-2 flex-grow-1  max-250">
                                                    <a href="<?php echo $i; ?>" class="fa fa-trash text-blue-0 hotel-item-trash"></a> <?php echo $tourinfo->room_type; ?>
                                                </div>
                                            <?php } ?>

                                                <div class="flex-grow-1">
                                                    <div class="d-flex flex-row justify-content-between">
                                                        <div class="p-2"><a href="#" class="fa fa-calendar-alt text-blue-0"></a> <?php echo date("M j, Y", strtotime($tourinfo->start_date)) . " - " . date("M j, Y", strtotime($tourinfo->end_date)); ?></div>
                                                        <div class="p-2 font-small text-blue-3 font-weight-normal"><span class="change_price" or_pic="<?php echo empty($tourinfo->total) ? '0' : round($tourinfo->total / $night / get_option("_cs_currency_USD")); ?>"> Rp <span class="price_span"> <?php echo empty($tourinfo->total) ? '0' : number_format(round($tourinfo->total / $night / get_option("_cs_currency_USD")), 2); ?> </span></span>/night</div>
                                                        <div class="p-2"><span class="change_price" or_pic="<?php echo empty($tourinfo->total) ? '0' : round($tourinfo->total / get_option("_cs_currency_USD")); ?>"> Rp <span class="price_span"> <?php echo empty($tourinfo->total) ? '0' : number_format(round($tourinfo->total / get_option("_cs_currency_USD")), 2); ?> </span></span></div>
                                                        <!--div class="p-2">Remarks</div-->
                                                    </div>
                                                    <?php if ($can_modify_item || !empty($notecontents[$nindex])) { ?>
                                                        <div class="v2-input d-flex flex-row align-items-center mb-1">
                                                            <?php if ($can_modify_item) { ?>
                                                                <div  class="prefix"><i class="fa fa-pen text-gray-0"></i> </div>
                                                                <textarea class="notecontent form-control" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                                            <?php } else { ?>
                                                                <p><?php echo 'Note: ' . $notecontents[$nindex]; ?></p>
                                                            <?php } ?>
                                                        </div>
                                                        <?php
                                                    }
                                                    $nindex++;
                                                    ?>

                                                </div>
                                            </div>
                                        </div>
                                        <?php /*
                                          <div class="v2-tb-row p-1">
                                          <div class="d-flex flex-row ">
                                          <div class="p-2 flex-grow-1  max-250"><a href="#" class="fa fa-trash text-blue-0"></a>  Room type B </div>
                                          <div class="flex-grow-1">
                                          <div class="d-flex flex-row justify-content-between">
                                          <div class="p-2"><a href="#" class="fa fa-calendar-alt text-blue-0"></a> October 30, 2018 -November 1st</div>
                                          <div class="p-2 font-small text-blue-3 font-weight-normal">$ 748/night</div>
                                          <div class="p-2">$ 1,496</div>
                                          <div class="p-2">Remarks</div>
                                          </div>
                                          <div class="v2-input d-flex flex-row align-items-center mb-1">
                                          <div  class="prefix"><i class="fa fa-pen text-gray-0"></i> </div>
                                          <input class="form-control" placeholder="Add Remarks/ enter customer name">
                                          </div>
                                          </div>
                                          </div>
                                          </div>
                                          <div class="v2-tb-row p-1">
                                          <div class="d-flex flex-row ">
                                          <div class="p-2 flex-grow-1  max-250"><a href="#" class="fa fa-trash text-blue-0"></a>  Room type A </div>
                                          <div class="flex-grow-1">
                                          <div class="d-flex flex-row justify-content-between">
                                          <div class="p-2"><a href="#" class="fa fa-calendar-alt text-blue-0"></a> October 30, 2018 -November 1st</div>
                                          <div class="p-2 font-small text-blue-3 font-weight-normal">$ 748/night</div>
                                          <div class="p-2">$ 1,496</div>
                                          <div class="p-2">Remarks</div>
                                          </div>

                                          </div>
                                          </div>

                                          </div>
                                         */ ?>

                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="v2-foot text-right p-3 pr-sm-5">
          <?php
          if(!empty($coupon) && $coupon->type=='hotel') {
            ?>
            <p>Total Cost <span class="change_price" or_pic="<?php echo empty($hotel_total_price) ? '0' : round($hotel_total_price / get_option("_cs_currency_USD")); ?>"> Rp <span class="price_span"> <?php echo empty($hotel_total_price) ? '0' : number_format(round($hotel_total_price / get_option("_cs_currency_USD")), 2); ?> </span></span></p>
            <?php
            if($coupon->discount=='$') {
              $discount=$coupon->discount.$coupon->figure;
              if($hotel_total_price<$coupon->figure) {
                $discount_rp=round($hotel_total_price/get_option("_cs_currency_USD")/get_option('person_price_ration'));
                $total_price-=$discount_rp;
                $hotel_total_price=0;
              }
              else {
                $discount_rp=$coupon->figure;
                $hotel_total_price-=$discount_rp;
                $total_price-=round($discount_rp/get_option("_cs_currency_USD")/get_option('person_price_ration'));
              }
            }
            else {
              $discount=$coupon->figure.$coupon->discount;
              $discount_rp=$hotel_total_price*$coupon->figure/100;
              $hotel_total_price-=$discount_rp;
              $total_price-=round($discount_rp/get_option("_cs_currency_USD")/get_option('person_price_ration'));
            }
            $wpdb->query("UPDATE `wp_coupons` SET discount_rp={$discount_rp} WHERE id={$coupon->id}");
            ?>
            <p>Discount -<?php echo $discount; ?></p>
            Net cost.
          <?php } else { ?>
            Total: 
          <?php } ?>
            <strong><span class="change_price" or_pic="<?php echo empty($hotel_total_price) ? '0' : round($hotel_total_price / get_option("_cs_currency_USD")); ?>"> Rp <span class="price_span"> <?php echo empty($hotel_total_price) ? '0' : number_format(round($hotel_total_price / get_option("_cs_currency_USD")), 2); ?> </span></span></strong>
                        <?php
                        if ($hotel_total_price >= 0 && $can_modify_item) {
                            easy_setcookie('hotel_price', $hotel_total_price);
                            setcookie('hotel_price', $hotel_total_price, (time() + 86400 * 30), '/hotel-and-flight-bookings/', 'www.travpart.com');
                        }
                        ?>
                    </div>


                </div>
                <?php 
                }
                
                ?>
                <div class="d-flex bg-blue-3 optional_location text-white align-items-center border-radius v2-header mb-2 optional_loc_tab">
                    <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl">
                    <i class="fa fa-map  fa-fw"></i> </div>
                    <div class="font-lg p-2 pr-3 pl-3 mr-auto">Optional Location</div>
                    <button onclick="clickCounter()" id="btnHotel" class="btn p-2 pr-3 pl-3 btn-none text-white l"><i class="fa fa-minus font-lg op_loc_icon"></i></button>
                </div>
                <div class="v2-box mb-3 op_loc_item_box">
                    <div class="v2-list">
                    	<div class="row div_d">
                    		<!--<div class="list_location">
                    			<div class="map_div">
                    				 
									  <a 
    href="https://maps.google.com/maps?q=5.965754,26,26.103516&hl=es;z=5&amp;output=embed" 
    style="color:#0000FF;text-align:left" 
    target="_blank"
   ><iframe 
									  width="200" 
									  height="120" 
									  frameborder="0"
									  scrolling="no" 
									  marginheight="0" 
									  marginwidth="0" 
									  src="https://maps.google.com/maps?q=5.965754,26.103516&hl=es&z=5&amp;output=embed"
									 ></iframe>
   </a>
                    			</div>
                    			<div class="hotel_map">
                    				<h1><i class="fas fa-utensils fa-fw"></i> Tao Asian Bistro</h1>
                    				<p><i class="fas fa-map-pin"></i> The saga, Las Vegas</p>
                    			</div>
                    			<div class="date_d">
                    				<i class="fas fa-calendar-week"></i> Day 2 
                    				<i class="fa fa-clock"></i>
                    				<br >1:00 PM
                    			</div>
                    		</div>-->
                    	</div>
                	</div>
                </div>
                <!-- meal section -->
                <?php
                if (!empty($meal_type) || !empty($restaurant_name)) {
                    $has_activity = true;
                    $meal_types = explode(',', unescape($meal_type));
                    $meal_dates = explode(',', unescape($meal_date));
                    $meal_times = explode(',', unescape($meal_time));
                    $meal_total_price = 0;
                    ?>

                    <div class="d-flex bg-blue-3  text-white align-items-center border-radius v2-header mb-2 dining_arr_tab">
                        <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl"><i class="fas fa-concierge-bell fa-fw"></i> </div>
                        <div class="font-lg p-2 pr-3 pl-3 mr-auto">Dining Arrangements</div>
                        <button onclick="clickCounter()" class="btn p-2 pr-3 pl-3 btn-none text-white"><i class="fa fa-minus font-lg dining_arr_icon"></i></button>
                    </div>
                    <div class="meal v2-box mb-3 dining_arr_item_box">

                        <!-- Meal --> 
                        <?php if (!empty($meal_type)) { ?>
                            <div class="v2-list">
                                <?php
                                for ($i = 0; $i < count($meal_types) - 1; $i++) {
                                    $meal_sub_items = explode('-', $meal_types[$i]);
                                    if (!empty($meal_sub_items[0])) {
                                        $mealid = intval($meal_sub_items[0]);
                                        $TravcustMealItem = unserialize($wpdb->get_row("SELECT * FROM `travcust_meal_new` WHERE `id` = {$mealid}")->_cs_travcust_meal);
                                        if (!empty($TravcustMealItem)) {
                                            ?>
                                            <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                                <div class="v2-list-left mb-3 mr-sm-3">
                                                    <div class="thumb-wrap">
                                                        <img src="<?php echo $TravcustMealItem['img']; ?>" width="180" alt="" class="img-responsive">
                                                    </div>
                                                </div>
                                                <div class="v2-list-right">
                                                    <div class="d-flex flex-row">
                            <?php if($can_modify_item) { ?>
                                                        <div class="v2-actions order-last">
                                                            <a item="<?php echo $i; ?>" class="meal-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                                            <a onclick="modifyItem('meal-section', 'mealindex', <?php echo $i; ?>)" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                                                        </div>
                            <?php } ?>
                                                        <div class="mr-auto">
                                                            <h4 class="v2-title mt-0 mb-3"><i class="fa fa-utensils text-gray-0"></i> <?php echo $TravcustMealItem['item']; ?></h4>
                                                            <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i>  <?php echo $TravcustMealItem['area']; ?> – <a href="#">Displayed on the map</a>  (100 m away from central area) </p>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    if (count($meal_sub_items) > 1) {
                                                        $subitems = $TravcustMealItem['subitem'];
                                                        for ($subi = 1; $subi < count($meal_sub_items); $subi++) {
                                                            if ($meal_sub_items[$subi] == 1) {
                                                                $total_price += $subitems[$subi - 1]['price'];
                                                                $meal_total_price += $subitems[$subi - 1]['price'];
                                                                ?>
                                                                <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                                                    <div class="mb-4 mb-sm-0 border-sm-right mx-40">
                                                                        <?php if ($can_modify_item) { ?>
                                                                            <textarea  class="notecontent" rows="4" cols="100" style="resize:none;" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                                                        <?php } else { ?>
                                                                            <p><?php if (!empty($notecontents[$nindex])) echo 'Note: ' . $notecontents[$nindex]; ?></p>
                                                                            <?php
                                                                        }
                                                                        $nindex++;
                                                                        ?>
                                                                    </div>
                                                                    <div class="mb-4 mb-sm-0 border-sm-right mx-40"><?php echo $subitems[$subi - 1]['name']; ?></div>
                                                                    <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                                                        <div class="flex-fill p-2 border-right ">
                                                                            <i class="fa fa-calendar-alt mr-1"></i> <?php echo 'Day ' . $meal_dates[$i]; ?> <i class="fas fa-clock mr-1"></i> <?php echo $meal_times[$i]; ?>
                                                                        </div>
                                                                        <!--div class="flex-fill p-2 border-right ">
                                                                            <i class="fa fa-user mr-1"></i> 3 people
                                                                        </div-->
                                                                        <div class="flex-fill p-2 border-right ">
                                                                            <span class="font-small text-blue-3 font-weight-normal"><span class="change_price" or_pic="<?php echo $subitems[$subi - 1]['price'] * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $subitems[$subi - 1]['price'] * get_option('person_price_ration'); ?></span></span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>

                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </div>
                        <?php } ?>

                        <!-- Restaurant -->
                        <?php if (!empty($restaurant_name)) { ?>
                            <div class="v2-list">
                                <?php
                                $restaurant_names = explode('--', unescape($restaurant_name));
                                $restaurant_locals = explode('--', unescape($restaurant_local));
                                $restaurant_imgs = explode('--', unescape($restaurant_img));
                                $restaurant_prices = explode(',', unescape($restaurant_price));
                                $restaurant_dinerss = explode(',', unescape($restaurant_diners));
                                $restaurant_dates = explode(',', unescape($restaurant_date));
                                $restaurant_times = explode(',', unescape($restaurant_time));

                                for ($i = 0; $i < count($restaurant_names) - 1; $i++) {
                                    ?>
                                    <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                        <div class="v2-list-left mb-3 mr-sm-3">
                                            <div class="thumb-wrap" style="height: 70px;">
                                                <img src="<?php echo $restaurant_imgs[$i]; ?>" width="180" alt="" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="v2-list-right" style="display: contents;">
                                            <div class="d-flex flex-row flex_row_dinn_arr">
                        <?php if($can_modify_item) { ?>
                                                <div class="v2-actions order-last">
                                                    <a item="<?php echo $i; ?>" class="restaurant-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                                </div>
                        <?php } ?>
                                                <div class="mr-auto">
                                                    <h4 class="v2-title mt-0 mb-3"><i class="fa fa-utensils text-gray-0"></i> <?php echo $restaurant_names[$i]; ?></h4>
                                                    <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i>  <?php echo $restaurant_locals[$i]; ?> </p>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                                <div class="mb-4 mb-sm-0 border-sm-right mx-40">
                                                    <?php if ($can_modify_item) { ?>
                                                        <textarea  class="notecontent" rows="4" cols="100" style="resize:none;" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                                    <?php } else { ?>
                                                        <p><?php if (!empty($notecontents[$nindex])) echo 'Note: ' . $notecontents[$nindex]; ?></p>
                                                        <?php
                                                    }
                                                    $nindex++;
                                                    ?>
                                                </div>
                                                <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                                    <div class="flex-fill p-2 border-right ">
                                                        <i class="fa fa-calendar-alt mr-1"></i> <?php echo 'Day ' . $restaurant_dates[$i]; ?> <i class="fas fa-clock mr-1"></i> <?php echo $restaurant_times[$i]; ?>
                                                    </div>
                                                    <div class="flex-fill p-2 border-right ">
                                                        <i class="fa fa-user mr-1"></i> <?php echo $restaurant_dinerss[$i]; ?> diners
                                                    </div>
                                                    <div class="flex-fill p-2 border-right ">
                                                        <span class="font-small text-blue-3 font-weight-normal"><span class="change_price" or_pic="<?php echo intval($restaurant_prices[$i]); ?>">Rp<span class="price_span"><?php echo intval($restaurant_prices[$i]); ?></span></span> /2 persons (approximately)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        <?php } ?>

                        <div class="v2-foot text-right p-3 pr-sm-5">
              <?php
              if(!empty($coupon) && $coupon->type=='meal') {
                ?>
                <p>Total Cost <span class="change_price" or_pic="<?php echo $meal_total_price * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $meal_total_price * get_option('person_price_ration'); ?></span></p>
                <?php
                if($coupon->discount=='$') {
                  $discount=$coupon->discount.$coupon->figure;
                  if(($meal_total_price*get_option('person_price_ration'))<($coupon->figure/get_option("_cs_currency_USD"))) {
                    $discount_rp=$meal_total_price;
                    $total_price-=$meal_total_price;
                    $meal_total_price=0;
                  }
                  else {
                    $discount_rp=$coupon->figure/get_option("_cs_currency_USD")/get_option('person_price_ration');
                    $meal_total_price-=$discount_rp;
                    $total_price-=$discount_rp;
                  }
                }
                else {
                  $discount=$coupon->figure.$coupon->discount;
                  $discount_rp=$meal_total_price*$coupon->figure/100;
                  $meal_total_price-=$discount_rp;
                  $total_price-=$discount_rp;
                }
                $wpdb->query("UPDATE `wp_coupons` SET discount_rp={$discount_rp} WHERE id={$coupon->id}");
                ?>
                <p>Discount -<?php echo $discount; ?></p>
                Net cost.
              <?php } else { ?>
                Total: 
              <?php } ?>
                            <strong><span class="font-small text-blue-3 font-weight-normal"><span class="change_price" or_pic="<?php echo $meal_total_price * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $meal_total_price * get_option('person_price_ration'); ?></span></span></strong>
                        </div>
                    </div>
                <?php } ?>

                <!--Recommend Places-->
                <?php
                if (!empty($tourlist_name) || !empty($place_name)) {
                    ?>
                    <div class="d-flex bg-blue-3  text-white align-items-center border-radius v2-header mb-2 recom_place_tab">
                        <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl"><i class="fas fa-map fa-fw"></i> </div>
                        <div class="font-lg p-2 pr-3 pl-3 mr-auto">Recommend Places</div>
                        <button onclick="clickCounter()" class="btn p-2 pr-3 pl-3 btn-none text-white"><i class="fa fa-minus font-lg recom_place_icon"></i></button>
                    </div>
                    <div class="place v2-box mb-3 recom_place_item_box">

                        <div class="v2-list">
                            <?php
                            if (!empty($place_name)) {
                                $place_names = explode('--', unescape($place_name));
                                $place_imgs = explode(',', unescape($place_img));
                                $place_dates = explode(',', unescape($place_date));
                                $place_times = explode(',', unescape($place_time));
                                for ($i = 0; $i < count($place_names) - 1; $i++) {
                                    $has_activity = true;
                                    ?>
                                    <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                        <div class="v2-list-left mb-3 mr-sm-3">
                                            <div class="thumb-wrap">
                                                <img src="<?php echo $place_imgs[$i]; ?>" width="180" alt="" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="v2-list-right">
                                            <div class="d-flex flex-row">
                        <?php if($can_modify_item) { ?>
                                                <div class="v2-actions order-last">
                                                    <a item="<?php echo $i; ?>" class="place-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                                    <a onclick="modifyItem('travel-location', 'placeindex', <?php echo $i; ?>)" class="place-item-edit cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                                                </div>
                        <?php } ?>
                                                <div class="mr-auto">
                                                    <h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i> <?php echo urldecode($place_names[$i]); ?></h4>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                                <div class="mb-4 mb-sm-0 border-sm-right mx-40">
                                                    <?php if ($can_modify_item) { ?>
                                                        <textarea  class="notecontent" rows="4" cols="100" style="resize:none;" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                                    <?php } else { ?>
                                                        <p><?php if (!empty($notecontents[$nindex])) echo 'Note: ' . $notecontents[$nindex]; ?></p>
                                                        <?php
                                                    }
                                                    $nindex++;
                                                    ?>
                                                </div>
                                                <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                                    <div class="flex-fill p-2 border-right ">
                                                        <i class="fa fa-calendar-alt mr-1"></i> <?php echo 'Day ' . $place_dates[$i]; ?> <i class="fas fa-clock mr-1"></i> <?php echo $place_times[$i]; ?>
                                                    </div>
                                                    <div class="flex-fill p-2 border-right ">
                                                        <span class="font-small text-blue-3 font-weight-normal"><span class="change_price" or_pic="0">Rp<span class="price_span">0</span></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                            <?php
                            if (!empty($tourlist_name)) {
                                $tourlist_names = explode('--', unescape($tourlist_name));
                                $tourlist_imgs = explode(',', unescape($tourlist_img));
                                $tourlist_prices = explode('--', unescape($tourlist_price));
                                $tourlist_dates = explode(',', unescape($tourlist_date));
                                $tourlist_times = explode(',', unescape($tourlist_time));
                                $tourlist_total_price = 0;

                                for ($i = 0; $i < count($tourlist_names) - 1; $i++) {
                                    $has_activity = true;
                                    $total_price += ceil($tourlist_prices[$i] / get_option("_cs_currency_USD") / get_option('person_price_ration'));
                                    $tourlist_total_price += ceil($tourlist_prices[$i] / get_option("_cs_currency_USD"));
                                    ?>
                                    <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                        <div class="v2-list-left mb-3 mr-sm-3">
                                            <div class="thumb-wrap">
                                                <img src="<?php echo $tourlist_imgs[$i]; ?>" width="180" alt="" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="v2-list-right">
                                            <div class="d-flex flex-row">
                        <?php if($can_modify_item) { ?>
                                                <div class="v2-actions order-last">
                                                    <a item="<?php echo $i; ?>" class="tourlist-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                                    <a onclick="modifyItem('sort_top_sellers', 'tourlistindex', <?php echo $i; ?>)" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                                                </div>
                        <?php } ?>
                                                <div class="mr-auto">
                                                    <h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i> <?php echo $tourlist_names[$i]; ?></h4>
                                                    <!--p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i>  Bali Island Kuta – <a href="#">Displayed on the map</a>  (100 m away from central area) </p-->
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                                <div class="mb-4 mb-sm-0 border-sm-right mx-40">
                                                    <?php if ($can_modify_item) { ?>
                                                        <textarea  class="notecontent" rows="4" cols="100" style="resize:none;" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                                    <?php } else { ?>
                                                        <p><?php if (!empty($notecontents[$nindex])) echo 'Note: ' . $notecontents[$nindex]; ?></p>
                                                        <?php
                                                    }
                                                    $nindex++;
                                                    ?>
                                                </div>
                                                <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                                    <div class="flex-fill p-2 border-right ">
                                                        <i class="fa fa-calendar-alt mr-1"></i> <?php echo 'Day ' . $tourlist_dates[$i]; ?> <i class="fas fa-clock mr-1"></i> <?php echo $tourlist_times[$i]; ?>
                                                    </div>
                                                    <!--div class="flex-fill p-2 border-right ">
                                                        <i class="fa fa-user mr-1"></i> 3 people
                                                    </div-->
                                                    <div class="flex-fill p-2 border-right ">
                                                        <span class="font-small text-blue-3 font-weight-normal"><span class="change_price" or_pic="<?php echo ceil($tourlist_prices[$i] / get_option("_cs_currency_USD")); ?>">Rp<span class="price_span"><?php echo ceil($tourlist_prices[$i] / get_option("_cs_currency_USD")) * get_option('person_price_ration'); ?></span></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php /*        
                                  <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                  <div class="v2-list-left mb-3 mr-sm-3">
                                  <div class="thumb-wrap">
                                  <img src="/English/wp-content/themes/bali/images/tour-details/40.jpg" width="180" alt="" class="img-responsive">
                                  </div>
                                  </div>
                                  <div class="v2-list-right">
                                  <div class="d-flex flex-row">
                                  <div class="v2-actions order-last">
                                  <a href="#" class="cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                  <a href="#" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                                  </div>
                                  <div class="mr-auto">
                                  <h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i> Kuta Beach </h4>
                                  <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i>  Bali Island Kuta – <a href="#">Displayed on the map</a>  (100 m away from central area) </p>
                                  </div>
                                  </div>
                                  <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                  <div class="mb-4 mb-sm-0 border-sm-right mx-40">•The most beautiful coast on the island, the most exciting surfing, night life is rich and colorful. Kuta Beach is a good place to enjoy sunbathing and enjoy sunset. The unique wave created the famous surf sanctuary...

                                  </div>
                                  <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                  <div class="flex-fill p-2 border-right ">
                                  <i class="fa fa-calendar-alt mr-1"></i> October 30, 2018 <i class="fas fa-clock mr-1"></i> 9:00 am
                                  </div>
                                  <div class="flex-fill p-2 border-right ">
                                  <i class="fa fa-user mr-1"></i> 3 people
                                  </div>
                                  <div class="flex-fill p-2 border-right ">
                                  <span class="font-small text-blue-3 font-weight-normal">$ 215</span>
                                  </div>
                                  </div>
                                  </div>





                                  </div>

                                  </div>
                                  <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                  <div class="v2-list-left mb-3 mr-sm-3">
                                  <div class="thumb-wrap">
                                  <img src="/English/wp-content/themes/bali/images/tour-details/46.jpg" width="180" alt="" class="img-responsive">
                                  </div>
                                  </div>
                                  <div class="v2-list-right">
                                  <div class="d-flex flex-row">
                                  <div class="v2-actions order-last">
                                  <a href="#" class="cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                  <a href="#" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                                  </div>
                                  <div class="mr-auto">
                                  <h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i> Kuta Beach </h4>
                                  <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i>  Bali Island Kuta – <a href="#">Displayed on the map</a>  (100 m away from central area) </p>
                                  </div>
                                  </div>
                                  <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                  <div class="mb-4 mb-sm-0 border-sm-right mx-40">•The most beautiful coast on the island, the most exciting surfing, night life is rich and colorful. Kuta Beach is a good place to enjoy sunbathing and enjoy sunset. The unique wave created the famous surf sanctuary...
                                  </div>
                                  <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                  <div class="flex-fill p-2 border-right ">
                                  <i class="fa fa-calendar-alt mr-1"></i> October 30, 2018 <i class="fas fa-clock mr-1"></i> 9:00 am
                                  </div>
                                  <div class="flex-fill p-2 border-right ">
                                  <i class="fa fa-user mr-1"></i> 3 people
                                  </div>
                                  <div class="flex-fill p-2 border-right ">
                                  <span class="font-small text-blue-3 font-weight-normal">$ 215</span>
                                  </div>
                                  </div>
                                  </div>





                                  </div>

                                  </div>
                                 */ ?>
                            </div>
                            <div class="v2-foot text-right p-3 pr-sm-5">
                                Total：<strong><span class="change_price" or_pic="<?php echo $tourlist_total_price; ?>">Rp<span class="price_span"><?php echo $tourlist_total_price; ?></span></span></strong>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>

                <!--Spa-->
                <?php
                if (!empty($spa_type)) {
                    $TravcustSpa = unserialize(get_option('_cs_travcust_spa'));
                    $spa_types = explode(',', unescape($spa_type));
                    $spa_dates = explode(',', unescape($spa_date));
                    $spa_times = explode(',', unescape($spa_time));
                    $spa_total_price = 0;
                    ?>

                    <div class="d-flex bg-blue-3  text-white align-items-center border-radius v2-header mb-2 spa_tab">
                        <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl"><i class="fas fa fa-spa"></i> </div>
                        <div class="font-lg p-2 pr-3 pl-3 mr-auto">Spa</div>
                        <button onclick="clickCounter()"  class="btn p-2 pr-3 pl-3 btn-none text-white"><i class="fa fa-minus font-lg spa_icon"></i></button>
                    </div>
                    <div class="spa v2-box mb-3 spa_item_box">

                        <div class="v2-list">
                            <?php
                            for ($i = 0; $i < count($spa_types) - 1; $i++) {
                                $has_activity = true;
                                ?>
                                <div class="v2-list-item d-flex flex-column flex-sm-row">
                                    <div class="v2-list-left mb-3 mr-sm-3">
                                        <div class="thumb-wrap">
                                            <img src="<?php echo $TravcustSpa[$spa_types[$i] - 1]['img']; ?>" width="180" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="v2-list-right flex-grow-1">
                                        <div class="d-flex flex-row">
                      <?php if($can_modify_item) { ?>
                                            <div class="v2-actions order-last">
                                                <a item="<?php echo $i; ?>" class="spa-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                                <a onclick="modifyItem('collapsespa', 'spaindex', <?php echo $i; ?>)" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                                            </div>
                      <?php } ?>
                                            <div class="mr-auto">
                                                <h4 class="v2-title mt-0 mb-3"><i class="fa fa-spa text-gray-0"></i> <?php echo $TravcustSpa[$spa_types[$i] - 1]['item']; ?></h4>
                                            </div>
                                        </div>
                                        <!--p class="mb-4">The first ranked Nusa Dua spa and regimen, facing the sea and early morning sunshine, accompanied by a slight sea breeze.</p-->
                                        <?php
                                        if (count(explode('-', $spa_types[$i])) > 1) {
                                            ?>
                                            <div class="v2-tb font-xsmall">
                                                <?php
                                                $subitems = $TravcustSpa[$spa_types[$i] - 1]['subitem'];
                                                $subitem_types = explode('-', $spa_types[$i]);
                                                for ($subi = 1; $subi < count($subitem_types); $subi++) {
                                                    if ($subitem_types[$subi] >= 1) {
                                                        $spa_numi = $subitem_types[$subi];
                                                        $total_price += $subitems[$subi - 1]['price'] * $spa_numi;
                                                        $spa_total_price += $subitems[$subi - 1]['price'] * $spa_numi;
                                                        ?>
                                                        <div class="v2-tb-row p-1">
                                                            <div class="d-flex flex-row ">
                                                                <div class="p-2 flex-grow-1  max-250"><a href="#" class="fa fa-trash text-blue-0"></a><?php echo $subitems[$subi - 1]['name']; ?></div>
                                                                <div class="flex-grow-1">
                                                                    <div class="d-flex flex-row justify-content-between">
                                                                        <div class="p-2"><a href="#" class="fa fa-calendar-alt text-blue-0"></a> <span class="mr-3"><?php echo 'Day ' . $spa_dates[$i] . ' ' . $spa_times[$i]; ?></span><i class="fas fa-clock"></i> </div>
                                                                        <!--div class="p-2 font-small text-blue-3 font-weight-normal"> $ 248/ person</div-->
                                                                        <div class="p-2"><?php echo $spa_numi . ' Person'; ?></div>
                                                                        <div class="p-2"><span class="change_price" or_pic="<?php echo $subitems[$subi - 1]['price'] * $spa_numi * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $subitems[$subi - 1]['price'] * $spa_numi * get_option('person_price_ration'); ?></span></span></div>
                                                                    </div>

                                                                    <?php if ($can_modify_item || !empty($notecontents[$nindex])) { ?>
                                                                        <div class="v2-input d-flex flex-row align-items-center mb-1">
                                                                            <?php if ($can_modify_item) { ?>
                                                                                <div  class="prefix"><i class="fa fa-pen text-gray-0"></i> </div>
                                                                                <textarea class="notecontent form-control" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                                                            <?php } else { ?>
                                                                                <p><?php echo 'Note: ' . $notecontents[$nindex]; ?></p>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    $nindex++;
                                                                    ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="v2-foot text-right p-3 pr-sm-5">
              <?php
              if(!empty($coupon) && $coupon->type=='massage') {
                ?>
                <p>Total Cost <span class="change_price" or_pic="<?php echo $spa_total_price * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $spa_total_price * get_option('person_price_ration'); ?></span></p>
                <?php
                if($coupon->discount=='$') {
                  $discount=$coupon->discount.$coupon->figure;
                  if(($spa_total_price*get_option('person_price_ration'))<($coupon->figure/get_option("_cs_currency_USD"))) {
                    $discount_rp=$spa_total_price;
                    $total_price-=$spa_total_price;
                    $spa_total_price=0;
                  }
                  else {
                    $discount_rp=$coupon->figure/get_option("_cs_currency_USD")/get_option('person_price_ration');
                    $spa_total_price-=$discount_rp;
                    $total_price-=$discount_rp;
                  }
                }
                else {
                  $discount=$coupon->figure.$coupon->discount;
                  $discount_rp=$spa_total_price*$coupon->figure/100;
                  $spa_total_price-=$discount_rp;
                  $total_price-=$discount_rp;
                }
                $wpdb->query("UPDATE `wp_coupons` SET discount_rp={$discount_rp} WHERE id={$coupon->id}");
                ?>
                <p>Discount -<?php echo $discount; ?></p>
                Net cost.
              <?php } else { ?>
                Total: 
              <?php } ?>
                            <strong><span class="change_price" or_pic="<?php echo $spa_total_price * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $spa_total_price * get_option('person_price_ration'); ?></span></span></strong>
                        </div>
                    </div>
                <?php } ?>

                <!--Tour Guide-->
                <?php
                if (!empty($popupguide_type)) {
                    $TravcustPopupguide = unserialize(get_option('_cs_travcust_popupguide'));
                    $popupguide_types = explode(',', unescape($popupguide_type));
                    $popupguide_dates = explode(',', unescape($popupguide_date));
                    $popupguide_times = explode(',', unescape($popupguide_time));
                    $guide_total_price = 0;
                    ?>
<script type="text/javascript">

        $(function () {
            $(window).bind("beforeunload", function () {
                ajax_loading(<?php echo $tour_id ?>,localStorage.clickcount );
                localStorage.clear();

            })
        });
</script>
                    <div class="d-flex bg-blue-3  text-white align-items-center border-radius v2-header mb-2 tour_guide_tab">
                        <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl"><i class="fa fa-child fa-fw"></i> </div>
                        <div class="font-lg p-2 pr-3 pl-3 mr-auto">Tour Guide</div>
                        <button onclick="clickCounter()" class="btn p-2 pr-3 pl-3 btn-none text-white"><i class="fa fa-minus font-lg tour_guide_icon"></i></button>
                    </div>
                    <div class="guide v2-box mb-3 tour_guide_item_box">

                        <div class="v2-list">
                            <?php
                            for ($i = 0; $i < count($popupguide_types) - 1; $i++) {
                                $has_activity = true;
                                $total_price += $TravcustPopupguide[$popupguide_types[$i] - 1]['price'];
                                $guide_total_price += $TravcustPopupguide[$popupguide_types[$i] - 1]['price'];
                                ?>
                                <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                    <div class="v2-list-left mb-3 mr-sm-3">
                                        <div class="thumb-wrap">
                                            <img src="<?php echo $TravcustPopupguide[$popupguide_types[$i] - 1]['img']; ?>" width="180" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="v2-list-right">
                                        <div class="d-flex flex-row">
                      <?php if($can_modify_item) { ?>
                                            <div class="v2-actions order-last">
                                                <a item="<?php echo $i; ?>" class="guide-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                                <a onclick="modifyItem('collapsepopup', 'guideindex', <?php echo $i; ?>)" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                                            </div>
                      <?php } ?>
                                            <div class="mr-auto">
                                                <h4 class="v2-title mt-0 mb-3"><i class="fa fa-running text-gray-0"></i> <?php echo $TravcustPopupguide[$popupguide_types[$i] - 1]['item']; ?></h4>
                                                <?php /*<p class="v2-address mb-3"><i class="fa fa-mobile-alt mr-1 text-blue-0"></i> <?php echo $TravcustPopupguide[$popupguide_types[$i] - 1]['phone']; ?> </p> */ ?>
                                                <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i> <?php echo $TravcustPopupguide[$popupguide_types[$i] - 1]['area']; ?> </p>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                            <div class="mb-4 mb-sm-0 border-sm-right mx-40">
                                                <?php if ($can_modify_item) { ?>
                                                    <textarea  class="notecontent" rows="4" cols="100" style="resize:none;" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                                <?php } else { ?>
                                                    <p><?php if (!empty($notecontents[$nindex])) echo 'Note: ' . $notecontents[$nindex]; ?></p>
                                                    <?php
                                                }
                                                $nindex++;
                                                ?>
                                            </div>
                                            <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                                <div class="flex-fill p-2 border-right ">
                                                    <i class="fa fa-calendar-alt mr-1 text-blue-0"></i> <?php echo 'Day ' . $popupguide_dates[$i] . ' ' . $popupguide_times[$i]; ?>
                                                </div>
                                                <? /*
                                                  <div class="flex-fill p-2 border-right ">
                                                  <i class="fa fa-sun mr-1 text-blue-0"></i> 3 days
                                                  </div>
                                                  <div class="flex-fill p-2 border-right ">
                                                  <i class="fa fa-yen-sign mr-1 text-blue-0"></i><span >$360/ days</span>
                                                  </div> */ ?>
                                                <div class="flex-fill p-2 border-right ">
                                                    <span class="font-small text-blue-3 font-weight-normal"><span class="change_price" or_pic="<?php echo $TravcustPopupguide[$popupguide_types[$i] - 1]['price'] * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $TravcustPopupguide[$popupguide_types[$i] - 1]['price'] * get_option('person_price_ration'); ?></span></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="v2-foot text-right p-3 pr-sm-5">
                            Total：<strong><span class="change_price" or_pic="<?php echo $guide_total_price * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $guide_total_price * get_option('person_price_ration'); ?></span></span></strong>
                        </div>
                    </div>
                <?php } ?>

                <? /* <!--Translation services-->
                  <div class="v2-header d-flex bg-blue-3 align-items-center mb-2">
                  <a href="#" class="header-left bg-blue-0 icon-wrap mr-3 text-white"><i class="fa fa-plus fa-2x"></i> </a>
                  <div class="text-white font-lg">Translation services</div>
                  </div>
                  <div class="v2-box mb-3">

                  <div class="v2-list">
                  <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                  <div class="v2-list-left mb-3 mr-sm-3">
                  <div class="thumb-wrap">
                  <img src="/English/wp-content/themes/bali/images/tour-details/30.jpg" width="180" alt="" class="img-responsive">
                  </div>
                  </div>
                  <div class="v2-list-right">
                  <div class="d-flex flex-row">
                  <div class="v2-actions order-last">
                  <a href="#" class="cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                  <a href="#" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                  </div>
                  <div class="mr-auto">
                  <h4 class="v2-title mt-0 mb-3"><i class="fab fa-weixin text-gray-0"></i> Maria <small>English translation</small></h4>
                  <p class="v2-address mb-3"><i class="fa fa-mobile-alt mr-1 text-blue-0"></i>  +62 (0) 123-456-7890 </p>
                  </div>
                  </div>
                  <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                  <div class="mb-4 mb-sm-0 border-sm-right mx-40">It is one of Bali's three largest temples and the most famous of the Seven Coastal temples. It has the most beautiful evening in Bali. The sunset is the best time to see the temple...</div>
                  <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                  <div class="flex-fill p-2 border-right ">
                  <i class="fa fa-calendar-alt mr-1 text-blue-0"></i> October 30, 2018<br>
                  November 1, 2018
                  </div>
                  <div class="flex-fill p-2 border-right ">
                  <i class="fa fa-sun mr-1 text-blue-0"></i> 3 days
                  </div>
                  <div class="flex-fill p-2 border-right ">
                  <i class="fa fa-yen-sign mr-1 text-blue-0"></i><span >$360/ days</span>
                  </div>
                  <div class="flex-fill p-2 border-right ">
                  <span class="font-small text-blue-3 font-weight-normal">$1080</span>
                  </div>
                  </div>
                  </div>

                  </div>

                  </div>


                  </div>
                  <div class="v2-foot text-right p-3 pr-sm-5">
                  Total：<strong>$ 1,080</strong>
                  </div>


                  </div> */ ?>

                <!--Transportation-->
                <?php
        $adivaha_cars=$wpdb->get_results("SELECT * FROM `wp_cars` WHERE `tour_id` = " . (int) $tour_id, ARRAY_A);
        if (!empty($car_type) || !empty($adivaha_cars)) {
                    $has_activity = true;
                    $car_total_price = 0;
                    ?>

                    <div class="d-flex bg-blue-3  text-white align-items-center border-radius v2-header mb-2 transport_tab">
                        <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl"><i class="fa fa-car fa-fw"></i> </div>
                        <div class="font-lg p-2 pr-3 pl-3 mr-auto">Transportation</div>
                        <button onclick="clickCounter()"  class="btn p-2 pr-3 pl-3 btn-none text-white"><i class="fa fa-minus font-lg transport_icon"></i></button>
                    </div>
                    <div class="car v2-box mb-3 transport_item_box">

                        <div class="v2-list">
                            <?php
              if(!empty($car_type)) {
              $TravcustCar = unserialize(get_option('_cs_travcust_car'));
              $car_types = explode(',', unescape($car_type));
              $car_dates = explode(',', unescape($car_date));
              $car_times = explode(',', unescape($car_time));
                            for ($i = 0; $i < count($car_types) - 1; $i++) {
                                if (floor($car_types[$i]) + 0.5 == $car_types[$i]) {
                                    $half_price = 0.5;
                                    $car_types[$i] = floor($car_types[$i]);
                                } else
                                    $half_price = 1;
	                                $total_price += $TravcustCar[$car_types[$i] - 1]['price'] * $half_price;
	                                $car_total_price += $TravcustCar[$car_types[$i] - 1]['price'] * $half_price;
                                ?>
                                <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                    <div class="v2-list-left mb-3 mr-sm-3">
                                        <div class="thumb-wrap">
                                            <img src="<?php echo $TravcustCar[$car_types[$i] - 1]['img']; ?>" width="180" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="v2-list-right">
                                        <div class="d-flex flex-row">
                      <?php if($can_modify_item) { ?>
                                            <div class="v2-actions order-last">
                                                <a item="<?php echo $i; ?>" class="car-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                                <a onclick="modifyItem('collapseCar', 'carindex', <?php echo $i; ?>)" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                                            </div>
                      <?php } ?>
                                            <div class="mr-auto">
                                                <h4 class="v2-title mt-0 mb-3"><i class="fa fa-car text-gray-0"></i> <?php
                                                    echo $TravcustCar[$car_types[$i] - 1]['item'];
                                                    if ($half_price == 0.5)
                                                        echo ' (half day or airport transfer)';
                                                    ?></h4>
                                                <?php /*<p class="v2-address mb-3"><i class="fa fa-mobile-alt mr-1 text-blue-0"></i> <?php echo $TravcustCar[$car_types[$i] - 1]['phone']; ?> </p> */ ?>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                            <div class="mb-4 mb-sm-0 border-sm-right mx-40">
                                                <?php if ($can_modify_item) { ?>
                                                    <textarea class="notecontent" rows="4" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                                <?php } else { ?>
                                                    <p><?php if (!empty($notecontents[$nindex])) echo 'Note: ' . $notecontents[$nindex]; ?></p>
                                                    <?php
                                                }
                                                $nindex++;
                                                ?>
                                            </div>
                                            <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                                <div class="flex-fill p-2 border-right ">
                                                    <i class="fa fa-calendar-alt mr-1 text-blue-0"></i> <?php echo 'Day ' . $car_dates[$i] . ' ' . $car_times[$i]; ?>
                                                </div>
                                                <!--div class="flex-fill p-2 border-right ">
                                                    <i class="fa fa-sun mr-1 text-blue-0"></i> 3 days
                                                </div>
                                                <div class="flex-fill p-2 border-right ">
                                                    <i class="fa fa-yen-sign mr-1 text-blue-0"></i><span >$1200/ days</span>
                                                </div-->
                                                <div class="flex-fill p-2 border-right ">
                                                    <span class="font-small text-blue-3 font-weight-normal"><span class="change_price" or_pic="<?php echo $TravcustCar[$car_types[$i] - 1]['price'] * get_option('person_price_ration') * $half_price; ?>">Rp<span class="price_span"><?php echo $TravcustCar[$car_types[$i] - 1]['price'] * get_option('person_price_ration') * $half_price; ?></span></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
              }
              }
              
              if(!empty($adivaha_cars)) {
              foreach($adivaha_cars as $row) {
                $adivaha_car_price=round(floatval($row['total'])/get_option("_cs_currency_USD"));
                // * get_option('person_price_ration')
                                $total_price += $adivaha_car_price/get_option('person_price_ration');
                                $car_total_price += $adivaha_car_price/get_option('person_price_ration');
                                ?>
                                <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                    <div class="v2-list-left mb-3 mr-sm-3">
                                        <div class="thumb-wrap">
                                            <img src="<?php echo $row['img']; ?>" width="180" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="v2-list-right">
                                        <div class="d-flex flex-row">
                      <?php if($can_modify_item) { ?>
                                            <div class="v2-actions order-last">
                                                <a item="<?php echo $row['id']; ?>" class="adivaha-car-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                            </div>
                      <?php } ?>
                                            <div class="mr-auto">
                                                <h4 class="v2-title mt-0 mb-3"><i class="fa fa-car text-gray-0"></i> <?php echo $row['name']; ?></h4>
                                                <p class="v2-address mb-3"> <?php echo $row['pickupName']; ?> </p>
                        <p class="v2-address mb-3"> <?php echo $row['returnName']; ?> </p>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                            <div class="mb-4 mb-sm-0 border-sm-right mx-40">
                                                <?php if ($can_modify_item) { ?>
                                                    <textarea class="notecontent" rows="4" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                                <?php } else { ?>
                                                    <p><?php if (!empty($notecontents[$nindex])) echo 'Note: ' . $notecontents[$nindex]; ?></p>
                                                    <?php
                                                }
                                                $nindex++;
                                                ?>
                                            </div>
                                            <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                                <div class="flex-fill p-2 border-right ">
                                                    <i class="fa fa-calendar-alt mr-1 text-blue-0"></i> <?php echo $row['pickupDate'].' - '.$row['returnDate']; ?>
                                                </div>
                                                <div class="flex-fill p-2 border-right ">
                                                    <span class="font-small text-blue-3 font-weight-normal"><span class="change_price" or_pic="<?php echo $adivaha_car_price; ?>">Rp<span class="price_span"><?php echo $adivaha_car_price; ?></span></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
              }
              }
              ?>
                        </div>
                        <div class="v2-foot text-right p-3 pr-sm-5">
              <?php
              if(!empty($coupon) && $coupon->type=='vehicle') {
                ?>
                <p>Total Cost <span class="change_price" or_pic="<?php echo $car_total_price * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $car_total_price * get_option('person_price_ration'); ?></span></p>
                <?php
                if($coupon->discount=='$') {
                  $discount=$coupon->discount.$coupon->figure;
                  if(($car_total_price*get_option('person_price_ration'))<($coupon->figure/get_option("_cs_currency_USD"))) {
                    $discount_rp=$car_total_price;
                    $total_price-=$car_total_price;
                    $car_total_price=0;
                  }
                  else {
                    $discount_rp=$coupon->figure/get_option("_cs_currency_USD")/get_option('person_price_ration');
                    $car_total_price-=$discount_rp;
                    $total_price-=$discount_rp;
                  }
                }
                else {
                  $discount=$coupon->figure.$coupon->discount;
                  $discount_rp=$car_total_price*$coupon->figure/100;
                  $car_total_price-=$discount_rp;
                  $total_price-=$discount_rp;
                }
                $wpdb->query("UPDATE `wp_coupons` SET discount_rp={$discount_rp} WHERE id={$coupon->id}");
                ?>
                <p>Discount -<?php echo $discount; ?></p>
                Net cost.
              <?php } else { ?>
                Total: 
              <?php } ?>
                            <strong><span class="change_price" or_pic="<?php echo $car_total_price * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $car_total_price * get_option('person_price_ration'); ?></span></span></strong>
                        </div>
                    </div>
                <?php } ?>

                <!--Helicopter and Cruiser-->
                <?php
                if (!empty($boat_type)) {
                    $TravcustBoat = unserialize(get_option('_cs_travcust_boat'));
                    $boat_types = explode(',', unescape($boat_type));
                    $boat_dates = explode(',', unescape($boat_date));
                    $boat_times = explode(',', unescape($boat_time));
                    $boat_total_price = 0;
                    ?>
                    <div class="d-flex bg-blue-3  text-white align-items-center border-radius v2-header mb-2 heli_cru_tab">
                        <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl"><i class="fa fa-ship fa-fw"></i> </div>
                        <div class="font-lg p-2 pr-3 pl-3 mr-auto">Helicopter and Cruiser</div>
                        <button onclick="clickCounter()" class="btn p-2 pr-3 pl-3 btn-none text-white"><i class="fa fa-minus font-lg heli_cru_icon"></i></button>
                    </div>
                    <div class="boat v2-box mb-3 heli_cru_item_box">

                        <div class="v2-list">
                            <?php
                            for ($i = 0; $i < count($boat_types) - 1; $i++) {
                                $has_activity = true;
                                ?>
                                <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                    <div class="v2-list-left mb-3 mr-sm-3">
                                        <div class="thumb-wrap">
                                            <img src="<?php echo $TravcustBoat[$boat_types[$i] - 1]['img']; ?>" width="180" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="v2-list-right">
                                        <div class="d-flex flex-row">
                      <?php if($can_modify_item) { ?>
                                            <div class="v2-actions order-last">
                                                <a item="<?php echo $i; ?>" class="boat-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                                <a onclick="modifyItem('collapseBoats', 'boatindex', <?php echo $i; ?>)" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                                            </div>
                      <?php } ?>
                                            <div class="mr-auto">
                                                <h4 class="v2-title mt-0 mb-3"><i class="fa fa-utensils text-gray-0"></i> <?php echo $TravcustBoat[$boat_types[$i] - 1]['item']; ?></h4>
                                                <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i>  <?php echo $TravcustBoat[$boat_types[$i] - 1]['area']; ?> </p>
                                            </div>
                                        </div>
                                        <?php
                                        if (count(explode('-', $boat_types[$i])) > 1) {
                                            $subitems = $TravcustBoat[$boat_types[$i] - 1]['subitem'];
                                            $subitem_types = explode('-', $boat_types[$i]);
                                            for ($subi = 1; $subi < count($subitem_types); $subi++) {
                                                if ($subitem_types[$subi] >= 1) {
                                                    $boat_numi = $subitem_types[$subi];
                                                    $total_price += $subitems[$subi - 1]['price'] * $boat_numi;
                                                    $boat_total_price += $subitems[$subi - 1]['price'] * $boat_numi;
                                                    ?>
                                                    <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                                        <div class="mb-4 mb-sm-0 border-sm-right mx-40" style="width: 500px;"><?php echo $subitems[$subi - 1]['name']; ?></div>
                                                        <div class="flex-grow-1">
                                                            <div class="d-flex flex-row justify-content-between align-items-center text-center">
                                                                <div class="flex-fill p-2 border-right ">
                                                                    <i class="fa fa-calendar-alt mr-1"></i> <?php echo 'Day ' . $boat_dates[$i]; ?> <i class="fas fa-clock mr-1"></i> <?php echo $boat_times[$i]; ?>
                                                                </div>
                                                                <div class="p-2"><?php echo 'Quantity:' . $boat_numi; ?></div>
                                                                <div class="flex-fill p-2 border-right ">
                                                                    <span class="font-small text-blue-3 font-weight-normal"><span class="change_price" or_pic="<?php echo $subitems[$subi - 1]['price'] * $boat_numi * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $subitems[$subi - 1]['price'] * $boat_numi * get_option('person_price_ration'); ?></span></span></span>
                                                                </div>
                                                            </div>
                                                            <?php if ($can_modify_item || !empty($notecontents[$nindex])) { ?>
                                                                <div class="v2-input d-flex flex-row align-items-center mb-1">
                                                                    <?php if ($can_modify_item) { ?>
                                                                        <div  class="prefix"><i class="fa fa-pen text-gray-0"></i> </div>
                                                                        <textarea class="notecontent form-control" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                                                    <?php } else { ?>
                                                                        <p><?php if (!empty($notecontents[$nindex])) echo 'Note: ' . $notecontents[$nindex]; ?></p>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php
                                                            }
                                                            $nindex++;
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </div>

                                </div>
                            <?php } ?>
                        </div>
                        <div class="v2-foot text-right p-3 pr-sm-5">
                            <?php
              if(!empty($coupon) && $coupon->type=='helicopter') {
                ?>
                <p>Total Cost <span class="change_price" or_pic="<?php echo $boat_total_price * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $boat_total_price * get_option('person_price_ration'); ?></span></p>
                <?php
                if($coupon->discount=='$') {
                  $discount=$coupon->discount.$coupon->figure;
                  if(($boat_total_price*get_option('person_price_ration'))<($coupon->figure/get_option("_cs_currency_USD"))) {
                    $discount_rp=$boat_total_price;
                    $total_price-=$boat_total_price;
                    $boat_total_price=0;
                  }
                  else {
                    $discount_rp=$coupon->figure/get_option("_cs_currency_USD")/get_option('person_price_ration');
                    $boat_total_price-=$discount_rp;
                    $total_price-=$discount_rp;
                  }
                }
                else {
                  $discount=$coupon->figure.$coupon->discount;
                  $discount_rp=$boat_total_price*$coupon->figure/100;
                  $boat_total_price-=$discount_rp;
                  $total_price-=$discount_rp;
                }
                $wpdb->query("UPDATE `wp_coupons` SET discount_rp={$discount_rp} WHERE id={$coupon->id}");
                ?>
                <p>Discount -<?php echo $discount; ?></p>
                Net cost.
              <?php } else { ?>
                Total: 
              <?php } ?>
              <strong><span class="change_price" or_pic="<?php echo $boat_total_price * get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $boat_total_price * get_option('person_price_ration'); ?></span></span></strong>
                        </div>
                    </div>
                <?php } ?>

                <!--Optional activities-->
                <?php
                $ExtraActivity = unserialize(get_option('_cs_extraactivity'));
                if (!empty($ExtraActivity) && !empty($eaiitem) && !empty($eainum) && !empty($eaidate) && !empty($eaitime)) {
                    $activity_total_price = 0;
                    ?>

                    <div class="d-flex bg-blue-3  text-white align-items-center border-radius v2-header mb-2 op_activities_tab">
                        <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl"><i class="fa fa-tint fa-fw"></i> </div>
                        <div class="font-lg p-2 pr-3 pl-3 mr-auto">Optional activities</div>
                        <button onclick="clickCounter()"  class="btn p-2 pr-3 pl-3 btn-none text-white"><i class="fa fa-minus font-lg op_activities_icon"></i></button>
                    </div>
                    <div class="activity v2-box mb-3 op_activities_item_box">

                        <div class="v2-list">
                            <?php
                            $eaiitems = explode(',', unescape($eaiitem));
                            $eainums = explode(',', unescape($eainum));
                            $eaidates = explode(',', unescape($eaidate));
                            $eaitimes = explode(',', unescape($eaitime));
                            for ($i = 0; $i < count($eaiitems) - 1; $i++) {
                                $p = $eaiitems[$i];
                                if (empty($p) || intval($p) < 0)
                                    continue;
                                $has_activity = true;
                                $total_price += $ExtraActivity[$p]['price'] * $eainums[$i];
                                $activity_total_price += $ExtraActivity[$p]['price'] * $eainums[$i];
                                ?>
                                <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                    <div class="v2-list-left mb-3 mr-sm-3">
                                        <div class="thumb-wrap">
                                            <img src="<?php echo $ExtraActivity[$p]['img']; ?>" width="180" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="v2-list-right">
                                        <div class="d-flex flex-row">
                      <?php if($can_modify_item) { ?>
                                            <div class="v2-actions order-last">
                                                <a item="<?php echo $i; ?>" class="activity-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                                <a onclick="modifyItem('extraactivity', 'activityindex', <?php echo $i; ?>)" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                                            </div>
                      <?php } ?>
                                            <div class="mr-auto">
                                                <h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i> <?php echo $ExtraActivity[$p]['item']; ?> </h4>
                                                <!--p class="v2-address mb-3"><i class="fa fa-mobile-alt mr-1 text-blue-0"></i>  +62 (0) 123-456-7890 </p-->
                                            </div>
                                        </div>
                                        <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                            <div class="mb-4 mb-sm-0 border-sm-right mx-40">
                                                <?php if ($can_modify_item) { ?>
                                                    <textarea class="notecontent" rows="4" placeholder="Add Note"><?php if (!empty($notecontents[$nindex])) echo $notecontents[$nindex]; ?></textarea>
                                                <?php } else { ?>
                                                    <p><?php if (!empty($notecontents[$nindex])) echo 'Note: ' . $notecontents[$nindex]; ?></p>
                                                    <?php
                                                }
                                                $nindex++;
                                                ?>
                                            </div>
                                            <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                                <div class="flex-fill p-2 border-right ">
                                                    <i class="fa fa-calendar-alt mr-1 text-blue-0"></i> <?php echo 'Day ' . $eaidates[$i] . ' ' . $eaitimes[$i]; ?>
                                                </div>
                                                <div class="flex-fill p-2 border-right ">
                                                    Quantity: <?php echo $eainums[$i]; ?>
                                                </div>
                                                <!--div class="flex-fill p-2 border-right ">
                                                    <i class="fa fa-yen-sign mr-1 text-blue-0"></i><span >¥360/天</span>
                                                </div-->
                                                <div class="flex-fill p-2 border-right ">
                                                    <span class="font-small text-blue-3 font-weight-normal"><span class="change_price" or_pic="<?php echo $ExtraActivity[$p]['price'] * $eainums[$i] * get_option('person_price_ration'); ?>"> Rp <span class="price_span"> <?php echo $ExtraActivity[$p]['price'] * $eainums[$i] * get_option('person_price_ration'); ?> </span></span></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="v2-foot text-right p-3 pr-sm-5">
                            Total: <strong><span class="change_price" or_pic="<?php echo $activity_total_price * get_option('person_price_ration'); ?>"> Rp <span class="price_span"> <?php echo $activity_total_price * get_option('person_price_ration'); ?> </span></span></strong>
                        </div>
                    </div>
                <?php } ?>

                <?php
                //if ($can_modify_item && current_user_can('upload_files')) {
        $manualitems = $wpdb->get_results("SELECT meta_id,meta_value FROM `wp_tourmeta` where meta_key='manualitem' and tour_id=" . (int) $tour_id . ' ORDER BY `meta_id` ASC');
        if(!empty($manualitems)) {
                    //wp_enqueue_media();
                    $manualitem_total_price = 0;
                    ?>
                    <!--Manually added items-->
                    <div id="AddAnotherTravelItemManually" class="d-flex bg-blue-3 v2-header text-white align-items-center border-radius mb-2 add_op_tra_item_tab">
                        <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl"><i class="fa fa-tint fa-fw"></i> </div>
                        <div class="font-lg p-2 pr-3 pl-3 mr-auto">Added Optional Travel Items</div>
                        <button onclick="clickCounter()" id="addManualItem" class="btn p-2 pr-3 pl-3 btn-none text-white"><i class="fa fa-minus font-lg add_op_tra_item_icon"></i></button>
                    </div>
                    <div class="manual-item-box v2-box mb-3 add_op_tra_item_box">

                        <div class="v2-list">
                            <?php
                            foreach ($manualitems as $item) {
                                $has_activity = true;
                                $item_id = $item->meta_id;
                                $item = unserialize($item->meta_value);
                                $manualitem_total_price += $item['price'];
                                $total_price += $item['price']/get_option('person_price_ration');
                                ?>
                                <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                                    <div class="v2-list-left mb-3 mr-sm-3">
                                        <div class="thumb-wrap">
                                            <?php if ($item['attachment_type'] == 'image') { ?>
                                                <img src="<?php echo wp_get_attachment_url($item['attachment']); ?>" width="180" alt="" class="img-responsive">
                                            <?php } else if ($item['attachment_type'] == 'video') { ?>
                                                <video class="wp-video-shortcode" width="200" controls="controls" src="<?php echo wp_get_attachment_url($item['attachment']); ?>">
                                                    <source type="video/mp4" src="<?php echo wp_get_attachment_url($item['attachment']); ?>">
                                                </video>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="v2-list-right">
                                        <div class="d-flex flex-row">
                      <?php if($can_modify_item) { ?>
                                            <div class="v2-actions order-last">
                                                <a item="<?php echo $item_id; ?>" class="manual-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                                            </div>
                      <?php } ?>
                                            <div class="mr-auto">
                                                <h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i> <?php echo $item['name']; ?>  </h4>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                            <div class="mb-4 mb-sm-0 border-sm-right mx-40">
                                                <textarea class="notecontent" rows="4" placeholder="Add Note"></textarea>
                                                <p></p>
                                            </div>
                                            <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                                <div class="flex-fill p-2 border-right ">
                                                    <i class="fa fa-calendar-alt mr-1 text-blue-0"></i>Day<?php echo $item['date']; ?> <?php echo $item['time']; ?>
                                                </div>
                                                <div class="flex-fill p-2 border-right ">
                                                    Quantity: <?php echo $item['qty']; ?>
                                                </div>
                                                <div class="flex-fill p-2 border-right ">
                                                    <span class="change_price" or_pic="<?php echo $item['price']; ?>"> Rp <span class="price_span"><?php echo $item['price']; ?> </span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                            <p><?php echo $item['description']; ?></p>
                                        </div>

                                    </div>

                                </div>
                            <?php } ?>

                        </div>
                        <div class="v2-foot text-right p-3 pr-sm-5">
                            Total: <strong><span class="change_price" or_pic="<?php echo $manualitem_total_price; ?>"> Rp <span class="price_span"><?php echo $manualitem_total_price; ?> </span></span></strong>
                        </div>
                    </div>
                    <script>
                        var existedManualItemBox = false;
                        jQuery(document).ready(function () {
                            new Tooltip(document.getElementById('AddAnotherTravelItemManually'), {title: 'Currently [some] travcust pricing only displays south east asia local areas. If you\'d like to create tour packages beyond south east asia, please add your travel items here with your own price and pictures/videos.', placement: 'bottom', html: true, container: 'body', template: '<div class="popper-tooltip popper-tooltip-style1" role="tooltip"><div class="tooltip__arrow"></div><div class="tooltip__inner"></div></div>'});

                            /*$('#addManualItem').click(function (e) {
                                e.preventDefault();
                                if (!existedManualItemBox)
                                    $(this).parent().next().find('.v2-list').append('<div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue"><div class="v2-list-left mb-3 mr-sm-3"><div class="thumb-wrap"><a class="img_video_upload"><i class="fa fa-cloud-upload-alt"></i><strong>Click to Upload a Pictures or Video</strong></a><input class="item_attachment"type="hidden"value=""/><input class="item_attachment_url"type="hidden"value=""/><input class="item_attachment_type"type="hidden"value=""/></div></div><div class="v2-list-right"><div class="d-flex flex-row"><div class="v2-actions order-last"><a item="0" class="manual-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i></a></div><div class="mr-auto"><h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i><input class="item_name"type="text"value=""/></h4></div></div><div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch"><div class="mb-4 mb-sm-0 border-sm-right mx-40"><textarea class="notecontent"rows="4"placeholder="Add Note"></textarea><p></p></div><div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center"><div class="flex-fill p-2 border-right "><i class="fa fa-calendar-alt mr-1 text-blue-0"></i>Day<input class="item_date"type="number"min="1"max="99"/><input class="item_time"type="time"/></div><div class="flex-fill p-2 border-right ">Quantity:<input class="item_qty"type="number"min="1"max="999"/></div><div class="flex-fill p-2 border-right ">USD<input class="item_price"type="text"value="0.00"/></div><div class="flex-fill p-2 border-right "><a class="btn btn-default add_manualitem">Add</a></div></div></div><div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center"><textarea class="item_description"placeholder="Description"style="width: 100%;"></textarea></div></div></div>');
                                $('.manual-item-box .v2-list').last().find('.item_name').focus();
                                existedManualItemBox = true;
                            });

                            var additem;
                            $(document).on('click', '.add_manualitem', function (event) {
                                additem = $(this).parent().parent().parent().parent().parent();
                                if (additem.find(".item_name").val() == '') {
                                    $('.item_name').focus();
                                    additem.find('.item_name').css('border-color', 'red');
                                    return;
                                }
                                additem.find('.item_name').css('border-color', '');
                                if (additem.find(".item_date").val() == '') {
                                    $('.item_date').focus();
                                    additem.find(".item_date").css('border-color', 'red');
                                    return;
                                }
                                additem.find(".item_date").css('border-color', '');
                                if (additem.find(".item_time").val() == '') {
                                    $('.item_time').focus();
                                    additem.find(".item_time").css('border-color', 'red');
                                    return;
                                }
                                additem.find(".item_time").css('border-color', '');
                                if (additem.find(".item_qty").val() == '') {
                                    $('.item_qty').focus();
                                    additem.find(".item_qty").css('border-color', 'red');
                                    return;
                                }
                                additem.find(".item_qty").css('border-color', '');
                                if (additem.find(".item_price").val() == '') {
                                    $('.item_price').focus();
                                    additem.find(".item_price").css('border-color', 'red');
                                    return;
                                }
                                additem.find(".item_price").css('border-color', '');
                                if (additem.find(".item_description").val() == '') {
                                    $('.item_description').focus();
                                    additem.find(".item_description").css('border-color', 'red');
                                    return;
                                }
                                additem.find(".item_description").css('border-color', '');

                                var contents = {
                                    name: additem.find(".item_name").val(),
                                    attachment: additem.find(".item_attachment").val(),
                                    attachment_type: additem.find(".item_attachment_type").val(),
                                    date: additem.find(".item_date").val(),
                                    time: additem.find(".item_time").val(),
                                    qty: additem.find(".item_qty").val(),
                                    price: Math.round(additem.find(".item_price").val() / $("#cs_USD").val()),
                                    description: additem.find(".item_description").val(),
                                };
                                $.ajax({
                                    type: "POST",
                                    url: '/English/wp-content/themes/bali/api.php?action=addManualItem&tour_id=<?php echo $tour_id; ?>',
                                    data: contents,
                                    success: function (data) {
                                        console.log(data);
                                        if (data != 0) {
                                            existedManualItemBox = false;
                                            var price = Math.round(additem.find(".item_price").val() / $("#cs_USD").val());

                                            if (additem.find(".item_attachment_type").val() == 'image') {
                                                additem.find(".img_video_upload").after('<img src="' + additem.find(".item_attachment_url").val() + '" width="180" alt="" class="img-responsive">');
                                            } else if (additem.find(".item_attachment_type").val() == 'video') {
                                                var attachment_url = additem.find(".item_attachment_url").val();
                                                additem.find(".img_video_upload").after('<video class="wp-video-shortcode" width="200" controls="controls" src="' + attachment_url + '"><source type="video/mp4" src="' + attachment_url + '"></video>');
                                            }
                                            additem.find(".img_video_upload").remove();

                                            additem.find(".manual-item-trash").attr('item', data);
                                            additem.find(".item_name").after(additem.find(".item_name").val()).remove();
                                            additem.find(".item_date").after(additem.find(".item_date").val() + " ").remove();
                                            additem.find(".item_time").after(additem.find(".item_time").val()).remove();
                                            additem.find(".item_qty").after(additem.find(".item_qty").val()).remove();
                                            additem.find(".item_description").after('<p>' + additem.find(".item_description").val() + '</p>').remove();
                                            additem.find(".item_price").parent().html('<span class="change_price" or_pic="' + price + '"> Rp <span class="price_span">' + price + ' </span></span>');
                                            additem.find(".add_manualitem").parent().remove();
                                            $(".manual-item-box .v2-foot .change_price").attr('or_pic', parseInt($(".manual-item-box .v2-foot .change_price").attr('or_pic')) + price);
                                            update_total_price();
                                            $(".header_curreny").change();
                                        }
                                    }
                                });
                            });

                            var upload_frame;
                            var t;
                            jQuery(document).on('click', '.img_video_upload', function (event) {
                                t = jQuery(this);
                                event.preventDefault();
                                if (upload_frame) {
                                    upload_frame.open();
                                    return;
                                }
                                upload_frame = wp.media({
                                    title: 'Insert image',
                                    button: {
                                        text: 'Insert',
                                    },
                                    multiple: false
                                });
                                upload_frame.on('select', function () {
                                    attachment = upload_frame.state().get('selection').first().toJSON();
                                    console.log(attachment);
                                    t.parent().find(".item_attachment").val(attachment.id);
                                    t.parent().find(".item_attachment_url").val(attachment.url);
                                    t.parent().find(".item_attachment_type").val(attachment.type);
                                });
                                upload_frame.open();
                            });*/
                        });
                    </script>
                <?php } ?>

                <!--map-->
                <?php
                $total_price *= get_option('person_price_ration');
                $discount = 0;
                $adults = empty($_COOKIE['hotel_adults']) ? 0 : $_COOKIE['hotel_adults'];
                $children = empty($_COOKIE['hotel_childs']) ? 0 : $_COOKIE['hotel_childs'];
                $noofguest = $adults + $children;
                $totalprice = $total_price;
                $_SESSION['totalprice'] = $total_price;


                $disbaligroup1 = (preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disbaligroup1', true))) / 100;
                $disbaligroup2 = (preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disbaligroup2', true))) / 100;
                $disbaligroup3 = (preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disbaligroup3', true))) / 100;
                $disbaligroup4 = (preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disbaligroup4', true))) / 100;
                $disbaligroup5 = (preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disbaligroup5', true))) / 100;


                $disnonbaligroup1 = (preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disnonbaligroup1', true))) / 100;
                $disnonbaligroup2 = (preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disnonbaligroup2', true))) / 100;
                $disnonbaligroup3 = (preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disnonbaligroup3', true))) / 100;
                $disnonbaligroup4 = (preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disnonbaligroup4', true))) / 100;
                $disnonbaligroup5 = (preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disnonbaligroup5', true))) / 100;

                $bali = 0;
                foreach ($locationnames as $locval) {
                    if ((strpos($locval, 'Bali') !== false) || (strpos($locval, 'BALI') !== false) || (strpos($locval, 'bali') !== false)) {

                        if ($noofguest >= 1 && $noofguest <= 2) {
                            $discount = $totalprice * $disbaligroup1;
                        }
                        if ($noofguest >= 3 && $noofguest <= 5) {
                            $discount = $totalprice * $disbaligroup2;
                        }
                        if ($noofguest >= 6 && $noofguest <= 10) {
                            $discount = $totalprice * $disbaligroup3;
                        }
                        if ($noofguest >= 11 && $noofguest <= 15) {
                            $discount = $totalprice * $disbaligroup4;
                        }
                        if ($noofguest > 15) {
                            $discount = $totalprice * $disbaligroup5;
                        }
                        $bali = $discount;
                    } else if ($bali != 0) {
                        $discount = $bali;
                    } else {
                        if ($noofguest >= 1 && $noofguest <= 2) {
                            $discount = $totalprice * $disnonbaligroup1;
                        }
                        if ($noofguest >= 3 && $noofguest <= 5) {
                            $discount = $totalprice * $disnonbaligroup2;
                        }
                        if ($noofguest >= 6 && $noofguest <= 10) {
                            $discount = $totalprice * $disnonbaligroup3;
                        }
                        if ($noofguest >= 11 && $noofguest <= 15) {
                            $discount = $totalprice * $disnonbaligroup4;
                        }
                        if ($noofguest > 15) {
                            $discount = $totalprice * $disnonbaligroup5;
                        }
                    }
                }

                $total_discount = $total_price - $discount;
                $discountp = $discount / $totalprice;
                $tax = $total_discount * (1 / 100);
                $net_price = $total_discount + $tax;

        //sync the price to database
                if ($can_modify_item) {
                    $wpdb->query("UPDATE `wp_tour` SET `total` = '" . round($net_price) . "' WHERE `wp_tour`.`id` = '{$tour_id}'");
                }

                if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_postmeta` WHERE `meta_key`='tour_id' AND `meta_value`='{$tour_id}'") == 1)
                    $post_id = $wpdb->get_row("SELECT post_id FROM `wp_postmeta` WHERE `meta_key`='tour_id' AND `meta_value`='{$tour_id}'")->post_id;
                else
                    $post_id = false;
                ?>
                <div class="total_cost_content_desk d-flex flex-column flex-sm-row mb-3">
                   
<!--                  start cost box-->
                    <div class="cost-box flex-fill">
<!--                      start cost box main-->
                      <div>
                      <div style="padding-bottom: 0;" class="bd">
                        <div class="cost-label">
                          <div>TOTAL COST : </div>
                          <div>APPLICABLE DISCOUNT :</div>
                          <div>TOTAL COST AFTER DISCOUNT : </div>
                          <div>TAX 1% : </div>
                          <div>NET PRICE : </div>
                        </div>
                        <div class="cost-value">
                            <?php if($discount_rp == 0){
                                $discount_rp = 1;
                            }?>
                          <div class="total_price"><span class="change_price" or_pic="<?php echo $total_price+$discount_rp*get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $total_price+$discount_rp*get_option('person_price_ration'); ?></span></span></div>
                          <div class="discount">-<span class="change_price" or_pic="<?php echo $discount+$discount_rp*get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $discount+$discount_rp*get_option('person_price_ration'); ?></span></span></div>
                          <div class="total_discount"><span class="change_price" or_pic="<?php echo $total_discount; ?>">Rp<span class="price_span"><?php echo $total_discount; ?></div>
                          <div class="tax"><span class="change_price" or_pic="<?php echo $tax; ?>">Rp<span class="price_span"><?php echo $tax; ?></span></span></div>
                          <div class="net_price"><span class="change_price" or_pic="<?php echo $net_price; ?>">Rp<span class="price_span"><?php echo $net_price; ?></span></span></div>
                        </div>
                      </div>
                        <div class="target-people" style="display:none;padding: 0 30px;text-align: right;">
                          <div><strong>You created for <span class="data-tour-peoples"></span> people</strong></div>
                          <div>you can buy this tour package for more than <span class="data-tour-peoples"></span> people</div>
                        </div>
                      </div>
<!--                      end cost box main-->
                      <?php if ($post_id !== false && get_post_status($post_id) == 'publish'):?>
                      <?php if ($tour_id != '' && is_user_logged_in()) {
                              $current_user = wp_get_current_user();
                              $sc_user_id = isset($current_user->ID) ? $current_user->ID : '';
                              $getDataForCheck = $wpdb->get_results("SELECT * FROM `social_connect` WHERE sc_tour_id = '$tour_id' AND sc_user_id = '$sc_user_id'", ARRAY_A);
                              $sc_type = isset($getDataForCheck[0]['sc_type']) ? $getDataForCheck[0]['sc_type'] : '';
                              //Get the count of like and dislike
                              $likecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$tour_id}' AND `sc_type` = 0");
                              $dislikecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$tour_id}' AND `sc_type` = 1");
                          }
                          ?>
<!-- start cost box footer-->
                      <form method="post" id="social_connect_form" class="pdfcrowd-remove">
                        <input type="hidden" name="action" value="social_connect_l_d">
                        <input type="hidden" name="sc_tour_id" value="<?php echo $tour_id; ?>">
                      <footer>
                        <label for="r1" class="cost-action" id="like-link"  style="cursor: pointer;margin-bottom: 0;">
                          <i class="fa fa-thumbs-up tr_dtl_com_desk"></i>
                          <div>
                            <input type="radio" id="r1" name="sc_type" value="0" <?php if ($sc_type == '0') echo "checked"; ?>  style="opacity: 0;position: absolute;left:-9999em;">
                            <div class="cost-action-value tr_dtl_com_desk" <?php if ($sc_type == '0') echo "style='color:red;'"; ?>><?php echo number_format($likecount); ?></div>
                            <div class="tr_dtl_com_mob"><i class="fa fa-thumbs-up">
                               </i><sup <?php if ($sc_type == '0') echo "style='color:red;'"; ?>><?php echo number_format($likecount); ?></sup></div>
                            <div class="cost-action-label">Like</div>
                          </div>
                        </label>
                        <label for="r2" class="cost-action" id="dislike-link" style="cursor: pointer;margin-bottom: 0;">
                          <i class="fa fa-thumbs-down tr_dtl_com_desk"></i>
                          <div>
                            <input type="radio" id="r2" name="sc_type" value="1" <?php if ($sc_type == '1') echo "checked"; ?> style="opacity: 0;position: absolute;left:-9999em;">
                            <div class="cost-action-value tr_dtl_com_desk"><?php echo number_format($dislikecount); ?></div>
                            <div class="tr_dtl_com_mob"><i class="fa fa-thumbs-down"></i><sup><?php echo number_format($dislikecount); ?></sup></div>
                            <div class="cost-action-label">Dislike</div>
                          </div>
                        </label>
                        <div class="cost-action" style="cursor: default;">
                          <i class="fa fa-eye tr_dtl_com_desk"></i>
                          <div>
                            <div class="cost-action-value tr_dtl_com_desk"><?php echo number_format(getPostViews($post_id)); ?></div>
                            <div class="tr_dtl_com_mob"><i class="fa fa-eye"></i><sup><?php echo number_format(getPostViews($post_id)); ?></sup></div>
                            <div class="cost-action-label">Viewers</div>
                          </div>
                        </div>
                          <?php
                          //Get follow count
                          $follow_count=$wpdb->get_var("SELECT COUNT(*) FROM `social_follow` WHERE `sf_agent_id` = '{$creater->meta_value}'");
                          $followData = [];
                          $sf_user_id = isset($current_user->ID) ? $current_user->ID : '';
                          $metaDataForAgent = $wpdb->get_results("SELECT * FROM `wp_tourmeta` WHERE tour_id = '$tour_id' AND meta_key = 'userid'", ARRAY_A);

                          if (!empty($metaDataForAgent)) {
                              $sf_agent_id = isset($metaDataForAgent[0]['meta_value']) ? $metaDataForAgent[0]['meta_value'] : '';
                              $followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$sf_user_id' AND sf_agent_id = '$sf_agent_id'", ARRAY_A);
                          }
                          ?>
                        <div class="cost-action" onclick="submitFollowAgent()">
                          <?php if (!empty($followData)):?>
                          <i class="fas fa-user-minus tr_dtl_com_desk" title="Unfollow Travel Advisor"></i>
                          <?php else:?>
                            <i class="fa fa-user-plus tr_dtl_com_desk" title="Follow Travel Advisor"></i>
                          <?php endif;?>
                          <div>
                            <div class="cost-action-value tr_dtl_com_desk"><?php echo number_format($follow_count); ?></div>
                                <div class="tr_dtl_com_mob">
                                    <?php if (!empty($followData)):?>
                                        <i class="fas fa-user-minus" title="Unfollow Travel Advisor"></i>
                                    <?php else:?>
                                        <i class="fa fa-user-plus" title="Follow Travel Advisor"></i>
                                    <?php endif;?>
                                    <sup><?php echo number_format($follow_count); ?></sup>
                                </div>
                            <div class="cost-action-label">Followers</div>
                          </div>
                        </div>
                        <div class="cost-more">
                          <a style="color: #fff;"  href="<?php echo get_permalink($post_id); ?>">
	                          ltinerary Details
	                          <i class="fa fa-angle-down"></i>
                          </a>
                        </div>
                      </footer>
                        <script>
                          jQuery(document).ready(function () {
                            $("input[name=sc_type]").change(function () {
                              var url = 'https://www.travpart.com/English/submit-ticket/'
                              $.ajax({
                                type: "POST",
                                url: url,
                                data: jQuery('#social_connect_form').serialize(), // serializes the form's elements.
                                success: function (data)
                                {
                                  var result = JSON.parse(data);
                                  if (result.status) {
                                    alert(result.message);
                                    window.location.reload()
                                  } else {
                                    window.location.reload()
                                  }
                                }
                              });
                            });
                          });
                        </script>
                      </form>
                      <?php endif;?>
<!--                      end cost box footer-->
                                            </div>
<!--                  end cost box -->
                                            </div>
              <style>
              .mobilemenucolor{
					padding-right: 15px !important;
					padding-left: 15px !important;
				  }
                /* btn*/
                body {
                  background: #fff;
                }
                .big-btn .btn {
                  padding: 22px 15px;
                  min-width: 220px;
                  width: 100%;
                }
                .btn-notify-left {
                  position: relative;
                }
                .notify-img {
                  position: absolute;
                  z-index: 9999;
                  left: -220px;
                  top: 30px;
                }
                @media (max-width: 640px) {
                  .btn-notify-left {
                    margin-bottom: 60px!important;
                  }
                  .row,.col-md-12{
				  	padding: 0px !important;
				  	margin: 0px !important;
				  }
				  
                  .customer_sat{
				  	padding: 10px !important;
				  }
				  .font-lg{
				  	font-size:14px !important;
				  }
				  .bg_white{
				  	   width: 60px !important;
					    height: 25px !important;
					    right: 5px !important;
					    font-size: 14px;
					    top: 16px !important;
				  }
				  .display_table_1 img{
				  	width: auto !important; 
				   	height: 36px !important;
				  }
                  .display_table_1 {
                    width: 1%;
                  }
                  .font-lg1{
                    text-align: left;
                    font-size: 13px !important;
                    font-weight: 600;
                    margin-left: 5px;
                  }
				  #mega-menu-wrap-main-menu{
				  	padding-top	:0px !important 
				  }
				  .English-logo img {
					    height: 32px !important;
				  }
                  .customer_sat h1{
				  	font-size: 14px !important;
				  }
                  .notify-img {
                    left: 0;
                    top: auto;
                    bottom:-40px;
                  }
                  #h-t{
				  	min-height:45px !important;	
				  }
				  .booking-code h2{
				  	margin-top: 0px;
                    margin-bottom: 0px;
                    border-radius: 0px;
                    border: 1px solid black;
                    padding: 13px 9px;
                    font-size: 14px!important;
				  }
                }
              </style>
                                            <div class="book_btn_content_desk big-btn pdfcrowd-remove d-flex justify-content-center flex-wrap mt-4">

                        <?php if(is_user_logged_in() && !current_user_can('um_travel-advisor')) { ?>
                                                <div class="marval-make-payment m-1">
                          <span id="openRedeemMyCoupon" class="btn btn-blue" data-toggle="modal" data-target="#redeemMyCoupon"><img src="https://www.travpart.com/English/wp-content/uploads/2020/03/coupon_new.png">Redeem Coupon</span>
                        </div>
                        <div class="marval-make-payment m-1">
                                                    <a href="#" class="btn btn-blue savenote saveitinerary"><i class="fa fa-save mr-2"></i>Save Itinerary</a>
                                                </div>
						<?php } ?>
						<?php if(!is_user_logged_in() || !current_user_can('um_travel-advisor')) { ?>
                                                <div class="marval-make-payment m-1">
                                                    <a href="http://www.travpart.com/English/total-cost/?tourid=<?php echo $tour_id; ?>" class="btn btn-green savenote" onclick="clickCounter()"><i class="fa fa-credit-card mr-2"></i>Make a Payment</a>
                                                </div>
                        <?php } ?>
                                                <div class="marval-make-payment m-1 marval_savepdf">
                                                    <a href="#" onclick="savepdf()" class="btn btn-blue savenotetest" onclick="clickCounter()"><i class="fa fa-file-pdf mr-2"></i>Save as Pdf</a>
                                                </div>
                                                <?php
                        if ($can_modify_item) {
                          if(get_user_meta(wp_get_current_user()->ID, 'can_referral', true)==1) {
                            $referral_budget_used=$wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND type='invite'");
                            if(floatval($referral_budget_used) < floatval(get_option('referral_budget_limit'))) {
                        ?>
                                                  <div class="marval-make-payment m-1">
                                                    <a href="<?php echo (is_user_logged_in()) ? ('http://www.travpart.com/English/user/' . um_user('display_name') . '/?profiletab=pages') : '#'; ?>" target="_blank" class="btn btn-blue  btn-blue-g" id="share5-btn" data-toggle="tooltip" title="Let your referral knows your tour package and create the same thing !"><i class="fa fa-share-alt mr-2" onclick="clickCounter()"></i> Share to earn $5</a>
                                                  </div>
                                                <?php
                            }
                          }
                        }
                        ?>
                                              </div>
                                              <div class="big-btn d-flex justify-content-center">
<?php
$budget_used = $wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m')");
if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    //$urow=$wpdb->get_row("SELECT userid,access FROM `user` WHERE `username`='{$current_user->user_login}'");
    if (!empty($urow->access) && $urow->access == 1 && $can_modify_item) {
        if ($post_id !== false) {
            ?>
                                                            <div class="marval-make-payment m-1">
                                                                <a href="<?php echo site_url() . '/newpost/?fep_action=edit&fep_id=' . $post_id; ?>" class="btn btn-blue"><i class="fas fa-edit mr-2"></i> Modify the complete details</a>
                                                            </div>
            <?php
        } else if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_tour_sale` WHERE tour_id='{$tour_id}'") == 1) {
            easy_setcookie('btour_id', $tour_id);
            $budget_user_used = $wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE type = 'publish' AND DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND `userid`={$urow->userid}");
            if (floatval($budget_used) < floatval(get_option('budget_limit')) && floatval($budget_user_used) < floatval(get_option('budget_person_limit')))
                easy_setcookie('earn', (($has_hotel && $has_activity) ? '1' : '0'));
            else
                easy_setcookie('earn', 0);
            ?>
                                                            <div class="marval-make-payment m-1">
                                                                <a href="<?php echo site_url() . '/newpost/'; ?>" class="btn btn-blue"><i class="fas fa-edit mr-2"></i> Modify the complete details</a>
                                                            </div>
            <?php
        }
        else {
            $budget_user_used = $wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE type = 'publish' AND DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND `userid`={$urow->userid}");
            if (floatval($budget_used) < floatval(get_option('budget_limit')) && floatval($budget_user_used) < floatval(get_option('budget_person_limit')))
                $earn_criteria = true;
            else
                $earn_criteria = false;
            if ($earn_criteria === false) {
                ?>
                                                                <div class="marval-make-payment m-1 marvel_publish_t_sale">
                                                                    <a href="#" class="<?php if($total_price<=0) echo 'disabled '; ?>btn btn-blue publish_tour"><i class="fas fa-cloud-upload-alt mr-2"></i> Publish for sale </a>
                                                                </div>
                                                                <style type="text/css">
                                                                    .marval-make-payment.m-1.marvel_publish_t_sale {
                                                                        position: relative;
                                                                        right: 10.4%;
                                                                    }
                                                                    .marval-make-payment.m-1.marval_savepdf {
                                                                        position: absolute;
                                                                        right: 30%;
                                                                    }
                                                                    @media(max-width: 600px) {
                                                                      .marval-make-payment.m-1.marvel_publish_t_sale {
                                                                        right: 0%;
                                                                      }
                                                                    }
                                                                </style>
            <?php } else { ?>
                                                                <div class="marval-make-payment m-1">
                                                                    <a href="#" earn="1" class="<?php if($total_price<=0) echo 'disabled '; ?>btn btn-org publish_tour btn-notify-left" data-toggle="tooltip" title="<?php echo ($has_hotel && $has_activity) ? "Publish to earn $5. Ensure you’ve literally written a complete tour package detail on the next page" : "You don't meet the $5 criteria, please read the criteria again"; ?>"><img class="notify-img" src="https://www.travpart.com/English/wp-content/themes/bali/images/notify-next.png" alt=""><i class="fas fa-cloud-upload-alt mr-2"></i> Publish for sale to earn $5</a>
                                                                </div>

                                                                <p id="publish_message" style="color: red; text-align: center; display:none;"></p>
                                                                <div style="display:none;" class="publish_to_earn_message <?php echo ($has_hotel && $has_activity) ? '' : 'spu-open-18271'; ?>"></div>
                <?php
            }
        }
    }
}
?>


                                                <!--div class="marval-make-payment Publish">
                                                    <a href="#" class="btn btn-default">Publish</a>
                                                                            <input type="hidden" value="<?php echo $total_price; ?>" /-->

                                            </div>
                      
                      <!-- Coupon List -->
                      <div class="modal fade" id="redeemMyCouponModel" tabindex="-1" role="dialog" aria-labelledby="redeemMyCouponLabel" aria-hidden="true" style="z-index:1500;">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button onclick="clickCounter()" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="redeemMyCouponLabel">Redeem My Coupon</h4>
                            </div>
                            <div class="modal-body">
                            <?php
                            $couponList=$wpdb->get_results("SELECT * FROM `wp_coupons` WHERE used=0 AND user_id='".wp_get_current_user()->ID."' AND (tour_id='{$tour_id}' OR tour_id IS NULL) AND DATE_SUB(CURDATE(), INTERVAL {$tp_cpn_valdty} DAY)<=date(get_time)");
                            $checkedOption=true;
                            foreach($couponList as $row) {
                            ?>
                              <div class="radio">
                                <label>
                                  <input type="radio" name="coupon" value="<?php echo $row->id; ?>" <?php echo ($checkedOption)?'checked':''; ?> /><?php echo $row->discount.$row->figure.' '.ucfirst($row->type); ?> Discount
                                </label>
                              </div>
                            <?php
                              $checkedOption=false;
                            }
                            ?>
                            <?php if(empty($couponList)) { ?>
                              <p>No Coupon</p>
                            <?php } ?>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button id="chooseCoupon" type="button" class="btn btn-primary">Confirm</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <script>
                      jQuery(document).ready(function() {
                        jQuery("#openRedeemMyCoupon").click(function() {
                          jQuery("#redeemMyCouponModel").modal("show");
                        });
                        jQuery('#chooseCoupon').click(function() {
                          jQuery("#redeemMyCouponModel").modal("hide");
                          var data = {
                            'action': 'choose_coupon',
                            'tour_id': <?php echo $tour_id; ?>,
                            'coupon': $('input[name="coupon"]:checked').val()
                          };
                          jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', data, function (response) {
                            if (!!response.msg && response.msg=='success') {
                              location.reload();
                            }
                            console.log(response);
                          }, 'json');
                        });
                      });
                      </script>

                                            <!-- Comment Section Start -->
<?php
$tourCommentData = $wpdb->get_results("SELECT * FROM `social_comments` LEFFT JOIN wp_users ON wp_users.ID = scm_user_id WHERE scm_tour_id = '$tour_id'", ARRAY_A);
?>
                                            <div class="row custom_comment_hide">
                                                <!-- For List of comments START -->
                                                <div class="row">
                                                    <div class="col-md-3">

                                                    </div>
                                                    <div class="col-md-9 commentbox_wrapper">
<?php
if (!empty($tourCommentData)) {
    foreach ($tourCommentData as $comment) {
        ?>
                                                                <div class="row commentbox">
                                                                    <div class="col-md-12 c-name">
                                                                        <p>
        <?php echo isset($comment['user_login']) ? ucfirst($comment['user_login']) : ''; ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-12 c-txt">
                                                                        <p>
        <?php echo isset($comment['scm_text']) ? $comment['scm_text'] : ''; ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-12 c-date">
        <?php
        echo date('d M Y', strtotime($comment['scm_created']));
        ?>
                                                                    </div>
                                                                </div>
                                                                <hr />
        <?php
    }
} else {
    ?>
                                                            <h2>No Comment posted yet!</h2>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!-- For List of comments END -->
                                                <div class="row">
                                                    <div class="col-md-3"></div>
                                                    <div class="col-md-9 p-0">
                                                        <form method="POST" id="commentForm">
                                                            <input type="hidden" name="scm_tour_id" value="<?php echo $tour_id; ?>">
                                                            <input type="hidden" name="action" value="social_comments_form">
                                                            <textarea name="scm_text" required="true"></textarea>
                                                            <br />
                                                            <button onclick="clickCounter()" type="button" onclick="submitComments()" class="btn btn-default">Post Comment</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <script>
                                                function submitFollowAgent() {
                                                    var url = '<?php echo site_url() ?>/submit-ticket/';
                                                    var tour_id = '<?php echo $tour_id ?>';
                                                    $.ajax({
                                                        type: "POST",
                                                        url: url,
                                                        data: {tour_id: tour_id, action: 'social_follow'}, // serializes the form's elements.
                                                        success: function (data)
                                                        {
                                                            var result = JSON.parse(data);
                                                            if (result.status) {
                                                                alert(result.message);
                                                                window.location.reload()// show response from the php script.
                                                            } else {
                                                                window.location.reload()// show response from the php script.
                                                            }
                                                        }
                                                    });
                                                } 



                                                function submitComments() {
                                                    var form = $("#commentForm");
                                                    var url = '<?php echo site_url() ?>/submit-ticket/';

                                                    $.ajax({
                                                        type: "POST",
                                                        url: url,
                                                        data: form.serialize(), // serializes the form's elements.
                                                        success: function (data)
                                                        {
                                                            var result = JSON.parse(data);
                                                            if (result.status) {
                                                                alert(result.message);
                                                                window.location.reload()// show response from the php script.
                                                            } else {
                                                                alert(result.message);
                                                                window.location.reload()// show response from the php script.
                                                            }
                                                        }
                                                    });
                                                }
                                            </script>
                                            <!-- Comment Section End -->

<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
FB.init({appId: '2346029709012370', status: true, cookie: true,
xfbml: true});
};
(function() {
var e = document.createElement('script'); e.async = true;
e.src = document.location.protocol +
'//connect.facebook.net/en_US/all.js';
document.getElementById('fb-root').appendChild(e);
}());
</script>

<script type="text/javascript">
$(document).ready(function(){
$('#share_button').click(function(e){
e.preventDefault();

FB.ui({
  method: 'share',
  href: 'https://www.travpart.com/English/tour-details/?bookingcode=BALI<?php echo $tour_id; ?>',
}, function(response){
  /*if (response && !response.error_message) {

      alert('Successfully Shared with Facebook Account Feeds.');
      save_share_data(<?php echo $current_user->ID ?>,<?php echo $tour_id;  ?>);
    


    } else {
            alert('Tour Not shared');
    }*/
});


});
});
</script>

                                            <div class="share-box text-center mt-5">
                                                <!--  css & js -->
                                                <link rel="stylesheet" href="/English/wp-content/themes/bali/css/social-share-media.css">
                                                <script src="/English/wp-content/themes/bali/js/social-share-media.js"></script>
                                                <script>
                                                const socialmediaurls = GetSocialMediaSiteLinks_WithShareLinks({
                                                    'url': 'https://www.travpart.com/English/tour-details/?bookingcode=BALI<?php
                                                        echo $tour_id;
                                                        ;
                                                        ?>',
                                                    'title': 'I recommend you use the travcust app for making money to sell tour packages. Download and get $5 credit.'
                                                });
                                                function gotoshare(medianame)
                                                {
                          $.post('https://www.travpart.com/English/wp-content/themes/bali/api.php?action=sharecount', {'tour_id': <?php echo $tour_id; ?>});
                                                    if (medianame == 'whatsapp') {
                                                        window.open('https://web.whatsapp.com/send?text=' + encodeURIComponent('I recommend you use the travcust app for making money to sell tour packages. Check https://www.travpart.com/English/tour-details/?bookingcode=BALI<?php echo $tour_id; ?> and get $5 credit.'));
                                                    } else {
                                                        window.open(socialmediaurls[medianame]);
                                                    }
                                                }
                                                </script>
                                            
                                                <!-- Load Facebook SDK for JavaScript -->
 
                                                <div class="marvel-shareoption-view pdfcrowd-remove">
                                                    <!-- <a href="#"><span><img src="https://www.tourfrombali.cn/test-box/wp-content/themes/bali/images/images/facebook-logo.png" alt="facebook"></span></a> -->
                                                    <a href="#" onclick="gotoshare('google.plus')"><span><img src="https://www.travpart.com/Chinese/wp-content/themes/bali/images/images/google-plus.png" alt="google-plus"></span></a>
                                                    <!-- <a href="#"><span><img src="https://www.travpart.com/Chinese/wp-content/themes/bali/images/images/instagram.png" alt="instagram"></span></a> -->
                                                    <a href="#" onclick="gotoshare('pinterest')"><span><img src="https://www.travpart.com/Chinese/wp-content/themes/bali/images/images/pinterest.png" alt="pinterest"></span></a>
                                                    <a href="#" id="share_button"><span><img src="https://www.travpart.com/Chinese/wp-content/themes/bali/images/images/facebook-logo.png" alt="facebook"></span></a>
                                                   <!--  <a href="#" onclick="gotoshare('instagram')"><span><img src="https://www.travpart.com/Chinese/wp-content/themes/bali/images/images/instagram.png" alt="instagram"></span></a> -->
                                                    <a href="#" onclick="gotoshare('whatsapp')"><span><img width=24px src="https://www.travpart.com/English/wp-content/uploads/2018/10/whatsapp.png" alt="whatsapp"></span></a>
                                                </div>




                                                <div class="text-center pdfcrowd-remove p-3">
                                                    *Due to high meals inventory turnover rate, therefore by using this application, you agree that the chosen meals may or may not ran out of stock right after the purchase. If the chosen meal is out of stock after the purchase; we'll choose the most similar relevant meals for you.
                                                </div>
                                            </div>
                                            </div>
                                            </div>
                                            </div>


                                            </div>
<?php
// If comments are open or we have at least one comment, load up the comment template.
if (comments_open() || get_comments_number()) :
    comments_template();
endif;

if($show_top_button) {
?>


                                            <div id="menuresize">
                                                <div id="menuresize-header" class="menuresize-header">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    <i class="fa fa-caret-up"></i>
                                                </div>
                                            </div>
                                            <div id="left_menu" style="width:130px;"> 
                                                <ul id="left_menu_content">  

                                                    <li class="first">
<?php
$display_name = um_user('display_name');
echo $display_name; // prints the user's display name
?>
                                                    </li>
                                                        <?php
                                                        if (is_user_logged_in()) {
                                                            ?>
                                                        <li class="total"> <a href="http://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=pages"> <i class="fa fa-user"></i><br>
                                                                <span><?php echo $display_name; ?></span>
                                                            </a> </li>
                                                        <li>
    <?php
    $my_balance = $wpdb->get_row("SELECT balance FROM `balance` WHERE `userid`={$urow->userid}")->balance;
    $my_balance /= get_option("_cs_currency_USD");
    ?>
                                                            <a href="<?php echo site_url() . '/user/' . $current_user->user_login . '/?profiletab=settlement'; ?>"><i class="fas fa-wallet"></i> <br><b>My commission balance</b></a>
                                                            <span class="change_price original" or_pic="<?php echo round($my_balance); ?>">Rp <span class="price_span"><?php echo round($my_balance); ?></span></span>
                                                        </li>
<?php } else { ?>
                                                        <li onclick="callsignin()"><i class="fa fa-user"></i> <br /><div id="button_onescroll"><span>Login</span></div> </li>
                                                    <?php } ?>

                                                    <?php if (!empty($tour_id)) { ?>
                                                        <a href="/English/tour-details/?bookingcode=BALI<?php echo $tour_id; ?>"><li><i class="fa fa-shopping-cart"></i>   <div id="button_threescroll"><span>Journey</span></div></li></a>
                                                    <?php } ?>
                                                    <li> <a href="#sectionzero"> <i class="fa fa-hotel"></i>  <br><b>Hotel</b>
                                                    <?php if (is_user_logged_in()) { ?>
                                                                <span class="change_price original" or_pic="<?php
                                                                echo round($hotel_total_price / get_option("_cs_currency_USD"));
                                                                ?>"> Rp <span class="price_span"><?php echo number_format(round($hotel_total_price / get_option("_cs_currency_RMD")), 2); ?></span></span>
                                                            <?php } ?>
                                                        </a></li>      

                                                    <a href="#sectionone" class="mealsection" style="text-decoration:none;">
                                                        <li><i class="fas fa-utensils"></i> <br><b>Meal</b>
<?php if (is_user_logged_in()) { ?>
                                                                <span class="change_price original" or_pic="<?php
                                                                echo $meal_total_price * get_option('person_price_ration');
                                                                ?>">Rp <span class="price_span"><?php echo number_format($meal_total_price * get_option('person_price_ration'), 2); ?></span></span>
                                                            <?php } ?>
                                                        </li></a>       

                                                    <a href="#sectiontwo" class="carsection" style="text-decoration:none;">
                                                        <li> <i class="fa fa-truck"></i> <br><b>Vehicle</b>
<?php if (is_user_logged_in()) { ?>
                                                                <span class="change_price original" or_pic="<?php
                                                                echo $car_total_price * get_option('person_price_ration');
                                                                ?>">Rp <span class="price_span"><?php echo number_format($car_total_price * get_option('person_price_ration'), 2); ?></span></span>
                                                            <?php } ?>
                                                        </li></a>

                                                    <a href="#sectionthree" class="guidesection" style="text-decoration:none;">
                                                        <li><i class="fa fa-child"></i> <br><b>Tour Guide</b>
<?php if (is_user_logged_in()) { ?>
                                                                <span class="change_price original" or_pic="<?php
                                                                echo $guide_total_price * get_option('person_price_ration');
                                                                ?>">Rp <span class="price_span"><?php echo number_format($guide_total_price * get_option('person_price_ration'), 2); ?></span></span>
                                                            <?php } ?>
                                                        </li></a>

                                                    <a href="#sectionfour" class="rcmdplacesection" style="text-decoration:none;">
                                                        <li><i class="fa fa-map"></i> <br><b>Recommended Places</b>
<?php if (is_user_logged_in()) { ?>
                                                                <span class="change_price original" or_pic="<?php
                                                                echo empty($tourlist_total_price)?0:$tourlist_total_price;
                                                                ?>">Rp <span class="price_span"><?php echo $tourlist_total_price; ?></span></span>
                                                            <?php } ?>
                                                        </li></a>

                                                    <a href="#sectionfive" class="spasection" style="text-decoration:none;">
                                                        <li><i class="fa fa-fire"></i> <br><b>SPA</b>
<?php if (is_user_logged_in()) { ?>
                                                                <span class="change_price original" or_pic="<?php
                                                                echo $spa_total_price * get_option('person_price_ration');
                                                                ?>">Rp <span class="price_span"><?php echo number_format($spa_total_price * get_option('person_price_ration'), 2); ?></span></span>
                                                            <?php } ?>
                                                        </li></a>

                                                    <a href="#sectionsix" class="boatsection" style="text-decoration:none;">
                                                        <li><i class="fa fa-ship"></i> <br><b>Cruise ship and Helicopter</b>
<?php if (is_user_logged_in()) { ?>
                                                                <span class="change_price original" or_pic="<?php
                                                                echo $boat_total_price * get_option('person_price_ration');
                                                                ?>">Rp <span class="price_span"><?php echo number_format($boat_total_price * get_option('person_price_ration'), 2); ?></span></span>
                                                            <?php } ?>
                                                        </li></a>

                                                    <a href="#sectionseven" class="activitysection" style="text-decoration:none;">
                                                        <li><i class="fa fa-tint"></i> <br><b>Optional Activity</b>
<?php if (is_user_logged_in()) { ?>
                                                                <span class="change_price original" or_pic="<?php
                                                                echo $activity_total_price * get_option('person_price_ration');
                                                                ?>">Rp <span class="price_span"><?php echo number_format($activity_total_price * get_option('person_price_ration'), 2); ?></span></span>
                                                            <?php } ?>
                                                        </li></a>
                            
                          <a href="#sectioneight" class="optionallocation" style="text-decoration:none;">
                          <li><i class="fa fa-map-marker fa-fw"></i> <br><b>Optional Location</b><br>
                            <span class="location_number"><?php echo count($locationnames); ?></span> Location
                          </li></a>
                            
                          <a href="#sectioneight" class="manualitemsection" style="text-decoration:none;">
                          <li><i class="fa fa-tint"></i> <br><b>Manual Item</b>
                          <?php  if(is_user_logged_in()){ ?>
                          <span class="change_price original" or_pic="<?php echo intval($manualitem_total_price); ?>">Rp <span class="price_span"><?php echo number_format($manualitem_total_price,2);?></span></span>
                          <?php }  ?>
                          </li></a>

                                                    <li class="total-cost-li">
                                                        <div class="total-cost">
                                                            <strong>Total Cost:</strong>
                                                            <span class="change_price original" or_pic="<?php echo $total_price; ?>">Rp <span class="price_span"><?php echo $total_price; ?></span></span>
                                                        </div>
                                                    </li>


                                                </ul>
                                            </div>      
                                            <script>
                                                function resizeOpen() {
                                                    $('#left_menu').show(1000);
                                                    $('#closebutton').show(1000);
                                                    $('#menuresize').width(130);
                                                }

                                                function resizeClose() {
                                                    $('#left_menu').hide(1000);
                                                    $('#closebutton').hide(1000);
                                                    $('#menuresize').width(30);
                                                }

                                                jQuery(function () {
                                                    if(typeof(eval($().tooltip))=='function') {
                                                        $('[data-toggle="tooltip"]').tooltip();
                                                    }
                                                    else if(typeof(eval(jQuery().tooltip))=='function') {
                                                        jQuery('[data-toggle="tooltip"]').tooltip()
                                                    }
                                                })

                                            </script>
                                            <style>
                                                #menuresize{position: fixed;top:5%;left:0px;background:white;width:120px;height:5%;}
                                                #menuresize img{float:left;margin:10px 2px;opacity: 0.8}
                                                #menuresize img:hover{opacity: 1;cursor: pointer}
                                                #left_menu_content li{cursor: pointer}
                                                #left_menu_content{font-size:10px !important;}
                                                #left_menu_content li:hover{border-right:2px solid #3f8888}
                                                #left_menu{top:10%!important;z-index:9999;}



                                            </style>
                                            <script>
                                                function trial()
                                                {
                                                    $.ajax({
                                                        type: "GET",
                                                        url: '/English/wp-content/themes/bali/api2.php?action=save_this&tour_id=<?php echo $tour_id; ?>&userid=<?php echo $userid; ?>',
                                                        success: function (data) {
                                                            console.log(data);

                                                            if (data == 1)
                                                            {
                                                                $('#saved_message').text("Successfully saved");
                                                                $('#saved_message').show();
                                                                setTimeout("$('#saved_message').hide()", 1800);
                                                            }
                                                        }
                                                    });
                                                }

                                                $('#left_menu a').on('click', function () {

                                                    var target = $(this).attr('href');
                                                    $(target).addClass('defaultshow');

                                                    var top = $(target).offset().top;
                                                    top = top > 100 ? (top - 80) : 0;
                                                    $('html, body').animate({
                                                        scrollTop: top
                                                    }, {
                                                        duration: 500,
                                                        complete: function () {
                                                        }
                                                    });

                                                    // return false;
                                                });
                                            </script>

<?php
}

$_SESSION['savestatus'] = 0;
$_SESSION['ltaprice'] = $ltaprice;

$newsql = "INSERT INTO `itinerary` SELECT * FROM `touristspots`";
$newretval = mysqli_query($connection, $newsql);
if (!$newretval) {
    die('Could not get data: ' . mysqli_error());
}




/*
$sql3 = "DELETE from `touristspots`";
$retval3 = mysqli_query($connection, $sql3);

if (!$retval3) {
    die('Could not get data: ' . mysqli_error());
}*/

if ($_POST['action'] == 'save_this') {





    $newsql2 = "INSERT INTO `finalitinerary` SELECT * FROM `itinerary`";
    $newretval2 = mysqli_query($connection, $newsql2);
    if (!$newretval2) {
        die('Could not get data: ' . mysqli_error());
    }
    $_SESSION['savestatus'] = 1;

    $delsql2 = "DELETE FROM `itinerary`";
    $delretval2 = mysqli_query($connection, $delsql2);
    if (!$delretval2) {
        die('Could not get data: ' . mysqli_error());
    }
}
if ($_POST['action'] == 'save_pdf') {
    
}
?>


<?php
// Send mail to the vendors for bidding 
// $query = "SELECT option_value FROM travpart.wp_options WHERE option_name = '_cs_travpart_vendorlist' ";
// global $wpdb;
// $resultsData = $wpdb->get_results($query);
//    if(!empty($resultsData)){
//      foreach ( $resultsData as $data ) {
//        $arr = unserialize(unserialize($data->option_value)); 
//          foreach ($arr as $res) {
//            $vendor_Email = $res['email'];
//            $htmlContentpath = get_theme_file_path("emailtemplateforvendor.php");
//            $htmlContent = file_get_contents($htmlContentpath);
//            // $htmlContent = str_replace("vendorData_heading", "shubbham", $htmlContent); // to chnage the veriables name
//            sendmail($vendor_Email, 'Bidding Mail', $htmlContent);
//          }
//      }
//    }

/* 这个function有问题，2018年12月10日，我�?�是修改了一些文字，这个页�?�就�?工作了，�?�说是我改引起的，我�?�能把这个函数注释掉。--heleni
  function sendmail($to, $subject, $message){
  $key="Tt3At58P6ZoYJ0qhLvqdYyx21";
  $postdata=array('to'=>$to,'subject'=>$subject,'message'=>$message);
  $url="https://www.tourfrombali.com/api.php?action=sendmail&key={$key}";
  $ch=curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 60);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
  $data=curl_exec($ch);
  if(curl_errno($ch) || $data==FALSE){
  curl_close($ch);
  return FALSE;
  }else{
  curl_close($ch);
  return TRUE;
  }
  }
 */
?>

                                            <script>

                                                function modifyItem(section, name, item) {
<?php if ($can_modify_item) { ?>
                                                        var url = '<?php echo site_url() . '/travcust/#'; ?>' + section;
                                                        setCookie(name, item, 1);
                                                        window.location.href = url;
<?php } ?>
                                                }

                                                function array_tostring(arr, index, splitchar) {
                                                    if (!Array.isArray(arr))
                                                        return '';

                                                    var str = '';

                                                    for (var i = 0; i < arr.length - 1; i++) {
                                                        if (i != index)
                                                            str += arr[i] + splitchar;
                                                    }
                                                    return str;
                                                }

                                                function savepdf()
                                                {
                                                    history.pushState({}, document.title, document.URL<?php if (empty($_GET['bookingcode'])) echo "+'?bookingcode=BALI{$tour_id}'"; ?>);
                                                    window.location.href = "https://pdfcrowd.com/url_to_pdf/?width=410mm&height=350mm&?use_print_media=1";
                                                }

                                                var $j = jQuery.noConflict();
                                                $j("#publish").click(function () {
                                                    var pcontent = '<p><h4>Testing</h4></p>';
                                                    //var pcontent='hello';
                                                    var url = "https://www.travpart.com/index.php/testajax/";
                                                    console.log("publish clicked");



                                                    var redirect = 'https://www.travpart.com/';
                                                    redirectPost(redirect, {contents: pcontent, redirecturl: url});

                                                    // jquery extend function

                                                    function redirectPost(location, args)
                                                    {
                                                        var form = $j('<form></form>');
                                                        form.attr("method", "post");
                                                        form.attr("action", location);

                                                        $j.each(args, function (key, value) {
                                                            var field = $j('<input></input>');

                                                            field.attr("type", "hidden");
                                                            field.attr("name", key);
                                                            field.attr("value", value);

                                                            form.append(field);
                                                        });
                                                        $j(form).appendTo('body').submit();
                                                    }



                                                });

                                                jQuery(document).ready(function () {

                                                    <?php if ($can_modify_item) { ?>
                          //set the data of price
                                                    setCookie('car_price', parseInt($('.car .v2-foot .change_price').attr('or_pic')), 30);
                                                    setCookie('meal_price', parseInt($('.meal .v2-foot .change_price').attr('or_pic')), 30);
                                                    setCookie('guide_price', parseInt($('.guide .v2-foot .change_price').attr('or_pic')), 30);
                                                    setCookie('place_price', parseInt($('.place .v2-foot .change_price').attr('or_pic')), 30);
                                                    setCookie('spa_price', parseInt($('.spa .v2-foot .change_price').attr('or_pic')), 30);
                                                    setCookie('boat_price', parseInt($('.boat .v2-foot .change_price').attr('or_pic')), 30);
                                                    setCookie('activity_price', parseInt($('.activity .v2-foot .change_price').attr('or_pic')), 30);
                          <?php } ?>

<?php if ($can_modify_item) { ?>
                                                        $('.savenote').click(function (e) {
                                                            snevent = $(this);
                                                            e.preventDefault();
                                                            var notecontents = [];
                                                            $('.notecontent').each(function () {
                                                                notecontents.push($(this).val());
                                                            });
                                                            $.ajax({
                                                                type: "POST",
                                                                url: '/English/wp-content/themes/bali/api.php?action=addnote&tour_id=<?php echo $tour_id; ?>',
                                                                data: {contents: notecontents},
                                                                success: function (data) {
                                                                    console.log("add note success!");
                                                                    if (snevent.attr('href') != '#')
                                                                        window.location.href = snevent.attr('href');
                                                                }
                                                            });
                                                        });
<?php } ?>


                                                    $('.publish_tour').click(function () {
                                                        var is_publish_earn = $(this).attr('earn');
                                                        var num = prompt("Please enter the number of passengers");
                                                        if (num > 0 && num < 200)
                                                        {
                                                            $.ajax({
                                                                type: "GET",
                                                                url: '/English/wp-content/themes/bali/api.php?action=publish_tour&tour_id=<?php echo $tour_id; ?>&number_of_days=<?php echo $number_of_days; ?>&hotel_location=<?php echo $hotel_location; ?>&ratingUrl=<?php echo $ratingurl; ?>&userid=<?php echo $userid; ?>&enddate=<?php echo $enddate; ?>&netprice=<?php echo $net_price; ?>&username=<?php echo $usernm; ?>&userstatus=<?php echo $userstatus; ?>&number_of_people=' + num + '&earn=' + (is_publish_earn &<?php echo ($has_hotel && $has_activity) ? '1' : '0'; ?>),
                                                                success: function (data) {


                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: '/wp-content/themes/twentyseventeen/travpart-iframe.php?action=call_frame&tour_id=<?php echo $tour_id; ?>&userid=<?php echo $userid; ?>',
                                                                        success: function (idata) {
                                                                            //console.log(idata);
                                                                        }
                                                                    });
                                                                    if (data == 1)
                                                                    {
                                                                        $('#publish_message').text("Successfully released");
                                                                        $('#publish_message').show();
                                                                        setTimeout("$('#publish_message').hide()", 1800);
                                                                        /*if(is_publish_earn==1)
                                                                         {
                                                                         //$('#publish_to_earn_message').show();
                                                                         //setTimeout("$('#publish_to_earn_message').hide()", 1800);
                                                                         $('.publish_to_earn_message').click();
                                                                         }*/
                                                                        setCookie('btour_id', '<?php echo $tour_id; ?>', 30);
                                                                        setCookie('earn', (is_publish_earn &<?php echo ($has_hotel && $has_activity) ? '1' : '0'; ?>), 30);
                                                                        window.location.href = '<?php echo site_url(); ?>/newpost/';

                                                                    } else if (data == 2)
                                                                    {
                                                                        $('#publish_message').text("Released, please do not click repeatedly");
                                                                        $('#publish_message').show();
                                                                        setTimeout("$('#publish_message').hide()", 1800);
                                                                    } else
                                                                    {
                                                                        $('#publish_message').text("Publishing failed, please try again later");
                                                                        $('#publish_message').show();
                                                                        setTimeout("$('#publish_message').hide()", 1800);
                                                                    }
                                                                }
                                                            });

                                                        }
                                                    });

<?php if ($can_modify_item) { ?>
                                                        $('.place-item-trash').click(function () {
                                                            var item = $(this).parent().parent().parent().parent();
                                                            //var title=item.find('.v2-title').text().trim();
                                                            var place_names = getCookie('place_name').split('--');
                                                            var place_imgs = getCookie('place_img').split(',');
                                                            var place_dates = getCookie('place_date').split(',');
                                                            var place_times = getCookie('place_time').split(',');

                                                            //var index=$.inArray(title, place_names);
                                                            var index = parseInt($(this).attr('item'));

                                                            place_name = array_tostring(place_names, index, '--');
                                                            place_img = array_tostring(place_imgs, index, ',');
                                                            place_date = array_tostring(place_dates, index, ',');
                                                            place_time = array_tostring(place_times, index, ',');

                                                            setCookie('place_img', place_img, 30);
                                                            setCookie('place_name', place_name, 30);
                                                            setCookie('place_date', place_date, 30);
                                                            setCookie('place_time', place_time, 30);
                                                            setCookie('update', 1, 1);

                                                            item.remove();
                                                            var i = 0;
                                                            $('.place-item-trash').each(function () {
                                                                $(this).attr('item', i);
                                                                i++;
                                                            });
                                                        });

                                                        $('.tourlist-item-trash').click(function () {
                                                            var item = $(this).parent().parent().parent().parent();
                                                            var price = parseInt(item.find('.change_price').attr('or_pic'));

                                                            var tourlist_names = getCookie('tourlist_name').split('--');
                                                            var tourlist_imgs = getCookie('tourlist_img').split(',');
                                                            var tourlist_prices = getCookie('tourlist_price').split('--');
                                                            var tourlist_dates = getCookie('tourlist_date').split(',');
                                                            var tourlist_times = getCookie('tourlist_time').split(',');

                                                            var index = parseInt($(this).attr('item'));

                                                            tourlist_name = array_tostring(tourlist_names, index, '--');
                                                            tourlist_img = array_tostring(tourlist_imgs, index, ',');
                                                            tourlist_price = array_tostring(tourlist_prices, index, '--');
                                                            tourlist_date = array_tostring(tourlist_dates, index, ',');
                                                            tourlist_time = array_tostring(tourlist_times, index, ',');

                                                            setCookie('tourlist_name', tourlist_name, 30);
                                                            setCookie('tourlist_img', tourlist_img, 30);
                                                            setCookie('tourlist_price', tourlist_price, 30);
                                                            setCookie('tourlist_date', tourlist_date, 30);
                                                            setCookie('tourlist_time', tourlist_time, 30);
                                                            setCookie('update', 1, 1);

                                                            price = parseInt(item.parent().parent().find('.v2-foot .change_price').attr('or_pic')) - price;
                                                            item.parent().parent().find('.v2-foot .change_price').attr('or_pic', price);
                                                            update_total_price();
                                                            $(".header_curreny").change();
                                                            setCookie('place_price', price, 30);
                                                            item.remove();
                                                            var i = 0;
                                                            $('.tourlist-item-trash').each(function () {
                                                                $(this).attr('item', i);
                                                                i++;
                                                            });
                                                        });

                                                        $('.car-item-trash').click(function () {
                                                            var item = $(this).parent().parent().parent().parent();
                                                            var price = parseInt(item.find('.change_price').attr('or_pic'));

                                                            var car_types = getCookie('car_type').split(',');
                                                            var car_dates = getCookie('car_date').split(',');
                                                            var car_times = getCookie('car_time').split(',');

                                                            var index = parseInt($(this).attr('item'));

                                                            car_type = array_tostring(car_types, index, ',');
                                                            car_date = array_tostring(car_dates, index, ',');
                                                            car_time = array_tostring(car_times, index, ',');

                                                            setCookie('car_type', car_type, 30);
                                                            setCookie('car_date', car_date, 30);
                                                            setCookie('car_time', car_time, 30);
                                                            setCookie('update', 1, 1);

                                                            price = parseInt(item.parent().parent().find('.v2-foot .change_price').attr('or_pic')) - price;
                                                            item.parent().parent().find('.v2-foot .change_price').attr('or_pic', price);
                                                            update_total_price();
                                                            $(".header_curreny").change();
                                                            setCookie('car_price', price, 30);
                                                            item.remove();
                                                            var i = 0;
                                                            $('.car-item-trash').each(function () {
                                                                $(this).attr('item', i);
                                                                i++;
                                                            });
                                                        });

                                                        $('.meal-item-trash').click(function () {
                                                            var item = $(this).parent().parent().parent().parent();
                                                            var price = 0;
                                                            item.find('.change_price').each(function () {
                                                                price += parseInt($(this).attr('or_pic'));
                                                            });

                                                            var meal_types = getCookie('meal_type').split(',');
                                                            var meal_dates = getCookie('meal_date').split(',');
                                                            var meal_times = getCookie('meal_time').split(',');

                                                            var index = parseInt($(this).attr('item'));

                                                            meal_type = array_tostring(meal_types, index, ',');
                                                            meal_date = array_tostring(meal_dates, index, ',');
                                                            meal_time = array_tostring(meal_times, index, ',');

                                                            setCookie('meal_type', meal_type, 30);
                                                            setCookie('meal_date', meal_date, 30);
                                                            setCookie('meal_time', meal_time, 30);
                                                            setCookie('update', 1, 1);

                                                            price = parseInt(item.parent().parent().find('.v2-foot .change_price').attr('or_pic')) - price;
                                                            item.parent().parent().find('.v2-foot .change_price').attr('or_pic', price);
                                                            update_total_price();
                                                            $(".header_curreny").change();
                                                            setCookie('meal_price', price, 30);
                                                            item.remove();
                                                            var i = 0;
                                                            $('.meal-item-trash').each(function () {
                                                                $(this).attr('item', i);
                                                                i++;
                                                            });
                                                        });

                                                        $('.restaurant-item-trash').click(function () {
                                                            var item = $(this).parent().parent().parent().parent();

                                                            var restaurant_name = getCookie('restaurant_name').split('--');
                                                            var restaurant_local = getCookie('restaurant_local').split('--');
                                                            var restaurant_img = getCookie('restaurant_img').split('--');
                                                            var restaurant_price = getCookie('restaurant_price').split(',');
                                                            var restaurant_diners = getCookie('restaurant_diners').split(',');
                                                            var restaurant_date = getCookie('restaurant_date').split(',');
                                                            var restaurant_time = getCookie('restaurant_time').split(',');

                                                            var index = parseInt($(this).attr('item'));

                                                            restaurant_name = array_tostring(restaurant_name, index, '--');
                                                            restaurant_local = array_tostring(restaurant_local, index, '--');
                                                            restaurant_img = array_tostring(restaurant_img, index, '--');
                                                            restaurant_price = array_tostring(restaurant_price, index, ',');
                                                            restaurant_diners = array_tostring(restaurant_diners, index, ',');
                                                            restaurant_date = array_tostring(restaurant_date, index, ',');
                                                            restaurant_time = array_tostring(restaurant_time, index, ',');

                                                            setCookie('restaurant_name', restaurant_name, 30);
                                                            setCookie('restaurant_local', restaurant_local, 30);
                                                            setCookie('restaurant_img', restaurant_img, 30);
                                                            setCookie('restaurant_price', restaurant_price, 30);
                                                            setCookie('restaurant_diners', restaurant_diners, 30);
                                                            setCookie('restaurant_date', restaurant_date, 30);
                                                            setCookie('restaurant_time', restaurant_time, 30);
                                                            setCookie('update', 1, 1);

                                                            item.remove();
                                                            var i = 0;
                                                            $('.tourlist-item-trash').each(function () {
                                                                $(this).attr('item', i);
                                                                i++;
                                                            });
                                                        });

                                                        $('.guide-item-trash').click(function () {
                                                            var item = $(this).parent().parent().parent().parent();
                                                            var price = parseInt(item.find('.change_price').attr('or_pic'));

                                                            var popupguide_types = getCookie('popupguide_type').split(',');
                                                            var popupguide_dates = getCookie('popupguide_date').split(',');
                                                            var popupguide_times = getCookie('popupguide_time').split(',');

                                                            var index = parseInt($(this).attr('item'));

                                                            popupguide_type = array_tostring(popupguide_types, index, ',');
                                                            popupguide_date = array_tostring(popupguide_dates, index, ',');
                                                            popupguide_time = array_tostring(popupguide_times, index, ',');

                                                            setCookie('popupguide_type', popupguide_type, 30);
                                                            setCookie('popupguide_date', popupguide_date, 30);
                                                            setCookie('popupguide_time', popupguide_time, 30);
                                                            setCookie('update', 1, 1);

                                                            price = parseInt(item.parent().parent().find('.v2-foot .change_price').attr('or_pic')) - price;
                                                            item.parent().parent().find('.v2-foot .change_price').attr('or_pic', price);
                                                            update_total_price();
                                                            $(".header_curreny").change();
                                                            setCookie('guide_price', price, 30);
                                                            item.remove();
                                                            var i = 0;
                                                            $('.guide-item-trash').each(function () {
                                                                $(this).attr('item', i);
                                                                i++;
                                                            });
                                                        });

                                                        $('.spa-item-trash').click(function () {
                                                            var item = $(this).parent().parent().parent().parent();
                                                            var price = 0;
                                                            item.find('.change_price').each(function () {
                                                                price += parseInt($(this).attr('or_pic'));
                                                            });

                                                            var spa_types = getCookie('spa_type').split(',');
                                                            var spa_dates = getCookie('spa_date').split(',');
                                                            var spa_times = getCookie('spa_time').split(',');

                                                            var index = parseInt($(this).attr('item'));

                                                            spa_type = array_tostring(spa_types, index, ',');
                                                            spa_date = array_tostring(spa_dates, index, ',');
                                                            spa_time = array_tostring(spa_times, index, ',');

                                                            setCookie('spa_type', spa_type, 30);
                                                            setCookie('spa_date', spa_date, 30);
                                                            setCookie('spa_time', spa_time, 30);
                                                            setCookie('update', 1, 1);

                                                            price = parseInt(item.parent().parent().find('.v2-foot .change_price').attr('or_pic')) - price;
                                                            item.parent().parent().find('.v2-foot .change_price').attr('or_pic', price);
                                                            update_total_price();
                                                            $(".header_curreny").change();
                                                            setCookie('spa_price', price, 30);
                                                            item.remove();
                                                            var i = 0;
                                                            $('.spa-item-trash').each(function () {
                                                                $(this).attr('item', i);
                                                                i++;
                                                            });
                                                        });

                                                        $('.boat-item-trash').click(function () {
                                                            var item = $(this).parent().parent().parent().parent();
                                                            var price = 0;
                                                            item.find('.change_price').each(function () {
                                                                price += parseInt($(this).attr('or_pic'));
                                                            });

                                                            var boat_types = getCookie('boat_type').split(',');
                                                            var boat_dates = getCookie('boat_date').split(',');
                                                            var boat_times = getCookie('boat_time').split(',');

                                                            var index = parseInt($(this).attr('item'));

                                                            boat_type = array_tostring(boat_types, index, ',');
                                                            boat_date = array_tostring(boat_dates, index, ',');
                                                            boat_time = array_tostring(boat_times, index, ',');

                                                            setCookie('boat_type', boat_type, 30);
                                                            setCookie('boat_date', boat_date, 30);
                                                            setCookie('boat_time', boat_time, 30);
                                                            setCookie('update', 1, 1);

                                                            price = parseInt(item.parent().parent().find('.v2-foot .change_price').attr('or_pic')) - price;
                                                            item.parent().parent().find('.v2-foot .change_price').attr('or_pic', price);
                                                            update_total_price();
                                                            $(".header_curreny").change();
                                                            setCookie('boat_price', price, 30);
                                                            item.remove();
                                                            var i = 0;
                                                            $('.boat-item-trash').each(function () {
                                                                $(this).attr('item', i);
                                                                i++;
                                                            });
                                                        });

                                                        $('.activity-item-trash').click(function () {
                                                            var item = $(this).parent().parent().parent().parent();
                                                            var price = parseInt(item.find('.change_price').attr('or_pic'));

                                                            var eaiitems = getCookie('eaiitem').split(',');
                                                            var eainums = getCookie('eainum').split(',');
                                                            var eaidates = getCookie('eaidate').split(',');
                                                            var eaitimes = getCookie('eaitime').split(',');

                                                            var index = parseInt($(this).attr('item'));

                                                            eaiitem = array_tostring(eaiitems, index, ',');
                                                            eainum = array_tostring(eainums, index, ',');
                                                            eaidate = array_tostring(eaidates, index, ',');
                                                            eaitime = array_tostring(eaitimes, index, ',');

                                                            setCookie('eaiitem', eaiitem, 30);
                                                            setCookie('eainum', eainum, 30);
                                                            setCookie('eaidate', eaidate, 30);
                                                            setCookie('eaitime', eaitime, 30);
                                                            setCookie('update', 1, 1);

                                                            price = parseInt(item.parent().parent().find('.v2-foot .change_price').attr('or_pic')) - price;
                                                            item.parent().parent().find('.v2-foot .change_price').attr('or_pic', price);
                                                            update_total_price();
                                                            $(".header_curreny").change();
                                                            setCookie('activity_price', price, 30);
                                                            item.remove();
                                                            var i = 0;
                                                            $('.activity-item-trash').each(function () {
                                                                $(this).attr('item', i);
                                                                i++;
                                                            });
                                                        });

                                                        $('.hotel-item-trash').click(function () {
                                                            var item = $(this).parent().parent().parent().parent();
                                                            var price = parseInt(item.find('.change_price').last().attr('or_pic'));
                                                            var i = parseInt($(this).attr('item'));

                                                            price = parseInt(item.parent().parent().find('.v2-foot .change_price').attr('or_pic')) - price;
                                                            item.parent().parent().find('.v2-foot .change_price').attr('or_pic', price);
                                                            setHotelCookie('hotel_price', ($('#cs_USD').val() * price).toFixed(2), 30);
                                                            update_total_price();
                                                            $(".header_curreny").change();
                                                            item.remove();

                                                            $.ajax({
                                                                type: "GET",
                                                                url: '/English/wp-content/themes/bali/api.php?action=removehotelitem&tourid=<?php echo $tour_id; ?>&item=' + i,
                                                                success: function (data) {
                                                                    console.log('Hotel item removed');
                                                                }
                                                            });

                                                        });
                            
                            $('.adivaha-car-item-trash').click(function () {
                                                            var item = $(this).parent().parent().parent().parent();
                                                            var price = parseInt(item.find('.change_price').attr('or_pic'));

                                                            var index = parseInt($(this).attr('item'));

                                                            setCookie('update', 1, 1);

                                                            price = parseInt(item.parent().parent().find('.v2-foot .change_price').attr('or_pic')) - price;
                                                            item.parent().parent().find('.v2-foot .change_price').attr('or_pic', price);
                                                            update_total_price();
                                                            $(".header_curreny").change();
                                                            setCookie('car_price', price, 30);
                                                            item.remove();
                                                            
                              $.ajax({
                                                                type: "GET",
                                                                url: '/English/wp-content/themes/bali/api.php?action=removecaritem&tourid=<?php echo $tour_id; ?>&item=' + index,
                                                                success: function (data) {
                                                                    console.log('Adivaha car item removed');
                                                                }
                                                            });
                                                        });

                                                        $('.v2-list').on('click', '.manual-item-trash', function () {
                                                            var item = $(this).parent().parent().parent().parent();
                                                            var price = parseInt(item.find('.change_price').last().attr('or_pic'));
                                                            var i = parseInt($(this).attr('item'));

                                                            if (i == 0) {
                                                                item.remove();
                                                                existedManualItemBox = false;
                                                                return;
                                                            }

                              $('.manualitemsection .change_price').attr('or_pic', (parseInt($('.manualitemsection .change_price').attr('or_pic'))-price ));
                                                            price = parseInt(item.parent().parent().find('.v2-foot .change_price').attr('or_pic')) - price;
                                                            item.parent().parent().find('.v2-foot .change_price').attr('or_pic', price);
                                                            update_total_price();
                                                            $(".header_curreny").change();
                                                            item.remove();

                                                            $.ajax({
                                                                type: "GET",
                                                                url: '/English/wp-content/themes/bali/api.php?action=delManualItem&tourid=<?php echo $tour_id; ?>&item=' + i,
                                                                success: function (data) {
                                                                    console.log(data);
                                                                }
                                                            });

                                                        });
<?php } ?>

                                                });
                                            </script>

                                            <script>
                                                jQuery('img[src=""]').parent('.thumb-wrap').html('<div class="no-img"></div>');
                                            </script>
                                            <style>
                                                .user-tip {
                                                    position: absolute;
                                                    left: -180px;
                                                    top: 0;
                                                    width: 180px;
                                                    padding-right: 10px;
                                                    display: none; }
                                                .user-tip .widget-user {
                                                    box-shadow: 1.4px 3.7px 13px 0 rgba(0, 0, 0, 0.11);
                                                    font-size: 14px; }
                                                .user-tip .widget-user .avatar {
                                                    width: 72px;
                                                    height: 72px; }
                                                .user-tip .widget-user .username {
                                                    font-size: 15px; }
                                                .user-tip .widget-user a.view-profile {
                                                    font-size: 12px; }
                                                .user-tip .arrow {
                                                    display: block;
                                                    width: 0;
                                                    height: 0;
                                                    border: 10px solid transparent;
                                                    border-left: 10px solid #dcdcdc;
                                                    position: absolute;
                                                    right: -9px;
                                                    top: 35px; }
                                                .user-tip .arrow:before {
                                                    content: '';
                                                    display: block;
                                                    width: 0;
                                                    height: 0;
                                                    border: 8px solid transparent;
                                                    border-left: 8px solid #fff;
                                                    position: absolute;
                                                    top: -8px;
                                                    right: -6px; }

                                                .chat-tip {
                                                    position: absolute;
                                                    left: -180px;
                                                    top: 0;
                                                    width: 180px;
                                                    padding-right: 10px;
                                                    display: none; }
                                                .chat-tip span {
                                                    display: block;
                                                    border-radius: 3px;
                                                    box-shadow: 1.4px 3.7px 13px 0 rgba(0, 0, 0, 0.11);
                                                    background-color: #1a6d84;
                                                    font-size: 16px;
                                                    font-weight: 600;
                                                    font-style: normal;
                                                    font-stretch: normal;
                                                    line-height: normal;
                                                    letter-spacing: -0.2px;
                                                    text-align: center;
                                                    color: #ffffff;
                                                    padding: 20px;
                                                    text-align: center; }
                                                .chat-tip i {
                                                    display: block;
                                                    width: 0;
                                                    height: 0;
                                                    border: 10px solid transparent;
                                                    border-left: 10px solid #1a6d84;
                                                    position: absolute;
                                                    right: -5px;
                                                    top: 50%;
                                                    margin-top: -10px; }

                                                .tour-chat-bar {
                                                    position: fixed;
                                                    z-index: 99999;
                                                    right: 10px;
                                                    top: 50%;
                                                    margin-top: -150px;
                                                    width: 72px;
                                                    height: 130px;
                                                    border-radius: 36px;
                                                    box-shadow: 0 0 5px 0 rgba(18, 18, 18, 0.29);
                                                    background-color: #ffffff; }
                                                .tour-chat-bar .avatar-small {
                                                    display: block;
                                                    width: 42px;
                                                    height: 42px; }
                                                .tour-chat-bar a {
                                                    text-decoration: none !important; }
                                                .tour-chat-bar .user-bar {
                                                    display: block;
                                                    height: 65px;
                                                    border-radius: 35px 35px 0px 0px;
                                                    padding: 15px;
                                                    cursor: pointer; }
                                                .tour-chat-bar .user-bar:hover {
                                                    background: #f9f9f9; }
                                                .tour-chat-bar .user-bar:hover .user-tip {
                                                    display: block; }
                                                .tour-chat-bar .chat-bar {
                                                    display: block;
                                                    height: 65px;
                                                    border-radius: 0px 0px 35px 35px;
                                                    padding: 15px;
                                                    position: relative; }
                                                .tour-chat-bar .chat-bar:hover {
                                                    background: #f9f9f9; }
                                                .tour-chat-bar .chat-bar:hover .chat-tip {
                                                    display: block; }
                                                .tour-chat-bar .chat-circle {
                                                    width: 42px;
                                                    height: 42px;
                                                    border-radius: 50%;
                                                    background-color: #66cccc;
                                                    color: #fff;
                                                    text-align: center;
                                                    font-size: 24px;
                                                    display: flex;
                                                    align-items: center;
                                                    justify-content: center; }

                                            </style>
<?php
$avatar = um_get_user_avatar_data($creater->meta_value);
$creater_user = get_user_by('id', $creater->meta_value);
$create_row = $wpdb->get_row("SELECT `userid`,`region`,`timezone` FROM `user` WHERE `username`='{$creater_user->user_login}'");

$rattingDataByTour = $wpdb->get_results("SELECT * FROM agent_feedback WHERE af_agent_user_name = '$creater_user->user_login'", ARRAY_A);


$totalRatinng = 0;
$avgRatting = 0;
$totalEntry = 0;
if (!empty($rattingDataByTour)) {
    $totalEntry = count($rattingDataByTour);
}

foreach ($rattingDataByTour as $ratings) {
    $totalRatinng = $totalRatinng + $ratings['af_ratting'];
    $avgRatting = $totalRatinng / $totalEntry;
}
$avgRatting = ceil($avgRatting);
?>
<style>
    .fa-star-unchecked{
        color: black;
    }
</style>
<div class="tour-chat-bar pdfcrowd-remove">
    <div class="user-bar" onclick="location.href = '#'">
        <span class="avatar-small" style="background-image: url(<?php echo $avatar['url'] ?>)"></span>
        <div class="user-tip">
            <i class="arrow"></i>
            <div class="widget-user">
                <div class="avatar" style="background-image: url(<?php echo $avatar['url'] ?>)"></div>
                <div class="username"><?php echo $creater_user->display_name; ?></div>
                <div class="title">Deal Expert</div>
                <div class="rating-wrap">
                    <span class="trav-rating"><span><?php echo $avgRatting . '.0'; ?></span>
                        <i>
<?php
if ($avgRatting == 1) {
?>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star fa-star-unchecked"></i>
                                <i class="fa fa-star fa-star-unchecked"></i>
                                <i class="fa fa-star fa-star-unchecked"></i>
                                <i class="fa fa-star fa-star-unchecked"></i>
<?php
} else if ($avgRatting == 2) {
?>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star fa-star-unchecked"></i>
                                <i class="fa fa-star fa-star-unchecked"></i>
                                <i class="fa fa-star fa-star-unchecked"></i>
<?php
} else if ($avgRatting == 3) {
?>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star fa-star-unchecked"></i>
                                <i class="fa fa-star fa-star-unchecked"></i>
<?php
} else if ($avgRatting == 4) {
?>

                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star fa-star-unchecked"></i>
<?php
} else if ($avgRatting == 5) {
?>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
<?php
} else {
echo "No Rating yet!";
}
?>
                        </i>
                    </span>
                </div>  
<?php
if (!empty($create_row->region)) {
date_default_timezone_set($create_row->timezone);
?>
                    <p><i class="fa fa-map-marker"></i> <strong><?php echo $create_row->region; ?></strong> - <?php echo date('g:i A'); ?> local time</p>
                <?php } ?>
                <div class="sidebar-user-footer">
                    <a href="<?php echo home_url("/bio/?user={$creater_user->user_login}"); ?>" class="view-profile">VIEW PROFILE</a>
                </div>
            </div>
        </div>
    </div>
    <a class="chat-bar" href="<?php echo home_url("/travchat/user/addnewmember.php?user={$create_row->userid}"); ?>">
        <span class="chat-circle">
            <i class="fas fa-comment-dots"></i>
            <span class="chat-tip"><i></i><span>Live Chat Now</span></span>
        </span>
    </a>
</div>
<div class="total_cost_mob">
    <div class="total_cost_display_1">
        <i class="fas fa-calculator"></i>
    </div>
    <div class="total_cost_display_2">
        <span>Total Cost</span>
    </div>
    <div class="total_cost_display_3">
        <i class="fas fa-chevron-down"></i>
        <i class="fas fa-chevron-up"></i>
    </div>
</div>
<div class="total_cost_content_mob">
<div class="d-flex flex-column flex-sm-row mb-3">
                   
<!--                  start cost box-->
                    <div class="cost-box flex-fill">
<!--                      start cost box main-->
                      <div>
                      <div style="padding-bottom: 0;" class="bd">
                        <div class="cost-label">
                          <div>TOTAL COST : </div>
                          <div>APPLICABLE DISCOUNT :</div>
                          <div>TOTAL COST AFTER DISCOUNT : </div>
                          <div>TAX 1% : </div>
                          <div>NET PRICE : </div>
                        </div>
                        <div class="cost-value">
                            <?php if($discount_rp == 0){
                                $discount_rp = 1;
                            }?>
                          <div class="total_price"><span class="change_price" or_pic="<?php echo $total_price+$discount_rp*get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $total_price+$discount_rp*get_option('person_price_ration'); ?></span></span></div>
                          <div class="discount">-<span class="change_price" or_pic="<?php echo $discount+$discount_rp*get_option('person_price_ration'); ?>">Rp<span class="price_span"><?php echo $discount+$discount_rp*get_option('person_price_ration'); ?></span></span></div>
                          <div class="total_discount"><span class="change_price" or_pic="<?php echo $total_discount; ?>">Rp<span class="price_span"><?php echo $total_discount; ?></div>
                          <div class="tax"><span class="change_price" or_pic="<?php echo $tax; ?>">Rp<span class="price_span"><?php echo $tax; ?></span></span></div>
                          <div class="net_price"><span class="change_price" or_pic="<?php echo $net_price; ?>">Rp<span class="price_span"><?php echo $net_price; ?></span></span></div>
                        </div>
                      </div>
                        <div class="target-people" style="display:none;padding: 0 30px;text-align: right;">
                          <div><strong>You created for <span class="data-tour-peoples"></span> people</strong></div>
                          <div>you can buy this tour package for more than <span class="data-tour-peoples"></span> people</div>
                        </div>
                      </div>
<!--                      end cost box main-->
                      <?php if ($post_id !== false && get_post_status($post_id) == 'publish'):?>
                      <?php if ($tour_id != '' && is_user_logged_in()) {
                              $current_user = wp_get_current_user();
                              $sc_user_id = isset($current_user->ID) ? $current_user->ID : '';
                              $getDataForCheck = $wpdb->get_results("SELECT * FROM `social_connect` WHERE sc_tour_id = '$tour_id' AND sc_user_id = '$sc_user_id'", ARRAY_A);
                              $sc_type = isset($getDataForCheck[0]['sc_type']) ? $getDataForCheck[0]['sc_type'] : '';
                              //Get the count of like and dislike
                              $likecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$tour_id}' AND `sc_type` = 0");
                              $dislikecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$tour_id}' AND `sc_type` = 1");
                          }
                          ?>
<!-- start cost box footer-->
                      <form method="post" id="social_connect_form" class="pdfcrowd-remove">
                        <input type="hidden" name="action" value="social_connect_l_d">
                        <input type="hidden" name="sc_tour_id" value="<?php echo $tour_id; ?>">
                      <footer>
                        <label for="r1" class="cost-action" id="like-link"  style="cursor: pointer;margin-bottom: 0;">
                          <i class="fa fa-thumbs-up tr_dtl_com_desk"></i>
                          <div>
                            <input type="radio" id="r1" name="sc_type" value="0" <?php if ($sc_type == '0') echo "checked"; ?>  style="opacity: 0;position: absolute;left:-9999em;">
                            <div class="cost-action-value tr_dtl_com_desk" <?php if ($sc_type == '0') echo "style='color:red;'"; ?>><?php echo number_format($likecount); ?></div>
                            <div class="tr_dtl_com_mob"><i class="fa fa-thumbs-up">
                               </i><sup <?php if ($sc_type == '0') echo "style='color:red;'"; ?>><?php echo number_format($likecount); ?></sup></div>
                            <div class="cost-action-label">Like</div>
                          </div>
                        </label>
                        <label for="r2" class="cost-action" id="dislike-link" style="cursor: pointer;margin-bottom: 0;">
                          <i class="fa fa-thumbs-down tr_dtl_com_desk"></i>
                          <div>
                            <input type="radio" id="r2" name="sc_type" value="1" <?php if ($sc_type == '1') echo "checked"; ?> style="opacity: 0;position: absolute;left:-9999em;">
                            <div class="cost-action-value tr_dtl_com_desk"><?php echo number_format($dislikecount); ?></div>
                            <div class="tr_dtl_com_mob"><i class="fa fa-thumbs-down"></i><sup><?php echo number_format($dislikecount); ?></sup></div>
                            <div class="cost-action-label">Dislike</div>
                          </div>
                        </label>
                        <div class="cost-action" style="cursor: default;">
                          <i class="fa fa-eye tr_dtl_com_desk"></i>
                          <div>
                            <div class="cost-action-value tr_dtl_com_desk"><?php echo number_format(getPostViews($post_id)); ?></div>
                            <div class="tr_dtl_com_mob"><i class="fa fa-eye"></i><sup><?php echo number_format(getPostViews($post_id)); ?></sup></div>
                            <div class="cost-action-label">Viewers</div>
                          </div>
                        </div>
                          <?php
                          //Get follow count
                          $follow_count=$wpdb->get_var("SELECT COUNT(*) FROM `social_follow` WHERE `sf_agent_id` = '{$creater->meta_value}'");
                          $followData = [];
                          $sf_user_id = isset($current_user->ID) ? $current_user->ID : '';
                          $metaDataForAgent = $wpdb->get_results("SELECT * FROM `wp_tourmeta` WHERE tour_id = '$tour_id' AND meta_key = 'userid'", ARRAY_A);

                          if (!empty($metaDataForAgent)) {
                              $sf_agent_id = isset($metaDataForAgent[0]['meta_value']) ? $metaDataForAgent[0]['meta_value'] : '';
                              $followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$sf_user_id' AND sf_agent_id = '$sf_agent_id'", ARRAY_A);
                          }
                          ?>
                        <div class="cost-action" onclick="submitFollowAgent()">
                          <?php if (!empty($followData)):?>
                          <i class="fas fa-user-minus tr_dtl_com_desk" title="Unfollow Travel Advisor"></i>
                          <?php else:?>
                            <i class="fa fa-user-plus tr_dtl_com_desk" title="Follow Travel Advisor"></i>
                          <?php endif;?>
                          <div>
                            <div class="cost-action-value tr_dtl_com_desk"><?php echo number_format($follow_count); ?></div>
                                <div class="tr_dtl_com_mob">
                                    <?php if (!empty($followData)):?>
                                        <i class="fas fa-user-minus" title="Unfollow Travel Advisor"></i>
                                    <?php else:?>
                                        <i class="fa fa-user-plus" title="Follow Travel Advisor"></i>
                                    <?php endif;?>
                                    <sup><?php echo number_format($follow_count); ?></sup>
                                </div>
                            <div class="cost-action-label">Followers</div>
                          </div>
                        </div>
                        <div class="cost-more">
                          <a style="color: #fff;"  href="<?php echo get_permalink($post_id); ?>">
                              ltinerary Details
                              <i class="fa fa-angle-down"></i>
                          </a>
                        </div>
                      </footer>
                        <script>
                          jQuery(document).ready(function () {
                            $("input[name=sc_type]").change(function () {
                              var url = 'https://www.travpart.com/English/submit-ticket/'
                              $.ajax({
                                type: "POST",
                                url: url,
                                data: jQuery('#social_connect_form').serialize(), // serializes the form's elements.
                                success: function (data)
                                {
                                  var result = JSON.parse(data);
                                  if (result.status) {
                                    alert(result.message);
                                    window.location.reload()
                                  } else {
                                    window.location.reload()
                                  }
                                }
                              });
                            });
                          });
                        </script>
                      </form>
                      <?php endif;?>
<!--                      end cost box footer-->
                                            </div>
<!--                  end cost box -->
                                            </div>
                                        </div>
<div class="book_btn_mob">
    <div class="book_btn_display_1">
        <i class="far fa-credit-card"></i>
    </div>
    <div class="book_btn_display_2">
        <span>Book</span>
    </div>
    <div class="book_btn_display_3">
        <i class="fas fa-chevron-down"></i>
        <i class="fas fa-chevron-up"></i>
    </div>
</div>
<div class="book_btn_content_mob">
    <div class="big-btn pdfcrowd-remove d-flex justify-content-center flex-wrap mt-4">

                        <?php if(is_user_logged_in() && !current_user_can('um_travel-advisor')) { ?>
                                                <div class="marval-make-payment m-1">
                          <span id="openRedeemMyCoupon" class="btn btn-blue" data-toggle="modal" data-target="#redeemMyCoupon"><img src="https://www.travpart.com/English/wp-content/uploads/2020/03/coupon_new.png">Redeem Coupon</span>
                        </div>
                        <div class="marval-make-payment m-1">
                                                    <a href="#" class="btn btn-blue savenote saveitinerary"><i class="fa fa-save mr-2"></i>Save Itinerary</a>
                                                </div>
                        <?php } ?>
                        <?php if(!is_user_logged_in() || !current_user_can('um_travel-advisor')) { ?>
                                                <div class="marval-make-payment m-1">
                                                    <a href="http://www.travpart.com/English/total-cost/?tourid=<?php echo $tour_id; ?>" class="btn btn-green savenote" onclick="clickCounter()"><i class="fa fa-credit-card mr-2"></i>Make a Payment</a>
                                                </div>
                        <?php } ?>
                                                <div class="marval-make-payment m-1">
                                                    <a href="#" onclick="savepdf()" class="btn btn-blue savenotetest" onclick="clickCounter()"><i class="fa fa-file-pdf mr-2"></i>Save as Pdf</a>
                                                </div>
                                                <?php
                        if ($can_modify_item) {
                          if(get_user_meta(wp_get_current_user()->ID, 'can_referral', true)==1) {
                            $referral_budget_used=$wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND type='invite'");
                            if(floatval($referral_budget_used) < floatval(get_option('referral_budget_limit'))) {
                        ?>
                                                  <div class="marval-make-payment m-1">
                                                    <a href="<?php echo (is_user_logged_in()) ? ('http://www.travpart.com/English/user/' . um_user('display_name') . '/?profiletab=pages') : '#'; ?>" target="_blank" class="btn btn-blue  btn-blue-g" id="share5-btn" data-toggle="tooltip" title="Let your referral knows your tour package and create the same thing !"><i class="fa fa-share-alt mr-2" onclick="clickCounter()"></i> Share to earn $5</a>
                                                  </div>
                                                <?php
                            }
                          }
                        }
                        ?>
                                              </div>
</div>
<style type="text/css">
    .total_cost_content_desk.d-flex.flex-column.flex-sm-row.mb-3 div.cost-box {
        position: relative;
        //left: 52.5%;
    }
   .total_cost_mob{
    display: none;
   } 
   .book_btn_mob{
    display: none;
   }
   .total_cost_content_mob{
    display: none;
   }
   .total_cost_display_3 i.fas.fa-chevron-up{
    display: none;
   }
   .book_btn_display_3 i.fas.fa-chevron-up{
    display: none;
   }
   .book_btn_content_mob{
    display: none;
   }
    .custom_row_tour_details {
        display: none;
    }
   @media(max-width: 600px){
        .v2-box.mb-3.hotel_item_box div.v2-list div.v2-list-item div.v2-list-left div.thumb-wrap div.slider div.sss img{
            height: 200px;
            object-fit: cover;   
        }
        .v2-box.mb-3.hotel_item_box div.v2-list div.v2-list-item div.v2-list-right.flex-grow-1{
            margin-top: 200px;
        }
        .breadcrumb {
            display: none;
        }
        .total_cost_mob{
            display: block;
            background-color: #1a6d84;
            position: fixed !important;
            padding: 10px 20px;
            bottom: 55px;
            left: 0%;
            right: 0%;
            z-index: 999; 
            border-radius: 3px;
        }
        .total_cost_display_1 {
            width: 20%;
            display: inline-block;
            vertical-align: middle;
            position: relative;
            color: white;
            text-align: left;
        }
        .total_cost_display_2 {
            width: 58%;
            display: inline-block;
            vertical-align: middle;
            position: relative;
            color: white;
            text-align: center;
        }
        .total_cost_display_3 {
            width: 20%;
            display: inline-block;
            vertical-align: middle;
            position: relative;
            color: white;
            text-align: right;
        }
        .total_cost_display_1 i.fas.fa-calculator {
            font-size: 25px;
        }
        .total_cost_display_2 span{
            font-size: 25px;
        }
        .total_cost_display_3 i.fas.fa-chevron-down{
            font-size: 25px;
        }
        .total_cost_display_3 i.fas.fa-chevron-up{
            font-size: 25px;
        }
        .total_cost_content_mob{
            position: fixed;
            left: 0%;
            right: 0%;
            bottom: 5%;
            z-index: 999;
        }
        .total_cost_content_desk {
            display: none !important;
        }
        .book_btn_mob{
            display: block;
            background-color: #22b14c;
            position: fixed !important;
            padding: 10px 20px;
            bottom: 0%;
            left: 0%;
            right: 0%;
            z-index: 999; 
            border-radius: 3px;
        }
        .book_btn_display_1 {
            width: 20%;
            display: inline-block;
            vertical-align: middle;
            position: relative;
            color: white;
            text-align: left;
        }
        .book_btn_display_2 {
            width: 58%;
            display: inline-block;
            vertical-align: middle;
            position: relative;
            color: white;
            text-align: center;
        }
        .book_btn_display_3 {
            width: 20%;
            display: inline-block;
            vertical-align: middle;
            position: relative;
            color: white;
            text-align: right;
        }
        .book_btn_display_1 i.far.fa-credit-card {
            font-size: 25px;
        }
        .book_btn_display_2 span{
            font-size: 25px;
        }
        .book_btn_display_3 i.fas.fa-chevron-down{
            font-size: 25px;
        }
        .book_btn_display_3 i.fas.fa-chevron-up{
            font-size: 25px;
        }
        .book_btn_content_mob{
            background-color: white;
            position: fixed;
            left: 0%;
            right: 0%;
            bottom: 0%;
            z-index: 999;
        }
        .book_btn_content_desk{
            display: none !important;
        }
        
   }
</style>



<?php get_footer(); ?>