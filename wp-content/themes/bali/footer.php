<?php $siteurl = site_url();
?>
</div>
</div>
</div>
</div>

<?php
$current_user=wp_get_current_user();
if(get_user_meta($current_user->ID, 'show_login_game_popup', true)==1) {
    update_user_meta($current_user->ID, 'show_login_game_popup', 0);
	$login_days=get_user_meta($current_user->ID, 'login_days', true);
?>

<style>
    .game-modal {
        position: fixed;
        z-index: 999999999;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: rgba(0, 0, 0, 0.44);
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .game-modal .game-modal-content {
        padding: 15px;
        flex: 1 1 970px;
        max-width: 970px;
        margin: auto;
    }
.click_<?php echo $login_days; ?> .tick_mmark{
	display: none;
}
    .game-modal .game-modal-inner {
        border-radius: 5px;
        box-shadow: 0 0 15px 0 rgba(0, 0, 0, 0.26);
        background-color: #ffffff;
        position: relative;
    }

    .game-modal .game-modal-header {
        position: relative;
        z-index: 1;
        background-image: linear-gradient(to right, #1a6d84, #1a6d84, #1a6d84 99%);
        height: 80px;
        margin-bottom: 22px;
    }
    .game-modal .game-modal-header-inner {
        margin-left: auto;
        margin-right: auto;
        max-width: 371px;
        height: 102px;
        background: url("https://www.travpart.com/English/wp-content/themes/bali/images/game-modal-header-2.png");
        text-align: center;
        color: #fff;
    }
    .game-modal .game-modal-header-inner .header1 {
        font-size: 36px;
        font-weight: bold;
        padding-top: 10px;
    }
    .game-modal .game-modal-header-inner .header2 {
        font-size: 18px;
        font-weight: normal;
    }
    .game-modal .game-modal-close {
        cursor: pointer;
        position: absolute;
        right: 15px;
        top: 20px;
        z-index: 2;
        opacity: 1;
    }
.game-modal-close i{
	background: #fff;
    border-radius: 100%;
    color: #040404;
    border: 3px solid #c3c3c3;
    padding: 1px;
    padding: 5px 8px;
    font-size: 20px;
    vertical-align: middle;
}
    .game-modal .game-modal-close:hover {
        opacity: 0.6;
    }

    .game-modal-bd {
        padding: 30px;
    }

    .game-modal-bot {
        padding: 30px;
        text-align: center;
    }

    .btn-next {
        border-radius: 0px;
        box-shadow: 0 0 0 5px hsl(0, 0%, 87%);
        background-image: linear-gradient(to right, #0ed145, #0ed145) !important;
        color: #fff;
        font-size: 18px;
        font-weight: bold;
        min-width: 120px;
    }

    .btn-next:hover,
    .btn-next:active,
    .btn-next:focus {
        color: #fff;
    }

    .bouns-list {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }

    .bouns-list .bouns-item {
        font-size: 16px;
        font-weight: bold;
        text-align: center;
        color: #333333;
        margin: 15px;
    }

    .bouns-list .icon-wrap {
        position: relative;
    }

    .bouns-list .icon-wrap .fa-check-circle {
        font-size: 36px;
        color: #00a8f3;
        position: absolute;
        z-index: 2;
        left: 60px;
        top: 60px;
        box-shadow: 0 0 0px 2px #fff;
        border-radius: 50%;
    }

    .bouns-list .trophy {
        position: relative;
        z-index: 1;
        display: inline-block;
        width: 80px;
        height: 80px;
        border: solid 6px #ffe26b;
        background-color: #f8b64c;
        border-radius: 50%;
        position: relative;
        margin-bottom: 10px;
        margin-top: 10px;
    }

    .bouns-list .trophy .fa-trophy {
        font-size: 42px;
        color: #ffe26b;
        position: absolute;
        left: 10px;
        top: 12px;
    }

    @media (max-width: 767px) {
        .bouns-list {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
        }

        .bouns-list .bouns-item {
            font-size: 16px;
            font-weight: bold;
            text-align: center;
            color: #333333;
            margin: 10px;
        }

        .bouns-list .icon-wrap .fa-check-circle {
            font-size: 14px;
            left: 30px;
            top: 30px;
        }

        .bouns-list .trophy {
            width: 40px;
            height: 40px;
            border: solid 2px #ffe26b;
            margin-bottom: 5px;
            margin-top: 5px;
        }

        .bouns-list .trophy .fa-trophy {
            font-size: 18px;
            left: 8px;
            top: 8px;
        }

        .game-modal-bd {
            padding: 15px;
        }

        .game-modal-bot {
            padding: 15px;
            padding-top: 0;
        }
    }
</style>

<div id="login-bouns-modal" class="game-modal">
    <div class="game-modal-content">
        <div class="game-modal-inner">
            
            <i class="game-modal-close" onclick="jQuery('#login-bouns-modal').hide();">
            	<i class="fas fa-times"></i>
            </i>
            
            <div class="game-modal-header">
                <div class="game-modal-header-inner">
                    <div class="header1">Bonus</div>
                    <div class="header2">My points：<?php echo intval(get_user_meta(wp_get_current_user()->ID, 'game_points', true)); ?></div>
                </div>
            </div>
            <div class="game-modal-bd">
                <div class="bouns-list">
                    <div class="bouns-item click_1">
                        <div class="text">Day 1</div>
                        <div class="icon-wrap">
                            <i class="trophy"><i class="fa fa-trophy"></i> </i>
                            <i class="fa fa-check-circle"></i>
                        </div>
                        <div class="text">x <?php echo $tp_pts_lg_dly=intval(get_option('_travpart_vo_option')['tp_pts_lg_dly']); ?></div>
                    </div>
                    <div class="bouns-item click_2">
                        <div class="text">Day 2</div>
                        <div class="icon-wrap">
                            <i class="trophy"><i class="fa fa-trophy"></i> </i>
							
							<div class="tick_mmark"> 
								<?php if($login_days>=2 || $login_days==0) { ?>
	                            	<i class="fa fa-check-circle"></i>
								<?php } ?>
							</div>
                        </div>
                        <div class="text">x <?php echo $tp_pts_lg_dly; ?></div>
                    </div>
                    <div class="bouns-item click_3">
                        <div class="text">Day 3</div>
                        <div class="icon-wrap">
                            <i class="trophy"><i class="fa fa-trophy"></i> </i>
							<div class="tick_mmark"> 
								<?php if($login_days>=3 || $login_days==0) { ?>
	                            	<i class="fa fa-check-circle"></i>
								<?php } ?>
							</div>
                        </div>
                        <div class="text">x <?php echo $tp_pts_lg_dly; ?></div>
                    </div>
                    <div class="bouns-item click_4">
                        <div class="text">Day 4</div>
                        <div class="icon-wrap">
                            <i class="trophy"><i class="fa fa-trophy"></i> </i>
							<div class="tick_mmark"> 
								<?php if($login_days>=4 || $login_days==0) { ?>
	                            <i class="fa fa-check-circle"></i>
								<?php } ?>
							</div>
                        </div>
                        <div class="text">x <?php echo $tp_pts_lg_dly; ?></div>
                    </div>
                    <div class="bouns-item click_5">
                        <div class="text">Day 5</div>
                        <div class="icon-wrap">
                            <i class="trophy"><i class="fa fa-trophy"></i> </i>
                            <div class="tick_mmark"> 
								<?php if($login_days>=5 || $login_days==0) { ?>
	                            	<i class="fa fa-check-circle"></i>
								<?php } ?>
							</div>
                        </div>
                        <div class="text">x <?php echo $tp_pts_lg_dly; ?></div>
                    </div>
                    <div class="bouns-item click_6">
                        <div class="text">Day 6</div>
                        <div class="icon-wrap">
                            <i class="trophy"><i class="fa fa-trophy"></i> </i>
							<div class="tick_mmark"> 
								<?php if($login_days>=6 || $login_days==0) { ?>
	                            <i class="fa fa-check-circle"></i>
								<?php } ?>
							</div>
                        </div>
                        <div class="text">x <?php echo $tp_pts_lg_dly; ?></div>
                    </div>
                    <div class="bouns-item">
                        <div class="text">Day 7</div>
                        <div class="icon-wrap">
                            <i class="trophy"><i class="fa fa-trophy"></i> </i>
							<div class="tick_mmark"> 
								<?php if($login_days==0) { ?>
	                            	<i class="fa fa-check-circle"></i>
								<?php } ?>
							</div>
                        </div>
                        <div class="text">x <?php echo $tp_pts_lg_dly; ?></div>
                    </div>
					<div class="bouns-item">
                        <div class="text">Weekly</div>
                        <div class="icon-wrap">
                            <i class="trophy"><i class="fa fa-trophy"></i> </i>
							<?php if($login_days==0) { ?>
                            <i class="fa fa-check-circle"></i>
							<?php } ?>
                        </div>
                        <div class="text">x <?php echo intval(get_option('_travpart_vo_option')['tp_pts_lg_wkly']); ?></div>
                    </div>
                </div>
            </div>
            <?php
                $current_user = wp_get_current_user();
            ?>
            <div class="game-modal-bot">
                <a href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=vouchers" onclick="jQuery('#login-bouns-modal').hide();" class="btn btn-next">Next</a>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php
if (isset($_GET['user_token']) && ($_GET['user_token'] != '')) {
    ?>

    <style>
        #mega-toggle-block-1{
            display: none !important;
        }
    </style>
<?php
}
//print_r($_GET['profiletab']);
//exit;
if (isset($_GET['profiletab']) && ($_GET['profiletab'] == 'profile' || $_GET['profiletab'] == 'bookingCodeList' || $_GET['profiletab'] == 'referal' || $_GET['profiletab'] == 'settlement' || $_GET['profiletab'] == 'verification' || $_GET['profiletab'] == 'transaction')) {
    ?>
    <style>
        .um{
            opacity: 1 !important;
        }
    </style>
    <?php
}
?>
<?php if($_GET['device']!='app') { ?>
<div class="mobileonly mobileapp" style="display: none;">
    <div class="container">
        <div class="row">       
            <div class="col-xs-12">
                <table class="mobileappbg" style="width: 100%">
                        <tr style="padding:5px">
                            <td style="padding:10px"><a href="https://www.travpart.com/mobileapp/"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/android.png" alt="Download our app" class="androidicon"></a></td>                    
                            <td style="padding:10px"><a href="https://www.travpart.com/mobileapp/">Download our mobile app ★★★★★</a></td>
                            <td id="closemobileapp" style="vertical-align: middle;text-align: right"><span style="padding:10px;border:1px solid white; -webkit-border-radius: 50%;-moz-border-radius: 50%;border-radius: 50%;">X</span></td>
                        </tr>
                    </table>  
            </div>               
        </div>
    </div>
</div>

<div id="footer" class="pdfcrowd-remove">
        <div class="container">
            <div class="row mobilefooter mobilemargintop">
                <div class="col-md-3 col-sm-12">
                    <b>Powered By TourFromBali</b>
                    <br>
                    <img src="https://www.travpart.com/English/wp-content/uploads/2018/08/tourfrombaliimg.png" width="auto" style="height:60px!important" />
                    <br>
                    <a href="https://www.travpart.com/English/wp-content/themes/bali/App/travpart_english.apk"><img src="https://www.travpart.com/wp-content/themes/aberration-lite/images/dl-our-app.png" alt="download our app" width="170"></a>
                </div>
                <div class="col-md-3 mobilenone"><b>DISCOVER</b>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo $siteurl; ?>#">Americas</a></li>
                        <li><a href="<?php echo $siteurl; ?>#">Europe</a></li>
                        <li><a href="<?php echo $siteurl; ?>#">South East Asia</a></li>
                        <li><a href="<?php echo $siteurl; ?>#">Middle East</a></li>
                        </ul>
                </div>
                <div class="col-md-3 mobilenone"><b>ABOUT US</b>
                    <ul class="list-unstyled">
                        <li><a href="https://www.travpart.com/English/about-us/" target="_blank">About us</a></li>
                        <li><a href="https://www.travpart.com/blog-design/" target="_blank">Blog</a></li>
                        <li><a class="contact_us" href="https://www.travpart.com/English/contact/" target="_blank">Custom</a></li>
                        <li><a href="<?php echo $siteurl; ?>/English/travchat/">Travchat</a></li>
                        <li><a href="<?php echo $siteurl; ?>/English/Travcust">Travcust</a></li>
                           </ul>
                </div>
                <div class="col-md-3 mobilenone"><b>HELP CENTER</b>
                    <ul class="list-unstyled">
                        <li><a href="https://www.travpart.com/English/contact/" target="_blank">Contact</a></li>
                        <li><a href="https://www.travpart.com/English/termsofuse/" target="_blank">Privacy Policy and Terms of Service</a></li>
                        <li><a href="https://www.travpart.com/English/sitemap/" target="_blank">Sitemap</a></li>
                        </ul>
                </div>

                <div class="col-md-12 mobilenone">
                <table class="footertable">
                    <tr>
                        <td>INDONESIA Bali Office :</td>
                        <td>I Alamanda Office 5th floor, Jl. By Pass Ngurah Rai No. 67, Br. Kerthayasa, Kedonganan, Kuta, Kedonganan, Kuta, Badung Regency, Bali 80361, Indonesia</td>
                    </tr>
                    <tr>
                        <td>INDONESIA Jakarta Office:</td>
                        <td>Wisma 46, Jl. Jend. Sudirman, Karet Tengsin, Tanah Abang, Central Jakarta City, Jakarta 10250, Indonesia</td>
                    </tr>
                    <tr>
                        <td>CHINA Office:</td>
                        <td>9th Floor, Office 960, No. 525 Xizang Middle Road, Huangpu District, Shanghai</td>
                    </tr>
                </table>
                </div>
            </div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div class="row mobilefooter">
                <div class="col-md-8 col-sm-12">
                    All rights reserved © 2019 travpart.com
                </div>
                <div class="col-md-4 mobilenone">
                    <?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
                </div>
            </div>
            <div class="mobileonly">
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
            </div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
        </div>
         <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=tyEgx3sntsb8OZeInQTX2HmAHKe5I2PrWE4pSD3ROhLDokhHsHIOc6nqY6He"></script></span>
    </div>
    <?php } ?>

<!--<div id="footertop">
    <div class="container">
        <div class="row">
            <div class="col-md-8 social_icon col-xs-12">
<?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
            </div>
        </div>
    </div>
</div>-->

<!--<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row mobilenone">
                    <div class="col-md-3" style="display:none;">
                        <ul class="list-unstyled">
                            <li><a href="<?php echo $siteurl; ?>/Travchat/">Travchat</a></li>
                            <li><a href="<?php echo $siteurl; ?>/west-java/">West Java</a></li>
                            <li><a href="<?php echo $siteurl; ?>/east-java/">East Java</a></li>
                            <li><a href="<?php echo $siteurl; ?>/central-java/">Central Java</a></li>
                            <li><a href="<?php echo $siteurl; ?>/indonesia-islands/">Islands</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-unstyled">
                            <li><a href="https://www.travpart.com/blog/" target="_blank">Blog</a></li>
                            <li><a class="contact_us" href="https://www.travpart.com/English/contact/" target="_blank">Custom</a></li>
                            <li><a href="http://www.tourfrombali.com/?p=2967" target="_blank">All reviews</a></li>-->
                            <!--<li><a href="http://www.tourfrombali.com/?p=2965" target="_blank">Leave review</a></li>-->
                        <!--</ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-unstyled">
                            <li><a href="https://www.tourfrombali.com/about-us/" target="_blank">About us</a></li>
                            <li><a href="https://www.tourfrombali.com/partners" target="_blank">Partner</a></li>
                            <li><a href="https://www.tourfrombali.com/contact/" target="_blank">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-unstyled">
                            <li><a href="https://www.travpart.com/English/privacy-policy/" target="_blank">Privacy Policy</a></li>
                            <li><a href="https://www.tourfrombali.com/terms-conditions" target="_blank">Terms and conditions</a></li>
                            <li><a href="https://www.travpart.com/English/sitemap/">Sitemap</a></li>
                            <li><a href="http://www.tourfrombali.com/insurance">Insurance</a></li>
                        </ul>
                    </div>
                </div>-->
                <!--<div id="f-cp" class="row" >
                    <div class="col-md-9">
                        <div class="mobilenone"><span id="f-ph" style="font-size:21px">
                                <div id="footer_skype">
                                    <div id="SkypeButton_Call_ilham.bachtiar205_1"  >

                                    </div>												 		
                                </div>	
                                <span id="footer_q">  </span>
                            </span> 

                            <table style="width:100%;border:none" class="footertable">
                                <tr>
                                   <td style="width:30%;vertical-align: top;text-align: left!important;background:#1A6D84;border:none">INDONESIA Bali Office :</td>
                                    <td style="text-align: left!important;background:#1A6D84;border:none">I Alamanda Office 5th floor, Jl. By Pass Ngurah Rai No. 67, Br. Kerthayasa, Kedonganan, Kuta, Kedonganan, Kuta, Badung Regency, Bali 80361, Indonesia</td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;text-align: left!important;border:none;background:#1A6D84;">INDONESIA Jakarta Office:</td>
                                    <td style="text-align: left!important;background:#1A6D84;border:none">Wisma 46, Jl. Jend. Sudirman, Karet Tengsin, Tanah Abang, Central Jakarta City, Jakarta 10250, Indonesia</td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;text-align: left!important;background:#1A6D84;border:none">CHINA Office:</td>
                                    <td style="text-align: left!important;background:#1A6D84;border:none">9th Floor, Office 960, No. 525 Xizang Middle Road, Huangpu District, Shanghai</td>
                                </tr>
                            </table>
                        </div>     
                        <div>&nbsp;</div>     
                        <div id="f-cpy">All rights reserved © 2019 travpart.com</div>
                    </div> 
                    <div class="col-md-3">                     
                    </div> 
                </div>
            </div>
            <div class="col-md-4 text-center">
                <ul id="f-qr" class="list-inline">

            </div>
        </div>

        <div class="row mobilenone">
            <div class="col-md-12">
                <ul id="f-lgs" class="list-inline">
                    <li><a href="https://www.indonesia.travel/gb/en/home"><img src="<?php bloginfo('template_url'); ?>/images/indonesia.jpg" alt=""></a></li>
                    <li><a href="https://www.aig.co.id/en/home"><img src="<?php bloginfo('template_url'); ?>/images/aig.jpg" alt=""></a></li>
                    <li><a href="https://www.worldnomads.com/travel-insurance/?gclid=Cj0KEQiAm-CyBRDx65nBhcmVtbIBEiQA7zm8lVwLUC-tqBVMi9DDJKlLm4EFEJmIrn1LPaqlo1mfYPMaAmuG8P8HAQ"><img src="<?php bloginfo('template_url'); ?>/images/world-nomads.png" alt=""></a></li>
                    <li><a href="https://kadinjakarta.or.id/" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/Kadin.png" alt=""></a></li>
                    <li><a href="https://kadinjakarta.or.id/" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/kadin-2.jpg" alt=""></a></li>
                </ul>
            </div>
        </div>

    </div>
</div>-->
<!--<span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=tyEgx3sntsb8OZeInQTX2HmAHKe5I2PrWE4pSD3ROhLDokhHsHIOc6nqY6He"></script></span>-->

<?php wp_footer(); ?>

<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1044452724;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script async="async" defer="defer"  type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<!--<div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1044452724/?value=0&amp;guid=ON&amp;script=0"/>
</div>-->
</noscript>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ4N2X6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<script>
$(document).ready(function () {


		//$('.click_'+<?php echo $login_days; ?>).click(function(){
			var da = '<?php echo $login_days; ?>';
			if(da==null || da==''){
			}else{
				$('.click_'+da).find('.tick_mmark').html('<i class="fa fa-check-circle"></i>').show(500);
			}
		//});
	
		function setCookie(cname,cvalue,exdays) {
			  var d = new Date();
			  d.setTime(d.getTime() + (exdays*24*60*60*1000));
			  var expires = "expires=" + d.toGMTString();
			  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
			}

			function getCookie(cname) {
			  var name = cname + "=";
			  var decodedCookie = decodeURIComponent(document.cookie);
			  var ca = decodedCookie.split(';');
			  for(var i = 0; i < ca.length; i++) {
			    var c = ca[i];
			    while (c.charAt(0) == ' ') {
			      c = c.substring(1);
			    }
			    if (c.indexOf(name) == 0) {
			      return c.substring(name.length, c.length);
			    }
			  }
			  return "";
			}

			function checkCookie() {
			  var user=getCookie("banner_closed");
			  if (user != "") {
			    //alert("Welcome again " + user);
			  } else {
			     //user = prompt("Please enter your name:","");
			     //if (user != "" && user != null) {
			       setCookie("banner_closed", '1', 1);
			     //}
			  }
			}
			var user=getCookie("banner_closed");
			if(user==1){
				$('.mobileapp').hide();
			}else{ 
				$('.mobileapp').show();
			}
        $('#closemobileapp').click(function () {
            $('.mobileapp').hide();
            checkCookie();
            
            $('.mobileonly').addClass('mobilenone');
        });

<?php
if (isset($_GET['user_token']) && ($_GET['user_token'] != '')) {
    ?>
            $('#username-14064').val('Agent2');
            $('#user_password-14064').val('IndonesiaWeb14');
            $('#um-submit-btn').trigger('click');
<?php } ?>
    });
</script>

</body>
</html>