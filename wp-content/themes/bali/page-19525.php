<?php
get_header();
/* Template Name: BIO info */

global $wpdb;
?>
<style>
    /* Rating Star Widgets Style */
    .rating-stars ul {
        list-style-type:none;
        padding:0;

        -moz-user-select:none;
        -webkit-user-select:none;
    }
    .rating-stars ul > li.star {
        display:inline-block;

    }

    /* Idle State of the stars */
    .rating-stars ul > li.star > i.fa {
        font-size:1.0em; /* Change the size of the stars */
        color:#ccc; /* Color on idle state */
    }

    /* Hover state of the stars */
    .rating-stars ul > li.star.hover > i.fa {
        color:#FFCC36;
    }

    /* Selected state of the stars */
    .rating-stars ul > li.star.selected > i.fa {
        color:#FF912C;
    }

    .rating label input:focus:not(:checked) ~ .icon:last-child {
        color: #000;
        text-shadow: 0 0 5px #09f;
    }
    .private-feedback,.publicFeedBack
    {
        background-color: #fff;
        text-align: center;
    }
    #content .container {
        padding: 0 10px 25px!important;
    }
    .row.likely {
        margin-top: 15px;
    }
    button.btn.btn-success.btn-lg {
        margin-left: -15px;
    }
    @media only screen and (max-width: 768px)
    {
        /*.private-feedback,.publicFeedBack*/
        /*{*/
        /*background-color:transparent;*/
        /*}*/
        .publicFeedBack i.fa.fa-bullhorn,.private-feedback i.fa.fa-lock {
            font-size: 40px;
            background-color: #fff;
        }
        .likely
        {
            margin-top: 20px!important;
        }
        .profile-pg h3,.profile-pg h3
        {
            font-size: 20px!important;
        }
        .profile-pg p,.profile-pg p
        {
            font-size: 15px!important;
        }
        button.btn.btn-success.btn-lg {
            margin-left: 0px;
        }
    }
    @media only screen and (max-width: 600px)
    {
        #content .container {
            padding: 10px!important;
        }
        .profile-pg.mt-4 {
            padding: 10px;
        }
        select.form-control {
            width: 100%!important;
        }
        .col-md-11.col-xs-11.publicFeedBacktxt {
            padding: 0px 30px;
        }
        tr:nth-of-type(odd) {
            /*background: #1A6D84!important;*/
            background: transparent!important;
        }

    }
    @media only screen and (max-width:414px)
    {
        .col-md-11.col-sm-11.col-xs-11.private-feedbacktxt {
            padding: 0px 33px;
        }
        .col-md-11.col-xs-11.publicFeedBacktxt
        {
            padding: 0px 36px;
        }
        .profile-pg p,.profile-pg p
        {
            font-size: 14px!important;
        }
        .profile-pg h3, .profile-pg h3 {
            font-size: 18px!important;
        }
    }
</style>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="https://www.travpart.com/English/wp-content/themes/bali/css/star-rating.css" rel="stylesheet" type="text/css">
<script src="https://www.travpart.com/English/wp-content/themes/bali/css/star-rating.js"></script>

<script>
    jQuery(document).on('ready', function () {
        jQuery('.rating,.kv-gly-star,.kv-gly-heart,.kv-uni-star,.kv-uni-rook,.kv-fa,.kv-fa-heart,.kv-svg,.kv-svg-heart').on(
                'change', function () {
                    console.log('Rating selected: ' + $(this).val());
                });
    });
</script>
<?php
if (isset($_GET['error_message']) && $_GET['error_message'] != '') {
    ?>
    <div class="row">
        <div class="border-box mb-4">
            <div class="p-3">
                <h2 style="color: red;text-align: center;">You are not authorized to access this page.</h2>
            </div>
        </div>
    </div>
    <?php
}
$tourId = isset($_GET['tour_id']) ? $_GET['tour_id'] : '';

$current_user = wp_get_current_user();

$bookingCodeList = $wpdb->get_results("SELECT t.id FROM (wp_tour t LEFT JOIN wp_postmeta p
											ON (t.id=p.meta_value AND p.meta_key='tour_id')) INNER JOIN wp_tourmeta m
											ON (t.id=m.`tour_id` AND m.meta_key='userid' AND m.meta_value='{$current_user->ID}')", ARRAY_A);


$myBookingIds = [];
foreach ($bookingCodeList as $bookingId) {
    $myBookingIds[] = $bookingId['id'];
}

if (in_array($tourId, $myBookingIds)) {
    
} else {
    $tourId == '';
}
if ($tourId == '') {
    ?>
    <div class="row">
        <div class="border-box mb-4">
            <div class="p-3">
                <h2 style="color: red;text-align: center;">You are not authorized to access this page.</h2>
            </div>
        </div>
    </div>
    <?php
} else {
    ?>

    <form method="POST" action="https://www.travpart.com/English/submit-ticket">
        <input type="hidden" name="af_tour_id" value="<?php echo $tourId; ?>">
        <input type="hidden" name="action" value="ratting_form">
        <div class="profile-pg mt-4">
            <div class="row mb-2">
                <div class="col-md-1 col-sm-1 col-xs-1 p-0 private-feedback">
                    <i class="fa fa-lock" style="font-size: 45px;     color: #66cccc;     padding: 10px;"></i>
                </div>
                <div class="col-md-11 col-sm-11 col-xs-11 private-feedbacktxt">
                    <h3 style="margin-top: 0px;">Private Feedback</h3>
                    <p>This feedback will be kept anonymous and never shared directly with the travel advisor.</p>
                </div>
            </div>
            <div class="row">
                <div class="border-box mb-4">
                    <div class="p-3">
                        <div class="row">
                            <div class="col-md-12">
                                <h5><b>Reason for ending contract.</b></h5>
                                <select class="form-control" style="width: 50%;">
                                    <option value="Tour Completed Success">Tour Completed Success.</option>
                                    <option value="I Deplay my tour">I Deplay my tour</option>
                                    <option value="Tour Cancel Due to Personal Issues">Tour Cancel Due to Personal Issues</option>
                                    <option value="I Can't Contact to agent for more information">I Can't Contact to agent for more information</option>
                                </select>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <h5><b>How likely are you to recommended this travel advisor to a friend or a colleague?</b></h5>
                                <table>
                                    <tr>
                                        <td>
                                            <input type="radio" name="r-agent" value="1">
                                        </td>
                                        <td>
                                            <input type="radio" name="r-agent" value="2">
                                        </td>
                                        <td>
                                            <input type="radio" name="r-agent" value="3">
                                        </td>
                                        <td>
                                            <input type="radio" name="r-agent" value="4">
                                        </td>
                                        <td>
                                            <input type="radio" name="r-agent" value="5">
                                        </td>
                                        <td>
                                            <input type="radio" name="r-agent" value="6">
                                        </td>
                                        <td>
                                            <input type="radio" name="r-agent" value="7">
                                        </td>
                                        <td>
                                            <input type="radio" name="r-agent" value="8">
                                        </td>
                                        <td>
                                            <input type="radio" name="r-agent" value="9">
                                        </td>
                                        <td>
                                            <input type="radio" name="r-agent" value="10">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>5</td>
                                        <td>6</td>
                                        <td>7</td>
                                        <td>8</td>
                                        <td>9</td>
                                        <td>10</td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <div class="row likely">
                            <div class="col-md-6" style="background: #e5e5e5; padding: 15px;">
                                <div class="row">
                                    <div class="col-md-6 col-xs-6">
                                        <p style="    margin: 0px;">Not at all Likely</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <p style="    margin: 0px; float: right;">Extermly Likely</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <h5><b>Why not? (Optional)</b></h5>
                                <div class="row">
                                    <div class="col-md-1"><input type="checkbox" value="" class="pull-right" name="why_note"></div>
                                    <div class="col-md-6"><p>Poor quality of work</p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"><input type="checkbox" value="" class="pull-right" name="why_note"></div>
                                    <div class="col-md-6"><p>Missed deadline</p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"><input type="checkbox" value="" class="pull-right" name="why_note"></div>
                                    <div class="col-md-6"><p>Poor Communication</p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"><input type="checkbox" value="" class="pull-right" name="why_note"></div>
                                    <div class="col-md-6"><p>Logged more time then necessary</p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"><input type="checkbox" value="" class="pull-right" name="why_note"></div>
                                    <div class="col-md-6"><p>Travel advisor who completed the work was not the person i interviewd.</p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"><input type="checkbox" value="" class="pull-right" name="why_note"></div>
                                    <div class="col-md-6"><p>Something else</p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="profile-pg mt-3">
            <div class="row mb-2">
                <div class="col-md-1 col-xs-1 p-0 publicFeedBack">
                    <i class="fa fa-bullhorn" style="font-size: 45px;     color: #66cccc;     padding: 10px;"></i>
                </div>
                <div class="col-md-11 col-xs-11 publicFeedBacktxt">
                    <h3 style="margin-top: 0px;">Public Feedback</h3>
                    <p>This feedback will be shared on your travel advisor's profile only after they've left feedback for you.</p>
                </div>
            </div>
            <div class="row">
                <div class="border-box mb-4">
                    <div class="p-3">
                        <div class="row">
                            <div class="col-md-12 rating-widget" >
                                <div class=''>
                                    <h5><b>Feedback</b></h5>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input name="skills" type="text" class="rating rating-loading" value="0" data-size="sm" title="">
                                        </div>
                                        <div class="col-md-6"><p>Skills</p></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <input name="quality_of_work" type="text" class="rating rating-loading" value="0" data-size="sm" title="">
                                        </div>
                                        <div class="col-md-6"><p>Quality of work</p></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <input name="availability" type="text" class="rating rating-loading" value="0" data-size="sm" title="">
                                        </div>
                                        <div class="col-md-6"><p>Availability</p></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <input name="schedule" type="text" class="rating rating-loading" value="0" data-size="sm" title="">
                                        </div>
                                        <div class="col-md-6"><p>Adhence to Schedule</p></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <input name="communication" type="text" class="rating rating-loading" value="0" data-size="sm" title="">
                                        </div>
                                        <div class="col-md-6"><p>Communication</p></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <input name="co_opration" type="text" class="rating rating-loading" value="0" data-size="sm" title="">
                                        </div>
                                        <div class="col-md-6"><p>Cooparation</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Total Score : 0.00</h4>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p>Share your experiance with this travel advisor's to the comminity.</p>
                                <textarea name="af_comments" required="true" class="form-control"></textarea>
                                <p class="pull-right"><i>5000 Character left.</i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button type='submit' class="btn btn-success btn-lg">Submit</button>
    </form>
    <?php
}
get_footer();
?>