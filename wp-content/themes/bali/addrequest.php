<?php
require_once('../../../wp-config.php');
global $wpdb;

session_start();

function make_random_string($N)
{
    $alph = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $s = "";
    for ($i = 0; $i != $N; ++$i) {
        $s.= $alph[mt_rand(0, 35)];
    }
    return $s;
}

$number_of_adult=filter_input(INPUT_POST, 'number_of_adult', FILTER_SANITIZE_NUMBER_INT);
$number_of_child=filter_input(INPUT_POST, 'number_of_child', FILTER_SANITIZE_NUMBER_INT);
$number_of_room=filter_input(INPUT_POST, 'number_of_room', FILTER_SANITIZE_NUMBER_INT);
$number_of_extra_bed=filter_input(INPUT_POST, 'number_of_extra_bed', FILTER_SANITIZE_NUMBER_INT);
$cs_addnightroom=filter_input(INPUT_POST, 'cs_addnightroom', FILTER_SANITIZE_NUMBER_INT);
$cs_addnightroomprice=filter_input(INPUT_POST, 'cs_addnightroomprice', FILTER_SANITIZE_NUMBER_INT);
$post_id=filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT);
$start_date=filter_input(INPUT_POST, 'start_date', FILTER_SANITIZE_STRING);
$end_date=$start_date=filter_input(INPUT_POST, 'end_date', FILTER_SANITIZE_STRING);;

$total=filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT);
if (!empty($_POST["total_dollar"])) {
    $total_dollar=ceil($_POST["total_dollar"]/get_option('_cs_currency_USD'));
    $total+=$total_dollar;
}

$tour_type=1; // can choose date
if ($cs_addnightroomprice==0 || $cs_addnightroomprice=="" || $cs_addnightroomprice=="null") {
    $tour_type=0;
} // static

$tour_date=date('d/m/Y');

$code=make_random_string(5); //uniqid();
$number_of_coding = $wpdb->get_var($wpdb->prepare("SELECT COUNT(code) FROM " . $wpdb->prefix .  "tour WHERE code = %s", $code));

while ($number_of_coding>0) {
    $code=make_random_string(5);
    $number_of_coding = $wpdb->get_var($wpdb->prepare("SELECT COUNT(code) FROM " . $wpdb->prefix .  "tour WHERE code = %s", $code));
}

$total_rb = intval(intval($_POST["hiddenTaxAndServiceRP"]) / get_option("_cs_currency_USD"));
$total += $total_rb;

$wpdb->query("INSERT INTO `wp_tour` (`id`, `tour_id`, `number_of_adult`, `number_of_child`, `number_of_room`, `number_of_extra_bed`, `addnightroom`, `tour_type`, `start_date`, `end_date`, `total`, `code`, `tour_date`)  
      VALUES (NULL, '".$post_id."', '".$number_of_adult."', '".$number_of_child."', '".$number_of_room."', '".$number_of_extra_bed."', '".$cs_addnightroom."', '".$tour_type."', '".$start_date."', '".$end_date."','".$total."', '".$code."','".$tour_date."')
      ");
 
$lastid = $wpdb->insert_id;
$_SESSION["tour_page_contact_form_details_$lastid"] = $_POST["hiddenTripDetails"];

echo $lastid;
