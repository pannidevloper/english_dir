<?php
/*
 * Template Name: Shortpost
 * Template Post Type: shortpost
 */
add_action('wp_head', 'add_meta_tags');
get_header();

if (!is_user_logged_in()) {
   // exit(wp_redirect(home_url('login')));
}
function add_meta_tags() {
global $wpdb;
$post_id = get_the_ID();
$item = $wpdb->get_row("SELECT wp_posts.ID,post_author,user_login,post_title,post_date,post_type,post_content
FROM `wp_posts`,`wp_users` WHERE wp_posts.ID='{$post_id}' AND post_author=wp_users.ID");

// url
$url = get_permalink($item->ID);
foreach (get_post_meta($item->ID, 'attachment') as $row) {
if (wp_attachment_is('image', $row)){

$images_for_meta=  wp_get_attachment_url($row);
echo' <meta name="og:image" content="'.$images_for_meta.'"/>';
echo'<meta property="og:image:secure_url" content="'.$images_for_meta.'" />';
}
else{
    $videos_for_meta=  wp_get_attachment_url($row);
    echo' <meta name="og:video" content="'.$videos_for_meta.'"/>';
echo'<meta property="og:video:secure_url" content="'.$videos_for_meta.'" />';
}

   }

 $title ='Travpost';
  echo' <meta name="og:description" content="'.$item->post_content.'"/>';
  echo' <meta name="og:title" content="'.$title.'"/>';
  echo' <meta name="og:url" content="'.$url.'"/>';
  echo' <meta name="description" content="'.$item->post_content.'"/>';
  echo'<meta property="og:type" content="article" />';


}

//add_action('wp_head', 'add_meta_tags');
?>
<link rel="stylesheet" type="text/css"
    href="https://www.travpart.com/English/wp-content/themes/bali/css/timeline-style.css?v=<?= time();?>" />
<style type="text/css">
    .popup_user-comment-area{
        margin-bottom: 5px;
        margin-top: 5px;
        display: flex;
    }
    .ShareDropdown::after,.ShareDropdown::before{
        left:5px !important; 
        right: auto !important;
    }
    .commets_list {
        overflow-y: scroll;
        height: 350px;
    }
    .ShareDropdown{
        min-width: auto
    }
    .share-area{
        position: relative;
    }
    .popup_user-comment-area img {
        border-radius: 50px;
        position: relative !important;
        margin-left: 18px;
        width: 35px !important;
        height: 35px !important;
        margin-top: 3px;
    }
    .popup_another-user-section {
        display: inline-block;
        background: #F2F3F5;
        padding: 7px;
        border-radius: 16px;
        margin: 0px 10px;
    }
    .popup_another-user-section a{
        color: #808080d6;
    }
    .popup_another-user-section span {
        word-break: break-word;
        font-size: 13px;
        margin: 0 10px 0px 3px;
    }
    .feed-post-one-image,.feed-post-two-image,.feed-post-2image-2video,.feed-post-2imagevideo{
        
    }

    .feed_post_image_div2 {
        width: 90%;
        display: flex;
        margin: 0px auto;
        height: 350px;
    }

    .user_comment_section,
    .submit-comment {
        display: block;
    }

    #footer {
        display: none;
    }

    .post_profile_img {
        width: 8%;
    }

    .header_user_content {
        width: 92%;
    }

    .single-post-header {
        background: #1abc9c;
        padding: 10px;
        text-align: center;
    }
    .single-post-header {
        display: none;
    }
    .post-content{
        border-top: none;
        border-bottom: none;
    }

    .single-post-header h2 {
        font-size: 16px !important;
        display: inline-block;
        margin: 0;
        color: #fff !important;
    }

    .sPback {
        display: inline-block;
        cursor: pointer;
        float: left;
    }

    .sPback i {
        font-size: 18px;
        color: #fff;
    }

    .right-section-dot {
        top: 55px;
    }

    .mobile_agent_name a {
        color: #060606c9;
        font-size: 16px;
    }

    .date_pub_mobile abbr {
        display: block;
        font-size: 12px;
        line-height: 25px;
        color: #90949c;
    }

    .PostDelete {
        top: 76px;
    }

    .user-comment-area input {
        width: 93%;
    }

    .feed_post_image_div {
        width: 96.4%;
        position: relative;
        height: 500px;
        display: inline-block;
        margin-bottom: 25px;
    }

    .feed-post-video video {
        width: 100%;
        height: auto;
    }

    .post-image img {
        cursor: pointer;
    }


    /* Add cover image change css */

    .cCovercommentR {
        background: #F2F3F5;
    }

    .cCovercommentR img {
        width: 35px !important;
        border-radius: 50% !important;
        display: inline-block !important;
        height: 35px !important;
        left: inherit !important;
        margin-left: 15px;
        position: inherit !important;
        margin: 5px 10px;
        margin-right: 5px;
    }

    .cCovercommentR input {
        width: 82%;
        border-radius: 5px;
        border: none;
        background-color: #fff;
        border: 1px solid #ccd0d5;
        border-radius: 16px;
        padding: 5px 15px;
        font-size: 14px;
        position: relative;
        top: 2px;
    }

    .cCovercommentR input:focus {
        outline: none;
    }

    .ttp {
        margin-top: 50px;
        font-size: 12px;
        color: #8e8989;
        margin-bottom: 5px;
    }

    .comment_box {
        padding: 10px;
    }

    .img_user {
        width: 10%;
        display: inline-block;
        vertical-align: middle;
    }

    .img_user img {
        border-radius: 100%;
        width: auto !important;
        position: inherit !important;
    }

    .user_content {
        width: 85%;
        padding: 10px;
        display: inline-block;
        vertical-align: middle;
    }

    .user_content a {
        font-size: 12px;
        text-decoration: none;
        color: #116fee
    }

    .user_content p {
        font-size: 11px;
        margin: 0px;
    }

    .pd_w {
        padding: 0px 25px;
    }

    .ttp i {
        color: #7f7ffb;
    }

    .Csocial-area-righ {
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }

    .CsocialThumbs {
        padding: 10px 19px !important;
    }

    .CsocialThumbs a {
        color: #616161 !important;
    }

    .cover-user-r {
        position: relative;
    }

    .C-date a {
        font-size: 12px !important;
        position: absolute !important;
        top: 34px !important;
        left: 18% !important;
        color: #90949c !important;
    }

    .C-date i {
        font-size: 13px !important;
        position: relative;
        top: -12px !important;
        left: 45% !important;
        color: #90949c !important;
    }

    .Coverdots-right {
        position: absolute;
        right: 0;
        top: 0px;
        padding: 10px;
    }

    .Coverdots-right i {
        color: #8c8c8c;
        font-size: 15px;
        cursor: pointer;
    }

    .cover-user-r img {
        width: 40px !important;
        border-radius: 50% !important;
        display: inline-block !important;
        height: 40px !important;
        left: inherit !important;
        margin-top: 10px;
        margin-left: 15px;
        position: inherit !important;
    }

    .cover-user-r a {
        font-size: 18px;
        margin-left: 10px;
        position: relative;
        top: 3px;
        text-decoration: none;
        color: #385898;
        font-weight: bold;
    }

    .coverP-right {
        float: right;
        width: 26.7%;
        //overflow: hidden;
    }

    .cover-user-r img {
        width: 40px;
        border-radius: 50%;
        display: inline-block;
    }

    .cover-close-m {
        position: fixed;
        z-index: 999999;
        right: 2%;
        top: 2%;
    }

    .cover-close-m i {
        font-size: 20px;
        color: #777;
    }

    .bottom-right-txt a {
        display: inline-block;
        text-align: right;
        color: #bbb;
        margin: 10px;
        font-size: 12px;
    }

    .bottom-right-txt a:hover {
        color: #fff;
    }

    .bottom-right-txt {
        display: inline-block;
        width: 69%;
        text-align: right;
    }

    .thumbs-up {
        padding: 10px;
        display: inline-block;
    }

    .coverP-bottom {
        position: absolute;
        bottom: 0;
        width: 73.3%;
        background: #131212ba;
    }

    .thumbs-up a {
        color: #bbb;
        font-size: 12px;
    }

    .thumbs-up a:hover {
        color: #fff;
    }

    .thumbs-up i {
        margin-right: 4px !important;

    }

    .CoverPic img {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        height: 100%;
        cursor: pointer;
        width: 73.3%;
    }

    .CoverPage {
        display: none;
    }

    .Cheader-left h4 {
        color: #fff;
        position: absolute;
        z-index: 9999;
        font-size: 26px;
        left: 30px;
        top: 15px;
    }

    .CoverPic {
        position: fixed;
        top: 10%;
        width: 90%;
        background: #fff;
        height: 80%;
        margin: 0px auto;
        display: block;
        z-index: 999999;
        left: 0;
        box-shadow: 0 2px 26px rgba(0, 0, 0, .3), 0 0 0 1px rgba(0, 0, 0, .1);
        left: 5%;
    }

    .CoverPage:after {
        content: "";
        width: 100%;
        height: 100%;
        bottom: 0;
        left: 0;
        position: fixed;
        right: 0;
        top: 0;
        background-color: rgba(0, 0, 0, .9);
        z-index: 99999;
    }

    .Cphotohide {
        display: block;
    }

    .feed_post_imagediv2 {
        width: 47%;
        display: flex;
        float: left;
        margin: 0px 0px 0px 20px;
        height: 350px;
    }


    .feed_post_image_div3 {
        width: 47%;
        display: flex;
        float: right;
        margin: 0px 20px 0px 0px;
        height: 350px;
    }

    /*Start video player css*/

.player-container {
    padding: 40px;
    max-width: 100%;
    margin: auto;
}
.player {
    width: 100%;
    height: 0;
    padding-bottom: 56.25%;
    position: relative;
    overflow: hidden;
    background: #000000;
}
.player:fullscreen {
    padding-bottom: 100vh;
}
.player:-webkit-full-screen {
    padding-bottom: 100vh;
}
.player:-moz-full-screen {
    padding-bottom: 100vh;
}
.player:-ms-fullscreen  {
    padding-bottom: 100vh;
}

.controls {
    padding: 0;
    position: absolute;
    bottom: -80px;
    width: 100%;
    height: 48px;
    box-sizing: border-box;
    background: linear-gradient(
        180deg,
        rgba(37, 37, 37, 0) 10%,
        rgba(37, 37, 37, 0.6) 80%
    );
    transition: all 0.2s ease-in 5s;
}
.player:hover .controls {
    bottom: 0;
    transition: all 0.2s ease-out;
}
.time {
    position: absolute;
    right: 30px;
    bottom: 100%;
    padding-bottom: 14px;
}
.progress {
    height: 8px;
    width: calc(100% - 40px);
    background: rgba(60, 60, 60, 0.6);
    margin: auto;
    border-radius: 6px;
    position: absolute;
    left: 20px;
    bottom: 100%;
    transition: height 0.1s ease-in-out;
}
.progress:hover {
    height: 10px;
}
.progress-filled {
    background: var(--accent);
    width: 0%;
    height: 100%;
    border-radius: 6px;
    transition: all 0.1s; 
}
.controls-main {
    width: calc(100% - 40px);
    margin: auto;
    height: 100%;
    display: flex;
    justify-content: space-between;
}
.controls-left,
.controls-right {
    flex: 1;
    display: flex;
    align-items: center;
    overflow: hidden;
}
.controls-left {
    margin-left: 10px;
}
.controls-right {
    margin-right: 10px;
    justify-content: flex-end;
}
.volume {
    display: flex;
    align-items: center;
}
.volume-btn {
    margin-right: 10px;
}
.volume-btn #volume-off, .volume-btn #volume-high {
    opacity: 0;
}
.volume-btn.loud #volume-high{
    opacity: 1;
}
.volume-btn.muted #volume-off {
    opacity: 1;
}
.volume-btn.muted #volume-high, .volume-btn.muted #volume-low {
    opacity: 0;
}
.volume-slider {
    height: 8px;
    width: 80px;
    background: rgba(60, 60, 60, 0.6);;
    border-radius: 6px;
    position: relative;
}
.volume-filled {
    background: var(--main);
    width: 100%;
    height: 100%;
    border-radius: 6px;
    transition: width 0.2s ease-in-out;
}
.volume-filled:hover, .play-btn:hover.play-btn:before, .play-btn:hover.play-btn:after{
    background: var(--accent);
}
button {
}
.play-btn {
    width: 30px;
    height: 30px;
    position: relative;
    margin: auto;
    transform: rotate(-90deg) scale(0.8);
    transition: -webkit-clip-path 0.3s ease-in 0.1s, shape-inside 0.3s ease-in 0.1s,
        transform 0.8s cubic-bezier(0.85, -0.25, 0.25, 1.425);
}
.play-btn.paused {
    transform: rotate(0deg);
}
.play-btn:before,
.play-btn:after {
    content: "";
    position: absolute;
    background: white;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    transition: inherit;
}
.play-btn:before {
    -webkit-clip-path: polygon(0 10%, 100% 10%, 100% 40%, 0 40%);
    shape-inside: polygon(0 10%, 100% 10%, 100% 40%, 0 40%);
}
.play-btn:after {
    -webkit-clip-path: polygon(0 60%, 100% 60%, 100% 90%, 0 90%);
    shape-inside: polygon(0 60%, 100% 60%, 100% 90%, 0 90%);
}
.play-btn.paused:before {
    -webkit-clip-path: polygon(10% 0, 90% 51%, 90% 51%, 10% 51%);
    shape-inside: polygon(0 0, 100% 51%, 100% 51%, 0 51%);
}
.play-btn.paused:after {
    -webkit-clip-path: polygon(10% 49.5%, 80% 49.5%, 90% 49.5%, 10% 100%);
    shape-inside: polygon(10% 49.5%, 80% 49.5%, 90% 49.5%, 10% 100%);
}
button:focus {
    outline: none;
}
.speed-list {
    list-style: none;
    margin: 0;
    padding: 0;
    display: flex;
    margin-right: 20px;
    text-align: center;
}
.speed-list li {
    color: var(--main);
    padding: 5px;
    cursor: default;
}
.speed-list li:hover,
.speed-list li.active {
    color: var(--accent);
    font-weight: bold;
}
.fullscreen {
    display: flex;
    justify-content: center;
}




    /*End video player css*/


.user-comment-area.submit-comment img{
    width: 35px!important;
    height: 35px!important;
    margin-left: 0!important;
    top: 3px;
}

.user-comment-area img{
    width: 35px!important;
    height: 35px!important;
    margin-left: 0!important;
    top: 3px;
}

.post-uesr-view{
    text-align: left;
    margin-top: 15px;
}   

.post-uesr-view span{
    color: #606060;
}


.user-comment-area.submit-comment{
    text-align: left;
    margin-left: 15px;
}

.video_views{
    font-size: 16px;
    color: #90949c;
}


/* End Add cover image change css */

.dropdown-menu.sign-in-user{
    top: 99.5%;
    left: 46%;
    border: 8px solid rgba(0, 14, 14, 0.2);
    border-radius: inherit;
    margin: 0;
    padding: 0;
}

.dropdown-menu.sign-in-user.sign-inlike{
    left: 16.5%;
    padding: 10px;
}

.sign-in-user p{
    font-size: 16px;
    font-weight: 500;
    padding: 10px;
}

.dropdown-menu.sign-in-user p a{
    font-size: 14px;
    font-weight: 600;
    color: #065fd4;
    margin: 0 5px;
    text-decoration: none;
}

.comment_mobile.open .dropdown-menu, .like_mobile.open .dropdown-menu{
    display: block!important;
}

.sign-inlike span {
    font-size: 16px;
    font-weight: 500;
    padding: 10px;
    color: rgb(33, 37, 41);
}

.sign-in-user.sign-inlike a.signa {
    font-size: 14px;
    font-weight: 600;
    color: #065fd4;
    margin: 0 5px;
    text-decoration: none;
}

.share_mobile a, .comment_mobile a, .like_mobile a{
    color: #808080;
}

    @media(max-width: 600px) {
        .container.for_remove_ad {
            margin-top: 80px;
        }
        .post-content{
            margin-top: 10px;
        }
        .container.short_post_contaner {
            padding: 0 10px;
        }
        .post-image div.feed-post-two-image img{
            height: 220px;
        }
        .post-image div.feed-post-one-image img{
            height: 220px;
        }
        .feed_post_image_div2{
            height: 230px;
        }
        .feed-post-one-image,.feed-post-two-image,.feed-post-three-image {
            margin-top: 10px;
        }

    }

    /*
        =======================
        StartResponsive Code
        =======================
        */

    @media (max-width: 767px) {
        .for_img img {
            width: 45px;
            margin: 10px;
            max-width: inherit;
        }

        .mobile_agent_name {
            margin-bottom: 0px !important;
            margin-top: 3px;
            margin-left: 31px;
        }

        .post-image img {
            height: auto;
        }

        .feed_post_image_div {
            width: 93.5%;
            position: relative;
            height: auto;
            display: inline-block;
            margin-bottom: 20px;
        }


        .feed_post_imagediv2 {
            height: 165px;
            width: 45%;
            margin: 0px 0px 0px 20px;
        }


        .feed_post_image_div3 {
            height: 165px;
            width: 45%;
            margin: 0px 20px 0px 0px;
        }

        .feed-post-video video {
            width: 100%;
            height: auto;
        }

        .user-comment-area input {
            width: 75%;
        }

        /*cover Popup for mobile css*/

        .CoverPic {
            top: 20%;
            width: 100%;
            height: auto;
            left: 0%;
        }

        .cover-close-m {
            top: 15%;
        }

        .coverP-bottom {
            display: none;
        }

        .CoverPic img {
            position: inherit;
            width: 100%;
            height: 300px;
        }

        .coverP-right {
            width: 100%;
        }

        .cover-user-r a {
            font-size: 16px;
            top: 0px;
        }

        .C-date a {
            left: 8.5% !important;
        }

        .thumbs-up a {
            text-decoration: none;
        }

        .row.ttp {
            display: none;
        }

        .cCovercommentR input {
            top: 0px;
        }

        .Cheader-left h4 {
            font-size: 16px;
            left: 10px;
            top: 0px;
        }

        /*Video Controls*/
        .controls {
            height: 30px;
        }

        .time-current{
            color: #fff;
            font-size: 12px;
        }

        .volume-btn,.play-btn,.fullscreen{
            width: 20px;
            height: 20px;
        }

        .player-container {
            padding: 0px;
            margin-bottom: 20px;
        }

        /*End Video Controls*/


    }

    @media (max-width: 576px) {
        .breadcrumb{
            display: none;
        }

        .for_img img {
            width: 45px;
            margin: 10px;
            max-width: inherit;
        }

        .mobile_agent_name {
            margin-bottom: 0px !important;
            margin-top: 3px;
            margin-left: 31px;
        }
        .post-image img {
            height: auto;
        }

        .feed_post_image_div {
            width: 93.5%;
            position: relative;
            height: auto;
            display: inline-block;
            margin-bottom: 10px;
        }

        .feed-post-video video {
            width: 100%;
            height: auto;

        }

        .user-comment-area input {
            width: 75%;
        }

        /*cover Popup for mobile css*/

        .CoverPic {
            top: 20%;
            width: 100%;
            height: auto;
            left: 0%;
        }

        .cover-close-m {
            top: 15%;
        }

        .coverP-bottom {
            display: none;
        }

        .CoverPic img {
            position: inherit;
            width: 100%;
        }

        .coverP-right {
            width: 100%;
        }

        .cover-user-r a {
            font-size: 16px;
            top: -25px;
        }

        .thumbs-up a {
            text-decoration: none;
        }

        .row.ttp {
            display: none;
        }

        .cCovercommentR input {
            top: -16px;
        }

        .Cheader-left h4 {
            font-size: 16px;
            left: 10px;
            top: -10px;
        }

        .C-date a {
            left: 16.5% !important;
        }


        .feed-post-image-video .feed_post_imagediv2 {
            height: 135px !important;
            width: 45%;
            margin: 0px 0px 0px 10px;
        }

        .feed_post_imagediv2 {
            height: 135px !important;
            width: 45%;
            margin: 0px 0px 0px 10px;
        }

        .feed_post_image_div3 {
            height: 135px;
            width: 45%;
            margin: 0px 10px 0px 0px;
        }


    /*Video Controls*/
        .controls {
            height: 30px;
        }

        .time-current{
            color: #fff;
            font-size: 12px;
        }

        .volume-btn,.play-btn,.fullscreen{
            width: 20px;
            height: 20px;
        }

        .player-container {
            padding: 0px;
            margin-bottom: 20px;
        }



        .user-comment-area.submit-comment img{
            top: 15px;
        }


        /*End Video Controls*/

    }

    .first-attachment:after {
        content: "";
        display: block;
        padding: 5px;
        clear: both;
    }
</style>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="post-area-main">
    <div class="container short_post_contaner">

        <!-- time line row -->
        <div class="row">

            <?php
                    $post_id = get_the_ID();

                    $item = $wpdb->get_row("SELECT wp_posts.ID,post_author,user_login,post_title,post_date,post_type,post_content
                        FROM `wp_posts`,`wp_users` WHERE wp_posts.ID='{$post_id}' AND post_author=wp_users.ID");
                    $liked = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE user_id = '{$current_user->ID}' AND feed_id = '{$post_id}'");
                    $like_count = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE feed_id = '{$post_id}'");

                $current_user = wp_get_current_user();
                // get author id  and current user id and check either user is blocked,if blocked hide comment/like
                $author_id = get_post_field('post_author', $item->ID);

                $query_to_check_block = $wpdb->get_var("SELECT * FROM `friends_requests` where user1='$author_id' AND user2='$current_user->ID' AND status=2");


                    $comments_count = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_comments` WHERE  `feed_id` = '{$item->ID}'");
                    // short post view counter
                    function setshortPostViews($post_id) {
                    $count_key = 'post_views_count';
                    $count = get_post_meta($post_id, $count_key, true);
                    if($count==''){
                    $count = 0;
                    delete_post_meta($post_id, $count_key);
                    add_post_meta($post_id, $count_key, '0');
                    }else{
                    $count++;
                    update_post_meta($post_id, $count_key, $count);
                    }
                    }
                    setshortPostViews($post_id);
                    $author_id = get_post_field('post_author', $item->ID);

      if(function_exists('visitor_profile_logs') AND is_user_logged_in()) {
        visitor_profile_logs ('short_post_'.$post_id,$author_id,$current_user->ID );
}
 


                    ?>

            <div class="col-md-12">
                <div class="post-area-content">
                    <div class="post-content">
                        <div class="single-post-header">
                            <div class="sPback">
                                <a href="javascript:close_window();">
                                    <i class="fas fa-arrow-left"></i>
                                </a>
                            </div>

                            <h2><?php echo $item->post_title;  ?></h2>
                        </div>

                        <div class="post_header">
                            <div class="post_profile_img">
                                <div class="for_img">
                                    <a
                                        href="<?php echo home_url('/my-time-line/?user=' . $item->user_login); ?>">
                                        <img
                                            src="<?php echo um_get_avatar_url(um_get_avatar('', $item->post_author, 52)); ?>">
                                    </a>
                                </div>
                            </div>
                            <?php
                                    $display_name = um_user('display_name'); ?>
                            <div class="header_user_content">
                                <div class="content_mobile">
                                    <h4 class="mobile_agent_name" style="margin-bottom: 20px;margin-top: 5px;">
                                        <a
                                            href="<?php echo home_url('/my-time-line/?user=' . $item->user_login); ?>"><?php echo $display_name; ?>
                                        </a>
                                        <span class="date_pub_mobile">
                                            <abbr><span><?php echo date("d,F Y  g:i:s A", strtotime($item->post_date));?></span></abbr>
                                        </span>

                                        <span class="video_views"><?php echo getPostViews($post_id).
                                        " Views" ;?></span>

                                        <?php if (get_post_meta($item->ID, 'lookingfriend', true) == 1) { ?>
                                        <a href="https://www.travpart.com/English/people/"
                                            class="changes_for_mob  button_green">Looking For A Travel Friend</a>
                                        <?php } ?>
                                    </h4>
                                    <p class="mobiel_feeling"></p>

                                    <!-- Right dot dropdown -->
                                    <div class="right-section-dot dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false">
                                        <a href="#" class="Pdeltebtn"><i class="fas fa-ellipsis-h"></i></a>
                                    </div>
                                    <!-- Right dot dropdown -->
                                    <?php
                                            $post_edit_text = preg_replace('/[^A-Za-z0-9 ]/', '', $item->post_content);
                                            ?>
                                    <!-- Post Delete Option PC-->
                                    <div id="PostDId" class="PostDelete dropdown-menu" role="menu"
                                        aria-labelledby="menu1">
                                        <?php if (is_user_logged_in()) {
                                                $author_id = get_post_field('post_author', $item->ID);
                                                if ($author_id == get_current_user_id()) { ?>
                                       
                                        <?php
                                                    }
                                            } ?>

                                        
                                        <a style="display: none" id="copytext"
                                            href="<?php echo get_permalink($item->ID); ?>"
                                            target="_blank">
                                            <li><a onclick="copy_link()" href="#">Copy link to this post</a></li>

                                    </div>
                                    <!--End Post Delete Option -->
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <?php if (!empty(get_post_meta($item->ID, 'feeling', true))) { ?>
                            <span class="short-post-feeling">
                                <?php echo $item->user_login . ' is feeling ' . base64_decode(get_post_meta($item->ID, 'feeling', true)); ?>
                            </span>
                            <?php } elseif (!empty(get_post_meta($item->ID, 'tag_user_id', true))) { ?>
                            <span class="short-post-tagfriend">
                                <?php echo $item->user_login . ' is with ' . um_get_display_name(get_post_meta($item->ID, 'tag_user_id', true)); ?>
                            </span>
                            <?php } ?>
                        </div>

                        <?php
                    $attachment = get_post_thumbnail_id($item->ID);
                    $attachment_url = '';
                    if (empty($attachment)) {
                        $attachment = get_post_meta($item->ID, 'attachment', true);
                    }
                    if (!empty($attachment)) {
                        if (wp_attachment_is('image', $attachment)) {
                            $attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
                        } else {
                            $attachment_url = wp_get_attachment_url($attachment);
                        }
                    } elseif (!empty(get_post_meta($item->ID, 'tour_img', true))) {
                        $attachment_url = get_post_meta($item->ID, 'tour_img', true);
                    }
                    $tour_post_id = get_post_meta($item->ID, 'tour_post', true);
                    if (!empty($attachment_url)) {
                        ?>
                        <div class="post-image">
                            <?php if (!is_array(get_post_meta($item->ID, 'attachment'))) { ?>
                            <?php if (empty($attachment) || wp_attachment_is('image', $attachment)) { ?>
                            <div class="row">
                                <div class="col-xs-12">
                                    <img
                                        src="<?php echo $attachment_url; ?>">
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="row">
                                <div class="col-xs-12">
                                    <video
                                        src="<?php echo $attachment_url; ?>"
                                        controls="controls"></video>
                                </div>
                            </div>

                            <?php } ?>
                            <?php } else {
                                /*
                            $attachments=get_post_meta($item->ID, 'attachment');
                            $i=1; ?>
                            <div class="row">
                                <?php
                            foreach ($attachments as $row) {
                                $attachment_url = wp_get_attachment_url($row); ?>
                                <?php if (wp_attachment_is('image', $row)) { ?>
                                <div
                                    class="col-xs-12 <?php echo $i==1?'first-attachment':''; ?>">
                                    <img
                                        src="<?php echo $attachment_url; ?>">
                                </div>
                                <?php } else { ?>

                                <div
                                    class="col-xs-12 <?php echo $i==1?'first-attachment':''; ?>">
                                    <video
                                        src="<?php echo $attachment_url; ?>"
                                        controls="controls"></video>
                                </div>
                                <?php } ?>
                                <?php
                                $i++;
                            } ?>
                            </div>
                            <?php
                        */} 

                            // 
                            $image=0;
                            $video=0;
                            foreach (get_post_meta($item->ID, 'attachment') as $row) {

                            if (wp_attachment_is('image', $row)){
                             $image++;
                             $images[]=  wp_get_attachment_url($row);

                              }
                            else{
                             $video++;
                              $videos[]=  wp_get_attachment_url($row);
                         }
                            }
                           

                            ?>

                        <!-- One images -->
                        <?php if($image==1  AND $video==0){

                         ?>
                        <div class="feed-post-one-image">
                            <div class="feed_post_image_div feed_post1"> 
                                <img id='open_photo' src="<?php echo $images[0];  ?>"> <a href="<?php  echo $images[0] ?>" data-fancybox="gallery"></a> 
                            </div>
                        </div>
                    <?php } ?>
                        <!--End One images -->



                        <?php if($image==0  AND $video==1){?>

                             <div class="feed-post-2imagevideo">

                            <div class="feed-post-video">
                                <video controls="controls">
                                    <source src="<?php echo $videos[0];  ?>" type="video/mp4" />
                                    <source src="<?php echo $videos[0];  ?>" type='video/ogg; codecs="theora, vorbis"' />
                                    <source src="<?php echo $videos[0];  ?>" type='video/webm; codecs="vp8, vorbis"' />
                                </video>
                            </div>
                        </div>  

                        <?php } ?>

                        <!-- two images | video -->
                         <?php if($image==2  AND $video==0){?>
                        <div class="feed-post-two-image">
                            <div class="feed_post_image_div feed_post1"> 
                                <img id='open_photo' src="<?php echo $images[0];  ?>"> <a href="<?php echo $images[0];  ?>" data-fancybox="gallery"></a>
                            </div>

                            <div class="feed_post_image_div2"> 
                                <img id='open_photo' src="<?php echo $images[1];  ?>"> <a href="<?php echo $images[1]; ?>" data-fancybox="gallery"></a>
                            </div>

                        </div>  
                        <!--End two images -->
                        <?php } ?>
                        <!-- two images | video -->
                         <?php if($image==2  AND $video==1){?>
                        <div class="feed-post-2imagevideo">
                            <div class="feed_post_image_div feed_post1"> 
                                <img id='open_photo' src="<?php echo $images[0];  ?>"> <a href="<?php echo $images[0];  ?>" data-fancybox="gallery"></a> 
                            </div>

                            <div class="feed_post_image_div2"> 
                                <img id='open_photo' src="<?php echo $images[1];  ?>"> <a href="h<?php echo $images[1];  ?>" data-fancybox="gallery"></a>
                            </div>

                            <div class="feed-post-video">
                               <video src="<?php echo $videos[0];  ?>" controls="controls"></video>
                            </div>
                        </div>  
                        <!--End two images -->

                        <?php } ?>
                        <!-- three images -->
                         <?php if($image==3  AND $video==1){?>
                        <div class="feed-post-three-image">
                            <div class="feed_post_image_div feed_post1"> 
                                <img id='open_photo' src="<?php echo $images[0];  ?>"> <a href="<?php echo $images[0];  ?>" data-fancybox="gallery"></a> 
                            </div>

                            <div class="feed_post_imagediv2"> 
                                <img id='open_photo' src="<?php echo $images[1];  ?>"> <a href="<?php echo $images[1];  ?>" data-fancybox="gallery"></a>
                            </div>

                            <div class="feed_post_image_div3"> 
                                <img id='open_photo' src="<?php echo $images[2];  ?>"> <a href="<?php echo $images[2];  ?>" data-fancybox="gallery"></a>
                            </div>

                            <div class="feed-post-video">
                                <video src="<?php echo $videos[0];  ?>" controls="controls"></video>
                            </div>
                        </div> 
                        <?php }?> 
                        <!--End three images -->

                        <?php if($image==2  AND $video==2){?>
                        <!-- two images and two video-->
                        <?php 
                        $url = get_permalink($item->ID);
                     //  add_meta_tags($item->post_content,$item->post_title,$images[0],$url);

                         ?>
                        <div class="feed-post-2image-2video">
                            <div class="feed_post_image_div feed_post1"> 
                                <img id='open_photo' src="<?php echo $images[0];  ?>"> <a href="<?php echo $images[0];  ?>" data-fancybox="gallery"></a> 
                            </div>

                            <div class="feed_post_image_div2"> 
                                <img id='open_photo' src="<?php echo $images[1];  ?>"> <a href="<?php echo $images[1];  ?>" data-fancybox="gallery"></a>
                            </div>

                            <div class="feed-post-video"> 
                                <video src="<?php echo $videos[0];  ?>" controls="controls"></video>
                            </div>                            

                            <div class="feed-post-video">
                                <video src="<?php echo $videos[1];  ?>" controls="controls"></video>
                            </div>
                        </div>  
                    <?php } ?>
                        <!--End two images and two video -->


                        </div>
                        <?php
                    } ?>

                        <?php if (!empty(get_post_meta($item->ID, 'location', true))) { ?>
                        <div class="post-category">
                            <a href="#"><?php echo get_post_meta($item->ID, 'location', true); ?></a>
                        </div>
                        <?php } ?>

                        <div class="post-content-txt">
                            <div class="more">
                                <?php echo strip_tags($item->post_content); ?>
                            </div>
                        </div>

                        <?php if (!empty(get_post_meta($item->ID, 'tour_post', true))) { ?>
                        <div class="post-category">
                            <a
                                href="<?php echo get_permalink(get_post_meta($item->ID, 'tour_post', true)); ?>">
                                <?php echo get_post_meta($item->ID, 'tour_title', true); ?>
                            </a>
                        </div>
                        <br>
                        <?php } ?>
                                
                        <div class="buttons_for_mobilepost">
                            <hr class="style-mobile-six" />
                            <div class="main_buttons_div">
                                 
                                <?php if (is_user_logged_in()) {?>
                                <?php
                                if( empty($query_to_check_block)){?>
                                <div class="like_mobile">
                                    <a href="#"
                                        feedid="<?php echo $item->ID; ?>">
                                        <i
                                            class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>

                                        <span><?php echo $like_count; ?></span>
                                    </a>
                                </div>
                            <?php }else{?>

                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <div class="like_mobile">
                                    <i class="far fa-thumbs-up"></i>

                                        <span><?php echo $like_count; ?></span>


                                      <div class="dropdown-menu sign-in-user sign-inlike"  id="signINlike" role="menu4"
                                            aria-labelledby="menu4">
                                 
                                         <span>You can not Like the post! <a target="_blank" href="travpart.com/English/login" class="signa"></a></span>
                                        
                                      </div>
    
                                </div>
                                </a>

                            

                       <?php  }} else{ ?>

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <div class="like_mobile">
                                    <i class="far fa-thumbs-up"></i>

                                        <span><?php echo $like_count; ?></span>


                                      <div class="dropdown-menu sign-in-user sign-inlike"  id="signINlike" role="menu4"
                                            aria-labelledby="menu4">
                                        
                                         <span>You need to <a target="_blank" href="travpart.com/English/login" class="signa">Sign in</a></span>
                                        
                                      </div>
    
                                </div>
                                </a>
                                        



                            <?php }?>
                                <!-- Start user commetns loading for mobile -->

                                <div class="comment_mobile">
                                    <a href="#">
                                        <i class="far fa-comment-alt"></i>
                                        Comment <?php  echo $comments_count; ?>
                                    </a>
                                    <?php if (!is_user_logged_in()) {?>
                                    <div class="dropdown-menu sign-in-user">
                                        <p>You need to <a target="_blank" href="https://www.travpart.com/English/login">SIGN IN</a></p>
                                    </div>
                                <?php }?>
                                </div>
                                <!-- End user commetns loading for mobile --> 
                    

                                <div class="share_mobile">
                                    <a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
                                        <i class="fas fa-share"></i> Share</a>
                                                    
                        
                                    <!--Share My DropDown PC-->
                                    <div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2"
                                        aria-labelledby="menu2" style="right: 40px;left: inherit;">
                                        <?php if (is_user_logged_in()  AND empty ($query_to_check_block)) {?>
                                        <li
                                            onclick="share_feed(<?php echo $item->ID; ?>)">
                                            Share To Timeline</li>

                                        <?php }?>

                                                <li><a target="_blank"  onclick="record_share_data('<?php echo $item->ID; ?>','whatsapp')" href="https://web.whatsapp.com/send?text=<?php echo substr($item->post_content, 0, 80) . "  ( see-more ) "; echo "https://www.travpart.com/English/shortpost/".$item->ID; ?>">Share to whats app</a></li>
                                        <li><a href="#"
                                                onclick="copy_link('<?php echo $p->ID; ?>')">Copy
                                                link to this post</a></li>
                                                <div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="<?php echo get_permalink($item->ID) ?>" data-a2a-title="<?php echo substr($item->post_content, 0, 80) ; ?>">
                                                <a class="a2a_button_facebook" onclick="record_share_data('<?php echo $item->ID; ?>','fb')"></a>
                                                <a class="a2a_button_twitter" onclick="record_share_data('<?php echo $item->ID; ?>','twitter')"></a>
                                                <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                                                </div>

                                                <script async src="https://static.addtoany.com/menu/page.js"></script>
                                                </div>



                                    </div>
                                </div>

                                <div id='append_comments'>
                                <?php
                                    $comments = $wpdb->get_results("SELECT comment,user_id,user_login FROM `user_feed_comments`,`wp_users` WHERE wp_users.ID=user_id AND `feed_id` = '{$item->ID}' ORDER BY user_feed_comments.id DESC");
                                    if (!empty($comments)) {
                                        foreach ($comments as $comment) {
                                            ?>
                                <div class="user_comment_section">
                                    <div class="user-comment-area submit-comment">
                                        <?php echo get_avatar($comment->user_id, 50); ?>
                                        <div class="another-user-section">
                                            <a
                                                href="<?php home_url('user/') . $comment->user_login; ?>"><?php echo um_get_display_name($comment->user_id); ?></a>
                                            <span><?php echo $comment->comment; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                        }
                                    } ?>
                                </div>
                                <?php if (is_user_logged_in() AND empty ($query_to_check_block) ) {?>
                                
                                <!-- Start user commetns loading for mobile -->
                                <div class="user-comment-area submit-comment">
                                    <?php echo get_avatar($current_user->ID, 50);  ?>
                                    <input placeholder="Write a Comment" type="text" name="feed_comment"  id="feed_comment" required>
                                    <input type="hidden" name="feed_id"
                                        value="<?php echo $item->ID; ?>">
                                </div>
                                <!-- End user commetns loading for mobile -->
                                <?php } ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>

    </div>

    <?php endwhile; ?>
    <?php endif; ?>


    <!-- TimeLine Popup -->
    <div class="CoverPage">
        <div class="cover-close-m">
            <a href="#"><i class="fas fa-times"></i></a>
        </div>
        <div class="CoverPic">
            <div class="edit_cover_header">
                <div class="Cheader-left">
                    <h4>Photos</h4>
                </div>
                <img id='pop_up_image_link' src="" />
                <div class="coverP-bottom">
                <?php 
                if (is_user_logged_in() AND empty ($query_to_check_block)) {?>
                    <div class="thumbs-up like_mobile">
                    <a href="#"
                    feedid="<?php echo $item->ID; ?>">
                    <i
                    class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>

                    <span><?php echo $like_count; ?></span>
                    </a>
                    </div>
                <?php }else{?>
                    <i
                    class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>

                    <span><?php echo $like_count; ?></span>
                <?php }?>


                    
            </div>
            <div class="coverP-right">
                <div class="cover-user-r">
                    <img src="<?php echo um_get_avatar_url(um_get_avatar('', $item->post_author, 52)); ?>">
                     <a  href="<?php echo home_url('/my-time-line/?user=' . $item->user_login); ?>"><?php echo $display_name; ?>
                                        </a>
                    <div class="C-date"><a href="#"><?php echo date("d,F Y  g:i:s A", strtotime($item->post_date));?>
                                        </a> <i class="fas fa-globe-europe"></i></div>
                </div>
                <div class="Coverdots-right"><i class="fas fa-ellipsis-h"></i></div>
                <div class="row ttp">
                    <!--
                    <div class="col-md-6 pd_w"><i class="far fa-thumbs-up"></i> <?php echo $like_count; ?></div>
                    <div class="col-md-6 pd_w text-right">Comments 0</div>
                    -->
                </div>

                <div class="Csocial-area-righ">
                   <?php  if (is_user_logged_in() AND empty ($query_to_check_block)) {?>
                    <div class="thumbs-up CsocialThumbs like_mobile">
                    <a href="#"
                    feedid="<?php echo $item->ID; ?>">
                    <i
                    class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>

                    <span><?php echo $like_count; ?></span>
                    </a>
                    </div>
                <?php } else{?>
                    <div class="thumbs-up CsocialThumbs like_mobile">
                    <i class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>

                    <span><?php echo $like_count; ?></span>
                </div>
                <?php } ?>
                    <div class="thumbs-up CsocialThumbs">
                        <a href="#"> <i class="far fa-comment-alt"></i> <span>Comment</span> </a>
                    </div>
                    <div class="thumbs-up CsocialThumbs">
                    <div class="share-area">
                    <a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
                        <i class="fas fa-share"></i> Share
                    </a>

                    <!--Share My DropDown PC-->
                    <div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2" aria-labelledby="menu2">

                         <li><a target="_blank" href="https://web.whatsapp.com/send?text=<?php echo substr($item->post_content, 0, 80) . "  ( see-more ) "; echo "https://www.travpart.com/English/shortpost/".$item->ID; ?>">Share to whats app</a></li>
                                        <li><a href="#"
                                                onclick="copy_link('<?php echo $p->ID; ?>')">Copy
                                                link to this post</a></li>
                                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="<?php echo get_permalink($item->ID) ?>" data-a2a-title="<?php echo substr($item->post_content, 0, 80) ; ?>">
    <a class="a2a_button_facebook"></a>
    <a class="a2a_button_twitter"></a>
    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
</div>
                       

                     

                    </div>

                    <!--End My DropDown-->

                </div>
                        <!--s<a href="#"> <i class="fas fa-share"></i> <span>Share</span> </a>-->
                    </div>
                </div>
    
                <?php if (is_user_logged_in() AND empty ($query_to_check_block)) {?>
                                  
                <div class="cCovercommentR user-cover-comment submit-commentt " owner="2960" c_name="Nazmul">
                     <?php echo get_avatar($current_user->ID, 50);  ?>
                    <input placeholder="Write a Comment" type="text" name="feed_commentt" id='feed_commentt' required />
                    <input type="hidden" name="feed_id"
                                        value="<?php echo $item->ID; ?>">
                </div>
            <?php } ?>
            <div class="commets_list">
                
            
                <?php
                 $comments = $wpdb->get_results("SELECT comment,user_id,user_login FROM `user_feed_comments`,`wp_users` WHERE wp_users.ID=user_id AND `feed_id` = '{$item->ID}' ORDER BY user_feed_comments.id DESC");
                        if (!empty($comments)) {
                         foreach ($comments as $comment) {?>
            
                                       
                <div class="popup_user-comment-area"> 

                     <?php echo get_avatar($comment->user_id, 50); ?> 
                    <div class="popup_another-user-section"> 
                          <a href="<?php home_url('user/') . $comment->user_login; ?>"><?php echo um_get_display_name($comment->user_id); ?></a>
                        <span><?php echo $comment->comment; ?></span> 
                    </div>

                    
                </div> 
              <?php    }  } ?>
</div>

            </div>
        </div>
    </div>

    <!-- End TimeLine Popup -->



    <script>
        //share feed
        function share_feed(id) {
            jQuery(document).ready(function($) {
                var tour_id = id;
                $.ajax({
                    type: "POST",
                    url: '<?php echo site_url() ?>/submit-ticket/',
                    data: {
                        feed_id: tour_id,
                        action: 'feedback_share'
                    }, // serializes the form's elements.
                    success: function(data) {
                        var result = JSON.parse(data);
                        if (result.status) {
                            alert(result.message);
                            window.location.reload() // show response from the php script.
                        } else {
                            alert(result.message);
                            //window.location.reload()// show response from the php script.
                        }
                    }
                });
            });
        }

        function copy_link() {
            var copyText = document.getElementById("copytext").href;
            document.addEventListener('copy', function(event) {
                event.clipboardData.setData('text/plain', copyText);
                event.preventDefault();
                document.removeEventListener('copy', handler, true);
            }, true);
            document.execCommand('copy');
            alert("Link Copied: ");
        }

        $(function() {
            $('.like_mobile a').click(function() {
                var sc_type = 0;
                if ($(this).find('i').hasClass('liked')) {
                    $(this).find('i').removeClass('liked');
                    $(this).find('span').text(parseInt($(this).find('span').text()) - 1);
                    sc_type = 1;
                } else {
                    $(this).find('i').addClass('liked');
                    $(this).find('span').text(parseInt($(this).find('span').text()) + 1);
                }

                $.ajax({
                    type: "POST",
                    url: '<?php echo site_url("submit-ticket") ?>',
                    data: {
                        feed_id: $(this).attr('feedid'),
                        action: 'feedback_like'
                    },
                    success: function(data) {}
                });
            });

            $('.comment_mobile').click(function() {
                $(this).parent().parent().find('.submit-comment').toggle();
                $(this).parent().parent().find('.user_comment_section').toggle();
            });
            <?php
            $image_url = get_avatar_url($current_user->ID,50);
            $user_name = um_get_display_name($current_user->ID);
            ?>
            $('.submit-comment').keydown(function(e) {
                var image = '<?php echo $image_url ?>';
                var user_name ='<?php echo $user_name  ?>';
                var feed_comment = $(this).find('input[name="feed_comment"]').val();
                if (e.which == 13 ) {
                    var feed_id = $(this).find('input[name="feed_id"]').val();
                    
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo admin_url("admin-ajax.php"); ?>',
                        dataType: 'json',
                        data: {
                            action: 'feed_comment',
                            feed_id: feed_id,
                            feed_comment: feed_comment
                        },
                        success: function(data) {
                            if (data.success) {
                              //  window.location.href =
                                    //echo add_query_arg(); ?>";
                       
                         $("#append_comments").append('<div class="user_comment_section"><div class="user-comment-area submit-comment"><div class="user-comment-area submit-comment"><img style="margin-left:-12px!important" src='+image+'><div class="another-user-section"><a href="">'+user_name+'</a><span>'+feed_comment+'</span></div></div></div></div>');
                         $('#feed_comment').val('');
  
                            } else {
                                alert('Please Write Comment!.');
                            }
                        }
                    });
                }
        
            });
             $('.submit-commentt').keydown(function(e) {
                var feed_comment = $(this).find('input[name="feed_commentt"]').val();
                if (e.which == 13) {
                     var image = '<?php echo $image_url ?>';
                var user_name ='<?php echo $user_name  ?>';
                    var feed_id = $(this).find('input[name="feed_id"]').val();
                    
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo admin_url("admin-ajax.php"); ?>',
                        dataType: 'json',
                        data: {
                            action: 'feed_comment',
                            feed_id: feed_id,
                            feed_comment: feed_comment
                        },
                        success: function(data) {
                            if (data.success) {
                             //  alert(' Comment Posted successfully');
                             $(".commets_list").append('<div class="popup_user-comment-area"><img style="margin-left:20px!important" src='+image+'> <div class="popup_another-user-section">'+user_name+'  <a href="#"></a><span>'+feed_comment+'</span> </div> </div>');
                                $('#feed_commentt').val('');
                            } else {
                                alert('Please Write Comment.');
                            }
                        }
                    });
                }
            });

            $(".post-image img").click(function() {
                $(".CoverPage").addClass("Cphotohide");
            });

            $(".cover-close-m").click(function() {
                $(".CoverPage").removeClass("Cphotohide");
            });

        });

    </script>

    <script>
        // $(".comment_mobile").click(function() {
     //        $(".sign-in-user").addClass("open");
     //    });

        $('.comment_mobile').on('click', function(e) {
          $('.sign-in-user').toggleClass('open');
        });

    </script>


    <style>
        .open{
            display: block;
        }
    </style>
        <script >
        function record_share_data(id,type){
        var post_id = id;
        var type=type;
        $.ajax({
        type: "POST",
        url: '<?php echo site_url() ?>/submit-ticket/',
        data: {
        feed_id: post_id,
        type:type,
        action: 'social_share_record'
        }, // serializes the form's elements.
        success: function(data) {
        }
        });
        }
        </script>

<script>
       // $("#open_photo").click(function() {
        $(document).on("click","#open_photo",function(){
        var currentAnchor = $(this);
       // alert(currentAnchor.attr('src'));
        var link=currentAnchor.attr('src');
        $('#pop_up_image_link').attr('src',link);

    });
</script>

<?php get_footer();
