<?php
/* Template Name: confirm */
get_header();
global $wpdb; 

$tokens = explode('/', $_SERVER['REQUEST_URI']);
$tour=$tokens[sizeof($tokens)-2];

if(!is_numeric(trim($tour))){
    exit(wp_redirect(home_url()));
}

$sql = "SELECT * FROM wp_tour where id=".intval($tour);

$result = $wpdb->get_results($sql) ;
if(count($result)==0){
    exit(wp_redirect(home_url()));
}

$firstname='';
$lastname='';
$city='';
$country='';
$email='';
$phone='';

$num=$result[0]->number_of_adult + $result[0]->number_of_child;
$net_price=$result[0]->total;
if(is_user_logged_in()) {
    $tp_cpn_valdty=intval(get_option('_travpart_vo_option')['tp_cpn_valdty']);
    $discount_rp=$wpdb->get_var("SELECT discount_rp FROM `wp_coupons` WHERE used=0 AND tour_id={$tour} AND DATE_SUB(CURDATE(), INTERVAL {$tp_cpn_valdty} DAY)<=date(get_time) AND user_id=".wp_get_current_user()->ID);
    $net_price-=$discount_rp*get_option('person_price_ration')*1.01;
    $wp_user=wp_get_current_user();
    $user=$wpdb->get_row("SELECT `email`,`country`,`region`,`phone` FROM `user` WHERE username='{$wp_user->user_login}'");
    $fullname=um_get_display_name($wp_user->ID);
    if($pos=strrpos($fullname,' ')) {
        $firstname=substr($fullname, 0, $pos);
        $lastname=substr($fullname, $pos);
    }
    else {
        $firstname=$fullname;
    }
    $city=$user->region;
    $country=$user->country;
    $email=$user->email;
    $phone=$user->phone;
}
?>

<input id="cs_RMB" type="hidden" value="<?php echo get_option('_cs_currency_RMD'); ?>" />
<input id="cs_USD" type="hidden" value="<?php echo get_option('_cs_currency_USD'); ?>" />
<input id="cs_AUD" type="hidden" value="<?php echo get_option('_cs_currency_AUD'); ?>" />

<script src="<?php bloginfo('template_url'); ?>/js/jquery-ui.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/tour.js?v5"></script>
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/smoothness/jquery-ui.css" type="text/css" media="all" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/toastr.min.css" />
<script src="<?php bloginfo('template_url'); ?>/js/toastr.min.js" type="text/javascript"></script>

 <style>
    .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
    .ui-timepicker-div dl { text-align: left; }
    .ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
    .ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
    .ui-timepicker-div td { font-size: 90%; }
    .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
    .ui-timepicker-rtl{ direction: rtl; }
    .ui-timepicker-rtl dl { text-align: right; }
    .ui-timepicker-rtl dl dd { margin: 0 65px 10px 10px; }
    
  .summary .col-md-12{
    margin-bottom:20px;
  }
.buy_btn {
margin-top:5px !important;}
    .summary input[type='text']{
    width:100%;
    }
    #turcnt h2 {
    padding-top: 10px;
    font-size: 22px;
}
.text-right {
    font-size: 15px;
}
.tour-info-text {
    font-size: 15px;
}
label {
    font-size: 15px;
}
.tour_mob_des {
    display: none;
}

.input-icon{
    position: absolute;
    left: 6px;
    color: #80808094;
    font-size:20px; 
    top: calc(50%); /* Keep icon in center of input, regardless of the input height */
}
#date_begin,#Depart_date    {
    padding-left: 30px;
}
.input-wrapper{
    position: relative;
}
.pay_con_pos{
    margin-left: 15px;
}
.col-md-12.tvl_mob_des {
    display: none;
}
.paypal_logo_pay input.btn{
    padding: 5px 20px;
    background-color: #22b14c;
    color: white;
    font-size: 20px;
    border-radius: 0px;
    box-shadow: 0 0 0 5px hsl(0, 0%, 87%);
}
.palpal_logo_img img {
    width: 30%;
} 

.paypal_logo_pay{
    padding-top: 15px;
}
.tvl_toggle {
    font-size: 15px;
    padding-left: 0px;
    margin-left: -90px;
}
.switch {
  position: relative;
  display: inline-block;
  width: 65px;
  height: 20px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: -1px;
  bottom: -3px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #00a2e8;
}

input:focus + .slider {
  box-shadow: 0 0 1px #00a2e8;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(50px);
  background-color: #3f48cc;
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.tvl_toggle_btn {
    padding-left: 0px;
    margin-left: -66px;
}
@media(max-width: 600px) {
    .tour-info-text {
        float: right;
    }
    .payment_info_text{
        font-size: 15px;
        text-align: justify;
    }
    .tour_mob_des {
        display: block;
    }
    .col-md-12.tour_desk_des {
        display: none;
    }
    .tour_dis_flex {
        display: flex;
    }
    .col-md-6.tour_pad {
        padding-left: 0px;
        padding-right: 0px;
    }
    .col-md-6.tour_main_pad {
        padding-left: 0px;
        padding-right: 0px;
    }
    .text-right {
        font-size: 14px;
    }
    .col-xs-12.pay_addr_pad {
        padding-left: 15px;
        padding-right: 15px;
    }
    .pay_con_pos{
        margin-left: 0px;
    }
    .col-xs-12.pay_flight_pad {
        padding-left: 15px;
    }
    .pay_trav_pad{
        padding-left: 15px !important;
        font-size: 13px !important;
    }
    .tvl_toggle {
        font-size: 12px !important;
        margin-left: 0px;
    }
    .switch {
        width: 53px;
    }
    .tvl_toggle_btn {
        margin-left: 0px;
    }
    input:checked + .slider:before {
        transform: translateX(30px);
    }
    .col-md-12.tvl_mob_des {
        display: block;
    }
    .col-md-6.tvl_desk_des {
        display: none;
    }
    .tvl_dis_flex {
        display: flex;
    }
    label {
        font-size: 13px;
    }
    .col-md-12.pay_mob_btn_dis {
        display: flex;
    }
    .paypal_logo_pay{
        padding-top: 0px;
    }
    .col-md-12.pay_heading {
        margin-top: 70px !important;
    }
}
@media(max-width: 320px) {
    .palpal_logo_img img {
        width: 120% !important;
    }
}
@media(max-width: 360px) {
    .palpal_logo_img img {
        width: 100% !important;
    }
}

@media(max-width: 411px) {
    .palpal_logo_img img {
        width: 70%;
    }
}
                                </style>
          
        <div class="row summary">
            <div class="col-md-1"></div>
            <div class="col-md-10 col-xs-12">
                <div class="row">
                    <div class="col-md-12 pay_heading">
                    <h2 style="text-align: center;">Payment order</h2>
                    </div>
                    <div class="col-md-12">
                        <div id="turcnt">
                            <h2 class="ico3">Tour Information</h2>
                            <div class="col-md-6 tour_main_pad">
                                 <div class="col-md-12 tour_desk_des"> <span class="col-md-7 text-right"><strong>Departure Date:</strong></span>  <span class="col-md-5 tour-info-text"> <?php echo $result[0]->start_date;?></span> </div>
                                 <div class="col-md-12 tour_desk_des"> <span class="col-md-7 text-right"><strong>Number of adults:</strong></span>  <span class="col-md-5 tour-info-text"> <?php echo $result[0]->number_of_adult;?></span> </div>
                                 <div class="col-md-12 tour_mob_des">
                                    <div class="tour_dis_flex">
                                        <div class="col-md-6 tour_pad"> <span class="col-md-12 text-right"><strong>Departure Date:</strong></span>  <span class="col-md-12 tour-info-text"> <?php echo $result[0]->start_date;?></span> </div>
                                        <div class="col-md-6 tour_pad"> <span class="col-md-12 text-right"><strong>Number of adults:</strong></span>  <span class="col-md-12 tour-info-text"> <?php
                                        echo $result[0]->number_of_adult;?></span> </div>
                                    </div>
                                 </div>
                                 <div class="col-md-12 tour_desk_des"> <span class="col-md-7 text-right"><strong>Number of rooms:</strong></span>  <span class="col-md-5 tour-info-text"> <?php echo $result[0]->number_of_room;?></span> </div>
                                 <div class="col-md-12 tour_desk_des"> <span class="col-md-7 text-right"><strong>Additional nights:</strong></span>  <span class="col-md-5 tour-info-text"> <?php echo $result[0]->addnightroom;?></span> </div>
                                 <div class="col-md-12 tour_mob_des">
                                    <div class="tour_dis_flex">
                                        <div class="col-md-6 tour_pad"> <span class="col-md-12 text-right"><strong>Number of rooms:</strong></span>  <span class="col-md-12 tour-info-text"> <?php echo $result[0]->number_of_room;?></span> </div>
                                        <div class="col-md-6 tour_pad"> <span class="col-md-12 text-right"><strong>Additional nights:</strong></span>  <span class="col-md-12 tour-info-text"> <?php echo $result[0]->addnightroom;?></span> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 tour_main_pad">
                                <div class="col-md-12 tour_desk_des"> <span class="col-md-7 text-right"><strong>End Date:</strong></span>  <span class="col-md-5 tour-info-text"> <?php echo $result[0]->end_date;?></span></div>
                                <div class="col-md-12 tour_desk_des"> <span class="col-md-7 text-right"><strong>Number of children:</strong></span>  <span class="col-md-5 tour-info-text"> <?php echo $result[0]->number_of_child;?></span></div>
                                <div class="col-md-12 tour_mob_des">
                                    <div class="tour_dis_flex">
                                        <div class="col-md-6 tour_pad"> <span class="col-md-12 text-right"><strong>End Date:</strong></span>  <span class="col-md-12 tour-info-text"> <?php echo $result[0]->end_date;?></span></div>
                                        <div class="col-md-6 tour_pad"> <span class="col-md-12 text-right"><strong>Number of children:</strong></span>  <span class="col-md-12 tour-info-text"> <?php echo $result[0]->number_of_child;?></span></div>
                                    </div>
                                </div>
                                <div class="col-md-12 tour_desk_des"> <span class="col-md-7 text-right"><strong>Number of extra beds:</strong></span>  <span class="col-md-5 tour-info-text"> <?php echo $result[0]->number_of_extra_bed;?></span></div>
                                <div class="col-md-12 tour_desk_des"> <span class="col-md-7 text-right"><strong>Total:</strong></span>  <span class="change_price col-md-5 tour-info-text" or_pic="<?php echo $net_price;?>"> Rp <span class="price_span"> <?php echo $net_price;?></span> </span></div>
                                <div class="col-md-12 tour_mob_des">
                                    <div class="tour_dis_flex">
                                        <div class="col-md-6 tour_pad"> <span class="col-md-12 text-right"><strong>Total:</strong></span>  <span class="change_price col-md-12 tour-info-text" or_pic="<?php echo $net_price;?>"> Rp <span class="price_span"> <?php echo $net_price;?></span> </span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                    <script type="text/javascript">
                                    function alipayment_post(formid) {
                                        var arr = [
                                            { id: "first_name", mes: "Please input first name." },
                                            { id: "last_name", mes: "Please input last name." },
                                            { id: "email", mes: "Please input email." },
                                            { id: "firtname_1", mes: "Please input traveler first name." },
                                            { id: "lastname_1", mes: "Please input traveler last name." },
                                        ];

                                        for (var i = 0; i < arr.length; i++) {
                                            if (document.getElementById(arr[i].id).value == "") {
                                                toastr.warning(arr[i].mes);
                                                return;
                                            }                                
                                        }

                                        var num = parseInt(document.getElementById("hidden_peoplesnum").value);
                                        /*var idds = ['passport_number_', 'passport_country_', 'expired_date_', 'firtname_',
                                            'lastname_', 'phone_number_', 'date_of_birth_', 'passport_email_'];*/
                                      
                                      var idds = ['first_name',
                                            'last_name'];
                                      
                                      
                                        for (var i = 1; i <= num; i++) {
                                            for (var j = 0; j < idds.length; j++) {
                                                var idd_1 = idds[j] + i;                                            

                                                if ($("#" + idd_1).attr("data-require") == "true" && $("#" + idd_1).val() == "") {
                                                    toastr.warning("Please input first name and last name details.");
                                                    return;
                                                }                                               
                                            }
                                        };

                                        document.getElementById(formid).submit();
                                    }
                    </script>
                    <form method="post" id="paypalPayment" action="<?php echo home_url()."/wp-content/themes/bali/"; ?>paypal/paypal.php"  class="payment_form">
                            <h2 class="ico4">Fill in the form below </h2>
                            <div style="padding-bottom:15px;font-size: 12px;" class="payment_info_text">
                            The purchase of any traveling packages, services or booking through The Company constitutes a contractual arrangement between you and The Company, and represents The Traveler’s acceptance of The Company’s Terms & Conditions set out herein. Please ensure that you read carefully and understand these Terms & Conditions prior to booking. You are advised to check The Company’s website or to request the latest version of the
                            <a href="https://www.travpart.com/English/termsofuse/" target="_blank">Terms & Conditions</a>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <span class="pay_con_pos" style="color:red;font-size: 15px;">CONTACT PERSON</span>
                        </div>

                            <div class="col-md-12 form-group">
                        
                                <div class="col-md-6 col-xs-12">
                                <label for="first_name">
                                    First name:
                                    <span style='color:red;'>*</span>
                                </label>
                                <input type="text" name="payer_fname" value="<?php echo $firstname; ?>" id="first_name" class="con_pay_fname payer_fname form-control" />
                                </div>
                                <div class="col-md-6 col-xs-12">
                                <label for="last_name">
                                    Last name:
                                    <span style='color:red;'>*</span>
                                </label>
                                <input type="text" name="payer_lname" value="<?php echo $lastname; ?>" id="last_name" class="con_pay_lname payer_lname form-control" /> 
                                </div>
                                <div class="col-xs-12 pay_addr_pad">
                                <label for="addrs">
                                    Address:
                                </label>
                                <textarea name="payer_address" id="addrs" class="payer_address form-control"></textarea>
                                </div>
                                <div class="col-md-3 col-xs-4">
                                <label for="zip">
                                    Zip code:
                                </label>
                                <input type="text" name="payer_zip" value="" id="zip" class="payer_zip form-control" />
                                </div>
                                <div class="col-md-9 col-xs-8">
                                <label for="city">
                                    City:
                                </label>
                                <input type="text" name="payer_city" value="<?php echo $city; ?>" id="city" class="payer_city form-control" />
                                </div>
                                <div class="col-md-3 col-xs-4">
                                <label for="state">
                                    State:
                                </label>
                                <input type="text" name="payer_state" value="" id="state" class="payer_state form-control" />
                                </div>
                                <div class="col-md-9 col-xs-8">
                                <label for="country">
                                    Country:
                                </label>
                                <input type="text" name="payer_country" value="<?php echo $country; ?>" id="country" class="payer_country form-control" /> 
                                </div>
                            <div class="col-md-6 col-xs-12">
                                <label for="email">
                                    E-mail:
                                    <span style='color:red;'>*</span>
                                </label>
                                <input type="text" name="payer_email" value="<?php echo $email; ?>" id="email" class="con_pay_email payer_email form-control" /> 
                                </div>
                            <div class="col-md-6 col-xs-12">
                                <label for="phone_number">
                                    Phone Number:
                                </label>
                                <input type="text" name="phone_number" value="<?php echo $phone; ?>" id="phone_number" class="con_pay_phno payer_email form-control" />
                            </div>


                            <div class="col-xs-12 pay_flight_pad" style="color:red;margin-top:15px;font-size: 15px;">FLIGHT & HOTEL</div>
                            <div class="col-md-6 col-xs-12">
                                <label for="flight_number">Flight Number:</label>
                                <input type="text" name="flight_number" value="" id="flight_number" class="payer_email form-control" />
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <label for="hotel">Hotel:</label>
                                <input type="text" name="hotel" value="" id="hotel" class="payer_email form-control" />
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <label for="air_name">Airport Arrival Name:</label>
                                <input type="text" name="air_name" value="" id="air_name" class="payer_email form-control" />
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <label for="air_depart_name">Airport Departure Name:</label>
                                <input type="text" name="air_depart_name" value="" id="air_depart_name" class="payer_email form-control" />
                            </div>
                            
                            
                             <div class="col-md-6 col-xs-12">
                                
                                
                                <div class="input-wrapper">
                                  <label for="stuff" class="far fa-calendar-alt input-icon"></label>
                                  <label for="arival_time"><strong>Arrival</strong></label>
                                <input type="text"  name="date_begin" id="date_begin"  class="form-control" value=""> 
                                                                    
                                </div>
                            </div> 
                            
                            
                            <div class="col-md-6 col-xs-12">
                                <div class="input-wrapper">
                                  <label for="stuff" class="far fa-calendar-alt input-icon"></label>
                                 <label for="arival_time"><strong>Departure</strong></label>
                                <input type="text" name="Depart_date" id="Depart_date" class="form-control" value=""> 
                                                                    
                                </div>
                            </div> 
                            <!--<div class="col-md-4 col-xs-12">
                                <label for="arival_time">Arrival Time（Date）</label>
                                <input type="date" name="arival_time" value="" id="arival_time" class="form-control" min="<?php echo date('Y-m-d'); ?>" onchange="arrival_date()" />
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <label for="a_hour">Arrival Time（Hour）</label>
                                <select name="a_hour" id="a_hour" class="form-control"></select>
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <label for="a_minute">Arrival Time（Minute）</label>
                                <select name="a_minute" id="a_minute" class="form-control"></select>
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <label for="depart_time">Depart-Time（Date）</label>
                                <input type="date" name="depart_time" value="" id="depart_time" class="form-control" />
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <label for="d_hour">Depart-Time（Hour）</label>
                                <select name="d_hour" id="d_hour" class="form-control"></select>
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <label for="d_minute">Depart-Time（Minute）</label>
                                <select name="d_minute" id="d_minute" class="form-control"></select>
                            </div>-->
                            <script type="text/javascript">
                                function arrival_date()
                                {
                                
                                    var date1= $("#arival_time").val();
                                    $("#depart_time").attr("min",date1);

                                }
                                $(document).ready(function () {
                                    var hour = "",
                                        minute = "";
                                    for (var i = 0; i <= 23; i++) {
                                        var str = i;
                                        if (i < 10)
                                            str = "0" + i;
                                        hour += "<option value='" + str + "'>" + str + "</option>";
                                    }
                                    for (var i = 0; i <= 59; i++) {
                                        var str = i;
                                        if (i < 10)
                                            str = "0" + i;
                                        minute += "<option value='" + str + "'>" + str + "</option>";
                                    }

                                    $("#a_hour,#d_hour").html(hour);
                                    $("#a_minute,#d_minute").html(minute);
                                });
                            </script>

                            <!--
                            <div class="col-xs-6">
                                <label for="passport_number">Passport Number（Option）:</label>
                                <input type="text" name="passport_number" value="" id="passport_number" class="form-control" />
                            </div>
                            <div class="col-xs-6">
                                <label for="passport_country">Passport Country（Option）:</label>
                                <input type="text" name="passport_country" value="" id="passport_country" class="form-control" />
                            </div>
                            <div class="clearfix"></div>-->
                             <?php
                                 for($autonum=1;$autonum<=1;$autonum++)
                                 {
                                     echo("<div class='col-xs-12'><hr></div>");
                                     echo("<div class='col-xs-12 tvl_dis_flex'><div class='col-xs-3 pay_trav_pad' style='color:red;font-size: 15px;padding-left: 0px;'>TRAVELER {$autonum}：</div><div class='col-xs-4 tvl_toggle'>Same as contact person</div><div class='col-xs-5 tvl_toggle_btn'><label class='switch'>
                                        <input type='checkbox' class='traveldata'><span class='slider round'></span></label></div></div>");

                                     echo "
                             <div class='col-md-6 tvl_desk_des'>
                                <label for='firtname_{$autonum}'>First Name:<span style='color:red;'>*</span></label>
                                <input type='text' name='firtname_{$autonum}' value='' id='firtname_{$autonum}' class='form-control trav_fname' data-require='true'/>
                            </div>
                             <div class='col-md-6 tvl_desk_des'>
                                <label for='lastname_{$autonum}'>Last Name:<span style='color:red;'>*</span></label>
                                <input type='text' name='lastname_{$autonum}' value='' id='lastname_{$autonum}' class='form-control trav_lname' data-require='true'/>
                            </div>
                            <div class='col-md-12 tvl_mob_des'>
                                <div class='tvl_dis_flex'>
                                   <div class='col-md-6'>
                                <label for='firtname_{$autonum}'>First Name:<span style='color:red;'>*</span></label>
                                <input type='text' name='firtname_{$autonum}' value='' id='firtname_{$autonum}' class='trav_fname form-control' data-require='true'/>
                            </div>
                             <div class='col-md-6'>
                                <label for='lastname_{$autonum}'>Last Name:<span style='color:red;'>*</span></label>
                                <input type='text' name='lastname_{$autonum}' value='' id='lastname_{$autonum}' class='form-control trav_lname' data-require='true'/>
                            </div> 
                                </div>
                            </div>
                            <div class='col-md-6 tvl_desk_des'>
                                <label for='passport_number_{$autonum}'>Passport Number:</label>
                                <input type='text' name='passport_number_{$autonum}' value='' id='passport_number_{$autonum}' class='form-control' data-require='true'/>
                            </div>
                            <div class='col-md-6 tvl_desk_des'>
                                <label for='passport_country_{$autonum}'>Passport Country:</label>
                                <input type='text' name='passport_country_{$autonum}' value='' id='passport_country_{$autonum}' class='form-control' data-require='true'/>
                            </div>
                            <div class='col-md-12 tvl_mob_des'>
                                <div class='tvl_dis_flex'>
                                    <div class='col-md-6'>
                                <label for='passport_number_{$autonum}'>Passport Number:</label>
                                <input type='text' name='passport_number_{$autonum}' value='' id='passport_number_{$autonum}' class='form-control' data-require='true'/>
                            </div>
                            <div class='col-md-6'>
                                <label for='passport_country_{$autonum}'>Passport Country:</label>
                                <input type='text' name='passport_country_{$autonum}' value='' id='passport_country_{$autonum}' class='form-control' data-require='true'/>
                            </div>
                                </div>
                            </div>
                            <!-- <div class='col-md-6'>
                                <label for='expired_date_{$autonum}'>Expired Date:<span style='color:red;'>*</span></label>
                                <input type='date' name='expired_date_{$autonum}' value='' id='expired_date_{$autonum}' class='form-control' data-require='true'/>
                            </div> -->
                              <div class='col-md-6 tvl_desk_des'>
                                <label for='phone_number_{$autonum}'>Phone Number:</label>
                                <input type='text' name='phone_number_{$autonum}' value='' id='phone_number_{$autonum}' class='form-control trav_phno' data-require='false'/>
                            </div>
                               <div class='col-md-6 tvl_desk_des'>
                                <label for='date_of_birth_{$autonum}'>Date of Birth:</label>
                                <input type='date' name='date_of_birth_{$autonum}' value='' id='date_of_birth_{$autonum}' class='form-control' data-require='true'/>
                            </div>
                            <div class='col-md-12 tvl_mob_des'>
                                <div class='tvl_dis_flex'>
                                <div class='col-md-6'>
                                <label for='phone_number_{$autonum}'>Phone Number:</label>
                                <input type='text' name='phone_number_{$autonum}' value='' id='phone_number_{$autonum}' class='form-control trav_phno' data-require='false'/>
                            </div>
                               <div class='col-md-6'>
                                <label for='date_of_birth_{$autonum}'>Date of Birth:</label>
                                <input type='date' name='date_of_birth_{$autonum}' value='' id='date_of_birth_{$autonum}' class='form-control' data-require='true'/>
                            </div>
                                </div>
                            </div>
                                 <div class='col-md-6'>
                                <label for='passport_email_{$autonum}'>Email:</label>
                                <input type='text' name='passport_email_{$autonum}' value='' id='passport_email_{$autonum}' class='form-control trav_email' data-require='false'/>
                            </div>";
                                 }      
                                 ?>                           
                                <div class="clearfix"></div>
                                
                                
                                    <div class="col-md-12" style="text-align:center;">
                                        
                                        <input type="hidden" name="action" value="process" />
                                        <input type="hidden" name="cmd" value="_cart" /> <?php // use _cart for cart checkout ?>
                                        <input type="hidden" name="currency_code" value="USD" />
                                        <input type="hidden" name="invoice" value="<?php echo date("His").rand(1234, 9632); ?>" />
                                        <input type="hidden" class="site_url" value="<?php echo get_site_url() ?>" />
                                        <input type="hidden" name="product_id" class="product_id" value="<?php echo $result[0]->id;?>" />
                                        <input type="hidden" name="productname" value="Tour" />
                                        <input type="hidden" name="requestmount" value="1" />
                                        <input type="hidden" name="tour_order" value="<?php echo $tour;?>" />

                                <input type="hidden" name="hidden_peoplesnum" id="hidden_peoplesnum" value="<?php echo $num;?>" />
                                
                                                
                                        <?php 
                                        $amount=$net_price;
                                        $from_Currency="IDR";
                                        $to_Currency="USD";

                                        $option='_cs_currency_'.$to_Currency;

                                        if(get_option($option)){
                                            $converted_amount=($amount*get_option($option));
                                        } else {

                                            $amount = urlencode($amount);
                                            $from_Currency = urlencode($from_Currency);
                                            $to_Currency = urlencode($to_Currency);
                                            $get = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency");
                                            $get = explode("<span class=bld>",$get);
                                            $get = explode("</span>",$get[1]);  
                                            $converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
                                        }
                                        ?>
                                        <input type="hidden" name="product_price" value="<?php echo number_format($converted_amount,2);?>" />
                                    </div>
                                

                            </div>
                            <div class="clearfix"></div>
                                <h2 class="ico4">Credit card / Paypal</h2>
                                <div class="col-md-12 pay_mob_btn_dis">
                                <div class="col-md-6 palpal_logo_img">
                                <img src="<?php bloginfo('template_url'); ?>/images/paypal_logo.png" alt="">
                                </div>
                                <div class="col-md-6 paypal_logo_pay">
                            <input class="btn btn-default arr-sm buy_btn" type="button" value="PAYMENT" onclick="alipayment_post('paypalPayment');" />
                                </div>
                                </div>
                            </form>
                            <div class="clearfix"></div>
                            <!-- <h2 class="ico4">Bank Wire Transfer</h2>
                            <div class="col-md-12">
                                <div class="col-md-8"> 
                                    Beneficiary Bank : （CIMB Niaga）<br />
Beneficiary Account Number :  <br />
800135524000 (IDR Account) <br>
800142963821 (CNY Account) <br>
800135713740 (USD Account)<br>
SwiftCode :  BNIAIDJA<br />
Bank Address  : Jl. Prof. Dr. Supomo No. 15 A, Tebet, Daerah Khusus Ibukota Jakarta 12810, Indonesia<br />
Company Name : PT Tur Indo Bali<br />
Company Address : Jalan Tanah Abang 1 No. 11F, Petojo Selatan, Gambir – Jakarta 10160, Jawa Barat, Indonesia<br />
                                </div>
                                <div class="col-md-4">
                                    <input class="btn btn-default arr-sm buy_btn pay_btn" Payment_Type="wire transfer" type="button" value="payment" />
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <h2 class="ico4">Cheque Deposit</h2>
                            <div class="col-md-12">
                                <div class="col-md-8">
                                        Pay to : PT Tur Indo Bali<br />
Mail to : PT Tur Indo Bali<br />
Address : Jalan Tanah Abang 1 No. 11F, Petojo Selatan, Gambir – Jakarta 10160, Jawa Barat, Indonesia<br /><br />
Deposited into: CIMB Niaga                                      
                                </div>
                                <div class="col-md-4">
                                    <input class="btn btn-default arr-sm buy_btn pay_btn" Payment_Type="cheque" type="button" value="payment" />
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <h2 class="ico4">Payment informations</h2>
                            <div class="col-md-12 text-justify">
                                <p>Cheque must be received into our office or deposited into our bank account 15 days prior to tour departure date. If payment is not received 15 days prior to tour departure date, an alternative payment method will be requested from the customer. Mailed Cheque will be returned to the sender.
<br /><br />
We recommend that you mail or send your cheque into our office at least 25-30 days prior to your tour departure date. We do not accept International Cheque.
<br /><br />
We will not process a reservation until we receive payment and the payment clears in our system.</p>

                            </div> -->
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
      
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js" ></script>
<script src="https://brandonrubenstein.com/js/core.js" ></script>
<script>

        
        $(document).ready(function(){
        
            var site_url=$(".site_url").val();
            
            $(".pay_btn").click(function(){
            
            var valid=1;
            $(".payment_form .form-control").each(function(){
                
                if($(this).val().trim()==""){
                    valid=2;
                    $(this).attr("style","border:1px solid red");
                    //$(this).focus();
                }else{
                    $(this).attr("style","");
                }
                
            });
            
            if(valid==1){
                var dataString = { 'product_id':$(".product_id").val(),
                                   'payer_fname':$(".payer_fname").val(),
                                   'payer_lname':$(".payer_lname").val(),
                                   'payer_address':$(".payer_address").val(),
                                   'payer_city':$(".payer_city").val(),
                                   'payer_state':$(".payer_state").val(),
                                   'payer_zip':$(".payer_zip").val(),
                                   'payer_country':$(".payer_country").val(),
                                   'payer_email':$(".payer_email").val(),
                                   'Payment_Type':$(this).attr("Payment_Type")
                                }; 

                        $.ajax({
                            type: "POST",
                            url: site_url+'/wp-content/themes/bali/sendconfirmationmail.php',
                            data: dataString,
                            success: function (data) {
                                window.location=site_url+"/payment";
                            }
                        });
             }
            });
        });
        function setTravelData(){
      if ($(".traveldata").is(":checked")) {
        $('.trav_fname').val($('.con_pay_fname').val());
        $('.trav_lname').val($('.con_pay_lname').val());
        $('.trav_phno').val($('.con_pay_phno').val());
        $('.trav_email').val($('.con_pay_email').val());
        $('.trav_fname,.trav_lname,.trav_phno,.trav_email').attr('disabled', 'disabled');
      } else {
        $('.trav_fname,.trav_lname,.trav_phno,.trav_email').removeAttr('disabled');
      }
    }
    
$(document).ready(function(){
    $('.traveldata').click(function(){
      setTravelData();
    });
});
</script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-sliderAccess.js"></script>
<script>
jQuery(document).ready(function($){
    $('#date_begin,#Depart_date').datetimepicker({'millisec':false}); 
});
</script>
           
<?php


get_footer();


?>
