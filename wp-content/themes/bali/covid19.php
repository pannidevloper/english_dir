<?php
/*
Template Name: covid19
*/
get_header();

global $wpdb;
$show_popup = get_option('show_covid_app_notifaction');

if ($show_popup == 0) {
  exit(wp_redirect(home_url('timeline')));
}
$current_user = wp_get_current_user();
$wp_user_ID = $current_user->ID;
if (!is_user_logged_in()) {
  exit(wp_redirect(home_url('login')));
}

$data = $wpdb->get_row("SELECT id,datetime,user_option FROM `wp_covid19_data` WHERE wp_user_id='{$current_user->ID}' ORDER BY datetime DESC LIMIT 1");
if (!empty($data->user_option) && $data->user_option == 'yes' && floor((time() - $data->datetime) / 86400) < 14) {
  exit(wp_redirect(home_url('timeline')));
}

if (isset($_POST['lat'])) {

  $lat =  $_POST['lat'];
  $lng = $_POST['lng'];
  $date = date('m/d/Y h:i:s a', time());
  $timestamp = strtotime($date);

  if (!empty($data->id)) {
    $wpdb->update(
      'wp_covid19_data',
      array(
        'datetime' => $timestamp,
        'lat' => $lat,
        'lng' => $lng,
      ),
      array('id' => $data->id),
      array(
        '%s',
        '%s',
        '%s',
      ),
      array('%d')
    );
  } else {
    $sql = $wpdb->prepare("INSERT INTO `wp_covid19_data` (`wp_user_id`, `user_option`,`datetime`,`lat`,`lng`) values (%d, %s,%s,%s,%s)", $wp_user_ID, 'yes', $timestamp, $lat, $lng);
    $wpdb->query($sql);
  }
  exit(wp_redirect(home_url('timeline')));
}
?>

<style>
  .gm-style-iw.gm-style-iw-c {
    width: 3%;
  }

  .top_tpp {
    padding: 20px;
  }

  .covclass {
    background: #cccccc00;
    padding: 20px;
  }

  .covclass p {
    color: #000;
    font-size: 15px;
    letter-spacing: 1px;
  }

  button.btn.btn_no {
    border: 1px solid #22b14c;
    color: #22b14c;
    padding: 5px 20px;
    font-size: 18px;
    background: no-repeat;
  }

  button.btn.btn_yes {
    padding: 5px 20px;
    font-size: 18px;
    background: #22b14c;
    color: #fff;
    margin-left: 15px;
  }

  .small_s {
    font-size: 10px !important;
    margin-top: 20px;
    letter-spacing: 1px;
  }

  .text-center2 {
    text-align: center !important;
    width: 25%;
    margin: auto;
    padding: 30px 0px;
  }

  .text-center_c img {
    max-width: 40%;
  }

  .lis_s {
    margin: 10px;
    font-weight: bold;
    width: 22%;
    padding: 10px;
  }

  .padd_l {
    margin: 10px;
  }

  .con {
    background: red;
    color: #fff;
    border-radius: 100%;
    padding: 6px 10px;
  }

  .media-left {
    margin: 5px;
  }

  .media {
    padding: 5px;
  }

  .reds {
    color: red;
  }

  .h-as {
    margin: 10px;
  }

  .icon_btn1 {
    width: 10%;
    background: #22b14c;
    padding: 10px;
    text-align: center;
    color: #fff;
    font-size: 14px;
  }

  .icon_btn2 {
    width: 75%;
  }

  .icon_btn3 {
    width: 10%;
    font-size: 14px;
    padding: 10px;
    text-align: center;
    color: #333;
    border: 2px solid #22b14c;
  }

  .icon_btn {
    display: inline-block;
    vertical-align: middle;
  }

  .relatinve_d {
    position: relative;
  }

  .row_bg {
    background: #22b14c;
    border: 2px solid #22b14c;
    width: 60%;
    z-index: 1;
    left: 10px;
    position: absolute;
    top: 20px;
  }

  @media (max-width: 600px) {

    button.btn.btn_yes,
    button.btn.btn_no {
      padding: 5px 15px;
      font-size: 11px;
      letter-spacing: 2px;
    }

    .text-center2,
    .padd_l .p1,
    .padd_l .p2,
    .lis_s {
      width: 100% !important;
    }

    .padd_l,
    .lis_s {
      margin: 0px !important;
    }

    .icon_btn1 {
      background: #22b14c;
      padding: 0px;
      text-align: center;
      color: #fff;
      font-size: 14px;
    }

    .icon_btn3 {
      width: 12%;
      font-size: 14px;
      background: #fff;
      padding: 0px;
      text-align: center;
      height: 37px;
      line-height: 37px;
      color: #333;
      border: 0px solid #22b14c;
    }

    .top_tpp {
      margin-top: 12px;
    }

    .custom_ini {
      font-size: 10px !important;
    }
 
    .form-control:disabled, .form-control[readonly] {
      background-color: #fff !important;
      opacity: 1;
      border: 0px;
      box-shadow: none;
  }
  .dlat_t {
      display: inline-block;
  }
  .dlat_i, .fr_white {
      display: inline-block;
      width: 40%;
  }

  .cs_h3{
    padding: 0px !important;  
  }
    .marf_b {
      margin-top: 55px !important;
    }

    .covclass {
      padding-top: 80px;
    }

    .text-center_c img {
      max-width: 100%;
    }

    .pac-container {
      width: 42% !important;
    }
  }

  button.gm-control-active.gm-fullscreen-control {
    display: none;
  }
    .custom_ini {
    border: 2px solid #9ac7cc;
  }
  #pac-input {
    padding: 10px;
    z-index: 1 !important;
    left: 0px !important;
    border-radius: 0px;
  }

  #map2 {
    position: relative;
    height: 600px;
  }

  #pac-input {
    z-index: 1;
    left: 0;
  }

  .top_tpp {
    background: #cccccc85;
    padding: 10px;
  }

  .gm-style-mtc {
    display: none;
  }
.dlat_t{
  padding: 5px; 
}
  .custom_f .form-group {
      margin: 10px 4px;
      border: 1px solid #ccc;
      border-radius: 2px;
  }

  .custom_ini {
    font-size: 15px;
  }

  .location_btn {
    position: absolute;
    right: 10px;
    top: 20px;
    z-index: 2;
  }

  .trac_btn {
    background: #fff;
    border: 1px solid #ccc;
    padding: 5px 20px;
    text-align: left;
  }

  .trac_btn:after {
    content: "\25bc";
    position: absolute;
    top: 30%;
    right: 3px;
  }

  .gmnoprint.gm-bundled-control.gm-bundled-control-on-bottom {
    right: 55px !important;
  }

  .l_plan:hover {
    background: #448c1f7a;
    transition: 1s ease;
  }

  .l_plan {
    width: 55px;
    height: 55px;
    cursor: pointer;
    text-align: center;
    border-radius: 100%;
    position: absolute;
    z-index: 2;
    padding: 7px;
    background-color: #fff;
    font-size: 25px;
    right: 19px;
    bottom: 20%;
}

  .m_plan {
    color: #fff;
    width: 100px;
    height: 100px;
    text-align: center;
    border-radius: 100%;
    position: absolute;
    z-index: 2;
    padding: 10px;
    background-color: #22b14c;
    font-size: 25px;
    right: 10px;
    top: 32%;
  }

  .cm_plan {
    color: #fff;
    width: 100px;
    height: 100px;
    text-align: center;
    border-radius: 100%;
    position: absolute;
    z-index: 2;
    padding: 10px;
    background-color: #22b14c;
    font-size: 25px;
    right: 10px;
    bottom: 2%;
  }

  .f_plan {
    color: #22b14c;
    width: 100px;
    height: 100px;
    text-align: center;
    border-radius: 100%;
    position: absolute;
    z-index: 2;
    padding: 10px;
    background-color: #fff;
    font-size: 25px;
    right: 10px;
    top: 12%;
  }

  .gm-svpc {
    display: none;
  }

  .f_plan p,
  .m_plan p,
  .l_plan p,
  .cm_plan p {
    font-size: 12px;
  }

  .mobileapp {
    display: none;
  }

  .div_d_match {
    background: #22b14c;
    width: 66%;
    position: absolute;
    bottom: 21px;
    padding: 17px 10px;
    z-index: 2;
    left: 5px;
  }

  .ds_b {
    display: inline-block;
    vertical-align: middle;
    color: #fff;
    width: 18%;
    font-size: 21px;
    text-align: center;
  }

  .font_s {
    font-size: 15px;
  }

  .padd_l .p1 {
    padding: 10px;
    width: 50%;
  }

  .padd_l .p2 {
    padding: 10px;
    width: 55%;
  }

  .form_g {
    background: #fff;
  }

  .cs_h3 h3 {
      margin: 0px;
      background: #fff;
      padding: 15px;
      font-size: 22px;
      text-align: center;
      letter-spacing: 1px;
  }
  .marf_b{
    margin-bottom: 10px;
  }
  .dlat_i,.fr_white{
      background:#fff;
  }
  .dlat_i input{
    background: #fff !important;
    border: 0px !important;
  }
  .rm_padding{
    padding:0px;
  }
  
</style>

<div class="top_tpp">


  <div class="mains" style="display: none">
  <div class="row marf_b">
    <div class="col-md-7 cs_h3">
      <h3>Please identify your current location and send it to us</h3>
    </div>
    <div class="col-md-5 rm_padding">
       <form class="form-inline custom_f" method="post" action="">
          <div class="form-group fr_white">
            
              <div class="dlat_t">
                Latitude:
              </div>
              <div class="dlat_i">
                <input type="text" class="form-control lat" name="lat" id='lat' readonly placeholder="Longitude">
              </div>
            
          </div>
          <div class="form-group fr_white">
           
            <div class="dlat_t">
              Longitude:
            </div>
            <div class="dlat_i">
              <input type="text" class="form-control lngg" name="lng" id='lng' readonly placeholder="Latitude">
            </div>
            
          </div>
          <button type="submit" class="btn btn-info custom_ini">Send</button>
        </form>
    </div>
  </div>
   




    <div class="relatinve_d">
      <!--<div class="row_bg">
          <div class="icon_btn icon_btn1">
            <i class="fas fa-arrow-down"></i>
          </div>
          <div class="icon_btn icon_btn2">
            <input id="pac-input" class="form-control" type="text" placeholder="Search Places">
          </div>
          <div class="icon_btn icon_btn3">
            <i class="fas fa-search"></i>
          </div>
        </div>-->
      <!--
        <div class="location_btn">
        <button class="btn btn-default trac_btn" type="button">
          Location <br /> Tracker
        </button>
      </div>
        
        <div class="f_plan">
          <i class="fas fa-users"></i>
          <p>Friend's Travel <br /> Plans</p>
        </div>
        
        <div class="m_plan">
          <i class="fas fa-file-alt"></i>
          <p>My Travel <br /> Plans</p>
        </div>
        
        -->
      <div class="l_plan find_l">
        <img src="https://www.travpart.com/English/wp-content/themes/bali/images/location1.png" width="40" height="40" />
      </div>
      <!--
        
        <div class="cm_plan">
          <i class="fas fa-calendar-alt"></i>
          <p>Create a Travel<br /> Plans</p>
        </div>        
        -->
      <!--
        <div class="div_d_match">
          <div class="ds_b">
            30
          </div>
          <div class="ds_b font_s">
            Mon <br> Mar
          </div>
          <div class="ds_b">
            <i class="fas fa-long-arrow-alt-right"></i>
          </div>
          <div class="ds_b">
            30
          </div>
          <div class="ds_b font_s">
            Mon <br> Mar
          </div>
          
        </div>
        -->
      <!--<div class="f_plan">
          <i class="fas fa-users"></i>
          <p>Friend's Travel <br /> Plans</p>
        </div>-->

      <div id="map2"></div>
    </div>
  </div>
  <div class="covclass">
    <div class="text-center_c text-center">
      <img src="https://www.travpart.com/English/wp-content/themes/bali/images/covid19.jpg" />
    </div>

    <div class="form_g">


      <div class="padd_l">
        <p class="p1">
          <span>Currently Travpart is helping to reduce the COVID19 spread.</span>
        </p>
        <p class="p2"><span>We'd need to know if you have any of these symptoms today?</span></p>
      </div>

      <div class="lis_s">
        <p><strong>-Cough</strong></p>
        <p><strong>-Fever</strong></p>
        <p><strong>-Tiredness</strong></p>
        <p><strong>-Difficulty breathing</strong></p>
      </div>
    </div>
    <div class="text-center text-center2">
      <button class="btn btn_no">No </button>
      <button class="btn btn_yes">Yes</button>
    </div>

    <p class="small_s">Your personal information will not be disclose for public but we'll just show your current location.</p>
  </div>
</div>
<script>
  $(document).ready(function() {
    $('.btn_yes').click(function() {
      $('.covclass').hide();
      $('.mains').show();
      $('.find_l').trigger('click');
    })

  })

  $(document).ready(function() {
    $('.btn_no').click(function() {
      $.ajax({
            type: "GET",
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            data: {
                action: 'update_covid19_data',
                user_option: 0
            },
            success: function (data) {
              window.location.href = "<?php echo home_url('timeline'); ?>";
            }
        });
    })
  });
</script>

<script>
  function initAutocomplete() {
    var map, infoWindow;


    var map = new google.maps.Map(document.getElementById('map2'), {
      center: {
        lat: -33.8688,
        lng: 151.2195
      },
      zoom: 13,
      mapTypeId: 'roadmap'
    });


    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location

        }));

        var location = place.geometry.location;
        var lat = location.lat();
        var lng = location.lng();
        $('#lat').val(lat);
        $('#lng').val(lng);

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }

        var infowindow = new google.maps.InfoWindow();

        var marker, i;
        var dt = <?php echo date('m/d/Y'); ?>;
        //for (i = 0; i < locations.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(lat, lng),
          map: map,
          draggable: true,
          icon: 'https://www.travpart.com/English/wp-content/themes/bali/images/mark.png'
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent('<div class="media"><div class="media-left"><a href="#" class="con"><i class="fa fa-info fa-lg"></i></a></div><div class="media-body"><h4> <strong>Possible Covid19 Spread</strong> </h4><p>The user on this location has <br>indicated to have one of the <br>COVID19 symptoms. <br>Reported on <span class="reds">"<?php echo date('m/d/Y'); ?>"</span> </p></div></div><h5 class="h-as">You are advised  to stay away at<br> least 10 meters away from this <br> location.</h5>');
            infowindow.open(map, marker);
          }
        })(marker, i));
        //}

      });
      map.fitBounds(bounds);
    });



    $('.find_l').click(function() {
      /*map = new google.maps.Map(document.getElementById('map2'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 9 
      });*/
      var dt = <?php echo date('m/d/Y'); ?>;
      var infoWindow = new google.maps.InfoWindow;
      // Try HTML5 geolocation.
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var lt = position.coords.latitude;
          var lngg = position.coords.longitude;
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          $('.lat').val(lt);
          $('.lngg').val(lngg);

          marker = new google.maps.Marker({
            position: new google.maps.LatLng(lt, lngg),
            map: map,
            draggable: true,
            icon: 'https://www.travpart.com/English/wp-content/themes/bali/images/mark.png',
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infoWindow.setContent('<div class="media"><div class="media-left"><a href="#" class="con"><i class="fa fa-info fa-lg"></i></a></div><div class="media-body"><h4> <strong>Possible Covid19 Spread</strong> </h4><p>The user on this location has <br>indicated to have one of the <br>COVID19 symptoms. <br>Reported on <span class="reds">"<?php echo date('m/d/Y'); ?>"</span> </p></div></div><h5 class="h-as">You are advised  to stay away at<br> least 10 meters away from this <br> location.</h5>');
              infoWindow.open(map, marker);
            }
          })(marker, 0));
          map.setCenter(pos);
        }, function() {
          handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
      }
    });

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      infoWindow.setPosition(pos);
      infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
      infoWindow.open(map);
    }

  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA069EQK3M96DRcza2xuQb0DZxmYYkVVw8&libraries=places&callback=initAutocomplete" async defer></script>

<?php
get_footer();
?>