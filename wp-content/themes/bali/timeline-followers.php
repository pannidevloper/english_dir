<?php
/*
Template Name: Followers
*/

global $wpdb;
get_header();
if (!is_user_logged_in()) {
   // exit(wp_redirect(home_url('timeline')));
}
if (isset($_GET['user'])) {

        $username = htmlspecialchars( $_GET['user'] );
        $user = get_user_by('login', htmlspecialchars($_GET['user']));
        if (is_numeric($username) AND $username>0) {

        $user = get_user_by('id',  htmlspecialchars ($_GET['user']));
        $username = $user->user_login;
        }

        if($user==NULL){

        exit(wp_redirect(home_url('my-time-line?user='.wp_get_current_user()->user_login)));

        }
        $wp_user_id = $user->ID;
    }
    else{

        $wp_user_id =  wp_get_current_user()->ID;
    }

//$wp_user_id = wp_get_current_user()->ID;

$get_followers = $wpdb->get_results("SELECT  * FROM `social_follow`  WHERE sf_agent_id = '$wp_user_id'");


?>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/friends_connect.js?1694"></script>
<div class="followers_main_area">
    <div class="container new-short-post">
        <div class="row">
            <div class="col-md-12">
                <div class="follower_header_txt">
                    <h2>FOLLOWERS</h2>
                </div>
            </div>
        </div>
        <?php  foreach ($get_followers as $single) { ?>
        <?php


$Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '{$current_user->ID}' AND user1 = '{$single->sf_user_id}'");
if($Data==NULL){

   $Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '{$single->sf_user_id}' AND user1 = '{$current_user->ID}'"); 
}
$btn_icon = $Data->status;
if (empty($Data)) {
    $button_text = '<i class="fas fa-user-plus"></i>'.'Connect';
    $_button_class= 'connect_button';
} elseif ($Data->status == 0 AND $Data->action_by_user== wp_get_current_user()->ID) {
    $button_text = '<i class="fas fa-user-minus"></i>'.'Request <br /> sent';
    $_button_class = 'connect_button_sent';
}
    elseif ($Data->status == 0 AND $Data->action_by_user== $single->sf_user_id) {
    $button_text = '<i class="fas fa-user-minus"></i>'.'Requested <br>to connect';
    $_button_class = 'requested_to_connect';
} elseif ($Data->status == 1) {
    $button_text = 'Connected';
    $_button_class = 'connect_button_connected';
}

$followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$current_user->ID' AND sf_agent_id = '$single->sf_user_id'", ARRAY_A);

          if (!empty($followData)) {
 
            $button_following ="Following";
            $following=1;
            $classs = "following_color";
            $connIcon = ' <i class="fas fa-wifi" id="plus"></i>';
          //  continue;
        }
        else{
            $following=0;
            $classs = "";
            $connIcon = ' <i class="fas fa-wifi" id="plus"></i>';
            $button_following ="Follow";
            

             }
    $author_obj = get_user_by('id', $single->sf_user_id);
 
 
    $user_meta = get_userdata($single->sf_user_id);
    $user_roles = $user_meta->roles;

    if ( in_array( 'um_travel-advisor', $user_roles, true ) ) {
      $role ="Seller";
    }
    elseif ( in_array( 'um_clients', $user_roles, true ) ) {
      $role = "Buyer";
    }
    else
    {
      $role="";
    }
 
 
 ?>
                
        <div class="row">
            
            <div class="col-md-2">
            </div>
            <div class="col-md-8 follower_area">
                <div class="follower_con_area">
                    <div class="follower_con_area_img">
                        <a href="">
                            <?php echo get_avatar( $single->sf_user_id,150 );  ?>
                        </a>
                    </div>
                    <?php if ($Data->status == 1) { ?>
                    <div class="follower_con_area_text button_con_position">
                        <span>
                            <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $author_obj->user_login; ?>">
                                <?php echo um_get_display_name($single->sf_user_id );  ?>
                            </a>
                        </span>
                    </div>
                    <?php } else { ?>
                    <div class="follower_con_area_text">
                        <span><a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $author_obj->user_login; ?>"><?php echo um_get_display_name($single->sf_user_id );  ?></a></span>
                    </div>
                    <?php } ?>
                    <?php if($single->sf_user_id !=  wp_get_current_user()->ID){ ?>
                      <?php if($role=="Seller"){ ?>  
                        <?php if (is_user_logged_in()) { ?>
                        <div class="follower_follow_button">
                            <?php if ($Data->status == 1) { ?>
                            <button  class="btn_follow btn_follow_pos_mob follow_button1 <?php echo $classs; ?> color<?php  echo $single->sf_user_id; ?>" id="<?php  echo $single->sf_user_id; ?>">
                                <i class="fas fa-wifi"></i> 
                                <span class="tx"><?php echo $button_following;  ?></span>
                            </button>
                            <?php } else { ?>
                            <button  class="btn_follow follow_button1 <?php echo $classs; ?> color<?php  echo $single->sf_user_id; ?>" id="<?php  echo $single->sf_user_id; ?>">
                                <i class="fas fa-wifi"></i> 
                                <span class="tx"><?php echo $button_following;  ?></span>
                            </button>
                            <?php } ?>
                        </div>
                      <?php } ?>
                <?php }} ?>
                <?php 
                if (is_user_logged_in()) {
                if($single->sf_user_id !=  wp_get_current_user()->ID){ ?>
                  <?php  if($Data->action_by_user== $single->sf_user_id){?>
                    <div class="follower_connect_button cent<?php echo $single->sf_user_id;?>" <?php echo(($role=="Seller")?"":"style='margin-left:10%;'"); ?> >
                        <button class="follower_con_btn btn_change_text<?php echo $single->sf_user_id;?> <?php echo $_button_class; ?>" id= "<?php echo $single->sf_user_id;?>">
                          <span>
                             <a href ="https://www.travpart.com/English/connection-request-list/">
                                <?php echo $button_text; ?>
                             </a>
                          </span>
                        </button>
                        <?php if ($Data->status == 1) { ?>
                        <button class="p-dots-area_1  hide_<?php echo $single->sf_user_id;?>">
                        <a class="dropdown-toggle" id="dropdownMenuButton_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-h"></i>
                        </a>
                        <div class="dropdown-menu dropdownMenuButton_1" aria-labelledby="dropdownMenuButton_1">
                          <?php if( $button_text=='Connected' ){ ?> 
                          <a data-id="<?php echo $single->sf_user_id;?>" class="unconnect dropdown-item" href="#" >Unconnected</a>
                        <?php } ?>
                        </div>
                      </button>
                      <?php } ?>
                    </div>
                    <?php } else { ?>
                    <div class="follower_connect_button cent<?php echo $single->sf_user_id;?>" <?php echo(($role=="Seller")?"":"style='margin-left:10%;'"); ?>>
                        <button class="follower_con_btn btn_change_text<?php echo $single->sf_user_id;?> <?php echo $_button_class; ?>" id= "<?php echo $single->sf_user_id;?>">
                             <span class= "p-connect-area-text"><?php echo $button_text; ?></span>
                        </button>
                        <?php if ($Data->status == 1) { ?>
                        <button class="p-dots-area_1  hide_<?php echo $single->sf_user_id;?>">
                        <a class="dropdown-toggle" id="dropdownMenuButton_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-h"></i>
                        </a>
                        <div class="dropdown-menu dropdownMenuButton_1" aria-labelledby="dropdownMenuButton_1">
                           <?php if( $button_text=='Connected' ){ ?> 
                             <a data-id="<?php echo $single->sf_user_id;?>" class="unconnect dropdown-item" href="#" >Unconnected</a>
                           <?php } ?>
                        </div>
                      </button>
                      <?php } ?>
                    </div>
                    <?php }}} ?>
                </div>
            </div>
            <div class="col-md-2">
            </div> 
        </div>

        <?php  } ?>
        <?php
        if (empty($get_followers)) { ?>
            <div class="timeline_no_followers">This person does not <br>has any followers</div>
        <?php  } ?>
    </div>
</div>


<style type="text/css">
    .button_con_position{
        width: 26.5% !important;
    }
    .following_color {
        background-color: #dad8d8 !important;
        color:#000 !important;
        font-weight:bold;
    }
     .timeline_no_followers{
        text-align: center;
        font-size: 40px;
        color: grey;
        margin-top: 50px;
    }
    .follower_header_txt h2 {
        color: #1A6D84;
        font-size: 30px;
        font-weight: 500;
        line-height: 1.1;
        margin-top: 20px;
        text-align: center;
        margin-bottom: 20px;
    }
    .follower_area{
        box-shadow: 0 0px 5px 0px #afacac;
        padding-top: 15px;
        padding-bottom: 15px;
    }
    .follower_con_area{
        width: 100%;
    }
    .follower_con_area_img{
        width: 33%;
        display: inline-block;
        vertical-align: middle;
    }
    .follower_con_area_img a img{
        border-radius: 50%;
        width: 190px;
        height: 190px;
    }
    .follower_con_area_text{
        width: 31%;
        display: inline-block;
        vertical-align: middle;
    }
    .follower_con_area_text span{
        font-size: 20px;
        text-transform: capitalize;
    }
    .follower_follow_button{
        width: 16%;
        display: inline-block;
        vertical-align: middle;
        text-align: end;
    }
    .follower_connect_button{
       width: 17%;
        display: inline-block;
        vertical-align: middle;
        text-align: end;
    }
    .btn_follow {
        width: 120px;
        background: #008a66;
        padding: 10px 10px;
        border: 0px;
        letter-spacing: 1px;
        border-radius: 5px;
        color: #fff;
        font-size: 13px;
        font-family: "Roboto", Sans-serif;
        cursor: pointer;
    }
    .btn_follow i.fa-wifi {
        transform: rotate(55deg);
        position: relative;
        right: 3px;
        top: 0px;
    }
    .follower_con_btn {
        width: 120px;
        background: #008a66;
        padding: 8px 10px;
        border: 0px;
        letter-spacing: 1px;
        border-radius: 5px;
        color: #fff;
        line-height: 15px;
        font-size: 13px;
        font-family: "Roboto", Sans-serif;
        cursor: pointer;
    }

    .follower_con_btn i {
        font-size: 13px !important;
        padding-right: 3px;
        margin-right: 0px !important;
        padding-left: 0px !important;
    }
    .connect_button{
        padding: 12px 10px;
    }
    .requested_to_connect{
        background: #dad8d8 !important;
        color: #000 !important;
        font-weight: bold;
        line-height: 10px;
    }
    .requested_to_connect span i{
        top: 5px;
        position: relative;
        left: -3px;
    }
    .requested_to_connect span a{
        color: #000 !important;
        text-decoration: none;
    }
    .requested_to_connect span{
        margin-right: -22px;
    }
    .connect_button_sent {
        background: #dad8d8 !important;
        color: #000 !important;
        font-weight: bold;
        line-height: 10px;
        padding: 8px 10px;
    }
    .connect_button_sent span i {
        position: relative;
        top: 5px;
        right: 5px;
    }
    .dropdownMenuButton_1{
      right: 0px  !important;
      min-width: 35% !important;
      float: none !important;
      left: auto !important;
    }
    .connect_button_connected span a{
         color: #000 !important;
        text-decoration: none;
    }
    .connect_button_connected{
        border-radius: 5px 0px 0px 5px !important;
        background: #cacaca !important;
        color: #000 !important;
        position: relative;
        padding: 12px 10px;
    }
    .connect_button_connected .p-connect-area-text{
         color: #000 !important;
    }
    .greencolor{
         background: #008a66!important;
    }
    .p-dots-area_1 {
        background: #bab1b1;
        padding: 7px 7px;
        outline: none !important;
        border-radius: 0px 5px 5px 0px;
        font-size: 15px;
        position: absolute;
        border: 1.5px solid #bab1b1;
    }
    .dest_follower{
        width: 33%;
        display: inline-block;
        position: relative;
        text-align: right;
    }
    .dest_follower i{
        font-size: 36px;
        text-align: center;
    }
    .dest_follower i.fas.fa-wifi{
        transform: rotate(55deg);
        position: relative;
        top: -11px;
        right: 16px;
    }
    .dest_follower span{
        font-size: 20px;
        color: #999999;
        position: relative;
        top: 30px;
        right: 70px;
    }
    .follower_follow_button button:focus{
        outline: none;
    }
    .follower_connect_button button:focus{
       outline: none; 
    } 

    @media (max-width: 600px){
        .btn_follow_pos_mob{
            right: -97px !important;
        }
        .btn_follow{
            position: absolute;
            right: -82px;
        }
        .connect_button_connected{
            position: absolute;
            padding: 12px 10px;
            right: -61px;
        }
        .follower_con_area_img{
            width: 29%;
        }
        .follower_con_btn{
            position: absolute;
            right: -78px;
        }
        .follower_follow_button{
            position: absolute;
            top:20px;
        }
        .follower_connect_button{
            position: absolute;
            top:65px;
            margin-left: 0px !important;
        }
        .p-dots-area_1 {
            right: -94px;
        }
        .timeline_no_followers{
            font-size: 35px;
        }
        .follower_area{
            padding-left: 0px;
            padding-right: 0px;
            box-shadow: none;
        }
        .follower_con_area_img a img{
            width: 90px;
            height: 90px;
        }
        .follower_con_area_text span{
            font-size: 15px;
        }
        .follower_header_txt h2{
            margin-top: 100px;
        }
        .dest_follower{
            width: 32%;
        }
        .dest_follower i{
            font-size: 30px;
        }
        .dest_follower i.fas.fa-wifi{
            top: -7px;
            right: 12px;
        }
        .dest_follower span {
            right: 63px;
        }
        body{
            overflow-x: hidden;
        }
    }

</style>
<style type="text/css">
    #footer {
        display: none;
    }
</style>

<?php
get_footer();
?>