<?php 
session_start();
set_time_limit(0);
require_once('../../../../wp-config.php');
require_once('../../../../wp-load.php');
global $wpdb;
// ini_set("display_errors", 1);
// error_reporting(E_ALL);

define('EMAIL_ADD', 'tourfrombali@gmail.com'); 			// define any notification email
define('PAYPAL_EMAIL_ADD', 'tourfrombali@gmail.com');

 		// facilitator email which will receive payments change this email to a live paypal account id when the site goes live
require_once("paypal_class.php");
$p 				= new paypal_class(); 									// paypal class
$p->admin_mail 	= EMAIL_ADD; 											// set notification email
$action 		= $_REQUEST["action"];

switch($action){	
	case "process": // case process insert the form data in DB and process to the paypal

	$wpdb->query("INSERT INTO `purchases` (`id`, `invoice`, `product_id`, `product_name`, 
														`product_quantity`, `product_amount`, `payer_fname`,
														`payer_lname`, `payer_address`, `payer_city`, 
														`payer_state`, `payer_zip`, `payer_country`,
														`payer_email`, `payment_status`, `posted_date`) VALUES
	 ('NULL','".$_POST["invoice"]."', '".$_POST["product_id"]."', '".$_POST["productname"]."',
	'".$_POST["requestmount"]."', '".$_POST["product_price"]."', 
	'".$_POST["payer_fname"]."', '".$_POST["payer_lname"]."', '".$_POST["payer_address"]."', '".$_POST["payer_city"]."', 
	'".$_POST["payer_state"]."', '".$_POST["payer_zip"]."', '".$_POST["payer_country"]."', '".$_POST['payer_email']."',
	'pending', NOW())");		

$this_script = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

		$tour_id=intval($_POST["product_id"]);
		$p->add_field('business', PAYPAL_EMAIL_ADD); // Call the facilitator eaccount
		$p->add_field('cmd', '_cart'); // cmd should be _cart for cart checkout
		$p->add_field('upload', '1');
		$p->add_field('return', $this_script.'?action=success&product_id='.intval($_POST["product_id"])); // return URL after the transaction got over
		$p->add_field('cancel_return', $this_script.'?action=cancel'); // cancel URL if the trasaction was cancelled during half of the transaction
		$p->add_field('notify_url', $this_script.'?action=ipn'); // Notify URL which received IPN (Instant Payment Notification)

		if ($wpdb->get_var("SELECT confirm_payment FROM wp_tour WHERE id='{$tour_id}'") == 0) {
			if (is_user_logged_in()) {
				$buyer = wp_get_current_user()->ID;
				$wpdb->query("UPDATE `wp_tour` SET `buyer`={$buyer} WHERE `wp_tour`.`id` = '{$tour_id}'");
			} else {
				$wpdb->query("UPDATE `wp_tour` SET `buyer`=0 WHERE `wp_tour`.`id` = '{$tour_id}'");
			}
		}
		
		$p->add_field('currency_code',  $_POST["currency_code"] );
		$p->add_field('invoice', $_POST["invoice"]);
		$p->add_field('item_name_1', $_POST["productname"]);
		$p->add_field('item_number_1',$_POST["product_id"]);
		$p->add_field('quantity_1', $_POST["requestmount"]);
		$p->add_field('amount_1', $_POST["product_price"]);
		$p->add_field('first_name', $_POST["payer_fname"]);
		$p->add_field('last_name', $_POST["payer_lname"]);
		$p->add_field('country', $_POST["payer_country"]);
		$p->add_field('zip', $_POST["payer_zip"]);
		$p->add_field('email', $_POST['payer_email']); 
		$p->submit_paypal_post(); // POST it to paypal
		//$p->dump_fields(); // Show the posted values for a reference, comment this line before app goes live

			foreach($_POST as $index=>$value ) 
			$_SESSION[$index]=$_POST[$index];

		include ('sendconfirmationmail.php');

		include ('update_tour.php');

	break;
	
	case "success": // success case to show the user payment got success
	
		//include ('sendconfirmationmail.php');

		//include ('update_tour.php');
		
		$url=home_url().'/thank-you/?tour='.intval($_GET['product_id']);
		exit(wp_redirect($url));

	break;
	
	case "cancel": // case cancel to show user the transaction was cancelled
		echo "<h1>Transaction Cancelled";
	break;
	
	case "ipn": // IPN case to receive payment information. this case will not displayed in browser. This is server to server communication. PayPal will send the transactions each and every details to this case in secured POST menthod by server to server. 

		$trasaction_id  = $_POST["txn_id"];
		$payment_status = strtolower($_POST["payment_status"]);
		$invoice		= $_POST["invoice"];
		$log_array		= print_r($_POST, TRUE);
		$log_query		= "SELECT * FROM `paypal_log` WHERE `txn_id` = '$trasaction_id'";
		$tour_id		= $_POST["item_number1"];
		$sent_email_status = FALSE;
		
		$log_check 		= $wpdb->get_results($log_query);
		if( $wpdb->num_rows<= 0){
			$wpdb->query("INSERT INTO `paypal_log` (`txn_id`, `log`, `posted_date`) VALUES ('$trasaction_id', '$log_array', NOW())");
			$sent_email_status=TRUE;
		}else{
			$wpdb->query("UPDATE `paypal_log` SET `log` = '$log_array' WHERE `txn_id` = '$trasaction_id'");
		} // Save and update the logs array
		//$paypal_log_fetch 	= mysql_fetch_array($wpdb->get_results($log_query));

		if ($sent_email_status && $p->validate_ipn()){
			//confirm payment
			$wpdb->query("UPDATE `wp_tour` SET `confirm_payment` = '1' WHERE `wp_tour`.`id` = '{$tour_id}'");
			//set coupon
			$wpdb->query("UPDATE `wp_coupons` SET used=1 WHERE tour_id={$tour_id}");
			//send notification to creator
			$tour_creator_id=$wpdb->get_var("SELECT meta_value FROM `wp_tourmeta` where meta_key='userid' and  tour_id=".intval($tour_id));
			if(!empty($tour_creator_id) && intval($tour_creator_id)>0) {
				$wpdb->insert('notification', array(
					'wp_user_id'=>$tour_creator_id,
					'content'=>'Congratulation! Some one just bought your booking code'),
					array('%d','%s'));
			}
			//send email to third party vendors 
			$query = "SELECT option_value FROM `travpart.wp_options` WHERE `option_name` = '_cs_travpart_vendorlist' ";
					global $wpdb;
					$resultsData = $wpdb->get_results($query);
							if(!empty($resultsData)){
								foreach ( $resultsData as $data ) {
									$arr = unserialize(unserialize($data->option_value)); 
										foreach ($arr as $res) {
											$vendor_Email = $res['email'];

											$htmlContentpath = get_theme_file_path("emailtemplateforvendor.html");
											$htmlContent = file_get_contents($htmlContentpath);
											
											sendmail($vendor_Email, 'Bidding Mail', $htmlContent);

										}
								}
								// sendmail($admin_email, 'Bidding Mail - BALI'.$tourid, $email_content);
							}
			}
		
		foreach( $wpdb->get_results($log_query) as $paypal_log_fetch ) {
			$paypal_log_id		= $paypal_log_fetch->id;
			if ($p->validate_ipn()){ // validate the IPN, do the others stuffs here as per your app logic
				$wpdb->query("UPDATE `purchases` SET `trasaction_id` = '$trasaction_id ', `log_id` = '$paypal_log_id', `payment_status` = '$payment_status' WHERE `invoice` = '$invoice'");
				$subject = dirname(__FILE__) . 'Instant Payment Notification - Recieved Payment';
				$p->send_report($subject); // Send the notification about the transaction
			}else{
				$subject = dirname(__FILE__). 'Instant Payment Notification - Payment Fail';
				$p->send_report($subject); // failed notification
			}
		}
		
	break;
}
?>