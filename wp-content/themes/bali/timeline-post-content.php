<?php

if (empty($timeLinePosts))
    exit();


foreach ($timeLinePosts as $item) {
    if ($item->post_type == 'blog') {
        $like_count = $wpdb->get_var("SELECT COUNT(*) FROM `blog_social_connect` WHERE `post_id` = '{$item->ID}' AND `sc_type` = 0");
        $liked = $wpdb->get_var("SELECT COUNT(*) FROM `blog_social_connect` WHERE `post_id` = '{$item->ID}' AND `sc_type` = 0 AND `sc_user_id`={$current_user->ID}");
?>

        <div class="col-md-4 post-md-4 box">

            <div class="post-area-content">
                <div class="post-content">

                    <div class="post-img-top">
                        <img class="blog-brand" src="<?php echo home_url('wp-content/uploads/2020/02/travpart-logo.png'); ?>" alt="Travpart">

                        <div class="share-post">
                            <a target="_blank" class="a2a_dd addtoany_share_save addtoany_share" href="https://www.addtoany.com/share#url=https%3A%2F%2Fwww.travpart.com%2FEnglish%2Fbali-tour-packages-5-days-and-4-nights-tours-itinerary%2F&amp;title=Bali%20Tour%20Packages%205%20Days%20and%204%20Nights%20%7C%20Travpart%20%E2%80%93%20world%20class%20premium%20travel">
                                <picture>
                                    <source type="image/webp" srcset="https://www.travpart.com/English/wp-content/uploads/2019/10/share2.png.webp">
                                    <picture>
                                        <source type="image/webp" srcset="https://www.travpart.com/English/wp-content/uploads/2019/10/share2.png.webp">
                                        <img class="share-img" src="https://www.travpart.com/English/wp-content/uploads/2019/10/share2.png" alt="Share">
                                    </picture>

                            </a>
                        </div>
                    </div>

                    <div class="post-image">
                        <?php
                        $post_image = get_template_directory_uri() . '/assets/img/rome.jpg';
                        if ($thum_url = get_the_post_thumbnail_url($item->ID)) {
                            $post_image = $thum_url;
                        } else if (preg_match('!<img.*?src="(.*?)"!', $item->post_content, $post_image_match)) {
                            $post_image = $post_image_match[1];
                        }
                        ?>
                        <a href="#">
                            <img src="<?php echo $post_image; ?>">
                        </a>
                    </div>

                    <div class="post-category">
                        <a href="#">Event / MICE</a>
                    </div>

                    <h2 class="post-title">
                        <a href="<?php echo get_permalink($item->ID); ?>"><?php echo $item->post_title; ?></a>
                    </h2>

                    <div class="post-date">
                        <span>By </span>&nbsp;/&nbsp;<?php echo date('F j, Y', strtotime($item->post_date)); ?>
                    </div>

                    <div class="post-content-txt">

                        <div>
                            <?php echo mb_strimwidth(strip_tags($item->post_content), 0, 230, "..."); ?>
                        </div>

                        <a class="readmorebtn" href="<?php echo get_permalink($item->ID); ?>">Read More</a>
                    </div>
                    <div class="buttons_for_mobilepost">
                        <div class="main_buttons_div">
                            <div class="like_mobile">
                                <a href="#" blogid="<?php echo $item->ID; ?>">
                                    <i class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>
                                    <span><?php echo $like_count; ?></span>
                                </a>
                            </div>
                            <div class="comment_mobile">
                                <a href="#">
                                    <i class="far fa-comment-alt"></i>
                                    Comment
                                </a>
                            </div>
                            <div class="share_mobile">
                                <a href="#" onclick="share_feed(<?php echo $item->ID; ?>)">
                                    <i class="fas fa-share"></i> Share
                                </a>
                            </div>
                        </div>
                        <div class="comment_main_div show_comment_<?php echo $item->ID; ?>">
                        <?php
                        $comments = $wpdb->get_results("SELECT scm_text comment,scm_user_id user_id,user_login FROM `blog_social_comments`,`wp_users` WHERE wp_users.ID=scm_user_id AND `post_id` = '{$item->ID}' ORDER BY scm_created DESC");
                        if (!empty($comments)) {
                            foreach ($comments as $comment) {
                        ?>
                                <div class="user-comment-area">
                                    <?php echo get_avatar($comment->user_id, 50);  ?>
                                    <div class="another-user-section">
                                        <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $comment->user_login; ?>"><?php echo um_get_display_name($comment->user_id); ?></a>
                                        <span><?php echo $comment->comment;  ?></span>
                                    </div>
                                </div>
                        <?php }
                        } ?>
                     </div>
                        <div class="user-comment-area submit-comment">
                            <?php echo get_avatar($current_user->ID, 50);  ?>
                            <input placeholder="Write a Comment" type="text" name="feed_comment" required="">
                            <input type="hidden" name="blog_id" value="<?php echo $item->ID; ?>">
                            <input type="hidden" name="loginuser_image" value="<?php echo get_avatar_url (wp_get_current_user()->ID)?>">
                                <input type="hidden" name="loginuser_name" value="<?php echo um_get_display_name (wp_get_current_user()->ID )?>">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } else if ($item->post_type == 'shortpost') {
        $liked = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE user_id = '{$current_user->ID}' AND feed_id = '$item->ID'");
        $like_count = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE feed_id = '$item->ID'");
        $lookingfriend = get_post_meta($item->ID, 'lookingfriend', true);
        switch ($lookingfriend) {
            case 1:
                $lookingfriend_value = 'Looking For A Travel Friend';
                break;
            case 2:
                $lookingfriend_value = 'Want to Hangout';
                break;
            case 3:
                $lookingfriend_value = 'Going for Party';
                break;
            case 4:
                $lookingfriend_value = 'Going for Sport';
                break;
            default:
                $lookingfriend_value = '';
        }
    ?>

        <div class="col-md-4 post-md-4 box">
            <div class="post-area-content">
                <div class="post-content">
                    <div class="post_header">
                        <div class="post_profile_img">
                            <div class="for_img">
                                <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>">
                                    <img alt="" src="<?php echo um_get_avatar_url(um_get_avatar('', $item->post_author, 52)); ?>">
                                </a>
                            </div>
                        </div>
                        <?php
                        $display_name = um_user('display_name'); ?>
                        <div class="header_user_content">
                            <div class="content_mobile">
                                <h4 class="mobile_agent_name" style="margin-bottom: 20px;margin-top: 5px;">
                                    <a class="title_post_username" href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>"><?php echo $display_name; ?> </a>
                                    
                                    <span class="date_pub_mobile">
                                        <p style="margin: 0px; font-size: 10px">
                                            <?php echo human_time_diff(strtotime($item->post_date), current_time('timestamp')); ?>
                                        </p>
                                    </span>
                                    <?php if (!empty(get_post_meta($item->ID, 'feeling', true))) { ?>
                                        <span class="short-post-feeling">
                                            <?php echo ' is feeling ' . base64_decode(get_post_meta($item->ID, 'feeling', true)); ?>
                                        </span>
                                    <?php } else if (!empty(get_post_meta($item->ID, 'tag_user_id', true))) { ?>
                                        <span class="short-post-tagfriend">
                                            <?php echo ' is with ' . um_get_display_name(get_post_meta($item->ID, 'tag_user_id', true)); ?>
                                        </span>
                                    <?php } ?>
                                    <?php if ($lookingfriend >= 1) { ?>
                                        <!--<p class="changes_for_mob button_green" style="background: #22B14C;color: #fff;padding: 5px 10px;border-radius: 3px;position: absolute;">Looking For A Travel Friend</p>-->
                                        <a class="changes_for_mob  button_green"><?php echo $lookingfriend_value; ?></a>
                                    <?php } ?>
                                </h4>
                                <p class="mobiel_feeling"></p>

                                <!-- Right dot dropdown -->
                                <div class="right-section-dot dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <a href="#" class="Pdeltebtn"><i class="fas fa-ellipsis-h"></i></a>
                                </div>
                                <!-- Right dot dropdown -->
                                <?php
                                $post_edit_text = preg_replace('/[^A-Za-z0-9 ]/', '', $item->post_content);
                                ?>
                                <!-- Post Delete Option PC-->
                                <div id="PostDId" class="PostDelete dropdown-menu" role="menu" aria-labelledby="menu1">
                                    <?php if (is_user_logged_in()) {
                                        $author_id = get_post_field('post_author', $item->ID);
                                        if ($author_id == get_current_user_id()) { ?>
                                            <li class="drp-edit edit-post"
                                                postid = "<?php echo $item->ID ?>"
                                                feeling="<?php echo base64_decode(get_post_meta($item->ID, 'feeling', true)) ?>"
                                                content="<?php echo $item->post_content ?>"
                                                taguserid="<?php echo get_post_meta($item->ID, 'tag_user_id', true) ?>"
                                                taguser="<?php echo um_get_display_name(get_post_meta($item->ID, 'tag_user_id', true)) ?>"
                                                tour="<?php echo get_post_meta($item->ID, 'tour_id', true) ?>"
                                                >
                                                <a href="#">Edit Post</a>
                                            </li>
                                            <li class="drp-delete" postid="<?php echo $item->ID; ?>"><a href="#">Delete</a></li>
                                    <?php
                                        }
                                    } ?>
                                    <li><a href="#">Turn On notifications For this post</a></li>
                                    <li class="ShowITc"><a href="#">Show in tab</a></li>
                                    <li class="hide-from-timeline" postid="<?php echo $item->ID ?>"><a href="#">Hide From timeline</a></li>
                                    <li><a href="#" onclick="copy_link('<?php  echo $item->ID; ?>')">Copy link to this post</a></li>
                                     <li><a id="copytext<?php echo $item->ID;  ?>" style="display: none"  href="<?php echo get_permalink($item->ID) ?>"></a></li>

                                </div>
                                <!--End Post Delete Option -->



                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <?php
                    $attachment = get_post_thumbnail_id($item->ID);
                    $attachment_url = '';
                    if (empty($attachment)) {
                        $attachment = get_post_meta($item->ID, 'attachment', true);
                    }
                    if (!empty($attachment)) {
                        if (wp_attachment_is('image', $attachment)) {
                            $attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
                        } else {
                            $attachment_url = wp_get_attachment_url($attachment);
                        }
                    } else if (!empty(get_post_meta($item->ID, 'tour_img', true))) {
                        $attachment_url = get_post_meta($item->ID, 'tour_img', true);
                    }
                    $tour_post_id = get_post_meta($item->ID, 'tour_post', true);
                    if (!empty($attachment_url)) {
                    ?>
                        <div class="post-image">
                            <?php if (!is_array(get_post_meta($item->ID, 'attachment'))) { ?>
                                <?php if (empty($attachment) || wp_attachment_is('image', $attachment)) { ?>
                                    <a href="<?php echo !empty($tour_post_id) ? get_permalink($tour_post_url) : '#'; ?>">
                                        <img src="<?php echo $attachment_url; ?>">
                                    </a>
                                <?php } else {
                                    $video_thumbnail_url = get_post_meta($attachment, 'kgvid-poster-url', true) ?>
                                    <!--<video poster="<?php //echo $video_thumbnail_url ?>"
                                        src="<?php //echo $attachment_url; ?>" controls="controls"></video>-->
                                        <video
                                     preload="auto"
                                     controls
                                     poster="<?php echo $video_thumbnail_url ?>"
                                 >
                                 <source src="<?php echo $attachment_url; ?>" type="video/mp4"></source>
                                 <source src="<?php echo $attachment_url; ?>" type="video/webm"></source>
                                 <source src="<?php echo $attachment_url; ?>" type="video/ogg"></source>
                                 <p class="vjs-no-js">
                                 To view this video please consider upgrading to a web browser that
                                 <a href="http://videojs.com/html5-video-support/" target="_blank">
                                 supports HTML5 video
                                 </a>
                                 </p>
                                </video>
                                <?php } ?>
                                <?php } else {
                                foreach (get_post_meta($item->ID, 'attachment') as $row) {
                                    $attachment_url = wp_get_attachment_url($row);
                                ?>
                                    <?php if (wp_attachment_is('image', $row)) { ?>
                                        <div class="feed_post_image_div">
                                            <img src="<?php echo $attachment_url; ?>">
                                            <a href="<?php echo $attachment_url; ?>" data-fancybox="gallery"></a>
                                        </div>
                                    <?php } else {
                                        $video_thumbnail_url = get_post_meta($row, 'kgvid-poster-url', true) ?>
                                        <!--<video poster="<?php //echo $video_thumbnail_url ?>"
                                            src="<?php //echo $attachment_url; ?>" controls="controls"></video>-->
                                             <video id="feed_video_autoplay"
                                     preload="auto" 
                                     controls
                                     poster="<?php echo $video_thumbnail_url ?>"
                                 >
                                 <source src="<?php echo $attachment_url; ?>" type="video/mp4"></source>
                                 <source src="<?php echo $attachment_url; ?>" type="video/webm"></source>
                                 <source src="<?php echo $attachment_url; ?>" type="video/ogg"></source>
                                 <p class="vjs-no-js">
                                 To view this video please consider upgrading to a web browser that
                                 <a href="http://videojs.com/html5-video-support/" target="_blank">
                                 supports HTML5 video
                                 </a> 
                                 </p>
                                </video>
                                    <?php } ?>
                            <?php
                                }
                            } ?>
                        </div>
                    <?php } ?>

                    <?php if (!empty(get_post_meta($item->ID, 'location', true))) { ?>
                        <div class="post-category">
                            <a href="#"><?php echo get_post_meta($item->ID, 'location', true); ?></a>
                        </div>
                    <?php } ?>

                    <div class="post-content-txt">
                        <div class="more">
                            <?php echo strip_tags($item->post_content); ?>
                        </div>
                    </div>

                    <?php if (!empty(get_post_meta($item->ID, 'tour_post', true))) { ?>
                        <div class="post-category">
                            <a href="<?php echo get_permalink(get_post_meta($item->ID, 'tour_post', true)); ?>">
                                <?php echo get_post_meta($item->ID, 'tour_title', true); ?>
                            </a>
                        </div>
                        <br>
                    <?php } ?>
                    <div class="buttons_for_mobilepost">
                        <hr class="style-mobile-six" />
                        <div class="main_buttons_div">
                            <div class="like_mobile">
                                <a href="#" feedid="<?php echo $item->ID; ?>">
                                    <i class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>
                                    <span><?php echo $like_count; ?></span>
                                </a>
                            </div>
                            <div class="comment_mobile" data-comment="<?php echo $item->ID; ?>">
                                <a href="#">
                                    <i class="far fa-comment-alt"></i>
                                    Comment
                                </a>
                            </div>
                            <div class="share_mobile">
                                <!-- pc view -->
                                <!-- <a href="#" onclick="share_feed(<?php //echo $item->ID; 
                                                                        ?>)">
                                                    <i class="fas fa-share"></i> Share </a> -->

                                <a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
                                    <i class="fas fa-share"></i> Share</a>


                                <!--Share My DropDown PC-->
                                <div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2" aria-labelledby="menu2" style="right: 40px;left: inherit;">
                                    <li onclick="share_feed(<?php echo $item->ID; ?>)">Share To Feed</li>
                                    

                                     <li><a target="_blank" onclick="record_share_data('<?php echo $item->ID; ?>','whatsapp')" href="https://web.whatsapp.com/send?text=<?php echo substr($item->post_content, 0, 80) . "  ( see-more ) "; echo get_permalink($item->ID) ?>"><div class="wh_feed_icon_div"><i class="fab fa-whatsapp share_wh_feed_i"></i></div>Share to whats app</a></li>
                                     <li><a  href="#" onclick="copy_link('<?php echo $p->ID; ?>')"><i class="far fa-copy share_copy_link_feed_i"></i>Copy link to this post</a></li>

                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="<?php echo get_permalink($item->ID) ?>" data-a2a-title="<?php echo substr($item->post_content, 0, 80);  ?>">
    <a class="a2a_button_facebook" onclick="record_share_data('<?php echo $item->ID; ?>','fb')"></a>
    <a class="a2a_button_twitter" onclick="record_share_data('<?php echo $item->ID; ?>','twitter')"></a>
    <a class="a2a_dd" href="https://www.addtoany.com/share" onclick="record_share_data('<?php echo $item->ID; ?>','other')"></a>
</div>

<script async src="https://static.addtoany.com/menu/page.js"></script>

                                </div>

                                <!--End My DropDown-->


                                <!-- End Pc view -->
                            </div>
                        </div>
                        <div class="comment_main_div show_comment_<?php echo $item->ID; ?>">
                        <?php
                        $comments = $wpdb->get_results("SELECT comment,user_id,user_login FROM `user_feed_comments`,`wp_users` WHERE wp_users.ID=user_id AND `feed_id` = '{$item->ID}' ORDER BY user_feed_comments.id DESC");
                        if (!empty($comments)) {
                            foreach ($comments as $comment) {
                        ?>
                                <div class="user-comment-area">
                                    <?php echo get_avatar($comment->user_id, 50);  ?>
                                    <div class="another-user-section">
                                        <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $comment->user_login; ?>"><?php echo um_get_display_name($comment->user_id); ?></a>
                                        <span><?php echo $comment->comment;  ?></span>
                                    </div>
                                </div>
                        <?php }
                        } ?>
                        </div>
                        <div class=" user-comment-area submit-comment">
                            <?php echo get_avatar($current_user->ID, 50);  ?>
                            <input placeholder="Write a Comment" type="text" name="feed_comment" required="">
                            <input type="hidden" name="feed_id" value="<?php echo $item->ID; ?>">
                            <input type="hidden" name="loginuser_image" value="<?php echo get_avatar_url (wp_get_current_user()->ID)?>">
                                <input type="hidden" name="loginuser_name" value="<?php echo um_get_display_name (wp_get_current_user()->ID )?>">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    <?php } else if ($item->post_type == 'eventpost') {
        if (get_post_meta($item->ID, 'event_type', true) == 'private') {
            $event_invited_people = get_post_meta($item->ID, 'event_invited_people', true);
            if ($current_user->ID != $item->post_author && !in_array($current_user->ID, $event_invited_people)) {
                continue;
            }
        }
        $event_name = get_post_meta($item->ID, 'event_name', true);
        $event_location = get_post_meta($item->ID, 'event_location', true);
        $attachment_url = get_template_directory_uri() . '/assets/img/rome.jpg';
        $attachment = get_post_meta($item->ID, 'event_location_image_id', true);
        if (!empty($attachment)) {
            $attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
        }
        $start_date = get_post_meta($item->ID, 'start_date', true);
        $event_tour_package = get_post_meta($item->ID, 'event_tour_package', true);
        $tour_img = get_post_meta($item->ID, 'tour_img', true);
        if (empty($tour_img)) {
            $tour_img = 'https://i.travelapi.com/hotels/25000000/24160000/24157900/24157865/e01875da_y.jpg';
        }
        $liked = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE user_id = '{$current_user->ID}' AND feed_id = '$item->ID'");
        $like_count = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE feed_id = '{$item->ID}'");
        $going_count = $wpdb->get_var("SELECT COUNT(*) FROM `event_going` WHERE post_id = '{$item->ID}' AND status=1");
        $going_status = $wpdb->get_var("SELECT status FROM `event_going` WHERE post_id='{$item->ID}' AND user_id='{$current_user->ID}'");
    ?>
        <div class="col-md-4 post-md-4 box">
            <div class="post-area-content">
                <div class="post-content">
                    <div class="post_header">
                        <div class="post_profile_img">
                            <div class="for_img">
                                <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>">
                                    <img alt="" src="<?php echo um_get_avatar_url(um_get_avatar('', $item->post_author, 52)); ?>">
                                </a>
                            </div>
                        </div>
                        <?php
                        $display_name = um_user('display_name'); ?>
                        <div class="header_user_content">
                            <div class="content_mobile">
                                <h4 class="mobile_agent_name" style="margin-bottom: 20px;margin-top: 5px;">
                                    <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>"><?php echo $display_name; ?> </a>
                                    <span class="date_pub_mobile">
                                        <p style="margin: 0px; font-size: 10px">
                                            <?php echo human_time_diff(strtotime($item->post_date), current_time('timestamp')); ?>
                                        </p>
                                    </span>
                                </h4>

                                <div class="right-section-dot dropdown-toggle text-right" data-toggle="dropdown" aria-expanded="false">
                                    <a href="#" class="Pdeltebtn"><i class="fas fa-ellipsis-h"></i></a>
                                    <br>
                                    <p class="changes_for_mob button_green_event">Created an Event</p>
                                    <!--<button style="padding: 0px 10px;line-height: 12px;" class="btn btn-success">Created an <br> Event</button>-->
                                </div>

                                <!-- Post Delete Option PC-->
                                <div id="PostDId" class="PostDelete dropdown-menu" role="menu" aria-labelledby="menu1">
                                    <li><a href="#">Turn On notifications For this post</a></li>
                                    <li class="ShowITc"><a href="#">Show in tab</a></li>
                                    <li><a href="#">Hide From timeline</a></li>
                                    <li><a href="<?php echo get_permalink($item->ID) ?>">Copy link to this post</a></li>

                                </div>
                                <!--End Post Delete Option -->

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="post-image">
                        <img src="<?php echo $attachment_url; ?>">
                    </div>

                    <div class="post-content-txt">
                        <div class="event_date">
                            <div class="event_date text-center">
                                <h5 class="color_d_p"><?php echo date('M', $start_date); ?></h5>
                                <h5><?php echo date('d', $start_date); ?></h5>
                            </div>
                        </div>
                        <div class="event_details">
                            <div class="event_details_content">
                                <p class="title_fes"><?php echo $event_name; ?></p>
                                <p class="date_fes"><?php echo event_week_time_display($start_date); ?> - <?php echo $event_location; ?></p>
                                <p class="peoplegoing_fes"><?php echo number_format($going_count); ?> people going</p>
                            </div>
                        </div>
                        <div class="event_button">
                            <div class="event_inner_div">
                                <!-- Single button -->
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-check"></i> <span class="event-going-current-status"><?php echo $going_status == 1 ? 'Going' : 'Not Going'; ?></span> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu event-going" post_id="<?php echo $item->ID; ?>">
                                        <li status="1"><a href="#">Going</a></li>
                                        <li status="0"><a href="#">Not Going</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php if (!empty($event_tour_package)) { ?>
                            <div class="tour_details_evvent">
                                <div class="event_display_table_cell">
                                    <img src="<?php echo $tour_img; ?>">
                                </div>
                                <div class="event_display_image_cell">
                                    <p>BALI<?php echo $event_tour_package; ?></p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="buttons_for_mobilepost">
                        <hr class="style-mobile-six">
                        <div class="main_buttons_div">
                            <div class="like_mobile">
                                <a href="#" feedid="<?php echo $item->ID; ?>">
                                    <i class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>
                                    <span><?php echo $like_count; ?></span>
                                </a>
                            </div>
                            <div class="comment_mobile" data-comment="<?php echo $item->ID; ?>">
                                <a href="#">
                                    <i class="far fa-comment-alt"></i>
                                    Comment
                                </a>
                            </div>
                            <div class="share_mobile">
                                <a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
                                    <i class="fas fa-share"></i> Share
                                </a>
                                <div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2" aria-labelledby="menu2" style="right: 40px;left: inherit;">
                                    <li onclick="share_feed(<?php echo $item->ID; ?>)">Share To Feed</li>
                                   <li><a target="_blank" onclick="record_share_data('<?php echo $item->ID; ?>','whatsapp')" href="https://web.whatsapp.com/send?text=<?php  echo get_permalink($item->ID) ?>">Share to whats app</a></li>

                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="<?php echo get_permalink($item->ID) ?>" data-a2a-title="<?php echo substr($item->post_content, 0, 80);  ?>">
    <a class="a2a_button_facebook" onclick="record_share_data('<?php echo $item->ID; ?>','fb')"></a>
    <a class="a2a_button_twitter" onclick="record_share_data('<?php echo $item->ID; ?>','twitter')"></a>
    <a class="a2a_dd" href="https://www.addtoany.com/share" onclick="record_share_data('<?php echo $item->ID; ?>','other')"></a>
</div>

                                </div>
                            </div>
                        </div>
                        <div class="comment_main_div show_comment_<?php echo $item->ID; ?>">
                        <?php
                        $comments = $wpdb->get_results("SELECT comment,user_id,user_login FROM `user_feed_comments`,`wp_users` WHERE wp_users.ID=user_id AND `feed_id` = '{$item->ID}' ORDER BY user_feed_comments.id DESC");

                        
                        if (!empty($comments)) {
                            foreach ($comments as $comment) {
                        ?>
                                <div class="user-comment-area">
                                    <?php echo get_avatar($comment->user_id, 50);  ?>
                                    <div class="another-user-section">
                                        <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $comment->user_login; ?>"><?php echo um_get_display_name($comment->user_id); ?></a>
                                        <span><?php echo $comment->comment;  ?></span>
                                    </div>
                                </div>
                        <?php }
    
                        } ?>
                        </div>
                        <div class="user-comment-area submit-comment">
                            <?php echo get_avatar($current_user->ID, 50);  ?>
                            <input placeholder="Write a Comment" type="text" name="feed_comment" required="">
                            <input type="hidden" name="feed_id" value="<?php echo $item->ID; ?>">
                            <input type="hidden" name="loginuser_image" value="<?php echo get_avatar_url (wp_get_current_user()->ID)?>">
                             <input type="hidden" name="loginuser_name" value="<?php echo um_get_display_name (wp_get_current_user()->ID )?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vissense/0.10.0/vissense.min.js" integrity="sha512-2ES1bMYVtDkWzlzuUr6EkA0WZ4Vhg/cYBCBBlr0QtKAd12X9EWO1/JwId+9AVQnX7xu684o+7BElKL9HLd/YfQ==" crossorigin="anonymous"></script>
<script >
        function record_share_data(id,type){
        var post_id = id;
        var type=type;
        $.ajax({
        type: "POST",
        url: '<?php echo site_url() ?>/submit-ticket/',
        data: {
        feed_id: post_id,
        type:type,
        action: 'social_share_record'
        }, // serializes the form's elements.
        success: function(data) {
        }
        });
        }
    
    var videos = document.getElementsByTagName("video");

// function checkScroll() {
//     var fraction = 0.5; // Play when 80% of the player is visible.

//     for(var i = 0; i < videos.length; i++) {

//         var video = videos[i];

//         var x = video.offsetLeft, y = video.offsetTop, w = video.offsetWidth, h = video.offsetHeight, r = x + w, //right
//             b = y + h, //bottom
//             visibleX, visibleY, visible;

//             visibleX = Math.max(0, Math.min(w, window.pageXOffset + window.innerWidth - x, r - window.pageXOffset));
//             visibleY = Math.max(0, Math.min(h, window.pageYOffset + window.innerHeight - y, b - window.pageYOffset));

//             visible = visibleX * visibleY / (w * h);
//             console.log("Visible : ", visible);
//             if (visible > fraction) {
//                 video.play();
//             } else {
//                 video.pause();
//             }

//     }

// }


function checkScroll() {

    for(var i = 0; i < videos.length; i++) {
        var myVideo = videos[i];

        VisSense.VisMon.Builder(VisSense(myVideo, {
            fullyvisible: 0.75
        }))
        .on('fullyvisible', function() {
            myVideo.play();
        })
        .on('hidden', function() {
            myVideo.pause();
        })
        .build()
        .start();
    }

}



// $(window).scroll(function() {
//     console.log("Window scrolled");
//     $('video').each(function(){
//         if ($(this).is(":in-viewport")) {
//             console.log("Inside viewport");
//             $(this)[0].play();
//         } else {
//             console.log("outside viewport");
//             $(this)[0].pause();
//         }
//     })
// });

window.addEventListener('scroll', checkScroll, false);
window.addEventListener('resize', checkScroll, false);
        </script>
<style type="text/css">
    .feed_post_image_div{
        width: 90%;
        position: relative;
        height: 250px;
        display: inline-block;
    }
    .feed_post_image_div img{
        width: 100%;
        height: 245px;
    }
    .post-image img{
        object-fit: fill;
    }
    .wh_feed_icon_div{
        display: inline-block;
        background-color: green;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 5px;
    }
    i.fab.fa-whatsapp.share_wh_feed_i{
        color: white;
        font-size: 17px;
        position: relative;
        top: 4px;
        left: 6px;
    }
    i.far.fa-copy.share_copy_link_feed_i{
        position: relative;
        margin-right: 5px;
        top: 3px;
        font-size: 22px;
        margin-left: 3px;
        margin-bottom: 5px;
    }
</style>