<?php
/*
Template Name: Sent Request List
*/


get_header();
if (!is_user_logged_in()) {
    exit(wp_redirect(home_url('login')));
}

$current_user = wp_get_current_user();

$_requests_sent_count = $wpdb->get_var(
    "SELECT COUNT(DISTINCT user2) FROM friends_requests WHERE  user1 = '{$current_user->ID}' AND status=0" );

$_requests_sent_list = $wpdb->get_results(
    "SELECT DISTINCT user2 FROM friends_requests WHERE  user1 = '{$current_user->ID}' AND status=0" );

?>
<div class="sentlist_main_area">
    <div class="container new-short-post rem_pd">
        <div class="row">
            <div class="col-md-12">
                <div class="header_txt">
                    <h2>My Sent Request List</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">   
	            <div class="connectionlistwrap">
	                <div class="connectionlist">
            		 	<div class="connectionlistitem">
            		   	    <div class="connectionlistiteminner connectionlistbtninner clearfix">
                		   		<div class="conlistleftbtn"><p href="#">You have sent <span id='counter'><?php echo $_requests_sent_count; ?></span> connection requests</p></div>
                		   		<div class="conlistrightbtn"><a href="https://www.travpart.com/English/connection-request-list/">Check my received connection requests</a></div>
            		   	  </div>
            		    </div>
                        <?php foreach ($_requests_sent_list as $single) {

                         $author_obj = get_user_by('id', $single->user2);

                         ?>
            		    <div class="connectionlistitem"  id="remove_<?php  echo $single->user2  ?>">
                    
                        
                    
            		   	    <div class="connectionlistiteminner_content">
            		            <div class="connectionphoto">
                              <img alt="travpart" width="100px" height="100px" src="<?php echo um_get_avatar_url(um_get_avatar('', $single->user2)); ?>">      
                                </div>
        		                <div class="connectiondetail">
        		                    <div class="connectionname"><a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $author_obj->user_login; ?>"><?php  echo um_get_display_name($single->user2);  ?></a></div>     
        		                </div>
            		            <div class="connectionbtns">
                		            <button class="request_sent_button request_remove" id="<?php  echo $single->user2  ?>">
                                        <div class="col-md-33 rm_d"> 
                                            <i class="fas fa-user-minus" id="minus"></i> 
                                        </div> 
                                        <div class="col-md-99 rm_d">Request <br> sent</div>
                                    </button>
            		            </div>
            		        </div>
                        
            		    </div>
                        <?php } ?>
	                </div>
	            </div>
            </div>
        </div>
    </div>
</div>
<script>
    
$(document).ready(function () {

$(document).on("click",".request_remove",function(){
var thiss = $(this);
var user_id = thiss.attr('id');
var count = $("#counter").text(); 
count = parseInt(count);

var website_url ="https://www.travpart.com/English";
//var user_id = $('#userid').val();
    $.ajax({
    type: "POST",
    url: website_url + '/submit-ticket/',
    data: {
    friend_id: user_id,
    action: 'connection_request_cancel'
    }, 
    success: function(data) {
    var result = JSON.parse(data);
    if (result.status==1) {

     $("#remove_"+user_id).remove();
    $("#counter").html(count-1);
      } 
    else
    {
        alert(' Some error occured!');
    }
}

});
});
});
</script>
<style>
.clearfix:after {
  visibility: hidden;
  display: block;
  font-size: 0;
  content: " ";
  clear: both;
  height: 0;
}
.clearfix { display: inline-block; }	
.connectionlist {
    font-family: "Open Sans", Arial, Helvetica, sans-serif;
    font-size: 18px;
}	
.connectionphoto {
    display: inline-block;
    vertical-align: middle;
    width: 10%;
}
.connectiondetail {
    display: inline-block;
    vertical-align: middle;
    width: 60%;
}
.connectionbtns {
    display: inline-block;
    vertical-align: middle;
    width: 25%;
}
.connectionname {
    color: #385898;
    font-weight: bold;
    padding-top: 17px;
}	
.connectionlist {
    background-color: #fff;
    box-sizing: border-box;
    padding: 0;
    box-shadow: 0 0px 5px 0px #ccc;
}
.connectionlistitem {
    border-bottom: 1px solid #ccc;
    text-align: center;
    padding-bottom: 10px;
    padding-top: 16px;
}
.connectionlistiteminner {
	text-align: left;
}
.header_txt h2 {
    text-align: center;
    margin-bottom: 50px;
        color: #1A6D84;
    font-size: 30px;
    text-transform: uppercase;
}
.connectionlistbtninner{
	width: 87%;
}
.conlistleftbtn {
    float: left;
    
}
.conlistleftbtn p {
       background-color: #385898;
    color: #fff;
    padding: 8px 15px;
    border-radius: 4px;
}
.conlistrightbtn {
    float: right;
    width: 35%;
     border-radius: 4px;
     color: black;
}
.conlistrightbtn a {
    color: #000;
    border: 1px solid #000;
    font-size: 17px;
    padding: 4px 17px;
    box-sizing: border-box;
}
.request_sent_button{
    background: #dad8d8;
    color: #000;
    font-weight: bold;
    line-height: 10px;
    width: 130px;
    padding: 12px 10px;
    border: 0px;
    letter-spacing: 1px;
    border-radius: 5px;
    font-size: 13px;
    font-family: "Roboto", Sans-serif;
    cursor: pointer;
}
.rm_d {
    padding: 0px !important;
    display: inline-block;
    vertical-align: middle;
}
button.request_sent_button i {
    padding-right: 3px;
}
@media only screen and (max-width: 600px) {
.header_txt h2 {
    font-size: 17px;
}
.sentlist_main_area {
    padding-top: 55px;
}
.rem_pd {
    padding: 0px;
}
.connectionphoto img {
    width: 70px;
    height: 70px;
    border-radius: 100%;
}
.connectionphoto {
    width: 20%;
}
.connectiondetail {
    width: 32%;
}
.connectionname {
    font-size: 16px;
    padding-top: 0px;
}
.connectionbtns {
    width: 41%;
    text-align: end;
}
.connectionlistbtninner {
    width: 100%;
    text-align: center;
}
.conlistleftbtn {
    float: none;
    width: 100%;
}
.conlistrightbtn {
    float: none;
    width: 100%;
    margin-top: 12px;
}

.conlistrightbtn a {  
    font-size: 15px;
    padding: 4px 4px;
}
.conlistleftbtn a {
    padding: 8px 8px;
    font-size: 18px;
}


}


</style>
<style type="text/css">
    #footer {
        display: none;
    }
</style>
<?php
get_footer();
?>