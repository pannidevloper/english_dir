<style>

.game{
	width: 50%;
	margin: 0 auto;
}
.back_center {
    margin-top: 15px;
    margin-bottom: 15px;
    text-align: center;
}
.btn-primary-back {
    background: #ad650c;
    padding: 10px 20px !important;
    border: none;
    box-shadow: 0 1px 5px 0 #999;
    color: white;
    text-decoration: none;
    }
a.btn:hover{
    background-color: #195369 !important;
    color: #fff;
}
@media (max-width: 600px) {
  
  .btn-primary-back{
    border-radius: 4px;
  }
}

	/* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
  
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {
  
  
}

/* 
  ##Device = Tablets, Ipads (landscape)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
  
  
}

/* 
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {
  

.game{
	width: 100%;
	margin-top: 65px!important;
}

  canvas#canvas{
  	width: 100%;
  	height: 100%;
  }
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  

	.game{
		width: 100%;
		margin-top: 65px!important;
	}
    canvas#canvas{
  	width: 100%;
  	height: 100%;
  }
}
</style>
<?php
/* Template Name: Game page */

get_header();

$scheme=get_post_meta(intval($_GET['type']), '_tp_vsicon_args', true);
if(is_user_logged_in() && !empty($scheme) && !empty($scheme['icon'])) {
	$possibility=array(0,0,0,0,0,0,0,0,0,0,0,0);
	$availabilityCount=0;
	$percentage=array('$0','$0','$0','$0','$0','$0','$0','$0','$0','$0','$0','$0');
	for($i=0; !empty($scheme['icon'][$i]) && $i<12; $i++) {
		$args=explode('|', $scheme['icon'][$i]['args']);
		if(intval($args[3])>0) {
			$possibility[$i]=$args[2];
			$availabilityCount+=$args[2];
		}
		if($args[0]=='$')
			$percentage[$i]=$args[0].$args[1];
		else
			$percentage[$i]=$args[1].$args[0];
	}
	
	$current_user = wp_get_current_user();
	$user_points=intval(get_user_meta(wp_get_current_user()->ID, 'game_points', true));
	if($user_points<$scheme['pts_to_play']) {
?>
<p>You have not enough points to play the game.</p>
	<?php } else if($availabilityCount<=0) { ?>
<p>The game is not available now.</p>
	<?php } else { ?>
<script>
window.PROB = "<?php echo implode(',',$possibility); ?>";

var DisplayText = {
    play:"Play",
    stop:"Stop",
    flightgold:"Flight <?php echo $percentage[0]; ?> discount",
    flightsilver:"Flight <?php echo $percentage[1]; ?> discount",
    mealgold:"Meal <?php echo $percentage[2]; ?> discount",
    mealsilver:"Meal <?php echo $percentage[3]; ?> discount",
    vehiclegold:"Vehicle <?php echo $percentage[4]; ?> discount",
    vehiclesilver:"Vehicle <?php echo $percentage[5]; ?> discount",
    massagegold:"Massage <?php echo $percentage[6]; ?> discount",
    massagesilver:"Massage <?php echo $percentage[7]; ?> discount",
    helicoptergold:"Helicopter <?php echo $percentage[8]; ?> discount",
    helicoptersilver:"Helicopter <?php echo $percentage[9]; ?> discount",
    hotelgold:"Hotel <?php echo $percentage[10]; ?> discount",
    hotelsilver:"Hotel <?php echo $percentage[11]; ?> discount"
};

const manifest = [
    {id:"background", src:"<?php echo site_url(); ?>/game-source/img/background.jpg"},
    {id:"button", src:"<?php echo site_url(); ?>/game-source/img/button.png"},
    {id:"light",  src:"<?php echo site_url(); ?>/game-source/img/light.png"},
    {id:"target", src:"<?php echo site_url(); ?>/game-source/img/target.png"},
    {id:"box", src:"<?php echo site_url(); ?>/game-source/img/box.png"},
    /*{id:"bgm", src:"sounds/BGM.mp3"},
    {id:"tada", src:"sounds/tada.mp3"},
    {id:"click", src:"sounds/click.mp3"},
    {id:"lightsaber", src:"sounds/lightsaber.mp3"}*/
];

const medalImages = [
    {id:"gold", src:"<?php echo site_url(); ?>/game-source/img/medal gold.png"},
    {id:"silver", src:"<?php echo site_url(); ?>/game-source/img/medal silver.png"}
];

const icons = [
    {id:"flight", src:"<?php echo site_url(); ?>/game-source/img/icons/flight.png"},
    {id:"helicopter", src:"<?php echo site_url(); ?>/game-source/img/icons/helicopter.png"},
    {id:"hotel", src:"<?php echo site_url(); ?>/game-source/img/icons/hotel.png"},
    {id:"massage", src:"<?php echo site_url(); ?>/game-source/img/icons/massage.png"},
    {id:"meal", src:"<?php echo site_url(); ?>/game-source/img/icons/meal.png"},
    {id:"vehicle", src:"<?php echo site_url(); ?>/game-source/img/icons/vehicle.png"},
];
</script>
<script src="https://code.createjs.com/1.0.0/createjs.min.js"></script>
<script src="https://code.createjs.com/1.0.0/preloadjs.min.js"></script>
<script src="<?php echo site_url(); ?>/game-source/lib/howler.core.js"></script>
<script src="<?php echo site_url(); ?>/game-source/lib/gamelib.js?v4"></script>
<script>
var bgm = new Howl({src: ['<?php echo site_url(); ?>/game-source/sounds/BGM.mp3'], loop: true});
var click = new Howl({src: ['<?php echo site_url(); ?>/game-source/sounds/click.wav']});
var lightsaber = new Howl({src: ['<?php echo site_url(); ?>/game-source/sounds/lightsaber.mp3']});
var tada = new Howl({src: ['<?php echo site_url(); ?>/game-source/sounds/tada.mp3']});
</script>
<?php
$current_user = wp_get_current_user();
?>

<div class="game">
	<canvas id="canvas" width="600" height="600"></canvas>
</div>
<div class="back_center">
  <a href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=vouchers" class="btn btn-primary-back">Back</a>
  </div>
  

<script>
init();
window.onResultGet = function(res) {
	console.log("Result is " + res)
    var data = {
		'action': 'add_coupons',
		'res': res,
		'post_id': <?php echo intval($_GET['type']); ?>
	};
	jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', data, function (response) {
		window.location="<?php echo site_url(); ?>/user/<?php echo $current_user->user_login; ?>/?profiletab=vouchers";
	}, 'json');
}
</script>
<?php
	}
}
else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>