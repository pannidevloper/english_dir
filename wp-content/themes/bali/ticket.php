<?php

function p($data, $exit = 1) {
    echo "<pre>";
    print_r($data);
    if ($exit == 1) {
        exit;
    }
}

/* Template Name: Ticket Template */ // get_header();
include ('header.php');

global $wpdb;
$current_user = wp_get_current_user();
//p($current_user);
$cs_user_id = $current_user->data->ID;
$myTickets = $wpdb->get_results("SELECT * FROM contact_support WHERE cs_user_id = '$cs_user_id'", ARRAY_A);
//p($myTickets);
?>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script>
<script type="text/javascript" src="app.js"></script>
<style>
    h1{font-size:30px!important;}
    section{padding:10px 0; border-left:1px solid #ccc;border-right:1px solid #ccc;}
    .input-group .form-control:last-child:focus{font-size:25px;color:#3b898a;height:70px;text-transform: uppercase; -webkit-transition: 0.7s ease-in-out;-moz-transition: 0.7s ease-in-out;-o-transition: 0.7s ease-in-out;transition: 0.7s ease-in-out;}
    .tickettable{padding:10px;}
    td,.ticketpadding{padding:10px!important;}
    tr:hover{background:#3b8989!important;color:white;cursor: pointer}
    th{background:white!important;}
    .swp_social_panel .swp_horizontal_panel, .at-share-btn-elements {display:none!important;}
    .ticketmenu{text-align:right;} 
    .bigfont{font-size: 20px;font-weight: 600;}
    .bigfont button{text-decoration: none; background:#3c9e28;color:white;padding:10px 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;}
    .tipsy, .swp_social_panel{display:none!important;}

    .modal {
        text-align: center;
    }
    .modal-header{font-size:35px!important;background:#5dba42;color:white;}
    .modal-header h4{font-size:35px!important;color:white;}
    .modal-body textarea{width: 100%;min-height: 300px;padding:5px;}
    .createyourticket{width: 100%;text-align: center;font-size:30px!important;text-decoration: none;color: white}
    .createyourticket a{padding:5px 10px;border:1px solid #5dba42;text-align: center;font-size:30px!important;text-decoration: none;color: #5dba42}
    .createyourticket a:hover{background:#5dba42;color:white;-webkit-transition: 0.7s ease-in-out;-moz-transition: 0.7s ease-in-out;-o-transition: 0.7s ease-in-out;transition: 0.7s ease-in-out;}
    @media screen and (min-width: 768px) { 
        .modal:before {
            display: inline-block;
            vertical-align: middle;
            content: " ";
            height: 100%;
        }
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
</style>
<section>
    <h1 class="bigfont"><?php the_title(); ?></h1>

    <div class="row ticketpadding">
        <div class="col-md-6 bigfont">
            <span></span>
        </div>
        <div class="col-md-6 ticketmenu bigfont">
            <?php if (is_user_logged_in()) { ?>
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#openSupport1">Create a new ticket</button>
            <?php } else {
                ?>
                <button type="button" class="btn btn-primary btn-lg" onclick="alert('You must need to login for create a ticket.')">Create a new ticket</button>
            <?php }
            ?>
        </div>
    </div>

    <div ng-app="sortApp" class="tickettable">
        <?php if (!empty($myTickets)) { ?>
            <form action="">
                <div class="form-group well">
                    <div class="input-group">
                        <div class="input-group-addon"><span class="fa fa-search"></span></div>
                        <input type="text" class="form-control" ng-model="searchUserTerm" placeholder="Search request"/>
                    </div>
                </div>
            </form>
            <table class="table table-striped" ng-controller="sortController">
                <thead>
                    <tr>
                        <th style="background:white">
                            <a href="javascript:;" ng-click="sortName = 'ticketID'; sortReverse = !sortReverse">ID
                                <span class="fa fa-caret-down" ng-show="!sortReverse"></span>
                                <span class="fa fa-caret-up" ng-show="sortReverse"></span>
                            </a>
                        </th>
                        <th style="background:white">
                            <a href="javascript:;" ng-click="sortName = 'ticketSubject'; sortReverse = !sortReverse">SUBJECT
                                <span class="fa fa-caret-down" ng-show="!sortReverse"></span>
                                <span class="fa fa-caret-up" ng-show="sortReverse"></span>
                            </a>
                        </th>
                        <th style="background:white">
                            <a href="javascript:;" ng-click="sortName = 'ticketCreated'; sortReverse = !sortReverse">CREATED
                                <span class="fa fa-caret-down" ng-show="!sortReverse"></span>
                                <span class="fa fa-caret-up" ng-show="sortReverse"></span>
                            </a>
                        </th>
                        <th style="background:white">
                            <a href="javascript:;" ng-click="sortName = 'lastActivity'; sortReverse = !sortReverse">LAST ACTIVITY
                                <span class="fa fa-caret-down" ng-show="!sortReverse"></span>
                                <span class="fa fa-caret-up" ng-show="sortReverse"></span>
                            </a>
                        </th>
                        <th style="background:white">
                            <a href="javascript:;" ng-click="sortName = 'ticketStatus'; sortReverse = !sortReverse">STATUS
                                <span class="fa fa-caret-down" ng-show="!sortReverse"></span>
                                <span class="fa fa-caret-up" ng-show="sortReverse"></span>
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="user in users|orderBy:sortName:sortReverse|filter: searchUserTerm">
                        <td ng-bind="user.ticketID"></td>
                        <td ng-bind="user.ticketSubject"></td>
                        <td ng-bind="user.ticketCreated"></td>
                        <td ng-bind="user.lastActivity"></td>
                        <td ng-bind="user.ticketStatus"></td>
                    </tr>
                </tbody>
            </table>
        <?php } else {
            ?>
            <h3 style="color: red;text-align: center;">You don't have any ticket yet!</h3>
        <?php }
        ?>

        <!--        <ul class="pagination">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li><a href="#">8</a></li>
                    <li><a href="#">9</a></li>
                    <li><a href="#">10</a></li>
                </ul>-->

        <!-- Button trigger modal -->
        <form method="POST" action="" id="ticketForm">
            <?php
            $cs_user_id = '';
            $cs_user_name = '';
//            $current_user = wp_get_current_user();
            if (isset($current_user->data->ID)) {
                $cs_user_id = $current_user->data->ID;
            }
            if (isset($current_user->data->user_login)) {
                $cs_user_name = $current_user->data->user_login;
            }
            ?>
            <input type="hidden" name="action" value="create_ticket">
            <input type="hidden" name="cs_user_id" value="<?php echo $cs_user_id; ?>">
            <input type="hidden" name="cs_user_name" value="<?php echo $cs_user_name; ?>">
            <!-- Modal 1-->
            <div class="modal fade" id="openSupport1" tabindex="-1" role="dialog" aria-labelledby="openSupportLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="openSupportLabel">How can we help?</h4>
                        </div>
                        <div class="modal-body">
                            <p>We're committed to finding the answers you need as quickly as possible. Please tell us a little about what you need help with.</p>
                            <textarea name="cs_description" placeholder="Type your question or a description of the problem you're trying to solve here."></textarea>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-next">Continue</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal 2-->
            <div class="modal fade" id="openSupport2" tabindex="-1" role="dialog" aria-labelledby="openSupportLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="openSupportLabel">REVIEW</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="sel1">Please select your question category*</label>
                                <select name="cs_type" class="form-control" id="sel1">		  
                                    <option>Account</option>
                                    <option>Payment</option>
                                    <option>Technical</option>
                                    <option>Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-prev">Prev</button>
                            <button type="button" class="btn btn-default btn-next">Continue</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal 3-->
            <div class="modal fade" id="openSupport3" tabindex="-1" role="dialog" aria-labelledby="openSupportLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="openSupportLabel">COMPLETE</h4>
                        </div>
                        <div class="modal-body">
                            You will open a support ticket if you click the button below.
                        </div>
                        <div class="modal-footer">
                            <div class="createyourticket">
                                <button type="button" onclick="submitTicketForm()" class="btn btn-primary btn-lg">CREATE YOUR TICKET</button>
                                <!--<a href="https://www.travpart.com/English/create-ticket/">CREATE YOUR TICKET</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                <!-- article -->
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php the_content(); ?>

                    <br class="clear">

                    <?php edit_post_link(); ?>

                </article>
                <!-- /article -->

            <?php endwhile; ?>

        <?php else: ?>

            <!-- article -->
            <article>

                <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>

            </article>
            <!-- /article -->

        <?php endif; ?>

        <!-- Table sort from traven -->
<!--        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-sanitize.js"></script>-->
        <script>
            angular.module('sortApp', [])
                    .controller('sortController', ['$scope', function ($scope) {
                            $scope.sortName = 'ticketID';
                            $scope.sortReverse = false;
                            $scope.users = [
<?php
if (!empty($myTickets)) {
    foreach ($myTickets as $ticket) {
        $cs_created = date("d M y", strtotime($ticket['cs_created']));
        $cs_updated = date("d M y", strtotime($ticket['cs_updated']));
        if ($ticket['cs_status'] == '0') {
            $cs_status = "OPEN";
        } else {
            $cs_status = "CLOSED";
        }
        ?>
                                        {
                                            ticketID: <?php echo isset($ticket['cs_id']) ? '000' . $ticket['cs_id'] : '' ?>,
                                            //                                                                    ticketSubject: '<a href="https://www.travpart.com/English/create-ticket/?ticketid=<?php echo isset($ticket['cs_id']) ? $ticket['cs_id'] : '' ?>"><?php echo isset($ticket['cs_type']) ? $ticket['cs_type'] : '' ?></a>',
                                            ticketSubject: '<?php echo isset($ticket['cs_type']) ? $ticket['cs_type'] : '' ?>',
                                            ticketCreated: '<?php echo $cs_created; ?>',
                                            lastActivity: '<?php echo $cs_updated; ?>',
                                            ticketStatus: '<?php echo $cs_status; ?>'
                                        },
        <?php
    }
}
?>
                            ]

                            $scope.search = function () {
                                $scope.filteredList = _.filter($scope.users,
                                        function (item) {
                                            return searchUtil(item, $scope.searchUserTerm);
                                        });
                                if ($scope.searchUserTerm == '') {
                                    $scope.filteredList = $scope.users;
                                }
                            }
                        }
                    ]);

        </script>

        <!-- popup code from traven -->
        <script>

            function submitTicketForm() {
                var form = $("#ticketForm");
                var url = 'https://www.travpart.com/English/submit-ticket/';

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    success: function (data)
                    {
                        var result = JSON.parse(data);
                        if (result.status) {
                            alert(result.message);
                            window.location.href = 'https://www.travpart.com/English/create-ticket/?ticketid=' + result.ticket_id;
                        } else {
                            alert(result.message);
                            window.location.reload()// show response from the php script.
                        }
                    }
                });

            }
			$(function() {
            $("div[id^='openSupport']").each(function () {

                var currentModal = $(this);

                //click next
                currentModal.find('.btn-next').click(function () {
                    currentModal.modal('hide');
                    currentModal.closest("div[id^='openSupport']").nextAll("div[id^='openSupport']").first().modal('show');
                });

                //click prev
                currentModal.find('.btn-prev').click(function () {
                    currentModal.modal('hide');
                    currentModal.closest("div[id^='openSupport']").prevAll("div[id^='openSupport']").first().modal('show');
                });

            });
			});

        </script>

</section>
<?php get_sidebar(); ?>

<?php get_footer(); ?>