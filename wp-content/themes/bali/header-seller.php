<!doctype html>
<html>
    <head>
        <meta name="google-site-verification" content="fjXvx4lEjsk876gksçKjgç0sjg99WwzB3QFldg8yCfkj1u4h61ej7C50drQI"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, height=device-height, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
        <title><?php
            wp_title('');
            echo ' | ';
            bloginfo('name');
            ?></title>


        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
        <link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

        <link href="<?php bloginfo('template_url'); ?>/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php bloginfo('template_url'); ?>/bxslider.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/jquery-range/jquery.range.css" type="text/css">
        <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php bloginfo('template_url'); ?>/css/notification.css" rel="stylesheet" type="text/css">
        <link rel="icon" href="http://www.travpart.com/wp-content/themes/aberration-lite/images/travpart_icon_lnA_icon.ico" />
        <link href="https://fonts.googleapis.com/css?family=Heebo" rel="stylesheet">


        <?php
        if (function_exists('bp_is_user_profile')) :
            if (bp_is_user_profile() || bp_is_profile_edit()) :
                ?>
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDG6aCrxwhxdbMCHZX8YrGYRfkMTrZfvks&libraries=places"></script>
                <script src="<?php bloginfo('template_url'); ?>/js/gmaps.js" type="text/javascript"></script>
                <?php
            endif;
        endif;
        ?>

        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/bxslider.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/jquery-range/jquery.range.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/default.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/layer/layer.js" type="text/javascript"></script>

        <link href="<?php bloginfo('template_url'); ?>/css/colorbox.css" rel="stylesheet" type="text/css">

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
       <!--  <script src="<?php //bloginfo('template_url');     ?>/js/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script> -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.colorbox.js" type="text/javascript"></script>
        <script>
            var $ = jQuery;
        </script>
        <script src="<?php bloginfo('template_url'); ?>/js/currency.js" type="text/javascript"></script>

        <script>
            jQuery(document).ready(function ($) {
                var width = $(document).width();

                if (width >= 992) {
                    width = 992;
                } else if (width >= 768 && width <= 992) {

                    width = 768;
                } else {
                    width = 300;
                }
                //$(".contact_us").colorbox({iframe:true, innerWidth:width, innerHeight:490});
            });
        </script>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-65718700-3', 'auto');
            ga('send', 'pageview');

        </script>

        <script type="text/javascript">

            var pdfdoc = new jsPDF();
            var specialElementHandlers = {
                '#ignoreContent': function (element, renderer) {
                    return true;
                }
            };

            $(document).on('click', '#pdfweb', function () {
                console.log('hi');
                pdfdoc.fromHTML($('.marvelapp-1a').html(), 10, 10, {
                    'width': 110,
                    'elementHandlers': specialElementHandlers
                });
                pdfdoc.save('First.pdf');
            });
			$(function(){
				$('.notificationicon').click(function() {
					$('.notificationtooltips').toggle();
				});
			});
        </script>
        <?php wp_head(); ?>
        
        
        <style>
        @media (max-width: 640px) {
        	 #mega-menu-wrap-main-menu{
			  	padding-top	:0px !important 
			  }
			  .English-logo img {
				    height: 40px !important;
			  }
        }
        </style>
        
		<?php if(is_user_logged_in()){ ?>
			<style>
					.input-group.md-form.form-sm.form-1.pl-0{
						left: 0px !important;
					}
					.English-logo img{
						height: 49px;
						margin-top: 6px;
					}
			</style>	
		<?php } ?>
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.js" type="text/javascript"></script>

        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQ4N2X6');</script>
<!-- End Google Tag Manager -->

    </head>
    <body id="<?php
    if (is_home() || is_front_page()) {
        echo 'hp';
    }
    ?>">
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ4N2X6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
              <?php
              $url = site_url();
              ?>

        <div id="header" style="background: white">
            <div class="container-fluid mobilemenucolor" style="background:#368a8c;padding-top:10px;">
                <div class="container">    
                    <div class="row">
                        <div id="h-t" class="col-md-12 mobilemenucolor" style="background:#368a8c;width:100%;background-size:100%;">
                            <div class="mobilephoneonly"><a href="https://www.travpart.com/English/"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/2019-logo.png" alt="travpart" width="150" style="margin-left:-10px"></a></div>
                            <div class="main-menu pull-right" style="margin:7px;margin-top: -10px;">                                

                               <?php
                                ob_start();
                                wp_nav_menu(array('theme_location' => 'main-menu'));
                                $main_menu = ob_get_contents();
                                ob_end_clean();
                                ?>                               
                                <?php ob_start(); ?>
                                <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/about-us/">ABOUT</a></li>                                
                                <!--<li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont cqheader" href="https://www.travpart.com/English/customers-request/">CUSTOMER REQUEST</a></li>-->
                                <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/travcust" data-toggle="tooltip" title="Create a tour package">TRAVCUST</a></li>
                                <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/travchat" data-toggle="tooltip" title="Chat with our Travel Agents">TRAVCHAT</a></li>
								<?php if(is_user_logged_in()) { ?>
                                <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-link profileicon"><?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?></li>
								<?php } ?>

                                <div class="dropdown">
                                   
                                    <div class="dropbtn"> <!--  Login start --> 
                                        <?php if (is_user_logged_in()) { ?>

                                            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout" style="background:#226d82;text-align: center!important;padding:10px">
                                                <a class="mega-menu-link loginstyle headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=pages"" >
                                                        <?php echo $display_name; ?></a>
                                                <a class="mega-menu-link loginstyle headerfont" href="<?php echo wp_logout_url(get_permalink()); ?>" class="buttype">LOGOUT <i class="fa fa-caret-down" style="display: inline;"></i></a>
                                            <?php } else { ?>
                                                <a class="mega-menu-link loginstyle" href="/English/login/" class="buttype" style="background:#226d82;text-align: center!important;padding:10px;display:block">LOGIN</a>
                                                <style>
                                                    .dropdown-content{display:none!important;}
                                                </style>
                                            <?php } ?>
                                        </li>   
                                        <!--  Login end -->
                                    </div>
                                    <div class="dropdown-content mobilenone">
                                        <!--  User name start -->
                                        <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                            <?php
                                            $display_name = um_user('display_name');
                                            if (is_user_logged_in()) {
                                                ?>
                                                <strong><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=pages"" >
                                                        <?php echo $display_name; ?></a></strong><br>
                                                <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=profile"" ><i class="fas fa-user-circle"></i> MY PROFILE
                                                </a><br>
                                                <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=referal"" ><i class="fas fa-undo-alt"></i> MY REFERRAL
                                                </a><br>
                                                <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=settlement"" ><i class="fas fa-calendar-check"></i> SETTLEMENT
                                                </a><br>
                                                <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=transaction"" ><i class="fas fa-dollar-sign"></i> TRANSACTION
                                                </a><br>
                                                <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=verification"" ><i class="fas fa-calendar-check"></i> VERIFICATION
                                                </a><br>
                                                <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=pages2"" ><i class="fas fa-book-open"></i> BOOKING
                                                </a><br>
                                                <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/ticket/" ><i class="fas fa-info-circle"></i> SUPPORT
                                                </a>
                                                <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/contact/" ><i class="fas fa-phone"></i> CONTACT
                                                </a>
                                            <?php } else { ?>
                                            <?php } ?>
                                        </li>
                                        <!--  User name end -->  
                                    </div>
                                    <div class="mobileonly">
                                    <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                        <table style="width:90%;z-index:99999" class="mobilemenutable">
                                            <tr style="background:#3b898a!important">
                                                <td><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/" style="margin-left:9px"><i class="fas fa-home" style="font-size: 30px;margin-left:9px"></i><div style="margin-left:9px">HOME
                                                </a></div></td>
                                                <td><a class="mega-menu-link headerfont mega-menu-msglink" href="https://www.travpart.com/English/about-us/" style="margin-left:22px"><i class="fas fa-briefcase" style="font-size: 30px;margin-left:9px"></i><div style="margin-left:9px">ABOUT
                                                </a></div></td>
                                                <td><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/travchat" style="margin-left:9px"><i class="fas fa-envelope" style="font-size: 30px;margin-right:9px"></i><div style="margin-right:9px">MESSAGE
												<?php
												$unread_message_count=getUnreadMessageCount(wp_get_current_user()->user_login);
												if($unread_message_count>0) {
													if($unread_message_count>99)
														$unread_message_count='99+';
													echo "<span>{$unread_message_count}</span>";
												}
												?>
                                                </a></div></td>
                                            </tr>
                                        </table>
                                        <!--  User name start -->
                                         <?php
                                            $display_name = um_user('display_name');
                                            if (is_user_logged_in()) {
                                                ?>
                                        <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                            <table style="width:90%" class="mobilemenutable">
                                            <tr>
                                                <td><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=profile"" ><i class="fas fa-user-circle" style="font-size: 30px;"></i><br>
                                                MY PROFILE
                                                </a><br></td>
                                                <td><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=referal"" ><i class="fas fa-undo-alt" style="font-size: 30px;"></i><br>MY REFERRAL
                                                </a></td>
                                                <td><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=settlement"" ><i class="fas fa-calendar-check" style="font-size: 30px;"></i><br> SETTLEMENT
                                                </a></td>
                                            </tr>
                                            <tr>                                                
                                                <td><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=transaction"" ><i class="fas fa-dollar-sign" style="font-size: 30px;"></i><br> TRANSACTION
                                                </a></td>
                                                <td><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=verification"" ><i class="fas fa-calendar-check" style="font-size: 30px;"></i><br> VERIFICATION
                                                </a></td>
                                                <td><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=pages2"" ><i class="fas fa-book-open" style="font-size: 30px;"></i><br> BOOKING
                                                </a></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="mega-menu-link headerfont" href="https://www.travpart.com/English/ticket/" ><i class="fas fa-info-circle" style="font-size: 30px;"></i><br> SUPPORT
                                                </a></td>
                                                <td>
                                                    <a class="mega-menu-link headerfont" href="https://www.travpart.com/Chinese"><i class="fas fa-language" style="font-size: 30px;"></i><br>中文</a></li>
                                                </td>
                                                <td>
                                                    <a class="mega-menu-link headerfont" href="http://www.travpart.com/tutorial/Partner_s_Guidance.pdf" target="_blank" class="mega-menu-link"><i class="fas fa-file-pdf" style="font-size: 30px;color:#e63c60"></i><br> TUTORIAL</li>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                                        <select class="header_curreny search_select" style="padding:0px;height:20px;background-image:none;display:block">

                                                            <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
                                                            <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
                                                            <option full_name="USD" syml="USD" value="USD"> USD </option>
                                                            <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                                                        </select>
                                                    </li>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                <a href="https://www.travpart.com/English/wp-content/themes/bali/App/travpart_english.apk"><div style="border:1px solid white;background:#5bbc2e;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;padding:10px"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/android.png" alt="Download our app" class="androidicon">
                                                Download our mobile app ★★★★★</div>
                                                </a></td> 
                                            </tr>
                                        </table>
                                          <?php } else { ?>
                                            <?php } ?>
                                            </li>
                                    </div>
                                </div>
                                <?php if (is_user_logged_in()) { ?>
                                    <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout notificationicon mobilenone" style="position: relative;"><a class="mega-menu-link" href="#"><img src="https://www.travpart.com/wp-content/uploads/2018/12/notification.png" alt="notification" width="25"></a>

                                        <div class="notificationtooltips" style="display:none;">
                                            <div class="arrow-up"></div>
                                            <div id="nheading">
                                                <div class="nheading-left">
                                                    <h6 class="nheading-title">Notifications</h6>
                                                </div>
                                                <div class="nheading-right">
                                                    <a class="notification-link" href="#">See all</a>
                                                </div>
                                            </div>
                                            <ul class="notification-list">
												
                                                <?php /*<li class="notification-item" v-for="user of users">
                                                    <!-- <div class="img-left">
                                                      <img class="user-photo" alt="User Photo" v-bind:src="user.picture.thumbnail" />
                                                    </div> -->
                                                    <div class="user-content">
                                                        <p class="user-info">You got a new follower.</p>
                                                        <p class="time">1 hour ago</p>
                                                    </div>
                                                </li>*/?>
												<?php
												$notifications=getNotification(wp_get_current_user()->ID);
												foreach($notifications as $row) {
												?>
                                                <li class="notification-item" v-for="user of users">
                                                    <div class="user-content">
                                                        <p class="user-info"><?php echo $row['content']; ?></p>
                                                        <p class="time"><?php echo $row['create_time']; ?></p>
                                                    </div>
                                                </li>
												<?php } ?>
                                            </ul>
                                        </div>
                                    </li>                                                                      

                                <?php } else { ?>
                                <?php } ?>  
                                <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link" href="https://www.travpart.com/Chinese"><img src="<?php bloginfo('template_url'); ?>/images/flg-cn.png" alt="中文" class="mobilenotshow"> 中文</a></li>
                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                    <select class="header_curreny search_select" style="padding:0px;height:20px;background-image:none;">

                                        <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
                                        <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
                                        <option full_name="USD" syml="USD" value="USD"> USD </option>
                                        <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                                    </select>
                                </li>


                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mobilenone"><a class="mega-menu-link" href="http://www.travpart.com/tutorial/Partner_s_Guidance.pdf" target="_blank" class="mega-menu-link"><img src="http://www.travpart.com/tutorial/tutorial-downloadpdf.png" alt="Download Tutorial" style="width:107px;"></a></li>
                                <!-- <li class="mega-menu-link">
                                    <a href="https://www.travpart.com/English/travcust/"><span>Create a tour package</span></a>
                                </li> -->
                                <?php 
                                    global $user_login, $current_user;
                                    get_currentuserinfo();
                                    $user_info = get_userdata($current_user->ID);
                                    $roles = array (
                                        'administrator',
                                        'um_sales-agent',
                                    );

                                if (is_user_logged_in() && array_intersect( $roles, $user_info->roles)) {

                                echo '<li class="mega-menu-link changecolor mobiletravcustli"><a href="https://www.travpart.com/English/travcust/"><span style="padding:5px" class="mobiletravcust">Create a tour package</span></a></li>';

                                } else {

                                echo '<li class="mega-menu-link changecolor mobiletravcustli"><a href="https://www.travpart.com/English/contact/"><span style="padding:5px" class="mobiletravcust">Contact us</span></a></li>';

                                }
                                ?>

                                <?php
                                $append_menu_items = ob_get_contents();
                                ob_end_clean();

                                $main_menu = substr(trim($main_menu), 0, strlen($main_menu) - 11);
								if($unread_message_count>0) {
									if($unread_message_count>99)
										$unread_message_count='99+';
									$main_menu = str_replace('Message', "Message <span>{$unread_message_count}</span>", $main_menu);
								}
                                echo $main_menu . $append_menu_items . '</ul></div></div>';
                                ?>                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid" style="background:white;padding-top:7px">
                <div class="container" style="background:white;height:80px">
                    <div class="row">
                        <div class="col-md-3">
                            <div id="logo" style="margin-top:12px"><a href="https://www.travpart.com"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/2019-logo.png" alt="Travcust"></a></div>
                        </div>
                        <div id="h-r" class="col-md-9">
                            <div style="float:right;margin-top:34px"><a href="https://www.travpart.com/English/travchat" target="_blank" data-toggle="tooltip" data-placement="left" title="Received New Customer Requests"><img src="https://www.travpart.com/English/wp-content/uploads/2018/11/travchat.jpg" alt="Travchat" width="250"></a></div>
                            <div id="menu">
                                <!--<div class="navbar navbar-default" role="navigation">
                                      <div class="navbar-header">
                                        <button class="navbar-toggle" data-target="#menu-lnk" data-toggle="collapse" type="button">
                                              <span class="icon-bar"></span>
                                              <span class="icon-bar"></span>
                                              <span class="icon-bar"></span>
                                          </button>
                                      </div>
                                    </div>
                                    </div>-->
                                <!--  <div class="navbar-collapse collapse" id="menu-lnk">
                                     <ul class="nav navbar-nav">
                                                                 
                                <?php /* $parents=array();
                                  $items = wp_get_nav_menu_items("3");
                                  $child = wp_get_nav_menu_items("3");

                                  foreach ( $items as $item ) {
                                  if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
                                  $parents[] = $item->menu_item_parent;
                                  }
                                  }
                                  foreach($items as $item): */ ?><!--

                                <?php /* if ( $item->menu_item_parent==0 ) {  */ ?>
<li >
<a href="<?php /* echo $item->url; */ ?>"  <?php /* if ( in_array( $item->ID, $parents ) ) { */ ?> class="dropdown-toggle" data-toggle="dropdown" <?php /* } */ ?>><?php /* echo $item->title; */ ?></a>
                                <?php /* }
                                  if ( in_array( $item->ID, $parents ) ) { */ ?>
 <ul class="dropdown-menu">
                                <?php /* foreach ( $child as $child_row ) {
                                  if($child_row->menu_item_parent==$item->ID){
                                  echo " <li><a href='".$child_row->url."'>".$child_row->title."</a></li> ";
                                  }}
                                 */ ?>
</ul> 
                                <?php /* } */ ?> </li>
                                --><?php /*
                                  endforeach; */
                                ?> 
                                <!--                             <li class="last" style="margin-top:18px;"><a class='contact_us' href="--><?php //bloginfo('url');    ?><!--/contact-form">Customize journey</a></li>   -->
                                <!--								-->
                                <!--                            </ul>-->
                                <!--                        </div> -->
                                <!--                    </div>-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            /* if( is_home() || is_front_page() || is_page('14047')) {
              echo '<div id="search_price_box" class="container">';
              echo '<div id="search_price_box_form">';
              echo search_price_box();
              // echo search_nontour_box();

              echo '</div>';
              echo '</div>';
              }
              if(is_home() || is_front_page())
              {
              echo '<div id="search_price_box" class="container">';
              echo '<div id="search_price_box_form2">';
              //echo search_price_box();
              echo search_nontour_box();

              echo '</div>';
              echo '</div>';
              } */

            if (is_page('user')) {
                echo ('<script type="text/javascript" src="' . get_stylesheet_directory_uri() . '/js/my-great-script.js"> </script>');
            }
            ?>

            <?php
// echo do_shortcode('[shubhshort]'); //for display
            /*
              if(is_home() || is_front_page()) {
              putRevSlider( 'homepage' );
              } */
            ?>

			Test Here

            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <?php if (!is_home() && !is_front_page()) { ?>
                                <div class="row">
                                    <div class="col-md-8">
                                        <?php getBcrumbs(); ?>
                                    </div>

                                </div>
                            <?php } ?>