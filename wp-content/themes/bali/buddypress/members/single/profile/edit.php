<?php
/**
 * BuddyPress - Members Single Profile Edit
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

/**
 * Fires after the display of member profile edit content.
 *
 * @since 1.1.0
 */
do_action( 'bp_before_profile_edit_content' );

if ( bp_has_profile( 'profile_group_id=' . bp_get_current_profile_group_id() ) ) :

	//check if is not group Map
	if( bp_get_current_profile_group_id() != 3 ) {

	while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

	<form action="<?php bp_the_profile_group_edit_form_action(); ?>" method="post" id="profile-edit-form" class="standard-form <?php bp_the_profile_group_slug(); ?>">

		<?php

			/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/profile/profile-wp.php */
			do_action( 'bp_before_profile_field_content' ); ?>

			<h2><?php printf( __( "Editing '%s' Profile Group", 'buddypress' ), bp_get_the_profile_group_name() ); ?></h2>

			<?php if ( bp_profile_has_multiple_groups() ) : ?>
				<ul class="button-nav" aria-label="<?php esc_attr_e( 'Profile field groups', 'buddypress' ); ?>" role="navigation">

					<?php bp_profile_group_tabs(); ?>

				</ul>
			<?php endif ;?>

			<div class="clear"></div>

			<?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>

				<div<?php bp_field_css_class( 'editfield' ); ?>>

					<?php
					$field_type = bp_xprofile_create_field_type( bp_get_the_profile_field_type() );
					$field_type->edit_field_html();

					/**
					 * Fires before the display of visibility options for the field.
					 *
					 * @since 1.7.0
					 */
					do_action( 'bp_custom_profile_edit_fields_pre_visibility' );
					?>

					<?php if ( bp_current_user_can( 'bp_xprofile_change_field_visibility' ) ) : ?>
						<p class="field-visibility-settings-toggle" id="field-visibility-settings-toggle-<?php bp_the_profile_field_id() ?>">
							<?php
							printf(
								__( 'This field can be seen by: %s', 'buddypress' ),
								'<span class="current-visibility-level">' . bp_get_the_profile_field_visibility_level_label() . '</span>'
							);
							?>
							<a href="#" class="visibility-toggle-link"><?php _e( 'Change', 'buddypress' ); ?></a>
						</p>

						<div class="field-visibility-settings" id="field-visibility-settings-<?php bp_the_profile_field_id() ?>">
							<fieldset>
								<legend><?php _e( 'Who can see this field?', 'buddypress' ) ?></legend>

								<?php bp_profile_visibility_radio_buttons() ?>

							</fieldset>
							<a class="field-visibility-settings-close" href="#"><?php _e( 'Close', 'buddypress' ) ?></a>
						</div>
					<?php else : ?>
						<div class="field-visibility-settings-notoggle" id="field-visibility-settings-toggle-<?php bp_the_profile_field_id() ?>">
							<?php
							printf(
								__( 'This field can be seen by: %s', 'buddypress' ),
								'<span class="current-visibility-level">' . bp_get_the_profile_field_visibility_level_label() . '</span>'
							);
							?>
						</div>
					<?php endif ?>

					<?php

					/**
					 * Fires after the visibility options for a field.
					 *
					 * @since 1.1.0
					 */
					do_action( 'bp_custom_profile_edit_fields' ); ?>

					<p class="description"><?php bp_the_profile_field_description(); ?></p>
				</div>

			<?php endwhile; ?>

		<?php

		/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/profile/profile-wp.php */
		do_action( 'bp_after_profile_field_content' ); ?>

		<div class="submit">
			<input type="submit" name="profile-group-edit-submit" id="profile-group-edit-submit" value="<?php esc_attr_e( 'Save Changes', 'buddypress' ); ?> " />
		</div>

		<input type="hidden" name="field_ids" id="field_ids" value="<?php bp_the_profile_field_ids(); ?>" />

		<?php wp_nonce_field( 'bp_xprofile_edit' ); ?>

	</form>

<?php endwhile; 
	  } else { ?>
	  	<h2><?php printf( __( "Editing '%s' Profile Group", 'buddypress' ), 'Map' ); ?></h2>

		<?php if ( bp_profile_has_multiple_groups() ) : ?>
			<ul style="margin-top: 15px; padding: 0;" class="button-nav" aria-label="<?php esc_attr_e( 'Profile field groups', 'buddypress' ); ?>" role="navigation">

				<?php bp_profile_group_tabs(); ?>
				<div class="clear"></div>
			</ul>
		<?php endif;
		//if current logged in user is buddypress user
		if( bp_displayed_user_id() == get_current_user_id() ) {
			?>
				<h3>How to add your place to the map:</h3>
				<blockquote>
					1. Right mouse click on the map and choose "Has lived here", "Has been here" or "Want to go here".<br />
					2. Remove the place you've added by right click on the marker and choose Remove.
				</blockquote>
    			<div id="google-map-wrap">
    				<!-- <input id="google-map-search-box" class="controls" type="text" placeholder="Search"> -->
    				<div id="map"></div>
    			</div>
			    <script>
			      var map = '';
			      function user_profile_gmap_initMap() {

			        map = new GMaps({
				        div: '#map',
				        lat: 0,
				        lng: 0,
			          	zoom: 2,
				      });

			        var iconBase = '<?php echo get_template_directory_uri(); ?>/images/';
			        var icons = {
			          has_lived_here: {
			            icon: iconBase + 'mm_20_red.png'
			          },
			          has_been_here: {
			            icon: iconBase + 'mm_20_blue.png'
			          },
			          want_to_go_here: {
			            icon: iconBase + 'mm_20_yellow.png'
			          }
			        };

			        map.setContextMenu({
					  control: 'map',
					  options: [
						  {
						    title: 'Has lived here',
						    name: 'has_lived_here',
						    action: function(e) {
						      this.addMarker({
						        lat: e.latLng.lat(),
						        lng: e.latLng.lng(),
						        title: 'has_lived_here',
						        icon: icons.has_lived_here.icon
						      });
						    }
					  	  },
						  {
						    title: 'Has been here',
						    name: 'has_been_here',
						    action: function(e) {
						      this.addMarker({
						        lat: e.latLng.lat(),
						        lng: e.latLng.lng(),
						        title: 'has_been_here',
						        icon: icons.has_been_here.icon
						      });
						    }
					  	  },
						  {
						    title: 'Want to go here',
						    name: 'want_to_go_here',
						    action: function(e) {
						      this.addMarker({
						        lat: e.latLng.lat(),
						        lng: e.latLng.lng(),
						        title: 'want_to_go_here',
						        icon: icons.want_to_go_here.icon
						      });
						    }
					  	  }
					  ]
					});
					map.setContextMenu({
				        control: 'marker',
				        options: [{
				            title: 'Remove',
				            name: 'remove_this_location',
				            action: function(e){
				                this.removeMarker(e.marker);
				            }
				        }]
				    });

				    <?php 
				    	$buddypress_user_profile_map = get_option('buddypress_user_profile_map_user_' . bp_displayed_user_id() );
				    	if( $buddypress_user_profile_map ) {
				    		foreach($buddypress_user_profile_map as $key => $value) {
	        				$positions = json_decode($value);
	        				//$positions = explode(',', $positions);
			        		switch($key) {
			        			case 'has_lived_here':
			        				foreach($positions as $position) {
	        							$latlong = explode(',', $position);
			        					?>
			        						map.addMarker({
									          lat: <?php echo trim($latlong[0]);?>,
									          lng: <?php echo trim($latlong[1]);?>,
											  title: '<?php echo $key; ?>',
											  icon: icons.<?php echo $key; ?>.icon
											});
			        					<?php
			        				}
			        			break;
			        			case 'has_been_here':
			        				foreach($positions as $position) {
	        							$latlong = explode(',', $position);
			        					?>
			        						map.addMarker({
									          lat: <?php echo trim($latlong[0]);?>,
									          lng: <?php echo trim($latlong[1]);?>,
											  title: '<?php echo $key; ?>',
											  icon: icons.<?php echo $key; ?>.icon
											});
			        					<?php
			        				}
			        			break;
			        			case 'want_to_go_here':
			        				foreach($positions as $position) {
	        							$latlong = explode(',', $position);
			        					?>
			        						map.addMarker({
									          lat: <?php echo trim($latlong[0]);?>,
									          lng: <?php echo trim($latlong[1]);?>,
											  title: '<?php echo $key; ?>',
											  icon: icons.<?php echo $key; ?>.icon
											});
			        					<?php
			        				}
			        			break;
			        		}
			        	}
				    	}
				    ?>
			      }

			      user_profile_gmap_initMap();

			      jQuery(document).ready(function(){
			      	jQuery('#front-end-user-profile-map-form input[type=submit]').remove();
				    jQuery('#map-edit input').on('click', function(){
				    	var buddypress_user_profile_map = [];
				    	var has_been_here = [];
				    	var has_lived_here = [];
				    	var want_to_go_here = [];

				    	if(map.markers.length > 0) {
					    	for (var i = map.markers.length - 1; i >= 0; i--) {
					    		switch(map.markers[i].title) {
					    			case 'has_been_here':
								        has_been_here.push( map.markers[i].position.lat() +', '+ map.markers[i].position.lng() );
								        break;
								    case 'has_lived_here':
								        has_lived_here.push( map.markers[i].position.lat() +', '+ map.markers[i].position.lng() );
								        break;
								    case 'want_to_go_here':
								        want_to_go_here.push( map.markers[i].position.lat() +', '+ map.markers[i].position.lng() );
								        break;
					    		}
					    	};
				    	}
				    	//"has_lived_here":["-25.274398, 133.77513599999997","-31.2532183, 146.92109900000003"],"has_been_here":["40.7127837, -74.00594130000002"],"want_to_go_here":["-8.4095178, 115.18891600000006"]
				    	var has_been_here_jsonString = JSON.stringify( has_been_here );
						jQuery('#front-end-user-profile-map-form input[name="has_been_here"]').val( has_been_here_jsonString );

						var has_lived_here_jsonString = JSON.stringify( has_lived_here );
						jQuery('#front-end-user-profile-map-form input[name="has_lived_here"]').val( has_lived_here_jsonString );

						var want_to_go_here_jsonString = JSON.stringify( want_to_go_here );
						jQuery('#front-end-user-profile-map-form input[name="want_to_go_here"]').val( want_to_go_here_jsonString );

						jQuery('#front-end-user-profile-map-form').submit();
				    })
			      });
			    </script>
			    <?php
			echo '<div id="map-edit" style="margin-top: 10px;">';
				echo '<p><input type="button" value="Save Changes" /></p>';
				echo do_shortcode('[cmb-frontend-form]');	
			echo '</div>';
		}
	  }
	endif; ?>

<?php

/**
 * Fires after the display of member profile edit content.
 *
 * @since 1.1.0
 */
do_action( 'bp_after_profile_edit_content' ); ?>
