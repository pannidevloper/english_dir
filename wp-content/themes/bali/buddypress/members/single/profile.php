<?php
/**
 * BuddyPress - Users Profile
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<div class="item-list-tabs no-ajax" id="subnav" aria-label="<?php esc_attr_e( 'Member secondary navigation', 'buddypress' ); ?>" role="navigation">
	<ul>
		<?php 
			if(is_user_logged_in()) : 
			bp_get_options_nav(); 
			endif;
		?>
	</ul>
</div><!-- .item-list-tabs -->

<?php

/**
 * Fires before the display of member profile content.
 *
 * @since 1.1.0
 */
do_action( 'bp_before_profile_content' ); ?>

<div class="profile">

<?php switch ( bp_current_action() ) :

	// Edit
	case 'edit'   :
		bp_get_template_part( 'members/single/profile/edit' );
		break;

	// Change Avatar
	case 'change-avatar' :
		bp_get_template_part( 'members/single/profile/change-avatar' );
		break;

	// Change Cover Image
	case 'change-cover-image' :
		bp_get_template_part( 'members/single/profile/change-cover-image' );
		break;

	// Compose
	case 'public' :

		// Display XProfile
		if ( bp_is_active( 'xprofile' ) ) :

			$buddypress_user_profile_map = get_option('buddypress_user_profile_map_user_' . bp_displayed_user_id() );

			if( $buddypress_user_profile_map ) {
				?>
				<p><img class="google-markers-intro" src="<?php echo get_template_directory_uri(); ?>/images/marker.png" alt="" /></p>
				<div id="user_profile_main_page_map">
					<div id="map"></div>
				</div>
			    <script>
			      var map;
			      function user_profile_main_page_initMap() {
			        map = new google.maps.Map(document.getElementById('map'), {
			          zoom: 1,
			          center: new google.maps.LatLng(0, 0),
			          mapTypeId: 'roadmap'
			        });

			        var iconBase = '<?php echo get_template_directory_uri(); ?>/images/';
			        var icons = {
			          has_lived_here: {
			            icon: iconBase + 'mm_20_red.png'
			          },
			          has_been_here: {
			            icon: iconBase + 'mm_20_blue.png'
			          },
			          want_to_go_here: {
			            icon: iconBase + 'mm_20_yellow.png'
			          }
			        };

			        function addMarker(feature) {
			          var marker = new google.maps.Marker({
			            position: feature.position,
			            icon: icons[feature.type].icon,
			            map: map
			          });
			        }

			        var user_maps = [];

			        <?php 
			        	foreach($buddypress_user_profile_map as $key => $value) {
			        		switch($key) {
			        			case 'has_lived_here':
			        				$positions = json_decode($value);
			        				foreach($positions as $latlong) {
			        					?>
			        						user_maps.push({ position: new google.maps.LatLng(<?php echo $latlong; ?>), type: 'has_lived_here' });
			        					<?php
			        				}
			        			break;
			        			case 'has_been_here':
			        				$positions = json_decode($value);
			        				foreach($positions as $latlong) {
			        					?>
			        						user_maps.push({ position: new google.maps.LatLng(<?php echo $latlong; ?>), type: 'has_been_here' });
			        					<?php
			        				}
			        			break;
			        			case 'want_to_go_here':
			        				$positions = json_decode($value);
			        				foreach($positions as $latlong) {
			        					?>
			        						user_maps.push({ position: new google.maps.LatLng(<?php echo $latlong; ?>), type: 'want_to_go_here' });
			        					<?php
			        				}
			        			break;
			        		}
			        	}
			        ?>

			        for (var i = 0, user_map; user_map = user_maps[i]; i++) {
			          addMarker(user_map);
			        }
			      }
			      user_profile_main_page_initMap();
			    </script>
			    <!--script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDG6aCrxwhxdbMCHZX8YrGYRfkMTrZfvks&callback=user_profile_main_page_initMap"></script-->
				<?php
			}

			bp_get_template_part( 'members/single/profile/profile-loop' );

			if(function_exists('rtmedia_load_template')) :
				echo '<div id="user-gallery">';
					echo '<h2 class="rtm-gallery-title">Photos</h2>';
					echo do_shortcode('[rtmedia_gallery context=profile context_id='. bp_displayed_user_id() .' global="false" media_title=false media_type="photo" per_page=12]');
					echo '<h2 class="rtm-gallery-title">Videos</h2>';
					echo do_shortcode('[rtmedia_gallery context=profile context_id='. bp_displayed_user_id() .' global="false" media_title=false media_type="video" per_page=12]');
				echo '</div>';
			endif;
		// Display WordPress profile (fallback)
		else :

			bp_get_template_part( 'members/single/profile/profile-wp' );
		
		endif;
		break;

	// Any other
	default :
		bp_get_template_part( 'members/single/plugins' );
		break;
endswitch; ?>

<?php 
?>

</div><!-- .profile -->

<?php

/**
 * Fires after the display of member profile content.
 *
 * @since 1.1.0
 */
do_action( 'bp_after_profile_content' ); ?>
