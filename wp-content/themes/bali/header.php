<!DOCTYPE html>

<head>
  <meta name="google-site-verification" content="fjXvx4lEjsk876gksçKjgç0sjg99WwzB3QFldg8yCfkj1u4h61ej7C50drQI" />
  <meta name="google-site-verification" content="NhWyLCbR3xVKqI-q-kyi_f2IkugtLhsqGP2aKi9YNkQ" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, height=device-height, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

  <?php
  // Added for Sharing and SEO ( Farhan and Lix) on 20 June 2019
  if (is_page(14061)) {
    if (!empty($_GET['bookingcode']))
      $tour_id = intval(substr($_GET['bookingcode'], 4));
    else if (!empty($_COOKIE['tour_id']))
      $tour_id = intval($_COOKIE['tour_id']);
    else
      $tour_id = '';
    $postdata = $wpdb->get_row("SELECT ID,post_title,post_content FROM wp_posts,wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$tour_id}'");
    if (!empty($postdata->post_title) && !empty($postdata->post_content)) {
      $post_title = htmlspecialchars($postdata->post_title);
      $post_content = htmlspecialchars(substr($postdata->post_content, 0, 120));

      $url = "https://www.travpart.com/English/tour-details/?bookingcode=BALI" . $tour_id;
      metafortour($post_title, $post_content, 'https://www.travpart.com/English/wp-content/uploads/2020/02/bali.jpg', $url);
    }
  }
  // added for Message button on tour page
  if (is_page(14061)) {

    if (isset($_GET['agent_id'])) {

      $agent_id =  $_GET['agent_id'];

      if (is_user_logged_in()) {

        $chat_link = site_url() . "/travchat/user/addnewmember.php?user=" . $agent_id;
      } else {
        $rand_guest_id = rand(99, 9999);
        $chat_link = site_url() . "/travchat/guestUserLogin.php?id=$rand_guest_id&to_connect=" . $agent_id;
      }
    } else {

      $chat_link = site_url() . "/travchat/";
    }
  } else {
    $chat_link = site_url() . "/travchat/";
  }
  ?>

  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-TQ4N2X6');
  </script>
  <!-- End Google Tag Manager -->
  <title>
    <?php
    // str_ireplace( " - Travpart", "Travpart", wp_title('') );
    // echo ' | ';
    bloginfo('name');
    ?></title>


  <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
  <link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
  <style>
  	.mobileappbg{
		display: none;
	}
	@media(max-width:600px){
		.mobileappbg{
			display: block;
		}
	}
  </style>	


  <?php
  if (is_page()) {
    $bail_template = basename(get_page_template());
    if ($bail_template == "tpl-mytimeline.php") {
    } else {

  ?>
      

  <?php
    }
  }
  ?>

  <link async="async" href="<?php bloginfo('template_url'); ?>/bootstrap.css" rel="stylesheet" media="(min-width: 40em)" type="text/css">

  <link async="async" href="<?php bloginfo('template_url'); ?>/bxslider.css" rel="stylesheet" media="(min-width: 40em)" type="text/css">
  <link async="async" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/jquery-range/jquery.range.css" media="(min-width: 40em)" type="text/css">
  <link async="async" href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css">
  <link async="async" href="<?php bloginfo('template_url'); ?>/css/notification.css" rel="stylesheet" media="(min-width: 40em)" type="text/css">
  <link async="async" rel="icon" href="http://www.travpart.com/English/wp-content/uploads/2020/01/travpart_icon_lnA_icon.ico" />

  <link async="async" href="<?php bloginfo('template_url'); ?>/css/style_custom.css?ver37" rel="stylesheet" type="text/css" />

  <style>
    .input-group-prepend {
        margin-right: -1px;
    }
    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0px;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 10rem;
        font-size: 1rem;
        color: rgb(33, 37, 41);
        text-align: left;
        background-color: rgb(255, 255, 255);
        background-clip: padding-box;
        padding: 0.5rem 0px;
        margin: 0.125rem 0px 0px;
        list-style: none;
        border-width: 1px;
        border-style: solid;
        border-color: rgba(0, 0, 0, 0.15);
        border-image: initial;
        border-radius: 0.25rem;
    }
    .input-group-append, .input-group-prepend {
        display: flex;
    }
    .input-group {
        position: relative;
        display: flex;
        flex-wrap: wrap; 
        -webkit-box-align: stretch;
        align-items: stretch;  
        width: 100%;
  }
  .dropdown-item {
      display: block;
      width: 100%;
      padding: .25rem 1.5rem;
      clear: both;
      font-weight: 400;
      color: #212529;
      text-align: inherit;
      white-space: nowrap;
      background-color: transparent;
      border: 0;
  }
  .dropdown-toggle::after {
      display: inline-block;
      width: 0;
      height: 0;
      margin-left: .255em;
      vertical-align: .255em;
      content: "";
      border-top: .3em solid;
      border-right: .3em solid transparent;
      border-bottom: 0;
      border-left: .3em solid transparent;
  }
  .input-group-prepend .dropdown-toggle:after {
      display: inline-block;
      vertical-align: middle;
      font-weight: 900;
  }

/********************************************************/
/* Max-Width 767PX     **********************************/
/********************************************************/
@media (max-width: 767px) {

/*Nav Menu Css*/
.logged-in .custom_menu_btn_mobile i {
    right: -12px;
    top: -3px;
}

.single-shortpost .custom_menu_btn_mobile i {
    right: -12px;
    top: -3px;
}

.col-mmd-4.header-mobile-curreny{
  display: none;
}

.col-mmd-4 .user-logout-btn, .col-mmd-4 .user-login-btn{
    font-size: 18px;
    box-shadow: none!important;
    background: inherit !important;

}

.rows .col-mmd-4 {
  margin-bottom: 15px;
}

.rows.menu_mob_dis .col-mmmd-4{
  margin: 10px 0;
}

/*End nav Menu css*/

  
}



/********************************************************/
/* Max-Width 576PX     **********************************/
/********************************************************/
@media (max-width: 576px) {


/*Nav Menu Css*/

.logged-in .custom_menu_btn_mobile i {
    right: -12px;
    top: -3px;
}

.single-shortpost .custom_menu_btn_mobile i {
    right: -12px;
    top: -3px;
}

.col-mmd-4.header-mobile-curreny{
  display: none;
}

.col-mmd-4 .user-logout-btn, .col-mmd-4 .user-login-btn{
    font-size: 18px;
    box-shadow: none!important;
    background: inherit !important;

}

.rows .col-mmd-4 {
  margin-bottom: 15px;
}

.rows.menu_mob_dis .col-mmmd-4{
  margin: 10px 0;
}

/*End nav Menu css*/


}


  </style>
  <?php if (!is_user_logged_in()) { ?>
    <style>
      .event_text_icon{
      font-size:12px;
    }
    
      .main-menu {
        padding-top: 5px;
      }
    </style>
  <?php } ?>



  <?php
  if (function_exists('bp_is_user_profile')) :
    if (bp_is_user_profile() || bp_is_profile_edit()) :
  ?>
      <script async="async" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDG6aCrxwhxdbMCHZX8YrGYRfkMTrZfvks&libraries=places"></script>
      <script async="async" src="<?php bloginfo('template_url'); ?>/js/gmaps.js" type="text/javascript"></script>
  <?php
    endif;
  endif;
  ?>

  <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>



  <?php
  if (is_page()) {
    $bail_template = basename(get_page_template());
    if ($bail_template == "timeline.php") {
    } else {

  ?>

      <link rel="preload" href="https://fonts.googleapis.com/css?family=Catamaran:800|Roboto+Condensed&display=swap" rel="stylesheet" />
      <link rel="preload" href="https://fonts.googleapis.com/css?family=Catamaran:800&display=swap" rel="stylesheet" />

      <script async="async" src="<?php bloginfo('template_url'); ?>/js/bxslider.js" type="text/javascript"></script>


  <?php
    }
  }
  ?>

      <script async="async" src="<?php bloginfo('template_url'); ?>/jquery-range/jquery.range.js"></script>
      <script async="async" src="<?php bloginfo('template_url'); ?>/js/default.js?v1" type="text/javascript"></script>
      <script async="async" src="<?php bloginfo('template_url'); ?>/layer/layer.js" type="text/javascript"></script>

      <link async="async" href="<?php bloginfo('template_url'); ?>/css/colorbox.css" rel="stylesheet" media="(min-width: 40em)" type="text/css">

      <script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>

      <script async="async" src="<?php bloginfo('template_url'); ?>/js/jquery.colorbox.js" type="text/javascript"></script>

      <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('.hide_not,.hide_pro').hide();
      $('.lighten-3').click(function() {
        $('.custom_d').toggle();
      })
      $('.custom_menu_btn_mobile').click(function() {
        $(this).find('i').toggleClass('fa-bars fa-times-circle');
        $('.home_mytime_msg').slideToggle();
      })
      $('.not_slide').click(function() {
        $(this).find('i').toggleClass('fa-angle-down fa-angle-up');
        $('.hide_not').slideToggle();
      });
      $('.pro_slide').click(function() {

        $(this).find('i').toggleClass('fa-angle-down fa-angle-up');
        $('.hide_pro').slideToggle();

      });
      $(".total_cost_display_2").click(function() {
        $(".total_cost_content_mob").show();
        $(".total_cost_display_3 i.fas.fa-chevron-down").hide();
        $(".total_cost_display_3 i.fas.fa-chevron-up").show();
      });
      $(".total_cost_display_3 i.fas.fa-chevron-down").click(function() {
        $(".total_cost_content_mob").show();
        $(".total_cost_display_3 i.fas.fa-chevron-down").hide();
        $(".total_cost_display_3 i.fas.fa-chevron-up").show();
      });
      $(".total_cost_display_3 i.fas.fa-chevron-up").click(function() {
        $(".total_cost_content_mob").hide();
        $(".total_cost_mob").css("bottom", "55px");
        $(".total_cost_display_3 i.fas.fa-chevron-down").show();
        $(".total_cost_display_3 i.fas.fa-chevron-up").hide();
      });
      $(".book_btn_display_2").click(function() {
        $(".book_btn_content_mob").show();
        $(".total_cost_content_mob").hide();
        $(".total_cost_mob").hide();
        $(".book_btn_display_3 i.fas.fa-chevron-down").hide();
        $(".book_btn_display_3 i.fas.fa-chevron-up").show();
      });
      $(".book_btn_display_3 i.fas.fa-chevron-down").click(function() {
        $(".book_btn_content_mob").show();
        $(".total_cost_content_mob").hide();
        $(".total_cost_mob").hide();
        $(".book_btn_display_3 i.fas.fa-chevron-down").hide();
        $(".book_btn_display_3 i.fas.fa-chevron-up").show();
      });
      $(".book_btn_display_3 i.fas.fa-chevron-up").click(function() {
        $(".book_btn_content_mob").hide();
        $(".total_cost_mob").show();
        $(".total_cost_mob").css("bottom", "55px");
        $(".book_btn_mob").css("bottom", "0%");
        $(".book_btn_display_3 i.fas.fa-chevron-down").show();
        $(".book_btn_display_3 i.fas.fa-chevron-up").hide();
      });
      <?php if (is_user_logged_in() && !current_user_can('um_travel-advisor')) { ?>
        $(".book_btn_display_3 i.fas.fa-chevron-down,.book_btn_display_2").click(function() {
          $(".book_btn_mob").css("bottom", "314px");
        });
      <?php } ?>
      <?php if (!is_user_logged_in() && !current_user_can('um_travel-advisor')) { ?>
        $(".book_btn_display_3 i.fas.fa-chevron-down,.book_btn_display_2").click(function() {
          $(".book_btn_mob").css("bottom", "165px");
        });
      <?php } ?>
      <?php if (is_user_logged_in() && current_user_can('um_travel-advisor')) { ?>
        $(".book_btn_display_3 i.fas.fa-chevron-down,.book_btn_display_2").click(function() {
          $(".book_btn_mob").css("bottom", "88px");
        });
      <?php } ?>

      function total_cost_tab_320(x320) {
        if (x320.matches) { // If media query matches
          $(".total_cost_display_3 i.fas.fa-chevron-down,.total_cost_display_2").click(function() {
            $(".total_cost_mob").css("bottom", "258px");
          });
        }
      }
      var x320 = window.matchMedia("(min-width:180px) and (max-width: 320px)")
      total_cost_tab_320(x320)
      x320.addListener(total_cost_tab_320)

      function total_cost_tab_360(x360) {
        if (x360.matches) { // If media query matches
          $(".total_cost_display_3 i.fas.fa-chevron-down,.total_cost_display_2").click(function() {
            $(".total_cost_mob").css("bottom", "278px");
          });
        }
      }
      var x360 = window.matchMedia("(min-width:321px) and (max-width: 360px)")
      total_cost_tab_360(x360)
      x360.addListener(total_cost_tab_360)

      function total_cost_tab_375(x375) {
        if (x375.matches) { // If media query matches
          $(".total_cost_display_3 i.fas.fa-chevron-down,.total_cost_display_2").click(function() {
            $(".total_cost_mob").css("bottom", "264px");
          });
        }
      }
      var x375 = window.matchMedia("(min-width:361px) and (max-width: 375px)")
      total_cost_tab_375(x375)
      x375.addListener(total_cost_tab_375)

      function total_cost_tab_411(x411) {
        if (x411.matches) { // If media query matches
          $(".total_cost_display_3 i.fas.fa-chevron-down,.total_cost_display_2").click(function() {
            $(".total_cost_mob").css("bottom", "298px");
          });
        }
      }
      var x411 = window.matchMedia("(min-width:376px) and (max-width: 411px)")
      total_cost_tab_411(x411)
      x411.addListener(total_cost_tab_411)

      function total_cost_tab_414(x414) {
        if (x414.matches) { // If media query matches
          $(".total_cost_display_3 i.fas.fa-chevron-down,.total_cost_display_2").click(function() {
            $(".total_cost_mob").css("bottom", "298px");
          });
        }
      }
      var x414 = window.matchMedia("(min-width:412px) and (max-width: 414px)")
      total_cost_tab_411(x414)
      x414.addListener(total_cost_tab_414)

      function total_cost_tab_600(x600) {
        if (x600.matches) { // If media query matches
          $(".total_cost_display_3 i.fas.fa-chevron-down,.total_cost_display_2").click(function() {
            $(".total_cost_mob").css("bottom", "298px");
          });
        }
      }
      var x600 = window.matchMedia("(min-width:415px) and (max-width: 600px)")
      total_cost_tab_600(x600)
      x600.addListener(total_cost_tab_600)

      $(".holel_tour_tab").on("click", function() {
        if ($('.hotel_item_box').is(':visible')) {
          $(".hotel_icon").addClass("fa-minus");
          $(".hotel_icon").removeClass("fa-plus");
        } else {
          $(".hotel_icon").addClass("fa-plus");
          $(".hotel_icon").removeClass("fa-minus");
        }
      });

      $(".optional_loc_tab").on("click", function() {
        if ($('.op_loc_item_box').is(':visible')) {
          $(".op_loc_icon").addClass("fa-minus");
          $(".op_loc_icon").removeClass("fa-plus");
        } else {
          $(".op_loc_icon").addClass("fa-plus");
          $(".op_loc_icon").removeClass("fa-minus");
        }
      });

      $(".dining_arr_tab").on("click", function() {
        if ($('.dining_arr_item_box').is(':visible')) {
          $(".dining_arr_icon").addClass("fa-minus");
          $(".dining_arr_icon").removeClass("fa-plus");
        } else {
          $(".dining_arr_icon").addClass("fa-plus");
          $(".dining_arr_icon").removeClass("fa-minus");
        }
      });
      $(".recom_place_tab").on("click", function() {
        if ($('.recom_place_item_box').is(':visible')) {
          $(".recom_place_icon").addClass("fa-minus");
          $(".recom_place_icon").removeClass("fa-plus");
        } else {
          $(".recom_place_icon").addClass("fa-plus");
          $(".recom_place_icon").removeClass("fa-minus");
        }
      });
      $(".spa_tab").on("click", function() {
        if ($('.spa_item_box').is(':visible')) {
          $(".spa_icon").addClass("fa-minus");
          $(".spa_icon").removeClass("fa-plus");
        } else {
          $(".spa_icon").addClass("fa-plus");
          $(".spa_icon").removeClass("fa-minus");
        }
      });
      $(".tour_guide_tab").on("click", function() {
        if ($('.tour_guide_item_box').is(':visible')) {
          $(".tour_guide_icon").addClass("fa-minus");
          $(".tour_guide_icon").removeClass("fa-plus");
        } else {
          $(".tour_guide_icon").addClass("fa-plus");
          $(".tour_guide_icon").removeClass("fa-minus");
        }
      });
      $(".transport_tab").on("click", function() {
        if ($('.transport_item_box').is(':visible')) {
          $(".transport_icon").addClass("fa-minus");
          $(".transport_icon").removeClass("fa-plus");
        } else {
          $(".transport_icon").addClass("fa-plus");
          $(".transport_icon").removeClass("fa-minus");
        }
      });
      $(".heli_cru_tab").on("click", function() {
        if ($('.heli_cru_item_box').is(':visible')) {
          $(".heli_cru_icon").addClass("fa-minus");
          $(".heli_cru_icon").removeClass("fa-plus");
        } else {
          $(".heli_cru_icon").addClass("fa-plus");
          $(".heli_cru_icon").removeClass("fa-minus");
        }
      });
      $(".op_activities_tab").on("click", function() {
        if ($('.op_activities_item_box').is(':visible')) {
          $(".op_activities_icon").addClass("fa-minus");
          $(".op_activities_icon").removeClass("fa-plus");
        } else {
          $(".op_activities_icon").addClass("fa-plus");
          $(".op_activities_icon").removeClass("fa-minus");
        }
      });
      $(".add_op_tra_item_tab").on("click", function() {
        if ($('.add_op_tra_item_box').is(':visible')) {
          $(".add_op_tra_item_icon").addClass("fa-minus");
          $(".add_op_tra_item_icon").removeClass("fa-plus");
        } else {
          $(".add_op_tra_item_icon").addClass("fa-plus");
          $(".add_op_tra_item_icon").removeClass("fa-minus");
        }
      });
  
    });
  </script>

  <link async="async" rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/mobile.css?v5" />
  <link async="async" href="<?php bloginfo('template_url'); ?>/css/header_custom.css?vr=15" rel="stylesheet" type="text/css">

  <?php if (is_page($page = 'about-us')) { ?>
    <style>
      @media only screen and (max-width: 600px) {

        .mobileonly,
        .mobilephoneonly {
          display: none !important;
        }
      }
    </style>
  <?php } ?>

  <script>
    var $ = jQuery;
  </script>
  <script src="<?php bloginfo('template_url'); ?>/js/currency.js" type="text/javascript"></script>

  <script>
    jQuery(document).ready(function($) {
      $('.click_main_search').click(function() {
        var color_c = $(this);
        $('.search_hea').attr('action', color_c.data('url'));
        $('.py-1').attr('name', color_c.data('name'));
        $('.click_main_search').removeClass('for_b_left');
        $('.py-1').attr('placeholder', color_c.data('text'));
        color_c.addClass('for_b_left');
      });


      var width = $(document).width();

      if (width >= 992) {
        width = 992;
      } else if (width >= 768 && width <= 992) {

        width = 768;
      } else {
        width = 300;
      }
      //$(".contact_us").colorbox({iframe:true, innerWidth:width, innerHeight:490});
    });
  </script>

<?php
    if(is_page()){
        $bail_template = basename(get_page_template());
        if($bail_template == "timeline.php"){
            
        }else{
            
?>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script src="https://www.googletagmanager.com/gtag/js?id=UA-65718700-6"></script>

  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-65718700-6');
  </script>

<?php
        }
    }
?>

  <script type="text/javascript">
    var pdfdoc = new jsPDF();
    var specialElementHandlers = {
      '#ignoreContent': function(element, renderer) {
        return true;
      }
    };

    $(document).on('click', '#pdfweb', function() {
      console.log('hi');
      pdfdoc.fromHTML($('.marvelapp-1a').html(), 10, 10, {
        'width': 110,
        'elementHandlers': specialElementHandlers
      });
      pdfdoc.save('First.pdf');
    });

    $(function() {
      $('.notificationicon').click(function() {
        //alert()
        var count = $(this).find('.notify').text();
        if (count > 0) {
            $(this).find('.custom_chaticon i').attr('style', '');
            $(this).find('.custom_chaticon span').css('visibility', 'hidden');
            $(this).find('.notify').hide();
            $.getJSON('<?php echo admin_url('admin-ajax.php'); ?>?action=read_notification', function(data) {});
        }
        $('.notificationtooltips').toggle();
      });
    });
  </script>

  <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.js" type="text/javascript"></script>
  

  <?php wp_head();?>

  <script async="async" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

  <?php if (current_user_can('um_clients')) { ?>
    <style>
      #profile_travcust_display_status {
        display: none;
      }

      .elementor-element-ff4ca19 {
        display: none;
      }
    </style>
  <?php } ?>


  <?php if (is_user_logged_in()) { ?>
    <style>
      .input-group.md-form.form-sm.form-1.pl-0 {
        left: 0px !important;
      }
      .English-logo img {
        height: 49px;
        margin-top: 6px;
      }
    </style>
  <?php } ?>

  <?php if ($_GET['device'] == 'app') { ?>

    <style>
      .mobileapp{
      display: none;
    }
    #header{
      display: none;      
    }
      @media screen and (max-width: 600px) {
        #content {
          margin-top: 0px !important;
        }
      }
    </style>
  <?php } ?>


  <?php if (is_page($page = 'Login')) { ?>
    <style>
      .page-id-20116.um-page-login {
        background: #EAE7F5;
      }
    </style>
  <?php } ?>


</head>

<body id="<?php
          if (is_home() || is_front_page()) {
            echo 'hp';
          }
          ?>" <?php body_class(); ?>>
  <?php
  $url = site_url();
  ?>


  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ4N2X6" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <div id="header" class="pdfcrowd-remove" style="background: white; <?php echo ($_GET['device'] == 'app') ? 'display:none;' : ''; ?>">
    <div class="container-fluid mobilemenucolor" style="background:white;padding-top: 10px;padding-bottom: 10px;">
      <div class="container custom_remove_pd remove_pad">

        <div class="mobile_header_balouch_show">
          <div class="row">
            <div class="for_logo_mobile text-center">
              <?php if (is_user_logged_in()) { ?>
              <a href="https://www.travpart.com/English/timeline/">
                <img class="custom_image_mobile" src="https://www.travpart.com/English/wp-content/uploads/2020/02/travpart-logo.png" alt="travpart">
              </a>
              <?php } else { ?>
              <a href="https://www.travpart.com/">
                <img class="custom_image_mobile" src="https://www.travpart.com/English/wp-content/uploads/2020/02/travpart-logo.png" alt="travpart">
              </a>
              <?php } ?>
            </div>
            <div class="for_menu_mobile text-center">
              <button class="btn custom_menu_btn_mobile">
                MENU
                <i class="fas fa-bars"></i>
              </button>
            </div>
          </div>

          <div class="home_mytime_msg" style="display: none;">
            <div style="padding: 10px;">
              <?php if (is_user_logged_in()) { ?>
              <div class="input-group md-form form-sm form-1 pl-0">
                <div class="input-group-prepend">
                  <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="height: 36px;">
                    <i class="fas fa-search text-white" aria-hidden="true"></i>
                    <!--<span class="dropdown_title">People</span>-->
                  </button>
                  <div class="dropdown-menu custom_d" style="width: 100%;">
                    <a class="dropdown-item click_main_search for_b_left" data-name="friend" data-url="<?php bloginfo('home'); ?>/people" data-text="Search for People" href="#">People</a>
                    <a class="dropdown-item click_main_search" data-name="search" data-url="<?php bloginfo('home'); ?>/toursearch" data-text="Search for Tour Packages" href="#">Tour Packages</a>
                  </div>
                </div>
                <form class="search_hea" action="<?php bloginfo('home'); ?>/people" method="get" style="width: 86% !important;padding: 5px;background: #fff">
                  <input class="form-control my-0 py-1" type="text" placeholder="Search for People" aria-label="Search" name="friend">
                </form>
              </div>
              <?php } ?>
            </div>
            <div class="rows menu_mob_dis">
              <div class="col-mmmd-4">
                <a href="https://www.travpart.com/English/timeline/">
                  <i class="fas fa-home" style="font-size: 30px;"></i>
                  <p>Feed</p>
                </a>
              </div>
              <div class="col-mmmd-4">
                <?php $current_user = wp_get_current_user(); ?>
                <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->user_login; ?>">
                  <i class="fas fa-clock" style="font-size: 30px;"></i>

                  <p>My Timeline</p>
                </a>
              </div>
              <div class="col-mmmd-4">
                <a href="/English/travchat/">
                  <i class="fas fa-envelope" style="font-size: 30px;margin-right:9px"></i>
                  <p>Message</p>
                </a>
              </div>
            </div>
            <div class="rows menu_mob_dis">
              <div class="col-mmmd-4">
                <a href="https://www.travpart.com/English/people/">
                  <i class="fas fa-user-plus" style="font-size: 30px;margin-left:9px"></i>
                  <p>Find People</p>
                </a>
              </div>
              <div class="col-mmmd-4">
                <a href="https://www.travpart.com/English/connection-request-list/">
                  <i class="fas fa-user-friends" style="font-size: 30px;"></i>
                  <?php if (is_user_logged_in()) { ?>
                    <!--<span class="badge" style="position:relative;background: red;color: #fff;font-size: 12px;top: -10px;padding-top: 5px;padding-bottom: 5px;padding-left: 2px;padding-right: 2px;">-->
                    <?php if(getFriendRequestCount(wp_get_current_user()->ID)>0){ ?>
                      <div class="badge" style="position:relative;background: red;color: #fff;font-size: 12px;top: -10px;padding-top: 5px;padding-bottom: 5px;padding-left: 2px;padding-right: 2px;">
                        <?php echo getFriendRequestCount(wp_get_current_user()->ID); ?>
                      </div>
                    <?php } ?>
                  <?php } ?>
                  <p>Connection Request</p>
                </a>
              </div>
            </div>

            <div class="for_border">

              <?php
              $display_name = um_user('display_name');
              $current_user = wp_get_current_user();
              if (is_user_logged_in()) { ?>
                <div class="rows profile_s" style="padding: 5px;">
                  <div class="col-mmd-2">
                    <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>" class="custpm_pr">
                      <?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?>
                    </a>
                  </div>
                  <div class="col-mmd-8">
                    <a href="#" class="pro_slide">
                      <strong>
                        <?php echo $display_name; ?>
                      </strong>
                    </a>
                  </div>
                  <div class="col-mmd-2">
                    <a class="pro_slide" style="color:#fff;font-weight: bold;font-size: 18px;margin-top: -10px;font-family: 'Heebo', sans-serif;padding: 5px 15px !important">
                      V
                    </a>
                  </div>
                </div>
              <?php } ?>

              <div class="hide_pro">

                <div class="rows " style="padding-top: 10px;">
                  <div class="col-mmd-4">
                    <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>">
                      <i class="fas fa-user-circle" style="font-size: 30px;"></i>
                      <p>My Profile</p>
                    </a>
                  </div>
                  <div class="col-mmd-4">
                    <a href="/English/my-connection/?user=<?php echo um_user('display_name'); ?>">
                      <i class="fas fa-undo-alt" style="font-size: 30px;"></i>
                      <p>My Refferal</p>
                    </a>
                  </div>
                  <div class="col-mmd-4">
                    <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=settlement">
                      <i class="fas fa-calendar-check" style="font-size: 30px;"></i>
                      <p>Settlement</p>
                    </a>
                  </div>
                </div>


                <div class="rows ">
                  <div class="col-mmd-4">
                    <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=transaction">
                      <i class="fas fa-dollar-sign" style="font-size: 30px;"></i>
                      <p>Transaction</p>
                    </a>
                  </div>
                  <div class="col-mmd-4">
                    <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=verification">
                      <i class="fas fa-calendar-check" style="font-size: 30px;"></i>
                      <p>Verification</p>
                    </a>
                  </div>
                  <div class="col-mmd-4">
                    <a href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=bookingCodeList">
                      <i class="fas fa-book-open" style="font-size: 30px;"></i>
                      <p>Booking</p>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="rows " style="padding: 10px 0px 0px 0px">
              <div class="col-mmd-4">
                <a href="/English/about-us/">
                  <i class="fas fa-briefcase" style="font-size: 30px;"></i>
                  <p>About</p>
                </a>
              </div>
              <div class="col-mmd-4">
                <a href="https://www.travpart.com/Chinese">
                  <i class="fas fa-language" style="font-size: 30px;"></i>
                  <p>中文</p>
                </a>
              </div>
              <div class="col-mmd-4 header-mobile-curreny">
                <select class="header_curreny form-control search_select">
                  <option full_name="IDR" syml="Rp" value="IDR"> IDR </option>
                  <option full_name="RMB" syml="元" value="CNY"> RMB </option>
                  <option full_name="USD" syml="USD" value="USD"> USD </option>
                  <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                </select>
              </div>


            <?php if (is_user_logged_in()) { ?>
              <div class="col-mmd-4">
                  <a href="https://www.travpart.com/English/logout/?redirect_to=https://www.travpart.com/English/login/" class="btn btn-primary user-logout-btn"><i class="fas fa-sign-out-alt"></i> Logout</a>
              </div>
            <?php } else { ?>
              <div class="col-mmd-4">
                  <a href="/English/login/" class="btn btn-primary user-login-btn"><i class="fas fa-sign-out-alt"></i> Login</a>
              </div>
            <?php } ?>


            </div>
            <?php
            $display_name = um_user('display_name');
            $current_user = wp_get_current_user();
            if (is_user_logged_in()) { ?>
              <div class="for_border">
                <div class="rows profile_s">
                  <div class="col-mmd-2" style="padding-top: 13px;padding-left: 10px;">
                    <a href="#" class="">
                      <i class="fas fa-bell" style="font-size: 23px;"></i>
                      <span class="badge" style="background: #f54949fa;border-radius: 10px;top: -14px;position: relative;left: -5px;padding: 3px 5px;">4</span>
                    </a>
                  </div>
                  <div class="col-mmd-8" style="padding-top: 7px;">
                    <a href="#" class="not_slide">
                      <strong> Notifications </strong>
                    </a>
                  </div>
                  <div class="col-mmd-2" style="padding-top: 7px;">
                    <a class="not_slide" style="color:#fff;font-weight: bold;font-size: 18px;margin-top: -10px;font-family: 'Heebo', sans-serif;padding: 5px 15px !important">
                      V
                    </a>
                  </div>
                </div>



                <div class="rows hide_not" style="padding: 7px;">
                  <div class="notification_div">
                    <div class="min_height">
                      <div class="row_c">

                        <?php echo notifications_display(); ?>

                        <!--<div class="col-mmd-2" style="width: 20%;display: table-cell;vertical-align: middle;text-align: center;">
                  <a href="#">
                    <i class="fas fa-circle" style="color: maroon;font-size: 15px;"></i>
                  </a>  
                </div>
                <div class="col-mmd-8" style="width: 60%;display: table-cell;vertical-align: middle;">
                  <p style="margin: 0px;">Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
                </div>
                <div class="col-mmd-2" style="width: 20%;display: table-cell;vertical-align: middle;text-align: center;">
                  <a href="#">
                    <i class="fas fa-times" style="color: #22b14c;font-size: 15px;"></i>
                  </a>
                </div>-->
                      </div>


                    </div>
                    <div class="see_all_not_dic_anchor" style="text-align: center;padding: 5px;">
                      <a href="https://www.travpart.com/English/see-all-notifications/" style="color: #22b14c;font-size: 14px;font-family: 'Heebo', sans-serif;">See all Notifications.</a>
                    </div>

                  </div>
                </div>

              </div>
            <?php } ?>

          </div>

        </div>


        <div class="row">


          <div id="h-t" class="col-md-12 mobilemenucolor" style="background:white;width:100%;background-size:100%;">


            <div class="row mobile_header_balouch_hide">
              <div class="col-md-2 col-xs-12  ">
                <?php if (is_user_logged_in()) { ?>
                <a href="https://www.travpart.com/English/timeline/">
                  <img src="https://www.travpart.com/English/wp-content/uploads/2020/02/travpart-logo.png" alt="Travcust" width="150" class="mobilenone English-logo">
                </a>
                <?php } else { ?>
                <a href="https://www.travpart.com/">
                  <img src="https://www.travpart.com/English/wp-content/uploads/2020/02/travpart-logo.png" alt="Travcust" width="150" class="mobilenone English-logo">
                </a>
                <?php } ?>
              </div>
              <div class="col-md-4 col-xs-6search" style="padding-top: 1px;padding-left: 30px;">
                 <?php if (is_user_logged_in()) { ?>
                <div class="input-group md-form form-sm form-1 pl-0">
                  <div class="input-group-prepend">
                    <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="basic-text1" style="height: 36px;">
                      <i class="fas fa-search text-white" aria-hidden="true"></i>
                      <!--<span class="dropdown_title">People</span>-->
                    </button>
                    <div class="dropdown-menu" style="width: 100%;">
                      <a style="font-size:12px !important;margin-left:0px; " class="dropdown-item click_main_search for_b_left" data-name="friend" data-url="<?php bloginfo('home'); ?>/people" data-text="Search for People" href="#">People</a>
                      <a style="font-size:12px !important;margin-left:0px; " class="dropdown-item click_main_search" data-name="search" data-url="<?php bloginfo('home'); ?>/toursearch" data-text="Search for Tour Packages" href="#">Tour Packages</a>
                    </div>
                  </div>
                  <form class="search_hea" action="<?php bloginfo('home'); ?>/people" method="get" style="width: 85%;margin: 0px;">
                    <input class="form-control my-0 py-1" type="text" placeholder="Search for People" aria-label="Search" name="friend">
                  </form>
                </div>
                 <?php } ?>
              </div>
              <div class="col-md-6 col-xs-6">

                <div class="mobilephoneonly ">
                  <a href="https://www.travpart.com/English/">
                    <img src="https://www.travpart.com/English/wp-content/uploads/2020/02/travpart-logo.png" alt="travpart" width="150" style="margin-left:-10px"></a></div>
                <div class="main-menu pull-right">
                  <?php
                  ob_start();
                  wp_nav_menu(array('theme_location' => 'main-menu'));
                  $main_menu = ob_get_contents();
                  ob_end_clean();
                  $unread_message_count = getUnreadMessageCount(wp_get_current_user()->user_login);
                  ?>
                  <?php ob_start(); ?>
                  <!--
                                <li class="mobilenone nomobileview mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/about-us/">ABOUT</a></li>
                                <li class="mobilenone nomobileview mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="<?php echo $chat_link  ?>">MESSAGE<?php
                                                                                                                                                                                                                                                                  /* if($unread_message_count>0) {
                                        if($unread_message_count>99)
                                            $unread_message_display='99+';
                                        else
                                            $unread_message_display=$unread_message_count;
                                        echo "<span>{$unread_message_display}</span>";
                                    }*/
                                                                                                                                                                                                                                                                  ?></a></li>  -->
                  <?php if (current_user_can('um_travel-advisor')) { ?>
                    <!--<li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                <a class="mega-menu-link headerfont cqheader" href="https://www.travpart.com/English/customers-request/">CUSTOMER REQUEST</a></li>-->
                  <?php } ?>
                  <?php if (is_user_logged_in()) { ?>
                    <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-link profileicon">
                      <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->user_login; ?>">
                        <?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?>
                      </a>
                    </li>
                  <?php } ?>
                  <div class="dropdown">

                    <div class="dropbtn">
                      <!--  Login start -->
                      <?php if (is_user_logged_in()) { ?>

                        <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout" style="background:white;text-align: center!important;padding:0px">
                          <a style="font-size: 16px;" class="mega-menu-link loginstyle headerfont" href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->user_login; ?>">
                            <?php echo $display_name; ?></a>
                          <!--<a class="mega-menu-link loginstyle" href="<?php echo wp_logout_url(get_permalink()); ?>" class="buttype">LOGOUT <i class="fa fa-caret-down" style="display: inline;"></i></a>-->
                        <?php } else { ?>
                          <!--<a class="mega-menu-link loginstyle headerfont" href="/English/login/" class="buttype" style="background:white;text-align: center!important;padding:10px;display:block">LOGIN</a>-->
                          <style>
                            .dropdown-content {
                              min-width: 300px !important;
                              display: none !important;
                            }
                          </style>
                        <?php } ?>
                        </li>
                        <!--  Login end -->
                    </div>
                    <div class="dropdown-content mobilenone" style="padding: 0px !important;min-width: 330px !important;">

                      <!--  User name start -->
                      <?php //if($_GET['debug']==2){ 
                      ?>
                      <style>
                        .custom_dd {
                          display: none !important;
                        }
                      </style>
                      <div class="home_mytime_msg for_text">

                        <!--<div style="padding: 10px;">
                        <div class="input-group md-form form-sm form-1 pl-0">
                                   <div class="input-group-prepend">
                                      <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="height: 36px;">
                                        <i class="fas fa-search text-white" aria-hidden="true"></i>
                                      
                                      </button>
                                    <div class="dropdown-menu custom_d" style="width: 100%;">
                            <a class="dropdown-item click_main_search for_b_left" data-name="friend" data-url="<?php bloginfo('home'); ?>/people" data-text="Search for People" href="#">People</a>
                            <a class="dropdown-item click_main_search" data-name="search" data-url="<?php bloginfo('home'); ?>/toursearch" data-text="Search for Tour Packages" href="#">Tour Packages</a>
                          </div>
                                    </div>                                   
                                    <form class="search_hea" action="<?php bloginfo('home'); ?>/people" method="get" style="width: 86% !important;background: #fff;margin: 0px;">
                                        <input class="form-control my-0 py-1" type="text" placeholder="Search for People" aria-label="Search" name="friend">
                                    </form>
                                  </div>
                              </div>-->
                        <div class="rows">
                          <div class="col-mmd-4">
                            <a href="https://www.travpart.com/English/timeline/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                              <i class="fas fa-home" style="font-size: 30px;"></i>
                              <p>Feed</p>
                            </a>
                          </div>
                          <div class="col-mmd-4">
                            <?php $current_user = wp_get_current_user(); ?>
                            <a style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; " href="https://www.travpart.com/English/my-time-line/?user=<?php echo $current_user->user_login; ?>">
                              <i class="fas fa-clock" style="font-size: 30px;"></i>

                              <p>My Timeline</p>
                            </a>
                          </div>
                          <div class="col-mmd-4">
                            <a href="/English/travchat/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                              <i class="fas fa-envelope" style="font-size: 30px;"></i>
                              <p>Message</p>
                            </a>
                          </div>
                        </div>


                        <div class="rows">
                          <div class="col-mmd-4">
                            <a href="https://www.travpart.com/English/events-list/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                              <i class="fas fa-calendar-alt" style="font-size: 30px;"></i>
                              <p>Event</p>
                            </a>
                          </div>
                          <div class="col-mmd-4">
                            <a href="https://www.travpart.com/English/videos/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                              <i class="fas fa-play" style="font-size: 30px;"></i>
                              <p>Videos</p>
                            </a>
                          </div>
                          <?php if (current_user_can('um_travel-advisor')) { ?>
                            <div class="col-mmd-4">
                              <a href="https://www.travpart.com/English/customers-request/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                                <i class="fas fa-headset" style="font-size: 30px;"></i>
                                <p>Customer</p>
                              </a>
                            </div>
                          <?php }else{ ?>
                            <div class="col-mmd-4">
                              <a href="https://www.travpart.com/English/TravPlan" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                                <i class="fas fa-map-marked-alt" style="font-size: 30px;"></i>
                                <p>Travplan</p>
                              </a>
                            </div>
                          <?php } ?>
                          
                           <?php if (current_user_can('um_travel-advisor')) { ?>
                            <div class="col-mmd-4">
                              <a href="https://www.travpart.com/English/travcust/" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                                <i class="fas fa-edit" style="font-size: 30px;"></i>
                                <p>Travcust</p>
                              </a>
                            </div>
                            <div class="col-mmd-4 performance_icon">
                              <a href="" style="font-size: 14px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                                <img src="https://www.travpart.com/English/wp-content/uploads/2020/06/performance_icon.png" style="border: none;width: 50px;height: 36px">
                                <p>Performance</p>
                              </a>
                            </div>
                          <?php } ?>
                          
                        </div>
                        <div class="for_border">

                          <?php
                          $display_name = um_user('display_name');
                          $current_user = wp_get_current_user();
                          if (is_user_logged_in()) { ?>
                            <div class="rows profile_s" style="padding: 5px;">
                              <div class="col-mmd-4">
                                <a href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>" style="border: 0px !important;" class="custpm_pr">
                                  <?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?>
                                </a>
                              </div>
                              <div class="col-mmd-4">
                                <a href="#" class="pro_slide" style="font-size: 12px;border: 0px !important;line-height: 30px;color: #fff !important;text-transform: capitalize; ">
                                  <strong>
                                    <?php echo $display_name; ?>
                                  </strong>
                                </a>
                              </div>
                              <div class="col-mmd-4">
                                <a class="pro_slide" style="cursor: pointer;color:#fff;font-weight: bold;font-size: 18px;font-family: 'Heebo', sans-serif;padding: 5px 15px !important;border: 0px !important;color: #fff !important;">
                                  V
                                </a>
                              </div>
                            </div>
                          <?php } ?>

                          <div class="hide_pro">
                            <div class="rows " style="padding-top: 10px;">
                              <div class="col-mmd-4">
                                <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/user/<?php echo um_user('display_name'); ?>">
                                  <i class="fas fa-user-circle" style="font-size: 30px;"></i>
                                  <p>My Profile</p>
                                </a>
                              </div>
                              <div class="col-mmd-4">
                                <a style="color: #fff !important;border: 0px !important;" href="/English/my-connection/?user=<?php echo um_user('display_name'); ?>">
                                  <i class="fas fa-undo-alt" style="font-size: 30px;"></i>
                                  <p>My Refferal</p>
                                </a>
                              </div>
                              <div class="col-mmd-4">
                                <a style="color: #fff !important;border: 0px !important;" href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=settlement">
                                  <i class="fas fa-calendar-check" style="font-size: 30px;"></i>
                                  <p>Settlement</p>
                                </a>
                              </div>
                            </div>


                            <div class="rows ">
                              <div class="col-mmd-4">
                                <a style="color: #fff !important;border: 0px !important;" href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=transaction">
                                  <i class="fas fa-dollar-sign" style="font-size: 30px;"></i>
                                  <p>Transaction</p>
                                </a>
                              </div>
                              <div class="col-mmd-4">
                                <a style="color: #fff !important;border: 0px !important;" href="/English/user/<?php echo um_user('display_name'); ?>/?profiletab=verification">
                                  <i class="fas fa-calendar-check" style="font-size: 30px;"></i>
                                  <p>Verification</p>
                                </a>
                              </div>
                              <div class="col-mmd-4">
                                <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/ticket/">
                                  <i class="fas fa-info-circle" style="font-size: 30px;"></i>
                                  <p>OPEN TICKETS</p>
                                </a>
                              </div>

                            </div>

                            <div class="rows ">
                              <div class="col-mmd-4">
                                <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=bookingCodeList">
                                  <i class="fas fa-book-open" style="font-size: 30px"></i>
                                  <p>BOOKING</p>
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="rows " style="padding: 10px 0px 0px 0px">
                          <!--<div class="col-mmd-4">
                        <a style="color: #fff !important;border: 0px !important;" href="/English/about-us/">
                        <i class="fas fa-briefcase" style="font-size: 30px;"></i>
                        <p>About</p>
                        </a>  
                      </div>-->
                          <div class="col-mmd-4">
                            <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/Chinese">
                              <i class="fas fa-language" style="font-size: 30px;"></i>
                              <p>中文</p>
                            </a>
                          </div>
                          <div class="col-mmd-4">
                            <a style="color: #fff !important;border: 0px !important;" href="https://www.travpart.com/English/contact/">
                              <i class="fas fa-phone" style="font-size: 30px;"></i>
                              <br>
                              CONTACT
                            </a>
                          </div>
                          <div class="col-mmd-4">
                            <select class="header_curreny form-control search_select">
                              <option full_name="IDR" syml="Rp" value="IDR"> IDR </option>
                              <option full_name="RMB" syml="元" value="CNY"> RMB </option>
                              <option full_name="USD" syml="USD" value="USD"> USD </option>
                              <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                            </select>
                          </div>
                        </div>

                        <div class="for_border">



                          <?php if (is_user_logged_in()) { ?>
                            <a href="https://www.travpart.com/English/logout/?redirect_to=https://www.travpart.com/English/login/" class="btn btn-primary" style="color: #fff !important;border: 0px !important;font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;text-align: left !important;padding-left: 10px !important;"><i class="fas fa-sign-out-alt"></i> Logout</a>
                          <?php } else { ?>
                              <a href="/English/login/" class="btn btn-primary" style="color: #fff !important;border: 0px !important;font-size: 18px;text-transform: uppercase;letter-spacing: 1px;;background: #368a8c !important;font-family: 'Heebo', sans-serif;text-align: left !important;padding-left: 10px !important;"><i class="fas fa-sign-out-alt"></i> Login</a>
                          <?php } ?>
                        </div>

                      </div>

                      <?php //} 
                      ?>

                      <li class="mega-menu-item custom_dd mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                        <?php
                        $display_name = um_user('display_name');
                        $current_user = wp_get_current_user();
                        if (is_user_logged_in()) {
                        ?>
                          <strong>
                            <p style="margin-left: 30px; color: black!important;line-height: 2.5em!important;margin-bottom:0px !important;" class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/">
                              <?php echo $display_name; ?></p>
                          </strong><br>
                          <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=profile"><i class="fas fa-user-circle"></i> MY PROFILE
                          </a><br>
                          <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=referal"><i class="fas fa-undo-alt"></i> REFERRAL
                          </a><br>
                          <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=settlement"><i class="fas fa-calendar-check"></i> SETTLEMENT
                          </a><br>
                          <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=transaction"><i class="fas fa-dollar-sign-sign"></i> TRANSACTION
                          </a><br>
                          <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=verification"><i class="fas fa-calendar-check"></i> VERIFICATION
                          </a><br>
                          <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=bookingCodeList"><i class="fas fa-book-open"></i> BOOKING
                          </a><br>
                          <a class="mega-menu-link" href="https://www.travpart.com/English/ticket/"><i class="fas fa-info-circle"></i> OPEN TICKETS</a><br>
                          <a class="mega-menu-link" href="https://www.travpart.com/English/contact/"><i class="fas fa-phone"></i> CONTACT
                          </a>
                        <?php } else { ?>
                        <?php } ?>
                      </li>
                      <!--  User name end -->
                    </div>
                    <div class="mobileonly">
                      <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                        <table style="width:100%;z-index:99999" class="mobilemenutable">
                          <tr style="background:#3b898a!important">
                            <td><a class="mega-menu-link" href="https://www.travpart.com/English/" style="margin-left:9px"><i class="fas fa-home" style="font-size: 30px;margin-left:9px"></i>
                                <div style="margin-left:9px">HOME
                              </a>
                    </div>
                    </td>
                    <td><a class="mega-menu-link" href="https://www.travpart.com/English/about-us/" style="margin-left:22px"><i class="fas fa-briefcase" style="font-size: 30px;margin-left:9px"></i>
                        <div style="margin-left:9px">ABOUT
                      </a>
                  </div>
                  </td>
                  <td><a class="mega-menu-link mega-menu-msglink" href="<?php echo $chat_link  ?>" style="margin-left:9px"><i class="fas fa-envelope" style="font-size: 30px;margin-right:9px"></i>
                      <div style="margin-right:9px">MESSAGE
                        <?php
                        if ($unread_message_count > 0) {
                          echo "<span>{$unread_message_display}</span>";
                        }
                        ?>
                    </a>
                </div>
                </td>
                </tr>
                </table>

                <!--  User name start -->
                <?php
                $display_name = um_user('display_name');
                $current_user = wp_get_current_user();
                if (is_user_logged_in()) {
                ?>

                  <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                    <table style="width:90%" class="mobilemenutable">
                      <tr>
                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=profile"><i class="fas fa-user-circle" style="font-size: 30px;"></i><br>
                            MY PROFILE
                          </a><br></td>
                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=referal"><i class="fas fa-undo-alt" style="font-size: 30px;"></i><br>MY REFERRAL
                          </a></td>
                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=settlement"><i class="fas fa-calendar-check" style="font-size: 30px;"></i><br> SETTLEMENT
                          </a></td>
                      </tr>
                      <tr>
                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=transaction"><i class="fas fa-dollar-sign-sign" style="font-size: 30px;"></i><br> TRANSACTION
                          </a></td>
                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=verification"><i class="fas fa-calendar-check" style="font-size: 30px;"></i><br> VERTIFICATION
                          </a></td>
                        <td><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login; ?>/?profiletab=bookingCodeList"><i class="fas fa-book-open" style="font-size: 30px;"></i><br> BOOKING
                          </a></td>
                      </tr>
                      <tr>
                        <td>
                          <a class="mega-menu-link" href="https://www.travpart.com/English/ticket/"><i class="fas fa-phone" style="font-size: 30px;"></i><br> CONTACT
                          </a></td>
                        <td>
                          <a class="mega-menu-link" href="https://www.travpart.com/Chinese"><i class="fas fa-language" style="font-size: 30px;"></i><br>中文</a>
                  </li>
                  </td>
                  <td>
                    <a class="mega-menu-link" href="http://www.travpart.com/tutorial/Partner_s_Guidance.pdf" target="_blank" class="mega-menu-link"><i class="fas fa-file-pdf" style="font-size: 30px;color:#e63c60"></i><br> TUTORIAL</li>
                  </td>
                  </tr>
                  <tr>
                    <td colspan="2" style="color:white;text-align: left;font-size:15px">Change Currency:</td>
                    <td>
                      <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                        <select class="header_curreny search_select" style="padding:0px;width:70px;height:40px;background-image:none;display:block;font-size:20px">

                          <option full_name="IDR" syml="Rp" value="IDR"> IDR </option>
                          <option full_name="RMB" syml="元" value="CNY"> RMB </option>
                          <option full_name="USD" syml="USD" value="USD"> USD </option>
                          <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                        </select>
                      </li>
                    </td>
                  </tr><?php if (current_user_can('um_travel-advisor')) { ?>
                    <tr>
                      <td colspan="3" style="width: 100%">
                        <p class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                          <a class="mega-menu-link headerfont cqheader" href="https://www.travpart.com/English/customers-request/">CUSTOMER REQUEST</a></p>
                      </td>
                    </tr> <?php } ?>

                  <tr>
                    <td colspan="3">
                      <a href="https://www.travpart.com/mobileapp/">
                        <div style="border:1px solid white;background:#5bbc2e;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;padding-bottom:10px;color:white;text-align: left;font-size:18px"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/android.png" alt="Download our app" class="androidicon">
                          Download our mobile app</div>
                      </a></td>
                  </tr>
                  </table>
                <?php } else { ?>
                <?php } ?>
                </li>
              </div>
            </div>
            <?php if (!is_user_logged_in()) { ?>
              <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                <a class="desktop_menu" href="https://www.travpart.com/English/login">LOGIN</a>
              </li>
              <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                <a class="desktop_menu" href="https://www.travpart.com/English/register2/">REGISTER</a>
              </li>
              <li class="mega-menu-link">
                <a href="https://www.travpart.com/English/contact/"><span style="margin-left: 10px;padding:5px;background: #37a000 !important;">Contact us</span></a>
              </li>
              <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                <a class="desktop_menu" href="https://www.travpart.com/Chinese">
                  <img src="/English/wp-content/themes/bali//images/flg-cn.png" alt="中文" class="mobilenotshow"> 中文
                </a>
              </li>
              <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">

                <select class="header_curreny form-control search_select" style="background: none;margin:0px 0px 0px 8px;">

                  <option full_name="IDR" syml="Rp" value="IDR"> IDR </option>
                  <option full_name="RMB" syml="元" value="CNY"> RMB </option>
                  <option full_name="USD" syml="USD" value="USD"> USD </option>
                  <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                </select>

              </li>

            <?php } ?>

            <?php if (is_user_logged_in()) { ?>
              <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                <a class="desktop_menu" href="https://www.travpart.com/English/timeline">Feed</a>
              </li>
              <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                <a class="desktop_menu" href="http://travpart.com/english/people">Find People</a>
              </li>
              <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout marg_right">
                <a href="https://www.travpart.com/English/connection-request-list" class="custom_ficon desktop_menu" style="color: #22b14c !important;font-size: 20px !important;">
                  <?php $countreq = getFriendRequestCount(wp_get_current_user()->ID); ?>
                  <?php if ($countreq > 0) { ?>
                    <i class="fas fa-user-friends" style="color: #1a2947;"></i>

                    <!--<span class="badge" style="position:relative;background: red;color: #fff;font-size: 10px;border-radius: 50%;top: -10px;left: -6px;">
                      <?php //echo getFriendRequestCount(wp_get_current_user()->ID); ?>
                    </span>-->
                    <div class="notify" >
                      <?php echo getFriendRequestCount(wp_get_current_user()->ID); ?>
                    </div>
                  <?php } else { ?>
                    <i class="fas fa-user-friends" style="padding-left: 2px;padding-right: 2px"></i>
                  <?php  } ?>

                </a>
              </li>
              <?php //} 
              ?>

              <?php //if(is_user_logged_in()) { 
              ?>
              <li class="mega-menu-item marg_right mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                <a href="https://www.travpart.com/English/travchat/" class="custom_chaticon" style="color: #22b14c !important;font-size: 20px !important;">
                  <?php $countmsg = getUnreadMessageCount(wp_get_current_user()->ID); ?>
                  <?php if ($countmsg > 0) { ?>
                    <i class="fas fa-comment-alt" style="color: #1a2947;"></i>

                    <span class="badge" style="position:relative;background: red;color: #fff;font-size: 10px;border-radius: 50%;top: -10px;left: -6px;">
                      <?php echo getUnreadMessageCount(wp_get_current_user()->ID); ?>
                    </span>
                  <?php } else { ?>
                    <i class="fas fa-comment-alt" style="padding-left: 2px;padding-right: 2px"></i>
                  <?php  } ?>

                </a>
              </li>
            <?php } ?>
            <?php
            if (is_user_logged_in()) {
              $notifications_count = getNotificationcount(wp_get_current_user()->ID);
            ?>
              <li class="mega-menu-item marg_right mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout notificationicon mobilenone" style="position: relative;">
                <a href="#" class="custom_chaticon" style="color: #22b14c !important;font-size: 20px !important;">
                  <i class="fas fa-bell" <?php echo $notifications_count > 0 ? 'style="color: #1a2947;"' : ''; ?>></i>
                  <div class="notify" style="background: red !important;border-radius: 50%;color:#fff; <?php echo $notifications_count > 0 ? '' : 'visibility:hidden'; ?>">
                    <?php echo $notifications_count; ?>
                  </div>
                </a>
                <div class="notificationtooltips" style="display:none;">
                  <div class="arrow-up"></div>
                  <div id="nheading">
                    <div class="nheading-left">
                      <h6 class="nheading-title">Notifications<span>
                        </span></h6>
                    </div>
                    <div class="nheading-right">
                      <a class="notification-link" href="https://www.travpart.com/English/see-all-notifications/">See all</a>
                    </div>
                  </div>
                  <ul class="notification-list">
                    <?php
                    $notifications = getNotification(wp_get_current_user()->ID);
                    foreach ($notifications as $row) {
                    ?>
                      <li class="notification-item" v-for="user of users">
                        <div class="user-content">
                          <p class="user-info" style="color: black"><?php echo str_replace("''", '',$row['content']); ?></p>
                        </div>
                      </li>
                    <?php } ?>
                  </ul>
                </div>
              </li>

            <?php } ?>

            <!--<li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                    <select class="header_curreny search_select" style="padding:0px;height:20px;background-image:none;">

                                        <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
                                        <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
                                        <option full_name="USD" syml="USD" value="USD"> USD </option>
                                        <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                                    </select>
                                </li>-->


            <?php
            global $user_login, $current_user;
            get_currentuserinfo();
            $user_info = get_userdata($current_user->ID);
            $roles = array(
              'administrator',
              'um_travel-advisor',
            );

            if (is_user_logged_in() && array_intersect($roles, $user_info->roles)) {

              //echo '<li class="mega-menu-link changecolor mobiletravcustli"><a href="https://www.travpart.com/English/travcust/"><span style="padding:5px" class="mobiletravcust">Create a tour package</span></a></li>';

              //} else {

              //echo '<li class="mega-menu-link changecolor mobiletravcustli"><a href="https://www.travpart.com/English/contact/"><span style="padding:5px" class="mobiletravcust"></span></a></li>';

            }
            ?>

            <?php
            $append_menu_items = ob_get_contents();
            ob_end_clean();

            $main_menu = substr(trim($main_menu), 0, strlen($main_menu) - 11);
            if ($unread_message_count > 0) {
              if ($unread_message_count > 99)
                $unread_message_count = '99+';
              $main_menu = str_replace('Message', "Message <span>{$unread_message_count}</span>", $main_menu);
            }
            echo $main_menu . $append_menu_items . '</ul></div></div>';
            ?>
          </div>
        </div>
      </div>





    </div>
  </div>
  </div>
  </div>
  <div class="container-fluid">


    <!--<div class="navbar navbar-default" role="navigation">
                                      <div class="navbar-header">
                                        <button class="navbar-toggle" data-target="#menu-lnk" data-toggle="collapse" type="button">
                                              <span class="icon-bar"></span>
                                              <span class="icon-bar"></span>
                                              <span class="icon-bar"></span>
                                          </button>
                                      </div>
                                    </div>
                                    </div>-->
    <!--  <div class="navbar-collapse collapse" id="menu-lnk">
                                     <ul class="nav navbar-nav">
                                                                 
                                <?php /* $parents=array();
                                  $items = wp_get_nav_menu_items("3");
                                  $child = wp_get_nav_menu_items("3");

                                  foreach ( $items as $item ) {
                                  if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
                                  $parents[] = $item->menu_item_parent;
                                  }
                                  }
                                  foreach($items as $item): */ ?><!--

                                <?php /* if ( $item->menu_item_parent==0 ) {  */ ?>
<li >
<a href="<?php /* echo $item->url; */ ?>"  <?php /* if ( in_array( $item->ID, $parents ) ) { */ ?> class="dropdown-toggle" data-toggle="dropdown" <?php /* } */ ?>><?php /* echo $item->title; */ ?></a>
                                <?php /* }
                                  if ( in_array( $item->ID, $parents ) ) { */ ?>
 <ul class="dropdown-menu">
                                <?php /* foreach ( $child as $child_row ) {
                                  if($child_row->menu_item_parent==$item->ID){
                                  echo " <li><a href='".$child_row->url."'>".$child_row->title."</a></li> ";
                                  }}
                                 */ ?>
</ul> 
                                <?php /* } */ ?> </li>
                                --><?php /*
                                  endforeach; */
                                    ?>
    <!--                             <li class="last" style="margin-top:18px;"><a class='contact_us' href="--><?php //bloginfo('url');    
                                                                                                              ?>
    <!--/contact-form">Customize journey</a></li>   -->
    <!--                                -->
    <!--                            </ul>-->
    <!--                        </div> -->
    <!--                    </div>-->


  </div>

  <?php
  /* if( is_home() || is_front_page() || is_page('14047')) {
              echo '<div id="search_price_box" class="container">';
              echo '<div id="search_price_box_form">';
              echo search_price_box();
              // echo search_nontour_box();

              echo '</div>';
              echo '</div>';
              }
              if(is_home() || is_front_page())
              {
              echo '<div id="search_price_box" class="container">';
              echo '<div id="search_price_box_form2">';
              //echo search_price_box();
              echo search_nontour_box();

              echo '</div>';
              echo '</div>';
              } */

  if (is_page('user')) {
    echo ('<script type="text/javascript" src="' . get_stylesheet_directory_uri() . '/js/my-great-script.js"> </script>');
  }
  ?>

  <?php
  // echo do_shortcode('[shubhshort]'); //for display
  /*
              if(is_home() || is_front_page()) {
              putRevSlider( 'homepage' );
              } */
  ?>
  <div id="content ">
    <div class="container for_remove_ad">
      <div class="row">
        <div class="col-md-12">

          <?php
          $pagename = get_query_var('pagename');
          if (!is_home() && !is_front_page() && $pagename != "login" && $pagename != "register2" && $_GET['device'] != 'app') { ?>
            <div class="row">
              <div class="col-md-12">
                <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
              </div>
            </div>
          <?php } ?>


<?php
  if (is_page()) {
    $bail_template = basename(get_page_template());
    if ($bail_template == "tpl-mytimeline.php") {
    } else {

  ?>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script> 
  <?php
    }
  }
  ?>
  <style type="text/css">
    section.elementor-element.elementor-element-a803033 {
        display: none;
      }
    .performance_icon a:hover{
      text-shadow: 5px 5px 10px #383838;
      cursor: pointer;
    }
  </style>