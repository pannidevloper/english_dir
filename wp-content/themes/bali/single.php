<?php

global $wpdb;
session_start();
get_header();
wpb_set_post_views(get_the_ID());
$author_id=$wpdb->get_var("SELECT meta_value FROM `wp_tourmeta` WHERE tour_id=".get_post_meta(get_the_ID(), 'tour_id', true)." AND meta_key='userid'");
if(!empty($author_id) && $author_id!=-1) {
  $authordata = get_user_by('id', $author_id);
}
$tour_id = get_post_meta(get_the_ID(), 'tour_id', true);
$userinfo = $wpdb->get_row("SELECT `userid`,`photo`,`country`,`region`,`timezone` FROM `user` WHERE `username`='{$authordata->user_login}'");
$rattingDataByTour = $wpdb->get_results("SELECT * FROM agent_feedback WHERE af_agent_user_name = '{$authordata->user_login}'", ARRAY_A);

$avatar = um_get_user_avatar_data($authordata->ID);
if(empty($userinfo->photo)){
  $agent_avatar = home_url() . "/travchat/upload/profile.jpg";
  if(!empty($avatar['url'])){
    $agent_avatar= $avatar['url'];
  }
}
else{
  $agent_avatar = home_url() . '/travchat/' . $userinfo->photo;
}
global $data;
if (!empty($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=" . (int)$tour_id)->meta_value)) {
 	$data = unserialize($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=" .$tour_id)->meta_value);
}

$car_type = $data['car_type'];
$eaiitem = $data['eaiitem'];
$meal_type= $data['meal_type'];
$restaurant_name= $data['restaurant_name'];
$restaurant_img= $data['restaurant_img'];
$tourlist_name = $data['tourlist_name'];
$tourlist_img = $data['tourlist_img'];
$place_name = $data['place_name'];
$place_img = $data['place_img'];
$spa_type = $data['spa_type'];

function unescape($str) {
    $i = 0;
    do {
        $str = unescapecall($str);
        $i++;
    } while ((stripos($str, '%25') !== FALSE || stripos($str, '%2C') !== FALSE) && $i < 8);
    return $str;
}


function unescapecall($str) {
    $ret = '';
    $len = strlen($str);
    for ($i = 0; $i < $len; $i ++) {
        if ($str[$i] == '%' && $str[$i + 1] == 'u') {
            $val = hexdec(substr($str, $i + 2, 4));
            if ($val < 0x7f)
                $ret .= chr($val);
            else
            if ($val < 0x800)
                $ret .= chr(0xc0 | ($val >> 6)) .
                        chr(0x80 | ($val & 0x3f));
            else
                $ret .= chr(0xe0 | ($val >> 12)) .
                        chr(0x80 | (($val >> 6) & 0x3f)) .
                        chr(0x80 | ($val & 0x3f));
            $i += 5;
        } else
        if ($str[$i] == '%') {
            $ret .= urldecode(substr($str, $i, 3));
            $i += 2;
        } else
            $ret .= $str[$i];
    }
    return $ret;
}  	
$totalRatinng = 0;
$avgRatting = 0;
$totalEntry = 0;
if (!empty($rattingDataByTour)) {
    $totalEntry = count($rattingDataByTour);
}
foreach ($rattingDataByTour as $ratings) {
    $totalRatinng = $totalRatinng + $ratings['af_ratting'];
    $avgRatting = $totalRatinng / $totalEntry;
}
$avgRatting = ceil($avgRatting);

$msg_table = $wpdb->prefix . 'chat_messages';
$row = $wpdb->get_row( "SELECT * FROM $msg_table WHERE type=0 ORDER BY RAND() LIMIT 1" );
if(!empty($row)):
	 $MsgId = $row->id;
     $Msg = $row->msg;
 else:
     $Msg="Hi, can I help you?<br/>We have a limited discount for you. Would you like to know more?";
 endif;
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/chatbox.css?r2" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css" />
<!--<link href="<?php bloginfo('template_url'); ?>/bootstrap.css" rel="stylesheet"  type="text/css" />-->

<script src="<?php bloginfo('template_url'); ?>/js/chatbox.js?v7"></script>
<script src="<?php bloginfo('template_url'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/sss.min.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/sssilder.css" type="text/css" media="all" />

<script type="text/javascript">
jQuery(function($) {
	$('.slider').sss();
	$('.slider2').sss();
	$('.slider_3').sss();
});


	jQuery(document).ready(function($){
	  
	  $('.owl-carousel_2').owlCarousel({
		    loop:false,
		    margin:15,
		    responsiveClass:true,
		    nav:true,
		    responsive:{
		        0:{
		            items:2,
		            nav:true
		        },
		        600:{
		            items:3,
		            nav:false
		        },
		        1000:{
		            items:6,
		            nav:true,
		            loop:false
		        }
		    }
		})
		  
		  
		  $('.owl-carousel_1').owlCarousel({
		    loop:true,
		    margin:10,
		    responsiveClass:true,
		    nav:true,
		    responsive:{
		        0:{ 
		            items:2, 
		            nav:true
		        },
		        600:{
		            items:3,
		            nav:false
		        },
		        1000:{
		            items:4,
		            nav:true,
		            loop:false
		        }
		    }
		});
		$('.you_may_hide').hide();
		var a=true;
		  $('.contentimage').on('scroll', function() {
	        if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
	           if(a==true){
			   	console.log('end reached');
	        	$('.you_may_hide').show();
	        	a=false;
			   }
	        }
	    })
	    
		$(window).scroll(function() {
		   if($(window).scrollTop() + $(window).height() == $(document).height()) {
		       $(".post-bookingcode-btn").css('position','initial');
		   }else{
		       //$(".post-bookingcode-btn").css('position','fixed');		   	
		   }
		});
	});
</script>
<style>
.widget-user-bar{
	display: none;
}
#content{
	background: #fff;
}
.input-group-prepend .dropdown-toggle:after {
    display: inline-block;
    vertical-align: middle;
    font-weight: 900;
}
.you_may_hide{
	transition: 1s ease;
}
.featured-img-wrap{
	margin-bottom: 5px !important;
}
.contentimage {
    background: #fff;
    height: 698px;
    overflow-y: scroll;
}
.display_overviewdiv{
	text-align: center;
    padding: 30px 0px;
}
.overview_div{
	background: #fff;
	padding: 10px 0px;
    padding-bottom: 0px;
	margin-bottom: 5px;
	border: 2px solid #1a6d84;
}
img.ssslide{
	width:auto;
	height: 100px;
}
.display_overviewdiv a{
	background-color: white;
    color: #22b14c;
    border: 1px solid #22b14c;
    opacity: 0.8;
    border-radius: 2px;
    padding: 8px 15px;
    font-size: 10px;
    letter-spacing: 1px;
}
.display_overviewdiv h3{
	color:#00518f;
	font-weight: 600;
	margin-bottom: 10px;
}
.display_block p a{
	color: #000;
	text-decoration: none;
}
.display_block p{
	margin-top: 10px;
}
.display_block{
	text-align: center;
    font-size: 12px;
    font-weight: bold;
    text-transform: uppercase;
}
.display_block img{
	max-width: 100%;
	height: 100px;
}
.owl-carousel_1 .owl-item img {
    display: block;
    width: auto;
    height: 200px;
    margin: auto;
}
.owl-nav{
	position: absolute;
    top: 35%;
    left: 0;
    display: block;
    width: 100%;
    font-size: 30px;
}
.owl-prev{
    width: 3%;
    float: left;
    border-radius: 100%;
    background: #fff !important;
    margin-left: -16px !important;
    box-shadow: 0px 0px 4px 1px #a29b9b78;
    font-size: 22px !important;
    height: 33px;
}
.owl-next{
	float: right;
    width: 3%;
    border-radius: 100%;
    background: #fff !important;
    margin-right: -16px !important;
    box-shadow: 0px 0px 4px 1px #a29b9b78;
    font-size: 22px !important;
    height: 33px;
}
.you_title{
	font-weight: bold;
}
.custom_related_products img{
	border-radius: 5px 5px 0px 0px;
}
.custom_related_products{
	background: #fff;
	border-radius: 5px;
	box-shadow: 0px 0px 4px 1px #a29b9b78;
}
.content_related_products{
	padding:10px;
}
.title_div {
    height: 40px;
}
.title_div a{
	color: #000;
	text-decoration: none;
	font-size: 14px;
}
.price_div{	
	font-size: 14px;
	color:#42a5f5;
	font-weight: bold; 
	margin-top: 20px;
  padding: 10px;
}
.post-bookingcode td.td_bali_trid {
    text-align: center !important;
}
.post-bookingcode td.td_bali_price span {
    color: #0ea5ea;
    text-align: center;
}
td.bookingcodeviewdetail a {
    background-color: white;
    color: #22b14c !important;
}
td.bookingcodeviewdetail {
    text-align: center !important;
}
    @media only screen and (max-width: 600px) {
    	.tour_overview img,.optionalact_overview img,.trans_overview img{
			max-width: 90% !important;
		}
		.owl-carousel_1 .owl-nav{
			display:none;
		}
		.owl-carousel_1 .owl-item img{
			width: 100% !important;
		}
		.post-bookingcode-btn{
			position: fixed;
			bottom: 0;
			margin: 0px !important;
			left: 0;
			width: 100% !important;
			z-index: 9999;
		}
		.owl-carousel_2 .owl-prev{
			margin-left: 0px !important;
			background: none !important;
			box-shadow: none;
		}
		.owl-carousel_2 .owl-next{
			margin-right: 0px !important;
			background: none !important;
			box-shadow: none;
		}
    }
    
    @media only screen and (max-width: 450px) {
	    .tour_overview img,.optionalact_overview img,.trans_overview img{
			max-width: 90% !important;
		}
	      .single_content img, .single_content iframe{
	        height: 50% !important;
	      }
      .post-bookingcode-btn {
        background: #f6f6f6;
      }
      img.vocher_banerr {
        height: 100px !important;
      }
    }
    .vouchers-banner i {
    padding: 0px !important;
    }
    a.getinfobtn i.fa-info-circle {
    right: 9%;
    top: -70px;
    color: #0a2727 !important;
    }
    @media only screen and (max-width: 600px) {
      a.getinfobtn i.fa-info-circle {
        right: 8%;
        top: -87px;
        color: #0a2727 !important;
      }
      .singleinput0 label {
        margin-top: 0px !important;
      }
      .singleinput0 label span {
        margin-top: 8px!important;
      }
    }
    @media only screen and (max-width: 280px) {
		.display_block img {
		    max-width: 100%;
		}
		.overview_div{
			text-align: center;
		}
    }
    .clear
    {
        clear: both;
        width: 100%;
        height: auto;
    }
    .fa-star-unchecked{
        color: black;
    }
    .vote-inner {
      display: flex;
      justify-content: center;
      max-width: 500px;
      margin-left: auto;
      margin-right: auto; }
    .vote-inner > div {
      text-align: center;
      cursor: pointer;
      padding: 10px;
      border-right: 1px solid rgba(192, 195, 202, 0.22);
      flex: 1 1 auto; }
    .vote-inner > div:last-child {
      border: none; }
    .vote-inner > div.views {
      cursor: default; }
    .vote-inner > div label {
      cursor: pointer;
    }
    /*.vote-inner > div:not(.views):hover i {
      color: rgb(192,195,202) !important;
    }*/
    .vote-inner > div input {
      opacity: 0!important;
      position: absolute; }
    .vote-inner > div i {
      display: block;
      font-size: 36px;
      color: #c0c3ca;
      height: 50px; }
    .vote-inner > div:not(.views):hover i {
      color:#1a6d84;
    }
    .vote-inner > div span {
      display: block;
      font-size: 12px;
      font-weight: bold;
      color: #333333;
       }
    .vote-inner > div span span {
      font-size: 16px;
      font-weight: normal;
      color: #999999; }

    .commentbox {
      position: relative;
      padding: 12px 24px 12px 104px;
      border: solid 1px #d2d2d2;
      border-bottom: none;
      border-top: none; }
    .commentbox:first-child {
      border-top: 1px solid #d2d2d2; }
    .commentbox .c-name {
      font-size: 18px;
      color: #333; }
    .commentbox .c-date {
      font-size: 14px;
      color: #999999; }
    .commentbox .c-txt {
      font-size: 14px;
      font-weight: normal;
      color: #666666; }
    .commentbox .comment-thumb {
      position: absolute;
      left: 24px;
      top: 12px;
      width: 60px;
      height: 60px;
      border-radius: 50%;
      background-size: cover;
      background-position: center center; }

    .commentbox .comment-reply {
      border: none;
      padding-left: 80px;
    }

    .commentbox .comment-reply .comment-thumb {
      left: 0px;
    }

    .comment-section {
      max-width: 460px;
      margin-left: auto;
      margin-right: auto; 
      }

    .commentbox_wrapper hr {
      margin: 0 !important;
      border-top: solid 1px #d2d2d2; }

    .comment-title {
      font-size: 24px;
      font-weight: bold;
      color: #333333;
      margin-bottom: 15px;
    }

    .comment-form-outer textarea {
      width: 100%;
      max-width: 400px;
    }

    .comment-form-outer .btn-default {
      margin-top: 10px;
      margin-bottom: 10px;
      color: #368a8b; }

    .singe-post-nav {
      max-width: 960px;
      margin-left: auto;
      margin-right: auto;
      margin-top: 30px;
      margin-bottom: 90px;
      clear: both; }
    .singe-post-nav:before, .singe-post-nav:after {
      content: '';
      clear: both; }
    .singe-post-nav .pull-left a {
      display: block;
      text-indent: -9999em;
      position: relative; }
    .singe-post-nav .pull-left a:before {
      text-indent: 0;
      position: absolute;
      top: -5px;
      left: 0;
      content: "\f060";
      font-family: "Font Awesome 5 Free";
      font-weight: 900;
      font-size: 24px;
      color: #226d82; }
    .singe-post-nav .pull-left a:after {
      content: "Previous";
      text-indent: 0;
      position: absolute;
      left: 24px;
      font-size: 36px;
      font-weight: 300;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.67;
      letter-spacing: normal;
      color: #333333; }
    .singe-post-nav .pull-right a {
      display: block;
      text-indent: -9999em;
      position: relative;
      width: 120px; }
    .singe-post-nav .pull-right a:before {
      text-indent: 0;
      position: absolute;
      top: -5px;
      left: 80px;
      content: "\f061";
      font-family: "Font Awesome 5 Free";
      font-weight: 900;
      font-size: 24px;
      color: #226d82; }
    .singe-post-nav .pull-right a:after {
      content: "Next";
      text-indent: 0;
      position: absolute;
      left: 0px;
      font-size: 36px;
      font-weight: 300;
      font-style: normal;
      font-stretch: normal;
      line-height: 0.67;
      letter-spacing: normal;
      color: #333333; }
      .contentimage img{width: 100%}
	  .contentimage{
	  	background: #fff;
	  }	
      .post-bookingcode-btn{
        text-align: center;
        margin-top: 15px;
        margin-left: 52px;
        width: 60%;
        background: #22b14c;
        cursor: pointer;
        padding: 5px;
        font-size: 20px;
        margin-bottom: 10px;
        box-shadow: 0 0 0 5px hsl(0, 0%, 87%);
      }      

      .post-bookingcode-btn a.book-now-btn{
        border: none;
        color: #fff;
      }
      
      .post-bookingcode table{
        width: 100%
      }

      .c-vote span.c-like, .c-vote span.c-dislike{
        margin-right: 20px;
      }
      .c-vote i{
        color: #c0c3ca;
      }
    
@keyframes click-wave {
  0% { height: 20px; width: 20px; opacity: 0.35; position: relative; }
  100% { height: 200px; width: 200px; margin-left: -80px; margin-top: -80px; opacity: 0; }
}
.option-input {
        -webkit-appearance: none;
        -moz-appearance: none;
        -ms-appearance: none;
        -o-appearance: none;
        appearance: none;
        position: relative;
        top: 5px;
        right: 0;
        bottom: 0;
        left: 0;
        height: 20px;
        width: 20px;
        transition: all 0.15s ease-out 0s;
        background: #cbd1d8;
        border: none;
        color: #fff;
        cursor: pointer;
        display: inline-block;
        margin-right: 0.5rem;
        outline: none;
        position: relative;
        z-index: 1000;
}
.option-input:hover {
  background: #9faab7;
}
.option-input:checked {
  background: #40e0d0;
}
.option-input:checked::before {
  height: 20px;
  width: 20px;
  position: absolute;
  content: '&#x2714;';
  display: inline-block;
  font-size: 26.66667px;
  text-align: center;
  line-height: 20px;
}
.option-input:checked::after {
  -webkit-animation: click-wave 0.65s;
  -moz-animation: click-wave 0.65s;
  animation: click-wave 0.65s;
  background: #40e0d0;
  content: '';
  display: block;
  position: relative;
  z-index: 100;
}
.option-input.radio {
  border-radius: 50%;
}
.option-input.radio::after {
  border-radius: 50%;
}

@keyframes click-wave {
0% {height: 20px;width: 20px;opacity: 0.35;position: relative;}
100% {height: 200px;width: 200px;margin-left: -80px;margin-top: -80px;opacity: 0;}
}
.option-input {-webkit-appearance: none;-moz-appearance: none;-ms-appearance: none;-o-appearance: none;appearance: none;position: relative;top: 5px;right: 0;bottom: 0;left: 0;height: 20px;width: 20px;transition: all 0.15s ease-out 0s;background: #cbd1d8;border: none;color: #fff;cursor: pointer;display: inline-block;margin-right: 0.5rem;outline: none;position: relative;z-index: 1000;}
.option-input:hover {background: #9faab7;}
.option-input:checked {background: #40e0d0;}
.option-input:checked::before {height: 20px;width: 20px;position: absolute;content: '✔';display: inline-block;font-size: 26.66667px;text-align: center;line-height: 20px;}
.option-input:checked::after {-webkit-animation: click-wave 0.65s;-moz-animation: click-wave 0.65s;animation: click-wave 0.65s;background: #40e0d0;content: '';display: block;position: relative;z-index: 100;}
.option-input.radio {border-radius: 50%;}
.option-input.radio::after {border-radius: 50%;}


.verified-status{

}



.verified-status i{
    color: #399E00;
}

.verified-status button{
	border: none;
    border-radius: 5px;
    text-transform: uppercase;
    font-weight: 500;
    background: none;
    color: #399E00;
    cursor: text;
    bottom: 13px;
    background: #ffffff;
    padding: 4px 10px;
    border-radius: 5px;
    margin: 5px 5px;
    border: 1px solid #399E00;
}

button.unverified-btn{
	border: none;
    border-radius: 5px;
    text-transform: uppercase;
    font-weight: 500;
    background: none;
    color: #656565;
    cursor: text;
    bottom: 13px;
    background: #ffffff;
    padding: 4px 10px;
    border-radius: 5px;
    margin: 5px 5px;
    border: 1px solid #656565;
}


.verified-status button:focus{
    outline: none;
}

i.fa-cus-check{
	color: #656565;
}

span.unverified{
	color: #656565;
}

a.report-flag{
  color: #000;
}

p.user-inf{
	margin-bottom: 7px;
}

.singleinput0 label{
  margin-top: 7px;
}

.singleinput0 label span{
  margin-top: 11px;
}

.singlepostchat{
  display: none;
}


/* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
  
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {
  
  
}

/* 
  ##Device = Tablets, Ipads (landscape)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
  
  
}

/* 
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {


.post-bookingcode-btn {
  width: auto;
  padding: 10px;
  margin: 10px 15px;
}    



  
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/
@media only screen and (min-width:180px) and (max-width: 400px) {
	.post-title{
		font-size: 14px !important;
	}
	.comment-title{
		font-size:14px !important;
		margin-bottom: 10px !important;
		margin-top: 10px  !important;
	}
	.custom_vote-inner{
		display: none;
	}
	.comment-form-inner{
		padding: 10px !important;
	}
	#commentForm textarea, #commentForm button{
		margin-left: 0px !important;
	}
	.vote-outer{
		position: inherit !important;
	}
}

@media only screen and (min-width: 768px) and (max-width: 1024px) {
	.post-bookingcode-btn{
		width: 100%;
		margin-left: 0px;
	}
	.col-md-2{
	    flex: 1 0 16% !important;
	    max-width: 22% !important;
	}
	.col-md-7{
		flex: 0 0 58% !important;
		max-width: 50% !important;
	}
	.col-md-3{
		max-width: 30% !important;
		flex: 0 0 28% !important;
	}
}

@media only screen and (min-width: 500px) and (max-width: 640px) {
	
	.col-md-2{
	    flex: 1 0 16% !important;
	    max-width: 30% !important;
	}
	.col-md-7{
		flex: 0 0 40% !important;
		max-width: 40% !important;
	}
	.col-md-3{
		max-width: 30% !important;
		flex: 0 0 30% !important;
	}
	.post-bookingcode-btn{
		padding: 0px;
		width: 100%;
		margin-left: 0px;
	}
}



@media only screen and  (min-width: 320px) and (max-width: 480px) {


 
.norm_row.sfsi_wDiv{
	width: 100% !important;
}

.vote-outer .vote-inner > div{
	padding: 2px 0px !important;
}
.vote-outer .vote-inner > div{
	width: 10% !important;
} 
.vote-outer .vote-inner{
	margin:0px !important;
}

.post-bookingcode-btn {
  text-align: center;
  margin-top: 15px;
  width: auto;
  background: #22b14c;
  border-radius: 3px;
  cursor: pointer;
  padding: 10px;
  margin: 10px 15px;
}  

.addtoany_list img{
  width: auto!important;
  height: auto!important;
}

.vote-inner > div {
    width: 24%;
    display: inline-block;
}

.vote-inner {

    display: inline-block;
}

.button-flw {
    display: inline-block!important;
}

.singlepostchat{
  display: none;
}

}

.hover_bkgr_fricc{
background:rgba(0,0,0,.4);
cursor:pointer;
display:none;
height:100%;
position:fixed;
text-align:center;
top:0;
width:100%;
z-index:10000;
left:0;
}
.hover_bkgr_fricc .helper{
display:inline-block;
height:100%;
vertical-align:middle;
}
.hover_bkgr_fricc > div {
background-color: #fff;
box-shadow: 10px 10px 60px #555;
display: inline-block;
height: auto;
max-width: 551px;
min-height: 100px;
vertical-align: middle;
width: 60%;
position: relative;
border-radius: 8px;
padding: 15px 5%;
}
.popupCloseButton {
background-color: #fff;
border: 3px solid #999;
border-radius: 50px;
cursor: pointer;
display: inline-block;
font-family: arial;
font-weight: bold;
position: absolute;
top: -20px;
right: -20px;
font-size: 25px;
line-height: 30px;
width: 30px;
height: 30px;
text-align: center;
}
.popupCloseButton:hover {
background-color: #ccc;
}
.trigger_popup_fricc {
cursor: pointer;
font-size: 20px;
margin: 20px;
display: inline-block;
font-weight: bold;
}
.widget-user{
  padding-top: 10px;
  padding-bottom: 10px;
}
.widget-user-bar div.avatar-small{
  width: 30px;
  height: 30px;
}
.widget-user-bar {
    display: flex;
}
.widget-user-bar div.username {
    font-size: 12px;
    padding: 6px;
    width: 60%;
    text-align: left;
}
.widget-user-bar div.widget_user_bar_i1{
  width: 20%;
}
.widget-user-bar div.widget_user_bar_i2{
  width: 100%;
  text-align: end;
  display: none;
}
.widget-user-bar div.widget_user_bar_i1 i.widget_user_bar_icon1{
  font-size: 37px;
  text-align: right;
  margin-top: -10px;
}
.widget-user-bar div.widget_user_bar_i2 i.widget_user_bar_icon2{
  font-size: 37px;
}

@media only screen and (max-width: 600px) {
    .mobilelogin{top:80px;}
    .widget-user-content{
	  display: none;
	}
	.widget-user-bar{
		display: flex !important;
	}
	.fabs{
		bottom:40px;
	}
	.for_logo_mobile{
		vertical-align: middle;
	}
	.custom_menu_btn_mobile{
		padding: 0px !important;
	}
	.custom_menu_btn_mobile i{
		position: inherit !important;
	}
	.post-cate{
		margin-top:40px;
	}
	img.custom_image_mobile {
	    width: auto;
	    height: 40px;
	}
}


.input-group{
	display:flex;
}
.input-group .dropdown-item {
    display: block;
    width: 100%;
    padding: .25rem 1.5rem;
    clear: both;
    font-weight: 400;
    color: #212529;
    text-align: inherit;
    white-space: nowrap;
    background-color: transparent;
    border: 0;
}

.input-group-prepend .dropdown-toggle:after {
    display: inline-block;
    vertical-align: middle;
    font-weight: 900;
}

.input-group.dropdown-toggle::after {
    display: none;
}
.col-mmmd-4 a,a{
	text-decoration: none;
}
.input-group .dropdown-toggle::after {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: .255em;
    vertical-align: .255em;
    content: "";
    border-top: .3em solid;
    border-right: .3em solid transparent;
    border-bottom: 0;
    border-left: .3em solid transparent;
}
.dropdown-toggle::after {
    color: #1bba9a !important;
}
#main-discover input[type="submit"]:hover{
  background-color: unset !important;
}
</style>
<script type="text/javascript">
  $(document).ready(function () {
  	$('.widget-user-bar').hide();
    $(".widget-user-bar div.widget_user_bar_i1 i.widget_user_bar_icon1").click(function(){
        $(".widget-user-content").show();
        $(".widget-user-bar div.widget_user_bar_i1 i.widget_user_bar_icon1").hide();
        $(".widget-user-bar div.widget_user_bar_i2").show();
        $(".widget-user-bar div.avatar-small").hide();
        $(".widget-user-bar div.username").hide();
    });
    $(".widget-user-bar div.widget_user_bar_i2 i.widget_user_bar_icon2").click(function(){
      $(".widget-user-content").hide();
        $(".widget-user-bar div.widget_user_bar_i1 i.widget_user_bar_icon1").show();
        $(".widget-user-bar div.widget_user_bar_i2").hide();
        $(".widget-user-bar div.avatar-small").show();
        $(".widget-user-bar div.username").show();
    });
  });
</script>
<script>
$(function() {
  var messageSendCount=0;

  function getChat() {
    if(!!$('textarea[name="chat_message"]').val() || !!$('#guestEmail').val())
      return;
    $.ajax({
      url: '/English/travchat/user/chatbox_fetch_chat.php',
      type: 'POST',
      async: false,
      data: {
        fetch: 1,
        agent: <?php echo $userinfo->userid; ?>,
      },
      success: function (response) {
		if(!response.length) {
		  response='<span class="chat_msg_item chat_msg_item_admin">\
                    <div class="chat_avatar"><img src="<?php echo $agent_avatar; ?>" /></div>\
            <?php echo $Msg  ?> </span>'+response;
		}
		else
		  messageSendCount++;
        $('#chat_converse').html(response);
        $("#chat_converse").scrollTop($("#chat_converse")[0].scrollHeight)
      }
    });
  }
  
  window.getChat=function(){ getChat(); }

  getChat();
  setTimeout("document.getElementById('chatbox_alert_sound').play();",9000);
  setTimeout("toggleFab()", 9000);
  var chatInit=false;
  //$('#prime').click(function() {
$(document).ready(function () {
    if(!chatInit) {
      $.ajax({
        url: '/English/travchat/user/chatbox_init.php',
        type: 'POST',
        async: false,
        data: {
          agent: <?php echo $userinfo->userid; ?>,
        },
        success: function (response) {
        }
      });
      setInterval("getChat()", 30000);
    }
    chatInit=true;
  });


// added on enter

 var sending=false;
var input = document.getElementById("chatSend");
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
   event.preventDefault();

       if(sending) {
      sending=true;
      return;
    }
	
	  if(messageSendCount==0) {
		  var MsgObj={
			  agent: <?php echo $userinfo->userid; ?>,
			  msg: $('textarea[name="chat_message"]').val(),
			  InitMsgId: <?php echo $MsgId; ?>,
			  InitMsg: '<?php echo $Msg; ?>'
		  };
	   }
	   else {
		   var MsgObj={
			   agent: <?php echo $userinfo->userid; ?>,
			   msg: $('textarea[name="chat_message"]').val(),
		   };
	  }

      if(!!$('textarea[name="chat_message"]').val()) {
      $.ajax({
        url: '/English/travchat/user/send_message.php',
        type: 'POST',
        data: MsgObj,
        success: function (response) {
          sending=false;
          $('textarea[name="chat_message"]').val('');
          getChat();
        }
          });
    }
    else
      alert('Please write message first');


  }
});

// added on click
  var sending=false;
  $('#fab_send').click(function() {
    if(sending) {
      sending=true;
      return;
    }
	
	if(messageSendCount==0) {
		var MsgObj={
			agent: <?php echo $userinfo->userid; ?>,
			msg: $('textarea[name="chat_message"]').val(),
			InitMsgId: <?php echo $MsgId; ?>,
			InitMsg: '<?php echo $Msg; ?>'
		};
	}
	else {
		var MsgObj={
			agent: <?php echo $userinfo->userid; ?>,
			msg: $('textarea[name="chat_message"]').val(),
		};
	}
	
    if(!!$('textarea[name="chat_message"]').val()) {
      $.ajax({
        url: '/English/travchat/user/send_message.php',
        type: 'POST',
        data: MsgObj,
        success: function (response) {
          sending=false;
          $('textarea[name="chat_message"]').val('');
          getChat();
        }
      });
    }
    else
      alert('Please write message first');
  });
});
</script>

<?php
if (have_posts()) {
    while (have_posts()) {
        the_post();
    //update view count
    setPostViews(get_the_ID());
        ?>
    <input id="cs_RMB" type="hidden" value="<?php echo get_option('_cs_currency_RMD'); ?>" />
    <input id="cs_USD" type="hidden" value="<?php echo get_option('_cs_currency_USD'); ?>" />
    <input id="cs_AUD" type="hidden" value="<?php echo get_option('_cs_currency_AUD'); ?>" />
        <div>
        <?php
        /* 
              global $user_login, $current_user;
              get_currentuserinfo();
              $user_info = get_userdata($current_user->ID);
              $roles = array (
                  'administrator',
                  'um_sales-agent',
              );

          if (is_user_logged_in() && array_intersect( $roles, $user_info->roles)) {

          echo '<div class="d-flex flex-column flex-sm-row mb-4">
                <div>
                    <div class="backtoformcustom">
                        <a href="#" class="btn btn-primary secondbackbtn mb-2"><i class="fa fa-angle-left" style="color:#fff;margin-right:15px;"></i><span>Back to Previous Page</span> </a> <a href="https://www.travpart.com/English/travcust" class="btn btn-primary firstbackbtn mb-2"><span>Your Destination</span> </a> <a href="#" data-toggle="tooltip" title="" class="btn btn-primary firstbackbtn mb-2" data-original-title="You have not set up a travel plan yet."><span>Check my journey details</span> <i class="fa fa-angle-right" style="color:#fff;margin-left:15px;"></i> </a>
                    </div>
                </div>
            </div>';

          } else {

          echo '<br>';

          }*/
          ?>
            
            <div class="post-cate"><span>
                    <?php
                    
                    $categories = get_the_category();
                    $separator = ' , ';
                    $output = '';
                    if (!empty($categories)) {
                        foreach ($categories as $category) {
                            $output .= '<a href="' . esc_url(get_category_link($category->term_id)) . '" alt="' . esc_attr(sprintf(__('View all posts in %s', 'textdomain'), $category->name)) . '">' . esc_html($category->name) . '</a>' . $separator;
                        }
                        echo trim($output, $separator);
                    }
                    ?>
                </span></div>
            <h1 class="post-title text-center"><?php the_title(); ?></h1>
            <meta property="og:type"               content="article" /> 
      
    <meta property="og:title" content="<?php the_title(); ?>" />
    <meta property="og:image"  content="https://r-ec.bstatic.com/images/hotel/max1024x768/126/126843767.jpg" />
            <?php if (get_the_post_thumbnail_url(null, 'large')): ?>
                <div class="featured-img-wrap" style="background-image: url(<?php echo get_the_post_thumbnail_url(null, 'large') ?>)">
                <?php endif; ?>

            </div>
             
             <div class="owl-carousel owl-carousel_2 overview_div">
          	 	<div class="display_overviewdiv">
          	 		<h3>OVERVIEW</h3>
          	 		<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>">VIEW DETAILS</a>
          	 	</div>
          	 	
          	 	<?php
      	 			$tourinfos = $wpdb->get_results("SELECT * FROM `wp_hotel` WHERE `tour_id` = " . (int) $tour_id);
                    if(count($tourinfos)>0){	
          	 	 ?>
          	 	<div class="display_block" >
          	 	
          	 		<?php
                        
                        $hotel_total_price = 0;
                        foreach ($tourinfos as $tourinfo) {
                            $has_hotel = true;
                            $night = round((strtotime($tourinfo->end_date) - strtotime($tourinfo->start_date)) / 86400);
                            $hotel_total_price += $tourinfo->total;
                            $total_price += round($tourinfo->total / get_option("_cs_currency_USD") / get_option('person_price_ration'));
                            ?>
                            
	                    <div class="slider">
	                    <?php if(unserialize($tourinfo->img)) {
	                      $hotel_imgs=unserialize($tourinfo->img);
	                      if (!empty($tourinfo->regionid)) { ?>
                                <a target=_blank href="https://www.travpart.com/hotel-and-flight-bookings/?tour_id=<?php echo $tour_id; ?>&travcust=1#/hotel-information/<?php echo $tourinfo->regionid; ?>/?fn=hotelInfo&checkIn=<?php echo date('m-d-Y', strtotime($tourinfo->start_date)); ?>&checkOut=<?php echo date('m-d-Y', strtotime($tourinfo->end_date)); ?>&language=en_US&currency=USD&hotelType=1&rooms=1&adults=1&childs=0&is_custom=0">
                                    <img src="<?php echo $hotel_imgs[0]; ?>" width="180" alt="" class="img-responsive" />
                                </a>
                            <?php } else { ?>
                                <img src="<?php echo $hotel_imgs[0]; ?>" width="180" alt="" class="img-responsive" />
                            <?php }
	                      for($i=1; $i<count($hotel_imgs); $i++) { ?>
	                      <img src="<?php echo $hotel_imgs[$i]; ?>" width="180" alt="" class="img-responsive" />
	                    <?php }
	                      } else {
	                        if (!empty($tourinfo->regionid)) { ?>
                                <a target=_blank href="https://www.travpart.com/hotel-and-flight-bookings/?tour_id=<?php echo $tour_id; ?>&travcust=1#/hotel-information/<?php echo $tourinfo->regionid; ?>/?fn=hotelInfo&checkIn=<?php echo date('m-d-Y', strtotime($tourinfo->start_date)); ?>&checkOut=<?php echo date('m-d-Y', strtotime($tourinfo->end_date)); ?>&language=en_US&currency=USD&hotelType=1&rooms=1&adults=1&childs=0&is_custom=0">
                                    <img src="<?php echo $tourinfo->img; ?>" width="180" alt="" class="img-responsive" />
                                </a>
                            <?php } else { ?>
                                <img src="<?php echo $tourinfo->img; ?>" width="180" alt="" class="img-responsive" />
                            <?php }
	                      } ?>
	                    </div>
          	 	 	<?php } ?>
          	 		<p>
          	 			<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>" onclick="">HOTEL</a>
          	 		</p>
          	 	</div>
          	 	<?php } ?>
          	 	
          	 	
          	 	<?php if(!empty($data['popupguide_type'])){ 
          	 		$TravcustPopupguide = unserialize(get_option('_cs_travcust_popupguide'));
          	 		$popupguide_types = explode(',', $data['popupguide_type']);
          	 	?>
	          	 	<div class="display_block">
	          			<?php
                        for ($i = 0; $i < count($popupguide_types) - 1; $i++) { ?>
			          	 	<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>">
			          	 		<img src="<?php echo $TravcustPopupguide[$popupguide_types[$i] - 1]['img']; ?>" >
		          	 		</a>
	          	 		<?php } ?>
		          	 		<p>
		          	 			<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>" onclick="">Tour guide</a>
		          	 		</p>
	          	 	</div>
          	 	<?php } ?>
          	 	
          	 	<?php
			        $adivaha_cars=$wpdb->get_results("SELECT * FROM `wp_cars` WHERE `tour_id` = " . (int) $tour_id, ARRAY_A);
			        if (!empty($car_type) || !empty($adivaha_cars)) { ?>
	          	 	<div class="display_block">
	          	 	 <?php 
	          	 	 	if(!empty($car_type)){ 
		          	 	 	$TravcustCar = unserialize(get_option('_cs_travcust_car'));
				            $car_types = explode(',', unescape($car_type));
				            for($i = 0; $i < count($car_types) - 1; $i++) {
				            	if (floor($car_types[$i]) + 0.5 == $car_types[$i]) {
		                            $car_types[$i] = floor($car_types[$i]);
		                        }
				            }
	          	 	 ?>
	          	 		<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>">
	          	 			<img src="<?php echo $TravcustCar[$car_types[0] - 1]['img']; ?>" width="180" alt="" class="img-responsive">
	          	 		</a>
	          	 		<p>
	          	 			<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>" onclick="">Transportation</a>
	          	 		</p>
	          	 	 <?php } ?>
	      	 	 	 <?php if(!empty($adivaha_cars)) { ?>	
	      	 	 		<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>">
	          	 			<img src="<?php echo $adivaha_cars[0]['img']; ?>" width="180" alt="" class="img-responsive">
	          	 		</a>
	          	 		<p>
	          	 			<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>" onclick="">Transportation</a>
	          	 		</p>
	          	 	 <?php } ?>
	          	 	</div>
          	 	<?php } ?>
          	 	
          	 	<?php
                $ExtraActivity = unserialize(get_option('_cs_extraactivity'));
                if (!empty($ExtraActivity) && !empty($eaiitem)) {
                    ?>
	          	 	<div class="display_block">
	          	 		 <?php
							$eaiitems = explode(',', unescape($eaiitem));
						  ?>
	          	 		<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>">
	          	 			<img src="<?php echo $ExtraActivity[$eaiitems[0]]['img']; ?>" width="180" alt="" class="img-responsive">
	          	 		</a>
	          	 		<p>
	          	 			<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>" onclick="">Optional Activities</a>
	          	 		</p>
	          	 	</div>
          	 	<?php } ?>
          	 	
          	 	<?php
	                if (!empty($meal_type) || !empty($restaurant_name)) { ?>
          	 	<div class="item dining display_block">
          	 	 	
	                <?php     
	                $meal_types = explode(',', unescape($meal_type));
	                	if (!empty($meal_type)) {
	                		for ($i = 0; $i < count($meal_types) - 1; $i++) {
		                    	$meal_sub_items = explode('-', $meal_types[$i]);
			                    if (!empty($meal_sub_items[0])) {
			                        $mealid = intval($meal_sub_items[0]);
			                        $TravcustMealItem = unserialize($wpdb->get_row("SELECT * FROM `travcust_meal_new` WHERE `id` = {$mealid}")->_cs_travcust_meal);
			                        if (!empty($TravcustMealItem)){
			                        ?>
			                        	<img src="<?php echo $TravcustMealItem['img']; ?>">
			                        	<p>
			                        		<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>" onclick="">DINING</a> 
			                        	</p>
			                        <?php 
			                        }
			                    }
		                    }
	                	}
	                }
	                ?>
                	
                	<?php
	                if (!empty($restaurant_name)){
	                	$restaurant_names = explode('--', unescape($restaurant_name));
	                	$restaurant_imgs = explode('--', unescape($restaurant_img));
	                ?>
                	<div class="slider2">
                         <?php for ($i = 0; $i < count($restaurant_names) - 1; $i++) { ?>
                    		<img src="<?php echo $restaurant_imgs[$i]; ?>" />
                   		 <?php  } ?>
                    </div>
                    <p><a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>" onclick="">DINING</a></p>
	            </div>
          	 	<?php }  ?>
          	 	<?php if (!empty($tourlist_name) || !empty($place_name)) { ?>
               		<div class="item recommended_place display_block">
               			 <?php
                            if (!empty($place_name)) {
                                $place_names = explode('--', unescape($place_name));
                                $place_imgs = explode(',', unescape($place_img));
                                 
                             ?>
                             	<div class="slider_3">
                             		<?php  for ($i = 0; $i < count($place_names) - 1; $i++) { ?>
                             			<img src="<?php echo $place_imgs[$i]; ?>" >                             
                         			<?php } ?>
                         	 	</div>
                            	<p>
			          	 			<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>" onclick="">RECOMMENDED</a>
			          	 		</p>
                             <?php 
                             	}
                             ?>
                        </div>    
                        <?php
                            if (!empty($tourlist_name)) { 
                            	$tourlist_names = explode('--', unescape($tourlist_name));
                                $tourlist_imgs = explode(',', unescape($tourlist_img));
                            ?> 
                        <div class="item recommended_place display_block">
                           	<div class="slider_3">
                         		<?php  for ($i = 0; $i < count($tourlist_names) - 1; $i++) { ?>
                         			<img src="<?php  echo $tourlist_imgs[$i]; ?>" >                             
                     			<?php } ?>
                         	</div>
                        	<p>
		          	 			<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>" onclick="">RECOMMENDED</a>
		          	 		</p>
               			</div>  
               			<?php } ?> 
               
                <?php } ?>     
                
                
                  <?php
	                if (!empty($spa_type)) {
                    $TravcustSpa = unserialize(get_option('_cs_travcust_spa'));
                    $spa_types = explode(',', unescape($spa_type));
                  ?>
    				<div class="item_spa display_block">
    				 <div class="slider_4">
    					<?php for ($i = 0; $i < count($spa_types) - 1; $i++) { ?>
    						<img src="<?php echo $TravcustSpa[$spa_types[$i] - 1]['img']; ?>">
    					<?php } ?>
    				 </div>
    				 <p>
          	 			<a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>" onclick="">Spa</a>
          	 		 </p>
    				</div>
    			  <?php } ?>
          	 	
          	 </div>
          
          <!--<div class="singe-post-nav">
          <span class="pull-left"> 
          <?php //previous_post_link('<strong>%link</strong>'); ?>  </span>
          <span class="pull-right"> 
          <?php //next_post_link('<strong>%link</strong>'); ?>  </span> </div>
          <div class="clearfix"></div>-->
			<!--<div class="col-md-4 col-sm-6 col-xs-12">
			<?php //echo get_the_post_thumbnail( get_the_id(),array(340, 400)  );        ?></div> -->
            <div class="row single_content mb-3">
                <div class="col-sm-3 col-md-2">
                    <?php
                    $tour_id = get_post_meta(get_the_ID(), 'tour_id', true);
                    $avatar = um_get_user_avatar_data($authordata->ID);
                    $verified = get_user_meta($authordata->ID, 'verification_status', true);
                    ?>
                    <!--widget user-->
                    <div class="widget-user mb-2">
                      <div class="widget-user-bar">
                        <div style="width: 20%">
                          <div class="avatar-small" style="background-image: url(<?php echo $avatar['url'] ?>)">
                          </div>
                        </div>
                        <div class="username"><?php echo $authordata->user_login; ?></div>
                        <div class="widget_user_bar_i1">
                          <i class="widget_user_bar_icon1 fas fa-sort-down"></i>
                        </div>
                        <div class="widget_user_bar_i2">
                          <i class="widget_user_bar_icon2 fas fa-sort-up"></i>
                        </div>
                      </div>
                      <div class="widget-user-content">
                      <div class="verified-status">
                          
              <?php
              if (empty($verified))
                            echo"<button class='unverified-btn'><i class='fas fa-times-circle fa-cus-check'></i> Unverified</button>";
                        else if ($verified == 1)
                            echo 'Pending';
                        else if ($verified == 2)
                          echo"<button class='verified-btn'> <i class='fas fa-check-circle'></i> Verified </button>";
                        else
                            echo 'Unverified';
                          ?>
             
            </div>
                        <div class="avatar" style="background-image: url(<?php echo $avatar['url'] ?>)"></div>
                        <div class="username"><?php echo $authordata->user_login; ?></div>
                        <div class="title">Deal Expert</div>
            <p class="user-inf">
            <?php
            if(!empty($userinfo->country)) {
              if(empty($userinfo->region))
                echo $userinfo->country;
              else
                echo $userinfo->country.','.$userinfo->region;
            }
            ?>
            </p>
            
          <div id="reportflag"> <i class="fas fa-flag"></i> Report</div>
                        <div class="rating-wrap">
                            <span class="trav-rating"><span><?php echo $avgRatting . '.0'; ?></span>
                                <i>
                                    <?php
                                    if ($avgRatting == 1) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <?php
                                    } else if ($avgRatting == 2) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <?php
                                    } else if ($avgRatting == 3) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <?php
                                    } else if ($avgRatting == 4) {
                                        ?>

                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <?php
                                    } else if ($avgRatting == 5) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <?php
                                    } else {
                                        echo "No Rating yet!";
                                    }
                                    ?>
                                </i>
                            </span>
                        </div>
            <?php
                        if (!empty($userinfo->region)) {
                            date_default_timezone_set($userinfo->timezone);
                            ?>
                            <p><i class="fa fa-map-marker"></i> <?php echo date('g:i A'); ?> local time</p>
            <?php } ?>
                        <div class="sidebar-user-footer">
                            <a href="<?php echo home_url("/bio/?user={$authordata->user_login}"); ?>" class="view-profile">VIEW PROFILE</a>
                        </div>
                    </div>
                  </div>
                    
                    <!--widget share -->                    
                    <link rel="stylesheet" href="/English/wp-content/themes/bali/css/social-share-media.css">         <script src="/English/wp-content/themes/bali/js/social-share-media.js"></script>          <script>                        const socialmediaurls = GetSocialMediaSiteLinks_WithShareLinks({'url': 'https://www.travpart.com/English/tour-details/?bookingcode=BALI<?php
            echo $tour_id;
            ;
            ?>', 'title': 'I recommend you use the travcust app for making money to sell tour packages. Download and get $5 credit.'});
                        function gotoshare(medianame) {
                            if (medianame == 'whatsapp') {
                                window.open('https://web.whatsapp.com/send?text=' + encodeURIComponent('I recommend you use the travcust app for making money to sell tour packages. Check https://www.travpart.com/English/tour-details/?bookingcode=BALI<?php
            echo $tour_id;
            ;
            ?> and get $5 credit.'));
                            } else {
                                window.open(socialmediaurls[medianame]);
                            }
                        }</script>
          <?php
                    if (is_user_logged_in()) {

                    $Link =site_url()."/travchat/user/addnewmember.php?user=".$userinfo->userid;

                    }
                    else
                    {
                    $rand_guest_id = rand (999,9999);
                    $Link =site_url()."/travchat/guestUserLogin.php?id=$rand_guest_id&to_connect=$userinfo->userid";

                    }
                          
                        ?>
                    <div class="widget-share widget-share-singlepost" onclick= "live_chat_js(<?php echo $tour_id ?>,1)"><a class="singlepostchat" href="<?php echo $Link; ?>" target="_blank">
                            <i class="icon-chat"></i>
                            <div class="chat-title">Trip<br>Advisor</div>
                            <div class="chat-to"><?php echo $authordata->user_login; ?></div>         </a>
                        <div class="share-title">Share this deal</div>
                        <div class="share-links">
                            <a href="#" onclick="gotoshare('google.plus')"><span><img src="https://www.travpart.com/Chinese/wp-content/themes/bali/images/images/google-plus.png" alt="google-plus"></span></a>           <a href="#" onclick="gotoshare('pinterest')"><span><img src="https://www.travpart.com/Chinese/wp-content/themes/bali/images/images/pinterest.png" alt="pinterest"></span></a>     <a href="#" id="share_button"><span><img src="https://www.travpart.com/Chinese/wp-content/themes/bali/images/images/facebook-logo.png" alt="facebook"></span></a>           <a href="#" onclick="gotoshare('whatsapp')"><span><img width=24px src="https://www.travpart.com/English/wp-content/uploads/2018/10/whatsapp.png" alt="whatsapp"></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-md-7 contentimage">
                    <?php
//                    remove_filter('the_content','bshare');
                    $content=get_the_content();
          $content = apply_filters( 'the_content', $content );
          $content = str_replace( ']]>', ']]&gt;', $content );
          $content = preg_replace_callback('!(<a)(.*?href=["\']{1}http[s]?://)(.*?)([/"\'])!i',function ($matches) { return stripos($matches[3],'travpart.com')===false?($matches[1].' target="_blank" '.$matches[2].$matches[3].$matches[4]):$matches[0]; },$content);
          echo $content;
                    ?>
                    
                           
	            
                    
                </div>
                <div class="col-sm-4 col-md-3">
                    <?php
                    //            test ui
                    if (!$tour_id)
                        $tour_id = 12345;

                    if (!empty($tour_id) && intval($tour_id) > 0) {
            $total_price=$wpdb->get_var("SELECT total FROM wp_tour WHERE id='{$tour_id}'");
            ?>
                        <div class="post-bookingcode">
              <table>
                <tr>
                  <td><strong>Booking Code:</strong></td>
                </tr>
                <tr>
                  <td class="td_bali_trid">BALI<?php echo intval($tour_id); ?></td>
                </tr>
                <tr>
                  <td><strong>Total Price:</strong></td>
                </tr>
                <tr>
                  <td class="td_bali_price"><span class="change_price"or_pic="<?php echo intval($total_price); ?>">Rp<span class="price_span"><?php echo intval($total_price); ?></span></span></td>
                </tr>
                <tr>
                  <td class="bookingcodeviewdetail" onclick="ajax_loading(<?php echo $tour_id ?>,1 );"><a href="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>&agent_id=<?php echo $userinfo->userid; ?>" onclick="">VIEW DETAILS</a></td>
                </tr>
              </table>
            </div>

            <div class="post-bookingcode-btn" onclick="ajax_loading(<?php echo $tour_id ?>,1 );">
              <a href="<?php echo site_url(); ?>/total-cost/?tourid=<?php echo intval($tour_id); ?>" class="book-now-btn">Book Now</a>
            </div>


            <?php
          }
                    ?>
          <?php if(get_option('_travpart_vo_option')['tp_game_enable']==1) { ?>
          
          <div class="vouchers-banner vouchers-banner-custom" style="text-align: right;margin-top: 20px;">
          
            <a href="https://www.travpart.com/English/user/?gametab" target="_blank" rel="noopener">
              <img class="vocher_banerr" style="margin-bottom: 20px;width: 100%;" src="https://www.travpart.com/English/wp-content/themes/bali/images/voucher-banner-en.png">
            </a>
		    <a class="getinfobtn" target="_blank" href="https://www.travpart.com/English/game-information/">
		    <i class="fa fa-info-circle" style="font-size: 24px;margin-left: 5px;color: #61cbc9;"></i>
		    </a>
		    <div class="text-80-off">
				<p>Get up to 80% off</p>
			</div>
          </div>
          <?php } ?>
                    <div class="post-bookingcode searchfcn">
  <div id="main-discover">
<div class="tourfilter">
<h4>Discover the perfect journey</h4>
<div dir="ltr" lang="en-US"><form class="wpcf7-form" action="<?php bloginfo('home'); ?>/toursearch" method="GET" target=_blank><label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="search" size="40" type="text" value="" placeholder="Search">
</span>
</label><label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="agent" size="40" type="text" value="" placeholder="Agent's name">
</span>
</label><label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="days" size="40" type="text" value="" placeholder="Days">
</span>
</label><label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="budget" size="40" type="text" value="" placeholder="Budget">
</span>
</label><label style="width: 100%;">
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="date" type="date" value="" placeholder="Choose your date">
</span>
</label>
<div class="pricerange">
<h4>Price Range</h4>
<div class="col-md-6 lbl-col"><label class="lbl-1">
<input class="option-input radio" name="example" type="radio">
$0 – $500
</label></div>
<label class="lbl-2">
<input class="option-input radio" name="example" type="radio">
$501 – $1000
</label>

</div>
<div class="col-md-6 lbl-col"><label class="lbl-1">
<input class="option-input radio" name="example" type="radio">
$1K – $5K
</label></div>
<label class="lbl-2">
<input class="option-input radio" name="example" type="radio">
$5K +
</label>
<input class="wpcf7-form-control wpcf7-submit form-control" type="submit" value="Search">

</form></div>
</div></div>
 </div>
                </div>
<meta property="og:url"     content="<?php echo site_url(); ?>/tour-details/?bookingcode=BALI<?php echo intval($tour_id); ?>" /> 
            </div>
            
            
            <?php
					
					//if($_GET['debug']==1){
						
					$related = get_posts( array( 'category__in' => wp_get_post_categories(get_the_ID()), 'numberposts' => '', 'post__not_in' => array(get_the_ID()) ) );
					if( $related ) 
					{
					?>
					<div class="you_may_hide">
						<h1 class="you_title">You May Also Like...</h1>
						<div class="owl-carousel owl-carousel_1" >
						<?php
						foreach( $related as $post ) {
							setup_postdata($post); 
							$tour_id = get_post_meta($post->ID, 'tour_id', true);
							if (!empty($tour_id) && intval($tour_id) > 0) {
			         			$total_price=$wpdb->get_var("SELECT total FROM wp_tour WHERE id='{$tour_id}'");
			        		}else{
								$total_price=0;
							}
						?>
						<?php if(get_the_post_thumbnail_url(get_the_ID())){ ?>
							<div class="custom_related_products ">
								<div class="image_related_product">
									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
										<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),null, 'small'); ?>" class="img-responsive" />		
									</a>	
								</div>
								<div class="content_related_products">
									<div class="title_div">
										<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
									</div>
								</div>
                <div class="price_div">
                    <span class="change_price" or_pic="<?php echo intval($total_price); ?>"> Rp <span class="price_span"> <?php echo intval($total_price); ?></span></span>
                  </div>
							</div>
					    <?php
					    }
					    } ?>
					    </div>
				    </div>
				    <?php
					}
				    // }
					wp_reset_postdata(); 
				
				
				?>
	            
            <!-- Like / Dislike / Follow Start by Amrut -->
            <?php
            if ($tour_id != '' && is_user_logged_in()) {
                $current_user = wp_get_current_user();
                $sc_user_id = isset($current_user->ID) ? $current_user->ID : '';
                $getDataForCheck = $wpdb->get_results("SELECT * FROM `social_connect` WHERE sc_tour_id = '$tour_id' AND sc_user_id = '$sc_user_id'", ARRAY_A);
                $sc_type = isset($getDataForCheck[0]['sc_type']) ? $getDataForCheck[0]['sc_type'] : '';
        //Get the count of like and dislike
        $likecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$tour_id}' AND `sc_type` = 0");
        $dislikecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$tour_id}' AND `sc_type` = 1");
            }
            ?>
            <div class="">
                <div class="">
                    <div class="vote-outer">
                        <form method="post" id="social_connect_form">
                            <input type="hidden" name="action" value="social_connect_l_d">
                            <input type="hidden" name="sc_tour_id" value="<?php echo $tour_id; ?>">
                            <div class="vote-inner custom_vote-inner">

									<div class="singleinput0">
										<label class="singlepage-label" for="s1">
                      <a target="_blank" class="a2a_dd addtoany_share_save addtoany_share" href="https://www.addtoany.com/share#url=https%3A%2F%2Fwww.travpart.com%2FEnglish%2Flas-vegas-5-days-4-nights%2F&amp;title=Las%20Vegas%205%20days%204%20nights"><picture>
<source type="image/webp" srcset="https://www.travpart.com/English/wp-content/uploads/2019/10/share2.png.webp">
<img src="https://www.travpart.com/English/wp-content/uploads/2019/10/share2.png" alt="Share">
</picture>
</a>
<span class="radio" style="margin-top: 0px;"><span>0</span> Share</span></label>
									</div>	

                                    <div class="singleinput1" <?php if ($sc_type == '0') echo "style='color:red;'"; ?>>
                                        <input type="radio" id="s1" name="sc_type"  value="0" style="width: auto;" <?php if ($sc_type == '0') echo "checked"; ?>>
                                      <label class="singlepage-label" for="s1"><i class="fa fa-thumbs-up"></i><span class="radio" style="margin-top: 0px;"><span><?php echo number_format($likecount); ?></span> Like</span></label>
                                    <script type="text/javascript">
                                       
                                          $(document).ready(function () {
                                              $(".singleinput1 .fa-thumbs-up").click(function () {
                                                var white= $('i.fa.fa-thumbs-up').css('color');
                                                
                                                  white =$.trim(white);
                                                  if (white === 'rgb(192, 195, 202)') {
                                                      $('i.fa.fa-thumbs-up').css("color", "rgb(26,109,132)");
                                                  } 
                                                   /*var blue= $('.singleinput2 .fa-thumbs-down').css('color');

                                                 if (blue === 'rgb(26,106,129)') {
                                                      $('.singleinput2 .fa-thumbs-down').css("color", "rgb(192, 195, 202)");
                                                  } 
                                                  else {
                                                      $('.singleinput2 .fa-thumbs-down').css("color", 'rgb(192, 195, 202)');
                                                  }*/
                                              });


                                               /*$(".singleinput2 .fa-thumbs-down").click(function () { 
                                                var white= $(this).css('color');
                                                  if (white === 'rgb(192, 195, 202)') {
                                                      $(this).css("color", "rgb(26,106,129)");
                                                  } else {
                                                      $(this).css("color", 'rgb(192, 195, 202)');
                                                  }
                                                   var blue= $('.singleinput1 .fa-thumbs-up').css('color');
                                                 if (blue === 'rgb(26,106,129)') {
                                                      $('.singleinput1 .fa-thumbs-up').css("color", "rgb(192, 195, 202)");
                                                  } 
                                                   else {
                                                      $('.singleinput1 .fa-thumbs-up').css("color", 'rgb(192, 195, 202)');
                                                  }
                                              });*/
                                          });
                                       </script>
                                   
                                    </div>
                                    <div class="singleinput2" <?php if ($sc_type == '1') echo "style='color:red;'"; ?>>
                                        <input type="radio" id="s2" name="sc_type" value="1" style="width: auto;" <?php if ($sc_type == '1') echo "checked"; ?>>
                                      <label class="singlepage-label" for="s2"><i class="fa fa-thumbs-down"></i><span class="radio1"><span><?php echo number_format($dislikecount); ?></span> Dislike</span></label>
                                    </div>
                                  <div class="views">
                                    <i class="fa fa-eye"></i>
                                    <span><span><?php echo number_format(getPostViews(get_the_ID())); ?></span> views</span>
                                  </div>
                  
                  
            
                  <div class="button-flw">
                  <?php
                  $followData = [];
                  $sf_user_id = isset($current_user->ID) ? $current_user->ID : '';
                  $metaDataForAgent = $wpdb->get_results("SELECT * FROM `wp_tourmeta` WHERE tour_id = '$tour_id' AND meta_key = 'userid'", ARRAY_A);
                  if (!empty($metaDataForAgent)) {
                    $sf_agent_id = isset($metaDataForAgent[0]['meta_value']) ? $metaDataForAgent[0]['meta_value'] : '';
                    $followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$sf_user_id' AND sf_agent_id = '$sf_agent_id'", ARRAY_A);
                  }
                  //Get follow count
                  $follow_count=$wpdb->get_var("SELECT COUNT(*) FROM `social_follow` WHERE `sf_agent_id` = '{$authordata->ID}'");
                  if (!empty($followData)) {
                  ?>
                    <div id="submitFollowAgent" class="b-follow" title="Unfollow Travel Advisor">
                      <i class="fa fa-user-minus"></i>
                      <span><span><?php echo number_format($follow_count); ?></span>Followers</span>
                    </div>
                  <?php } else { ?>
                    <div id="submitFollowAgent" class="b-follow" title="Follow Travel Advisor">
                      <i class="fa fa-user-plus"></i>
                      <span><span><?php echo number_format($follow_count); ?></span>Followers</span>
                    </div>
                                    <?php } ?>


                                    </div>
                                    <?php
                                    $tourCommentData = $wpdb->get_results("SELECT * FROM `social_comments`,wp_users WHERE scm_tour_id = '$tour_id' AND wp_users.ID = scm_user_id AND scm_parent=0", ARRAY_A);

                                    ?>

                                  <div class="views">
                                    <i class="fa fa-comment"></i>
                                    <span><span><?php echo $commentCount=$wpdb->get_var("SELECT COUNT(*) FROM `social_comments` WHERE scm_tour_id='{$tour_id}'");?></span> Comments</span>
                                  </div>
                            </div>







                        </form>
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
FB.init({appId: '2346029709012370', status: true, cookie: true,
xfbml: true});
};
(function() {
var e = document.createElement('script'); e.async = true;
e.src = document.location.protocol +
'//connect.facebook.net/en_US/all.js';
document.getElementById('fb-root').appendChild(e);
}());
</script>

<script type="text/javascript">
$(document).ready(function(){
$('#share_button').click(function(e){
e.preventDefault();

FB.ui({
  method: 'share',
  //href : '<?php echo $current_url;  ?>',
 href: 'https://www.travpart.com/English/tour-details/?bookingcode=BALI<?php echo $tour_id; ?>',
}, function(response){
  /*if (response && !response.error_message) {

      alert('Successfully Shared with Facebook Account Feeds.');
      save_share_data(<?php echo $current_user->ID ?>,<?php echo $tour_id;  ?>);
    


    } else {
            alert('Tour Not shared');
    }*/
});


});
});
</script>
                        <script>
              jQuery(document).ready(function () {
                $('#submitFollowAgent').click(function (e) {
                  e.preventDefault();
                  var url = '<?php echo site_url() ?>/submit-ticket/';
                  var tour_id = '<?php echo $tour_id ?>';
                  $.ajax({
                    type: "POST",
                    url: url,
                    data: {tour_id: tour_id, action: 'social_follow'}, // serializes the form's elements.
                    success: function (data) {
                      var result = JSON.parse(data);
                      if (result.status) {
                        alert(result.message);
                        //window.location.reload()// show response from the php script.
                      } else {
                        alert(result.message);
                        window.location.reload()// show response from the php script.
                      }
                    }
                  });
                });
                
                                $("input[name=sc_type]").change(function () {
                                    var url = 'https://www.travpart.com/English/submit-ticket/'
                                    $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: jQuery('#social_connect_form').serialize(), // serializes the form's elements.
                                        success: function (data)
                                        {
                                            var result = JSON.parse(data);
                                            if (result.status) {
                                                alert(result.message);
                                                window.location.reload()
                                            }else{
                                                window.location.reload()
                                            }
                                        }
                                    });
                                });
                
                $(".header_curreny").change(function () {
                  var full_name = $(this).find("option:selected").attr("full_name");
                  var syml = $(this).find("option:selected").attr("syml");
                  var curreny = $(this).val();
                  if (curreny == "IDR") {
                    $(".change_price").each(function () {
                      $(this).html(syml + "<span class='price_span' style='display:inline;'>" + $(this).attr("or_pic") + " </span>");
                    });
                  } else {
                    $(".change_price").each(function () {
                      var amount = $(this).attr("or_pic");
                      if (syml == '元') {
                        $(this).html("<span class='price_span' style='display:inline;'>" + Math.round($('#cs_RMB').val() * amount) + " </span>" + syml);
                      } else {
                        $(this).html(syml + "<span class='price_span' style='display:inline;'>" + ($('#cs_' + curreny).val() * Math.round(amount)).toFixed(2) + " </span>");
                      }
                    });
                  }
                });
                setTimeout('$(".header_curreny").change();', 300);
                
                $('.loginstyle').click(function () {
                    $('.hover_bkgr_fricc').show();
                });
                
                            });
                        </script>
                    </div>
                </div>


            </div>
             <script>
              jQuery(document).ready(function () {
                $('#reportflag').click(function (e) {
                  console.log('aca');
                   var url = '<?php echo site_url() ?>/submit-ticket/';
                  var tour_id = '<?php echo $tour_id ?>';
                  $.ajax({
                    type: "POST",
                    url: url,
                    data: {tour_id: tour_id, action: 'report_seller'}, // serializes the form's elements.
                    success: function (data) {
                      var result = JSON.parse(data);
                      if (result.status) {
                        alert(result.message);
                        //window.location.reload()// show response from the php script.
                      } else {
                        alert(result.message);
                        window.location.reload()// show response from the php script.
                      }
                    }
                  });

          
                  });
                });
                </script>
            
            
            <!-- Comment Section Start -->

            <br />
            <div class="comment-section">
              <!-- For List of comments START -->
              <div class="commentbox_wrapper_outer">
                <?php if (!empty($tourCommentData)):?>
                <div class="comment-title">
                  <?php echo $commentCount; ?> Comments
                </div>
                <?php endif;?>
                <div class="commentbox_wrapper">
                  <?php
                  if (!empty($tourCommentData)) {
                    foreach ($tourCommentData as $comment) {
                      ?>
                      <div class="commentbox">
                        <div class="comment-thumb" style="background-image: url(<?php echo um_get_avatar_url(um_get_avatar('', $comment['scm_user_id'], 40)); ?>)"></div>
                        <div class=" c-name">
                          <p><?php echo isset($comment['user_login'])? ucfirst($comment['user_login']):''; ?>&nbsp;&nbsp;&nbsp;<span class="c-date"><?php echo date('d M Y', strtotime($comment['scm_created'])); ?></span></p>
                        </div>
                        <div class="c-txt">
                          <p><?php echo isset($comment['scm_text'])?$comment['scm_text']:''; ?></p>
                        </div>
                        <?php
                        $voted_type_of_user=$wpdb->get_var("SELECT vote_type FROM `social_commentvote` WHERE user_id={$current_user->ID} AND scm_id={$comment['scm_id']}");
                        $likestyle='';
                        $dislikestyle='';
                        if($voted_type_of_user==='0') {
                          $likestyle='color:rgb(26, 106, 129)';
                        }
                        if($voted_type_of_user==='1') {
                          $dislikestyle='color:rgb(26, 106, 129)';
                        }
                        ?>
                        <div class="c-vote" scm_id="<?php echo $comment['scm_id']; ?>">
                          <span class="c-like"><i class="fa fa-thumbs-up" style="<?php echo $likestyle; ?>"></i>&nbsp;
                            <span><?php echo  $wpdb->get_var("SELECT COUNT(*) FROM `social_commentvote` WHERE scm_id={$comment['scm_id']} AND vote_type=0"); ?></span>
                          </span>
                          <span class="c-dislike"><i class="fa fa-thumbs-down" style="<?php echo $dislikestyle; ?>"></i>&nbsp;
                            <span><?php echo  $wpdb->get_var("SELECT COUNT(*) FROM `social_commentvote` WHERE scm_id={$comment['scm_id']} AND vote_type=1"); ?></span>
                          </span>
                          <span class="c-reply" uname="<?php echo isset($comment['user_login'])? ucfirst($comment['user_login']):''; ?>">
                            <i class="fa fa-reply"></i>&nbsp;
                            <span>0</span>
                          </span>
                        </div>
                        <!-- comment reply section -->
                        <?php
                        $replylist=$wpdb->get_results("SELECT wp_users.user_login,social_comments.* FROM `social_comments`,`wp_users` WHERE scm_parent={$comment['scm_id']} AND scm_user_id=wp_users.ID");
                        foreach($replylist as $reply) {
                        ?>
                        <div class="commentbox comment-reply">
                          <div class="comment-thumb" style="background-image: url(<?php echo um_get_avatar_url(um_get_avatar('', $reply->scm_user_id, 40)); ?>)"></div>
                          <div class=" c-name">
                            <p><?php echo ucfirst($reply->user_login); ?>&nbsp;&nbsp;&nbsp;<span class="c-date"><?php echo date('d M Y', strtotime($reply->scm_created)); ?></span></p>
                          </div>
                          <div class="c-txt">
                            <p><?php echo $reply->scm_text; ?></p>
                          </div>
                        </div>
                        <?php } ?>
                      </div>
                      <hr />
                      <?php
                    }
                  } else {
                    ?>
                    <!--<div class="comment-title">No Comment posted yet!</div>-->
                  <?php } ?>
                </div>
              </div>
              <!-- For List of comments END -->
              <div class="comment-form-outer">
                <div class="comment-title">Leave a comment</div>
                <div class="comment-form-inner">
                  <form method="POST" id="commentForm">
                    <input type="hidden" name="scm_tour_id" value="<?php echo $tour_id; ?>">
                    <input type="hidden" name="action" value="social_comments_form">
                    <input id="comment_parent" type="hidden" name="comment_parent" value="0">
                    <textarea name="scm_text" required="true" class="singletextarea"></textarea>
                    <br />
                    <button type="button" onclick="submitComments()" class="btn btn-lg btn-default">Post Comment</button>
                  </form>
                </div>
              </div>
            </div>
           
            
            <script>
              function submitComments() {
                var form = $("#commentForm");
                var url = '<?php echo site_url() ?>/submit-ticket/';
                $.ajax({
                   type: "POST",
                   url: url,
                   data: form.serialize(), // serializes the form's elements.
                   success: function (data) {
                     var result = JSON.parse(data);
                     if (result.status) {
                       alert(result.message);
                       window.location.reload()// show response from the php script.
                     }
                     else {
                       alert(result.message);
                       window.location.reload()// show response from the php script.
                     }
                   }
                });
              }
              $(function(){
                $('.c-vote .c-like, .c-vote .c-dislike').click(function() {
                  var that=$(this).find('i');
                  var color=that.css('color');
                  var count=parseInt($(this).find('span').text());
                  var likecount,dislikecount;
                  if (color === 'rgb(192, 195, 202)') {
                    that.css("color", "rgb(26,106,129)");
                    $(this).find('span').text(count+1);
                  }
                  else {
                    that.css("color", 'rgb(192, 195, 202)');
                    $(this).find('span').text(count-1);
                  }
                  if (that.hasClass('fa-thumbs-up')) {
                    if ($(this).parent().find('.fa-thumbs-down').css("color")=='rgb(26, 106, 129)') {
                      that=$(this).parent().find('.fa-thumbs-down').parent().find('span');
                      dislikecount=parseInt(that.text());
                      that.text(dislikecount-1);
                    }
                    $(this).parent().find('.fa-thumbs-down').css("color", "rgb(192, 195, 202)");
                  }
                  else {
                    if ($(this).parent().find('.fa-thumbs-up').css("color")=='rgb(26, 106, 129)') {
                      that=$(this).parent().find('.fa-thumbs-up').parent().find('span');
                      likecount=parseInt(that.text());
                      that.text(likecount-1);
                    }
                    $(this).parent().find('.fa-thumbs-up').css("color", "rgb(192, 195, 202)");
                  }

                  var vote_type=$(this).hasClass('c-like')?0:1;
                  var scm_id=$(this).parent().attr('scm_id');
                  $.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    dataType: 'json',
                    data: {
                      action: 'social_comment_vote',
                      vote_type: vote_type,
                      scm_id: scm_id
                    },
                    success: function(data) {
                      console.log(data);
                    }
                  });
                });

                $('.c-vote .c-reply').click(function(){
                  var scm_id=$(this).parent().attr('scm_id');
                  var uname=$(this).attr('uname');
                  $('.comment-title').text('Reply '+uname);
                  $('#comment_parent').val(scm_id);
                });

              });
            </script>
            <!-- Comment Section End -->
            
        </div>

  <div class="fabs">
        <div class="chat">
            <div class="chat_header">
                <div class="chat_option">
                    <div class="header_img">
                        <img src="<?php echo $agent_avatar; ?>" style="height: 55px;" />
                    </div>
                    <span id="chat_head"><?php echo $authordata->user_login; ?></span> <br>
          <span class="agent">Agent</span>
          <!--span class="online">(Online)</span-->
                    <span id="chat_fullscreen_loader" class="chat_fullscreen_loader">
          <i class="fullscreen zmdi zmdi-window-maximize"></i></span>
                </div>
            </div>

            <div id="chat_converse" class="chat_conversion chat_converse">
            </div>

            <div class="fab_field">
                <a id="fab_send" class="fab"><i class="zmdi zmdi-mail-send"></i></a>
                <textarea id="chatSend" name="chat_message" placeholder="Send a message"
                    class="chat_field chat_message"></textarea>
            </div>
      <audio id="chatbox_alert_sound" src="<?php bloginfo('template_url'); ?>/inc/messagealert.mp3"></audio>
    </div>
        <a id="prime" class="fab"><i class="prime zmdi zmdi-comment-outline"></i></a>
    </div>
<script type="text/javascript">
    window._mfq = window._mfq || []; 
    (function() { 
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true; 
        mf.src = "//cdn.mouseflow.com/projects/e34f1763-afe4-4f4e-9fc6-df81af96c04f.js";
        document.getElementsByTagName("head")[0].appendChild(mf); 
    })(); 
    window._mfq.push(["setVariable", "chat_button_click", "fab_send"]);
  window._mfq.push(["setVariable", "chat_box", "chatSend"]);
  window._mfq.push(["setVariable", "click_on_chat_button_fab", "prime"]);
  window._mfq.push(["setVariable", "keyup", "keyup"]);
  
</script>

      <div class="hover_bkgr_fricc">
        <span class="helper"></span>
        <div class="mobilelogin">
            <?php echo do_shortcode('[ultimatemember form_id=14064]'); ?>
            <hr>
            <span style="color:#555555">Login as a buyer only:</span><br>
            <?php echo do_shortcode('[nextend_social_login login="1" link="1" unlink="1" redirect="https://www.travpart.com/English/user/"]'); ?>
            <div class="row">
                <div class="col-sm-12" style="text-align: center;">
                    <a href="https://www.travpart.com/"><img style="height: 50px;" src="https://www.travpart.com/English/wp-content/themes/bali/images/2019-logo.png" /></a>
                </div>
            </div>
        </div>
      </div>
      
      <div class="clearfix"> </div>

        <?php
        $tags = wp_get_post_tags(get_the_ID());
        $tag_in = array();

        if ($tags) {
            foreach ($tags as $row) {
                $tag_in[] = $row->term_id;
            }
        }
        ?>
    <?php } ?>
  <?php /*
    <h2>  Hot articles </h2>
    <ul class="wpp-list">
        <?php
        if (!empty($tag_in)) {
            $wp_query = new WP_Query(array('tag__in' => $tag_in, 'post_status' => 'publish', 'posts_per_page' => 5, 'cat' => '2', 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'));
        } else {
            $wp_query = new WP_Query(array('post_status' => 'publish', 'posts_per_page' => 5, 'cat' => '2', 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'));
        }
        if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
                ?>

                <li> <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="wpp-post-title" target="_self">
                        <?php the_title(); ?></a>  
                </li>           

                <?php
            endwhile;
        endif;
        ?>


    </ul>
  */?>
<!-- Set up your HTML -->

<?php
} else {
    get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>
