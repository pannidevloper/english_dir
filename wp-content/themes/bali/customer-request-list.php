<style>
	.potential_buyer{
		margin-top: 30px;
	    border-style: solid;
	    border-width: 2px 2px 2px 2px;
	    border-color: #d1d1d1;
	    margin-bottom: 30px;
	    padding: 20px 0;
	    background: #fff;
	    border-style: solid;
	    border-width: 1px 1px 1px 1px;
	    border-color: #d1d1d1;
        margin-top: 75px !important;
	}

	.potential_buyer h2{
		color: #1A6D84;
		font-size: 38px!important;
	    text-align: center;
	    margin-top: 15px;
	    margin-bottom: 0;
	}

	.icon_user{
		background: #3DB9AC;
	    padding: 16px 18px;
	    border-radius: 50%;
	    display: inline-block;
	}

	.potential_buyer i{
		color: #fff;
    	font-size: 36px;
	}

	.icon-md-3{
		text-align: right;
	}
    .cus_icon_mob {
        display: none;
    }
    @media (max-width: 600px) {
       .cus_icon_desk {
            display: none;
        } 
        .cus_icon_mob {
            display: block;
        }
        .icon_user.cus_icon_left_mob {
            margin-top: -45px;
        }
        .icon_user.cus_icon_right_mob {
            margin-top: -67px;
        }
    }
    @media (min-width: 180px) and (max-width: 320px) {
        .potential_buyer h2 {
            font-size: 34px !important;
        }
    }
    @media (min-width: 376px) and (max-width: 414px) {
        .potential_buyer h2 {
            font-size: 45px !important;
        }
    }
</style>

<?php
/* Template Name: Customer Request List */
if(!is_user_logged_in() || !(current_user_can('administrator')||current_user_can('um_travel-advisor')) ) {
	wp_redirect(site_url('/login'));
	exit;
}
get_header();
if(have_posts()) {
	while(have_posts()) {
		the_post();
?>

<div class="potential_buyer">
	<div class="row">
		<div class="col-md-3 icon-md-3 cus_icon_desk">
			<div class="icon_user">
				<i class="fa fa-user" aria-hidden="true"></i>
			</div>
		</div>

		<div class="col-md-6">
			<h2>Potential Buyer's List</h2>
		</div>

		<div class="col-md-3">
			<div class="icon_user cus_icon_left_mob">
				<i class="fas fa-tasks"></i>
			</div>
		</div>
        <div class="col-md-3 icon-md-3 cus_icon_mob">
            <div class="icon_user cus_icon_right_mob">
                <i class="fa fa-user" aria-hidden="true"></i>
            </div>
        </div>
	</div>

	
</div>

<?php
		echo '<h1>'.get_the_title().'</h1>';
		$content=get_the_content();
		$content=apply_filters('the_content', $content);

		$request_list=$wpdb->get_results("SELECT contact_sales_agent.*,user.userid FROM `contact_sales_agent`,`user` WHERE `csa_user_name`=`user`.`username` AND DATE_SUB(CURDATE(), INTERVAL 1 MONTH)<=DATE(csa_created) ORDER BY `csa_created` DESC");
		$customer_request_list_html='<div id="request-list">';
		foreach($request_list as $row) {
			$customer_request_list_html.='
<section
    class="elementor-element elementor-element-958e3a8 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section animated fadeInUp"
    data-id="958e3a8" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;}">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-287e043 requestlink elementor-column elementor-col-25 elementor-top-column request-item" csa_id="'.$row->csa_id.'"
                data-id="287e043" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-33d5e30 elementor-view-stacked elementor-position-left elementor-shape-circle elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                            data-id="33d5e30" data-element_type="widget" data-widget_type="icon-box.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-icon-box-wrapper">
                                    <div class="elementor-icon-box-icon">
                                        <span class="elementor-icon elementor-animation-">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <div class="elementor-icon-box-content">
                                        <h3 class="elementor-icon-box-title">
                                            <span>'.$row->csa_user_name.'</span>
                                        </h3>
                                        <!--<p class="elementor-icon-box-description"><a
                                                href="mailto:customer@gmail.com">customer@gmail.com</a></p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-element elementor-element-6413d67 requestlink elementor-column elementor-col-50 elementor-top-column request-item" csa_id="'.$row->csa_id.'"
                data-id="6413d67" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-18eecd4 elementor-view-stacked elementor-position-left elementor-shape-circle elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                            data-id="18eecd4" data-element_type="widget" data-widget_type="icon-box.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-icon-box-wrapper">
                                    <div class="elementor-icon-box-icon">
                                        <span class="elementor-icon elementor-animation-">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <div class="elementor-icon-box-content">
                                        <h3 class="elementor-icon-box-title">
                                            <span>Request Date</span>
                                        </h3>
                                        <p class="elementor-icon-box-description">'.$row->csa_created.'</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--div class="elementor-element elementor-element-6bf4453 elementor-view-stacked elementor-position-left elementor-shape-circle elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                            data-id="6bf4453" data-element_type="widget" data-widget_type="icon-box.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-icon-box-wrapper">
                                    <div class="elementor-icon-box-icon">
                                        <span class="elementor-icon elementor-animation-">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <div class="elementor-icon-box-content">
                                        <h3 class="elementor-icon-box-title">
                                            <span>Travel Plan date</span>
                                        </h3>
                                        <p class="elementor-icon-box-description">05/May/2019</p>
                                    </div>
                                </div>
                            </div>
                        </div-->
                        <div class="elementor-element elementor-element-eba81b3 elementor-view-stacked elementor-position-left elementor-shape-circle elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                            data-id="eba81b3" data-element_type="widget" data-widget_type="icon-box.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-icon-box-wrapper">
                                    <div class="elementor-icon-box-icon">
                                        <span class="elementor-icon elementor-animation-">
                                            <i class="fa fa-plane" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <div class="elementor-icon-box-content">
                                        <h3 class="elementor-icon-box-title">
                                            <span>Destination Plan</span>
                                        </h3>
                                        <p class="elementor-icon-box-description">
											'.$row->csa_destination_plan.'
										</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-element elementor-element-b428c1f elementor-column elementor-col-25 elementor-top-column"
                data-id="b428c1f" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-d5e35c0 elementor-align-center requestlink elementor-widget elementor-widget-button"
                            data-id="d5e35c0" data-element_type="widget" data-widget_type="button.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-button-wrapper">
                                    <a href="'.site_url().'/travchat/user/addnewmember.php?user='.$row->userid.'"
                                        class="elementor-button-link elementor-button elementor-size-md" target="_blank"
                                        role="button">
                                        <span class="elementor-button-content-wrapper">
                                            <span class="elementor-button-icon elementor-align-icon-left">
                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                            </span>
                                            <span class="elementor-button-text">Message</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-cf31235 elementor-align-center elementor-widget elementor-widget-button"
                            data-id="cf31235" data-element_type="widget" data-widget_type="button.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-button-wrapper">
                                    <a href="'.site_url().'/travcust"
                                        class="elementor-button-link elementor-button elementor-size-md" target="_blank"
                                        role="button">
                                        <span class="elementor-button-content-wrapper">
                                            <span class="elementor-button-icon elementor-align-icon-left">
                                                <i class="fa fa-forward" aria-hidden="true"></i>
                                            </span>
                                            <span class="elementor-button-text">Travcust</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
		';
		}
		$customer_request_list_html.='</div>';
		$content = str_replace('[customer_request_list]',$customer_request_list_html,$content);
		$content .= '
		<script>
		$(function() {
			$("#request-list").on("click",".request-item",function() {
				window.location.href="'.site_url().'/customers-request-details/?csa_id="+$(this).attr("csa_id");
			});
			
			$("input[name=destination]").click(function(){
				if($(this).val()=="_all") {
					if($(this).is(":checked")) {
						$("input[name=destination]").prop("checked",false);;
						$("#destination_all").prop("checked",true);;
					}
				}
				else {
					if($("#destination_all").is(":checked")) {
						$("#destination_all").prop("checked",false);
					}
				}
			});
			
			$("#apply_filter").click(function(){
				
				var destination=[];
				$("input[name=destination]:checked").each(function(){
					destination.push($(this).val());
				});
				
				if($("#destination_other").is(":checked"))
					destination.push($("input[name=other]").val());
				
				var data = {
					"action": "customer_request_list_filter",
					"destination": destination,
					"orderby": $("input[name=orderby]:checked").val()
				};
				$.post("'.admin_url('admin-ajax.php').'", data, function(response) {
					if(response.status) {
						$("#request-list").html(response.html);
					}
				},"json");
			});
		});
		</script>';
		$content = str_replace( ']]>', ']]&gt;', $content );
		echo $content;
	}
} else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>