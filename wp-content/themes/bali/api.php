<?php 
require_once('../../../wp-load.php');
include('./phpinvoice.php');
global $wpdb;

define('ZOMATO_KEY', '0138313f73553d4524409f5b600a1ac8');

if($_GET['action']=='addnote' && !empty($_GET['tour_id']) && !empty($_POST['contents']))
{
	addnote($_GET['tour_id'], $_POST['contents']);
}
else if($_GET['action']=='addManualItem')
{
	if(! $tourid=filter_input(INPUT_GET, 'tour_id', FILTER_VALIDATE_INT, array('options'=>array('min_range'=>1,'max_range'=>999999)) ) )
		exit;
	$name=filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
	$attachment=filter_input(INPUT_POST, 'attachment', FILTER_SANITIZE_STRING);
	$attachment_type=filter_input(INPUT_POST, 'attachment_type', FILTER_SANITIZE_STRING);
	if(! $date=filter_input(INPUT_POST, 'date', FILTER_VALIDATE_INT, array('options'=>array('min_range'=>1,'max_range'=>99)) ) )
		exit;
	$time=filter_input(INPUT_POST, 'time', FILTER_SANITIZE_STRING);
	if(! $qty=filter_input(INPUT_POST, 'qty', FILTER_VALIDATE_INT, array('options'=>array('min_range'=>1,'max_range'=>99999)) ) )
		exit;
	$price=filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
	$description=filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
	
	$contents=array('name'=>$name,
		'attachment'=>$attachment,
		'attachment_type'=>$attachment_type,
		'date'=>$date,
		'time'=>$time,
		'qty'=>$qty,
		'price'=>$price,
		'description'=>$description,
	);
	
	addManualItem($tourid, $contents);
}
else if($_GET['action']=='delManualItem') {
	if(! $tourid=filter_input(INPUT_GET, 'tourid', FILTER_VALIDATE_INT, array('options'=>array('min_range'=>1,'max_range'=>99999)) ) )
		exit;
	if(! $id=filter_input(INPUT_GET, 'item', FILTER_VALIDATE_INT, array('options'=>array('min_range'=>1,'max_range'=>99999)) ) )
		exit;
	delManualItem($tourid, $id);
}
elseif($_GET['action']=='publish_tour' && !empty($_GET['tour_id']) && !empty($_GET['number_of_people']))
{
	
publish_tour($_GET['tour_id'], intval($_GET['number_of_people']),intval($_GET['number_of_days']),$_GET['hotel_location'],$_GET['ratingUrl'],$_GET['userid'],$_GET['enddate'],$_GET['netprice'],$_GET['username'],$_GET['userstatus'], intval($_GET['earn']));
}

else if($_GET['action']=='getlocation' && !empty($_GET['category']))
{
	if(empty($_GET['country_id']))
		get_location($_GET['category']);
	else
		get_location($_GET['category'],htmlspecialchars($_GET['country_id']));
}
else if($_GET['action']=='gettourlist' && !empty($_GET['page']))
{
	get_tourlist($_GET['page'],$_GET['sort'], $_GET['country_id']);
}
else if($_GET['action']=='get_location_id' && !empty($_GET['name']))
{
	get_location_id($_GET['name']);
}
else if($_GET['action']=='getitineray' && !empty($_GET['tour_id']))
{
	get_itinerary($_GET['tour_id']);
}
else if($_GET['action']=='check_city')
{
	$city_name=filter_input(INPUT_GET, 'city_name', FILTER_SANITIZE_STRING);
	echo search_locations($city_name)==false?0:1;
}
else if($_GET['action']=='get_restaurant_list')
{
	$city_name=filter_input(INPUT_GET, 'city_name', FILTER_SANITIZE_STRING);
	$category=filter_input(INPUT_GET, 'category', FILTER_VALIDATE_INT, array('options'=>array('min_range'=>1,'max_range'=>15)) );
	get_restaurant_list($city_name, $category);
}
else if($_GET['action']=='get_restaurants' && !empty($_GET['kw']))
{
	get_restaurants($_GET['kw']);
}
else if($_GET['action']=='get_restaurant_detail' && !empty($_GET['res_id']))
{
	get_restaurant_detail($_GET['res_id']);
}
else if($_GET['action']=='get_restaurant_review' && !empty($_GET['res_id']))
{
	get_restaurant_review($_GET['res_id']);
}
else if($_GET['action']=='getSingleCatZomatoData' && !empty($_GET['category']))
{
	get_single_cat_zomato_data($_GET['category']);
}else if($_GET['action']=='getinvoice' && !empty($_GET['agent_name']) && !empty($_GET['tourid']))
{
	getinvoice($_GET['agent_name'], $_GET['tourid']);
}
else if($_GET['action']=='downloadpdf' && !empty($_GET['tourid']))
{
	downloadpdf($_GET['tourid']);
}
else if($_GET['action']=='removehotelitem' && !empty($_GET['tourid']) && !empty($_GET['item']))
{
	removehotelitem(intval($_GET['tourid']),intval($_GET['item']));
}
else if($_GET['action']=='removecaritem' && !empty($_GET['tourid']) && !empty($_GET['item']))
{
	removecaritem(intval($_GET['tourid']),intval($_GET['item']));
}
else if($_GET['action']=='sharecount') {
	if(! $tourid=filter_input(INPUT_POST, 'tour_id', FILTER_VALIDATE_INT, array('options'=>array('min_range'=>1,'max_range'=>999999)) ) )
		exit;
	updateShareCount($tourid);
}
else
	echo FALSE;

function downloadpdf($tourid)
{
	$url='https://pdfcrowd.com/url_to_pdf/?width=410mm&height=350mm&?use_print_media=1';
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_REFERER, "http://travpart.com/English/wp-content/themes/bali/api.php?action=getitineray&tour_id={$tourid}&tp=1"); 
	$output = curl_exec($ch);
	curl_close($ch);
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename="Bidding Mail - BALI'.$tourid.'.pdf"');
	echo $output;
}



function addnote($tourid,$contents)
{
	global $wpdb;
	//$contents=json_decode(str_replace('\\','',$contents));
  	//var_dump($contents);
  	//return;
	if(!empty($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='notecontents' and  tour_id=".(int)$tourid)->meta_value))
	{
		if($wpdb->query("UPDATE `wp_tourmeta` SET `meta_value` = '".serialize($contents)."' where meta_key='notecontents' and tour_id=".(int)$tourid))
			echo TRUE;
		else
			echo FALSE;	
	}
	else if($wpdb->query("INSERT INTO `wp_tourmeta` (`meta_id`, `tour_id`, `meta_key`, `meta_value`) VALUES (NULL, '".$tourid."', 'notecontents', '".serialize($contents)."')"))
		echo TRUE;
	else
		echo FALSE;
  
  //echo $contents;
}

function addManualItem($tourid,$contents)
{
	global $wpdb;

	if($wpdb->query("INSERT INTO `wp_tourmeta` (`meta_id`, `tour_id`, `meta_key`, `meta_value`) VALUES (NULL, '".$tourid."', 'manualitem', '".serialize($contents)."')"))
		echo $wpdb->insert_id;
	else
		echo 0;
}

function delManualItem($tourid, $id)
{
	global $wpdb;
	
	if(is_user_logged_in()) {
		$current_user = wp_get_current_user();
		if($wpdb->get_var("SELECT COUNT(*) FROM `wp_tourmeta` WHERE `tour_id`={$tourid} AND meta_key='userid' AND meta_value='{$current_user->ID}'")==1) {
			if($wpdb->query("DELETE FROM `wp_tourmeta` WHERE `tour_id`='{$tourid}' AND meta_id='{$id}'")) {
				echo 1;
				return;
			}
		}
	}
	
	echo 0;
}

function publish_tour($tourid, $number_of_people,$number_of_days,$hotel_location,$rating_url,$user_id,$end_date,$netprice,$username,$userstatus, $earn=0)
{
	global $wpdb;

	if(empty($wpdb->get_row("SELECT * FROM `wp_tour` WHERE id='{$tourid}'")) || !is_user_logged_in())
		echo 0;
	else if(empty($wpdb->get_row("SELECT * FROM `wp_tour_sale` WHERE tour_id='{$tourid}'")))
	{
		$current_user = wp_get_current_user();
		$agent_username=$current_user->user_login;
		$urow=$wpdb->get_row("SELECT * FROM `user` WHERE `username`='{$agent_username}'");
		if(!empty($urow->access) && $urow->access==1)
		{
			//if($wpdb->query("INSERT INTO `wp_tour_sale`(`tour_id`, `number_of_people`, `price`, `agent_username`) VALUES ({$tourid}, {$number_of_people}, {$total_price}, '{$agent_username}')"))
			if($wpdb->query("INSERT INTO `wp_tour_sale`(`tour_id`, `number_of_people`,`number_of_days`,`ratingurl`,`hotellocation`,`userid`,`username`,`enddate`,`netprice`,`userstatus`) VALUES ('{$tourid}', '{$number_of_people}', '{$number_of_days}','{$rating_url}','{$hotel_location}','{$user_id}','{$username}','{$end_date}','{$netprice}','{$userstatus}')"))
			{
				//send notification to admin email
				$notification_content='<p>Dear Travpart,
				<p>We just received a tour packages submission request.</p>
				<p>Please check their tour package here <a href="'.site_url().'/tour-details/?bookingcode=BALI'.$tourid.'">BALI'.$tourid.'</a></p>';
				wp_mail(get_option('admin_email') , 'Travpart - Received a tour package submission', $notification_content);
				
				/*
				$budget_used=$wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m')");
				if($earn==1 && floatval($budget_used)<floatval(get_option('budget_limit')) )
				{
					if($wpdb->query("INSERT INTO `rewards` (`userid`, `tourid`, `commission`, `type`, `create_time`) VALUES ('{$urow->userid}', '{$tourid}', '5.0', 'publish', NOW())") )
					{
						//update balance
						if( empty($wpdb->get_row("SELECT * FROM `balance` WHERE userid='{$urow->userid}'")) )
						{
							$wpdb->query("INSERT INTO `balance` (`userid`, `balance`, `update_time`) VALUES ('{$urow->userid}', '5.0', CURRENT_TIMESTAMP)");
						}
						else
						{
							$wpdb->query("UPDATE `balance` SET balance=balance+5.0 where userid='{$urow->userid}'");
						}
						
						//add invite rewards
						if( $wpdb->get_var("SELECT COUNT(*) as publish_count FROM `rewards` WHERE userid='{$urow->userid}' AND type='publish'")==1 )
						{
							$inviteby=$wpdb->get_row("SELECT * FROM `user` WHERE `username`='{$urow->inviteby}'");
							if( !empty($inviteby->userid) )
							{
								$invite_commisson=0;
								if( $wpdb->get_var("SELECT COUNT(*) as publish_count FROM `rewards` WHERE userid='{$inviteby->userid}' AND type='invite'")<5 )
									$invite_commisson=1.0;
								else
									$invite_commisson=0.75;
								$wpdb->query("INSERT INTO `rewards` (`userid`, `tourid`, `commission`, `type`, `create_time`) VALUES ('{$inviteby->userid}', '{$tourid}', '{$invite_commisson}', 'invite', NOW())");
							}
							if( empty($wpdb->get_row("SELECT * FROM `balance` WHERE userid='{$inviteby->userid}'")) )
							{
								$wpdb->query("INSERT INTO `balance` (`userid`, `balance`, `update_time`) VALUES ('{$inviteby->userid}', '{$invite_commisson}', CURRENT_TIMESTAMP)");
							}
							else
							{
								$wpdb->query("UPDATE `balance` SET balance=balance+{$invite_commisson} where userid='{$inviteby->userid}'");
							}
						}
						echo 1;
					}
					else
						echo 0;
				}
				else
					echo 1;
				*/
				echo 1;
			}
			else
				echo 0;
		}
		else
			echo 3;
	}
	else
		echo 2;
}




function get_location($category, $country_id = 'country:93')
{
    global $wpdb;

    $sygic_api_limit = $wpdb->get_row("SELECT option_value FROM `travpart`.`wp_options` WHERE `option_name`='sygic_api_limit'")->option_value;
    $sygic_api_used = $wpdb->get_row("SELECT option_value FROM `travpart`.`wp_options` WHERE `option_name`='sygic_api_used'")->option_value;

    if ($sygic_api_limit > $sygic_api_used) {
        $url = "https://api.sygictravelapi.com/1.1/en/places/list?parents={$country_id}&categories={$category}&limit=10";
        $headers = array(
            'Content-type: application/json',
            'Accept: application/json',
            'x-api-key: lO2pTxLGsm7BBvziH3wJE3kFH1tndHaF8iBiJOkt'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $data = curl_exec($ch);
        if (!curl_error($ch)) {
            echo $data;
        }
        curl_close($ch);
        $wpdb->query("UPDATE `travpart`.`wp_options` SET option_value=option_value+1 WHERE `option_name`='sygic_api_used'");
    } else {
        echo json_encode(array('error_msg' => 'The API usage has been used up'));
    }
}

function get_tourlist($page=1, $sort='top_sellers', $country_id='country:93')
{
	global $wpdb;
	
	$sygic_api_limit=$wpdb->get_row("SELECT option_value FROM `travpart`.`wp_options` WHERE `option_name`='sygic_api_limit'")->option_value;
	$sygic_api_used=$wpdb->get_row("SELECT option_value FROM `travpart`.`wp_options` WHERE `option_name`='sygic_api_used'")->option_value;
	
	if($sygic_api_limit>$sygic_api_used)
	{
	if($sort!='price' && $sort!='rating')
		$sort='top_sellers';
	if($sort=='price' || $sort=='rating')
		$sort_direction='desc';
	else
		$sort_direction='asc';
	if(empty($country_id))
		$country_id='country:93';
	$url="https://api.sygictravelapi.com/1.1/en/tours/viator?parent_place_id={$country_id}&page={$page}&sort_by={$sort}&sort_direction={$sort_direction}";
	$headers=array(
		'Content-type: application/json',
		'Accept: application/json',
		'x-api-key: lO2pTxLGsm7BBvziH3wJE3kFH1tndHaF8iBiJOkt'
		);
	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	$data=curl_exec($ch);
	if(!curl_error($ch))
	{
		echo $data;
	}
	curl_close($ch);
	$wpdb->query("UPDATE `travpart`.`wp_options` SET option_value=option_value+1 WHERE `option_name`='sygic_api_used'");
	}
	else
	{
		echo json_encode(array('error_msg'=>'The API usage has been used up'));
	}
}

function get_location_id($name)
{
	global $wpdb;
	$name=htmlspecialchars($name);
	echo json_encode($wpdb->get_results("SELECT * FROM `sygic_country` WHERE `name` LIKE '{$name}%' ORDER BY `name` ASC LIMIT 5", ARRAY_A));
}

function get_single_cat_zomato_data($category){
	$url="https://developers.zomato.com/api/v2.1/search?category={$category}";
	$headers=array(
		'Content-type: application/json',
		'Accept: application/json',
		'user-key: 0138313f73553d4524409f5b600a1ac8'
		);
	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	$data=curl_exec($ch);
	if(!curl_error($ch))
	{
		echo $data;
	}
	curl_close($ch);	
}

function currency_converter($scur)
{
	if(get_option("currency_{$scur}"))
		return get_option("currency_{$scur}");

	$url="http://api.k780.com/?app=finance.rate&scur={$scur}&tcur=IDR&appkey=39082&sign=0e5b567a08c208094b43c900947c33cf";
	$headers=array(
		'Content-type: application/json',
		'Accept: application/json'
		);
	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	$data=curl_exec($ch);
	if(!curl_error($ch))
	{
		$r=json_decode($data, true);
	}
	curl_close($ch);

	if( !empty($r['result']['rate']) ) {
		update_option("currency_{$scur}", $r['result']['rate']);
		return $r['result']['rate'];
	}
	else
		return false;
}

function search_locations($city_name)
{
	$url="https://developers.zomato.com/api/v2.1/locations?query=".urlencode($city_name);
	$headers=array(
		'Content-type: application/json',
		'Accept: application/json',
		'user-key: '.ZOMATO_KEY
		);
	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	$data=curl_exec($ch);
	if(!curl_error($ch))
	{
		$r=json_decode($data, true);
	}
	curl_close($ch);

	if( !empty($r['location_suggestions'][0]) )
		return $r['location_suggestions'][0];
	else
		return false;
}

function get_restaurant_list($city_name=null, $category=null)
{
	$search_option='';
	if(!empty($city_name)) {
		$city=search_locations($city_name);
		if( !empty($city) && !empty($city['entity_id']) && !empty($city['entity_type']) )
			$search_option.="entity_id={$city['entity_id']}&entity_type={$city['entity_type']}";
	}
	if(!empty($category)) {
		if(empty($search_option))
			$search_option.='category='.$category;
		else
			$search_option.='&category='.$category;
	}

	$url="https://developers.zomato.com/api/v2.1/search?{$search_option}";
	$headers=array(
		'Content-type: application/json',
		'Accept: application/json',
		'user-key: '.ZOMATO_KEY
		);
	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	$data=curl_exec($ch);
	if(!curl_error($ch))
	{
		$data=json_decode($data, true);
		foreach($data['restaurants'] as &$item) {
			if($item['restaurant']['currency']=='Rs.') {
				$item['restaurant']['average_cost_for_two']=round($item['restaurant']['average_cost_for_two']*currency_converter('INR'));
				$item['restaurant']['currency']='IDR';
			}
			else if($item['restaurant']['currency']=='£') {
				$item['restaurant']['average_cost_for_two']=round($item['restaurant']['average_cost_for_two']*currency_converter('GBP'));
				$item['restaurant']['currency']='IDR';
			}
			else if($item['restaurant']['currency']=='$')
				continue;
			else if(currency_converter($item['restaurant']['currency'])) {
				$item['restaurant']['average_cost_for_two']=round($item['restaurant']['average_cost_for_two']*currency_converter($item['restaurant']['currency']));
				$item['restaurant']['currency']='IDR';
			}
		}
		echo json_encode($data);
	}
	curl_close($ch);
}

function get_restaurants($rest_name)
{
	$url="https://developers.zomato.com/api/v2.1/search?q={$rest_name}";
	$headers=array(
		'Content-type: application/json',
		'Accept: application/json',
		'user-key: '.ZOMATO_KEY
		);
	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	$data=curl_exec($ch);
	if(!curl_error($ch))
	{
		echo $data;
	}
	curl_close($ch);
}

function get_restaurant_detail($res_id)
{
	$url="https://developers.zomato.com/api/v2.1/restaurant?res_id={$res_id}";
	$headers=array(
		'Content-type: application/json',
		'Accept: application/json',
		'user-key: '.ZOMATO_KEY
		);
	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	$data=curl_exec($ch);
	if(!curl_error($ch))
	{
		echo $data;
	}
	curl_close($ch);
}

function get_restaurant_review($res_id)
{
	$url="https://developers.zomato.com/api/v2.1/reviews?res_id={$res_id}&count=10";
	$headers=array(
		'Content-type: application/json',
		'Accept: application/json',
		'user-key: '.ZOMATO_KEY
		);
	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	$data=curl_exec($ch);
	if(!curl_error($ch))
	{
		echo $data;
	}
	curl_close($ch);
}

function getinvoice($agent_name, $tourid)
{
	global $wpdb;
	$spotdates=array();
	$spotnames=array();
	$nindex=0;
	$total_price=0;
	if(!empty($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=".(int)$tourid)->meta_value))
	{
		extract(unserialize($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=".(int)$tourid)->meta_value),EXTR_OVERWRITE);
		//var_dump(unserialize($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=".(int)$tourid)->meta_value));//,EXTR_OVERWRITE);
		$spotdates=empty($spotdate)?array():explode(':,',rtrim($spotdate,':'));
		$spotnames=empty($spotname)?array():explode(':,',rtrim($spotname,':'));
		foreach($spotdates as &$t)
		{
			$t=empty($t)?'':explode('/',$t);
			$t=empty($t)?'':($t[2].'-'.$t[0].'-'.$t[1]);
		}
	}
  	else
	{
		//return;
	}

	$tour_information=$wpdb->get_row("SELECT * FROM `wp_tour` WHERE `id` = ".(int)$tourid);
	
	$invoice = new phpinvoice();
  /* Header Settings */
  $invoice->setLogo("images/logo.jpg");
  $invoice->setColor("#007fff");
  $invoice->setType("Sale Invoice");
  //$invoice->setReference("INV-55033645");
  $invoice->setDate(date('M dS ,Y',time()));
  $invoice->setTime(date('h:i:s A',time()));
  $invoice->setDue(date('M dS ,Y',strtotime('+3 months')));
  $invoice->setFrom(array("TOURFROMBALI","TOURFROMBALI","Jl. Dewi Sartika No.133","RT.5/RW.2, Cawang, Kramatjati","Kota Jakarta Timur","Daerah Khusus Ibukota Jakarta", "Travel advisor: {$agent_name}"));
  $invoice->setTo(array(google_translate($tour_information->first_name.$tour_information->last_name,'en'),"",google_translate($tour_information->addrs,'en'),
    google_translate($tour_information->city.' , '.$tour_information->state,'en').' '.$tour_information->zip, google_translate($tour_information->country,'en'),
    $tour_information->email, $tour_information->phone_number ));
  /* Adding Items in table */

		//meal section
		if(!empty($meal_type))
		{
            $TravcustMeal=unserialize(get_option('_cs_travcust_meal'));
			$meal_types=explode(',', $meal_type);
			$meal_dates=explode(',', $meal_date);
			$meal_times=explode(',', $meal_time);
			for($j=0; $j<count($meal_types)-1; $j++)
			{
				if(count(explode('-',$meal_types[$j]))>1)
				{
					$subitems=$TravcustMeal[$meal_types[$j]-1]['subitem'];
					$subitem_types=explode('-',$meal_types[$j]);
					for($subi=1; $subi<count($subitem_types); $subi++)
					{
						if($subitem_types[$subi]==1)
						{
							$price=round($subitems[$subi-1]['price']*get_option('person_price_ration')*get_option("_cs_currency_USD"),2);
							$total_price+=$price;							
							$invoice->addItem($subitems[$subi-1]['name'],$subitems[$subi-1]['type'],1,0,$price,$subitems[$subi-1]['price']);
						}
					}
				}
			}
		}

		//car section
		if(!empty($car_type))
		{
			$TravcustCar=unserialize(get_option('_cs_travcust_car'));
			$car_types=explode(',', $car_type);
			$car_dates=explode(',', $car_date);
			$car_times=explode(',', $car_time);
			for($j=0; $j<count($car_types)-1; $j++)
			{
				if(floor($car_types[$j])+0.5==$car_types[$j])
				{
					$half_price=0.5;
					$car_types[$j]=floor($car_types[$j]);
				}
				else
					$half_price=1;
				$price=round($TravcustCar[$car_types[$j]-1]['price']*get_option('person_price_ration')*$half_price*get_option("_cs_currency_USD"),2);
				$total_price+=$price;
				$item=$TravcustCar[$car_types[$j]-1]['item'];
				if($half_price==0.5)
					$item.=' (Half day or Airport Transfer)';
				$invoice->addItem($item,"",1,$price*0.1,$price,$TravcustCar[$car_types[$j]-1]['price']);
			}
		}
		
		//guide section
		if(!empty($popupguide_type))
		{
			$TravcustPopupguide=unserialize(get_option('_cs_travcust_popupguide'));
			$popupguide_types=explode(',', $popupguide_type);
			$popupguide_dates=explode(',', $popupguide_date);
			$popupguide_times=explode(',', $popupguide_time);
			for($j=0; $j<count($popupguide_types)-1; $j++)
			{
				$price+=round($TravcustPopupguide[$popupguide_types[$j]-1]['price']*get_option('person_price_ration')*get_option("_cs_currency_USD"),2);
				$total_price+=$price;
				$invoice->addItem(google_translate($TravcustPopupguide[$popupguide_types[$j]-1]['item'],'en'),"",1,$price*0.1,$price,$TravcustPopupguide[$popupguide_types[$j]-1]['price']);
			}
		}
		
		//spa section
		if(!empty($spa_type))
		{
			$TravcustSpa=unserialize(get_option('_cs_travcust_spa'));
			$spa_types=explode(',', $spa_type);
			$spa_dates=explode(',', $spa_date);
			for($j=0; $j<count($spa_types)-1; $j++)
			{
				if(count(explode('-',$spa_types[$j]))>1)
				{
					$subitems=$TravcustSpa[$spa_types[$j]-1]['subitem'];
					$subitem_types=explode('-',$spa_types[$j]);
					for($subi=1; $subi<count($subitem_types); $subi++)
					{
						if($subitem_types[$subi]==1)
						{
							$price=round($subitems[$subi-1]['price']*get_option('person_price_ration')*get_option("_cs_currency_USD"),2);
							$total_price+=$price;
							$invoice->addItem(google_translate($subitems[$subi-1]['name'],'en'),"",1,$price*0.1,$price,$subitems[$subi-1]['price']);
						}
					}
				}
			}
		}
		
		//boat section
		if(!empty($boat_type))
		{
			$TravcustBoat=unserialize(get_option('_cs_travcust_boat'));
			$boat_types=explode(',', $boat_type);
			$boat_dates=explode(',', $boat_date);
			for($j=0; $j<count($boat_types)-1; $j++)
			{
				//show boat sub items
					if(count(explode('-',$boat_types[$j]))>1)
					{
						$subitems=$TravcustBoat[$boat_types[$j]-1]['subitem'];
						$subitem_types=explode('-',$boat_types[$j]);
						for($subi=1; $subi<count($subitem_types); $subi++)
						{
							if($subitem_types[$subi]==1)
							{
								$price=round($subitems[$subi-1]['price']*get_option('person_price_ration')*get_option("_cs_currency_USD"),2);
								$total_price+=$price;
								$invoice->addItem(google_translate($subitems[$subi-1]['name'],'en'),"",1,$price*0.1,$price,$subitems[$subi-1]['price']);
							}
						}
					}
			}
		}
		
	$ExtraActivity=unserialize(get_option('_cs_extraactivity'));
	if(!empty($ExtraActivity))
	{
		$i=0;
      	foreach($ExtraActivity as $t)
		{
			if($eainums[$i]>0)
			{
				$price=round($t['price']*$eainums[$i]*get_option('person_price_ration')*get_option("_cs_currency_USD"),2);
				$total_price+=$price;
				$invoice->addItem(google_translate($t['item'],'en'),"",1,$price*0.1,$price,$t['price']);
			}
			$i++;
		}
	}
	
	/*$tourinfos=$wpdb->get_results("SELECT * FROM `wp_hotel` WHERE `tour_id` = ".(int)$tourid);
	foreach($tourinfos as $tourinfo)
	{
		$price=round($tourinfo->total*get_option('person_price_ration'),2);
		$total_price+=$price;
		$invoice->addItem($tourinfo->name,"",1,$price*0.01,$price,round($tourinfo->total*get_option('person_price_ration')));
	}*/
		
  $invoice->addItem("Tour Guide Fee","2 DAYS Service",1,0,0,0);
  
  $price=round(50000*get_option('person_price_ration')*get_option("_cs_currency_USD"),2);
  $invoice->addItem("Tips Guide","2 DAYS Service",1,$price*0.1,$price,50000);

  $price=round(50000*get_option('person_price_ration')*get_option("_cs_currency_USD"),2);
  $invoice->addItem("Drive Fee","",1,$price*0.1,$price,50000);

  /* Add totals */
  $invoice->addTotal("Total",$total_price);
  $invoice->addTotal("VAT 1%",$total_price*0.1);
  $invoice->addTotal("Total due",$total_price*1.1,true);
  /* Set badge */ 
  $invoice->addBadge("PT.TUR INDO BALI");
  /* Add title */
  //$invoice->addTitle("Important Notice");
  /* Add Paragraph */
  //$invoice->addParagraph("No item will be replaced or refunded if you don't have the invoice with you. You can refund within 2 days of purchase.");
  /* Set footer note */
  $invoice->setFooternote("TOURFROMBALI");
  /* Render */
  $invoice->render('invoice.pdf','I'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
}


function get_itinerary($tourid)
{
	global $wpdb;
	$itinerary='';
	$notecontents=array();
	$spotdates=array();
	$spotnames=array();
	$nindex=0;
	$total_price=0;
	if(!empty($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=".(int)$tourid)->meta_value))
	{
		extract(unserialize($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=".(int)$tourid)->meta_value),EXTR_OVERWRITE);
		//var_dump(unserialize($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=".(int)$tourid)->meta_value));//,EXTR_OVERWRITE);
		$spotdates=empty($spotdate)?array():explode(':,',rtrim($spotdate,':'));
		$spotnames=empty($spotname)?array():explode(':,',rtrim($spotname,':'));
	}
  	else
	{
		return;
	}
	if(!empty($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='notecontents' and tour_id=".(int)$tourid)->meta_value))
		$notecontents=unserialize($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='notecontents' and tour_id=".(int)$tourid)->meta_value);
		//var_dump(unserialize($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='notecontents' and tour_id=".(int)$tourid)->meta_value));
         
	$itinerary.='<h2>Booking code：<span>BALI'.$tourid.'</span></h2>';
	$itinerary.='<table> <thead> <tr style="background:#368a8c;padding:20px"> <th><h3>
Date</h3></th> <th><h3>Description</h3></th> <th><h3>price unit</h3></th> </tr> </thead>';
	for($i=0; !empty($spotdates[$i]); $i++)
	{
		$itinerary.='<tr>
						<th><h2>Day '.($i+1).'<br><p>'.$spotdates[$i].'</p></h2></th> <th></th> <th></th>
					 </tr>';
		$itinerary.='<tr>
						<td> <img src="https://www.tourfrombali.cn/test-box/wp-content/themes/bali/images/images/Pura-tanah-lot.jpg" style="width:200px;height:200px;" /> </td>
						<td> <h4> Place to Visit:</h4><br/>
							'.(empty($spotnames[$i])?'':$spotnames[$i]).'
						</td>
						<td> <p>$<span>0</span></p> </td>
                    </tr>';
		$itinerary.='<tr>
						<td>Note:</td><td>'.$notecontents[$nindex++].'</td>
					</tr>';
      	//recommended place section
		if(!empty($place_name))
		{
			$place_names=explode('--', $place_name);
			$place_imgs=explode(',', $place_img);
			$place_dates=explode(',', $place_date);
			$place_times=explode(',', $place_time);
			for($j=0; $j<count($place_names)-1; $j++)
			{
				if($spotdates[$i]==$place_dates[$j])
				{
					$itinerary.='<tr>
						<td>
							<img width="80" height="80" src="'.$place_imgs[$j].'" />
						</td>
						<td>
							<p>'.$place_names[$j].'</p>
							<p>'.$place_times[$j].'</p>
						</td>
						<td></td>
					</tr>';
					$itinerary.='<tr>
						<td>Note:</td> <td>'.$notecontents[$nindex++].'</td>
					</tr>';
				}
			}
		}
		//tour list section
		if(!empty($tourlist_name))
		{
			$tourlist_names=explode('--', $tourlist_name);
			$tourlist_imgs=explode(',', $tourlist_img);
			$tourlist_prices=explode('--', $tourlist_price);
			$tourlist_dates=explode(',', $tourlist_date);
			$tourlist_times=explode(',', $tourlist_time);
			for($j=0; $j<count($tourlist_names)-1; $j++)
			{
				if($spotdates[$i]==$tourlist_dates[$j])
				{
					$total_price+=ceil($tourlist_prices[$j]/get_option("_cs_currency_USD"));
					$itinerary.='<tr>
						<td>
							<img width="80" height="80" src="'.$tourlist_imgs[$j].'" />
						</td>
						<td>
							<p>'.$tourlist_names[$j].'</p>
							<p>'.$tourlist_times[$j].'</p>
						</td>
						<td>$<span>'.$tourlist_prices[$j].'</span></td>
					</tr>';
					$itinerary.='<tr>
						<td>Note:</td> <td>'.$notecontents[$nindex++].'</td>
					</tr>';
				}
			}
		}
		//meal section
		if(!empty($meal_type))
		{
            $TravcustMeal=unserialize(get_option('_cs_travcust_meal'));
			$meal_types=explode(',', $meal_type);
			$meal_dates=explode(',', $meal_date);
			$meal_times=explode(',', $meal_time);
			for($j=0; $j<count($meal_types)-1; $j++)
			{
				if($spotdates[$i]==$meal_dates[$j])
				{
					$itinerary.='<tr>
									<td><img width="80" height="80" src="'.$TravcustMeal[$meal_types[$j]-1]['img'].'" /></td>
									<td>
										<p>'.$TravcustMeal[$meal_types[$j]-1]['item'].'</p>
										<p>'.$meal_times[$j].'</p>
									</td>
									<td></td>
								</tr>';
					$itinerary.='<tr> <td>Area:</td> <td>'.$TravcustMeal[$meal_types[$j]-1]['area'].' </td> </tr>
								 <tr> <td>Vendor name:</td> <td>'.$TravcustMeal[$meal_types[$j]-1]['vendor'].' </td> </tr>
								 <tr> <td>Phone number(email):</td> <td>'.$TravcustMeal[$meal_types[$j]-1]['phone'].' </td> </tr> ';
					$itinerary.='<tr><td></td></tr><tr>
						<td>Note:</td><td>'.$notecontents[$nindex++].'</td>
					</tr>';
					//show meal sub items
					if(count(explode('-',$meal_types[$j]))>1)
					{
						$subitems=$TravcustMeal[$meal_types[$j]-1]['subitem'];
						$subitem_types=explode('-',$meal_types[$j]);
						for($subi=1; $subi<count($subitem_types); $subi++)
						{
							$total_price+=$subitems[$subi-1]['price'];
							if($subitem_types[$subi]==1)
							{
								$itinerary.='<tr>
						<td>
							<img width="80" height="80" src="'.$subitems[$subi-1]['img'].'" />
						</td>
						<td>
							<p>'.$subitems[$subi-1]['name'].'</p>
							<p>Type: '.$subitems[$subi-1]['type'].'</p>
							<p>'.$meal_times[$j].'</p>
						</td>
						<td>
							<p>$<span>'.round($subitems[$subi-1]['price']*get_option('person_price_ration')*get_option("_cs_currency_USD"), 2).'</span></p>
						</td>
						</tr>';
								$itinerary.='<tr>
												<td>Note:</td> <td>'.$notecontents[$nindex++].'</td>
											</tr>';
							}
						}
					}
				}
			}
		}
		//car section
		if(!empty($car_type))
		{
			$TravcustCar=unserialize(get_option('_cs_travcust_car'));
			$car_types=explode(',', $car_type);
			$car_dates=explode(',', $car_date);
			$car_times=explode(',', $car_time);
			for($j=0; $j<count($car_types)-1; $j++)
			{
				if($spotdates[$i]==$car_dates[$j])
				{
					if(floor($car_types[$j])+0.5==$car_types[$j])
					{
						$half_price=0.5;
						$car_types[$j]=floor($car_types[$j]);
					}
					else
						$half_price=1;
					$total_price+=$TravcustCar[$car_types[$j]-1]['price']*get_option('person_price_ration')*$half_price;
					$item=$TravcustCar[$car_types[$j]-1]['item'];
					if($half_price==0.5)
						$item.=' (Half day or Airport Transfer)';
					$itinerary.='<tr>
						<td>
							<img width="80" height="80" src="'.$TravcustCar[$car_types[$j]-1]['img'].'" />
						</td>
						<td>
							<p>'.$item.'</p>
							<p>'.$car_times[$j].'</p>
						</td>
						<td> <p>$<span>'.round($TravcustCar[$car_types[$j]-1]['price']*get_option('person_price_ration')*$half_price*get_option("_cs_currency_USD"),2).'</span></p> </td>
						</tr>';
                  	$itinerary.='<tr> <td>Area:</td> <td>'.$TravcustCar[$car_types[$j]-1]['area'].' </td> </tr>
								 <tr> <td>Vendor name:</td> <td>'.$TravcustCar[$car_types[$j]-1]['vendor'].' </td> </tr>
								 <tr> <td>Phone number(email):</td> <td>'.$TravcustCar[$car_types[$j]-1]['phone'].' </td> </tr> ';
					$itinerary.='<tr>
						<td>Note:</td> <td>'.$notecontents[$nindex++].'</td>
						</tr>';
				}
			}
		}
		//guide section
		if(!empty($popupguide_type))
		{
			$TravcustPopupguide=unserialize(get_option('_cs_travcust_popupguide'));
			$popupguide_types=explode(',', $popupguide_type);
			$popupguide_dates=explode(',', $popupguide_date);
			$popupguide_times=explode(',', $popupguide_time);
			for($j=0; $j<count($popupguide_types)-1; $j++)
			{
				if($spotdates[$i]==$popupguide_dates[$j])
				{
					$total_price+=$TravcustPopupguide[$popupguide_types[$j]-1]['price'];
					$itinerary.='<tr>
						<td>
							<img width="80" height="80" src="'.$TravcustPopupguide[$popupguide_types[$j]-1]['img'].'" />
						</td>
						<td>
							<p>'.$TravcustPopupguide[$popupguide_types[$j]-1]['item'].'</p>
							<p>'.$popupguide_times[$j].'</p>
						</td>
						<td> <p>$<span>'.round($TravcustPopupguide[$popupguide_types[$j]-1]['price']*get_option('person_price_ration')*get_option("_cs_currency_USD"),2).'</span></p> </td>
						</tr>';
                   	$itinerary.='<tr> <td>Area:</td> <td>'.$TravcustPopupguide[$popupguide_types[$j]-1]['area'].' </td> </tr>
								 <tr> <td>Vendor name:</td> <td>'.$TravcustPopupguide[$popupguide_types[$j]-1]['vendor'].' </td> </tr>
								 <tr> <td>Phone number(email):</td> <td>'.$TravcustPopupguide[$popupguide_types[$j]-1]['phone'].' </td> </tr> ';
					$itinerary.='<tr>
						<td>Note:</td> <td>'.$notecontents[$nindex++].'</td>
						</tr>';
				}
			}
		}
		//spa section
		if(!empty($spa_type))
		{
			$TravcustSpa=unserialize(get_option('_cs_travcust_spa'));
			$spa_types=explode(',', $spa_type);
			$spa_dates=explode(',', $spa_date);
			for($j=0; $j<count($spa_types)-1; $j++)
			{
				if($spotdates[$i]==$spa_dates[$j])
				{
					$itinerary.='<tr>
						<td>
							<img width="80" height="80" src="'.$TravcustSpa[$spa_types[$j]-1]['img'].'" />
						</td>
						<td>
							<p>'.$TravcustSpa[$spa_types[$j]-1]['item'].'</p>
							<p>'.$spa_times[$j].'</p>
						</td>
						<td> <p>$<span>'.round($TravcustSpa[$spa_types[$j]-1]['price']*get_option('person_price_ration')*get_option("_cs_currency_USD"),2).'</span></p> </td>
						</tr>';
                  	$itinerary.='<tr> <td>Area:</td> <td>'.$TravcustSpa[$spa_types[$j]-1]['area'].' </td> </tr>
								 <tr> <td>Vendor name:</td> <td>'.$TravcustSpa[$spa_types[$j]-1]['vendor'].' </td> </tr>
								 <tr> <td>Phone number(email):</td> <td>'.$TravcustSpa[$spa_types[$j]-1]['phone'].' </td> </tr> ';
					$itinerary.='<tr>
						<td>Note:</td> <td>'.$notecontents[$nindex++].'</td>
						</tr>';
					//show spa sub items
					if(count(explode('-',$spa_types[$j]))>1)
					{
						$subitems=$TravcustSpa[$spa_types[$j]-1]['subitem'];
						$subitem_types=explode('-',$spa_types[$j]);
						for($subi=1; $subi<count($subitem_types); $subi++)
						{
							if($subitem_types[$subi]==1)
							{
								$total_price+=$subitems[$subi-1]['price'];
								$itinerary.='<tr>
									<td>
										<img width="80" height="80" src="'.$subitems[$subi-1]['img'].'" />
									</td>
									<td>
									<p>'.$subitems[$subi-1]['name'].'</p>
									<p>'.$spa_times[$j].'</p>
									</td>
									<td> <p>$<span>'.round($subitems[$subi-1]['price']*get_option('person_price_ration')*get_option("_cs_currency_USD"),2).'</span></p> </td>
								</tr>';
								$itinerary.='<tr>
								<td>Note:</td> <td>'.$notecontents[$nindex++].'</td>
								</tr>';
							}
						}
					}
				}
			}
		}
	}

	//extra activity list
	$ExtraActivity=unserialize(get_option('_cs_extraactivity'));
	if(!empty($ExtraActivity))
	{
		$i=0;
      	foreach($ExtraActivity as $t)
		{
			if($eainums[$i]>0)
			{
				$itinerary.='<tr>
                    <td>
                        '.((!empty($t['img']))?('<span><img width=84 height=84 src="'.$t['img'].'"></span>'):'').'
                    </td>
                    <td> 
                    	<h1>'.$t['item'].'</h1>
                        <p>'.$eaidates[$i].' '.$eaitimes[$i].'</p>
                    </td>
					<td> <p>$<span>'.round($t['price']*$eainums[$i]*get_option('person_price_ration')*get_option("_cs_currency_USD"),2).'</span></p> </td>
				</tr>';
				$itinerary.='<tr>
								<td>Note:</td> <td>'.$notecontents[$nindex++].'</td>
							</tr>';
				$total_price+=$t['price']*$eainums[$i];
			}
			$i++;
		}
	}
	
	//hotel information
	//$tourinfo=$wpdb->get_row("SELECT * FROM `wp_tour` where id=".(int)$tourid);
	$tourinfos=$wpdb->get_results("SELECT * FROM `wp_hotel` WHERE `tour_id` = ".(int)$tourid);
	foreach($tourinfos as $tourinfo)
	{
		$itinerary.='<tr>
                    <td>
                        <span><img src="'.(empty($tourinfo->img)?'https://www.travpart.com/English/wp-content/themes/bali/images/nohotel.png':$tourinfo->img).'"></span>
						<h4>'.(empty($tourinfo->name)?"You haven't chosen hotel!":$tourinfo->name).'</h4>
                      </td>
                    <td>';
		if(!empty($tourinfo->name))
		{
			$itinerary.='<h4>'.$tourinfo->room_type.'</h4>
							<p>From '.$tourinfo->start_date.' to '.$tourinfo->end_date.'</p>';
			foreach(unserialize($tourinfo->valueAdds) as $t)
			{
				$itinerary.='<p>'.$t.'</p>';
			}
		}
        $itinerary.='</td>
					<td>';
					if($tp!=1)
						$itinerary.='<p>$<span>'.(empty($tourinfo->total)?'0':round($tourinfo->total,2)).'</span></p>';
					$itinerary.='</td>
				</tr>';
		$itinerary.='<tr>
								<td>Note:</td> <td>'.$notecontents[$nindex++].'</td>
							</tr>';
		$total_price+=$tourinfo->total;
	}
	
	$itinerary.='<tr>
					<td>Total: $'.round($total_price*get_option('person_price_ration')*get_option("_cs_currency_USD"),2).'</td>
				</tr>';
	$itinerary.='</table>';
	echo $itinerary;
}

function removehotelitem($tourid, $item)
{
	global $wpdb;
	if($wpdb->query("DELETE FROM `wp_hotel` WHERE tour_id={$tourid} AND id={$item}"))
		echo 1;
	else
		echo 0;
}

function removecaritem($tourid, $item)
{
	global $wpdb;
	if($wpdb->query("DELETE FROM `wp_cars` WHERE tour_id={$tourid} AND id={$item}"))
		echo 1;
	else
		echo 0;
}

function updateShareCount($tourid)
{
	global $wpdb;
	if(($wpdb->get_var("SELECT COUNT(*) FROM `wp_tour` WHERE id=".$tourid))==1) {
		if(empty($wpdb->get_var("SELECT meta_value FROM `wp_tourmeta` where meta_key='sharecount' and  tour_id=".$tourid)))
			$wpdb->query("INSERT INTO `wp_tourmeta` (`tour_id`, `meta_key`, `meta_value`) VALUES ('{$tourid}', 'sharecount', '1')");
		else
			$wpdb->query("UPDATE `wp_tourmeta` SET `meta_value`=meta_value+1 WHERE meta_key='sharecount' and tour_id=".$tourid);
		echo 1;
	}
	else
		echo 0;
}

?>