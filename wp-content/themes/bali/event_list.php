<?php
/*
Template Name: Event List
*/

global $wpdb;
get_header();
wp_enqueue_media();

if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    $friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE ID IN(
        SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$user_id}')
        OR ID IN(
            SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$user_id}')");
    if (current_user_can('um_travel-advisor') || current_user_can('administrator')) {
        $tourPackages = $wpdb->get_results("SELECT ID,post_title, meta_value as tour_id FROM `wp_posts`,`wp_postmeta`
        WHERE post_type='post' AND post_status='publish' AND `meta_key`='tour_id'
        AND wp_posts.ID=wp_postmeta.post_id AND post_author='{$user_id}'");
    }
}


$pageNum = filter_input(INPUT_GET, 'pageNum', FILTER_SANITIZE_NUMBER_INT);
$pageNum = ($pageNum > 0) ? ($pageNum - 1) : 0;
$post_per_page = 15;
$postOffset = $pageNum * $post_per_page;
$eventPosts = $wpdb->get_results("SELECT wp_posts.ID,post_author,user_login,post_title,post_date,post_type,post_content
                                        FROM `wp_posts`,`wp_users` WHERE `post_type`='eventpost'
                                        AND post_status='publish' AND post_author=wp_users.ID
                                        ORDER BY post_date DESC LIMIT {$postOffset},{$post_per_page}");
?>


<?php if(count($eventPosts)==0){ ?>
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />

<link href="https://www.travpart.com/English/wp-content/themes/bali/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<link href="https://www.travpart.com/English/wp-content/themes/bali/css/tokenize2.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<link async="async" href="<?php bloginfo('template_url'); ?>/css/tpl-mytimeline.css?v29" rel="stylesheet" type="text/css" />
<input id="website-url" type="hidden" value="<?php echo home_url(); ?>" />
<input id="admin-ajax-url" type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" />
<input id="current-page-url" type="hidden" value="<?php echo remove_query_arg('feed_id'); ?>" />
<input id="has_profile" type="hidden" value="<?php echo !um_profile('profile_photo') ? '0' : '1'; ?>" />
<input id="post-id-of-comment" type="hidden" value="<?php echo intval($_GET['feed_id']); ?>" />
<script type="text/javascript" src="https://www.travpart.com/English/wp-content/themes/bali/js/tokenize2.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://www.travpart.com/English/wp-content/themes/bali/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA069EQK3M96DRcza2xuQb0DZxmYYkVVw8&amp;libraries=places"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/mytimeline.js?v10"></script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<?php } ?>

  <?php if ($_GET['device'] == 'app') { ?>

    <style>
      .mobileapp{
	  	display: none !important; 
	  }
	  #header{
	  	display: none !important;	  	
	  }
	  </style>
	 <?php } ?>

<style>
    a {
        color: #0254EB
    }

    a:visited {
        color: #0254EB
    }

    .content_mobile {
        padding: 12px 10px;
    }

    a,
    p,
    div,
    h1,
    h2,
    h3,
    h4,
    button,
    input {
        font-family: "Roboto", Sans-serif;
    }

    .buttons_for_mobilepost {
        padding: 5px;
        margin-top: 10px;
    }

    hr.style-mobile-six {
        border: 0;
        margin: 0px;
        height: 0;
        border-top: 1px solid rgba(0, 0, 0, 0.1);
        border-bottom: 1px solid rgba(255, 255, 255, 0.3);
    }
    .wh_event_icon_div{
        display: inline-block;
        background-color: green;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 5px;
    }
    i.fab.fa-whatsapp.share_wh_event_i{
        color: white;
        font-size: 17px;
        position: relative;
        top: 4px;
        left: 6px;
    }
    i.far.fa-copy.share_copy_link_event_i{
        position: relative;
        margin-right: 5px;
        top: 3px;
        font-size: 22px;
        margin-left: 3px;
        margin-bottom: 5px;
    }

    .mobile_agent_name a {
        font-weight: bold;
        color: #1d4354c9;
        text-decoration: none;
        text-transform: capitalize;
    }

    .for_img {
        text-align: center;
    }

    .post-content {
        border: 0px;
        border-top: 4px solid #138967;
        border-bottom: 11px solid #138967;
        box-shadow: 0 1px 1px 0 rgba(60, 64, 67, .08), 0 1px 3px 1px rgba(60, 64, 67, .16);
    }

    .post-content-txt {
        padding: 0 12px;
        font-size: 12px;
    }

    .for_img img {
        max-width: 80%;
        border-radius: 100%;
        padding: 2px;
        margin-top: 7px;
        border: 1px solid #b1b1b163;
    }

    .content_mobile p {
        margin: 0px;
        color: grey;
    }

    .post_profile_img {
        float: left;
        width: 20%;
    }

    .header_user_content {
        float: right;
        width: 80%;
        padding-left: 10px;
    }

    .clearfix {
        clear: both;
    }

    .main_buttons_div div a {
        color: #808080d6;
        font-size: 12px;
    }

    .ShareDropdown li a {
        color: #000;
    }
    .another-user-section span {
        word-break: break-word;
    }
    .user-comment-area {
        margin-bottom: 10px;
        display: flex;
    }
    .post-image {
        height: 200px;
        margin-bottom: 5px;
        position: relative;
        text-align: center;
        max-height: 200px;
        overflow: hidden;
    }
    .post-image img {
        height: auto;
        /* max-height: 500px; */
        width: 100% !important;
        border-radius: 0;
    }

    @media screen and (max-width: 600px) {
        .row {
            margin: 0px;
        }

        input,
        textarea,
        .mobile-button-link,
        .m-post1 a {
            font-family: "Roboto", Sans-serif;
        }

        div.pac-container {
            z-index: 99999999999 !important;
        }

        .looking_travil span {
            color: #22b14c;
            margin: 0 5px;
        }

        .col-md-12 {
            padding: 0px;
        }

        .col-md-4 {
            padding: 0px;
        }

        #footer {
            display: none;
        }
    }

    .like_mobile a {
        text-decoration: none;
    }

    i.liked {
        color: goldenrod;
    }

    .user-comment-area img {
        border-radius: 50px;
        position: relative;
        margin-left: 18px;
        width: 35px;
        height: 35px;
    }

    .user-comment-area input {
        width: 75%;
        border-radius: 5px;
        border: none;
        background-color: #f2f3f5;
        border: 1px solid #ccd0d5;
        border-radius: 16px;
        padding: 8px;
        font-size: 12px;
        position: relative;
        left: 10px;
        top: 2px;
    }

    .user-comment-area input:focus {
        outline: none;
    }

    .another-user-section a {
        font-size: 13px;
        margin: 0 10px 0px 3px;
    }

    .another-user-section span {
        font-size: 13px;
        margin: 0 10px 0px 3px;
    }

    .another-user-section {
        display: inline-block;
        background: #F2F3F5;
        padding: 7px;
        border-radius: 16px;
        margin: 0px 10px;
    }

    .submit-comment {
        display: none;
    }
    
    p {
        margin: 0px;
    }

    .width15 {
        text-align: center;
        font-size: 17px;
        color: grey;
        width: 13%;
    }

    .text-normal {
        color: grey;
    }

    .inline_t {
        display: inline-table;
        font-size: 12px;
        margin-left: 10px;
        vertical-align: middle;
    }

    .invite_connection_title,
    .event_type,
    .title_location {
        margin: 15px 0px;
    }

    .for_mututal {
        font-size: 12px;
        font-weight: bold
    }

    .for_mut_color {
        color: grey
    }

    .tokenize>.tokens-container {
        min-height: 65px;
        font-size: 15px;
    }

    .inputs_data {
        margin-top: 10px;
    }

    .shedule_event_main_wrapper {
        background: #fff;
        border: 1px solid #3333338a;
        margin-top: 10px;
    }

    .shedule_pageheading {
        position: relative;
        background: #F5F6F8;
        padding: 10px 5px;
        border-bottom: 1px solid #ccc6;
    }

    .content_row {
        padding: 15px;
    }

    .shedule_pageheading h1 {
        color: #000 !important;
        margin: 0px;
    }

    .shedule_pageheading span {
        font-size: 18px;
        position: absolute;
        right: 10px;
        top: 13px;
    }

    .event_name_shedule {
        display: inline-table;
        width: 20%;
    }

    #shedule_title {
        font-size: 14px;
    }

    .event_name_shedule p,
    .invite_connection_title p,
    .event_type p,
    .title_location p {
        font-size: 17px;
        margin: 0px;
        font-weight: bold;
    }

    .event_text_shedule {
        display: inline-table;
        width: 76%;
    }

    .event_text_shedule input {
        width: 60%;
    }

    .event-tour-package-row {
        margin-top: 10px;
    }

    .event-submit-row {
        text-align: right;
        border-top-style: solid;
        padding-top: 15px;
        margin-top: 15px;
        color: #dee1e4;
        border-width: 1px;
    }

    .event_display_table_cell img {
        width: 100%;
    }

    .event_display_table_cell {
        display: inline-table;
        width: 30%;
        vertical-align: middle;
    }

    .event_display_image_cell {
        display: inline-table;
        vertical-align: middle;
        width: 30%;
    }

    .event_display_image_cell p {
        margin: 0px;
        font-size: 20px;
        margin-left: 10px;
    }

    .event_button {
        width: 23%;
    }

    .event_button button {
        background: #b7b4b4a8;
        width: 100%;
        padding: 5px 0px;
        color: #6b6b6bb5;
    }

    .peoplegoing_fes {
        color: #0254EB;
    }

    .event_inner_div .btn-group {
        width: 100% !important;
    }

    .event_details p {
        margin: 0px;
    }

    .event_details {
        width: 60%;
    }

    .event_date {
        width: 10%;
    }

    .event_button,
    .event_details,
    .event_date {
        display: inline-table;
        vertical-align: middle;
    }

    .color_d_p {
        color: red;
    }

    .title_fes {
        color: #000;
        letter-spacing: 1px;
        font-weight: bold;
    }

    .date_fes {
        font-weight: bold;
        color: #675c5cbf;
        font-size: 11px;
    }

    .cusomt_private_dropdown,
    .cusomt_private_dropdown .dropdown,
    .cusomt_private_dropdown .dropdown .btn {
        width: 100%;
        background: #f4f4f4;
        text-align: left;
    }

    .inline_t i {
        font-size: 23px;
    }

    .cusomt_private_dropdown .dropdown-menu li {
        margin-bottom: 10px;
        cursor: pointer;
    }

    .cusomt_private_dropdown .dropdown-menu {
        width: 100%;
    }

    .width_25 {
        display: inline-table;
        width: 25%;
        vertical-align: middle;
    }

    .width_69 {
        display: inline-table;
        vertical-align: middle;
        width: 69%;
    }

    .custom_round .btn {
        padding: 3px 10px !important;
    }

    .custom_round {
        background-size: cover;
        background-repeat: no-repeat;
        font-weight: bold;
        text-align: center;
        border: 1px solid #333;
        width: 55%;
        margin: auto;
        border-radius: 50%;
        padding: 34px 31px;
    }

    .text_dates {
        display: inline-table;
        vertical-align: middle;
    }

    .input-icon {
        position: absolute;
        right: 6px;
        color: #80808094;
        font-size: 20px;
        top: 7px;
        /* Keep icon in center of input, regardless of the input height */
    }

    .input-wrapper {
        position: relative;
    }

    .bootstrap-datetimepicker-widget ul {
        padding: 0px;
    }

    /* On screens that are 992px or less, set the background color to blue */
    @media screen and (max-width: 992px) {
        .width_69 {
            width: 59%;
        }

        .custom_round {
            width: 80%;
        }

        .input-icon {
            top: 2px;
        }

        .cus {
            width: 100% !important;
        }
    }

    @media(max-width: 600px) {
		.post-area-main{
			margin-top: 50px;
		}
        .event-tab {
            margin: 68px 0px;
        }

        .width_25,
        .width_69,
        .cus,
        .event_text_shedule,
        .event_name_shedule,
        .event_text_shedule input {
            width: 100% !important;
            margin-top: 10px;
        }

        .custom_round {
            width: 55%;
        }

        .dropdown {
            z-index: 1 !important;
        }

        .shedule_pageheading h1 {
            color: #000 !important;
            margin: 0px;
            font-size: 15px !important;
            margin-top: 11px;
            font-weight: bold;
        }

        .text_dates {
            font-size: 12px !important;
        }

        p,
        .text_dates {
            font-size: 11px !important;
        }
		
        .input-icon {
            font-size: 16px;
            top: 4px;
        }
    }
    .post-content{
		margin-bottom:10px;  
	}
	.event_notfound{
	    background: #fff;
	    padding: 50px;
        box-shadow: 2px 2px 10px 0px #ccc;
        font-family: "Times New Roman", Georgia, Serif;
	    border-radius: 10px;
	    margin-top: 20px;
	    font-size: 16px;
	    text-align: center;
	    margin-bottom: 20px;
	}
	.create_event_btn{
		margin-top: 20px;
		background: #22b14c;
	    border: 3px solid #c3c3c3;
	    padding: 5px 15px;
	    color: #fff;
	    border-radius: 3px;
	}
</style>
<div class="post-area-main">
    <section>
    	
    	<?php
            if(count($eventPosts)>0){
    	?>
        <div id="container">

            <!-- time line row -->
            <div id="loading_next_page" class="row">
				
                <?php
                foreach ($eventPosts as $item) {
                    if (get_post_meta($item->ID, 'event_type', true) == 'private') {
                        $event_invited_people = get_post_meta($item->ID, 'event_invited_people', true);
                        if (!is_user_logged_in() && $current_user->ID != $item->post_author
                            && !in_array($current_user->ID, $event_invited_people)) {
                            continue;
                        }
                    }
                    $event_name = get_post_meta($item->ID, 'event_name', true);
                    $event_location = get_post_meta($item->ID, 'event_location', true);
                    $attachment_url = get_template_directory_uri() . '/assets/img/rome.jpg';
                    $attachment = get_post_meta($item->ID, 'event_location_image_id', true);
                    if (!empty($attachment)) {
                        $attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
                    }
                    $start_date = get_post_meta($item->ID, 'start_date', true);
                    $event_tour_package = get_post_meta($item->ID, 'event_tour_package', true);
                    $tour_img = get_post_meta($item->ID, 'tour_img', true);
                    if (empty($tour_img)) {
                        $tour_img = 'https://i.travelapi.com/hotels/25000000/24160000/24157900/24157865/e01875da_y.jpg';
                    }
                    if (is_user_logged_in()) {
                        $liked = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE user_id = '{$current_user->ID}' AND feed_id = '$item->ID'");
                        $going_status = $wpdb->get_var("SELECT status FROM `event_going` WHERE post_id='{$item->ID}' AND user_id='{$current_user->ID}'");
                    }
                    else {
                        $liked = 0;
                    }
                    $like_count = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE feed_id = '{$item->ID}'");
                    $going_count = $wpdb->get_var("SELECT COUNT(*) FROM `event_going` WHERE post_id = '{$item->ID}' AND status=1");
                    ?>
                    
                    <div class="col-md-4 post-md-4 box">
                        <div class="post-area-content">
                            <div class="post-content">
                                <div class="post_header">
                                    <div class="post_profile_img">
                                        <div class="for_img">
                                            <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>">
                                                <img alt="" src="<?php echo um_get_avatar_url(um_get_avatar('', $item->post_author, 52)); ?>">
                                            </a>
                                        </div>
                                    </div>
                                    <?php
                                    $display_name = um_user('display_name'); ?>
                                    <div class="header_user_content">
                                        <div class="content_mobile">
                                            <h4 class="mobile_agent_name" style="margin-bottom: 20px;">
                                                <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>"><?php echo $display_name; ?> </a>
                                                <span class="date_pub_mobile">
                                                    <p style="margin: 0px; font-size: 10px">
                                                        <?php echo human_time_diff(strtotime($item->post_date), current_time('timestamp')); ?>
                                                    </p>
                                                </span>
                                            </h4>

                                            <div class="right-section-dot dropdown-toggle text-right" data-toggle="dropdown" aria-expanded="false">
                                                <a href="#" class="Pdeltebtn"><i class="fas fa-ellipsis-h"></i></a>
                                                <br>
                                                <button style="padding: 0px 10px;line-height: 12px;box-shadow: 0 0 0 5px hsl(0, 0%, 87%);border-radius: 0px;margin-top: 2px;" class="btn btn-success">Created an <br> Event</button>
                                            </div>

                                            <!-- Post Delete Option PC-->
                                            <div id="PostDId" class="PostDelete dropdown-menu" role="menu" aria-labelledby="menu1">
                                                <li><a href="#">Turn On notifications For this post</a></li>
                                                <li class="ShowITc"><a href="#">Show in tab</a></li>
                                                <li><a href="#">Hide From timeline</a></li>
                                                <li><a href="#">Copy link to this post</a></li>

                                            </div>
                                            <!--End Post Delete Option -->

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="post-image">
                                    <img src="<?php echo $attachment_url; ?>">
                                </div>

                                <div class="post-content-txt">
                                    <div class="event_date">
                                        <div class="event_date text-center">
                                            <h5 class="color_d_p"><?php echo date('M', $start_date); ?></h5>
                                            <h5><?php echo date('d', $start_date); ?></h5>
                                        </div>
                                    </div>
                                    <div class="event_details">
                                        <div class="event_details_content">
                                            <p class="title_fes"><?php echo $event_name; ?></p>
                                            <p class="date_fes"><?php echo event_week_time_display($start_date); ?> - <?php echo $event_location; ?></p>
                                            <p class="peoplegoing_fes"><?php echo number_format($going_count); ?> people going</p>
                                        </div>
                                    </div>
                                    <?php if(is_user_logged_in()) { ?>
                                    <div class="event_button">
                                        <div class="event_inner_div">
                                            <!-- Single button -->
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-check"></i> <span class="event-going-current-status"><?php echo $going_status == 1 ? 'Going' : 'Not Going'; ?></span> <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu event-going" post_id="<?php echo $item->ID; ?>">
                                                    <li status="1"><a href="#">Going</a></li>
                                                    <li status="0"><a href="#">Not Going</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if (!empty($event_tour_package)) { ?>
                                        <div class="tour_details_evvent">
                                            <div class="event_display_table_cell">
                                                <img src="<?php echo $tour_img; ?>">
                                            </div>
                                            <div class="event_display_image_cell">
                                                <p>BALI<?php echo $event_tour_package; ?></p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>

                                <div class="buttons_for_mobilepost">
                                    <hr class="style-mobile-six">
                                    <div class="main_buttons_div">
                                        <div class="like_mobile <?php echo is_user_logged_in()?'like-the-post':'login-alert' ?>">
                                            <a href="#" feedid="<?php echo $item->ID; ?>">
                                                <i class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>
                                                <span><?php echo $like_count; ?></span>
                                            </a>
                                        </div>
                                        <div class="comment_mobile" data-comment="<?php echo $item->ID; ?>">
                                            <a href="#">
                                                <i class="far fa-comment-alt"></i>
                                                Comment
                                            </a>
                                        </div>
                                        <div class="share_mobile">
                                            <a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
                                                <i class="fas fa-share"></i> Share
                                            </a>
                                            <div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2" aria-labelledby="menu2" style="right: 40px;left: inherit;">
                                                <li onclick="share_feed(<?php echo $item->ID; ?>)">Share To Feed</li>
                                                <li><a target="_blank" href="https://web.whatsapp.com/send?text=<?php echo substr($value->post_content, 0, 80) . "  ( see-more ) "; ?>https://www.travpart.com/English/my-time-line/?user=<?php echo esc_html($username); ?>"><div class="wh_event_icon_div"><i class="fab fa-whatsapp share_wh_event_i"></i></div>Share to whats app</a></li>
                                                <li><a  href="#" onclick="copy_link('<?php echo $p->ID; ?>')"><i class="far fa-copy share_copy_link_event_i"></i>Copy link to this post</a></li>
                                                <li class="mores"><a class="a2a_dd" href="#"><i class="fas fa-plus-square"></i> More...</a></li>
                                            </div>
                                        </div>
                                    </div>
									
									<div class="comment_main_div show_comment_<?php echo $item->ID; ?>">
                                    <?php
                                    $comments = $wpdb->get_results("SELECT comment,user_id,user_login FROM `user_feed_comments`,`wp_users` WHERE wp_users.ID=user_id AND `feed_id` = '{$item->ID}' ORDER BY user_feed_comments.id DESC");
                                    if (!empty($comments)) {
                                        foreach ($comments as $comment) {
                                    ?>
                                            <div class="user-comment-area">
                                                <?php echo get_avatar($comment->user_id, 50);  ?>
                                                <div class="another-user-section">
                                                    <a href="<?php home_url('user/') . $comment->user_login; ?>"><?php echo um_get_display_name($comment->user_id); ?></a>
                                                    <span><?php echo $comment->comment;  ?></span>
                                                </div>
                                            </div>
                                    <?php }
                                    } ?>
									</div>
                                    <?php if(is_user_logged_in()) { ?>
                                    <div class="user-comment-area submit-comment">
                                        <?php echo get_avatar($current_user->ID, 50);  ?>
                                        <input placeholder="Write a Comment" type="text" name="feed_comment" required="">
                                        <input type="hidden" name="feed_id" value="<?php echo $item->ID; ?>">
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
               
            </div>
         </div>
        <?php  } else if(is_user_logged_in()) { ?>
           <div class="col-md-12">
           	<div class="event_notfound">
           		<p>This is the page where you can see the list of events that your friends had made. <br> Unfortunately, you don't have any event created in your circle yet.<br>You can create one now.</p>
           		<button class="create_event_btn"> 
           			<i class="fas fa-calendar"></i> Create Event
           		</button>
           	</div>
           	
           	<div class="event-tab">
			    <div class="shedule_event_main_wrapper">
			        <div class="container_shedule">
			            <div class="shedule_pageheading">
			                <h1>Shedule an Event</h1>
			                <span><i class="fa fa-close"></i></span>
			            </div>

			            <?php wp_nonce_field('event_post_nonce_action', 'eventpostnonce'); ?>
			            <div class="content_row">
			                <div class="name_text_row">
			                    <div class="event_name_shedule">
			                        <p>Event Name</p>
			                    </div>
			                    <div class="event_text_shedule">
			                        <input class="form-control" name="event_name" placeholder="Add a short, clear name" />
			                    </div>
			                </div>

			                <div class="invite_connection_div">
			                    <div class="invite_connection_title">
			                        <p>Invite your connection</p>
			                    </div>
			                    <select name="event_invited_people" class="event-invited-people" multiple>
			                        <?php foreach ($friends as $f) { ?>
			                            <option value="<?php echo $f->ID; ?>"><?php echo um_get_display_name($f->ID); ?></option>
			                        <?php } ?>
			                    </select>

			                    <!--p class="for_mututal"><span class="for_mut_color">Mutual friends</span> <a href="#">Balouch</a>,<a href="#">Lix</a></p-->
			                </div>

			                <div class="event_type">
			                    <p>Event Type</p>
			                </div>
			                <div class="cusomt_private_dropdown">
			                    <div class="dropdown">
			                        <div class="dropdown-toggle" id="event-type-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			                            <div class="inline_t">
			                                <i class="fas fa-lock"></i>
			                            </div>
			                            <div class="inline_t event-type-chosen">
			                                <p><strong>Private</strong></p>
			                                <p>The event's members are the only people whom can see the post and other event's members</p>
			                            </div>
			                            <input name="event_type" type="hidden" />

			                        </div>
			                        <ul class="dropdown-menu" aria-labelledby="event-type-dropdown">
			                            <li>
			                                <div class="inline_t">
			                                    <i class="fas fa-lock"></i>
			                                </div>
			                                <div class="inline_t event-type-option" type="private">
			                                    <p><strong>Private</strong></p>
			                                    <p>The event's members are the only people whom can see the post and other event's members</p>
			                                </div>
			                            </li>
			                            <li>
			                                <div class="inline_t">
			                                    <i class="fas fa-globe-americas"></i>
			                                </div>
			                                <div class="inline_t event-type-option" type="public">
			                                    <p><strong>Public</strong></p>
			                                    <p>Everyone can see the event's member and post</p>
			                                </div>
			                            </li>
			                        </ul>
			                    </div>
			                </div>


			                <div class="locaion_shedule_div">
			                    <div class="title_location">
			                        <p>Location</p>
			                    </div>
			                    <div class="width_25 location_profile">
			                        <div class="custom_round">
			                            <p>location <br /> picture<br /> of video</p>
			                            <button class="btn btn-primary event-location-image">Upload</button>
			                            <input name="event_location_image_id" type="hidden" />
			                        </div>
			                        <div class="text-normal text-center">
			                            <p>Upload a picture of the <br />location's unique <br />landmark</p>
			                        </div>
			                    </div>
			                    <div class="width_69 location_profile">
			                        <div class="input_record">
			                            <input id="event-location" name="event_location" type="text" class="cus form-control" placeholder="(Required)" style="width: 60%" />
			                            <div class="inputs_data">
			                                <div class="text_dates width15">
			                                    start
			                                </div>
			                                <div class="text_dates width40">
			                                    <div id="datetimepicker1" class="input-append date">
			                                        <div class="input-wrapper">
			                                            <label for="stuff" class="far fa-calendar-alt add-on input-icon"></label>
			                                            <input name="start_date" class="form-control" data-format="MM/dd/yyyy" type="text"></input>
			                                        </div>
			                                    </div>
			                                </div>
			                                <div class="text_dates width40">
			                                    <div id="datetimepicker3" class="input-append date">
			                                        <div class="input-wrapper">
			                                            <label for="stuff" class="fas fa-stopwatch add-on input-icon"></label>
			                                            <input name="start_time" class="form-control" data-format="hh:mm:ss" type="text"></input>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>

			                            <div class="inputs_data">
			                                <div class="text_dates width15">
			                                    Ends
			                                </div>
			                                <div class="text_dates width40">
			                                    <div id="datetimepicker2" class="input-append date">
			                                        <div class="input-wrapper">
			                                            <label for="stuff" class="far fa-calendar-alt add-on input-icon"></label>
			                                            <input name="end_date" class="form-control" data-format="MM/dd/yyyy" type="text"></input>
			                                        </div>
			                                    </div>
			                                </div>
			                                <div class="text_dates width40">
			                                    <div id="datetimepicker4" class="input-append date">
			                                        <div class="input-wrapper">
			                                            <label for="stuff" class="fas fa-stopwatch add-on input-icon"></label>
			                                            <input name="end_time" class="form-control" data-format="hh:mm:ss" type="text"></input>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>

			                <div class="event-tour-package-row">
			                    <div class="event_name_shedule">
			                        <p>Tour package</p>
			                    </div>
			                    <div class="event_text_shedule">
			                        <select name="event_tour_package" class="event-tour-package" style="width: 60%;">
			                            <option></option>
			                            <?php foreach ($tourPackages as $t) { ?>
			                                <option value="<?php echo $t->tour_id; ?>">BALI<?php echo $t->tour_id; ?></option>
			                            <?php } ?>
			                            <?php if (empty($tourPackages)) { ?>
			                                <option disabled="disabled">You have not created any tour package.</option>
			                            <?php } ?>
			                        </select>
			                    </div>
			                </div>

			                <div class="event-submit-row">
			                    <button id="create-event-submit" class="btn btn-lg btn-success">Create</button>
			                </div>

			            </div>

			        </div>
			    </div>
			</div>
           	
           </div>	                
        <?php } ?>
    </section>
</div>

<script>
    //share feed
    
    $(document).ready(function(){
    	$('.create_event_btn').click(function(){
    		$('.event_notfound').hide();
    		$('.event-tab').show();
    	})
    })
    
    function share_feed(id) {
        jQuery(document).ready(function($) {
            var tour_id = id;
            $.ajax({
                type: "POST",
                url: '<?php echo site_url() ?>/submit-ticket/',
                data: {
                    feed_id: tour_id,
                    action: 'feedback_share'
                }, // serializes the form's elements.
                success: function(data) {
                    var result = JSON.parse(data);
                    if (result.status) {
                        alert(result.message);
                        window.location.reload() // show response from the php script.
                    } else {
                        alert(result.message);
                        //window.location.reload()// show response from the php script.
                    }
                }
            });
        });
    }

    jQuery(document).ready(function ($) {
        var pageIndex = 1;
        
        var $container = '';
	    $('#loading_next_page').imagesLoaded(function () {
	        $container = jQuery('#loading_next_page').masonry({
	            itemSelector: '.box',
	            columnWidth: 5
	        });
	    });
        
        var page_loading = false;
        $(window).scroll(function() {
            if (($(document).scrollTop() + 1.5 * $(window).height()) > $(document).height()) {
                if (!page_loading) {
                    page_loading = true;
                    pageIndex++;
                    $.ajax({
                        type: "GET",
                        url: '<?php echo admin_url('admin-ajax.php'); ?>',
                        data: {
                            pageNum: pageIndex,
                            action: 'loading_more_eventpost'
                        },
                        success: function(data) {
                            if (data != 0) {
                                page_loading = false;
                                var $content = $(data);
                                $container.append($content).masonry('appended', $content);
                                //$('#loading_next_page').append(data);
                                jQuery('.event_button .dropdown-toggle').dropdown();
                            }
                        }
                    });
                }
            }
        });

        $('#loading_next_page').on('click', '.login-alert', function() {
            alert("Please login");
        });

        $('#loading_next_page').on('click', '.event-going li', function() {
            var post_id = $(this).parent().attr('post_id');
            var status = $(this).attr('status');
            $(this).parent().parent().find('.event-going-current-status').text($(this).text());
            $.ajax({
                type: "POST",
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                data: {
                    post_id: post_id,
                    status: status,
                    action: 'update_event_going'
                },
                success: function(data) {}
            });
        });

        $('#loading_next_page').on('click', '.like-the-post a', function() {
            var sc_type = 0;
            if ($(this).find('i').hasClass('liked')) {
                $(this).find('i').removeClass('liked');
                $(this).find('span').text(parseInt($(this).find('span').text()) - 1);
                sc_type = 1;
            } else {
                $(this).find('i').addClass('liked');
                $(this).find('span').text(parseInt($(this).find('span').text()) + 1);
            }
            if (!!$(this).attr('feedid')) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo site_url('submit-ticket') ?>',
                    data: {
                        feed_id: $(this).attr('feedid'),
                        action: 'feedback_like'
                    },
                    success: function(data) {}
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: '<?php echo site_url('submit-ticket') ?>',
                    data: {
                        post_id: $(this).attr('blogid'),
                        sc_type: sc_type,
                        action: 'blog_social_connect_l_d'
                    },
                    success: function(data) {}
                });
            }
        });

        /*$('#loading_next_page').on('click', '.comment_mobile', function() {
            $(this).parent().parent().find('.submit-comment').toggle();
        });*/
        $('.submit-comment').hide();
	    $('#loading_next_page').on('click', '.comment_mobile', function () {
	        if($('.show_comment_'+$(this).data('comment')).is(":hidden")){
	            $(this).parent().parent().find('.submit-comment').show();
	            $('.show_comment_'+$(this).data('comment')).show();
	            $container.masonry('layout');
	        } else{
	            $('.show_comment_'+$(this).data('comment')).hide();
	            $(this).parent().parent().find('.submit-comment').hide();
	            $container.masonry('layout');
	        }
	    });

        $('#loading_next_page').on('keydown', '.submit-comment', function(e) {
            if (e.which == 13) {
                var feed_id = $(this).find('input[name="feed_id"]').val();
                var blog_id = $(this).find('input[name="blog_id"]').val();
                var feed_comment = $(this).find('input[name="feed_comment"]').val();
                if (!!feed_id) {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo admin_url('admin-ajax.php'); ?>',
                        dataType: 'json',
                        data: {
                            action: 'feed_comment',
                            feed_id: feed_id,
                            feed_comment: feed_comment
                        },
                        success: function(data) {
                            if (data.success) {
                                window.location.href = "<?php echo add_query_arg(); ?>";
                            } else {
                                alert('System is busy. Try again later.');
                            }
                        }
                    });
                } else {
                    $.ajax({
                        type: "POST",
                        url: '<?php echo site_url('submit-ticket') ?>',
                        data: {
                            post_id: blog_id,
                            scm_text: feed_comment,
                            action: 'blog_social_comments_form'
                        },
                        success: function(data) {
                            window.location.href = "<?php echo add_query_arg(); ?>";
                        }
                    });
                }
            }
        });

    });
</script>

<script src="<?php bloginfo('template_url'); ?>/js/jquery.masonry.js"></script>
<script type="text/javascript">
 jQuery(document).ready(function($){
	/*setTimeout(function(){
		$('#container').masonry({
		      itemSelector: '.box',
		      columnWidth:5
	    });
	},2000)*/
	
	
 })
 </script>
 <style type="text/css">
    #footer {
        display: none;
    }
    .comment_main_div{display: none;}
</style>
<?php
get_footer();
?>
