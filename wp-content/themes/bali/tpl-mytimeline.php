<?php
/*
Template Name: New MyTimeLine
*/
global $wpdb;
session_start();
if (!is_user_logged_in()) {
  //  exit(wp_redirect(home_url('login')));
}

if (!empty($_GET['user']) && ($user = get_user_by('login', htmlspecialchars($_GET['user'])))) {
} else {
    exit(wp_redirect(home_url('my-time-line?user='.wp_get_current_user()->user_login)));
}
$user_id = $user->ID;

if(function_exists('visitor_profile_logs') AND is_user_logged_in()) {
    visitor_profile_logs ('timeline',$user_id,$current_user->ID );
}


//get  user fb sharing option
    $fb_sharing_option = get_user_meta( wp_get_current_user()->ID, 'fb_sharing_option' , true );
    if(!empty ($fb_sharing_option)){
    $fb_sharing_option = $fb_sharing_option;
    }
    else{
    $fb_sharing_option = 0;
    }

if ($user->ID == wp_get_current_user()->ID) {
    $friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE ID IN(
        SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}')
        OR ID IN(
            SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}')");
}
if (current_user_can('um_travel-advisor') || current_user_can('administrator')) {
    $tourPackages = $wpdb->get_results("SELECT ID,post_title, meta_value as tour_id FROM `wp_posts`,`wp_postmeta`
        WHERE post_type='post' AND post_status='publish' AND `meta_key`='tour_id'
        AND wp_posts.ID=wp_postmeta.post_id AND post_author='{$user->ID}'");
}

$ratings = $wpdb->get_row("SELECT SUM(CASE WHEN rating=5 THEN 1 ELSE 0 END) as star5,
                                  SUM(CASE WHEN rating=4 THEN 1 ELSE 0 END) as star4,
                                  SUM(CASE WHEN rating=3 THEN 1 ELSE 0 END) as star3,
                                  SUM(CASE WHEN rating=2 THEN 1 ELSE 0 END) as star2,
                                  SUM(CASE WHEN rating=1 THEN 1 ELSE 0 END) as star1
                                FROM social_reviews WHERE user_id='{$user->ID}'", ARRAY_A);

$rating_people_count = array_sum($ratings);
$avg_rating = $wpdb->get_var("SELECT AVG(rating) FROM `social_reviews` WHERE user_id='{$user->ID}'");

if ($rating_people_count == 0) {
    $avg_rating = 0;
}

$user_photos = $wpdb->get_results("SELECT guid,post_mime_type,ID FROM `wp_posts` WHERE `post_author` = '{$user->ID}'
                                    AND post_status='inherit' AND `post_type` = 'attachment'
                                    ORDER BY `post_date` DESC LIMIT 6");

$user_location = $wpdb->get_row("SELECT region,country FROM `user` WHERE username='{$user->user_login}'");
if (!empty($user_location)) {
    $user_location = trim($user_location->region . ', ' . $user_location->country, ',');
}
$location_visiable = get_user_meta($user->ID, 'location_visiable', true);
if ($location_visiable == 'anyone' || $user->ID == wp_get_current_user()->ID) {
    $location_visiable = true;
} else if (empty($location_visiable) || $location_visiable == 'family') {
    if (strpos(get_user_meta($user->ID, 'family_member', true), wp_get_current_user()->user_login) !== false) {
        $location_visiable = true;
    } else {
        $location_visiable = false;
    }
} else if ($location_visiable == 'friend') {
    $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}' AND user2=" . wp_get_current_user()->ID);
    if ($is_friend == 0) {
        $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}' AND user1=" . wp_get_current_user()->ID);
    }
    if ($is_friend > 0) {
        $location_visiable = true;
    } else {
        $location_visiable = false;
    }
} else if ($location_visiable == 'family_friend') {
    if (strpos(get_user_meta($user->ID, 'family_member', true), wp_get_current_user()->user_login) !== false) {
        $location_visiable = true;
    } else {
        $location_visiable = false;
    }
    if (!$location_visiable) {
        $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}' AND user2=" . wp_get_current_user()->ID);
        if ($is_friend == 0) {
            $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}' AND user1=" . wp_get_current_user()->ID);
        }
        if ($is_friend > 0) {
            $location_visiable = true;
        } else {
            $location_visiable = false;
        }
    }
} else {
    $location_visiable = false;
}

//co ordinates

$user_live_location = $wpdb->get_row("SELECT lat,lng FROM `user_location` WHERE ID='{$user->ID}'");

if ($user_live_location) {

    $latitude = $user_live_location->lat;
    $longitude = $user_live_location->lng;

    $map_url = "https://www.travpart.com/English/map-custom/?lat=" . $latitude . "&&lng=" . $longitude;
}

//$user_id = get_current_user_id();
$args = array(
    'post_type'  => 'shortpost',
    'author'     =>  $user_id,
);
$posts = get_posts($args);
get_header();
wp_enqueue_media(); 
?>

</head>
<body> 
<link rel="preload" href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
<link href="https://www.travpart.com/English/wp-content/themes/bali/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<link href="https://www.travpart.com/English/wp-content/themes/bali/css/tokenize2.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<link async="async" href="<?php bloginfo('template_url'); ?>/css/tpl-mytimeline.css?v=<?= time()?>" rel="stylesheet" type="text/css" />
<input id="website-url" type="hidden" value="<?php echo home_url(); ?>" />
<input id="admin-ajax-url" type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" />
<input id="current-page-url" type="hidden" value="<?php echo remove_query_arg('feed_id'); ?>" />
<input id="has_profile" type="hidden" value="<?php echo !um_profile('profile_photo') ? '0' : '1'; ?>" />
<input id="post-id-of-comment" type="hidden" value="<?php echo intval($_GET['feed_id']); ?>" />
<script type="text/javascript" src="https://www.travpart.com/English/wp-content/themes/bali/js/tokenize2.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA069EQK3M96DRcza2xuQb0DZxmYYkVVw8&amp;libraries=places"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/mytimeline.js?v=<?= time()?>"></script>

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>


<?php

$user_meta = get_userdata($user->ID);
$user_roles = $user_meta->roles;

if ( in_array( 'um_travel-advisor', $user_roles, true ) ) {
    
}
elseif ( in_array( 'um_clients', $user_roles, true ) ) {
?>
<style>
.btn_profile_con{
    top:10px !important;
}
</style>
<?php
}
 ?>

<!-- My Post PopUp -->
<div class="mobile-popup-test2" style="z-index:100;">
    <div class="mobile-popup-m1" style="z-index:100000; left: 0;top: 0;">

        <div class="col-md-12 for_bg_cr" style="padding:0">
            <div class="mobile-create-post">
                <div class="back-post">
                    <i class="fas fa-arrow-left"></i>
                </div>

                <h5>Create Post </h5>
                <div class="m-post1">
                    <span></span>
                    <a href="#">Post</a>
                </div>

            </div>

            <div class="mobile-pc-scetion">
                <?php /*        
                <?php echo get_avatar($user->ID, 40); ?>
                <a href="#"><?php echo um_get_display_name($current_user->ID); ?></a><span id="mobile-short-post-feeling-text"></span>
                */ ?>
                <div class="table_main">
                    <div class="row_table">
                        <div class="for_vector_div">
                            <?php echo get_avatar($user->ID, 40); ?>
                        </div>
                        <div class="for_name_div text-left">
                            <a href="#"><?php echo um_get_display_name($current_user->ID); ?></a><span id="mobile-short-post-feeling-text"></span>
                        </div>
                        <!--<div class="for_createevent_div">
                            <div class="create_event">
                                <a href="#">Create an <br /> Event</a>
                            </div>
                        </div>
                        <div class="for_looking_travel_div looking-travel-friend">
                            <div class="looking_travil">
                                <a href="#">Looking for a travel friend</a>
                                <span>[notify your friend]</span>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>

            <div class="m-post-area">
                <div class="Cwrap">
                    <div>
                        <div class="C2search">
                            <!--<input id="short-post-location-mobile" type="text" class="searchTerm" placeholder="Add a location tag (optional)">
                            <button type="submit" class="searchButton" style="top:0!important">
                                <i class="fa fa-search"></i>
                            </button>-->
                            <div class="search">
                                <input type="text" class="searchTerm" placeholder="Add a location tag (required)" id="short-post-location-mobile">
                            </div>
                        </div>
                    </div>
                    <!-- Date Time Picker -->
                    <div class="Search-DatePicker">
                        <div class='date-area'>
                            <div class='date-container'>
                                <label class="start-date">Start Date: </label>
                                <div class="short-post-datepicker">
                                    <input id="short-post-startdate-mobile" type="text" placeholder="required" />
                                    <span class="calendar-btn"><span class="fas fa-calendar-alt"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class='date-area'>
                            <div class='date-container'>
                                <label class="start-date">End Date: </label>
                                <div class="short-post-datepicker">
                                    <input id="short-post-enddate-mobile" type="text" placeholder="required" />
                                    <span class="calendar-btn"><span class="fas fa-calendar-alt"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Date Time Picker -->
                    <div class="m-text-area">
                        <textarea id="short-post-content-mobile" placeholder="What’s going on <?php echo um_get_display_name($current_user->ID); ?>?"></textarea>
                    </div>
                    <input class="post-featured-image-id" type="hidden" value="-1">
                </div>
            </div>
        </div>

        <div class="post-mobile-area">
            <div class="custom_photos">
                <div class="buttons_row_add">
                    <div class="add_img  image_d add-photo-video">
                        <div class="border_d">
                            <i class="fas fa-plus"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row_buttons">
                <div class="row1">
                    <div class="row1_col_1 add-photo-video">
                        <div class="m-box-icon">
                            <span class="icon-box"></span>
                            <i class="fas fa-images"></i>
                        </div>

                        <div class="icon-box-content">
                            <h4>
                                Photo/Video
                            </h4>
                        </div>
                    </div>
                    <div class="row1_col_2">
                        <div class="m-box-icon">
                            <span class="icon-box"></span>
                            <i aria-hidden="true" class="far fa-smile"></i>
                        </div>

                        <div class="icon-box-content">
                            <h4 class="feeling-t">
                                Feeling/Activity
                            </h4>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row_buttons">
                <div class="row1">
                    <div class="row1_col_1">
                        <div class="m-box-icon">
                            <span class="icon-box"></span>
                            <i aria-hidden="true" class="fas fa-user-tag"></i>
                        </div>

                        <div class="icon-box-content">
                            <h4 class="tags-t">
                                Tag Peoples
                            </h4>
                        </div>
                    </div>
                    <div class="row1_col_2 ">
                        
                        <div class="m-box-icon">
                            <span class="icon-box"></span>
                            <i aria-hidden="true" class="fas fa-calendar-alt"></i>
                        </div>

                        <div class="icon-box-content">
                            <h4>
                                Create Event
                            </h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row_buttons">
                <div class="row1">
                    
                    <div class="row1_col_1" <?php echo !current_user_can('um_travel-advisor') && !current_user_can('administrator') ? 'style="display:none;"' : ''; ?>>
                        <div class="m-box-icon">
                            <span class="icon-box"></span>
                            <i aria-hidden="true" class="fas fa-plane"></i>
                        </div>
                        <div class="icon-box-content">
                            <h4 class="mytour-p">
                                My Tour Packages
                            </h4>
                        </div>
                    </div>

                </div>
            </div>
            <!--<div class="col-md-3 w-md-20">
                <div class="add-photo-video widget_area_mobile widget_a2" <?php echo ($_GET['device'] == 'app') ? 'style="display:none"' : ''; ?>>
                    <div class="m-box-icon">
                        <span calss="icon-box"></span>
                        <i class="fas fa-images"></i>
                    </div>

                    <div class="icon-box-content">
                        <h4 class="icon-box-title">
                            <span>Photo/Video</span>
                        </h4>
                    </div>

                </div>
            </div>

            <div class="col-md-3 w-md-20">
                <div class="widget_area_mobile widget_a2">
                    <div class="m-box-icon">
                        <span calss="icon-box"></span>
                        <i aria-hidden="true" class="far fa-smile"></i>
                    </div>

                    <div class="icon-box-content">
                        <h4 class="feeling-t">
                            Feeling/Activity
                        </h4>
                    </div>
                </div>

            </div>

            <div class="col-md-3 w-md-20">
                <div class="widget_area_mobile widget_a2">
                    <div class="m-box-icon">
                        <span calss="icon-box"></span>
                        <i aria-hidden="true" class="fas fa-user-tag"></i>
                    </div>

                    <div class="icon-box-content">
                        <h4 class="tags-t">
                            Tag Peoples
                        </h4>
                    </div>
                </div>
            </div>

            <div class="col-md-3 w-md-20">
                <div class="widget_area_mobile widget_a2" style="border-bottom: 0px;">
                    <div class="m-box-icon">
                        <span calss="icon-box"></span>
                        <i aria-hidden="true" class="fas fa-plane"></i>
                    </div>

                    <div class="icon-box-content">
                        <h4 class="mytour-p">
                            My Tour Packages
                        </h4>
                    </div>
                </div>
            </div>-->
            <!--<div class="div_trave" style="<?php echo(($_GET['debug']==1)?'display: none;':''); ?>">
                <div class="trav_checkbox">
                    <input type="checkbox" name="optiona" id="opta" />
                    <span class="checkboxtext">
                        <i class="fas fa-users"></i>
                    </span>
                </div>
                <div class="div_mobile_travel looking-travel-friend">
                    Looking for a travel friend
                    <br><span>(You will be listed in TravMatch)</span>
                </div>
            </div>-->
            <?php //if($_GET['debug']==1){ ?>
                <div class="travmatch_wrapper">
                    <div class="trvmatch_heading">
                        <div class="cold col_tr1">
                            <hr />
                        </div>
                        <div class="cold col_tr2">
                            <h3>TravMatch</h3>  
                        </div>
                        <div class="cold col_tr3">
                            <hr />
                        </div>
                    </div>
                </div>
                
                <div class="looking-travel-friend">
                <div class="iconsdd icon33">
                    <div class="cr_event">
                      <label>
                        <div class="for_r event_check_icon cent_d">
                            <input type="radio" name="event_n" atype="1" />
                        </div>
                        <div class="for_r event_image_icon icon_m">
                            <i class="fas fa-users"></i>
                        </div>
                        <div class="for_r event_text_icon size_m">
                            <p>
                                Looking for  a Travel Friend
                            </p>
                        </div>
                      </label>
                    </div>
                </div>
                
                
                <div class="iconsdd icon44">
                    <div class="cr_event">
                      <label>
                        <div class="for_r event_check_icon cent_d" >
                            <input type="radio" name="event_n" atype="2" />
                        </div>
                        <div class="for_r event_image_icon icon_m">
                            <i class="fas fa-coffee"></i>
                        </div>
                        <div class="for_r event_text_icon size_m">
                            <p>
                                Want to Hangout
                            </p>
                        </div>
                      </label>
                    </div>
                </div>
                
                <div class="iconsdd icon55">
                    <div class="cr_event">
                      <label>
                        <div class="for_r event_check_icon cent_d">
                            <input type="radio" name="event_n" atype="3" />
                        </div> 
                        <div class="for_r event_image_icon icon_m">
                            <i class="fas fa-wine-bottle"></i>
                        </div>
                        <div class="for_r event_text_icon size_m">
                            <p>
                                Going for Party
                            </p>
                        </div>
                      </label>
                    </div>
                </div>

                <div class="iconsdd icon66">
                    <div class="cr_event">
                      <label>
                        <div class="for_r event_check_icon cent_d">
                            <input type="radio" name="event_n" atype="4" />
                        </div>
                        <div class="for_r event_image_icon icon_m">
                            <i class="fas fa-futbol"></i>
                        </div>
                        <div class="for_r event_text_icon size_m">
                            <p>
                                Going for Sport
                            </p>
                        </div>
                      </label>
                    </div>
                </div>
                </div>
                <!--
                <div class="iconsd_d">
                    <div class="">
                        <div class="dis_b w1">
                            <strong>
                                Also Post to <br /> Facebook
                            </strong>
                        </div>
                        <div class="dis_b w2">
                            
                        </div>
                        <div class="dis_b w3">
                            <label class="switch">
                              <input type="checkbox" <?php  echo ($fb_sharing_option==1) ? "checked" : ""  ?>  name="toggle" id="toggle">
                              <span class="slider round"></span>
                            </label>
                        </div>
                        
                    </div>
                </div>
-->                
            <?php //} ?>
            <div class="mobile-button-link2 submitShortPost">
                <span class="button-text">Post</span>
            </div>
        </div>

    </div>

</div>

<!--End My Post PopUp -->


<!-- Feeling Activity popup -->
<div class="MytFilling Mytimelinef">
    <div class="filing-section-main feeling-pc">
        <div class="Fill-header-top">
            <i class="fas fa-times-circle mytimefeelpop_close"></i>
        </div>

        <form action="method" class="form-emoji">
            <?php include(get_template_directory() . '/inc/emoji.php'); ?>
        </form>
    </div>

</div>
<!--End Feeling Activity popup -->


<!--Tag People Popup HTML -->
<div class="MytTagsp Mytimetags-p">
    <div class="tag-people-main">
        <div class="tags-header-top">
            <i class="fas fa-times-circle tag-close"></i>
        </div>

        <div class="tagBodyM">
            <div class="typing_name">
                <input type="text" placeholder="Start typing a name...">
            </div>

            <div class="suggestion_name">
                <input type="text" placeholder="SUGGESTION">
            </div>

            <div class="userTagArea">
                <?php foreach ($friends as $friend) { ?>
                    <div class="user-1 tag_friend" userid="<?php echo $friend->ID; ?>">
                        <img src="<?php echo um_get_user_avatar_data($friend->ID)['url']; ?>">
                        <p class="username"><?php echo um_get_display_name($friend->ID); ?></p>
                        <div class="border-b-u"></div>
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>
<!--End Tag People Popup HTML -->

<!--MyTimeLine Popup HTML -->
<div class="MytimetourP Mypack">
    <span class="helper"></span>
    <i class="fas fa-times-circle mytimetour_close"></i>
    <div>
        <div class="input-group md-form form-sm form-1 pl-0" style="left:0">

            <div class="input-group-prepend">
                <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="basic-text1">
                    <i class="fas fa-search text-white" aria-hidden="true"></i>
                </button>
            </div>

            <form class="search_hea" action="" method="get" style="margin-bottom:0">
                <input class="form-control my-0 py-1" type="text" placeholder="Search for tour packages" aria-label="Search" name="friend">
            </form>

        </div>

        <div class="col-md-12">
            <p class="sugg">Tour Packages Name</p>
        </div>
        <?php foreach ($tourPackages as $tour) { ?>
            <div class="col-md-12 feel_display tourpackage" tour_id="<?php echo $tour->tour_id; ?>">
                <i class="fa fa-calendar feel_icon"></i>
                <p class="feel_text"><?php echo $tour->post_title; ?></p>
            </div>
        <?php } ?>
        <?php if (empty($tourPackages)) { ?>
            <div class="col-md-12 feel_display">
                <p class="feel_text" style="margin:0;">You have not created any tour package.</p>
            </div>
        <?php } ?>

    </div>
</div>
<!--End MyTimeLine Popup HTML -->

<!-- Feeling popup for mobile -->
<div class="feeling-activity-m" style="left: 0;top: 0;">
    <div class="filing-section-main">
        <div class="Fill-header-top h-top-new">
            <a class="cancel-f" href="#">Cancel</a>
            <h4>What are you doing?</h4>
        </div>

        <form action="method" class="form-emoji">
            <?php include(get_template_directory() . '/inc/emoji.php'); ?>
        </form>
    </div>

</div>
<!--End Feeling popup for mobile -->

<!-- Tags Friends popup for mobile -->
<div class="tags-friend-m" style="left: 0; top: 0">
    <div class="tags-section-main">
        <div class="Fill-header-top h-top-new">
            <a class="cancel-tags" href="#">Cancel</a>
            <h4>Who are you with?</h4>
        </div>

        <div class="tagBodyM">
            <div class="typing_name">
                <input type="text" placeholder="Start typing a name...">
            </div>

            <div class="suggestion_name">
                <input type="text" placeholder="SUGGESTION">
            </div>

            <div class="userTagArea">
                <?php foreach ($friends as $friend) { ?>
                    <div class="user-1 tag_friend" userid="<?php echo $friend->ID; ?>">
                        <img src="<?php echo um_get_user_avatar_data($friend->ID)['url']; ?>">
                        <p class="username"><?php echo um_get_display_name($friend->ID); ?></p>
                        <div class="border-b-u"></div>
                    </div>
                <?php } ?>
            </div>
        </div>

    </div>

</div>
<!-- End Tags Friends popup for mobile -->





<!-- Tours packages Popup mobile -->
<div class="tour-packages-Popup tours-p" style="left: 0; top:0">
    <div class="tag-people-main">
        <div class="tour-header-top">
            <div class="col-md-12">
                <a class="cancel-tours" href="#">Cancel</a>
                <h4>My Tour Packages</h4>
            </div>
        </div>

        <div class="tagBodyM tourBodyM">

            <div class="input-group md-form form-sm form-1 pl-0" style="left:0">

                <div class="input-group-prepend">
                    <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="basic-text1">
                        <i class="fas fa-search text-white" aria-hidden="true"></i>
                    </button>
                </div>

                <form class="search_hea" action="" method="get" style="margin-bottom:0">
                    <input class="form-control my-0 py-1" type="text" placeholder="Search for tour packages" aria-label="Search" name="friend">
                </form>

            </div>

            <div class="tour-ps">
                <p class="sugg">Tour Packages Name</p>
            </div>
            <?php foreach ($tourPackages as $tour) { ?>
                <div class="col-md-12 feel_display tourpackage" tour_id="<?php echo $tour->tour_id; ?>">
                    <i class="fa fa-calendar feel_icon"></i>
                    <p class="feel_text"><?php echo $tour->post_title; ?></p>
                </div>
            <?php } ?>
            <?php if (empty($tourPackages)) { ?>
                <div class="col-md-12 feel_display">
                    <p class="feel_text" style="margin:0;">You have not created any tour package.</p>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- End Tours packages Popup mobile -->

<div class="normal-tab">
    <div class="main_area mobile-main">
        <div class="mytimelinewrap">

            <?php include 'timeline-common-section.php'; ?>

            <div class="timeline_for_mobile">
                <!--Balouch Khan-->

                <!-- see all connection popup -->

                <!-- Modal -->

                <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document" style="margin-top: 20%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="popup_header">
                                    <div class="popupheader_content">
                                        <div class="back_btn">
                                            <button data-dismiss="modal" class="fas fa-arrow-left"></button>
                                        </div>
                                        <div class="popup_header_title">
                                            Tanvir Ahmed Friend list
                                        </div>
                                        <div class="search_icon">
                                            <i class="fas fa-search"></i>
                                        </div>
                                    </div>

                                    <div class="popupsearch">
                                        <input type="text" class="header_search" placeholder="Search Your Connection Here." />
                                    </div>

                                </div>
                            </div>


                            <div class="modal-body">
                                <p class="margin_r">A</p>
                                <hr />
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object media_friend_image" src="https://secure.gravatar.com/avatar/?s=380&d=wavatar&r=g 2x" alt="...">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Abdul Samad</h4>
                                        <p class="remove_m">Connections : 2</p>
                                        <button class="btn">Connected</button>
                                    </div>
                                </div>

                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object media_friend_image" src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/851/profile_photo-190x190.jpg?1575060068" alt="...">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Abdul Samad</h4>
                                        <p>Connections : 30</p>
                                        <button class="btn">Connected</button>
                                    </div>
                                </div>

                                <p class="margin_r">B</p>
                                <hr />
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object media_friend_image" src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/851/profile_photo-190x190.jpg?1575060068" alt="...">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Abdul Samad</h4>
                                        <p>Connections : 30</p>
                                        <button class="btn">Connected</button>
                                    </div>
                                </div>

                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object media_friend_image" src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/851/profile_photo-190x190.jpg?1575060068" alt="...">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Abdul Samad</h4>
                                        <p>Connections : 30</p>
                                        <button class="btn">Connected</button>
                                    </div>
                                </div>

                            </div>




                        </div>
                    </div>
                </div>


                <!-- see all connection popup -->



                <!-- photos popup -->

                <!-- Modal -->

                <div class="modal" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document" style="margin-top: 20%;">
                        <div class="modal-content" style="min-height: 400px;">
                            <div class="modal-header">
                                <div class="popup_header">
                                    <div class="popupheader_content">
                                        <div class="back_btn">
                                            <button data-dismiss="modal" class="fas fa-arrow-left"></button>
                                        </div>
                                        <div class="popup_header_title">
                                            Photos
                                        </div>
                                        <div class="search_icon">

                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="modal-body">
                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Profile Pictures</a></li>
                                        <!--li role="presentation">
                                        <a href="#profile" class="custom_color" aria-controls="profile" role="tab" data-toggle="tab">Uploaded Media</a>
                                    </li-->
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="home">
                                            <div class="peoples_row">
                                                <?php foreach ($user_photos as $p) { ?>
                                                    <div class="col_3">
                                                        <?php if (stripos($p->post_mime_type, 'image') !== false) { ?>
                                                            <img src="<?php echo $p->guid; ?>">
                                                        <?php } else {
                                                            $video_thumbnail_url = get_post_meta($p->ID, 'kgvid-poster-url', true) ?>
                                                            <video poster="<?php echo $video_thumbnail_url ?>"
                                                                src="<?php echo $p->guid; ?>" controls="controls"></video>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="profile">
                                            <div class="peoples_row">
                                                <div class="col_3">
                                                    <img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/851/profile_photo-190x190.jpg?1575060068">
                                                </div>
                                                <div class="col_3">
                                                    <img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/851/profile_photo-190x190.jpg?1575060068">
                                                </div>
                                                <div class="col_3">
                                                    <img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/851/profile_photo-190x190.jpg?1575060068">
                                                </div>
                                            </div>

                                            <div class="peoples_row">
                                                <div class="col_3">
                                                    <img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/851/profile_photo-190x190.jpg?1575060068">
                                                </div>
                                                <div class="col_3">
                                                    <img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/851/profile_photo-190x190.jpg?1575060068">
                                                </div>
                                                <div class="col_3">
                                                    <img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/851/profile_photo-190x190.jpg?1575060068">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>




                        </div>
                    </div>
                </div>


                <!-- photos popup -->



                <div class="social_reviews">
                    <div class="editprofile_mobile">


                        <?php
                        if (is_user_logged_in()) {
                            if ($user_id == get_current_user_id()) {
                        ?>
                                <a href="https://www.travpart.com/English/user/<?php echo $username; ?>/?profiletab=pages">
                                    <span class="edit_icon_profile">
                                        <i class="fas fa-user-edit"></i>
                                    </span>
                                </a>
                            <?php  } else { ?>
                                <a href="https://www.travpart.com/English/user/<?php echo $username; ?>/?profiletab=profile">
                                    <span class="edit_icon_profile">
                                        <i class="fas fa-eye"></i>
                                    </span>
                                </a>
                        <?php   }
                        } ?>


                        <p>
                            <?php
                            if (is_user_logged_in()) {
                                if ($user_id == get_current_user_id()) {
                                    echo '<a target="_blank" href="https://www.travpart.com/English/user/' . $username . '/?profiletab=profile">Edit Profile</a>';
                                } else {
                                    echo '<a target="_blank" href="https://www.travpart.com/English/user/' . $username . '/?profiletab=profile">View Profile</a>';
                                    

                                }
                            }
                            ?>
                        </p>
                        <?php if($_GET['debug']==1){ ?>
                        <p>
                            <button class="btn btn-small report_btn">
                                <i class="fas fa-flag"></i> Report
                            </button>
                        </p>
                        <?php } ?>
                    </div>
                    <!--<div class="connection_mobile">
            <h6 class="follow-time">Connection</h6>
            <p><?php echo $connection_count  > 0 ? $connection_count  : 0; ?></p>
        </div>-->
                    <div class="copylink_profile_mobile">
                        <div class="copy_link_profile">
                            <a id="copy-link-profile" data-clipboard-text="<?php echo home_url('my-time-line/?user=' . $user->user_login); ?>">
                                <span class="copy_link_profile">
                                    <i class="fas fa-link"></i>
                                </span>
                            </a>
                            <p>Copy link to Profile</p>
                        </div>
                    </div>
                </div>


                <div class="what_happening_mobile">
                    <!-- whats_happening_section -->

                    <?php if ($user->ID == wp_get_current_user()->ID && get_user_meta($current_user->ID, 'profile_completeness', true) != 1) { ?>
                        <div class="what_happening_mobile_innerdiv">
                            <h5><b>Profile Completeness</b></h5>
                            <div class="progress progress-striped">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo get_user_meta($user->ID, 'profile_completeness', true) * 100; ?>%;background-color: #0078fe;">
                                    <span class="sr-only"><?php echo get_user_meta($current_user->ID, 'profile_completeness', true) * 100; ?>%</span>
                                </div>
                            </div>
                            <!--<div class="avatar_profile">
                <div class="inner_img">
                    <img src="https://secure.gravatar.com/avatar/?s=380&d=wavatar&r=g 2x" />
                </div>
            </div>
            <div class="content_avatar">
                <p><strong>What's Happening "User Name"?</strong></p>
                <p>Here Goes Description this is test description. Here Goes Description this is test description.</p>
            </div>
            <div class="clearfix"></div>
            <div class="what_happening_mobile">
                <div class="notnow_profile_button">
                    <button class="btn_custom_happening"> Not Now </button>
                </div>
                <div class="update_profile_button">
                    <button class="btn_custom_happening"> Update Profile </button>
                </div>
                <div class="clearfix">
                    
                </div>
            </div>-->

                        </div>
                    <?php } ?>

                </div><!-- whats_happening_section end-->


                <div class="social_reviews_mobile" onclick="myFunction()">
                    <h2 class="social_review_head">Social Reviews</h2>
                    <div class="row">
                        <div class="col-xs-12 col-md-6 text-center for_right">

                            <h1 class="rating-num"><?php echo round($avg_rating, 1); ?></h1>
                            <div class="rating">
                                <?php for ($i = 0; $i < floor($avg_rating); $i++) { ?>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                <?php } ?>
                                <?php if (floor($avg_rating) < ceil($avg_rating)) { ?>
                                    <i class="fas fa-star-half-alt" aria-hidden="true"></i>
                                <?php } ?>
                                <?php for ($i = ceil($avg_rating); $i < 5; $i++) { ?>
                                    <i class="far fa-star" aria-hidden="true"></i>
                                <?php } ?>
                            </div>
                            <div>
                                <i class="fas fa-user-tie"></i> <?php echo number_format($rating_people_count); ?> total
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 for_left ">
                            <div class="row rating-desc remove_mar">
                                <div class="col-xs-3 col-md-3 text-right for_left_bar">
                                    5
                                </div>
                                <div class="col-xs-8 col-md-9 for_right_bar">
                                    <div class="progress progress-striped">
                                        <div class="progress-bar progress-rating-5" style="<?php echo 'width: ' . ($ratings['star5'] / $rating_people_count * 100) . '%'; ?>">
                                            <span class="sr-only"><?php echo $ratings['star5'] / $rating_people_count * 100; ?>%</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- end 5 -->
                                <div class="col-xs-3 col-md-3 text-right for_left_bar">
                                    4
                                </div>
                                <div class="col-xs-8 col-md-9 for_right_bar">
                                    <div class="progress">
                                        <div class="progress-bar progress-rating-4" style="<?php echo 'width: ' . ($ratings['star4'] / $rating_people_count * 100) . '%'; ?>">
                                            <span class="sr-only"><?php echo $ratings['star4'] / $rating_people_count * 100; ?>%</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- end 4 -->
                                <div class="col-xs-3 col-md-3 text-right for_left_bar">
                                    3
                                </div>
                                <div class="col-xs-8 col-md-9 for_right_bar">
                                    <div class="progress">
                                        <div class="progress-bar progress-rating-3" style="<?php echo 'width: ' . ($ratings['star3'] / $rating_people_count * 100) . '%'; ?>">
                                            <span class="sr-only"><?php echo $ratings['star3'] / $rating_people_count * 100; ?>%</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- end 3 -->
                                <div class="col-xs-3 col-md-3 text-right for_left_bar ">
                                    2
                                </div>
                                <div class="col-xs-8 col-md-9 for_right_bar">
                                    <div class="progress">
                                        <div class="progress-bar progress-rating-2" style="<?php echo 'width: ' . ($ratings['star2'] / $rating_people_count * 100) . '%'; ?>">
                                            <span class="sr-only"><?php echo $ratings['star2'] / $rating_people_count * 100; ?>%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <!-- end 2 -->
                                <div class="col-xs-3 col-md-3 text-right for_left_bar">
                                    1
                                </div>
                                <div class="col-xs-8 col-md-9 for_right_bar">
                                    <div class="progress">
                                        <div class="progress-bar progress-rating-1" style="<?php echo 'width: ' . ($ratings['star1'] / $rating_people_count * 100) . '%'; ?>">
                                            <span class="sr-only"><?php echo $ratings['star1'] / $rating_people_count * 100; ?>%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <!-- end 1 -->
                            </div>
                            <!-- end row -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>


                <!--followers section-->
                <div class="followers_div_mobile">
                    <div class="followers_div_mobile_mobile">
                        <div class="followers_left_div">
                            <h4>
                                <strong> Followers : <a href="https://www.travpart.com/English/followers/"><?php echo $follow_count  > 0 ? $follow_count  : 0; ?></a></strong></h4>
                        </div>
                        <!--<div class="followers_right_div">
                            <h4>: <a href="#"><?php echo $follow_count  > 0 ? $follow_count  : 0; ?></a></h4>
                        </div>-->
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!--followers section-->
                <div class="green_big"></div>

                <?php

                // friends count

                $friends_count = $wpdb->get_var(
                    "SELECT COUNT(*) FROM friends_requests WHERE  user2 = '{$user_id}' OR user1 = '{$user_id}' AND status=1"
                );

                ?>
                <div class="connection_div">
                    <div class="connection_header">
                        <h4 class="remove_margin connection_header_text">
                            <strong>Connections : <a href="https://www.travpart.com/English/my-connection/?user=<?php echo esc_html($username); ?>"><?php echo $friends_count;  ?></a></strong>
                        </h4>
                        <i class="fas fa-chevron-up connection_header_icon"></i>
                        <i class="fas fa-chevron-down connection_header_icon"></i>
                    </div>
                    <div class="peoples_row">
                        <?php

                        $list1 = $wpdb->get_results(
                            "SELECT user1 FROM friends_requests WHERE  user2 = '{$user_id}'  AND status=1  limit 6"
                        );
                        foreach ($list1 as $value) {

                            $friend_list1[] = $value->user1;
                        }

                        $list2 = $wpdb->get_results(
                            "SELECT user2 FROM friends_requests WHERE  user1 = '{$user_id}'  AND status=1 limit 6"
                        );
                        foreach ($list2 as $value) {

                            $friend_list2[] = $value->user2;
                        }
                        if (!empty($friend_list1) and !empty($friend_list2)) {
                            $final = array_merge($friend_list1, $friend_list2);
                        } elseif (empty($friend_list1)) {

                            $final = $friend_list2;
                        } elseif (empty($friend_list2)) {

                            $final = $friend_list1;
                        } else {
                            $final = "";
                        }

                        foreach ($final as $value) {

                            $Data = $wpdb->get_row("SELECT * FROM `wp_users` WHERE ID = '$value' limit 6");

                        ?>


                            <div class="col_3"><a target="_blank" href="https://www.travpart.com/English/my-time-line/?user=<?php echo $Data->user_login; ?>">
                                    <?php echo get_avatar($Data->ID, 100);  ?>
                                    <p class="people_margin"><strong><?php echo $Data->user_login ?></strong></p>
                                </a></div>



                        <?php  } ?>
                    </div>

                </div>

         <?php if (is_user_logged_in()) { ?>
                <div class="small_green_big"></div>
                
                <div class="mobile_posts_section">
                    <div class="post_section_create_mobile">
                        <h6><strong>POSTS</strong></h6>

                        <div class="profile_photo_createpost">
                            <img src="<?php echo um_get_user_avatar_data(wp_get_current_user()->ID)['url'] ?>" />
                        </div>
                        <div class="profile_input_createpost">
                            <input class="input_lo" placeholder="What’s going on?" />

                            <div class="m-add-photo">
                                <i class="fas fa-images"></i>
                                <span>Photo</span>
                            </div>
                            <div class="popup">
                                <div class="col-md-12" style="display: flex;">
                                    <div class="col-md-2" style="text-align: left;">
                                        <i class="ph_arr_back1 fas fa-arrow-left" name="close" style="font-size: 15px"></i>
                                        <i class="ph_arr_back2 fas fa-arrow-left" style="font-size: 15px"></i>
                                    </div>
                                    <div class="col-md-10" style="margin-left: -78px;text-align: left;">
                                        <p class="" style="font-size: 15px !important">Photos</p>
                                    </div>
                                </div>

                                <div class="row_ph">
                                    <?php foreach ($user_photos as $p) { ?>
                                        <div class="column_ph">
                                            <img id="phpop111" src="<?php echo $p->guid; ?>" onclick="myphFunction('<?php echo $p->guid; ?>')" style="width: 100%">
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="row1_ph">
                                    <img id="single_pop_img" src="" style="width: 100%">
                                </div>
                            </div>

                        </div>

                    </div>
              </div>        
<?php } ?>
              


                <div class="small_green_big"></div>
                <div class="life_expr_header">
                    <h4 class="life_expr_header_text">
                        <strong>Life Experience</strong>
                    </h4>
                    <i class="fas fa-chevron-up life_expr_header_icon"></i>
                    <i class="fas fa-chevron-down life_expr_header_icon"></i>
                </div>

                <div class="photos_grid">
                    <div class="photos_grid_row">
                        <?php /*
                        <div class="photos_grid_coloum bg_image_grid_1 ">
                            <div  data-js="open" class="vertical_content_ph mode_d1">
                                Photos
                            </div>
                        </div> */ ?>
                        <?php if ($role = 'Buyer') { ?>
                            <div class="photos_grid_coloum bg_image_grid_2">
                                <div class="vertical_content_trpkg">
                                    <a href="<?php echo add_query_arg(array('user' => $user->user_login), home_url('my-tour-experience/')); ?>">Tour Packages</a>
                                </div>
                            </div>
                            <!--<div class="photos_grid_coloum bg_image_grid_3">
                                <div class="vertical_content_indes">
                                    <a href="<?php //echo add_query_arg(array('user' => $user->user_login), home_url('my-interested-destination/')); 
                                                ?>">Interested Destination</a>
                                </div>
                            </div>-->
                            <div class="photos_grid_coloum bg_image_grid_3">
                                <div class="vertical_content_indes">
                                    <a href="#"><span class="badge">25</span> Visited Destinations</a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="photos_grid_row">
                        <div class="photos_grid_coloum bg_image_grid_4 ">
                            <div class="vertical_content_follow">
                                <a href="https://www.travpart.com/English/following?user=<?php echo $username ?>">Following</a>
                            </div>
                        </div>
                        <div class="photos_grid_coloum bg_image_grid_5">
                            <div class="vertical_content_tvl_feed">
                                <a href="#">Travel Feedbacks</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="small_green_big"></div>
                
                <div class="p_v_mobile">
                    <a href="https://www.travpart.com/English/my-photo-videos/?user=<?php echo esc_html($username); ?>">
                        Photos &amp; Videos
                    </a>    
                </div>
                
                <div class="photos_timeline_header">
                    
                    <div class="photos_header_icon">
                        <i class="fas fa-table"></i>
                    </div>
                    <div class="timeline_header_icon">
                        <i class="fas fa-clock"></i>
                    </div>
                </div>
                
                <div class="mytime_mob_photo_gallery" userid="<?php echo $user_id; ?>">
                    <?php $a=0; foreach ($user_photos as $p) { $a++; ?>
                        <?php if (stripos($p->post_mime_type, 'image') !== false) { ?>
                            <div class="mytime_ph_gallery">
                                <img class="mytime_ph_gallery_popup" src="<?php echo $p->guid; ?>">
                            </div>
                        <?php } else {
                            $video_thumbnail_url = get_post_meta($p->ID, 'kgvid-poster-url', true) ?>
                            <div class="mytime_vd_gallery">
                                <video poster="<?php echo $video_thumbnail_url ?>" src="<?php echo $p->guid; ?>"></video>
                                <div class="video_overlay">
                                    <div class="vd_ovlay_btn">
                                        <!--<a href="<?php //echo $p->guid; ?>" class="mytime_vd_gallery_popup"><i class="fas fa-play"></i></a><i class="fas fa-volume-up"></i>-->
                                        <div style="float: left;">
                                           <a href="<?php echo $p->guid; ?>" class="mytime_video_popup">
                                                <i class="fas fa-play"></i>
                                           </a> 
                                       </div>  
                                        <div class="ph-share-mobile ph-share-mobile<?php echo $p->ID; ?>" style="float: right;padding: 0px;">
       

                                            
                                            <!-- Button trigger modal -->
                                        
                                            <a href="#">
                                                <i class="fas fa-share"></i>
                                            </a>
                                            
                                             <!--Share My DropDown PC-->
                                           <div class="ph_share_mob_dropdown ph_share_mob_dropdown<?php echo $p->ID; ?>">
                                                                            <?php if(is_user_logged_in()) {?>
                                                                    <li onclick="share_feed(<?php echo $p->ID; ?>)">Share Now</li>
                                                                <?php }?>

                


                                                                    <li><a target="_blank" onclick="record_share_data('<?php echo $value->ID; ?>','whatsapp')" href="https://web.whatsapp.com/send?text=<?php echo substr($value->post_content, 0, 80) . "  ( see-more ) "; echo get_permalink($value->ID)?>"><div class="wh_mytime_icon_div"><i class="fab fa-whatsapp share_wh_mytime_i"></i></div>Share to whats app</a></li>
                                                                    <li><a  href="#" onclick="copy_link('<?php echo $p->ID; ?>')"><i class="far fa-copy share_copy_link_mytime_i"></i>Copy link to this post</a></li>

                                                                  <!--  <li class="mores"><a class="a2a_dd" href="#"><i class="fas fa-plus-square"></i> More...</a></li>
                                                                  -->
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="<?php echo get_permalink($value->ID) ?>" data-a2a-title="<?php echo substr($value->post_content, 0, 80) ?>">
                    <a class="a2a_button_facebook" onclick="record_share_data('<?php echo $value->ID; ?>','fb')"></a>
                    <a class="a2a_button_twitter" onclick="record_share_data('<?php echo $value->ID; ?>','twitter')"></a>
                    <a class="a2a_dd" href="https://www.addtoany.com/share" onclick="record_share_data('<?php echo $value->ID; ?>','other')"></a>
                    </div>

                                                                        </div>
                                                                        <script type="text/javascript">
                                                                            $(document).ready(function () {
                                                                                $('.ph-share-mobile<?php echo $p->ID; ?>').on('click',function () {
                                                                                    $('.ph_share_mob_dropdown<?php echo $p->ID; ?>').toggle();
                                                                                });
                                                                            });

                                                                            
                                                                        </script>

                                            <!--End My DropDown-->

                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    
                </div>
            </div>
            <!--Balouch Khan-->



            <!-- Short post section -->
            <section class="elementor-element elementor-element-f6cfa78 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section elementor-column-wrapsec3" data-id="f6cfa78" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row elementor-rowsec">
                        <div class="elementor-element timeline_for_desktop elementor-element-986189b elementor-column elementor-col-50 elementor-top-column" data-id="986189b" data-element_type="column">
                            <div class="elementor-element-populatedsec social-follow-pos">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-1429839 elementor-widget elementor-widget-html" data-id="1429839" data-element_type="widget" data-widget_type="html.default">
                                        <div class="elementor-widget-container">

                                            <div class="div_m div_mss">

                                                <div class="row custom_ds" style="margin: 0px;">
                                                    <div class="col-xs-12 remove_pad col-md-5 text-center for_right">

                                                        <h1 class="rating-num" style="color: #000;"><?php echo round($avg_rating, 1); ?></h1>
                                                        <div class="rating custom_size_icon">
                                                            <?php for ($i = 0; $i < floor($avg_rating); $i++) { ?>
                                                                <i class="fas fa-star" aria-hidden="true"></i>
                                                            <?php } ?>
                                                            <?php if (floor($avg_rating) < ceil($avg_rating)) { ?>
                                                                <i class="fas fa-star-half-alt" aria-hidden="true"></i>
                                                            <?php } ?>
                                                            <?php for ($i = ceil($avg_rating); $i < 5; $i++) { ?>
                                                                <i class="far fa-star" aria-hidden="true"></i>
                                                            <?php } ?>
                                                        </div>
                                                        <h6 style="color: #000;font-weight: bold;">Social Reviews</h6>
                                                    </div>
                                                    <div class="col-xs-12 remove_pad col-md-6 for_left ">
                                                        <div class="row rating-desc">
                                                            <div class="col-xs-3 remove_pad col-md-3 text-center for_left_bar">
                                                                5
                                                            </div>
                                                            <div class="col-xs-8 col-md-9 remove_pad for_right_bar">
                                                                <div class="progress remove_mar">
                                                                    <div class="progress-bar progress-rating-5" style="<?php echo 'width: ' . ($ratings['star5'] / $rating_people_count * 100) . '%'; ?>">
                                                                        <span class="sr-only"><?php echo $ratings['star5'] / $rating_people_count * 100; ?>%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end 5 -->
                                                            <div class="col-xs-3 col-md-3 remove_pad text-center for_left_bar">
                                                                4
                                                            </div>
                                                            <div class="col-xs-8 col-md-9 remove_pad for_right_bar">
                                                                <div class="progress remove_mar">
                                                                    <div class="progress-bar progress-rating-4" style="<?php echo 'width: ' . ($ratings['star4'] / $rating_people_count * 100) . '%'; ?>">
                                                                        <span class="sr-only"><?php echo $ratings['star4'] / $rating_people_count * 100; ?>%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end 4 -->
                                                            <div class="col-xs-3 col-md-3  remove_pad text-center for_left_bar">
                                                                3
                                                            </div>
                                                            <div class="col-xs-8 col-md-9 remove_pad for_right_bar">
                                                                <div class="progress remove_mar">
                                                                    <div class="progress-bar progress-rating-3" style="<?php echo 'width: ' . ($ratings['star3'] / $rating_people_count * 100) . '%'; ?>">
                                                                        <span class="sr-only"><?php echo $ratings['star3'] / $rating_people_count * 100; ?>%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end 3 -->
                                                            <div class="col-xs-3 remove_pad col-md-3 text-center for_left_bar ">
                                                                2
                                                            </div>
                                                            <div class="col-xs-8 remove_pad col-md-9 for_right_bar">
                                                                <div class="progress remove_mar">
                                                                    <div class="progress-bar progress-rating-2" style="<?php echo 'width: ' . ($ratings['star2'] / $rating_people_count * 100) . '%'; ?>">
                                                                        <span class="sr-only"><?php echo $ratings['star2'] / $rating_people_count * 100; ?>%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <!-- end 2 -->
                                                            <div class="col-xs-3 remove_pad col-md-3 text-center for_left_bar">
                                                                1
                                                            </div>
                                                            <div class="col-xs-8 remove_pad col-md-9 for_right_bar">
                                                                <div class="progress remove_mar">
                                                                    <div class="progress-bar progress-rating-1" style="<?php echo 'width: ' . ($ratings['star1'] / $rating_people_count * 100) . '%'; ?>">
                                                                        <span class="sr-only"><?php echo $ratings['star1'] / $rating_people_count * 100; ?>%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <!-- end 1 -->
                                                        </div>
                                                        <!-- end row -->
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="connection_d">
                                                    <div class="connection_table">
                                                        <div class="table_row">
                                                            <div class="connection_td for_70">
                                                                Connection
                                                            </div>
                                                            <div class="connection_td for_3">
                                                                :
                                                            </div>
                                                            <div class="connection_td for_20">
                                                                <a href="https://www.travpart.com/English/my-connection/?user=<?php echo esc_html($username); ?>" style="text-decoration: none;"><?php echo $connection_count  > 0 ? $connection_count  : 0; ?></a>
                                                            </div>
                                                        </div>
                                                        <div class="table_row">
                                                            <div class="connection_td for_70">
                                                                Followers&nbsp;
                                                            </div>
                                                            <div class="connection_td for_3">
                                                                :
                                                            </div>
                                                            <div class="connection_td for_20">
                                                                <a href="https://www.travpart.com/English/followers/?user=<?php echo $username ?>" style="text-decoration: none;"><?php echo $follow_count  > 0 ? $follow_count  : 0; ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <!--<div class="feedback-area">
                            <h2 class="social-rev">Social Reviews</h2>
                            <div class="s_star">
                                <?php for ($i = 0; $i < floor($avg_rating); $i++) { ?>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                <?php } ?>
                                <?php if (floor($avg_rating) < ceil($avg_rating)) { ?>
                                    <i class="fas fa-star-half-alt" aria-hidden="true"></i>
                                <?php } ?>
                                <?php for ($i = ceil($avg_rating); $i < 5; $i++) { ?>
                                    <i class="far fa-star" aria-hidden="true"></i>
                                <?php } ?>
                                <span itemprop="ratingValue" class="elementor-screen-only">3/5</span>
                                <span class="stat-f"><?php echo round($avg_rating, 1); ?></span>
                            </div>
                        </div>-->
                                        </div>
                                    </div>
                                    <!--<div class="elementor-element elementor-element-2485dcc elementor-widget elementor-widget-text-editor" data-id="2485dcc" data-element_type="widget" data-widget_type="text-editor.default">
                                    <div class="elementor-widget-container">
                                        <div class="elementor-text-editor elementor-clearfix">
                                           
                                        </div>
                                    </div>
                                </div>-->
                                    <!--<div class="elementor-element elementor-element-2485dcc elementor-widget elementor-widget-text-editor" data-id="2485dcc" data-element_type="widget" data-widget_type="text-editor.default">
                                    <div class="elementor-widget-container">
                                        <div class="elementor-text-editor elementor-clearfix">
                                            
                                        </div>
                                    </div>
                                </div>-->
                                </div>
                            </div>
                        </div>
                        
                        <!-- Post Section By Nazmul -->
                        <div class="elementor-element elementor-element-36e06d8 timelinecontentwrap elementor-column elementor-col-50 elementor-top-column" data-id="36e06d8" data-element_type="column"> 
                            
                            <div class="container new-short-post mytimepst">
                            <?php if (is_user_logged_in() AND empty ($query_to_check_block)) { ?>
                            <div class="row customda">
                                
                                <div class="col-md-12">
                                    <div class="create_post">
                                        <?php //if($_GET['debug']==1){ ?>
                                            <div class="row2">
                                                <div class="iconsd icon1">
                                                    create post -
                                                </div>
                                                <div class="iconsd icon2 create_event_desktop">
                                                    <div class="cr_event cr_vd">
                                                        <div class="for_r event_image_icon event_icon_desktop">
                                                            <i class="fas fa-calendar-alt"></i>
                                                        </div>
                            <div class="for_r event_text_icon event_text_desktop">
                                                            <p>
                                                                Create an <br /> Event
                                                            </p>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                <div class="looking-travel-friend_desktop">
                                                <div class="iconsd icon3">
                                                    <div class="cr_event">
                                                      <label>
                                                        <div class="for_r event_check_icon">
                                                            <input type="radio" name="event_n" atype="1" />
                                                        </div>
                                                        <div class="for_r event_image_icon">
                                                            <i class="fas fa-user-friends"></i>
                                                        </div>
                                                        <div class="for_r event_text_icon">
                                                            <p>
                                                                Looking for <br /> a Travel Friend
                                                            </p>
                                                        </div>
                                                      </label>
                                                    </div>
                                                </div>
                                                <div class="iconsd icon4">
                                                    <div class="cr_event">
                                                      <label>
                                                        <div class="for_r event_check_icon">
                                                            <input type="radio" name="event_n" atype="2" />
                                                        </div>
                                                        <div class="for_r event_image_icon">
                                                            <i class="fas fa-coffee"></i>
                                                        </div>
                                                        <div class="for_r event_text_icon">
                                                            <p>
                                                                Want to <br /> Hangout
                                                            </p>
                                                        </div>
                                                      </label>
                                                    </div>
                                                </div>
                                                <div class="iconsd icon5">
                                                    <div class="cr_event">
                                                      <label>
                                                        <div class="for_r event_check_icon">
                                                            <input type="radio" name="event_n" atype="3" />
                                                        </div> 
                                                        <div class="for_r event_image_icon">
                                                            <i class="fas fa-wine-bottle"></i>
                                                        </div>
                                                        <div class="for_r event_text_icon">
                                                            <p>
                                                                Going for<br /> Party
                                                            </p>
                                                        </div>
                                                      </label>
                                                    </div>
                                                </div>
                                                <div class="iconsd icon6">
                                                    <div class="cr_event">
                                                      <label>
                                                        <div class="for_r event_check_icon">
                                                            <input type="radio" name="event_n" atype="4" />
                                                        </div>
                                                        <div class="for_r event_image_icon">
                                                            <i class="fas fa-futbol"></i>
                                                        </div>
                                                        <div class="for_r event_text_icon">
                                                            <p>
                                                                Going for<br /> Sport
                                                            </p>
                                                        </div>
                                                      </label>
                                                    </div>
                                                </div>
                                                </div>
                                                
                                                
                                                <div class="iconsd_d">
                                                    <div class="">
                                                        <div class="dis_b">
                                                            <strong>
                                                                <span class="tabla_hi"> Share to <br /> Facebook </span>
                                                                <span class="tabla_show">  <i class="fab fa-facebook-f"></i> </span>
                                                            </strong>
                                                        </div>
                                                        <div class="dis_b">
                                                            <p></p>
                                                        </div>
                                                        <div class="dis_b">
                                                            <label class="switch">
                                                              <input type="checkbox" <?php  echo ($fb_sharing_option==1) ? "checked" : ""  ?>  name="toggle" id="toggle">
                                                              <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                                



                                                
                                            </div>
                                        <?php //} ?>
                                        
                                        
                                        <!--<h4 >
                                            Create a Post
                                        </h4>

                                        <div class="for_createevent_div_desktop">
                                            <div class="create_event_desktop">
                                                <a href="#">Create an <br /> Event</a>
                                            </div>
                                        </div>
                                        <div class="for_looking_travel_div_desktop looking-travel-friend_desktop" style="width: 178px;padding-left: 5px;padding-right: 5px;" >
                                            <div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
                                                <div class="col-md-2" style="padding-left: 0px;padding-right: 0px;">
                                                    <i class="fas fa-circle"></i>
                                                    <i class="far fa-check-circle"></i>
                                                </div>
                                                <div class="looking_travil_desktop col-md-10" style="padding-left: 0px;padding-right: 0px;">

                                                    <a href="#">Looking for a travel friend</a>
                                                    <br><span>[notify your friend]</span>
                                                </div>
                                            </div>
                                        </div>-->
                                    </div>

                                    <div class="add_locations">
                                        <div class="wrap-mytimeline">
                                            <div class="search">
                                                <input id="short-post-location" type="text" class="searchTerm" placeholder="Add a location tag (optional)">
                                                <input type="hidden" id="short-post-lat" />
                                                <input type="hidden" id="short-post-lng" />
                                                <button type="submit" class="searchButton">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>



                                            <div class="Search-DatePicker">
                                                <div class='date-area'>
                                                    <div class='date-container'>
                                                        <label class="start-date">Start Date: </label>
                                                        <div class="short-post-datepicker">
                                                            <input id="short-post-startdate" type="text" placeholder="required" />
                                                            <span class="calendar-btn"><span class="fas fa-calendar-alt"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class='date-area'>
                                                    <div class='date-container'>
                                                        <label class="start-date">End Date: </label>
                                                        <div class="short-post-datepicker">
                                                            <input id="short-post-enddate" type="text" placeholder="required" />
                                                            <span class="calendar-btn"><span class="fas fa-calendar-alt"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="text-area">

                                                <?php
                                                $match_name = um_user('user_login');
                                                $cuser = wp_get_current_user();
                                                ?>
                                                <?php if ($cuser->user_login == $match_name) { ?>
                                                    <textarea id="short-post-content" placeholder="What's going on <?php echo um_get_display_name( wp_get_current_user()->ID); ?>?"></textarea>
                                                <?php } else { ?>
                                                    <textarea id="short-post-content" placeholder="Tell <?php echo um_get_display_name( wp_get_current_user()->ID); ?> Something"></textarea>
                                                <?php } ?>
                                            </div>
                                            <div class="custom_photos" style="display:none">
                                                <div class="buttons_row_add">
                                                    <div class="add_img  image_d add-photo-video">
                                                        <div class="border_d">
                                                            <i class="fas fa-plus"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input id="short-post-feeling" type="hidden" value="" />
                                            <input id="short-post-tagfriend" type="hidden" value="" />
                                            <input id="short-post-tourpackage" type="hidden" value="" />
                                            <input id="looking-for-travel-friend" type="hidden" value="0" />
                                            <input class="post-featured-image-id" type="hidden" value="-1" />
                                            <?php wp_nonce_field('short_post_nonce_action', 'shortpostnonce'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row_buttons">
                                <div class="row" style="border-bottom: 1px solid #000;    margin: 0px;">
                                    <div class="col-md-12">
                                        <div class="post_area" style="background: #f6f6f6;">
                                            <div class="row">
                                                <div class="col-md-3 w-md-20">
                                                    <div class="add-photo-video widget_area" class="widget_area">

                                                        <div class="t-box-icon">
                                                            <span calss="icon-box"></span>
                                                            <i class="fas fa-images"></i>
                                                        </div>

                                                        <div class="icon-box-content">
                                                            <h4 class="icon-box-title">
                                                                <span>Photo/Video</span>
                                                            </h4>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="col-md-3 w-md-20">
                                                    <div class="widget_area">
                                                        <div class="t-box-icon">
                                                            <span calss="icon-box"></span>
                                                            <i aria-hidden="true" class="far fa-smile"></i>
                                                        </div>

                                                        <div class="icon-box-content">
                                                            <h4 class="icon-box-title">
                                                                <span class="timefeeling-popup">Feeling/Activity</span>
                                                            </h4>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-3 w-md-20">
                                                    <div class="widget_area">
                                                        <div class="t-box-icon">
                                                            <span calss="icon-box"></span>
                                                            <i aria-hidden="true" class="fas fa-user-tag"></i>
                                                        </div>

                                                        <div class="icon-box-content">
                                                            <h4 class="icon-box-title">
                                                                <span class="timetourpackages-popup">Tag Peoples</span>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-3 w-md-20">
                                                    <div class="widget_area widget-extra-a">
                                                        <div class="t-box-icon">
                                                            <span calss="icon-box"></span>
                                                            <i aria-hidden="true" class="fas fa-plane"></i>
                                                        </div>

                                                        <div class="icon-box-content">
                                                            <h4 class="icon-box-title">
                                                                <span class="MytimelineT-popup">My Tour Packages</span>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="button-link btn-myTPost">
                                                    <a class="submitShortPost" href="#"><span class="button-text">Post</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="photos_feed_section">
                                <div class="post_photos_header_desk">
                                    <span class="for-v">
                                        <i class="fas fa-table"></i>
                                        <span>PHOTOS</span>
                                    </span>
                                </div>
                                <div class="post_feed_header_desk">
                                  <span class="for-v">
                                    <i class="fas fa-clock"></i>
                                    <span>FEED</span>
                                  </span>  
                                </div>
                            </div>
                        </div>
                        </div>
                        <!-- End Post Section By Nazmul -->
                    </div>

                <?php }

                elseif(!empty ($query_to_check_block) ){?>

                    <div class="loggedin_post" style="padding: 16%;text-align: center;">
                            <h1> You can not create post on this page!</h1>
                        </div>
                         <style>
                            .post_photos_header_desk,.post_feed_header_desk{
                                border:1px solid #000;
                                border-left:0px;
                                border-bottom:0px;
                            }
                        </style>
                        <div class="photos_feed_section">
                            <div class="post_photos_header_desk">
                                <span class="for-v">
                                    <i class="fas fa-table"></i>
                                    <span>PHOTOS</span>
                                </span>
                            </div>
                            <div class="post_feed_header_desk">
                              <span class="for-v">
                                <i class="fas fa-clock"></i>
                                <span>FEED</span>
                              </span>  
                            </div>
                        </div>

                    
                    <?php }else{ ?>
                        <div class="loggedin_post" style="padding: 16%;text-align: center;">
                            <h1>Please log in to create post on this page</h1>
                        </div>
                        <style>
                            .post_photos_header_desk,.post_feed_header_desk{
                                border:1px solid #000;
                                border-left:0px;
                                border-bottom:0px;
                            }
                        </style>
                        <div class="photos_feed_section">
                            <div class="post_photos_header_desk">
                                <span class="for-v">
                                    <i class="fas fa-table"></i>
                                    <span>PHOTOS</span>
                                </span>
                            </div>
                            <div class="post_feed_header_desk">
                              <span class="for-v">
                                <i class="fas fa-clock"></i>
                                <span>FEED</span>
                              </span>  
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </section>
            <!--<div class="row_buttons">
                <div class="row" style="border-bottom: 1px solid #000;    margin: 0px;">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-10">
                        <div class="post_area" style="background: #f6f6f6;">
                            <div class="row">
                                <div class="col-md-3 w-md-20">
                                    <div class="add-photo-video widget_area" class="widget_area">

                                        <div class="t-box-icon">
                                            <span calss="icon-box"></span>
                                            <i class="fas fa-images"></i>
                                        </div>

                                        <div class="icon-box-content">
                                            <h4 class="icon-box-title">
                                                <span>Photo/Video</span>
                                            </h4>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-3 w-md-20">
                                    <div class="widget_area">
                                        <div class="t-box-icon">
                                            <span calss="icon-box"></span>
                                            <i aria-hidden="true" class="far fa-smile"></i>
                                        </div>

                                        <div class="icon-box-content">
                                            <h4 class="icon-box-title">
                                                <span class="timefeeling-popup">Feeling/Activity</span>
                                            </h4>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-3 w-md-20">
                                    <div class="widget_area">
                                        <div class="t-box-icon">
                                            <span calss="icon-box"></span>
                                            <i aria-hidden="true" class="fas fa-user-tag"></i>
                                        </div>

                                        <div class="icon-box-content">
                                            <h4 class="icon-box-title">
                                                <span class="timetourpackages-popup">Tag Peoples</span>
                                            </h4>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-3 w-md-20">
                                    <div class="widget_area widget-extra-a">
                                        <div class="t-box-icon">
                                            <span calss="icon-box"></span>
                                            <i aria-hidden="true" class="fas fa-plane"></i>
                                        </div>

                                        <div class="icon-box-content">
                                            <h4 class="icon-box-title">
                                                <span class="MytimelineT-popup">My Tour Packages</span>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="button-link btn-myTPost">
                                    <a class="submitShortPost" href="#"><span class="button-text">Post</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>-->
            <!-- end of Short post section -->

            <?php if (is_user_logged_in()) {

            ?>
                <div class="row post-ph-ac-tour-tag timeline_for_desktop">
                    <div class="col-md-12 post-display">
                        <div class="col-md-3 icon-design">
                            <i aria-hidden="true" class="fas fa-images"></i>
                            <h6><span>Photo/Video</span></h6>
                        </div>
                        <div class="col-md-3 icon-design">
                            <i aria-hidden="true" class="far fa-smile"></i>
                            <h6><span>Feeling/Activity</span></h6>
                        </div>
                        <div class="col-md-3 icon-design">
                            <i aria-hidden="true" class="fas fa-user-tag"></i>
                            <h6><span>Tag Friends</span></h6>
                        </div>
                        <div class="col-md-3 icon-design">
                            <i aria-hidden="true" class="fas fa-plane"></i>
                            <h6><span>My Tour Packages</span></h6>
                        </div>
                    </div>
                    <div class="elementor-element elementor-element-b0e149c elementor-column elementor-col-20 elementor-top-column collasticonlist" data-id="b0e149c" data-element_type="column">
                        <div class="elementor-element-populatedsec">
                            <div class="elementor-widget-wrap">
                                <div class="elementor-element elementor-element-058a721 elementor-align-right elementor-widget elementor-widget-button" data-id="058a721" data-element_type="widget" data-widget_type="button.default">
                                    <div class="elementor-widget-container">
                                        <div class="elementor-button-wrapper">
                                            <a href="#" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                                <span class="elementor-button-content-wrapper">
                                                    <span class="elementor-button-text">Post</span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php  } ?>

            <section class="elementor-element elementor-element-6fc585d elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section section5wrap" data-id="6fc585d" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row elementor-rowsec">
                        <div class="elementor-element elementor-element-7aa27be elementor-column elementor-col-50 elementor-top-column" data-id="7aa27be" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                            <div class="elementor-element-populatedsec">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-29f5bb4 elementor-widget elementor-widget-text-editor" data-id="29f5bb4" data-element_type="widget" data-widget_type="text-editor.default">
                                        <div class="elementor-widget-container">
                                            <div class="for_pad elementor-text-editor elementor-clearfix">
                                                <a href="https://www.travpart.com/English/my-photo-videos/?user=<?php echo esc_html($username); ?>"><strong style="font-size: 12px;float: left;font-family: 'Roboto', sans-serif;">Photo &amp; Videos</strong></a>
                                                <?php if (is_user_logged_in() and get_current_user_id() == $user_id) { ?>
                                                    <strong style="font-size: 12px;float: right;font-family: 'Roboto', sans-serif;">
                                                        <a class="add-photo-video" only="only" href="#">Add Photo</a>
                                                    </strong>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>


                                    <?php if ($posts > 0) { ?>
                                        <div class="photos_and_videos">


                                            <?php foreach ($user_photos as $p) { ?>
                                                <div class="col_photo">
                                                    <?php if (stripos($p->post_mime_type, 'image') !== false) { ?>
                                                        <a href="<?php echo $p->guid; ?>" class=" mytime_photo_popup">
                                                            <img class="img-responsive" src="<?php echo $p->guid; ?>"></a>
                                                    <?php } else {
                                                        $video_thumbnail_url = get_post_meta($p->ID, 'kgvid-poster-url', true) ?>
                                                        <div class="video_container">
                                                            <video poster="<?php echo $video_thumbnail_url ?>" src="<?php echo $p->guid; ?>"></video>
                                                            <!--<a href="<?php echo $p->guid; ?>" class="mytime_video_popup"><i class="far fa-play-circle"></i></a>-->
                                                            <div class="photos_video_overlay">
                                                                <div class="photos_vd_ovlay_btn"><a href="<?php echo $p->guid; ?>" class="mytime_video_popup"><i class="fas fa-play"></i></a><i class="fas fa-volume-up"></i></div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>

                                        </div>

                                        <?php if (count($user_photos) == 0) { ?>
                                            <div class="photos_and_videos">
                                                <div class="col_photo">
                                                    <img src="https://www.travpart.com/English/wp-content/uploads/2019/12/54a5d11f-ba72-4bd3-b749-9a167fa2f880.jpg" class="img-responsive" />
                                                </div>
                                                <div class="col_photo">
                                                    <img src="https://www.travpart.com/English/wp-content/uploads/2019/12/54a5d11f-ba72-4bd3-b749-9a167fa2f880.jpg" class="img-responsive" />
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>

                                        <div class="photos_and_videos">
                                            <div class="col_photo">
                                                <img src="https://www.travpart.com/English/wp-content/uploads/2019/12/54a5d11f-ba72-4bd3-b749-9a167fa2f880.jpg" class="img-responsive" />
                                            </div>
                                            <div class="col_photo">
                                                <img src="https://www.travpart.com/English/wp-content/uploads/2019/12/54a5d11f-ba72-4bd3-b749-9a167fa2f880.jpg" class="img-responsive" />
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>

                                    <div class="elementor-text-editor elementor-clearfix space_top">
                                        <!--<div class="tra_expr">

                                            <div class="row" style="margin: 0px">
                                                <div class="col-md-3 remove_pad text-center">
                                                    <i class="fas fa-globe-europe"></i>
                                                </div>
                                                <div class="col-md-9 remove_pad">
                                                    <strong>
                                                        <a href="<?php echo add_query_arg(array('user' => $user->user_login), home_url('my-tour-experience/')); ?>" target="_blank" rel="noopener">Travel Experience</a>
                                                    </strong>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="in_dest">
                                            <div class="row" style="margin: 0px">
                                                <div class="col-md-3 remove_pad text-center">
                                                    <i class="far fa-thumbs-up"></i> &nbsp; <i class="fas fa-map-marker-alt"></i>
                                                </div>
                                                <div class="col-md-9 remove_pad">
                                                    <strong class="interested-txt">
                                                        <a href="<?php echo add_query_arg(array('user' => $user->user_login), home_url('my-interested-destination/')); ?>" target="_blank" rel="noopener">Interested Destination</a>
                                                    </strong>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="follow-buttom">
                                            <div class="row" style="margin: 0px">
                                                <div class="col-md-3 remove_pad text-center">
                                                    <i class="fas fa-user-plus"></i>
                                                </div>
                                                <div class="col-md-9 remove_pad">
                                                    <a href="javascript:" target="_blank" rel="noopener"><strong>Following</strong></a>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="feed-star-display">
                                            <div class="row" style="margin: 0px">
                                                <div class="col-md-3 remove_pad text-center">
                                                    <i class="far fa-comment-dots"></i>
                                                </div>
                                                <div class="col-md-9 remove_pad">
                                                    <a href="javascript:" target="_blank" rel="noopener"><strong class="feedback-txt">Feedback</strong></a>
                                                </div>
                                            </div>

                                        </div>-->
                                        <div class="small_green_big"></div>
                                        <div class="photos_grid">
                                            <div class="photos_grid_row">
                                                <?php if ($role = 'Buyer') { ?>
                                                    <div class="photos_grid_coloum bg_image_grid_2">
                                                        <div class="vertical_content_trpkg">
                                                            <a href="<?php echo add_query_arg(array('user' => $user->user_login), home_url('my-tour-experience/')); ?>">Tour Packages</a>
                                                        </div>
                                                    </div>
                                                    <!--<div class="photos_grid_coloum bg_image_grid_3">
                                <div class="vertical_content_indes">
                                    <a href="<?php //echo add_query_arg(array('user' => $user->user_login), home_url('my-interested-destination/')); 
                                                ?>">Interested Destination</a>
                                </div>
                            </div>-->
                                                    <div class="photos_grid_coloum bg_image_grid_3">
                                                        <div class="vertical_content_indes">
                                                            <a href="#"><span class="badge">25</span> Visited Destinations</a>
                                                        </div>
                                                    </div>

                                                <?php } ?>
                                            </div>

                                            <div class="photos_grid_row">
                                                <div class="photos_grid_coloum bg_image_grid_4 ">
                                                    <div class="vertical_content_follow">
                                                        <a href="https://www.travpart.com/English/following?user=<?php echo $username ?>">Following</a>
                                                    </div>
                                                </div>
                                                <div class="photos_grid_coloum bg_image_grid_5">
                                                    <div class="vertical_content_tvl_feed">
                                                        <a href="#">Travel Feedbacks</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="small_green_big"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-36e06d8 timelinecontentwrap elementor-column elementor-col-50 elementor-top-column" data-id="36e06d8" data-element_type="column">
                            <div class="elementor-element-populatedsec">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-1be8f11 elementor-widget elementor-widget-html" data-id="1be8f11" data-element_type="widget" data-widget_type="html.default">
                                        <div class="elementor-widget-container">




                                            <div class="post-container">
                                                <?php
                                                if(count($posts)>0){
                                                    
                                                foreach ($posts as $value) {
                                                    $feeling = get_post_meta($value->ID, 'feeling', true);
                                                    $tag_user_id = get_post_meta($value->ID, 'tag_user_id', true);
                                                    $tag_location = get_post_meta($value->ID, 'location', true);

                                                    $current_user_id = get_current_user_id();
                                                    $liked = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE user_id = '{$current_user_id}' AND feed_id = '$value->ID'");
                                                    $like_count = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE feed_id = '$value->ID'");

                                                    $attachment = get_post_thumbnail_id($value->ID);
                                                    $attachment_url = '';
                                                    if (empty($attachment)) {
                                                        $attachment = get_post_meta($value->ID, 'attachment', true);
                                                    }
                                                    if (!empty($attachment)) {
                                                        if (wp_attachment_is('image', $attachment)) {
                                                            $attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
                                                        } else {
                                                            $attachment_url = wp_get_attachment_url($attachment);
                                                        }
                                                    } else if (!empty(get_post_meta($value->ID, 'tour_img', true))) {
                                                        $attachment_url = get_post_meta($value->ID, 'tour_img', true);
                                                    }
                                                    $tour_post_id = get_post_meta($value->ID, 'tour_post', true);

                                                    $lookingfriend = get_post_meta($value->ID, 'lookingfriend', true);
                                                    switch ($lookingfriend) {
                                                        case 1:
                                                            $lookingfriend_value = 'Looking For A Travel Friend';
                                                            break;
                                                        case 2:
                                                            $lookingfriend_value = 'Want to Hangout';
                                                            break;
                                                        case 3:
                                                            $lookingfriend_value = 'Going for Party';
                                                            break;
                                                        case 4:
                                                            $lookingfriend_value = 'Going for Sport';
                                                            break;
                                                        default:
                                                        $lookingfriend_value = '';
                                                    }

                                                ?>

                                                    <?php
                                                   // $display_name = um_user('display_name');
                                                    $display_name = um_get_display_name($user_id);
                                                    ?>
                                                    <div class="post_for_mobile">
                                                        <div class="mobile_post_container">
                                                            <div class="post_header">
                                                                <div class="post_profile_img">
                                                                    <div class="for_img">
                                                                        <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo esc_html($username); ?>"><?php echo get_avatar($user_id, 52);  ?></a>
                                                                    </div>
                                                                </div>
                                                                <div class="header_user_content">
                                                                    <div class="content_mobile">
                                                                        <h4 class="mobile_agent_name">
                                                                            <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo esc_html($username); ?>"><?php echo $display_name; ?>
                                                                            </a>
                                                                            <span class="date_pub_mobile">
                                                                                <?php
                                                                                $orig_time = strtotime($value->post_date);
                                                                                ?>
                                                                                <p style="margin: 0px; font-size: 10px">
                                                                                    <?php echo human_time_diff($orig_time, current_time('timestamp')) . ' ' . __('ago'); ?>
                                                                                </p>
                                                                            </span>
                                                                            <?php if ($lookingfriend >= 1) { ?>
                                                                                <a href="#" class="button_green changes_for_mob"><?php echo $lookingfriend_value; ?></a>
                                                                            <?php } ?>
                                                                        </h4>
                                                                        <p class="mobiel_feeling">
                                                                            <?php
                                                                            if (!empty($feeling))
                                                                                echo " is feeling " . base64_decode($feeling);

                                                                            if (!empty($tag_user_id)) {
                                                                                $tag_friend = get_user_by('id', $tag_user_id);
                                                                                echo " is with <a href=https://www.travpart.com/English/my-time-line/?user=$tag_friend->user_login>" . um_get_display_name($tag_user_id) . "</a>";
                                                                            }
                                                                            ?>
                                                                        </p>
                                                                    </div>

                                                                    <div class="right-section-dot dropdown-toggle" data-toggle="dropdown"><a href="#" class="Pdeltebtn"> <i id="postid" class="fas fa-ellipsis-h"></i></a>
                                                                    </div>

                                                                    <?php
                                                                    $post_edit_text = preg_replace('/[^A-Za-z0-9 ]/', '', $value->post_content);
                                                                    ?>
                                                                    <!-- Post Delete Option for mobile-->
                                                                    <div id="PostDId" class="PostDelete dropdown-menu" role="menu" aria-labelledby="menu1">
                                                                        <?php if (is_user_logged_in()) {
                                                                            if ($user_id == get_current_user_id()) { ?>
                                                                                <li class="drp-edit edit-post"
                                                                                    postid = "<?php echo $value->ID ?>"
                                                                                    feeling="<?php echo base64_decode(get_post_meta($value->ID, 'feeling', true)) ?>"
                                                                                    content="<?php echo $value->post_content ?>"
                                                                                    taguserid="<?php echo get_post_meta($value->ID, 'tag_user_id', true) ?>"
                                                                                    taguser="<?php echo um_get_display_name(get_post_meta($value->ID, 'tag_user_id', true)) ?>"
                                                                                    tour="<?php echo get_post_meta($value->ID, 'tour_id', true) ?>"
                                                                                >
                                                                                    <a href="#">Edit Post</a>
                                                                                </li>
                                                                                <li class="drp-delete" onclick="getid('<?php echo $value->ID; ?>')"><a href="#">Delete</a></li>
                                                                        <?php
                                                                            }
                                                                        } ?>
                                                                        <li><a href="#">Turn On notifications For this post</a></li>
                                                                        <li class="ShowITc"><a href="#">Show in tab</a></li>
                                                                        <li><a href="#">Hide From timeline</a></li>
                                                                        <a style=" display: none" href="<?php echo get_permalink($value->ID) ?>">Copy link to this post</a>
                                                                        <li><a href="#" onclick="copy_link('<?php echo $value->ID; ?>')">Copy link to this post</a></li>

                                                                    </div>

                                                                    <!--End Post Delete Option -->


                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            
                                                            <div class="mobile_post_content">
                                                                <div class="mobile_post_image">
                                                                    <?php if (!empty($attachment_url)) { ?>
                                                                    <div class="post_image">
                                                                        <?php if (!is_array(get_post_meta($value->ID, 'attachment'))) { ?>
                                                                            <?php if (empty($attachment) || wp_attachment_is('image', $attachment)) { ?>
                                                                                <a href="<?php echo !empty($tour_post_id) ? get_permalink($tour_post_url) : '#'; ?>">
                                                                                    <img src="<?php echo $attachment_url; ?>" style="width:650px;max-height:350px">
                                                                                </a>
                                                                            <?php } else {
                                                                                $video_thumbnail_url = get_post_meta($attachment, 'kgvid-poster-url', true) ?>
                                                                                <video poster="<?php echo $video_thumbnail_url ?>"
                                                                                    src="<?php echo $attachment_url; ?>" controls="controls"></video>
                                                                            <?php } ?>
                                                                        <?php } else {
                                                                            foreach (get_post_meta($value->ID, 'attachment') as $row) {
                                                                                $attachment_url = wp_get_attachment_url($row);
                                                                                ?>
                                                                                <?php if (wp_attachment_is('image', $row)) { ?>
                                                                                    <!--<a href="<?php echo !empty($tour_post_id) ? get_permalink($tour_post_url) : '#'; ?>">
                                                                                        <img src="<?php echo $attachment_url; ?>" style="width:650px;max-height:350px">
                                                                                    </a>-->
                                                                                     <div class="mytime_image_div test_d">
                                                                                       
                                                                                        <a href="<?php echo $attachment_url; ?>" data-fancybox="gallery"> <img src="<?php echo $attachment_url; ?>"></a>
                                                                                     </div>
                                                                                <?php } else {
                                                                                    $video_thumbnail_url = get_post_meta($attachment, 'kgvid-poster-url', true) ?>
                                                                                    <video poster="<?php echo $video_thumbnail_url ?>"
                                                                                         src="<?php echo $attachment_url; ?>" controls="controls"></video>
                                                                                <?php }
                                                                            }
                                                                        } ?>
                                                                    </div>
                                                                    <?php } ?>
                                                                </div>

                                                                <div class="mobile_post_content ">
                                                                    <div class="location_div"><?php echo esc_html($tag_location); ?></div>
                                                                    <p class="more"><?php echo $value->post_content; ?></p>
                                                                </div>
                                                            </div>
                                                            <hr class="style-two" />
                                                            <div class="buttons_for_mobilepost">
                                                                <div class="main_buttons_div">
                                                                    <div class="like_mobile">
                                                                         <?php if (is_user_logged_in() AND empty($query_to_check_block)) { ?>
                                                                <a href="#" feedid="<?php echo $value->ID; ?>">
                                                                    <i class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>
                                                                    <span><?php echo $like_count; ?></span>
                                                                </a>
                                                            <?php }else{ ?>
                                                            
                                                                    <i class="far fa-thumbs-up"></i>
                                                                    <span><?php echo $like_count; ?></span>
                                                                
                                                            <?php } ?>
                                                                    </div>
                                                                    <div class="comment_mobile">
                                                                        <a href="#">
                                                                            <i class="far fa-comment-alt"></i>
                                                                            Comment
                                                                        </a>
                                                                    </div>
                                                                    <div class="share_mobile share_mobile<?php echo $value->ID; ?>">

                                                                        <a href="#">
                                                                            <i class="fas fa-share"></i> Share
                                                                        </a>



                                                                        <!--My DropDown Mobile-->

                                                                        <div class="share_mob_dropdown share_mob_dropdown<?php echo $value->ID; ?>">
                                                                            <?php if(is_user_logged_in() AND empty($query_to_check_block)) {?>
                                                                    <li onclick="share_feed(<?php echo $value->ID; ?>)">Share Now</li>
                                                                <?php }?>

                


                                                                    <li><a target="_blank" onclick="record_share_data('<?php echo $value->ID; ?>','whatsapp')" href="https://web.whatsapp.com/send?text=<?php echo substr($value->post_content, 0, 80) . "  ( see-more ) "; echo get_permalink($value->ID)?>"><div class="wh_mytime_icon_div"><i class="fab fa-whatsapp share_wh_mytime_i"></i></div>Share to whats app</a></li>
                                                                    <li><a  href="#" onclick="copy_link('<?php echo $value->ID; ?>')"><i class="far fa-copy share_copy_link_mytime_i"></i>Copy link to this post</a></li>

                                                                  <!--  <li class="mores"><a class="a2a_dd" href="#"><i class="fas fa-plus-square"></i> More...</a></li>
                                                                  -->
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="<?php echo get_permalink($value->ID) ?>" data-a2a-title="<?php echo substr($value->post_content, 0, 80) ?>">
                    <a class="a2a_button_facebook" onclick="record_share_data('<?php echo $value->ID; ?>','fb')"></a>
                    <a class="a2a_button_twitter" onclick="record_share_data('<?php echo $value->ID; ?>','twitter')"></a>
                    <a class="a2a_dd" href="https://www.addtoany.com/share" onclick="record_share_data('<?php echo $value->ID; ?>','other')"></a>
                    </div>

                                                                        </div>
                                                                        <script type="text/javascript">
                                                                            $(document).ready(function () {
                                                                                $('.sharemobile<?php echo $value->ID; ?>').on('click',function () {
                                                                                    $('.share_mob_dropdown<?php echo $value->ID; ?>').toggle();
                                                                                });
                                                                            });

                                                                            
                                                                        </script>

                                                                        <!--End My DropDown-->



                                                                    </div>
                                                                </div>
                                                                <a name="shortpost<?php echo $value->ID; ?>"></a>
                                                                
                                                                <div class="div_commets user_comm_d_<?php echo $value->ID; ?>">
                                                                
                                                                <?php
                                                                $load_comments = $wpdb->get_results("SELECT comment,user_id  FROM `user_feed_comments` WHERE `feed_id` = '{$value->ID}'  ORDER BY id DESC");
                                                                if ($load_comments) {
                                                                    echo "<hr>";
                                                                      
                                                                    echo "<h3> User Comments</h3>";
                                                                    echo '<div id="comments_show'.$value->ID.'">';
                                                                    foreach ($load_comments as $comment) {

                                                                    $author_obj = get_user_by('id', $comment->user_id);
                                                                ?>

                                                                        <div class="user-comment-area user-2">
                                                                            <?php echo get_avatar($comment->user_id, 50);  ?>
                                                                            <div class="another-user-section">
                                                                                <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $author_obj->user_login?>" target="_blank" style="font-size: 10px!important"><?php echo um_get_display_name($comment->user_id); ?></a><span><?php echo $comment->comment;  ?></span>
                                                                            </div>
                                                                        </div>

                                                                <?php
                                                                 }
                                                                 echo '</div>';
                                                                }else{
                                                                    echo '<div id="comment_show'.$value->ID.'">';
                                                                    echo '</div>';
                                                                } ?>
                                                                </div>
                                                            </div>
                                                            <?php if (is_user_logged_in() AND empty($query_to_check_block)) {  ?>
                                                                <div class="user-comment-area submit-comment">
                                                                    <?php echo get_avatar(wp_get_current_user()->ID, 50);  ?>
                                                                    <input style="width: 75%" placeholder="Write a Comment" type="text" name="feed_comment" required="">

                                                                    <input type="hidden" name="feed_id" value="<?php echo $value->ID;  ?>">
                                                                    <input type="hidden" name="loginuser_image" value="<?php echo get_avatar_url (wp_get_current_user()->ID)?>">
                                                                    <input type="hidden" name="loginuser_name" value="<?php echo um_get_display_name (wp_get_current_user()->ID )?>">
                                                                    
                                                                    <div class="sent-btn-comment">
                                                                        
                                                                    </div>


                                                                </div>
                                                            <?php } ?>
                                                        </div>


                                                    </div>



                                                    <div class="post-main-section hide_in_mobile">
                                                        <div class="post-profile-area">
                                                            <?php echo get_avatar($user_id, 40);  ?>

                                                            <h5><a href="#"><?php echo $display_name; ?></a>
                                                                <?php

                                                                if (!empty($feeling))
                                                                    echo " is feeling " . base64_decode($feeling);

                                                                if (!empty($tag_user_id)) {
                                                                    $tag_friend = get_user_by('id', $tag_user_id);
                                                                    echo " is with <a href=https://www.travpart.com/English/my-time-line/?user=$tag_friend->user_login>" . um_get_display_name($tag_user_id) . "</a>";
                                                                }

                                                                ?>
                                                            </h5>
                                                            <?php
                                                            // time conversion
                                                            $orig_time = strtotime($value->post_date);
                                                            ?>
                                                            <div><span class="post_area_time_span">
                                                                <?php echo
                                                                    human_time_diff($orig_time, current_time('timestamp')) . ' ' . __('ago') . ", "; ?></span><br>
                                                                    <span class="post_area_loc_span"><?php echo esc_html($tag_location); ?></span>
                                                            </div>
                                                        </div>

                                                        <!--<p class="button_green changes_for_mob">Created an EventLooking For A Travel Friend</p>-->
                                                        <?php if ($lookingfriend >= 1) { ?>
                                                            <a href="#" class="button_green changes_for_mob"><?php echo $lookingfriend_value; ?></a>
                                                        <?php } ?>
                                                        <div class="right-section-dot dropdown-toggle" data-toggle="dropdown"><a href="#" class="Pdeltebtn"> <i id="postid" class="fas fa-ellipsis-h"></i></a>
                                                        </div>


                                                        <?php
                                                        $post_edit_text = preg_replace('/[^A-Za-z0-9 ]/', '', $value->post_content);
                                                        ?>
                                                        <!-- Post Delete Option PC-->
                                                        <div id="PostDId" class="PostDelete dropdown-menu" role="menu" aria-labelledby="menu1">
                                                            <?php if (is_user_logged_in()) {
                                                                if ($user_id == get_current_user_id()) { ?>
                                                                    <li class="drp-edit edit-post"
                                                                        postid = "<?php echo $value->ID ?>"
                                                                        feeling="<?php echo base64_decode(get_post_meta($value->ID, 'feeling', true)) ?>"
                                                                        content="<?php echo $value->post_content ?>"
                                                                        taguserid="<?php echo get_post_meta($value->ID, 'tag_user_id', true) ?>"
                                                                        taguser="<?php echo um_get_display_name(get_post_meta($value->ID, 'tag_user_id', true)) ?>"
                                                                        tour="<?php echo get_post_meta($value->ID, 'tour_id', true) ?>"
                                                                    >
                                                                        <a href="#">Edit Post</a>
                                                                    </li>
                                                                    <li class="drp-delete"><a onclick="getid('<?php echo $value->ID; ?>')" href="#">Delete</a></li>
                                                            <?php
                                                                }
                                                            } ?>
                                                            <li><a href="#">Turn On notifications For this post</a></li>
                                                            <li class="ShowITc"><a href="#">Show in tab</a></li>
                                                            <li><a href="#">Hide From timeline</a></li>
                                                            <a style="display: none" id="copytext<?php echo $value->ID; ?>" href="<?php echo get_permalink($value->ID) ?>"></a>
                                                            <li><a  href="#" onclick="copy_link('<?php echo $value->ID; ?>')">Copy link to this post</a></li>

                                                        </div>

                                                        <!--End Post Delete Option -->



                                                        <!--Start Adding Edit popup -->


                                                        <!--End Adding Edit popup -->

                                                        <div class="profile-content more">
                                                            <?php echo $value->post_content; ?>
                                                        </div>
                                                        <?php
                                                        if (!empty($attachment_url)) {
                                                        ?>
                                                            <h4>
                                                            
                                                                <div class="post_image">
                                                                    <?php if (!is_array(get_post_meta($value->ID, 'attachment'))) { ?>
                                                                        <?php if (empty($attachment) || wp_attachment_is('image', $attachment)) { ?>
                                                                            <a href="<?php echo !empty($tour_post_id) ? get_permalink($tour_post_url) : '#'; ?>">
                                                                                <img src="<?php echo $attachment_url; ?>">
                                                                            </a>
                                                                        <?php } else {
                                                                            $video_thumbnail_url = get_post_meta($attachment, 'kgvid-poster-url', true) ?>
                                                                            <video poster="<?php echo $video_thumbnail_url ?>"
                                                                                src="<?php echo $attachment_url; ?>" controls="controls"></video>
                                                                        <?php } ?>
                                                                    <?php } else {
                                                                        foreach (get_post_meta($value->ID, 'attachment') as $row) {
                                                                            $attachment_url = wp_get_attachment_url($row);
                                                                            ?>
                                                                            <?php if (wp_attachment_is('image', $row)) { ?>
                                                                                 
                                                                                 
                                                                                    <?php //if($_GET['debug']==1){ ?>
                                                                                      <a href="<?php echo $attachment_url; ?>" data-fancybox="gallery"> 
                                                                                        <div class="time_image" style="background:url('<?php echo $attachment_url; ?>');background-size: cover;background-position:center center;width: 100%;height:350px;margin-top:10px;">
                                                                                        
                                                                                        </div>
                                                                                      </a>  
                                                                                    <?php// } ?>    
                                                                                
                                                                                 <!--<div class="mytime_image_div" style="<?php //echo(($_GET['debug']==1)?'display: none;':''); ?>">
                                                                                    
                                                                                    <a href="<?php// echo $attachment_url; ?>" data-fancybox="gallery"><img src="<?php //echo $attachment_url; ?>"></a>
                                                                                 </div>-->
                                                                            <?php } else {
                                                                                $video_thumbnail_url = get_post_meta($row, 'kgvid-poster-url', true) ?>
                                                                                <video poster="<?php echo $video_thumbnail_url ?>"
                                                                                    src="<?php echo $attachment_url; ?>" controls="controls"></video>
                                                                            <?php } ?>
                                                                        <?php
                                                                        }
                                                                    } ?>
                                                                </div>
                                                            </h4>
                                                        <?php } ?>
                                                        <div class="post-share-area">
                                                            <div class="like-area">
                                                                <?php if (is_user_logged_in() AND empty ($query_to_check_block) ) { ?>
                                                                <a href="#" feedid="<?php echo $value->ID; ?>">
                                                                    <i class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>
                                                                    <span><?php echo $like_count; ?></span>
                                                                </a>
                                                            <?php }else{ ?>
                                                            
                                                                    <i class="far fa-thumbs-up"></i>
                                                                    <span><?php echo $like_count; ?></span>
                                                                
                                                            <?php } ?>
                                                            </div>

                                                            <div class="comment-area" data-comment="<?php echo $value->ID; ?>">
                                                                <a href="#">
                                                                    <i class="far fa-comment-alt"></i>
                                                                    Comment
                                                                </a>
                                                            </div>


                                                            <div class="share-area">
                                                                <a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
                                                                    <i class="fas fa-share"></i> Share
                                                                </a>

                                                                <!--Share My DropDown PC-->
                                                                <div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2" aria-labelledby="menu2">
                                                                    <?php if(is_user_logged_in() AND empty($query_to_check_block)) {?>
                                                                    <li onclick="share_feed(<?php echo $value->ID; ?>)">Share Now</li>
                                                                <?php }?>

                


                                                                    <li><a target="_blank" onclick="record_share_data('<?php echo $value->ID; ?>','whatsapp')" href="https://web.whatsapp.com/send?text=<?php echo substr($value->post_content, 0, 80) . "  ( see-more ) "; echo get_permalink($value->ID)?>"><div class="wh_mytime_icon_div"><i class="fab fa-whatsapp share_wh_mytime_i"></i></div>Share to whats app</a></li>
                                                                    <li><a  href="#" onclick="copy_link('<?php echo $value->ID; ?>')"><i class="far fa-copy share_copy_link_mytime_i"></i>Copy link to this post</a></li>

                                                                  <!--  <li class="mores"><a class="a2a_dd" href="#"><i class="fas fa-plus-square"></i> More...</a></li>
                                                                  -->
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="<?php echo get_permalink($value->ID) ?>" data-a2a-title="<?php echo substr($value->post_content, 0, 80) ?>">
                    <a class="a2a_button_facebook" onclick="record_share_data('<?php echo $value->ID; ?>','fb')"></a>
                    <a class="a2a_button_twitter" onclick="record_share_data('<?php echo $value->ID; ?>','twitter')"></a>
                    <a class="a2a_dd" href="https://www.addtoany.com/share" onclick="record_share_data('<?php echo $value->ID; ?>','other')"></a>
                    </div>

    </div>

                                                                <!--End My DropDown-->

                                                            </div>
                                                            <div class="div_commets user_comm_d_<?php echo $value->ID; ?>">
                                                            <?php
                                                          //  $load_comments = $wpdb->get_results("SELECT comment,user_id  FROM `user_feed_comments` WHERE `feed_id` = '{$value->ID}'  ORDER BY id DESC");
                                                            if ($load_comments) {
                                                                echo "<hr>";
                                                                echo "<h3> User Comments</h3>";
                                                                echo '<div id="comment_show'.$value->ID.'">';
                                                                foreach ($load_comments as $comment) {

                                                                    $author_obj = get_user_by('id', $comment->user_id);
                                                            ?>

                                                                    <div class="user-comment-area user-2">
                                                                        <?php echo get_avatar($comment->user_id, 50);  ?>
                                                                        <div class="another-user-section">
                                                                            <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $author_obj->user_login ?>" target="_blank" ><?php echo um_get_display_name($comment->user_id); ?></a><span><?php echo $comment->comment;  ?></span>
                                                                        </div>
                                                                    </div>

                                                            <?php 
                                                        }
                                                        echo '</div>';
                                                            } else{
                                                                 echo '<div id="comment_show'.$value->ID.'">';
                                                                  echo'</div>';
                                                            }?>
                                                            
                                                            <?php if (is_user_logged_in() AND empty($query_to_check_block)) {  ?>
                                                                <div class="user-comment-area submit-comment">
                                                                    <?php echo get_avatar(wp_get_current_user()->ID, 50);  ?>
                                                                    <input placeholder="Write a Comment" type="text" name="feed_comment" required="">
                                                                    <input type="hidden" name="feed_id" value="<?php echo $value->ID;  ?>">
                                                                    <input type="hidden" name="loginuser_image" value="<?php echo get_avatar_url (wp_get_current_user()->ID)?>">
                                                                    <input type="hidden" name="loginuser_name" value="<?php echo um_get_display_name (wp_get_current_user()->ID )?>">
                                                                </div>
                                                            <?php } ?>
                                                            </div>
                                                            <hr>
                                                        </div>
                                                    </div>
                                                <?php  }
                                                }else{ ?>
                                                    <div class="not_posted">
                                                        <h1>
                                                            This user unfortunately has <br />
                                                            not posted anything yet
                                                        </h1>
                                                    </div>
                                                <?php } ?>

                                            </div>
                                            <div class="mytime_desk_photo_gallery" userid="<?php echo $user->ID; ?>">
                                                
                                         
                                                
                                                <?php 
                                                if(count($user_photos)>0){
                                                foreach ($user_photos as $p) {

                                                $shortpost_id = $wpdb->get_var("SELECT post_id FROM `wp_postmeta` WHERE `meta_key` = 'attachment' AND `meta_value`='{$p->ID}'");
                                                if(!empty($shortpost_id)) {
                                                $shortpost_link = get_permalink($shortpost_id);
                                                }
                                                else {
                                                $shortpost_link = $p->src;
                                                }
                                                 ?>
                                                    <?php if (stripos($p->post_mime_type, 'image') !== false) { ?>
                                                        <div class="mytime_desk_photo">
                                                            <a href="<?php echo $p->guid; ?>" class=" mytime_photo_popup">
                                                            <img class="mytime_ph_desk_popup" src="<?php echo $p->guid; ?>"></a>
                                                        </div>
                                                    <?php } else {
                                                        $video_thumbnail_url = get_post_meta($p->ID, 'kgvid-poster-url', true) ?>
                                                        <div class="mytime_video_desk">
                                                            <video poster="<?php echo $video_thumbnail_url ?>" src="<?php echo $p->guid; ?>"></video>
                                                            <div class="post_video_overlay">
                                                               <div class="post_vd_ovlay_btn">
                                                               <div style="float: left;">
                                                                   <a href="<?php echo $p->guid; ?>" class="mytime_video_popup">
                                                                        <i class="fas fa-play"></i>
                                                                   </a>
                                                               </div>  
                                                                   <!--<i class="fas fa-volume-up"></i>-->
                                                                        
                                                                <div class="share-area" style="float: right;padding: 0px;">
                                                                    <a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
                                                                        <i class="fas fa-share"></i> Share
                                                                    </a>

                                                                    <!--Share My DropDown PC-->
                                                                    <div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2" aria-labelledby="menu2">
                                                    
                                                                        <li onclick="share_feed(<?php echo $p->ID; ?>)">Share Now</li>
                                                                        <li><a target="_blank" href="https://web.whatsapp.com/send?text=<?php echo substr($value->post_content, 0, 80) . "  ( see-more ) "; echo $shortpost_link; ?>"><div class="wh_mytime_icon_div"><i class="fab fa-whatsapp share_wh_mytime_i"></i></div>Share to whats app</a></li>
                                                                        <a  style="display: none" id="copytext<?php echo $p->ID; ?>" href="<?php echo $shortpost_link;  ?>">Copy link to this post</a>
                                                                        <li><a  href="#" onclick="copy_link('<?php echo $p->ID; ?>')"><i class="far fa-copy share_copy_link_mytime_i"></i>Copy link to this post</a></li>
                                                                      <!--  <li class="mores"><a class="a2a_dd" href="#"><i class="fas fa-plus-square"></i> More...</a></li>
                                                                      -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="<?php echo $shortpost_link; ?>" data-a2a-title="<?php echo substr($value->post_content, 0, 80) ; ?>">
    <a class="a2a_button_facebook"></a>
    <a class="a2a_button_twitter"></a>
    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
</div>

<script async src="https://static.addtoany.com/menu/page.js"></script>
                                                                    </div>

                                                                    <!--End My DropDown-->

                                                                </div>
                                                                <div style="clear: both;"> </div>
                                                               </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php }
                                                }else{ ?>
                                                    <div class="not_posted">
                                                        <h1>
                                                            This user unfortunately has <br />
                                                            not posted anything yet
                                                        </h1>
                                                    </div>
                                                <?php } ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--- end of section 5 -->
            <!--section class="elementor-element elementor-element-2c53917 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="2c53917" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row elementor-rowsec">
                        <div class="elementor-element elementor-element-b282f6a elementor-column elementor-col-50 elementor-top-column" data-id="b282f6a" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                            <div class="elementor-element-populatedsec tr_in_fo_fe">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-f3e342d elementor-widget elementor-widget-text-editor" data-id="f3e342d" data-element_type="widget" data-widget_type="text-editor.default">
                                        <div class="elementor-widget-container">
                                            <?php if ($role == "Buyer") { ?>
                                                <div class="elementor-text-editor elementor-clearfix">
                                                    <div class="tra_expr">

                                                        <div class="row" style="margin: 0px">
                                                            <div class="col-md-3 remove_pad text-center">
                                                                <i class="fas fa-globe-europe"></i>
                                                            </div>
                                                            <div class="col-md-9 remove_pad">
                                                                <strong>
                                                                    <a href="<?php echo add_query_arg(array('user' => $user->user_login), home_url('my-tour-experience/')); ?>" target="_blank" rel="noopener">Travel Experience</a>
                                                                </strong>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="in_dest">
                                                        <div class="row" style="margin: 0px">
                                                            <div class="col-md-3 remove_pad text-center">
                                                                <i class="far fa-thumbs-up"></i> &nbsp; <i class="fas fa-map-marker-alt"></i>
                                                            </div>
                                                            <div class="col-md-9 remove_pad">
                                                                <strong class="interested-txt">
                                                                    <a href="<?php echo add_query_arg(array('user' => $user->user_login), home_url('my-interested-destination/')); ?>" target="_blank" rel="noopener">Interested Destination</a>
                                                                </strong>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="follow-buttom">
                                                        <div class="row" style="margin: 0px">
                                                            <div class="col-md-3 remove_pad text-center">
                                                                <i class="fas fa-user-plus"></i>
                                                            </div>
                                                            <div class="col-md-9 remove_pad">
                                                                <a href="javascript:" target="_blank" rel="noopener"><strong>Following</strong></a>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="feed-star-display">
                                                        <div class="row" style="margin: 0px">
                                                            <div class="col-md-3 remove_pad text-center">
                                                                <i class="far fa-comment-dots"></i>
                                                            </div>
                                                            <div class="col-md-9 remove_pad">
                                                                <a href="javascript:" target="_blank" rel="noopener"><strong class="feedback-txt">Feedback</strong></a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-cba43e9 zhide elementor-column elementor-col-50 elementor-top-column" data-id="cba43e9" data-element_type="column">
                            <div class="elementor-column-wrap">
                                <div class="elementor-widget-wrap">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>-->
            <!-- End of section 6 -->

            <section class="elementor-element elementor-element-045d5c8 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="045d5c8" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row elementor-rowsec">
                        <div class="elementor-element elementor-element-d4b9cb2 elementor-column elementor-col-50 elementor-top-column" data-id="d4b9cb2" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                            <div class="elementor-element-populatedsec">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-82756aa elementor-widget elementor-widget-text-editor" data-id="82756aa" data-element_type="widget" data-widget_type="text-editor.default">
                                        <?php if ($role == "Seller") { ?>
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix">
                                                    <p><strong>Tour Packages</strong></p>
                                                </div>
                                            </div>
                                    </div>

                                    <div class="elementor-element elementor-element-d38d230 elementor-widget elementor-widget-html" data-id="d38d230" data-element_type="widget" data-widget_type="html.default">
                                        <div class="elementor-widget-container">
                                            <div class="tltourpackages">
                                                <?php
                                                $tour_list = $wpdb->get_results("SELECT wp_posts.ID,wp_postmeta.meta_value tour_id
                                                                                        FROM `wp_posts`,`wp_postmeta` WHERE wp_posts.post_status='publish' AND wp_posts.post_author={$user->ID}
                                                                                        AND wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id'");

                                                foreach ($tour_list as $tour) {
                                                    $hotelinfo = $wpdb->get_row("SELECT img FROM wp_hotel WHERE tour_id='{$tour->tour_id}' LIMIT 1");
                                                    if (!empty($hotelinfo->img)) {
                                                        if (is_array(unserialize($hotelinfo->img))) {
                                                            foreach (unserialize($hotelinfo->img) as $hotelImgItem)
                                                                $image_url = $hotelImgItem;
                                                        } else
                                                            $image_url = $hotelinfo->img;
                                                    }
                                                    if (empty($image_url)) {
                                                        $image_url = "https://i.travelapi.com/hotels/1000000/910000/903000/902911/26251f24_y.jpg";
                                                    }
                                                ?>
                                                    <a href="https://www.travpart.com/English/?p=<?php echo $tour->ID ?>">
                                                        <img src="<?php echo $image_url;  ?>" style="widows: 85px;height: 85px">
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-7a12a05 zhide elementor-column elementor-col-50 elementor-top-column" data-id="7a12a05" data-element_type="column">
                            <div class="elementor-column-wrap">
                                <div class="elementor-widget-wrap">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- end of section7 -->

        </div>
        <!--mytimelinewrap -->
    </div>
    <!--Main -->
</div>

<div class="event-tab">
    <div class="shedule_event_main_wrapper">
        <div class="container_shedule">
            <div class="shedule_pageheading">
                <h1>Shedule an Event</h1>
                <span class="close_ev"><i class="fas fa-times"></i></span>
            </div>

            <?php wp_nonce_field('event_post_nonce_action', 'eventpostnonce'); ?>
            <div class="content_row">
                <div class="name_text_row">
                    <div class="event_name_shedule">
                        <p>Event Name</p>
                    </div>
                    <div class="event_text_shedule">
                        <input class="form-control" name="event_name" placeholder="Add a short, clear name" />
                    </div>
                </div>

                <div class="invite_connection_div">
                    <div class="invite_connection_title">
                        <p>Invite your connection</p>
                    </div>
                    <select name="event_invited_people" class="event-invited-people" multiple>
                        <?php foreach ($friends as $f) { ?>
                            <option value="<?php echo $f->ID; ?>"><?php echo um_get_display_name($f->ID); ?></option>
                        <?php } ?>
                    </select>

                    <!--p class="for_mututal"><span class="for_mut_color">Mutual friends</span> <a href="#">Balouch</a>,<a href="#">Lix</a></p-->
                </div>

                <div class="event_type">
                    <p>Event Type</p>
                </div>
                <div class="cusomt_private_dropdown">
                    <div class="dropdown">
                        <div class="dropdown-toggle" id="event-type-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <div class="inline_t">
                                <i class="fas fa-lock"></i>
                            </div>
                            <div class="inline_t event-type-chosen">
                                <p><strong>Private</strong></p>
                                <p>The event's members are the only people whom can see the post and other event's members</p>
                            </div>
                            <input name="event_type" type="hidden" />

                        </div>
                        <ul class="dropdown-menu" aria-labelledby="event-type-dropdown">
                            <li>
                                <div class="inline_t">
                                    <i class="fas fa-lock"></i>
                                </div>
                                <div class="inline_t event-type-option" type="private">
                                    <p><strong>Private</strong></p>
                                    <p>The event's members are the only people whom can see the post and other event's members</p>
                                </div>
                            </li>
                            <li>
                                <div class="inline_t">
                                    <i class="fas fa-globe-americas"></i>
                                </div>
                                <div class="inline_t event-type-option" type="public">
                                    <p><strong>Public</strong></p>
                                    <p>Everyone can see the event's member and post</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>


                <div class="locaion_shedule_div">
                    <div class="title_location">
                        <p>Location</p>
                    </div>
                    <div class="width_25 location_profile">
                        <div class="custom_round">
                            <p>location <br /> picture<br /> of video</p>
                            <button class="btn btn-primary event-location-image">Upload</button>
                            <input name="event_location_image_id" type="hidden" />
                        </div>
                        <div class="text-normal text-center">
                            <p>Upload a picture of the <br />location's unique <br />landmark</p>
                        </div>
                    </div>
                    <div class="width_69 location_profile">
                        <div class="input_record">
                            <input id="event-location" name="event_location" type="text" class="cus form-control" placeholder="(Required)" style="width: 60%" />
                            <div class="inputs_data">
                                <div class="text_dates width15">
                                    start
                                </div>
                                <div class="text_dates width40">
                                    <div id="datetimepicker1" class="input-append date">
                                        <div class="input-wrapper">
                                            <label for="stuff" class="far fa-calendar-alt add-on input-icon"></label>
                                            <input name="start_date" class="form-control" data-format="MM/dd/yyyy" type="text"></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="text_dates width40">
                                    <div id="datetimepicker3" class="input-append date">
                                        <div class="input-wrapper">
                                            <label for="stuff" class="fas fa-stopwatch add-on input-icon"></label>
                                            <input name="start_time" class="form-control" data-format="hh:mm:ss" type="text"></input>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="inputs_data">
                                <div class="text_dates width15">
                                    Ends
                                </div>
                                <div class="text_dates width40">
                                    <div id="datetimepicker2" class="input-append date">
                                        <div class="input-wrapper">
                                            <label for="stuff" class="far fa-calendar-alt add-on input-icon"></label>
                                            <input name="end_date" class="form-control" data-format="MM/dd/yyyy" type="text"></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="text_dates width40">
                                    <div id="datetimepicker4" class="input-append date">
                                        <div class="input-wrapper">
                                  <label for="stuff" class="fas fa-stopwatch add-on input-icon"></label>
                                            <input name="end_time" class="form-control" data-format="hh:mm:ss" type="text"></input>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="event-tour-package-row">
                    <div class="event_name_shedule">
                        <p>Tour package</p>
                    </div>
                    <div class="event_text_shedule">
                        <select name="event_tour_package" class="event-tour-package" style="width: 60%;">
                            <option></option>
                            <?php foreach ($tourPackages as $t) { ?>
                                <option value="<?php echo $t->tour_id; ?>">BALI<?php echo $t->tour_id; ?></option>
                            <?php } ?>
                            <?php if (empty($tourPackages)) { ?>
                                <option disabled="disabled">You have not created any tour package.</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="event-submit-row">
                    <button id="create-event-submit" class="btn btn-lg btn-success">Create</button>
                </div>

            </div>

        </div>
    </div>
</div>





<!--Start Show Post Tabs -->

<div id="showTId" class="show_tabs">
    <div class="show_container">
        <div class="show_content_top">
            <div class="show-post-left">
                <h4> Your Post </h4>
            </div>

            <div class="show-post-right" onclick="Showtabs2()">
                <i class="fas fa-times"></i>
            </div>

        </div>

        <div class="show_content_body">
            <div class="profile_area_show">
                <img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/261/profile_photo-40x40.jpg" width="35" height="35">
                <a href="#"><?php echo $display_name; ?></a>
                <br>
                <p> 7 hours ago, Lahore, Pakistan </p>
            </div>


            <div class="post_body_excerpt">

            </div>

            <div class="post_image">
                <img src="https://www.travpart.com/English/wp-content/themes/bali/assets/img/rome.jpg" style="width:650px;max-height:350px">
            </div>

            <div class="post-share-show-a">
                <div class="like-area like-area-show">
                    <a href="#" feedid="22734">
                        <i class="far fa-thumbs-up "></i>
                        <span>Like</span>
                    </a>
                </div>

                <div class="comment-area comment-area-show">
                    <a href="#">
                        <i class="far fa-comment-alt"></i>
                        Comment
                    </a>
                </div>


                <div class="share-area share-area-show">
                    <a href="#" class="dropbtn">
                        <i class="fas fa-share"></i> Share
                    </a>

                    <!--My DropDown-->

                    <div id="myDropdown" class="ShareDropdown">
                        <li onclick="share_feed(22734)">Share</li>
                        <li><a target="_blank" href="https://web.whatsapp.com/send?text=Off course he won't be hanged but any future dictator won't dare to break consti  ( see-more ) https://www.travpart.com/English/my-time-line/?user=t3">Share to whats app</a></li>

                        <li class="mores"><a class="a2a_dd" href="https://www.addtoany.com/share#url=https%3A%2F%2Fwww.travpart.com%2FEnglish%2Fmy-time-line%2F%3Fuser%3Dt3&amp;title=My%20Time%20Line%20-%20Travpart%20-%20world%20class%20premium%20travel%20%7C%20Travpart%20%E2%80%93%20world%20class%20premium%20travel"><i class="fas fa-plus-square"></i> More...</a></li>

                    </div>

                    <!--End My DropDown-->
                </div>
            </div>



            <div class="post-share-area">
                <div class="user-comment-area submit-comment show-post-comm">
                    <img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/261/profile_photo-40x40.jpg" width="35" height="35">
                    <input placeholder="Write a Comment" type="text" name="feed_comment" required="">
                    <input type="hidden" name="feed_id" value="22734">
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
<!--End Show Post Tabs -->

<!-- Show Edit Popup -->
<div class="editPost">
    <div class="EditConte">
        <div class="edit_post_header">
            <div class="Eheader-left">
                <h4>Edit Post</h4>
            </div>

            <div class="Epostright">
                <i class="fas fa-times"></i>
            </div>
        </div>

        <div class="Edit_content_body">
                <div class="profile_area_edit">
                    <textarea name="posttext"></textarea>
                    <input type="hidden" name="postid" />
                    <input type="hidden" name="feeling" />
                    <input type="hidden" name="tag_user_id" />
                    <input type="hidden" name="edit_tourpackage" />
                    <input type="hidden" name="edit_featured_image_id" />
                </div>

                <div class="editbodyexcerpt">

                    <p><span class="edit-feeling"></span> <span class="edit-tag-user"></span></p>

                    <div class="userPhThumbnail"></div>

                    <div class="userPhThumbnail2 edit-photo-video-button">
                        <i class="fas fa-plus"></i>
                    </div>
                </div>

                <div class="edit-all-btn">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="edit-photo-video edit-photo-video-button">
                                <div class="e-box-icon">
                                    <span calss="icon-box"></span>
                                    <i class="fas fa-images"></i>
                                </div>

                                <div class="e-box-content">
                                    <h4 class="icon-box-title">
                                        <span>Photo/Video</span>
                                    </h4>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="edit-photo-video edit-feeling-activity">
                                <div class="e-box-icon">
                                    <span calss="icon-box"></span>
                                    <i class="far fa-smile"></i>
                                </div>

                                <div class="e-box-content">
                                    <h4 class="icon-box-title">
                                        <span>Feeling/Activity</span>
                                    </h4>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="edit-photo-video edit-tag-people">
                                <div class="e-box-icon">
                                    <span calss="icon-box"></span>
                                    <i class="fas fa-user-tag"></i>
                                </div>

                                <div class="e-box-content">
                                    <h4 class="icon-box-title">
                                        <span>Tag Peoples</span>
                                    </h4>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="edit-photo-video edit-tour-package-button">
                                <div class="e-box-icon">
                                    <span calss="icon-box"></span>
                                    <i class="fas fa-plane"></i>
                                </div>

                                <div class="e-box-content">
                                    <h4 class="icon-box-title">
                                        <span>My Tour Packages</span>
                                    </h4>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="edit-delete-footer">
            <div class="btn-myTsave">
                <input id="update-post-submit" type="submit" value="Save">
            </div>
        </div>

        <div class="Filling-section edit-feeling-choose" style="top:35%; left:40%; position:fixed; display:none;">
            <div class="filing-section-main">
                <div class="Fill-header-top">
                    <i class="fas fa-times-circle"></i>
                </div>
                <form action="method" class="form-emoji">
                    <?php include(get_template_directory() . '/inc/emoji.php'); ?>
                </form>
            </div>
        </div>

        <div class="tag-people-Popup" style="top:35%; left:40%; position:fixed; display:none;">
            <div class="tag-people-main">
                <div class="tags-header-top">
                    <i class="fas fa-times-circle tag-close"></i>
                </div>

                <div class="tagBodyM">
                    <div class="userTagArea">
                        <?php foreach ($friends as $friend) { ?>
                        <div class="user-1 edit_tag_friend" userid="<?php echo $friend->ID; ?>">
                            <img src="<?php echo um_get_user_avatar_data($friend->ID)['url']; ?>">
                            <p class="username"><?php echo um_get_display_name($friend->ID); ?></p>
                            <div class="border-b-u"></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="hover_bkgr_fricc edit-tour-packages" style="left:15%;">
            <span class="helper"></span>
            <div>
                <div class="popupClose-close">X</div>
                <div class="input-group md-form form-sm form-1 pl-0" style="left:0">
                    <div class="input-group-prepend">
                        <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="basic-text1">
                            <i class="fas fa-search text-white" aria-hidden="true"></i>
                        </button>
                    </div>

                    <form class="search_hea" action="" method="get">
                        <input class="form-control my-0 py-1" type="text" placeholder="Search for tour packages" aria-label="Search" name="friend">
                    </form>
                </div>
                
                <div class="col-md-12">
                    <p class="sugg">Tour Packages Name</p>
                </div>
                <?php foreach ($tourPackages as $tour) { ?>
                <div class="col-md-12 feel_display edit-tourpackage" tour_id="<?php echo $tour->tour_id; ?>">
                    <i class="fa fa-calendar feel_icon"></i>
                    <p class="feel_text"><?php echo $tour->post_title; ?></p>
                </div>
                <?php } ?>
                <?php if (empty($tourPackages)) { ?>
                <div class="col-md-12 feel_display">
                    <p class="feel_text" style="margin:0;">You have not created any tour package.</p>
                </div>
                <?php } ?>
            </div>
        </div>

    </div>
</div>
<!-- End Show Edit Popup -->


<!-- Show Delete Popup -->

<div class="DeletePost">
    <div class="DeleteConte">
        <div class="edit_post_header">
            <div class="Eheader-left">
                <h4>Delete Post</h4>
            </div>

            <div class="Dpostright">
                <i class="fas fa-times"></i>
            </div>
        </div>

        <div class="Edit_content_body">

            <p> You can Edit it if you just need to change something. if you didn't create this post, we can help you <a href="">secure your account.</a> </p>
        </div>

        <div class="edit-delete-footer">
            <div class="btn-myTsave">
                <input id="deleteid" type="hidden" name="Delete" />
                <input type="submit" name="Delete" value="Delete" onclick="delete_post()" />
            </div>
        </div>
    </div>
</div>
<!-- End Delete Popup -->


<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js' charset="UTF-8"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js' charset="UTF-8"></script>

<script type="text/javascript">

    jQuery( document ).ready(function($){
        
        $('.iconsdd').click(function(){
            $('.iconsdd').css({'background':'#8cfffa'});
            $('.size_m').css('color','#000');
            $(this).css('background','#22b14c');
            $(this).find('.size_m').css('color','#fff');
            //$('.icon1').css('background','transparent');
        })
        
        $('.iconsd').click(function(){
            $('.iconsd').css('background','#128462');
            $(this).css('background','#22b14c');
            $('.icon1').css('background','transparent');
        })
        
        $(".post_photos_header_desk .for-v").css({"color":"green","border-bottom":"2px solid green"});
        $(".post_feed_header_desk").click(function(){
            $(".post-container").show();
            $(".mytime_desk_photo_gallery").hide();
            $(this).find('.for-v').css({"color":"green","border-bottom":"2px solid green"});
            $(".post_feed_header_desk").css("background-color","white");
            $(".post_photos_header_desk").css("background-color","lightgray");
            $(".post_photos_header_desk .for-v").css({"color":"black","border-bottom":"2px solid transparent"});
        });
        $(".post_photos_header_desk").click(function(){
            $(".post-container").hide();
            $(this).find('.for-v').css({"color":"green","border-bottom":"2px solid green"});
            $(".mytime_desk_photo_gallery").show();
            $(".post_photos_header_desk").css("background-color","white");
            $(".post_feed_header_desk").css("background-color","lightgray");
            $(".post_feed_header_desk .for-v").css({"color":"black","border-bottom":"2px solid transparent"});
        });
        $('.mytime_vd_gallery_popup').on('click', function (e) {
            e.preventDefault();
            var mytimevdgallery = $(this).attr('href');
            $('html').addClass('no-scroll');
            $('body').append('<div class="mytime_vd_gallery_opened"><video src="' + mytimevdgallery + '" controls="controls" autoplay></div>');
        }); 

    // Close Lightbox
    $('body').on('click', '.mytime_vd_gallery_opened', function () {
        $('html').removeClass('no-scroll');
        $('.mytime_vd_gallery_opened').remove();
    });
    $(".timeline_header_icon").click(function () {
        $(".post-container").show();
        $(".mytime_mob_photo_gallery").hide();
        $(".photos_header_icon").css({"border-bottom":"none","background-color":"white"});
        $(".timeline_header_icon").css({"border-bottom":"1px solid black","background-color":"lightgray"});
    });
    $(".photos_header_icon").click(function () {
        $(".post-container").hide();
        $(".mytime_mob_photo_gallery").show();
        $(".timeline_header_icon").css({"border-bottom":"none","background-color":"white"});
        $(".photos_header_icon").css({"border-bottom":"1px solid black","background-color":"lightgray"});
    });
        
    });
</script>
 <script >
        function record_share_data(id,type){
        var post_id = id;
        var type=type;
        $.ajax({
        type: "POST",
        url: '<?php echo site_url() ?>/submit-ticket/',
        data: {
        feed_id: post_id,
        type:type,
        action: 'social_share_record'
        }, // serializes the form's elements.
        success: function(data) {
        }
        });
        }
        </script>

<script>

function copy_link(id) {
  var copyText = document.getElementById("copytext"+id).href;
  document.addEventListener('copy', function(event) {
    event.clipboardData.setData('text/plain', copyText);
    event.preventDefault();
    document.removeEventListener('copy', handler, true);
  }, true);
  document.execCommand('copy');
  alert("Link Copied ");
}

</script>
<style type="text/css">
    .close_ev{
        font-size: 20px;
        cursor: pointer;
    }
    .photos_header_icon{
        background-color: lightgray;
    }
    .timeline_header_icon {
        background-color: white;
    }
    .mytime_image_div{
        width: 90%;
        position: relative;
        height: 370px;
        display: inline-block;
    }
    .mytime_image_div img{
        width: 100%;
        height: 350px;
    }
    .iconsd.icon2.create_event_desktop {
        background-color: lightblue;
    }
    .for_r.event_image_icon.event_icon_desktop {
        color: black;
    }
    .for_r.event_text_icon.event_text_desktop {
        color: black;
    }
    .post_video_overlay {
        z-index: 9999;
    }
    .post_video_overlay div.post_vd_ovlay_btn div.share-area div.ShareDropdown{
        left: -121px;
    }
    .post_video_overlay div.post_vd_ovlay_btn div.share-area div.ShareDropdown::before{
        left: 176px;
    }
    .post_video_overlay div.post_vd_ovlay_btn div.share-area div.ShareDropdown::after{
        left: 176px;
    }
    .wh_mytime_icon_div{
        display: inline-block;
        background-color: green;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 5px;
    }
    i.fab.fa-whatsapp.share_wh_mytime_i{
        color: white;
        font-size: 17px;
        position: relative;
        top: 3px;
        left: 5px;
        text-rendering: optimizeLegibility;
    }
    i.far.fa-copy.share_copy_link_mytime_i{
        position: relative;
        margin-right: 5px;
        top: 3px;
        font-size: 22px;
        margin-left: 3px;
        margin-bottom: 5px;
        color: black;
        text-rendering: optimizeLegibility;
    }
    .post-share-area div.share-area div.ShareDropdown{
        left: 40px;
    }
    .post-share-area div.share-area div.ShareDropdown::before{
        left: 100px;
    }
    .post-share-area div.share-area div.ShareDropdown::after{
        left: 100px;
    }
    .col_photo video {
        width: 64px;
        height: 70px;
    }
    .col_photo img {
        width: 66px;
        max-width: none;
        height: 76px;
    }
    .col_photo {
        margin-top: 5px;
    }
    .video_container {
        border: 1px solid #ccc;
    }
    .photos_video_overlay {
        position: absolute;
        top: 69%;
        background: rgba(0,0,0,0.7);
        width: 100%;
        text-align: center;
    }
    .photos_vd_ovlay_btn{
        color: white;
        font-size: 10px;
        padding: 4px 8px;
    }
    .photos_vd_ovlay_btn i.fas.fa-play{
        position: relative;
        left: -32%;
        color: white;
    }
    .photos_vd_ovlay_btn i.fas.fa-volume-up {
        position: relative;
        right: -15px;
    }
    .mytime_video_opened {
        z-index: 9999;
    }
    .post_video_overlay {
        transition: none;
        position: absolute;
        top: 86%;
        left: 0;
        transform: none;
    }
    .row.rating-desc{
        display: inherit;
    }
    .elementor-element-populatedsec.social-follow-pos{
        margin-top: 0px;
        padding-top: 100px;
    }
    .icon-box-content h4{
        font-family: sans-serif;
    }
    .dis_b{
        font-family: sans-serif;
    }
    .updatecoverpic.dropdown-toggle.updatecoverp_desk {
        font-family: sans-serif;
    }
    .event_image_icon i.fas.fa-calendar-alt, .event_image_icon i.fas.fa-user-friends, .event_image_icon i.fas.fa-coffee, .event_image_icon i.fas.fa-wine-bottle, .event_image_icon i.fas.fa-futbol {
        text-rendering: optimizeLegibility;
    }
    .t-box-icon i.fas.fa-images, .t-box-icon i.far.fa-smile, .t-box-icon i.fas.fa-user-tag, .t-box-icon i.fas.fa-plane {
        text-rendering: optimizeLegibility;
    }
    .p-dots-area_1 a i.fas.fa-ellipsis-h{
        text-rendering: optimizeLegibility;
    }
    .rating.custom_size_icon i {
        text-rendering: optimizeLegibility;
    }
    button.searchButton i{
        text-rendering: optimizeLegibility;
    }
    .post_feed_header_desk span i.fas.fa-clock{
        text-rendering: optimizeLegibility;
    }
    @media (max-width: 600px) {
        .col-xs-3.col-md-3.text-right.for_left_bar {
            margin-bottom: 5px;
        }
    }
    .share_mob_dropdown {
        position: absolute;
        background-color: white !important;
        min-width: 190px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1000 !important;
        border: 1px solid #ddd;
        margin-top: 4px;
        right: 0%;
        display: none;
        padding: .5rem 0;
    }
    .share_mob_dropdown li {
        display: block;
        margin: 5px;
        font-size: 16px;
        padding: 0 7px;
    }
    .share_mob_dropdown li a {
        color: #000;
        display: block;
        font-size: 16px;
        padding: 3px 3px 3px 0px;
        clear: both;
        font-weight: 400;
        line-height: 1.42857143;
        white-space: nowrap;
    }
    .share_mob_dropdown li:hover {
        background: #1ABC9C;
        color: #fff;
        cursor: pointer;
    }
    .share_mob_dropdown::before {
        content: "";
        position: absolute;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-bottom: 9px solid #ffffff;
        top: -6px;
        z-index: 9999;
        right: 45px;
    }
    .share_mob_dropdown::after {
        content: "";
        position: absolute;
        width: 10px;
        height: 10px;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-bottom: 8px solid #ddd;
        top: -10px;
        right: 45px;
    }
    .ph_share_mobile{
        position: relative;
    }
    .ph_share_mob_dropdown {
        position: absolute;
        background-color: white !important;
        min-width: 190px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1000 !important;
        border: 1px solid #ddd;
        margin-top: 4px;
        right: 0%;
        display: none;
        padding: .5rem 0;
    }
    .ph_share_mob_dropdown li {
        display: block;
        margin: 5px;
        font-size: 16px;
        padding: 0 7px;
    }
    .ph_share_mob_dropdown li a {
        color: #000;
        display: block;
        font-size: 16px;
        padding: 3px 3px 3px 0px;
        clear: both;
        font-weight: 400;
        line-height: 1.42857143;
        white-space: nowrap;
    }
    .ph_share_mob_dropdown li:hover {
        background: #1ABC9C;
        color: #fff;
        cursor: pointer;
    }
    .ph_share_mob_dropdown::before {
        content: "";
        position: absolute;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-bottom: 9px solid #ffffff;
        top: -6px;
        z-index: 9999;
        right: 45px;
    }
    .ph_share_mob_dropdown::after {
        content: "";
        position: absolute;
        width: 10px;
        height: 10px;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-bottom: 8px solid #ddd;
        top: -10px;
        right: 45px;
    }
</style>


<?php
get_footer();
?>