<?php

function p($data, $exit = 1)
{
  echo "<pre>";
  print_r($data);
  if ($exit == 1) {
    exit;
  }
}

/* Template Name: Contact Popup */ // get_header();
//include('header-ticket.php');
include('header.php');

global $wpdb;
$current_user = wp_get_current_user();

$cs_user_id = $current_user->data->ID;

$agentListSQL=<<<'EOD'
SELECT (SELECT userid FROM wp_users,user WHERE access = '1' AND activated=1 AND wp_users.user_login=user.username AND wp_users.ID=meta_value LIMIT 1) as userid,
	MAX(score)
FROM `wp_tour`,`wp_tourmeta`
WHERE
	meta_key='userid' AND wp_tour.id=wp_tourmeta.tour_id
	AND wp_tour.id IN(
		SELECT meta_value FROM `wp_posts`,`wp_postmeta` WHERE `meta_key`='tour_id' AND wp_posts.ID=wp_postmeta.post_id AND wp_posts.post_status='publish'
	)
GROUP BY meta_value
ORDER BY `MAX(score)` DESC
EOD;
$allAgents = $wpdb->get_results($agentListSQL, ARRAY_A);
$userIds='';
foreach($allAgents as $row) {
	if(!empty($row['userid']))
		$userIds.=$row['userid'].',';
}
$userIds=rtrim($userIds,',');
$userDetailsDB=$wpdb->get_results("SELECT userid,wp_users.ID as wp_user_id,username,country,region FROM `user`,`wp_users` WHERE user.username=wp_users.user_login AND userid IN ({$userIds})", ARRAY_A);
$userDetails=array();
foreach($userDetailsDB as $row) {
	$userDetails[$row['userid']]=$row;
}

?>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script>
<script type="text/javascript" src="app.js"></script>

<script type="text/javascript">
  $(window).on('load', function() {
    //$('#openSupport1').modal('show');
  });
</script>
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css">
<style>
  h1 {
    font-size: 30px !important;
  }
  input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  	color: #ccc;
  }
  .modal-header{
  	position: relative;
  }	
  .modal-header .close{
  	margin: 0px !important;
  	padding: 0px !important;
  	position: absolute;
  	right: 5px;
  	top: 5px;
  	
  }	
  i.fas.fa-chevron-down.con_pop_sel_i {
    position: absolute;
    right: 4%;
    margin-top: 32px;
  }
  section {
    padding: 10px 0;
    border-left: 1px solid #ccc;
    border-right: 1px solid #ccc;
  }

  /*.input-group .form-control:last-child:focus {
    font-size: 25px;
    color: #3b898a;
    height: 70px;
    text-transform: uppercase;
    -webkit-transition: 0.7s ease-in-out;
    -moz-transition: 0.7s ease-in-out;
    -o-transition: 0.7s ease-in-out;
    transition: 0.7s ease-in-out;
  }*/
.elementor-widget-text-editor,.elementor-19120 .elementor-element.elementor-element-a75a75e .elementor-accordion .elementor-tab-title,div,p,a,button{
				font-size: 14px;
			}
  .tickettable {
    padding: 10px;
  }

  td,
  .ticketpadding {
    padding: 10px !important;
  }

  tr:hover {
    background: #3b8989 !important;
    color: white;
    cursor: pointer
  }

  th {
    background: white !important;
  }

  #bcrumb {
    display: none !important;
  }

  .elementor-19120 .elementor-element.elementor-element-a28f40f {
    border: none;
  }

  .elementor-19120 .elementor-element.elementor-element-716b989 {
    border: none;
  }

  .swp_social_panel .swp_horizontal_panel,
  .at-share-btn-elements {
    display: none !important;
  }

  .ticketmenu {
    text-align: right;
  }

  .bigfont {
    font-size: 20px;
    font-weight: 600;
  }

  .bigfont button {
    text-decoration: none;
    background: #3c9e28;
    color: white;
    padding: 10px 20px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border-radius: 20px;
  }

  .tipsy,
  .swp_social_panel {
    display: none !important;
  }

  .modal {
    text-align: center;
  }

  .modal-header {
    font-size: 35px !important;
    background: #5dba42;
    color: white;
  }

  .modal-header h4 {
    font-size: 35px !important;
    color: white;
  }
  .close_1{
    position: absolute;
    top: -10px;
    right: -10px;
    border-radius: 20px;
    padding: 9px 15px;
    color:#fff;
    background: #3b898a;
    border: 1px solid #3b898a;
}

  .modal-body textarea {
    width: 100%;
    min-height: 200px;
    padding: 5px;
  }

  .createyourticket {
    width: 100%;
    text-align: center;
    font-size: 30px !important;
    text-decoration: none;
    color: white
  }

  .createyourticket a {
    padding: 5px 10px;
    border: 1px solid #5dba42;
    text-align: center;
    font-size: 30px !important;
    text-decoration: none;
    color: #5dba42
  }

  .createyourticket a:hover {
    background: #5dba42;
    color: white;
    -webkit-transition: 0.7s ease-in-out;
    -moz-transition: 0.7s ease-in-out;
    -o-transition: 0.7s ease-in-out;
    transition: 0.7s ease-in-out;
  }

  @media screen and (min-width: 768px) {
    .modal:before {
      display: inline-block;
      vertical-align: middle;
      content: " ";
      height: 100%;
    }
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    //vertical-align: middle;
  }

  .hover_bkgr_fricc {
    background: rgba(0, 0, 0, .4);
    cursor: pointer;
	  <?php if(is_user_logged_in()) { ?>
	    display: none;
	  <?php } else { ?>
	  display: block;
	  <?php } ?>
    height: 100%;
    position: fixed;
    text-align: center;
    top: 0;
    width: 100%;
    z-index: 10000;
    left: 0;
  }

  .hover_bkgr_fricc .helper {
    display: inline-block;
    height: 100%;
    vertical-align: middle;
  }

  .hover_bkgr_fricc>div {
    background-color: #fff;
    box-shadow: 10px 10px 60px #555;
    display: inline-block;
    height: auto;
    max-width: 551px;
    min-height: 100px;
    vertical-align: middle;
    width: 60%;
    position: relative;
    border-radius: 8px;
    padding: 15px 5%;
  }

  .popupCloseButton {
    background-color: #fff;
    border: 3px solid #999;
    border-radius: 50px;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: -20px;
    right: -20px;
    font-size: 25px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
  }

  .popupCloseButton:hover {
    background-color: #ccc;
  }

  .um .um-login .um-14064 .uimob500{
	margin-bottom: 0!important;
  }

  .nsl-container-buttons{
  	text-align: center;
  	display: block!important;
  }
  div.nsl-container-block .nsl-container-buttons a{
  	display: inline-block;
  }
  
  
@media (max-width: 640px) {
  .elementor-19120 .elementor-element.elementor-element-6bc331d{
    padding:50px !important; 
  }
  .row,.col-md-12{
  	padding: 0px;
  	margin: 0px;
  }

}    
</style>
<section class="custom_se_d">
  <!-- <h1 class="bigfont"><?php the_title(); ?></h1> -->

  <div class="hover_bkgr_fricc">
    <span class="helper"></span>
    
    <div class="mobilelogin">
    <div class="row">
        <div class="col-sm-12" style="text-align: center;">
          <a href="https://www.travpart.com/">
          <img style="height: 50px;" src="https://www.travpart.com/wp-content/uploads/2019/10/travpart-main.png" /></a>
        </div>
      </div>
      <?php echo do_shortcode('[ultimatemember form_id=14064]'); ?>
		<hr>
      <span style="color:#555555">Login as a buyer only:</span><br>
      <?php echo do_shortcode('[nextend_social_login login="1" link="1" unlink="1" redirect="https://www.travpart.com/English/user/"]'); ?>

      
    </div>
  </div>

  <div ng-app="sortApp" class="tickettable">

    <!-- Button trigger modal -->
    <form method="POST" action="" id="ticketForm">
      <?php
      $cs_user_name = '';
      //            $current_user = wp_get_current_user();
      if (isset($current_user->data->user_login)) {
        $cs_user_name = $current_user->data->user_login;
      }
      ?>
      <input type="hidden" name="action" value="customer_request">
      <input type="hidden" name="csa_tmp_user_name" value="<?php echo $cs_user_name; ?>">
      <!-- Modal 1-->
      <div class="modal fade" id="openSupport1" tabindex="-1" role="dialog" aria-labelledby="openSupportLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close_1" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="openSupportLabel">How can we help?</h4>
            </div>
            <div class="modal-body">
              <p>We're committed to finding the answers you need as quickly as possible. Please tell us a little about yourself and what you need help with.</p>

              <div class="form-group">
                <label for="usr">Your name:*</label>
                <input type="text" placeholder="Adam Anderson" class="form-control csa_user_name" name="csa_user_name" id="usr" required>
              </div>
              <div class="form-group">
                <label for="email">Your email:*</label>
                <input type="email" class="form-control csa_user_email" placeholder="example@gmail.com" name="csa_user_email" id="usr" required>
              </div>
              <div class="form-group">
                <label for="subject">Subject:</label>
                <input type="text" class="form-control csa_subject" placeholder="Thailand, Phuket" name="csa_subject" id="usr" placeholder="Price to Thailand, Phuket for 4 people">
              </div>
              <div class="form-group">
                <label for="plan">Your destination plan:</label>
                <input type="text" class="form-control csa_destination_plan" name="csa_destination_plan" id="ydp">
              </div>
              <div class="form-group">
                <label for="plan">Select Agent For Contact:</label>
                <i class="fas fa-chevron-down con_pop_sel_i"></i>
                <select class="form-control csa_agent_name" name="csa_agent_name">
                  <option value="0">I don't know any travel advisor yet</option>
                  <?php
                  foreach ($allAgents as $agent) {
					  if(!empty($agent['userid']) && !empty($userDetails[$agent['userid']])) {
						  $userinfo=$userDetails[$agent['userid']];
						  if(!empty($userinfo['country'])) {
							  if(!empty($userinfo['region']))
								  $location=" ({$userinfo['country']}, {$userinfo['region']})";
							  else
								  $location=" ({$userinfo['country']})";
						  }
						  else
							  $location='';
                    ?>
                    <option class="option_d" value="<?php echo $userinfo['username']; ?>"><?php echo um_get_display_name($userinfo['wp_user_id']),$location; ?></option>
                  <?php
					  }
				  }
                ?>
                </select>
              </div>
              <div class="form-group">
                <label for="message">Your message:*</label>
                <textarea class="csa_message" name="csa_message" placeholder="Hi, i'm coming as a family  to phuket, Thailand for 4 days next week.Would you be able to create a tour package for us? Thank You"></textarea>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" style="border: 1px solid #ccc;font-family: arial;background: #fff;color: #23b14d;padding: 7px 10px;border-radius: 0px;font-size: 13px;box-shadow: 0 0 0 5px hsl(0, 0%, 90%);margin-right:10px;">Close</button>
              <button type="button" class="btn btn-default btn-next" style="background: #23b14d;color: #fff;padding: 7px 10px;font-family: arial;border-radius: 0px;font-size: 13px;box-shadow: 0 0 0 5px hsl(0, 0%, 90%);">Continue</button>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal 2-->
      <div class="modal fade" id="openSupport2" tabindex="-1" role="dialog" aria-labelledby="openSupportLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="openSupportLabel">REVIEW</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="sel1">Please select your question category*</label>
                <select name="csa_question_cat" class="form-control" id="sel1">
                  <option>Price of a journey</option>
                  <option>Other</option>
                </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-prev">Prev</button>
              <button type="button" class="btn btn-default btn-next">Continue</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal 3-->
      <div class="modal fade" id="openSupport3" tabindex="-1" role="dialog" aria-labelledby="openSupportLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="openSupportLabel">COMPLETE</h4>
            </div>
            <div class="modal-body">
              If you can not find your question on our FAQ section, please click the button below to send your message, we will reply to you as soon as possible.
            </div>
            <div class="modal-footer">
              <div class="createyourticket">
                <button type="button" onclick="submitTicketForm()" class="btn btn-primary btn-lg">Submit</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <!-- article -->
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

          <?php the_content(); ?>

          <br class="clear">

          <?php edit_post_link(); ?>

        </article>
        <!-- /article -->

      <?php endwhile; ?>

    <?php else : ?>

      <!-- article -->
      <article>

        <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>

      </article>
      <!-- /article -->

    <?php endif; ?>

    <!-- Table sort from traven -->
    <!--        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-sanitize.js"></script>-->
    <script>
      angular.module('sortApp', [])
        .controller('sortController', ['$scope', function($scope) {
          $scope.sortName = 'ticketID';
          $scope.sortReverse = false;
          $scope.users = [
            <?php
            if (!empty($myTickets)) {
              foreach ($myTickets as $ticket) {
                $cs_created = date("d M y", strtotime($ticket['cs_created']));
                $cs_updated = date("d M y", strtotime($ticket['cs_updated']));
                if ($ticket['cs_status'] == '0') {
                  $cs_status = "OPEN";
                } else {
                  $cs_status = "CLOSED";
                }
                ?> {
                  ticketID: <?php echo isset($ticket['cs_id']) ? '000' . $ticket['cs_id'] : '' ?>,
                  //                                                                    ticketSubject: '<a href="https://www.travpart.com/English/create-ticket/?ticketid=<?php echo isset($ticket['cs_id']) ? $ticket['cs_id'] : '' ?>"><?php echo isset($ticket['cs_type']) ? $ticket['cs_type'] : '' ?></a>',
                  ticketSubject: '<?php echo isset($ticket['cs_type']) ? $ticket['cs_type'] : '' ?>',
                  ticketCreated: '<?php echo $cs_created; ?>',
                  lastActivity: '<?php echo $cs_updated; ?>',
                  ticketStatus: '<?php echo $cs_status; ?>'
                },
              <?php
            }
          }
          ?>
          ]

          $scope.search = function() {
            $scope.filteredList = _.filter($scope.users,
              function(item) {
                return searchUtil(item, $scope.searchUserTerm);
              });
            if ($scope.searchUserTerm == '') {
              $scope.filteredList = $scope.users;
            }
          }
        }]);
    </script>

    <!-- popup code from traven -->
    
    <script>
	
		
	$(document).ready(function(){
		//alert()
		var options = $('.csa_agent_name .option_d');
	    var arr = options.map(function(_, o) {
	        return {
	            t: $(o).text(),
	            v: o.value
	        };
	    }).get();
	    arr.sort(function(o1, o2) {
		  var t1 = o1.t.toLowerCase(), t2 = o2.t.toLowerCase();

		  return t1 > t2 ? 1 : t1 < t2 ? -1 : 0;
		});
	    options.each(function(i, o) {
	        console.log(i);
	        o.value = arr[i].v;
	        $(o).text(arr[i].t);
	    });
	})	
    
      function submitTicketForm() {
      	
        var form = $("#ticketForm");
        var url = 'https://www.travpart.com/English/submit-ticket';
        $('.createyourticket button').addClass('disabled');

        $.ajax({
          type: "POST",
          url: url,
          data: form.serialize(), // serializes the form's elements.
          success: function(data) {
            var result = JSON.parse(data);
            if (result.status) {
              alert(result.message);
              //                            window.location.href = 'https://www.travpart.com/English/travchat';
              window.location.href = 'https://www.travpart.com/English/travchat/user/index.php?user=' + result.agentUserid;
            } else {
              $('.createyourticket button').removeClass('disabled');
              alert(result.message);
              window.location.reload() // show response from the php script.
            }
          }
        });

      }
      /* $("div[id^='openSupport']").each(function () {
       
       var currentModal = $(this);
       
       //click next
       currentModal.find('.btn-next').click(function () {
       currentModal.modal('hide');
       currentModal.closest("div[id^='openSupport']").nextAll("div[id^='openSupport']").first().modal('show');
       });
       
       //click prev
       currentModal.find('.btn-prev').click(function () {
       currentModal.modal('hide');
       currentModal.closest("div[id^='openSupport']").prevAll("div[id^='openSupport']").first().modal('show');
       });
       
       });*/
      // Code by Amrut for correcttion Contact form [ 22 Jan 2019 ] START
      $("div[id^='openSupport']").each(function() {

        var currentModal = $(this);

        //click next
        currentModal.find('.btn-next').click(function() {
      	// var csa_destination_plan = $('#csa_destination_plan').val();
      	 //var csa_subject = $('#csa_subject').val();	
      	 var csa_user_email = $('.csa_user_email').val();
      	 var csa_user_name = $('.csa_user_name').val();
      	 var csa_message = $('.csa_message').val();
         var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

         if((csa_user_name=='' || csa_user_name==null) || (csa_user_email=='' || csa_user_email==null) || (csa_message =='' || csa_message ==null)){
         	alert('Please fill all the * required fields.');
         	return false;
         }else if(!pattern.test(csa_user_email)){
         	//$('.csa_user_email').css({'border':'1px solid red','color':'red'})
         	alert('not a valid e-mail address');
         	return false;
         }else{
         	//alert('filled	');
         };
         var modal = $(this).closest('.modal').modal('hide');
          modal.closest("div[id^='openSupport']").nextAll("div[id^='openSupport']").first().modal('show');
        });

        //click prev
        currentModal.find('.btn-prev').click(function() {
          var modal = $(this).closest('.modal').modal('hide');
          modal.closest("div[id^='openSupport']").prevAll("div[id^='openSupport']").first().modal('show');
        });

      });
      // Code by Amrut for correcttion Contact form [ 22 Jan 2019 ] END
    </script>

</section>
<?php get_sidebar(); ?>

<?php get_footer(); ?>