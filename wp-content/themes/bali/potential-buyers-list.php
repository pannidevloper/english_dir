<?php
/*
Template Name: Potential Buyers List
*/

wp_head();

get_header();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Seller Profile</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
	
	
	<div class="potential_buyer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="potential_headding">
						<h2>Potential Buyer List </h2>
					</div>

					<div class="potential_user_list">
						<div class="row">
							<div class="col-md-3 cus-md-3">
								<div class="customer_area">
									

<div class="tooltip2"><h5>Customer's Name</h5>
  <span class="tooltiptext">This is the list of your potential customer's name</span>
</div>

									
								</div>

								<div class="customer_list">
									<ul>
										<li>1. Adam Sandler</li>
										<li>2. Sandy Morgan</li>
										<li>3. Sina Nokal</li>
										<li>4. Michael jackson</li>
									</ul>
								</div>
							</div>

							<div class="col-md-3 cus-md-3">
								<div class="customer_area">

<div class="tooltip2"><h5>Searched</h5>
  <span class="tooltiptext">This column shows what's your page visitors has been searching for</span>
</div>


								</div>

								<div class="customer_list"></div>
							</div>

							<div class="col-md-3 cus-md-3">
								<div class="customer_area">

<div class="tooltip2"><h5>Visited Pages</h5>
  <span class="tooltiptext">These are the pages that your potential buyers visited in the last 45 days</span>
</div>

								</div>

								<div class="customer_list"></div>
							</div>
							<div class="col-md-3 cus-md-3">
								<div class="customer_area">

<div class="tooltip2"><h5>Reach Out</h5>
  <span class="tooltiptext">Talks to your potential buyer's</span>
</div>

								</div>

								<div class="customer_list">
									<div class="talk-btn">
										<a href="#" class="talk-btn">Talk</a>
									</div>

									<div class="talk-btn">
										<a href="#" class="talk-btn">Talk</a>
									</div>

									<div class="talk-btn">
										<a href="#" class="talk-btn">Talk</a>
									</div>	

									<div class="talk-btn">
										<a href="#" class="talk-btn">Talk</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php 
		wp_footer();
		get_footer();
	?>

</body>
</html>