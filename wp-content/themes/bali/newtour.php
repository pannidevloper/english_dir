<?php
require_once('../../../wp-load.php');

function easy_setcookie($key, $value)
{
    setcookie($key, $value, (time() + 86400 * 30), '/English/', '.travpart.com');
    setcookie($key, $value, (time() + 86400 * 30), '/english/', '.travpart.com');
}

function easy_sethotelcookie($key, $value)
{
    setcookie($key, $value, (time() + 86400 * 30), '/English/', '.travpart.com');
    setcookie($key, $value, (time() + 86400 * 30), '/english/', '.travpart.com');
	setcookie($key, $value, (time() + 86400 * 30), '/hotel-and-flight-bookings/', '.travpart.com');
}

easy_setcookie('tour_id', '');

easy_sethotelcookie('hotel_price', '');

easy_setcookie('meal_type', '');
easy_setcookie('meal_date', '');
easy_setcookie('meal_time', '');
easy_setcookie('meal_price', '');

easy_setcookie('restaurant_name', '');
easy_setcookie('restaurant_local', '');
easy_setcookie('restaurant_img', '');
easy_setcookie('restaurant_price', '');
easy_setcookie('restaurant_diners', '');
easy_setcookie('restaurant_date', '');
easy_setcookie('restaurant_time', '');

easy_setcookie('car_type', '');
easy_setcookie('car_date', '');
easy_setcookie('car_time', '');
easy_setcookie('car_price', '');

easy_setcookie('popupguide_type', '');
easy_setcookie('popupguide_date', '');
easy_setcookie('popupguide_time', '');
easy_setcookie('guide_price', '');

easy_setcookie('spa_type', '');
easy_setcookie('spa_date', '');
easy_setcookie('spa_time', '');
easy_setcookie('spa_price', '');

easy_setcookie('boat_type', '');
easy_setcookie('boat_date', '');
easy_setcookie('boat_time', '');
easy_setcookie('boat_price', '');

easy_setcookie('eaiitem', '');
easy_setcookie('eainum', '');
easy_setcookie('eaidate', '');
easy_setcookie('eaitime', '');
easy_setcookie('activity_price', '');

easy_setcookie('place_img', '');
easy_setcookie('place_name', '');
easy_setcookie('place_date', '');
easy_setcookie('place_time', '');
easy_setcookie('place_price', '');

easy_setcookie('tourlist_img', '');
easy_setcookie('tourlist_name', '');
easy_setcookie('tourlist_price', '');
easy_setcookie('tourlist_date', '');
easy_setcookie('tourlist_time', '');

easy_setcookie('spotname', '');
easy_setcookie('spotlat', '');
easy_setcookie('spotlong', '');
easy_setcookie('spotdate', '');
easy_setcookie('spottime', '');

easy_setcookie('update', 0);

wp_redirect(site_url() . '/travcust');

?>