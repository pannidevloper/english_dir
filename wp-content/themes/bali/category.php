<?php
get_header();
?>


<?php
if(have_posts()) {
	echo '<h1>';
	single_cat_title();
	echo '</h1>';
	while(have_posts()) {
		the_post();
		$sImg = '';
		if(has_post_thumbnail()) {
			$aFimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'single-post-thumbnail');
			$img = matthewruddy_image_resize(clsImg($aFimg[0]), 320, 205, true, false);
			$sImg = $img['url'];
		} 
		echo '<div class="rmds row">
				<div class="col col-md-4">
					<a href="'.get_the_permalink().'"><img src="'.$sImg.'" alt="'.get_the_title().'" /></a>
				</div>
				<div class="col col-md-8">
					<div class="pull-right"><a href="'.get_the_permalink().'" class="btn btn-default arr-sm">Read</a></div>
					<h2><a href="'.get_the_permalink().'">'.get_the_title().'</a></h2><p class="blgdt"> '.date('M d, Y', strtotime(get_the_date())).'</p><p>'.get_the_excerpt().'</p>
				</div>
			</div>';
	}
	kriesi_pagination();
?>

<h2> Hot Articles</h2>
	<ul class="wpp-list">
			<?php
			$wp_query = new WP_Query( array ('post_status' => 'publish', 'posts_per_page' => 10, 'cat' => '2' , 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num','order'=>'DESC') ); 

			 if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();  ?>
				
			<li> <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="wpp-post-title" target="_self">
				<?php the_title(); ?></a>  
			</li>						
					
			<?php endwhile; endif;  ?>

			
	</ul>

<?php

} else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>




