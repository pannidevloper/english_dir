<?php
/* Template Name: Travel Hub */

get_header();
if(have_posts()) {
	while(have_posts()) {
		the_post(); ?>
        <div class="left-sidebarforum">
            <?php if ( is_active_sidebar( 'forum-side-bar' ) ) : ?>
                <?php dynamic_sidebar( 'forum-side-bar' ); ?>
            <?php endif; ?>
        </div>
        </div>
        <div style="width: 70%; float:left" class="travel-hub right-forum">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="/forums/forum/travel-companions/destinations/">Most Recent</a></li>
                <li role="presentation"><a href="/most-viewed">Most Viewed</a></li>
                <li role="presentation" class="active"><a href="#">Travel Hub</a></li>
            </ul>
            <hr>
            <div style="width: 50%; float: left;">
                <?php
                    wp_list_pages(array(
                        'title_li' => 'Destinations',
                        'child_of' => 5040,
                        'post_type' => 'forum',
                        'depth' => 2
                    ));
                ?>
            </div>
            <div style="width: 50%; float: left; padding-left: 10px">
                <?php
                wp_list_pages(array(
                    'title_li' => 'Tour Type',
                    'child_of' => 5043,
                    'post_type' => 'forum',
                    'depth' => 2
                ));
                wp_list_pages(array(
                    'title_li' => 'Advice Corner',
                    'child_of' => 5045,
                    'post_type' => 'forum',
                    'depth' => 2
                ));
                ?>
            </div>
        </div>

        <?php
	}
} else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>