<?php
/* Template Name: Submit Ticket */
//error_reporting(E_ALL);

function to_get_user_device_token($tour_creator_id)
{
    global $wpdb;

$get_user_data =$wpdb->get_row("SELECT * FROM `wp_users` where  ID= " . $tour_creator_id);
$user_email = $get_user_data->user_email;

if(!empty($user_email)) {

$get_user_data_tabl2 = $wpdb->get_row("SELECT * FROM `user` where  email = '{$user_email}'");
$user_id = $get_user_data_tabl2->userid;

if(!empty($user_id)) {

$get_user_data_tabl3 =$wpdb->get_row("SELECT * FROM `sessions` where  user_id = " . $user_id);
$user_device_token = $get_user_data_tabl3->device_token;

return $user_device_token;
}
}
}

function p($data, $exit = 1) {
    echo "<pre>";
    print_r($data);
    if ($exit == 1) {
        exit;
    }
}

global $wpdb;
if (isset($_POST['action']) && $_POST['action'] == 'remove_from_suggestions') {

    if (!is_user_logged_in()) {
    $data['status'] = 0;
    $data['message'] = "You must need to login first.";
    echo json_encode($data);
    exit;
    }
    {
    $current_user = wp_get_current_user();
    $user_id = $_POST['user_id'];
    $wpdb->insert('wp_remove_suggestions', array('wp_user_id'=>$current_user->ID, 'removed_user_id'=>$user_id));
    $data['status'] = 1;
    $data['message'] = "Removed Successfully.";
    echo json_encode($data);
    exit;
    }

    }
if (isset($_POST['action']) && $_POST['action'] == 'social_follow') {
    if (!is_user_logged_in()) {
        $data['status'] = 1;
        $data['message'] = "You must need to login first.";
        echo json_encode($data);
        exit;
    }
    $current_user = wp_get_current_user();
    $sf_user_id = isset($current_user->ID) ? $current_user->ID : '';


    if (intval($_POST['user_id'])<=0) {
        $data['status'] = 1;
        $data['message'] = "Wrong user id.";
        echo json_encode($data);
        exit;
    }

/*
    $tour_id = intval($_POST['tour_id']);
    //Checking tour is in our DB or not
    $tourData = $wpdb->get_results("SELECT * FROM `wp_tour` WHERE id = '$tour_id'", ARRAY_A);
    if (empty($tourData)) {
        $data['status'] = 1;
        $data['message'] = "Tour is not found in travpart records.";
        echo json_encode($data);
        exit;
    }
*/
  /*  //From tour id getting agent id
    $metaDataForAgent = $wpdb->get_results("SELECT meta_value FROM `wp_tourmeta` WHERE tour_id = '$tour_id' AND meta_key = 'userid'", ARRAY_A);
    if (empty($metaDataForAgent) || empty($metaDataForAgent[0]['meta_value'])
        || intval($metaDataForAgent[0]['meta_value'])<0) {
        $data['status'] = 1;
        $data['message'] = "No such a agent valid for this action.";
        echo json_encode($data);
        exit;
    }
*/
    $sf_agent_id =   intval($_POST['user_id']);
    
    if($sf_agent_id===intval($sf_user_id)) {
        $data['status'] = 1;
        $data['message'] = "You can't follow yourself.";
        echo json_encode($data);
        exit;
    }

    $followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$sf_user_id' AND sf_agent_id = '$sf_agent_id'", ARRAY_A);
    if (!empty($followData)) {
        $sf_id = isset($followData[0]['sf_id']) ? $followData[0]['sf_id'] : '';
        $wpdb->delete('social_follow', array('sf_id' => $sf_id), array('%d'));
        $data['status'] = 0;
        $data['action'] = 'unfollow';
        $data['message'] = "You unfollow  Successfully!.";
        echo json_encode($data);
        exit;
    } else {

        $wpdb->insert(
                'social_follow', array(
            'sf_agent_id' => $sf_agent_id,
            'sf_user_id' => $sf_user_id,
            'sf_created' => date('Y-m-d H:i:s'),
                ), array(
            '%d',
            '%d',
            '%s',
                )
        );
        $wpdb->insert('notification', array(
            'wp_user_id'=>$sf_agent_id,
            'content'=> "Congratulations ".um_get_display_name($sf_user_id)." Started Following you!",
            'identifier'=>'new_follower'),
            array('%d','%s','%s'));
        $data['status'] = 0;
        $data['action'] = 'follow';
        $data['message'] = "You followed This user successfully!.";

        // send email to user

        if( function_exists('some_one_following_your_page_email')){
            $user_data = get_user_by('id', $sf_agent_id);
            $enable_notification=$wpdb->get_var("SELECT subscribe FROM `unsubscribed_emails` WHERE email='{$user_data->user_email}' AND type=6");
            if (is_null($enable_notification) || $enable_notification==1) {
                $userinfo=$wpdb->get_row("SELECT username,user_registered FROM user WHERE email='{$user_data->user_email}'");
                $unsubscribe_link=home_url('unsubscribe').'/?qs='.base64_encode($user_data->user_email.'||'.md5('EC'.$userinfo->username.$user_data->user_email.$userinfo->user_registered));
                $connection_count = $wpdb->get_var("SELECT COUNT(*) FROM friends_requests WHERE ( user2 = '{$sf_user_id}' OR user1 = '{$sf_user_id}') AND status=1");
                some_one_following_your_page_email($user_data->user_email, um_get_display_name($sf_user_id), get_avatar_url($sf_user_id), $connection_count, $unsubscribe_link);
            }
        }

           //Added by farhan
    
        $msg=um_get_display_name($sf_user_id)." Started Following you!";

        if(!empty($sf_agent_id)){

        $token= to_get_user_device_token_from_wp_id($value);

    $count_last_1_hour_notifaction ="SELECT COUNT(*) FROM `notification` WHERE create_time>= NOW()- INTERVAL 1 HOUR AND wp_user_id='{$sf_agent_id}' AND identifier = 'new_follower'";

        if(!empty($token) AND $count_last_1_hour_notifaction <1) {
         notification_for_user( $token,$msg,'new_follower');

        }
        }
        echo json_encode($data);
        exit;
    }
}
if (isset($_POST['action']) && $_POST['action'] == 'block_unblock_method') {

   $user_id = $_POST['block_id']; 
   $current_user = wp_get_current_user();
$loggedin_user_id = isset($current_user->ID) ? $current_user->ID : '';

$query = $wpdb->get_row("SELECT * FROM `friends_requests` where user1='{$loggedin_user_id}' AND user2='{$user_id}'");

if($query){
if( $query->status==2 ){

$wpdb->delete( 'friends_requests', array( 'id' => $query->id ) );
$data['status']=1;
$data['msg'] ="Unblocked successfully";
}
}else{

$sql = $wpdb->prepare("INSERT INTO `friends_requests` (`user1`, `user2`,`status`,`action_by_user`) values (%d, %d,%d,%d)", $loggedin_user_id,$user_id,'2',$loggedin_user_id);

$wpdb->query($sql);
$data['status']=1;
$data['msg'] ="blocked successfully";
}

echo json_encode($data);

}
// confirm friend
if (isset($_POST['action']) && $_POST['action'] == 'add_friend') {

$request_id = $_POST['request_id'];
$request_type = $_POST['typeo'];
$request_type =(int) $request_type;

$reciver_id = $wpdb->get_row("SELECT * FROM `friends_requests` where id='{$request_id}'");
$current_user = wp_get_current_user();
$loggedin_user_id = isset($current_user->ID) ? $current_user->ID : '';
if($request_type==3){

    $wpdb->delete( 'friends_requests', array( 'id' => $request_id ) );
}
$wpdb->update(
                    'friends_requests', array(
                'status' => $request_type,
                'action_by_user' => $loggedin_user_id,
            
                    ), array('id' => $request_id), array(
                '%d',
                '%d',
                    ), array('%d')
            );

$data['status'] = 1;
if($request_type==1){
    $data['message'] = "Friend Request Accepted";

    $token= to_get_user_device_token_from_wp_id($reciver_id->user1);
    $receiver_name = get_user_by('id', $reciver_id->user2);
    $receiver_name = $receiver_name->user_login;
    $msg2 ="<a href='https://www.travpart.com/English/my-time-line/?user=$receiver_name'>".um_get_display_name($reciver_id->user2)." accepted your friend's request. Post something on his/her timeline.</a>";
    $wpdb->insert('notification', array('wp_user_id'=>$reciver_id->user1, 'content'=>$msg2,'identifier'=>'friend_request_confirmed_'.$receiver_name));

     $wpdb->insert('notification', array('wp_user_id'=>$reciver_id->user2, 'content'=>'You are  connected with '. um_get_display_name($reciver_id->user1),'identifier'=>'friends_now'));
   //  $token ='fKu4Z8LZcS4:APA91bHpsoV9K-RjhKXPDSSB-0i0Hb7BQ7Y_CtwwzkZkICyh25SWImzrs6-rbBmsx9Mrb_V8bVELpHEUOhEF6gkXFcBEu63Rx3PLWnpq1Ntgf8rOaz4wqsAckUxxE5e6Y1aCFwf38xXa';
    if(!empty($token)){

    $msg = um_get_display_name($reciver_id->user2)." accepted your friend's request. Post something on his/her timeline.";

    // sendtomobilefunction($msg,$token);
    notification_for_user( $token,$msg,'friend_request_confirmed_'.$receiver_name);
    } 
}
else{
    $data['message'] = "Friend Request Removed";
    $data['action'] ='removed';
}
echo json_encode($data);


}
if (isset($_POST['action']) && $_POST['action'] == 'connection_request_cancel') {

$current_user = wp_get_current_user();
$loggedin_user_id = isset($current_user->ID) ? $current_user->ID : '';
$user2 = $_POST['friend_id'];
$check_id = $wpdb->get_row("SELECT * FROM `friends_requests` where user1='{$loggedin_user_id}' AND user2='{$user2}'");
if($check_id==NULL){
   $check_id = $wpdb->get_row("SELECT * FROM `friends_requests` where user2='{$loggedin_user_id}' AND user1='{$user2}'"); 
}
if($check_id->status==0 || $check_id->status==1){

$wpdb->delete( 'friends_requests', array( 'id' => $check_id->id) );
$data['status'] = 1;
$data['message'] = "Friend Request canceled";

}
/*
if($check_id->status==1){

$wpdb->update(
                'friends_requests', array(
                'status' => 0,
                'action_by_user' => $loggedin_user_id,
            
                    ), array('id' => $check_id->id), array(
                '%d',
                '%d',
                    ), array('%d')
            );
$data['status'] = 2;
$data['message'] = "Connection removed";


}*/
echo json_encode($data);
}

$friend = $_POST['friend_id'];

// connect friend
if (isset($_POST['action']) && $_POST['action'] == 'connect_friend') {

$friend = $_POST['friend_id'];

$current_user = wp_get_current_user();
$loggedin_user_id = isset($current_user->ID) ? $current_user->ID : '';

$check_for_request = $wpdb->get_row("SELECT * FROM `friends_requests` where user1='{$loggedin_user_id}' AND user2='{$friend}'");
if($check_for_request==NULL){

$sql = $wpdb->prepare("INSERT INTO `friends_requests` (`user1`, `user2`,`action_by_user`) values (%d, %d,%d)", $loggedin_user_id,$friend,$loggedin_user_id);

$wpdb->query($sql);

    $token= to_get_user_device_token_from_wp_id($friend);


     $msg2 ="<a href='https://www.travpart.com/English/connection-request-list/'>".um_get_display_name($current_user->ID)." sent you friend Request</a>";
                $wpdb->insert('notification', array('wp_user_id'=>$friend, 'content'=>$msg2,'identifier'=>'friend_request_received_'.$current_user->user_login));

    if(!empty($token)){

    $msg = um_get_display_name($current_user->ID)." sent you friend Request";

    // sendtomobilefunction($msg,$token);
    notification_for_user( $token,$msg,'friend_request_received_'.$current_user->user_login);
    } 

$data['status'] = 1;
$data['message'] = "Friend Request Sent successfully";
echo json_encode($data);
}

}
if (isset($_POST['action']) && $_POST['action'] == 'social_share_record') {

$post_id = $_POST['feed_id'];
$type = $_POST['type'];
 $current_user = wp_get_current_user();
$loggedin_user_id = isset($current_user->ID) ? $current_user->ID : '';
if($loggedin_user_id==0) {
    $loggedin_user_id = $_SERVER['REMOTE_ADDR'];
}
$wpdb->insert('user_feed_shared_data', array('post_id'=>$post_id, 'share_by_user_id'=>$loggedin_user_id,'type'=>$type));

$data['status'] = 1;
echo json_encode($data);

 }
// feed back share
if (isset($_POST['action']) && $_POST['action'] == 'feedback_share') {
$post_id = $_POST['feed_id'];
if (!is_user_logged_in()) {
$data['status'] = 1;
$data['message'] = "You must  need to login first for this action.";
echo json_encode($data);
}
else{
    $current_user = wp_get_current_user();
$loggedin_user_id = isset($current_user->ID) ? $current_user->ID : '';

//get feed date for shareing
$oldpost = get_post($post_id);
    $post    = array(
      'post_content' => $oldpost->post_content,
      'post_status' => 'publish',
      'post_type' => $oldpost->post_type,
      'post_author' => $loggedin_user_id
    );
    $new_post_id = wp_insert_post($post);

    /// share data record in db
$wpdb->insert('user_feed_shared_data', array('post_id'=>$post_id, 'share_by_user_id'=>$loggedin_user_id,'type'=>'website_share'));

/// notifaction
    $msg = "<a href='https://www.travpart.com/English/shortpost/$post_id'>".um_get_display_name($wp_userid) . " shared a post </a>";
                $wpdb->insert('notification', array('wp_user_id'=>$oldpost->post_author, 'content'=>$msg,'identifier'=>'share_post'));

    $data = get_post_custom($post_id);
    foreach ( $data as $key => $values) {
      foreach ($values as $value) {
        add_post_meta( $new_post_id, $key, $value );
      }
    }


$data['status'] = 1;
$data['message'] = "Successfully Shared.";
echo json_encode($data);
}

}


// Feed Like function added by farhan
if (isset($_POST['action']) && $_POST['action'] == 'feedback_like') {
$feed_id = $_POST['feed_id'];
if (!is_user_logged_in()) {
$data['status'] = 1;
$data['message'] = "You must  need to login first for this action.";
echo json_encode($data);
}
else{

$current_user = wp_get_current_user();
$sf_user_id = isset($current_user->ID) ? $current_user->ID : '';

$Data = $wpdb->get_row("SELECT * FROM `user_feed_likes` WHERE user_id = '$sf_user_id' AND feed_id = '$feed_id'");
if($Data){
$feed_id_get =$Data->id;
$table = 'user_feed_likes';
$wpdb->delete( $table, array( 'ID' => $feed_id_get ) ); 

$data['status'] = 1;
$data['message'] =  "Updated Status" ;
echo json_encode($data);
}
else{

$wpdb->insert(
'user_feed_likes',
array(
'feed_id' => $feed_id,
'user_id' => $sf_user_id,

),
array(
'%s',
'%s',
)
);

//get the post username from post id

$author_id = get_post_field( 'post_author', $feed_id );

/// User Notifaction process

$msg = "<a href='https://www.travpart.com/English/shortpost/$feed_id'>".um_get_display_name($wp_userid) . " Liked your post </a>";
                $wpdb->insert('notification', array('wp_user_id'=>$author_id, 'content'=>$msg,'identifier'=>'some_one_liked_the_post_'.$feed_id));

    // get  user Token
    $token= to_get_user_device_token_from_wp_id($author_id);

    if(!empty($token)){

    $msg = um_get_display_name($wp_user_id)." Liked your post";

    notification_for_user( $token,$msg,'some_one_liked_the_post'.$feed_id);
   } 


$data['status'] = 0;

$data['message'] = "Successfully Liked the post";
echo json_encode($data);
}
}
}
// delete post 
if (isset($_POST['action']) && $_POST['action'] == 'delete_feed_post') {
  $post_id = $_POST['post_id'];
  wp_delete_post($post_id);
   $data['message'] = "Your Post deleted successfully";
        echo json_encode($data);
}
if (isset($_POST['action']) && $_POST['action'] == 'user_report') {
$user_id = $_POST['user_id'];
 $type = $_POST['type'];

if(isset($user_id)){
    $current_user = wp_get_current_user();
    $reported_user = get_user_by('id', $user_id);
    $to = 'tourfrombali@gmail.com';
        $subject = 'User Reported';
        $body = $current_user->user_login . " Reported the user"."<br>";
        $body.= 'UserName : ' .$reported_user->user_login."<br>";
        $body.= 'Reason : ' . $type;

        $headers = array('Content-Type: text/html; charset=UTF-8');

        wp_mail( $to, $subject, $body, $headers );
 }
    $data['message'] = "Your successfully Reported the user";
    echo json_encode($data);

    }

// Code for Reporting issue to admin

if (isset($_POST['action']) && $_POST['action'] == 'report_seller') {
  $tour_id = $_POST['tour_id'];
    if (!is_user_logged_in()) {
    $data['status'] = 1;
        $data['message'] = "You must need to login first.";
        echo json_encode($data);
    }
    else{
    $current_user = wp_get_current_user();
    $sf_user_id = isset($current_user->ID) ? $current_user->ID : '';
    $email = $current_user->user_email;
    $tourData = $wpdb->get_results("SELECT * FROM `reported_tours` WHERE reported_tour_id = '$tour_id' AND report_by_user_id = '$sf_user_id'", ARRAY_A);
    if($tourData)
    {
        $data['status'] = 1;
        $data['message'] =  "You already Reported that Tour" ;
        echo json_encode($data);
    }
    else{
  
        $wpdb->insert(
            'reported_tours',
            array(
                'reported_tour_id' => $tour_id,
                'report_by_user_id' => $sf_user_id,
                'report_by_user_email'=>$email,
               
            ),
            array(
                '%s',
                '%s',
                '%s',
            )
        );

        // send email

        $to = 'tourfrombali@gmail.com';
        $subject = 'Tour Reported';
        $body = $current_user->user_login . " Reported the Tour with id: ".$tour_id. " Please Review the issue";
        $headers = array('Content-Type: text/html; charset=UTF-8');

        wp_mail( $to, $subject, $body, $headers );

        $data['status'] = 1;
        $data['message'] =  'You Reported Tour Successfully,Thank you !' ;
        echo json_encode($data);
    }

                

    }

    }

if (isset($_POST['action']) && $_POST['action'] == 'post_social_follow') {
    if (!is_user_logged_in()) {
        $data['status'] = 1;
        $data['message'] = "You must need to login first.";
        echo json_encode($data);
        exit;
    }
    $current_user = wp_get_current_user();
    $sf_user_id = isset($current_user->ID) ? $current_user->ID : '';


    if (intval($_POST['post_id']) <= 0) {
        $data['status'] = 1;
        $data['message'] = "You have seleted wrong post.";
        echo json_encode($data);
        exit;
    }

    $post_id = intval($_POST['post_id']);
    $blog = get_post($post_id);
    if (empty($blog)) {
        $data['status'] = 1;
        $data['message'] = "Post is not found in travpart records.";
        echo json_encode($data);
        exit;
    }

    $sf_agent_id = $blog->post_author;

    if ($sf_agent_id == $sf_user_id) {
        $data['status'] = 1;
        $data['message'] = "You can't follow yourself.";
        echo json_encode($data);
        exit;
    }

    $followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$sf_user_id' AND sf_agent_id = '$sf_agent_id'", ARRAY_A);
    if (!empty($followData)) {
        $sf_id = isset($followData[0]['sf_id']) ? $followData[0]['sf_id'] : '';
        $wpdb->delete('social_follow', array('sf_id' => $sf_id), array('%d'));
        $data['status'] = 0;
        $data['action'] = 'unfollow';
        $data['message'] = "You unfellow agent Successfully!.";
        echo json_encode($data);
        exit;
    } else {
        $wpdb->insert(
            'social_follow',
            array(
                'sf_agent_id' => $sf_agent_id,
                'sf_user_id' => $sf_user_id,
                'sf_created' => date('Y-m-d H:i:s'),
            ),
            array(
                '%d',
                '%d',
                '%s',
            )
        );
        $wpdb->insert(
            'notification',
            array(
                'wp_user_id' => $sf_agent_id,
                'content' => 'Congratulation! You received a new follower'
            ),
            array('%d', '%s')
        );
        $data['status'] = 0;
        $data['action'] = 'follow';
        $data['message'] = "You fellowed This Agent successfully!.";

        //Added by farhan

        $msg = "Congratulation! You received a new follower";

        if (!empty($sf_agent_id)) {

            $user_device_token = to_get_user_device_token($sf_agent_id);

            if (!empty($user_device_token)) {

                sendtomobilefunction($msg, $user_device_token);
            }
        }
        echo json_encode($data);
        exit;
    }
}


if (isset($_POST['action']) && $_POST['action'] == 'social_comments_form') {
    //Checking user is logged in or not
    if (!is_user_logged_in()) {
        $data['status'] = 1;
        $data['message'] = "You must need to login first.";
        echo json_encode($data);
        exit;
    }

    $current_user = wp_get_current_user();
    //Checking comment text is required
    if (!isset($_POST['scm_text']) || $_POST['scm_text'] == '') {
        $data['status'] = 1;
        $data['message'] = "Comment text is required.";
        echo json_encode($data);
        exit;
    }
    $scm_text = $_POST['scm_text'];
    // checking tour
    if (!isset($_POST['scm_tour_id'])) {
        $data['status'] = 1;
        $data['message'] = "You have seleted wrong tour.";
        echo json_encode($data);
        exit;
    }
    $scm_tour_id = filter_input(INPUT_POST, 'scm_tour_id', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1)));
    $comment_parent = filter_input(INPUT_POST, 'comment_parent', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0)));

    //Checking tour is in our DB or not
    $tourData = $wpdb->get_results("SELECT * FROM `wp_tour` WHERE id = '$scm_tour_id'", ARRAY_A);
    if (empty($tourData)) {
        $data['status'] = 1;
        $data['message'] = "Tour is not found in travpart records.";
        echo json_encode($data);
        exit;
    }
    $scm_created = date('Y-m-d H:i:s');
    $scm_user_id = isset($current_user->ID) ? $current_user->ID : '';

    $wpdb->insert(
        'social_comments',
        array(
            'scm_parent'  => $comment_parent,
            'scm_tour_id' => $scm_tour_id,
            'scm_text' => $scm_text,
            'scm_user_id' => $scm_user_id,
            'scm_created' => $scm_created,
        ),
        array(
            '%d',
            '%d',
            '%s',
            '%d',
            '%s',
        )
    );


    $tour_creator_id = $wpdb->get_var("SELECT meta_value FROM `wp_tourmeta` where meta_key='userid' and  tour_id=" . intval($scm_tour_id));
    if (!empty($tour_creator_id) && intval($tour_creator_id) > 0 && $tour_creator_id != $scm_user_id) {
        $wpdb->insert(
            'notification',
            array(
                'wp_user_id' => $tour_creator_id,
                'content' => '<a href="' . site_url() . '/tour-details/?bookingcode=BALI' . intval($scm_tour_id) . '">You received a new comment</a>'
            ),
            array('%d', '%s')
        );
    }

    //Added by farhan

    $msg = "You received a new comment on Tour Post" . intval($scm_tour_id);

    if (!empty($tour_creator_id)) {

        $user_device_token = to_get_user_device_token($tour_creator_id);

        if (!empty($user_device_token)) {

            sendtomobilefunction($msg, $user_device_token);
        }
    }

    $data['status'] = 0;
    $data['message'] = "Comment has been successfully posted!.";
    echo json_encode($data);
    exit;
}

if (isset($_POST['action']) && $_POST['action'] == 'blog_social_comments_form') {
    //Checking user is logged in or not
    if (!is_user_logged_in()) {
        $data['status'] = 1;
        $data['message'] = "You must need to login first.";
        echo json_encode($data);
        exit;
    }

    $current_user = wp_get_current_user();
    //Checking comment text is required
    if (!isset($_POST['scm_text']) || $_POST['scm_text'] == '') {
        $data['status'] = 1;
        $data['message'] = "Comment text is required.";
        echo json_encode($data);
        exit;
    }
    $scm_text = $_POST['scm_text'];
    // checking post
    if (!isset($_POST['post_id'])) {
        $data['status'] = 1;
        $data['message'] = "You have seleted wrong post.";
        echo json_encode($data);
        exit;
    }
    $post_id = intval($_POST['post_id']);

    $blog = get_post($post_id);

    if ($post_id<=0 || empty($blog)) {
        $data['status'] = 1;
        $data['message'] = "Post is not found in travpart records.";
        echo json_encode($data);
        exit;
    }
    $scm_created = date('Y-m-d H:i:s');
    $scm_user_id = isset($current_user->ID) ? $current_user->ID : '';

    $wpdb->insert(
        'blog_social_comments',
        array(
            'post_id' => $post_id,
            'scm_text' => $scm_text,
            'scm_user_id' => $scm_user_id,
            'scm_created' => $scm_created,
        ),
        array(
            '%d',
            '%s',
            '%d',
            '%s',
        )
    );

    $blog_author_id = $blog->post_author;
    if (!empty($blog_author_id) && $blog_author_id != $scm_user_id) {
        $wpdb->insert(
            'notification',
            array(
                'wp_user_id' => $blog_author_id,
                'content' => '<a href="' . get_permalink($post_id) . '">You received a new comment</a>'
            ),
            array('%d', '%s')
        );
    }

    //Added by farhan

    $msg = "You received a new comment on Blog" . intval($post_id);

    if (!empty($blog_author_id)) {

        $user_device_token = to_get_user_device_token($blog_author_id);

        if (!empty($user_device_token)) {

            sendtomobilefunction($msg, $user_device_token);
        }
    }

    $data['status'] = 0;
    $data['message'] = "Comment has been successfully posted!.";
    echo json_encode($data);
    exit;
}


if (isset($_POST['action']) && $_POST['action'] == 'social_connect_l_d') {
    if (!is_user_logged_in()) {
        $data['status'] = 1;
        $data['message'] = "You must need to login first.";
        echo json_encode($data);
        exit;
    }
    $current_user = wp_get_current_user();
    if (isset($_POST['sc_type']) && ($_POST['sc_type'] == '0' || $_POST['sc_type'] == '1')) {

        if (!isset($_POST['sc_tour_id'])) {
            $data['status'] = 1;
            $data['message'] = "You have seleted wrong tour.";
            echo json_encode($data);
            exit;
        }

        $sc_tour_id = $_POST['sc_tour_id'];
        $tour_creator_id = $wpdb->get_var("SELECT meta_value FROM `wp_tourmeta` where meta_key='userid' and  tour_id=".intval($sc_tour_id));
        $tourData = $wpdb->get_results("SELECT * FROM `wp_tour` WHERE id = '$sc_tour_id'", ARRAY_A);
        if (empty($tourData)) {
            $data['status'] = 1;
            $data['message'] = "Tour is not found in travpart records.";
            echo json_encode($data);
            exit;
        }

        $sc_type = $_POST['sc_type'];
        $sc_created = date('Y-m-d H:i:s');
        $sc_user_id = isset($current_user->ID) ? $current_user->ID : '';

        $duplicateDataCheck = $wpdb->get_results("SELECT * FROM `social_connect` WHERE sc_tour_id = '$sc_tour_id' AND sc_user_id = '$sc_user_id'", ARRAY_A);
        if($sc_type==1)
        {
            $typo = 'Sad News you got a dislike for tour';

        }
        else
        {
            $typo ='Congratulation! You tour received a new like';
        }
        $notification = '<a href="'.site_url().'/tour-details/?bookingcode=BALI'.intval($sc_tour_id).'">'.$typo.'</a>';


        if (!empty($duplicateDataCheck)) {
            $duplicateDataCheck = $duplicateDataCheck[0];
            $sc_id = isset($duplicateDataCheck['sc_id']) ? $duplicateDataCheck['sc_id'] : '';
            $wpdb->update(
                    'social_connect', array(
                'sc_tour_id' => $sc_tour_id,
                'sc_type' => $sc_type,
                'sc_user_id' => $sc_user_id,
                'sc_created' => $sc_created,
                    ), array('sc_id' => $sc_id), array(
                '%d',
                '%s',
                '%d',
                '%s',
                    ), array('%d')
            );

         if(!empty($tour_creator_id)){

        $user_device_token = to_get_user_device_token($tour_creator_id);

        if(!empty($user_device_token)) {

        sendtomobilefunction($typo,$user_device_token);

        }
        }
        

        $wpdb->insert('notification', array(
            'wp_user_id'=>$tour_creator_id,
            'content'=> $notification,'identifier'=>'new_activity_on_tour'),
            array('%d','%s','%s'));

        } else {

          
             $wpdb->insert('notification', array(
            'wp_user_id'=>$tour_creator_id,
            'content'=> $notification,'identifier'=>'new_activity_on_tour'),
            array('%d','%s','%s'));

         if(!empty($tour_creator_id)){

        $user_device_token = to_get_user_device_token($tour_creator_id);

        if(!empty($user_device_token)) {

        sendtomobilefunction($typo,$user_device_token);

        }
        }

            $wpdb->insert(
                    'social_connect', array(
                'sc_tour_id' => $sc_tour_id,
                'sc_type' => $sc_type,
                'sc_user_id' => $sc_user_id,
                'sc_created' => $sc_created,
                    ), array(
                '%d',
                '%s',
                '%d',
                '%s',
                    )
            );


        }
        $data['status'] = 0;
        $data['message'] = "Wow! Perfect thank you very much for your action.";
        echo json_encode($data);
        exit;
    } else {
        $data['status'] = 1;
        $data['message'] = "Opps, Something want wrong.";
        echo json_encode($data);
        exit;
    }
}

if (isset($_POST['action']) && $_POST['action'] == 'blog_social_connect_l_d') {
    if (!is_user_logged_in()) {
        $data['status'] = 1;
        $data['message'] = "You must need to login first.";
        echo json_encode($data);
        exit;
    }
    $current_user = wp_get_current_user();
    if (isset($_POST['sc_type']) && ($_POST['sc_type'] == '0' || $_POST['sc_type'] == '1')) {

        if (!isset($_POST['post_id'])) {
            $data['status'] = 1;
            $data['message'] = "You have seleted wrong post.";
            echo json_encode($data);
            exit;
        }

        $post_id = $_POST['post_id'];

        $sc_type = $_POST['sc_type'];
        $sc_created = date('Y-m-d H:i:s');
        $sc_user_id = isset($current_user->ID) ? $current_user->ID : '';

        $duplicateDataCheck = $wpdb->get_results("SELECT * FROM `blog_social_connect` WHERE post_id = '$post_id' AND sc_user_id = '$sc_user_id'", ARRAY_A);

        if (!empty($duplicateDataCheck)) {
            $duplicateDataCheck = $duplicateDataCheck[0];
            $sc_id = isset($duplicateDataCheck['sc_id']) ? $duplicateDataCheck['sc_id'] : '';
            $wpdb->update(
                'blog_social_connect',
                array(
                    'post_id' => $post_id,
                    'sc_type' => $sc_type,
                    'sc_user_id' => $sc_user_id,
                    'sc_created' => $sc_created,
                ),
                array('sc_id' => $sc_id),
                array(
                    '%d',
                    '%s',
                    '%d',
                    '%s',
                ),
                array('%d')
            );
        } else {

            $wpdb->insert(
                'blog_social_connect',
                array(
                    'post_id' => $post_id,
                    'sc_type' => $sc_type,
                    'sc_user_id' => $sc_user_id,
                    'sc_created' => $sc_created,
                ),
                array(
                    '%d',
                    '%s',
                    '%d',
                    '%s',
                )
            );
        }
        $data['status'] = 0;
        $data['message'] = "Wow! Perfect thank you very much for your action.";
        echo json_encode($data);
        exit;
    } else {
        $data['status'] = 1;
        $data['message'] = "Opps, Something want wrong.";
        echo json_encode($data);
        exit;
    }
}


if (isset($_POST['action']) && $_POST['action'] == 'ratting_form') {
    $current_user = wp_get_current_user();
    $userName = isset($current_user->user_login) ? $current_user->user_login : '';
    $userID = isset($current_user->ID) ? $current_user->ID : '';

    $af_tour_id = isset($_POST['af_tour_id']) ? $_POST['af_tour_id'] : '';

    $tourBookedData = $wpdb->get_results("SELECT * FROM wp_tour_sale WHERE tour_id = '$af_tour_id' AND userid = '$userID'", ARRAY_A);

    if (empty($tourBookedData)) {
        $redirectUrl = site_url() . '/tour-feedback/?tour_id=' . $af_tour_id . '&error_message=You are not authorized for feedback.';
        header('Location:' . $redirectUrl);
    }

    $skills = isset($_POST['skills']) ? $_POST['skills'] : 0;
    $quality_of_work = isset($_POST['quality_of_work']) ? $_POST['quality_of_work'] : 0;
    $availability = isset($_POST['availability']) ? $_POST['availability'] : 0;
    $schedule = isset($_POST['schedule']) ? $_POST['schedule'] : 0;
    $communication = isset($_POST['communication']) ? $_POST['communication'] : 0;
    $co_opration = isset($_POST['co_opration']) ? $_POST['co_opration'] : 0;

    $af_ratting = ($co_opration + $communication + $schedule + $availability + $quality_of_work + $skills) / 6;
    
    $ceilRatting = ceil($af_ratting);
    if ($ceilRatting == '1') {
        $earnPoints = '20';
    } else if ($ceilRatting == '2') {
        $earnPoints = '30';
    } else if ($ceilRatting == '3') {
        $earnPoints = '40';
    } else if ($ceilRatting == '4') {
        $earnPoints = '50';
    } else {
        $earnPoints = '60';
    }
    
    $af_agent_user_name = $userName;
    $af_agent_user_id = $userID;
//    $af_ratting = isset($_POST['stars']) ? $_POST['stars'] : '';
    $af_comments = isset($_POST['af_comments']) ? $_POST['af_comments'] : '';


    $wpdb->insert(
            'vouchers_user_earn_points', array(
        'vuep_user_id' => $af_agent_user_id,
        'vuep_points' => $earnPoints,
        'vuep_points_from' => '0',
        'vuep_created' => date('Y-m-d H:i:s'),
            ), array(
        '%d',
        '%s',
        '%s',
        '%s',
            )
    );
    
    $wpdb->insert(
            'agent_feedback', array(
        'af_agent_user_name' => $af_agent_user_name,
        'af_agent_user_id' => $af_agent_user_id,
        'af_ratting' => $af_ratting,
        'af_tour_id' => $af_tour_id,
        'af_comments' => $af_comments,
            ), array(
        '%s',
        '%d',
        '%s',
        '%d',
        '%s',
            )
    );
    $redirectUrl = site_url() . '/user/' . $af_agent_user_name . '/?profiletab=bookingCodeList';

    header('Location:' . $redirectUrl);
}



if (isset($_POST['action']) && $_POST['action'] == 'client_ratting_form') {

    $current_user = wp_get_current_user();
    $userName = isset($current_user->user_login) ? $current_user->user_login : '';
    $userID = isset($current_user->ID) ? $current_user->ID : '';

    $af_tour_id = isset($_POST['ac_tour_id']) ? $_POST['ac_tour_id'] : '';

    $tourBookedData = $wpdb->get_results("SELECT * FROM wp_tour_sale WHERE tour_id = '$af_tour_id' AND userid = '$userID'", ARRAY_A);

    if (empty($tourBookedData)) {
        $redirectUrl = site_url() . '/tour-feedback/?tour_id=' . $af_tour_id . '&error_message=You are not authorized for feedback.';
        header('Location:' . $redirectUrl);
    }

    $skills = isset($_POST['skills']) ? $_POST['skills'] : 0;
    $quality_of_work = isset($_POST['quality_of_work']) ? $_POST['quality_of_work'] : 0;
    $availability = isset($_POST['availability']) ? $_POST['availability'] : 0;
    $schedule = isset($_POST['schedule']) ? $_POST['schedule'] : 0;
    $communication = isset($_POST['communication']) ? $_POST['communication'] : 0;
    $co_opration = isset($_POST['co_opration']) ? $_POST['co_opration'] : 0;

    $af_ratting = ($co_opration + $communication + $schedule + $availability + $quality_of_work + $skills) / 6;
    $af_agent_user_name = $userName;
    $af_agent_user_id = $userID;
//    $af_ratting = isset($_POST['stars']) ? $_POST['stars'] : '';
    $af_comments = isset($_POST['ac_comments']) ? $_POST['ac_comments'] : '';


    $ceilRatting = ceil($af_ratting);
    if ($ceilRatting == '1') {
        $earnPoints = '20';
    } else if ($ceilRatting == '2') {
        $earnPoints = '30';
    } else if ($ceilRatting == '3') {
        $earnPoints = '40';
    } else if ($ceilRatting == '4') {
        $earnPoints = '50';
    } else {
        $earnPoints = '60';
    }
    
    $wpdb->insert(
            'vouchers_user_earn_points', array(
        'vuep_user_id' => $af_agent_user_id,
        'vuep_points' => $earnPoints,
        'vuep_points_from' => '0',
        'vuep_created' => date('Y-m-d H:i:s'),
            ), array(
        '%d',
        '%s',
        '%s',
        '%s',
            )
    );
    
    
    $wpdb->insert(
            'agent_client_feedback', array(
        'ac_agent_user_name' => $af_agent_user_name,
        'ac_agent_user_id' => $af_agent_user_id,
        'ac_ratting' => $af_ratting,
        'ac_tour_id' => $af_tour_id,
        'ac_comments' => $af_comments,
            ), array(
        '%s',
        '%d',
        '%s',
        '%d',
        '%s',
            )
    );
    $redirectUrl = site_url() . '/user/' . $af_agent_user_name . '/?profiletab=bookingCodeList';

    header('Location:' . $redirectUrl);
}


if (isset($_POST['action']) && $_POST['action'] == 'customer_request') {

    $csa_tmp_user_name = isset($_POST['csa_tmp_user_name']) ? $_POST['csa_tmp_user_name'] : '';
    $userData = $wpdb->get_results("SELECT userid FROM user WHERE username = '$csa_tmp_user_name'", ARRAY_A);

    $chatUserId = isset($userData[0]['userid']) ? $userData[0]['userid'] : '';

    $toAgentMailBody = file_get_contents('https://www.travpart.com/mail/customer_request.html');

    $csa_user_name = isset($_POST['csa_user_name']) ? $_POST['csa_user_name'] : '';
    $csa_user_email = isset($_POST['csa_user_email']) ? $_POST['csa_user_email'] : '';
    $csa_subject = isset($_POST['csa_subject']) ? $_POST['csa_subject'] : '';
    $csa_destination_plan = htmlspecialchars(isset($_POST['csa_destination_plan']) ? $_POST['csa_destination_plan'] : '');
    $csa_message = isset($_POST['csa_message']) ? $_POST['csa_message'] : '';
    $csa_question_cat = isset($_POST['csa_question_cat']) ? $_POST['csa_question_cat'] : '';

    $csa_agent_name=filter_input(INPUT_POST, 'csa_agent_name', FILTER_SANITIZE_STRING);
    if($csa_agent_name!=='0' && empty($csa_agent_name)) {
        echo json_encode(array('status'=>0,'message'=>'OPPS!Something went wrong.'));
        exit;
    }
    
    $csa_agents=array();
    if($csa_agent_name=='0') {
        $csa_agents=$wpdb->get_results("SELECT username,email FROM `user` LEFT JOIN `sessions` ON `sessions`.`id` = (
            SELECT `id` from `sessions` where `user_id` = `user`.`userid` ORDER BY `id` DESC LIMIT 1)
            WHERE access=1 AND activated=1 ORDER BY last_activity DESC LIMIT 10", ARRAY_A);
        $csa_agent_name=$csa_agents[0]['username'];
    }

    $agentInfo=$wpdb->get_row("SELECT `userid`,`email` FROM `user` WHERE `username`='{$csa_agent_name}'");
    if(empty($agentInfo->userid)){
        echo json_encode(array('status'=>0,'message'=>'OPPS!Something went wrong.'));
        exit;
    }
    $csa_agent_email = $agentInfo->email;
    $agentUserid = $agentInfo->userid;

    $toAgentMailBody = str_replace("_NAME_", ucfirst($csa_agent_name), $toAgentMailBody);
    $toAgentMailBody = str_replace("_USERNAME_", ucfirst($csa_user_name), $toAgentMailBody);
    //$toAgentMailBody = str_replace("_USER_EMAIL_", ucfirst($csa_user_email), $toAgentMailBody);
    $toAgentMailBody = str_replace("_DESTINATION_", ucfirst($csa_destination_plan), $toAgentMailBody);
    $toAgentMailBody = str_replace("_MESSAGE_", ucfirst($csa_message), $toAgentMailBody);
    $subject = "Inquery From Travpart";

    $wpdb->insert(
        'contact_sales_agent', array(
        'csa_user_name' => $csa_user_name,
        'csa_user_email' => $csa_user_email,
        'csa_subject' => $csa_subject,
        'csa_destination_plan' => $csa_destination_plan,
        'csa_agent_email' => $csa_agent_email,
        'csa_message' => $csa_message,
        'csa_question_cat' => $csa_question_cat,
        'csa_created' => date('Y-m-d H:i:s'),
            ), array(
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s'
            )
    );

    if ($wpdb->insert_id) {
        
        $wpdb->query("insert into `chat` (message, userid, chat_date,c_to,c_from,type,attachment_type) values ('".trim($toAgentMailBody)."' , '{$chatUserId}', NOW(),'{$agentUserid}','{$chatUserId}','','')");

        $data['status'] = 1;
        $data['agentUserid'] = $agentUserid;
        $data['ticket_id'] = $wpdb->insert_id;
        $data['message'] = "Your request has been received, Our travel advisor reach you soon at your email address.";
        echo json_encode($data);
        //sendingMail($csa_agent_email, $subject, $toAgentMailBody);
        include("potential-sales-agents-email-content.php");
        
        wp_mail($csa_agent_email, 'A travel customer for you from travpart.com', str_replace('[name]', $csa_agent_name, $potential_sale_agent_email_content));
        $potential_sale_agents=$wpdb->get_results("SELECT `name`,`email`,`create_time` FROM `wp_potential_sale_agent` WHERE location LIKE '{$csa_destination_plan}'");
        foreach($potential_sale_agents as $t) {
            $lasted_email_send_time=$wpdb->get_var("SELECT `csa_created` FROM `contact_sales_agent` WHERE `csa_created`>'{$t->create_time}' ORDER BY `csa_created` ASC LIMIT 1");
            $create_time=strtotime($t->create_time);
            if(!empty($lasted_email_send_time) && $create_time<strtotime($lasted_email_send_time)) {
                if((time()-strtotime($lasted_email_send_time))>15*24*60*60)
                    continue;
            }
            $enable_notification=$wpdb->get_var("SELECT subscribe FROM `unsubscribed_emails` WHERE email='{$t->email}' AND type=4");
            if (is_null($enable_notification) || $enable_notification==1) {
                $unsubscribe_link=home_url('unsubscribe').'/?qs='.base64_encode($t->email.'||'.md5('EC'.$t->email.$t->create_time));
                wp_mail($t->email, 'A travel customer for you from travpart.com', str_replace(array('[name]','UNSUBSCRIBE_LINK'), array($t->name,$unsubscribe_link), $potential_sale_agent_email_content));
            }
        }
        foreach($csa_agents as $key=>$t) {
            if($key==0)
                continue;
            wp_mail($t['email'], 'A travel customer for you from travpart.com', str_replace('[name]',$t['username'],$potential_sale_agent_email_content));
        }
        exit;
    } else {
        $data['status'] = 0;
        $data['message'] = "OPPS!Something want wrong.";
        echo json_encode($data);
        exit;
    }
}


if (isset($_POST['action']) && $_POST['action'] == 'create_ticket') {
//    p($_POST);

    $toUserMailBody = file_get_contents('https://www.travpart.com/mail/create_ticket_user.html');
    $cs_user_id = isset($_POST['cs_user_id']) ? $_POST['cs_user_id'] : '';
    $cs_user_name = isset($_POST['cs_user_name']) ? $_POST['cs_user_name'] : '';
    $toUserMailBody = str_replace("_NAME_", ucfirst($cs_user_name), $toUserMailBody);
    $cs_type = isset($_POST['cs_type']) ? $_POST['cs_type'] : '';
    $cs_description = isset($_POST['cs_description']) ? $_POST['cs_description'] : '';
    $subject = "Request receive by Travpart Help Center: " . $cs_type;

    $userType = 'Guest';
    $userEmail = '';

    $localUserData = $wpdb->get_results("SELECT * FROM user WHERE username = '$cs_user_name'", ARRAY_A);
    if (!empty($localUserData)) {
        $localUserData = $localUserData[0];
        $userEmail = isset($localUserData['email']) ? $localUserData['email'] : '';
        $access = $localUserData['access'];
        if ($access == 2) {
            $userType = 'Customer';
        } else if ($access == 1) {
            $userType = 'Agent';
        }
    }

    $toAdminMailBody = file_get_contents('https://www.travpart.com/mail/create_ticket_admin.html');
    $toAdminMailBody = str_replace("_UserName_", ucfirst($cs_user_name), $toAdminMailBody);
    $toAdminMailBody = str_replace("_SUBJECT_", $cs_type, $toAdminMailBody);
    $toAdminMailBody = str_replace("_UserType_", $userType, $toAdminMailBody);
    $toAdminMailBody = str_replace("_UserEmail_", $userEmail, $toAdminMailBody);
    $toAdminMailBody = str_replace("_DESC_", $cs_description, $toAdminMailBody);


    $wpdb->insert(
            'contact_support', array(
        'cs_user_id' => $cs_user_id,
        'cs_user_name' => $cs_user_name,
        'cs_type' => $cs_type,
        'cs_description' => $cs_description,
        'cs_created' => date('Y-m-d H:i:s'),
        'cs_updated' => date('Y-m-d H:i:s'),
            ), array(
        '%d',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s'
            )
    );
    if ($wpdb->insert_id) {
        $toUserMailBody = str_replace("_TICKET_ID_", '000' . $wpdb->insert_id, $toUserMailBody);
        $toAdminMailBody = str_replace("_TICKET_ID_", '000' . $wpdb->insert_id, $toAdminMailBody);
        sendingMail($userEmail, $subject, $toUserMailBody);
        sendingMail('contact@tourfrombali.com', 'New Support ticket received', $toAdminMailBody);
        $data['status'] = 1;
        $data['ticket_id'] = $wpdb->insert_id;
        $data['message'] = "Your ticket has been created success, we will reach you in next 2 working days.";
        echo json_encode($data);
        exit;
    } else {
        $data['status'] = 0;
        $data['message'] = "OPPS!Something want wrong.";
        echo json_encode($data);
        exit;
    }
}

if (isset($_POST['action']) && $_POST['action'] == 'ticket_comment') {
    $cscr_attachment = '';
    $cscr_contact_support_id = isset($_POST['cscr_contact_support_id']) ? $_POST['cscr_contact_support_id'] : '';
    $cscr_user_id = isset($_POST['cscr_user_id']) ? $_POST['cscr_user_id'] : '';
    $cscr_user_name = isset($_POST['cscr_user_name']) ? $_POST['cscr_user_name'] : '';
    $cscr_description = isset($_POST['cscr_description']) ? $_POST['cscr_description'] : '';
    $uploads_dir = 'support_upload/';
    if (isset($_FILES["cscr_attachment"]["tmp_name"]) && $_FILES["cscr_attachment"]["tmp_name"] != '') {
        $tmp_name = $_FILES["cscr_attachment"]["tmp_name"];
        $name = 'support_' . time() . basename($_FILES["cscr_attachment"]["name"]);
        $fullPath = $uploads_dir . $name;
        move_uploaded_file($tmp_name, $fullPath);
        $cscr_attachment = 'https://www.travpart.com/English/' . $fullPath;
    }
    $wpdb->insert(
            'contact_support_comment_rel', array(
        'cscr_contact_support_id' => $cscr_contact_support_id,
        'cscr_user_id' => $cscr_user_id,
        'cscr_user_name' => $cscr_user_name,
        'cscr_description' => $cscr_description,
        'cscr_attachment' => $cscr_attachment,
        'cscr_created' => date('Y-m-d H:i:s'),
        'cscr_modified' => date('Y-m-d H:i:s'),
            ), array(
        '%d',
        '%d',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s'
            )
    );
    if (isset($_POST['from']) && $_POST['from'] == 'admin') {
        header('Location:https://www.travpart.com/English/wp-admin/options-general.php?page=ticket-details&ticket_id=' . $cscr_contact_support_id);
    } else {
        header('Location:https://www.travpart.com/English/create-ticket/?ticketid=' . $cscr_contact_support_id);
    }
    exit;
}

if (isset($_POST['action']) && $_POST['action'] == 'ticket_clost') {
    $ticket_id = isset($_POST['ticket_id']) ? $_POST['ticket_id'] : '';

    $wpdb->update(
            'contact_support', array(
        'cs_status' => '1',
            ), array('cs_id' => $ticket_id)
    );

    $data['status'] = 1;
    $data['message'] = "Success.";
    echo json_encode($data);
    exit;
}

if (isset($_POST['action']) && $_POST['action'] == 'ticket_reopen') {
    $ticket_id = isset($_POST['ticket_id']) ? $_POST['ticket_id'] : '';

    $wpdb->update(
            'contact_support', array(
        'cs_status' => '0',
            ), array('cs_id' => $ticket_id)
    );

    $data['status'] = 1;
    $data['message'] = "Success.";
    echo json_encode($data);
    exit;
}

function sendingMail($to, $subject, $message) {

    $headers = "From: no-reply@travpart.com\r\n";
    $headers .= "Reply-To: no-reply@travpart.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

    $key = "Tt3At58P6ZoYJ0qhLvqdYyx21";
    $postdata = array('to' => $to,
        'subject' => $subject,
        'message' => $message);
    $url = "https://www.tourfrombali.com/api.php?action=sendmail&key={$key}";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $data = curl_exec($ch);
    if (curl_errno($ch) || $data == FALSE) {
        curl_close($ch);
        return FALSE;
    } else {
        curl_close($ch);
        return TRUE;
    }

//    mail($to, $subject, $message, $headers);
}
