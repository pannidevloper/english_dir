<?php
get_header();
/* Template Name: BIO info */

global $wpdb;
$username = filter_input(INPUT_GET, 'user', FILTER_SANITIZE_STRING);
$userID = isset($current_user->ID) ? $current_user->ID : '';

$rattingDataByTour = $wpdb->get_results("SELECT * FROM agent_feedback WHERE af_agent_user_name = '$username'", ARRAY_A);


$rattingDataByTourDetails = $wpdb->get_results("SELECT * FROM agent_feedback LEFT JOIN wp_tour ON wp_tour.id = agent_feedback.af_tour_id WHERE af_agent_user_name = '$username'", ARRAY_A);


/* echo "<pre>";
  print_r($rattingDataByTourDetails);
  exit; */
$totalRatinng = 0;
$avgRatting = 0;
$totalEntry = 0;
if (!empty($rattingDataByTour)) {
    $totalEntry = count($rattingDataByTour);
}
foreach ($rattingDataByTour as $ratings) {
    $totalRatinng = $totalRatinng + $ratings['af_ratting'];
    $avgRatting = $totalRatinng / $totalEntry;
}
$avgRatting = ceil($avgRatting);
$user = get_user_by('login', $username);
if (empty($username) || !$user)
    $username = wp_get_current_user()->user_login;

$userinfo = $wpdb->get_row("SELECT * FROM `user` WHERE (access=1 OR access=2) AND `username`='{$username}'");

if (empty($userinfo)) {

    get_template_part('tpl-pg404', 'pg404');
} else {
    ?>
    <style>
        .latest-item {
            margin-bottom: 30px; }

        .latest-item .item-title {
            min-height: 80px; }

        .trav-rating {
            display: inline-block;
            vertical-align: middle; }
        .trav-rating > span {
            border-radius: 2px;
            background-color: #fc8c14;
            font-size: 14px;
            line-height: 1;
            padding: 2px 4px;
            color: #fff;
            text-align: center; }
        .trav-rating > i {
            line-height: 21px;
            display: inline-block;
            color: #fc8c14;
            margin-left: 5px;
            margin-right: 5px; }
        .trav-rating > small {
            font-size: 14px;
            line-height: 21px;
            color: #666666; }

        .average-feedback {
            margin-bottom: 60px; }
        .average-feedback header {
            background-color: #f9f9f9;
            padding: 25px;
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
            border: 1px solid #dcdcdc; }

        .feedback-tilte {
            font-size: 22px;
            font-weight: 400;
            line-height: 1.36;
            color: #333333; }

        .feedback-item {
            border: 1px solid #dcdcdc;
            border-top: none;
            background: #fff;
            padding: 35px 185px 35px 25px;
            position: relative; }
        .feedback-item .feedback-price {
            position: absolute;
            width: 185px;
            text-align: center;
            right: 0;
            top: 35px; }
        .feedback-item .feedback-price strong {
            display: block;
            font-size: 18px;
            font-weight: bold;
            color: #333333; }
        .feedback-item .feedback-price small {
            font-size: 14px;
            color: #999999; }
        .feedback-item h4 {
            color: #66cccc;
            font-size: 18px;
            margin-bottom: 10px;
            margin-top: 0; }
        .feedback-item h4 a {
            color: #66cccc; }
        .feedback-item .feedback-detail {
            margin-top: 10px;
            margin-bottom: 10px; }

        .profile-pg .avatar {
            width: 150px;
            height: 150px;
            background-position: center center;
            background-size: cover;
            border-radius: 50%; }

        .latest-rating-wrap {
            margin-bottom: 10px;
            margin-left: 10px; 
        }
        .fa-star-unchecked{
            color: black;
        }
		
		.profile-total-sales {
			display: flex;
		}
		.profile-total-sales .sale-item {
			padding: 35px;
			margin: 0 15px;
			text-align: center;
		}
		.profile-total-sales .sale-value {
			font-size: 24px;
			font-weight: bold;
		}
		.profile-total-sales .sale-label {
			font-size: 16px;
			font-weight: normal;
			color: #999999;
		}
    </style>
    <div class="profile-pg mt-4">
        <div class="border-box mb-4">
            <div class="p-3 d-flex flex-column flex-sm-row align-items-center">
                <div class="avatar" style="background-image: url(<?php echo (!empty(um_get_avatar_url(get_avatar($user->ID, 150))))?um_get_avatar_url(get_avatar($user->ID, 150)):'https://www.travpart.com/Chinese/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg'; ?>)"></div>
                <div class="flex-fill">
                    <p class="d-flex align-items-center"><strong class="font-lg" style="margin-right: 5px;"><?php echo $userinfo->uname; ?></strong> 
                        <span class="trav-rating"><span><?php echo $avgRatting . '.0'; ?></span>
                            <i>
                                <?php
                                if ($avgRatting == 1) {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star fa-star-unchecked"></i>
                                    <i class="fa fa-star fa-star-unchecked"></i>
                                    <i class="fa fa-star fa-star-unchecked"></i>
                                    <i class="fa fa-star fa-star-unchecked"></i>
                                    <?php
                                } else if ($avgRatting == 2) {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star fa-star-unchecked"></i>
                                    <i class="fa fa-star fa-star-unchecked"></i>
                                    <i class="fa fa-star fa-star-unchecked"></i>
                                    <?php
                                } else if ($avgRatting == 3) {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star fa-star-unchecked"></i>
                                    <i class="fa fa-star fa-star-unchecked"></i>
                                    <?php
                                } else if ($avgRatting == 4) {
                                    ?>

                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star fa-star-unchecked"></i>
                                    <?php
                                } else if ($avgRatting == 5) {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <?php
                                } else {
                                    echo "No Rating yet!";
                                }
                                ?>
                            </i>
                            <small> (<?php echo $totalEntry; ?> reviews)</small>
                        </span>
                    </p>
                    <?php
                    if (!empty($userinfo->region)) {
                        date_default_timezone_set($userinfo->timezone);
                        ?>
                        <p><i class="fa fa-map-marker"></i> <strong><?php echo $userinfo->region; ?></strong> - <?php echo date('g:i A'); ?> local time</p>
                    <?php } ?>
                    <p class="mb-0"><span class="user-type"><?php echo $userinfo->access == 1 ? 'Travel Advisor' : 'Client'; ?></span></p>
                </div>
            </div>
            <div class="p-3">
                <h4>Contact infomation</h4>
                <div class="d-flex flex-row profile-contact">
                    <?php //Hide
					/*<div class="mr-3 mr-sm-4"><span>Phone Number</span><br><span><?php echo $userinfo->phone; ?></span></div> */ ?>
                    <div class="mr-3 mr-sm-4"><span>User ID</span><br><span><?php echo $userinfo->userid; ?></span></div>
                    <div class="mr-3 mr-sm-4"><span>Age</span><br><span>18</span></div>
                    <div class="mr-3 mr-sm-4"><span>Gender</span><br><span>Female</span></div>
                    <?php //Hide
					/*
					<div class="mr-3 mr-sm-4"><span>Email</span><br><span><?php echo $userinfo->email; ?></span></div> */ ?>

                </div>
            </div>
            <div class="p-3 border-top profile-desc">
                <p>
                    <?php echo get_user_meta($user->ID, 'description', true); ?>
                </p>
            </div>
			
			<div class="border-top profile-total-sales">
				<div class="sale-item">
					<?php
					$sales_earned=$wpdb->get_var("SELECT SUM(t.total) FROM wp_tour t INNER JOIN wp_tourmeta m
													ON (t.id=m.`tour_id` AND m.meta_key='userid' AND m.meta_value='{$user->ID}') WHERE t.confirm_payment=1");
					$commission_earned=$wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE userid='{$userinfo->userid}'");
					if(floatval($sales_earned)<=0)
						$sales_earned='0.00';
					else
						$sales_earned=round($sales_earned*get_option('_cs_currency_USD'),2);
					if(floatval($commission_earned)<=0)
						$commission_earned='0.00';
					?>
					<div class="sale-value">$<?php echo $sales_earned; ?></div>
					<div class="sale-label">Total Sales Earned</div>
				</div>
				<div class="sale-item">
					<div class="sale-value">$<?php echo $commission_earned; ?></div>
					<div class="sale-label">Commission Earned</div>
				</div>
			</div>
			
			<div class="share-box text-center mt-5">
				<!--  css & js -->
				<link rel="stylesheet" href="/English/wp-content/themes/bali/css/social-share-media.css">
                <script src="/English/wp-content/themes/bali/js/social-share-media.js"></script>
                <script>
				const socialmediaurls = GetSocialMediaSiteLinks_WithShareLinks({
					'url': '<?php echo home_url("/bio/?user={$username}"); ?>',
					'title': 'I recommend you use the travcust app for making money to sell tour packages. Download and get $5 credit.'
				});
				function gotoshare(medianame)
				{
					if (medianame == 'whatsapp') {
						window.open('https://web.whatsapp.com/send?text=' + encodeURIComponent('I recommend you use the travcust app for making money to sell tour packages. Check <?php echo home_url("/bio/?user={$username}"); ?> and get $5 credit.'));
					} else {
						window.open(socialmediaurls[medianame]);
					}
				}
				</script>
				<div class="marvel-shareoption-view">
					<a href="#" onclick="gotoshare('google.plus')"><span><img src="https://www.travpart.com/Chinese/wp-content/themes/bali/images/images/google-plus.png" alt="google-plus"></span></a>
					<a href="#" onclick="gotoshare('pinterest')"><span><img src="https://www.travpart.com/Chinese/wp-content/themes/bali/images/images/pinterest.png" alt="pinterest"></span></a>
					<a href="#" onclick="gotoshare('facebook')"><span><img src="https://www.travpart.com/Chinese/wp-content/themes/bali/images/images/facebook-logo.png" alt="facebook"></span></a>
					<a href="#" onclick="gotoshare('whatsapp')"><span><img width=24px src="https://www.travpart.com/English/wp-content/uploads/2018/10/whatsapp.png" alt="whatsapp"></span></a>
				</div>
			</div>


        </div>
        <div class="average-feedback">
            <header class="d-flex">
                <div class="feedback-tilte mr-auto">Average Feedback</div>
                <div class="feedback-filter">
                    <select class="form-control">
                        <option value="Newest first" selected>Newest first</option>
                    </select>
                </div>
            </header>
            <div class="feedback-items">
                <?php
                foreach ($rattingDataByTourDetails as $ratingsDetails) {
                    $avgRatting = isset($ratingsDetails['af_ratting']) ? $ratingsDetails['af_ratting'] : '';
                    ?>
                    <div class="feedback-item">
                        <h4><a href="#">Booking Code：<strong>BALI<?php echo isset($ratingsDetails['id']) ? $ratingsDetails['id'] : ''; ?></strong></a></h4>
                        <div class="feedbak-rating-wrap">
                            <span class="trav-rating"><span><?php echo isset($ratingsDetails['af_ratting']) ? $ratingsDetails['af_ratting'] . '.0' : ''; ?></span>
                                <i>

                                    <?php
                                    if ($avgRatting == 1) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <?php
                                    } else if ($avgRatting == 2) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <?php
                                    } else if ($avgRatting == 3) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <?php
                                    } else if ($avgRatting == 4) {
                                        ?>

                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star fa-star-unchecked"></i>
                                        <?php
                                    } else if ($avgRatting == 5) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <?php
                                    } else {
                                        echo "No Rating yet!";
                                    }
                                    ?>

                                </i>
                                <small> Dec 2018</small>
                            </span>
                        </div>
                        <div class="feedback-detail">
                            <?php echo isset($ratingsDetails['af_comments']) ? $ratingsDetails['af_comments'] : ''; ?>
                        </div>
                        <div class="exp-date">
                            Experience Date: <?php echo isset($ratingsDetails['tour_date']) ? $ratingsDetails['tour_date'] : ''; ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

    </div>
    <?php
}
get_footer();
?>