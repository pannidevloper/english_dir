
<?php
/*
Template Name: Profile Page
*/

get_header();

if (!is_user_logged_in() || wp_get_current_user()->ID != um_profile_id() || isset($_GET['view_as_others'])) {
    include get_template_directory().'/profile_viewer.php';
}
else {

    if (have_posts()) {
        while (have_posts()) {
            the_post();
            echo '<h1>' . get_the_title() . '</h1>';
            the_content();
        }
    }
    else {
        get_template_part('tpl-pg404', 'pg404');
    }
}

get_footer();
