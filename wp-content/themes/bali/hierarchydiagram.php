<?php

/* Template Name: hierarchydiagram */
get_header();
?>
<link href="https://unpkg.com/treeflex/dist/css/treeflex.css" rel="stylesheet" />
<div class="tf-tree example">
  <ul>
    <li>
      <span class="tf-nc">Homepage</span>
      <ul>
        <li>
          <span class="tf-nc">Vacation</span>
          <ul>
            <li>
            	<span class="tf-nc">Tour Detail</span>
            </li>
            <li>
              <span class="tf-nc">Total Cost</span>
            </li>
          </ul>
          <ul>
	        <li><span class="tf-nc">Confirm Payment Page</span></li>
	      </ul>
        </li>
        <li>
          <span class="tf-nc">Tour Search</span>
          </li>
        
        <li>
          <span class="tf-nc">Login</span>
          <ul>
            <li><span class="tf-nc">7</span></li>
            <li><span class="tf-nc">8</span></li>
            <li><span class="tf-nc">7</span></li>
            <li><span class="tf-nc">8</span></li>
            <li><span class="tf-nc">7</span></li>
            <li><span class="tf-nc">8</span></li>
            <li><span class="tf-nc">7</span></li>
            <li>
            	<span class="tf-nc">8</span>
            	 <ul>
		            <li>
		            	<span class="tf-nc">Tour Detail</span>
		            </li>
		            <li>
		              <span class="tf-nc">Total Cost</span>
		            </li>
		          </ul>
            </li>
          </ul>
          <ul>
            <li><span class="tf-nc">7</span></li>
            <li><span class="tf-nc">8</span></li>
            <li><span class="tf-nc">7</span></li>
            <li><span class="tf-nc">8</span></li>
            <li><span class="tf-nc">7</span></li>
            <li><span class="tf-nc">8</span></li>
            <li><span class="tf-nc">7</span></li>
            <li><span class="tf-nc">8</span></li>
          </ul>
        </li>
        
        <li>
          <span class="tf-nc">About us</span>
        </li>
      </ul>
    </li>
  </ul>
</div>

<?php 
	get_footer();
?>