<?php
/*
Template Name: Unsubscribe
*/
global $wpdb;
$qs=filter_input(INPUT_GET, 'qs', FILTER_SANITIZE_STRING);
if($qs=base64_decode($qs)) {
    $tmp=explode('||',$qs);
    if(is_email($tmp[0])) {
        $email=$tmp[0];
        $code=$tmp[1];
    }
}

if(empty($email) || empty($code)) {
    exit(wp_redirect(home_url()));
}

$userinfo=$wpdb->get_row("SELECT username,email,user_registered FROM user WHERE email='{$email}'");
$psuser=$wpdb->get_row("SELECT email,create_time FROM `wp_potential_sale_agent` WHERE email='{$email}'");

if(!empty($userinfo) && $code==md5('EC'.$userinfo->username.$userinfo->email.$userinfo->user_registered)) {
    setcookie('email', $userinfo->email, (time() + 86400), '/English/', '.travpart.com');
    exit(wp_redirect(home_url('email-preferences')));
}
else if(!empty($psuser) && $code==md5('EC'.$psuser->email.$psuser->create_time)) {
    setcookie('email', $psuser->email, (time() + 86400), '/English/', '.travpart.com');
    exit(wp_redirect(home_url('email-preferences')));
}
else {
    exit(wp_redirect(home_url()));
}