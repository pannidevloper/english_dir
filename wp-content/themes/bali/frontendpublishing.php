<style>
	p.copyright_laws{
		position: absolute;
	    right: 0;
	    width: 32%;
	    margin-top: 19px;
	}
</style>

<?php
/* Template Name: Frontend Publishing */
get_header();
if(have_posts()) {
	while(have_posts()) {
		the_post();
		echo '<h1>'.get_the_title().'</h1>';
		the_content();

		echo '<p class="copyright_laws">Travpart is not liable for any 3rd party content used. It is the responsibility of each user to comply with 3rd party copyright laws.</p>';

		echo do_shortcode('[fep_submission_form]');
		
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
	}
} else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>