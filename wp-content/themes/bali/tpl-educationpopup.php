<?php 
/*
Template Name: Education popup
*/
get_header();
?>
<div class="row">
<div class="col-md-12">
      <div class="profilecover">
        <img src="https://www.travpart.com/English/wp-content/uploads/2019/11/sunrise-1014712_1920.jpg">
      </div>
      <picture class="avatar">
        <img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/42/profile_photo-190x190.jpg?1574601972" alt="Avatar">
      </picture>
      <div class="p-connect-area">
        <a href="#"><i class="fa fa-snowflake"></i>Connect</a>
      </div>
      <div class="p-follow-area-top">
        <a href="#"><i class="fas fa-wifi"></i>Follow</a>
      </div>
      <div class="p-mess-area">
        <a id="chatwithuser" href="#"><i class="far fa-comment-alt"></i>Messages</a>
      </div>
      <div class="p-dots-area">
        <a href="#">
          <p>...</p>
        </a>
      </div>
      <div class="profile-name">
        <h2>Lix</h2>
      </div>
    </div>
  </div>
 <div class="button_area">
  <div class="row">
    <div class="col-md-6">
      <div class="col-md-2">
      </div>
      <div class="col-md-4">
        <a href="#" class="pro-gen-btn-info" role="button">General Profile</a>
      </div>
      <div class="col-md-4">
        <a href="#" class="car-edu-btn-info active" role="button">Career &amp; Education</a>
      </div>
      <div class="col-md-2">
      </div>
    </div>
  </div>
</div>

<div class="educonarea carrerwraptop">
	<div class="row">
		<div class="col-md-8 edutitle"><h3>Career</h3></div>
		<div class="col-md-4 triggerbtn"> <button popupheadtext="Add Expercience" class="trigger triggercareer">+</button></div>
	</div>
	<div class="row">
		<div class="carrerwrap">
			<div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/Capture.jpg"/></div>
			<div class="carrerright">
				<h4>Retail Sales Manager</h4>
				<div class="comname">Microsoft</div>
				<div class="extime">Aug 2015 - Present . 4yrs 4 mos</div>
				<div class="exloc">Urbana Champaign, IL USA</div>
				<ul>
					<li>Responsible in Managing a Team consist of 50 people to reach the target</li>
					<li>Stategic Directing, including business plan & sales strategy development.</li>
				</ul>
			</div>
      <div class="editbtn editcareer"><button popupheadtext="Edit Expercience" class="trigger triggercareer"><i class="fa fa-edit"></i></button></div>
		</div> <!-- end of carrerwrap -->
		<div class="carrerwrap">
			<div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/Capture.jpg"/></div>
			<div class="carrerright">
				<h4>Retail Sales Manager</h4>
				<div class="comname">Microsoft</div>
				<div class="extime">Aug 2015 - Present . 4yrs 4 mos</div>
				<div class="exloc">Urbana Champaign, IL USA</div>
				<ul>
					<li>Responsible in Managing a Team consist of 50 people to reach the target</li>
					<li>Stategic Directing, including business plan & sales strategy development.</li>
				</ul>
			</div>
      <div class="editbtn editcareer"><button popupheadtext="Edit Expercience" class="trigger triggercareer"><i class="fa fa-edit"></i></button></div>
		</div> <!-- end of carrerwrap -->

	</div>

</div>

<div class="educonarea carrerwraptop">
	<div class="row">
		<div class="col-md-8 edutitle"><h3>Education</h3></div>
		<div class="col-md-4 triggerbtn"> <button class="trigger triggeredu" popupheadtext="Add Education">+</button></div>
	</div>
	<div class="row">
		<div class="carrerwrap">
			<div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/Capture.jpg"/></div>
			<div class="carrerright">
				<h4>University of Indiana</h4>
				<div class="comname">Master's Degree, Master of Science in Finance</div>
				<div class="extime">2015</div>
				
			</div>
      <div class="editbtn editedu"><button popupheadtext="Edit Education" class="trigger triggeredu"><i class="fa fa-edit"></i></button></div>
		</div> <!-- end of carrerwrap -->
		<div class="carrerwrap">
			<div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/Capture.jpg"/></div>
			<div class="carrerright">
				<h4>University of Indiana</h4>
				<div class="comname">Master's Degree, Master of Science in Finance</div>
				<div class="extime">2015</div>
				
			</div>
      <div class="editbtn editedu"><button popupheadtext="Edit Education" class="trigger triggeredu"><i class="fa fa-edit"></i></button></div>
		</div> <!-- end of carrerwrap -->

	</div>

</div>
 
    <div class="overlay careerpopup">
         <div class="popup" id="carpopup">
          <div class="popupheader">
            <h2>Add Expercience</h2>
            <div class="close">X</div>
          </div>  
          <div class="popupbody">  
            <form action="" name="carrer">
             <div class="fieldwrap">
               <div class="popuplabel">Title *</div>
               <div class="popupfield"><input type="text" name="title" placeholder="Ex Manager" /></div>
             </div>
              <div class="fieldwrap">
               <div class="popuplabel">Company *</div>
               <div class="popupfield"><input type="text" name="title" placeholder="Ex Google" /></div>
             </div>
              <div class="fieldwrap">
               <div class="popuplabel">Location *</div>
               <div class="popupfield"><input type="text" name="title" placeholder="Ex NewYork USA" /></div>
             </div>
              <div class="fieldwrap">
                <div class="popupfield checkboxfield"><input type="checkbox" name="title" placeholder="Ex Manager" value="yes" /></div>
               <div class="popuplabel checkboxlabel">I am currently working in this role</div>               
             </div>
             <div class="fieldwrap">
              <div class="edudatewrap">
                 <div class="timewrap">
                <div class="popuplabel">StartDate</div> 
              
                <div class="startdatewrap">
                 <select>
                   <option selected value="">Month</option>
                 <option  value='1'>Janaury</option>
    <option value='2'>February</option>
    <option value='3'>March</option>
    <option value='4'>April</option>
    <option value='5'>May</option>
    <option value='6'>June</option>
    <option value='7'>July</option>
    <option value='8'>August</option>
    <option value='9'>September</option>
    <option value='10'>October</option>
    <option value='11'>November</option>
    <option value='12'>December</option>
                 </select>
                 <select>
                   <option>Year</option>
                   <option value="1990">1990</option>
                 </select>
               </div>
             </div>
             <div class="timewrap">
             <div class="popuplabel">EndDate</div> 
               
                <div class="startdatewrap">
                 <select>
                   <option selected value="">Month</option>
                 <option  value='1'>Janaury</option>
    <option value='2'>February</option>
    <option value='3'>March</option>
    <option value='4'>April</option>
    <option value='5'>May</option>
    <option value='6'>June</option>
    <option value='7'>July</option>
    <option value='8'>August</option>
    <option value='9'>September</option>
    <option value='10'>October</option>
    <option value='11'>November</option>
    <option value='12'>December</option>
                 </select>
                 <select>
                   <option>Year</option>
                   <option value="1990">1990</option>
                 </select>
               </div>
             </div>
             </div>
             </div>
              <div class="fieldwrap">
               <div class="popuplabel">Description *</div>
               <div class="popupfield">
                 <textarea rows="8" name="description"></textarea>
               </div>
            
             </div>
                 <div class="fieldwrap frmbtn">
                  <input type="submit" name="submit" value="Save" />
                </div>
            
              
            </form>
          </div>
         </div>
      </div>
 <div class="overlay educationpopup">
         <div class="popup" id="edupopup">
            <div class="popupheader">
            <h2>Add Education</h2>
            <div class="close">X</div>
          </div>  
          <div class="popupbody">  
            <form action="" name="carrer">
             <div class="fieldwrap">
               <div class="popuplabel">School *</div>
               <div class="popupfield"><input type="text" name="title" placeholder="Ex Miami University" /></div>
             </div>
              <div class="fieldwrap">
               <div class="popuplabel">Degree *</div>
               <div class="popupfield"><input type="text" name="title" placeholder="Ex Bachelor's" /></div>
             </div>
              <div class="fieldwrap">
               <div class="popuplabel">Field of study *</div>
               <div class="popupfield"><input type="text" name="title" placeholder="Ex Political Study" /></div>
             </div>
              
             <div class="fieldwrap">
              <div class="edudatewrap">
                 <div class="timewrap timewrapedu">
                <div class="popuplabel">StartDate</div> 
              
                <div class="startdatewrap">
                 <select>
                   <option>Select Year</option>
                   <option value="1990">1990</option>
                 </select>
               </div>
             </div>

             <div class="timewrap timewrapedu">
                <div class="popuplabel">End Year (or expected)</div> 
              
                <div class="startdatewrap">
                 
                 <select>
                   <option>Select Year</option>
                   <option value="1990">1990</option>
                 </select>
               </div>
             </div>
             
             </div>
             </div>
             <div class="fieldwrap">
               <div class="popuplabel">Grade</div>
               <div class="popupfield"><input type="text" name="title" /></div>
             </div>
              <div class="fieldwrap">
               <div class="popuplabel">Activites and societies *</div>
               <div class="popupfield">
                 <textarea rows="8" name="description"></textarea>
               </div>                
             </div>
              <div class="fieldwrap">
               <div class="popuplabel">Description *</div>
               <div class="popupfield">
                 <textarea rows="8" name="description"></textarea>
               </div>                
             </div>
             <div class="fieldwrap frmbtn">
                  <input type="submit" name="submit" value="Save" />
                </div>
            
              
            </form>
          </div>
         </div>
      </div>
    

<style>
	/* Education */
.editbtn {
    color: rgba(0,0,0,0.6);
    font-weight: normal;
    margin-right: 14px;
    margin-top: 18px;
}
.editbtn button.trigger{
    color: rgba(0,0,0,0.6);
    font-size: 15px;
}
.carrerwrap {
    display: flex;
    width: 100%;
}
.educonarea {
    width: 800px;
    margin: 0 auto;
    background-color: #fff;
    padding: 15px;
        margin-bottom: 30px;
}

.edutitle h3 {
    font-weight: bold;
    font-size: 18px;
    text-transform: uppercase;
    font-family: 'Heebo', sans-serif!important;
}
.carrerleft {
    width: auto;
    margin-left: 15px;
}
.carrerright {
    margin-left: 15px;
    font-family: 'Heebo', sans-serif!important;
    font-size: 14px;
    color: rgba(0,0,0,0.6);
    width: 92%;
    border-bottom: 1px solid #e6e9ec;
    padding-bottom: 6px;
    margin-bottom: 20px;
}
.carrerright h4 {
    font-size: 16px;
    font-family: 'Heebo', sans-serif!important;
    font-weight: bold;
    color: rgba(0,0,0,.7);
margin-bottom: 0;
}
.comname {
    color: rgba(0,0,0,0.9);
    font-size: 14px;
}
.carrerright ul {
    margin-right: 15px;
    margin-top: 7px;
}	
.popupbody {
    padding: 18px 15px 15px;
    font-size: 16px;
    font-family: 'Heebo', sans-serif!important;
}
.fieldwrap {
    margin-bottom: 8px;
}
.popupfield input[type="text"] {
    width: 100%;
    padding: 5px;
    border-radius: 4px;
    border: 1px solid rgba(0,0,0,.6);
    box-sizing: border-box;
}
.popupheader{
font-family: 'Heebo', sans-serif!important;
}
.popupheader h2 {
    margin-left: 13px;
    font-size: 22px;
}
.checkboxfield{
display:inline-block;
}
.checkboxlabel{
display:inline-block;
margin-left:5px;
}
.startdatewrap select {
    color: #000;
    font-size: 17px;
    -webkit-appearance: button;
    font-family: 'Heebo', sans-serif!important;
    font-weight: normal;
     padding: 5px;
    border-radius: 4px;
    border: 1px solid rgba(0,0,0,.6);
    -moz-appearance: button;
-webkit-appearance: button;
width: 48%;
box-sizing: border-box;
}
.edudatewrap {
    display: flex;
}
.timewrap {
    width: 50%;
}
.fieldwrap textarea {
    width: 100%;
    height: 80px;
    padding: 5px;
    border-radius: 4px;
    border: 1px solid rgba(0,0,0,.6);
    box-sizing: border-box;
}
.fieldwrap.frmbtn {
    float: right;
    margin-top: 9px;
}
.frmbtn input[type="submit"] {
    font-size: 18px;
    font-weight: normal;
}
.timewrap.timewrapedu .startdatewrap select {
    width: 98%;
}
/* end Eduction*/
.triggerbtn{
text-align: right;
}
button.trigger {
    background-color: transparent;
    color: #226D82;
    border: none;
    padding: 0;
    font-size: 34px;
    margin-top: -11px;
}

.overlay {
   position: fixed;
   height: 100%; 
   width: 100%;
   top: 0;
   right: 0;  
   bottom: 0;
   left: 0;
   background: rgba(0,0,0,0.8);
   display: none;
   z-index: 9999;
}

.popup {
  max-width: 600px;
    width: 80%;
    max-height: 400px;
    height: 90%;
    padding: 20px 0;
    position: relative;
    background: #fff;
    margin: 20px auto;
    overflow: auto;
    top: 10%;
}
.popupheader {
    display: flex;
    flex-wrap: wrap;
    border-bottom: 1px solid #ccc;
}
.close {
   position: absolute;
   top: 24px;
   font-weight: normal;
   right: 10px;
   cursor: pointer;
   color: #000;
   font-size: 19px;
}	

  .profilecover {
    position: relative;
  }

  .profilecover img {
    width: 100%;
    max-height: 350px;
  }

  picture.avatar img {
    width: 150px;
    height: 150px;
    border-radius: 50%;
    position: absolute;
    bottom: 0%;

  }

  .p-follow-area-top {
    background: #cacaca;
    padding: 5px 10px;
    position: absolute;
    bottom: 8%;
    right: 15%;
  }

  .p-follow-area-top a {
    color: #000;
  }

  .p-follow-area-top:hover {
    background: #1abc9c;
    cursor: pointer;
  }

  .p-mess-area {
    background: #cacaca;
    padding: 5px 10px;
    position: absolute;
    bottom: 8%;
    right: 8%;
  }

  .p-mess-area a {
    color: #000;
  }

  .p-mess-area:hover {
    background: #1abc9c;
    cursor: pointer;
  }

  .p-connect-area {
    background: #cacaca;
    padding: 5px 10px;
    position: absolute;
    bottom: 8%;
    right: 21%;
  }

  .p-connect-area a {
    color: #000;
  }

  .p-connect-area:hover {
    background: #1abc9c;
    cursor: pointer;
  }

  .p-dots-area {
    background: #cacaca;
    padding: 0px 8px;
    position: absolute;
    height: 25px;
    bottom: 8%;
    right: 5%;
  }

  .p-dots-area a {
    color: #000;
  }

  .p-dots-area:hover {
    background: #1abc9c;
    cursor: pointer;
  }

  .p-dots-area p {
    margin-top: -8px;
    font-size: 20px
  }

  .profile-name {
    position: absolute;
    bottom: 4%;
    left: 16%;
  }

  .profile-name h2 {
    color: white !important;
  }

  i.fa-wifi {
    transform: rotate(55deg);
    position: relative;
    right: 3px;
    top: 0px;
  }

  i.far.fa-comment-alt {
    padding-right: 4px;
  }

  i.fa.fa-snowflake {
    padding-right: 4px;
  }

  .button_area {
    margin: 15px;
  }
  .pro-gen-btn-info,.car-edu-btn-info{
    color: #000;
    background-color: white;
    border: solid 1px #000;
    padding: 7px;
    font-size: 14px;
  }
  .pro-gen-btn-info.active,.car-edu-btn-info.active {
    color: green;
    border: solid 1px green;
  }

  .pro-gen-btn-info:hover {
    color: green;
    text-decoration: none;
  }

  .car-edu-btn-info {
    color: black;
    background-color: white;
    padding: 7px;
    font-size: 14px;
  }

  .car-edu-btn-info:hover {
    color: green;
    background-color: white;
    border: solid 1px green;
    padding: 7px;
    font-size: 14px;
  }

  .mobile-button {
    display: none;
  }

  .p-v-label {
    text-align: right;
  }

  .p-v-text h3 {
    padding-top: 4px;
    font-size: 18px;
  }

  .p-v-label label.control-label {
    font-size: 16px;
    padding-top: 0px;
  }

  i.fas.fa-atom {
    font-size: 18px;
    padding-right: 5px;
  }

  i.fas.fa-home {
    font-size: 18px;
    padding-right: 5px;
  }

  i.fas.fa-city {
    font-size: 18px;
    padding-right: 5px;
  }

  i.fas.fa-ruler-vertical {
    font-size: 18px;
    padding-right: 5px;
  }

  i.fas.fa-wine-glass-alt {
    font-size: 18px;
    padding-right: 5px;
  }

  i.fas.fa-graduation-cap {
    font-size: 18px;
    padding-right: 5px;
  }

  i.fas.fa-smoking {
    font-size: 18px;
    padding-right: 5px;
  }

  i.fas.fas.fa-landmark {
    font-size: 18px;
    padding-right: 5px;
  }

  i.fas.fa-universal-access {
    font-size: 18px;
    padding-right: 5px;
  }

  i.fas.fa-praying-hands {
    font-size: 18px;
    padding-right: 5px;
  }

  i.fa.fa-language {
    font-size: 18px;
    padding-right: 5px;
  }

  i.fas.fa-table {
    font-size: 18px;
    padding-right: 5px;
  }

  .mobile-bio-age {
    display: none;
  }

  .lets_text {
    position: absolute;
    top: 55%;
    left: 38%;
    color: white;
    font-size: 27px;
    transform: translate(-50%, -50%);
  }

  .let_hang {
    margin-top: -31px;
  }

  i.far.fa-star {
    font-size: 25px;
    color: orange;
  }

  i.fas.fa-star {
    font-size: 25px;
    color: orange;
  }

  .social_text {
    margin-top: 25px;
  }

  .p_v_s_star {
    text-align: center;
    margin: 10px;
  }

  .p_v_so_rev {
    border: solid 2px #3333;
    padding-left: 0px;
    padding-right: 0px;
  }

  .social_avg_rate a {
    background-color: #7aa05f;
    font-family: "Roboto", Sans-serif;
    font-weight: 600;
    font-size: 15px;
    padding: 7px 21px;
    border-radius: 3px;
    color: #fff;
    text-decoration: none;
    display: inline-block;
  }

  .social_avg_rate {
    text-align: center;
    margin: 10px;
  }

  .all_rev_button {
    margin: 10px;
  }

  .all_rev_button a {
    background-color: white;
    font-family: "Roboto", Sans-serif;
    font-weight: 600;
    font-size: 15px;
    padding: 7px 21px;
    border: solid 1px green;
    color: #bae840;
    text-decoration: none;
    display: inline-block;
  }

  .all-review-area {
    margin-top: 10px;
  }

  .all-rev-box {
    border: solid 2px #3333;
  }

  .social-sec {
    border-bottom: solid 2px #3333;
  }

  .all-rev-text {
    margin-top: 15px;
  }

  .all-rev-reply p {
    color: green;
    text-align: right;
    margin-right: 60px;
    font-size: 20px;
  }

  .give_rev_button a {
    background-color: #22b14c;
    font-family: "Roboto", Sans-serif;
    font-weight: 600;
    font-size: 20px;
    padding: 7px 21px;
    border: solid 1px #22b14c;
    color: white;
    text-decoration: none;
    display: inline-block;
  }

  .give_rev_button {
    text-align: center;
    margin-bottom: 10px;
  }
  @media (max-width: 900px) {
.educonarea {
    width: 100%;
}
}
  @media (max-width: 600px) {
    picture.avatar img {
      width: 100px;
      height: 100px;
    }

    .p-connect-area {
      right: 48%;
    }

    .p-follow-area-top {
      right: 32%;
    }

    .p-mess-area {
      right: 13%;
    }

    .profile-name {
      bottom: 17%;
      left: 34%;
    }

    .profile-name h2 {
      font-size: 25px !important;
    }

    .mobile-button {
      display: block !important;
    }

    .button_area {
      display: none;
    }

    .button-display {
      display: flex;
    }

    .p-v-label {
      text-align: left;
    }

    .let_hang {
      margin-top: 0px;
    }

    .mobile-bio-age {
      display: block !important;
    }

    .desk-bio-age {
      display: none;
    }

    .social_text {
      margin-top: 0px;
      text-align: center;
    }

    .all-rev-reply p {
      text-align: center;
      margin-right: 0px;
    }

    .all-rev-text {
      text-align: center;
    }

    .all_rev_button {
      text-align: center;
    }

  }
</style>
<script type="text/javascript">
	jQuery(document).ready(function($){
   $('.triggercareer').click(function() {
      $('.careerpopup').fadeIn(300);
       var popuptit = $(this).attr("popupheadtext");
     $("#carpopup h2").text(popuptit);
    })  
   $('.close').click(function() {
      $('.careerpopup').fadeOut(300);
   });

    $('.triggeredu').click(function() {
      $('.educationpopup').fadeIn(300);
     var popuptit = $(this).attr("popupheadtext");
     $("#edupopup h2").text(popuptit);
    })  
   $('.close').click(function() {
      $('.educationpopup').fadeOut(300);
   });
});

</script>


<?php get_footer(); ?>