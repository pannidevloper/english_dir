<?php
get_header();
/* Template Name: BIO info */

global $wpdb;
?>
<div class="profile-pg mt-4">
    <div class="border-box mb-4">
        <div class="p-3 d-flex flex-column flex-sm-row align-items-center">
            <div class="avatar" style="background-image: url(https://www.travpart.com/Chinese/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg)"></div>
            <div class="flex-fill">
                <p class="d-flex align-items-center"><strong class="font-lg" style="margin-right: 5px;">ilhama bacha</strong> 
                    <span class="trav-rating"><span>4.0</span>
                        <i>

                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star fa-star-unchecked"></i>
                        </i>
                        <small> (3 reviews)</small>
                    </span>
                </p>
                <p><i class="fa fa-map-marker"></i> <strong>West Java</strong> - 8:33 PM local time</p>
                <p class="mb-0"><span class="user-type">Travel Advisor</span></p>
            </div>
        </div>
        <div class="p-3">
            <h4>Contact infomation</h4>
            <div class="d-flex flex-row profile-contact">
                <div class="mr-3 mr-sm-4"><span>Phone Number</span><br><span>628111882244</span></div>
                <div class="mr-3 mr-sm-4"><span>User ID</span><br><span>343</span></div>
                <div class="mr-3 mr-sm-4"><span>Age</span><br><span>18</span></div>
                <div class="mr-3 mr-sm-4"><span>Gender</span><br><span>Female</span></div>
                <div class="mr-3 mr-sm-4"><span>Email</span><br><span>ilham.bachtiar@tourfrombali.com</span></div>

            </div>
        </div>
        <div class="p-3 border-top profile-desc">
            <p>
            </p>
        </div>


    </div>
    <div class="average-feedback">
        <header class="d-flex">
            <div class="feedback-tilte mr-auto">Average Feedback</div>
            <div class="feedback-filter">
                <select class="form-control">
                    <option value="Newest first" selected="">Newest first</option>
                </select>
            </div>
        </header>
        <div class="feedback-items">
            <div class="feedback-item">
                <h4><a href="#">Booking Code?<strong>BALI1834</strong></a></h4>
                <div class="feedbak-rating-wrap">
                    <span class="trav-rating"><span>4.0</span>
                        <i>


                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star fa-star-unchecked"></i>

                        </i>
                        <small> Dec 2018</small>
                    </span>
                </div>
                <div class="feedback-detail">
                    Really enjoyed it the ambiance is amazing .and the cakes and pastries is damn osum. Liked it seriously . The harvest is for the foodies go there for atleast one                         </div>
                <div class="exp-date">
                    Experience Date: 13/12/2018                        </div>
            </div>
            <div class="feedback-item">
                <h4><a href="#">Booking Code?<strong>BALI1829</strong></a></h4>
                <div class="feedbak-rating-wrap">
                    <span class="trav-rating"><span>3.0</span>
                        <i>

                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star fa-star-unchecked"></i>
                            <i class="fa fa-star fa-star-unchecked"></i>

                        </i>
                        <small> Dec 2018</small>
                    </span>
                </div>
                <div class="feedback-detail">
                    Unfortunately this ruined the experience for me as you felt hussled up the paths, and it was hard to find a place to take a decent photo of the temple, which is what you came to see. It is certainly worth seeing but perhaps try visiting in the morning??                         </div>
                <div class="exp-date">
                    Experience Date: 13/12/2018                        </div>
            </div>
            <div class="feedback-item">
                <h4><a href="#">Booking Code?<strong>BALI1870</strong></a></h4>
                <div class="feedbak-rating-wrap">
                    <span class="trav-rating"><span>4.0</span>
                        <i>


                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star fa-star-unchecked"></i>

                        </i>
                        <small> Dec 2018</small>
                    </span>
                </div>
                <div class="feedback-detail">
                </div>
                <div class="exp-date">
                    Experience Date: 17/12/2018                        </div>
            </div>
        </div>
    </div>

</div>
<?php
get_footer();
?>