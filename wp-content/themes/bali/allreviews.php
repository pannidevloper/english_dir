<?php
/*
Template Name: allreviews
*/

global $wpdb;
if (!is_user_logged_in()) {
	exit(wp_redirect(home_url('login')));
}

if (!empty($_GET['user']) && ($user = get_user_by('login', htmlspecialchars($_GET['user'])))) { } else {
	$user = wp_get_current_user();
}

$star = filter_input(INPUT_GET, 'star', FILTER_VALIDATE_INT, array('options'=>array('default'=>0, 'min_range'=>1, 'max_range'=>5)));
$sql_star = '';
if($star!=0) {
	$sql_star=' AND rating='.$star;
}

$review_count=$wpdb->get_var("SELECT COUNT(*) FROM social_reviews WHERE parent_id=0 AND user_id={$user->ID} {$sql_star}");
$review_per_page=10;
$pageNum = filter_input(INPUT_GET, 'pageNum', FILTER_VALIDATE_INT, array('options'=>array('default'=>1, 'min_range'=>1, 'max_range'=>ceil($review_count / $review_per_page))));
$reviewOffset = ($pageNum-1) * $review_per_page;

$social_reviews = $wpdb->get_results("SELECT * FROM social_reviews WHERE parent_id=0 AND user_id={$user->ID} {$sql_star}
									ORDER BY `create_time` DESC LIMIT {$reviewOffset},{$review_per_page}");

get_header();
?>

<style>
	/* Tabs panel */
	.pagination>li>a,
	.pagination>li>span {
		border: 1px solid #689f38;
		color: #689f38;
		font-weight: bold;
		font-family: 'Heebo', sans-serif;
	}

	.reviews_wrapper {
		padding: 20px;
	}

	.tabbable-panel {
		border: 1px solid #eee;
		background: #fff;
		padding: 10px;
	}

	/* Default mode */
	.nav {
		display: inherit !important;
	}

	.tabbable-line>.nav-tabs {
		border: none;
		margin: 0px;
	}

	.tabbable-line>.nav-tabs>li {
		margin-right: 2px;
	}

	.tabbable-line>.nav-tabs>li>a {
		border: 0;
		margin-right: 0;
		color: #737373;
	}

	.tabbable-line>.nav-tabs>li>a>i {
		color: #a6a6a6;
	}

	.tabbable-line>.nav-tabs>li.open,
	.tabbable-line>.nav-tabs>li:hover {
		background: #689f38;
		color: #fff !important;
	}

	.nav-tabs>li:hover a {
		color: #fff !important;
	}

	.tabbable-line>.nav-tabs>li.open>a,
	.tabbable-line>.nav-tabs>li:hover>a {
		border: 0;
		background: none !important;
		color: #fff;
	}

	.nav-tabs>li:hover a i {
		color: #fff !important;
	}

	.tabbable-line>.nav-tabs>li.open .dropdown-menu,
	.tabbable-line>.nav-tabs>li:hover .dropdown-menu {
		margin-top: 0px;
	}

	.tabbable-line>.nav-tabs>li.active {
		position: relative;
	}

	.tabbable-line>.nav-tabs>li.active a {
		color: #fff !important;
		background: #689f38;
	}

	.tabbable-line>.nav-tabs>li.active a i {
		colo: #fff !important;
	}

	.tabbable-line>.nav-tabs>li.active>a {
		border: 0;
		color: #333333;
	}

	.tabbable-line>.nav-tabs>li.active>a>i {
		color: #404040;
	}

	.tabbable-line>.tab-content {
		background-color: #fff;
		padding-top: 45px !important;
		padding-bottom: 45px !important;
		border: 0;
		border-top: 1px solid #eee;
		padding: 15px 0;
	}

	.portlet .tabbable-line>.tab-content {
		padding-bottom: 0;
	}

	/* Below tabs mode */

	.tabbable-line.tabs-below>.nav-tabs>li>a {
		margin-top: 0;
	}

	.tabbable-line.tabs-below>.nav-tabs>li:hover {
		border-bottom: 0;
	}

	.tabbable-line.tabs-below>.nav-tabs>li.active {
		margin-bottom: -2px;
		border-bottom: 0;
	}

	.tabbable-line.tabs-below>.tab-content {
		margin-top: -10px;
		border-top: 0;
		border-bottom: 1px solid #eee;
		padding-bottom: 15px;
	}

	.nav-tabs>li>a>i {
		color: #689f38 !important;
	}

	.nav-tabs>li>a {
		font-size: 12px;
		letter-spacing: 1px;
		color: #689f38 !important;
		font-family: 'Heebo', sans-serif;
		font-weight: bold;
	}

	.nav-tabs>li {
		float: none;
		display: inline-block;
		zoom: 1;
	}

	.nav-tabs {
		text-align: center;
	}

	.custom_btn_d {
		background: #689f38;
		padding: 5px 15px;
		color: #fff;
		font-family: 'Heebo', sans-serif;
		font-size: 15px;
	}

	.row_custom {
		padding: 10px 5px;
	}

	.coloum_custom {
		width: 12%;
		font-family: 'Heebo', sans-serif;
		font-weight: bold;
		font-size: 14px;
		display: table-cell;
		text-align: center;
	}
	.coloum_custom p {
	    word-break: break-word;
	}

	.s_star {
		text-align: right;
	}

	.s_star i {
		color: #ffd902;
		font-size: 26px;
		color: orange;
	}
	@media (max-width: 600px){
		.s_star i {
			font-size: 9px !important;
			color: orange;
		}
		.s_star {
			text-align: center;
		}
		.give_rating.s_star i{
			font-size: 20px !important;
			color: orange;
		}

	}

	@media (min-width: 280px) and (max-width: 320px) {
		.coloum_custom {
			font-size: 8px;
		}

		.for_med {
			padding: 0px;
		}

		.custom_btn_d {
			padding: 0px 7px;
		}

		.nav-tabs li a {
			padding: 0px;
		}

		.row_custom {
			padding: 10px 0px;
		}
	}
@media (max-width: 768px) {

}
@media (max-width: 320px) {

}

	@media (min-width: 321px) and (max-width: 640px) {
		.coloum_custom {
			font-size: 10px;
		}

		.tabbable-line>.nav-tabs>li {
			margin: 0px !important;
		}

		.row_custom {
			padding: 10px 0px;
		}

		.nav-tabs>li {
			padding: 2px;
		}

		.for_med {
			padding: 0px;
		}

		.custom_btn_d {
			padding: 0px 7px !important;
		}
		.reviews_tabs {
		    margin-top: 60px;
		}
	}

	.give_rating.s_star {
		text-align: center;
		padding: 10px;
	}
	.give_review textarea {
		width: 80%;
		height: 100px;
		border-color: grey;
		font-size: 18px;
	}
</style>

<script>
	$(function(){
		$('.give_rating i').click(function() {
			var that = $(this);
			var i = 0;
			$('.give_rating i').removeClass('fas').addClass('far');
			$('.give_rating i').each(function() {
				$(this).removeClass('far').addClass('fas');
				i++;
				if ($(this).is(that))
					return false;
			});
			$('#review_rating').val(i);
		});

		$('.all-rev-reply').click(function() {
			$('#parent_id').val($(this).attr('parent_id'));
			$('#review_content').attr('placeholder', 'Reply ' + $(this).attr('username')).focus();
		});

		$('#submit_review').click(function() {
			if ($(this).hasClass('disabled')) {
				return;
			}

			$(this).addClass('disabled');

			var rating = $('#review_rating').val(),
				content = $('#review_content').val(),
				parent_id= $('#parent_id').val() || 0;

			$.ajax({
				type: 'POST',
				url: '<?php echo admin_url('admin-ajax.php'); ?>',
				dataType: 'json',
				data: {
					action: 'add_social_view',
					user_id: <?php echo $user->ID; ?>,
					parent_id: parent_id,
					content: content,
					rating: rating
				},
				success: function(data) {
					if (data.success) {
						window.location.href = "<?php echo add_query_arg(); ?>";
					} else {
						alert(data.msg);
					}
					$('#submit_review').removeClass('disabled');
				}
			});
		});


	});
</script>

<div class="reviews_wrapper">
	<div class="container">


		<div class="reviews_tabs">

			<div class="tabbable-panel">
				<div class="tabbable-line">
					<ul class="nav nav-tabs">
						<li role="presentation" <?php echo ($star==0)?'class="active"':''; ?>>
							<a href="<?php echo add_query_arg(array('user'=>htmlspecialchars($_GET['user']),'star' => '','pageNum' => '')); ?>">Most Recent</a>
						</li>
						<li role="presentation" <?php echo ($star==5)?'class="active"':''; ?>>
							<a href="<?php echo add_query_arg(array('user'=>htmlspecialchars($_GET['user']),'star' => 5,'pageNum' => '')); ?>">
								5<i class="fas fa-star" aria-hidden="true"></i>
							</a>
						</li>
						<li role="presentation" <?php echo ($star==4)?'class="active"':''; ?>>
							<a href="<?php echo add_query_arg(array('user'=>htmlspecialchars($_GET['user']),'star' => 4,'pageNum' => '')); ?>">
								4<i class="fas fa-star" aria-hidden="true"></i>
							</a>
						</li>
						<li role="presentation" <?php echo ($star==3)?'class="active"':''; ?>>
							<a href="<?php echo add_query_arg(array('user'=>htmlspecialchars($_GET['user']),'star' => 3,'pageNum' => '')); ?>">
								3<i class="fas fa-star" aria-hidden="true"></i>
							</a>
						</li>
						<li role="presentation" <?php echo ($star==2)?'class="active"':''; ?>>
							<a href="<?php echo add_query_arg(array('user'=>htmlspecialchars($_GET['user']),'star' => 2,'pageNum' => '')); ?>">
								2<i class="fas fa-star" aria-hidden="true"></i>
							</a>
						</li>
						<li role="presentation" <?php echo ($star==1)?'class="active"':''; ?>>
							<a href="<?php echo add_query_arg(array('user'=>htmlspecialchars($_GET['user']),'star' => 1,'pageNum' => '')); ?>">
								1<i class="fas fa-star" aria-hidden="true"></i>
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="all">

							<?php foreach ($social_reviews as $r) { ?>
								<div class="row_custom">
									<div class="coloum_custom">
										<p>By <?php echo um_get_display_name($r->creator_id); ?></p>
									</div>
									<div class="coloum_custom">
										<p><?php echo str_replace("\n", "<br>", $r->review); ?></p>
									</div>
									<div class="coloum_custom">
										<div class="s_star">
											<?php for ($i = 0; $i < $r->rating; $i++) { ?>
												<i class="fas fa-star" aria-hidden="true"></i>
											<?php } ?>
											<?php for ($i = $r->rating; $i < 5; $i++) { ?>
												<i class="far fa-star" aria-hidden="true"></i>
											<?php } ?>
											<?php $reply = $wpdb->get_var("SELECT review FROM social_reviews WHERE parent_id={$r->id} AND user_id=" . um_profile_id() . " LIMIT 1"); ?>
											<?php if (empty($reply)) { ?>
												<?php if(wp_get_current_user()->ID==$user->ID) { ?>
												<p class="all-rev-reply" username="<?php echo um_get_display_name($r->creator_id); ?>" parent_id="<?php echo $r->id; ?>"
												style="text-align: right;cursor: pointer;color: #689f38 !important;font-family: 'Heebo', sans-serif;">Reply</p>
												<?php } ?>
											<?php } else { ?>
												<p class="for_med" style="text-align: center;cursor: pointer;font-family: 'Heebo', sans-serif;border:1px solid #ccc;padding: 10px;">
													<?php echo str_replace("\n", "<br>", $reply); ?>
												</p>
											<?php } ?>
										</div>
									</div>
									<div class="coloum_custom">
										<p><?php echo date("m/d/Y", strtotime($r->create_time)); ?></p>
									</div>
								</div>
							<?php } ?>

						</div>
					</div>
				</div>
				<div class="text-center">

					<div class="give_rating s_star">
						<i class="fa-star fas" aria-hidden="true"></i>
						<i class="fa-star fas" aria-hidden="true"></i>
						<i class="fa-star fas" aria-hidden="true"></i>
						<i class="fa-star fas" aria-hidden="true"></i>
						<i class="fa-star fas" aria-hidden="true"></i>
					</div>
					<input id="review_rating" type="hidden" value="5">
					<input id="parent_id" type="hidden" value="0" />
					<div class="give_review">
						<textarea id="review_content"></textarea>
					</div>
					<button id="submit_review" class="btn btn-default custom_btn_d">Give Review</button>

					<?php if (ceil($review_count / $review_per_page) > 1) { ?>
					<nav aria-label="Page navigation">
						<ul class="pagination" style=" display:table;margin:0 auto;">
						<?php if ($pageNum > 1) { ?>
							<li>
								<a href="<?php echo add_query_arg(array('pageNum' => $pageNum-1)); ?>" aria-label="Previous">
									&laquo;
								</a>
							</li>
						<?php } ?>
						<?php for($p=$i=($pageNum-2)>0?($pageNum-2):1; ($i-$p<5 && $i<=ceil($review_count / $review_per_page)); $i++) { ?>
							<li><a href="<?php echo ($i==$pageNum)?'#':add_query_arg(array('pageNum' => $i)); ?>"><?php echo $i; ?></a></li>
						<?php } ?>
						<?php if ($pageNum+1 <= ceil($review_count / $review_per_page)) { ?>
							<li>
								<a href="<?php echo add_query_arg(array('pageNum' => $pageNum+1)); ?>" aria-label="Next">
									&raquo;
								</a>
							</li>
						<?php } ?>
						</ul>
					</nav>
					<?php } ?>

				</div>
			</div>

		</div>
	</div>
</div>

<style type="text/css">
    #footer {
        display: none;
    }
</style>

<?php
get_footer();
?>