<?php
session_start();
/* Template Name: Land Arrangements */

//get_header();
include ('header-travcust.php');
$meta = get_post_meta( get_the_ID() );

$person_price_ration=get_option('person_price_ration')?(float)get_option('person_price_ration'):1.0;
if(!is_float($person_price_ration) || $person_price_ration<=0)
  $person_price_ration=1.0;

// Add tour if tour id isn't existed
function make_random_string($N)
{
    $alph = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    $s = "";
    for ($i = 0; $i != $N; ++$i)
        $s .= $alph[mt_rand(0, 35)];
    return $s;
}

if(is_user_logged_in() && current_user_can('um_clients')) {
    exit(wp_redirect(home_url('toursearch')));
}

if(is_user_logged_in() && current_user_can('um_travel-advisor')) {
if (empty($_COOKIE['tour_id'])) {
    $code = make_random_string(5); //uniqid();
    $number_of_coding = $wpdb->get_var($wpdb->prepare("SELECT COUNT(code) FROM " . $wpdb->prefix . "tour WHERE code = %s", $code));
    while ($number_of_coding > 0) {
        $code = make_random_string(5);
        $number_of_coding = $wpdb->get_var($wpdb->prepare("SELECT COUNT(code) FROM " . $wpdb->prefix . "tour WHERE code = %s", $code));
    }

    $tour_date = date('d/m/Y');
    $start_date = date('Y-m-d');
    $end_date = date('Y-m-d', time() + 86400);

    $wpdb->query("INSERT INTO `wp_tour` (`id`, `tour_id`, `number_of_adult`, `number_of_child`, `number_of_room`, `number_of_extra_bed`, `addnightroom`, `tour_type`, `start_date`, `end_date`, `total`, `code`, `tour_date`, `hotels`, `tickets`)  
            VALUES (NULL, '" . get_the_ID() . "', '1', '0', '0', '0', '0', '0', '" . $start_date . "', '" . $end_date . "','0', '" . $code . "','" . $tour_date . "', '','');
    ");
    $tour_id = $wpdb->insert_id;
    setcookie('tour_id', $tour_id, (time()+86400*30), '/English/', 'www.travpart.com');
} else {
    $tour_id=intval($_COOKIE['tour_id']);
}
}

// Add tour end

$car_price=intval($_COOKIE['car_price']);
//Get adivaha car price
if(!empty($_COOKIE['adivaha_car_price']) && floatval($_COOKIE['adivaha_car_price'])>0) {
    $adivaha_car_price=round(floatval($_COOKIE['adivaha_car_price'])/get_option("_cs_currency_USD"));
    $car_price+=$adivaha_car_price;
    setcookie('car_price', (intval($_COOKIE['car_price'])+$adivaha_car_price), (time()+86400*30), '/English/', 'www.travpart.com');
    setcookie('adivaha_car_price', 0, (time()+86400*30), '/English/', 'www.travpart.com');
}

?>

<?php /*
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/remodal/remodal.css"/>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/remodal/remodal-default-theme.css"/>
*/ ?>
<link href="<?php bloginfo('template_url'); ?>/css/tour.css" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/datepicker/css/style.css" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/travcust.css?v=51" rel="stylesheet" type="text/css">

<!-- duplicate style-->
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
<!--  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">-->

<script src="<?php bloginfo('template_url'); ?>/js/jquery-ui.js"></script>
<!--<script src="<?php bloginfo('template_url'); ?>/remodal/remodal.js" type="text/javascript"></script> -->
<script src="<?php bloginfo('template_url'); ?>/js/tour.js?version=18"></script>
<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/tableresponsive.css"/>
 
<!-- add this version jquery ,the  datapicker Ability to work -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap-notify.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/travcust.js?71"></script>

<?php/*<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/remodal/remodal.css"/>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/remodal/remodal-default-theme.css"/>
<link href="<?php bloginfo('template_url'); ?>/css/tour.css" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/jquery-ui.css" rel="stylesheet" type="text/css">
<script src="https://cdn.bootcss.com/jqueryui/1.12.1/jquery-ui.js"></script>
<script src="<?php bloginfo('template_url'); ?>/remodal/remodal.js" type="text/javascript"></script> 
<script src="<?php bloginfo('template_url'); ?>/js/tour.js?version=10"></script>
<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/tableresponsive.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maps.api.sygic.com/js/leaflet/1.0.3/leaflet.css" />
<link rel="stylesheet" href="https://maps.api.sygic.com/js/sygic/1.2.0/leaflet.sygic.css" />
<script class="jquery library" src="/js/sandbox/jquery/jquery-1.8.2.min.js" type="text/javascript"></script> 
<script src="https://maps.api.sygic.com/js/leaflet/1.0.3/leaflet.js"></script>
<script src="https://maps.api.sygic.com/js/leaflet.sygic-1.1.0.js"></script>*/ ?>
<style>
#search_itname {
    font-size: 20px;
}
.date_d{
	border-left:1px solid #ccc;
	border-right:1px solid #ccc
}
.for_m_bottom{
	margin-bottom:10px;
}
.hotel_map,.map_div,.date_d{
	display: inline-block;
	padding: 0px 15px;
	vertical-align: middle;
}
.hotel_map h1 i{	
	color: #333;
}
.hotel_map p i,.date_d i{
	color: #16b9aabf
}
.hotel_map p,.date_d{
	font-size: 14px;
}
.scroll_top button:focus {outline:none !important}
	.scroll_top button{
		    position: fixed;
		    padding: 15px 22px;
		    border-radius: 50%;
		    background: #0cad83;
		    right: 15px;
		    color: #fff;
		    border: none;
		    z-index: 100000;
		    bottom: 85px;
		    font-size: 20px;
	}
		.mobiletrav{
			display: none;
		}
        .v2-list-right.trav_cu_add_item {
            width: 100%;
        }
        .mr-auto.trav_cu_add_item_head h4 {
            font-size: 35px;
        }
        i.fa.fa-flag.text-gray-0.trav_cu_add_item_icon {
            font-size: 22px !important;
        }
        .flex-fill.p-2.border-right.trav_cu_add_item_data_size {
            font-size: 20px;
        }
        .d-flex.flex-row.justify-content-between.align-items-center.flex-grow-1.text-center.trav_cu_item_desc p {
            font-size: 15px;
        }
	@media only screen and (max-width: 640px){
		.mobiletrav{
			//display: block;
			background: #22b14c;
			color: #fff;
			letter-spacing: 1px;
			font-size: 16px;
			padding-top: 0px;
			padding:30px;
			font-family: 'Heebo', sans-serif;
		}
		.for_mobile_travcust{
			padding-top:20px;
		}
		.clic_hide{
			color: #0000007d;
			position: absolute;
			top: 5px;
		}
		#left_menu{
			//display: none !important;
		}
		#h-t{
			margin-top:0px !important;
		}
		.total-tour-submit-wrapper,.elementor.elementor-14047,.tour_info,#menuresize {
		   //display: none;
		}
		.computer_div img,.laptop_div img{
			width: 100%;
		}
		.for_mobile_travcust_content p {
		    margin-bottom: 35px;
		}
		.computer_div,.laptop_div{
			width:48%;
			display: inline-block;
			text-align: center;
		}
	}
</style>
<div id="loading-image" style="display: none;" ></div>

<div class="mobiletrav">
 <div class="for_mobile_travcust"> 
 	<div class="for_mobile_travcust_content">
 		<div class="text-right">
 			<button class="btn clic_hide"><i class="fas fa-times"></i></button>
 		</div>
 		<p>We're sorry that Our current mobile view  for Travcurt page is not ready.</p>
 		<p>Please try going to this page by using PC to have the best view.</p>
 		<div class="computer_div">
 			<img src="https://www.travpart.com/English/wp-content/themes/bali/images/computer1.jpg" />
 		</div>
 		<div class="laptop_div">
 			<img src="https://www.travpart.com/English/wp-content/themes/bali/images/laptop.jpg" />
 		</div>
 		<div class="clearfix"></div>
 	</div> 
 </div>
</div>

<!-- code for slide brands -->
<style>
      .below-logo {
        display: flex;
        justify-content: center;
    }

    ul.list-unstyle.header-logo {
    list-style: none;
    padding: 0;
    margin: 0;
  }
  ul.list-unstyle.header-logo li {
    background-color: #f2f2f2;
    display: block;
    padding: 20px;
    width: 47%;
    text-align: center;
    float: left;
    margin: 10px;
}
.below-logo a {
    display: block;
    font-weight: 600;
}
.below-logo .ttl-1-logo, .below-logo .ttl-2-logo {
    display: none;
}
.below-logo img {
   width: 100%;
}

.divider-row{border-bottom: 1px solid #000;}

  #login_form{
    width:350px;
    margin: 60px auto 0 auto;   
  }

  .btn-facebook {
    background-color: #052940;
  }

  .btn-twitter {
    background-color: #1DA1F2;
  }

  .btn-google {
    background-color: #DB4437;
  }

  /* SPA section*/
.boatimg, .spaimg{width: 350px!important;opacity: 0.7;background: #f3f3f3!important;border: 1px solid #0dad83!important;padding: 20px!important;margin: 20px!important;text-align: center;display: block;margin: 20px auto;-webkit-border-radius: 5px!important;-moz-border-radius: 5px!important;border-radius: 5px!important;}
.boat-subitem, .spa-subitem{width:94%!important;margin-bottom:3%;margin-left:3%;}
.boat-subitem td, .spa-subitem td{border:1px solid #ccc;padding:15px;text-align:center;}
.boat-subitem tr:nth-child(odd), .spa-subitem tr:nth-child(odd){background:#eee;}
.boat-subitem tr:nth-child(even), .spa-subitem tr:nth-child(even){background:white;}
.spa-select{padding:0!important;margin-top:50px;}
.spa-select p{margin-left: 3%;background: #0dad83;padding: 20px;color: white;width: 94%;}
.spa-select img{padding:20px;}
.spa-select li img {float:none!important;clear:both!important;width:100px!important;}
.spa-select li {padding:0!important;box-shadow: 0 16px 24px 2px rgba(0, 0, 0, .14), 0 6px 30px 5px rgba(0, 0, 0, .12), 0 8px 10px -5px rgba(0, 0, 0, .2);border:none;}
.spa-select li:hover {border:2px solid #368a8c;transition: 0.5s;}
  

.tooltip {
    position: relative;
    display: inline-block;
}
.tooltip .tooltiptext {
    visibility: hidden;
    width: auto!important;
    background-color: black;
    color: #fff;
    text-align: center;
    padding: 2px 0;
    border-radius: 6px; 
    position: absolute;
    top: 110%!important;
    z-index: 1;
}



.tooltip:hover .tooltiptext {
    visibility: visible;
}

.mobileonly{display:none;}
  </style>


<script type="text/javascript">
	
	jQuery(document).ready(function($){
		
		$('.scroll_top button').click(function(){
			$('html, body').animate({scrollTop: 0}, 700);
		})
		
		$('.clic_hide').click(function(){
			$('.mobiletrav').hide();
			//$('.elementor.elementor-14047,.tour_info,#menuresize,.total-tour-submit-wrapper').show();
		});
	})
 
  $("li.ttl-1-li").hover(function(){
        $(".ttl-1-logo").slideToggle(800);
    });
  $("li.ttl-2-li").hover(function(){
        $(".ttl-2-logo").slideToggle(800);
    });
</script>
<!-- code for slide brands end -->

<style>
/*silky code*/
.collapse.in{
  display: block !important;
}

#loading-image {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 90px;
    height: 90px;
    animation: spin 2s linear infinite;
    position: fixed;
    left: 50%;
    top: 50%;
    z-index: 9999;
    margin-right: -50%;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}

#travelPlaceHeading{ margin-bottom: 22%;}

#travelPlaceDesc{ position: relative; bottom: 300px; }

#travelPlaceOtherInfo{ position: relative; bottom: 250px; }

#search_price_box{display: none;}

/* End silky code*/

li{list-style: none!important}
.dropdown-content { display: none; z-index:10; position: absolute; background-color: #f9f9f9; min-width: 160px; box-shadow: 0px 8px 16px 0px rgba(0,0,0,0,2);  margin:-20px 0px 0px 0px; padding:0px }
.dropdown-content li { color: black; padding: 12px 16px; text-decoration: none; display: block; }
.dropdown-content li:hover { background-color: #A0A0A0 }
.extraactivity{position: relative}
.extraactivity th{padding:10px 20px;background:#3b898a;color: white;font-size:1.5em;}
.extraactivity tr:nth-child(odd){padding:50px 10px;background:#f4f4f4!important;}
.extraactivity tr:nth-child(2){border-left:7px solid #21d21d;}
.extraactivity tr:nth-child(3){border-left:7px solid #ea863d;}
.extraactivity tr:nth-child(4){border-left:7px solid #ca5f71;}
.extraactivity tr:nth-child(n+5):nth-child(-n+23){border-left:7px solid #4b8fc1;}
.extraactivity tr:nth-child(n+24):nth-child(-n+37){border-left:7px solid #368a8c;}
.extraactivity tr:nth-child(n+38):nth-child(-n+47){border-left:7px solid #ebdc5d;}
.extraactivity tr:nth-child(n+48):nth-child(-n+93){border-left:7px solid #0093ff;}
.extraactivity tr:nth-child(n+94):nth-child(-n+102){border-left:7px solid #ab7d2c;}
.extraactivity tr:nth-child(54){border-left:5px solid #368a8c;}
.extraactivity tr:nth-child(55){display:none!important;}
.extraactivity tr:nth-child(57){display:none!important;}
.extraactivity tr:nth-child(58){display:none!important;}
.extraactivity tr:nth-child(60){display:none!important;}
.extraactivity tr:nth-child(62){display:none!important;}
.extraactivity tr:nth-child(63){display:none!important;}
.extraactivity tr:nth-child(65){display:none!important;}
.extraactivity tr:nth-child(66){display:none!important;}
.extraactivity tr:nth-child(68){display:none!important;}
.extraactivity tr:nth-child(69){display:none!important;}
.extraactivity tr:nth-child(71){display:none!important;}
.extraactivity tr:nth-child(72){display:none!important;}
.extraactivity tr:nth-child(74){display:none!important;}
.extraactivity tr:nth-child(75){display:none!important;}
.extraactivity tr:nth-child(77){display:none!important;}
.extraactivity tr:nth-child(79){display:none!important;}
.extraactivity tr:nth-child(81){display:none!important;}
.extraactivity tr:nth-child(83){display:none!important;}
.extraactivity tr:nth-child(84){display:none!important;}
.extraactivity tr:nth-child(86){display:none!important;}
.extraactivity tr:nth-child(87){display:none!important;}
.extraactivity tr:nth-child(89){display:none!important;}
.extraactivity tr:nth-child(90){display:none!important;}
.extraactivity tr:nth-child(91){display:none!important;}
.extraactivity tr:nth-child(93){display:none!important;}
.extraactivity tr:nth-child(94){display:none!important;}
.extraactivity tr:nth-child(95){display:none!important;}
.extraactivity tr:last-child{display:none;}
.extraactivity tr{padding:20px;}
.extraactivity tr td{padding:50px 20px;vertical-align:top;font-size:15px;}
.extraactivity tr td:first-child{padding:40px 20px 50px 43px;width:21%!important;color:#368a8c;font-weight: bold}
.extraactivity tr td:first-child img{margin:10px 0;display:block;}
.extraactivity tr td:nth-child(2){text-align:center;padding:50px 20px;vertical-align:top;}
.extraactivity tr td:nth-child(3){text-align:center;vertical-align:top;}
.extraactivity tr:hover{background:#eaffff!important;}
.extraactivity input{padding:20px 10px!important;margin:30px;background:#1076c8;color:white;font-weight: bold;border:2px solid #cccccc!important; vertical-align:top!important;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
.mockupimage{margin:10px;border:2px solid #cccccc; vertical-align:top;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px; width:250px;height:150px;box-shadow: 0 2px 1px 0 #ccc;background-color: #fff;}
.change_price{width:100%;padding:10px;background:#1076c8;color:white;font-weight: bold;vertical-align:top;border:2px solid #cccccc; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
.change_price.original{background:none;padding:1px;margin:1px;color:#333333;border:none;}
.eaitotal{width:15%;font-size:22px;vertical-align:top;color:#64d75e;font-weight: bold;text-align:center;}
.eainum{padding:7px!important;text-align:center;}
.eaidate{padding:40px 20px!important;}
.eaidate a{border:2px solid #cccccc; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;text-decoration: none}
.eaidate a:hover{background:#1076c8;}
.totalprice{width:100%;text-align:right;font-size: 2em;color:white;float:right;background:#1076c8;font-weight: bold;padding:20px;margin-bottom:50px;}
.firstcontent{width:25%!important;vertical-align:top;}
.secondcontent{vertical-align:top;}
.secondcontent, .thirdcontent, .forthcontent{text-align:center;}
.car-select, .meal-select, .popupguide-select, .spa-select{list-style: none!important;}
.car-select li:hover, .meal-select li:hover{cursor: pointer}
.meal-select img, .car-select img, .popupguide img{width:70%!important;opacity: 0.7;margin:0 auto;display:block;}
.meal-subitem td img{width:64px!important;}
#menuresize{position: fixed;top:5%;left:0px;background:white;width:120px;height:5%;}
#menuresize img{float:left;margin:10px 2px;opacity: 0.8}
#menuresize img:hover{opacity: 1;cursor: pointer}
#left_menu_content{
	padding: 30px 0px !important; 
}
#left_menu_content li{cursor: pointer}
#left_menu_content{font-size:10px !important;}
#left_menu_content li:hover{border-right:2px solid #3f8888}
#left_menu{top:0%!important;z-index:9999;}
#right_menu{position: fixed;top: 25%;padding:0;right: 0;z-index: 9999;text-align: center;border: 1px solid #e6e6e6;width:130px;background:#232a2b;-webkit-border-top-left-radius: 30px;-webkit-border-bottom-left-radius: 30px;-moz-border-radius-topleft: 30px;-moz-border-radius-bottomleft: 30px;border-top-left-radius: 30px;border-bottom-left-radius: 30px;}
#right_menu div{margin-bottom:10px;font-size:15px;color:#ffc80a;margin: 0;padding: 20px;text-decoration: none;}
#right_menu div:hover{cursor: pointer}
#right_menu a{color:#ffc80a;}
.button_zero:hover, .button_one:hover, .button_two:hover, .button_three:hover, .button_four:hover{cursor:pointer;}
.buttonmargin{padding:20px 0 55px 30px;}
#button_zeroscroll span,#button_onescroll span,#button_twoscroll span,#button_threescroll span{font-weight: bolder}

.grow { transition: all .2s ease-in-out; }
.grow:hover { transform: scale(1.1); border:2px solid #3f8888;margin-bottom:50px;}

.searchButton2{margin-left:0!important;}
#eaidate_choose{border:none;border:1px solid #f0f0f0!important}
.spu{background:white!important;}

.elementor-29596 .elementor-element.elementor-element-a53f9c0 > .elementor-element-populated {
background-color: #404040;
transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
color: #ffffff;
margin: 0px 0px 0px 0px;
padding: 20px 35px 20px 35px;
}

.btn-primary {
    background: #0cad83!important;
    padding: 10px 20px;
    border: none!important;
    color: white!important;
}
.btn-primary:hover {
    background: #46cdaa!important;
    border: none!important;    
    color: white!important;
}

.baidumap {
float: left;
width:100%;

}
.locsubmit{
border: 1px solid #1bba9a;
background: #1bba9a;
text-align: center;
color: #fff;
border-radius: 5px;
cursor: pointer;
font-size: 30px !important;
text-transform: capitalize;
 margin-top:50px;
}
#addlocation {
font-size: 23px;
padding: 20px 0px;
display: inline-block;
text-transform: capitalize;
margin-left: 60px;
}
.elementor-element-0653260 {width:1140px;margin-left:-25px;}
.elementor-element-e21e5f8 {width:1140px;margin-left:-25px;margin-top:-19px!important;}
.elementor-element-7461908 {width:1140px;margin-left:-25px;}
.elementor-element-a451f50 {width:1140px;margin-left:-25px;}
.elementor-element-743d3fc {width:1140px;margin-left:-25px;}
.elementor-element-02a506e {width:1140px;margin-left:-25px;}
.elementor-element-a7b0be5 {width:1140px;margin-left:-25px;}
.elementor-element-caa49fc {width:1140px;margin-left:-25px;}

/* .datehide {display:none} */
.defaultsection, .defaultsection2, .defaultsection3, .defaultsection4, .defaultsection5, .defaultsection6, .defaultsection7 {display:none;}
.defaultshow {display:block!important;}
/* Popup box BEGIN */
.hover_bkgr_fricc{
background:rgba(0,0,0,.4);
cursor:pointer;
display:none;
height:100%;
position:fixed;
text-align:center;
top:0;
width:100%;
z-index:10000;
left:0;
}
.hover_bkgr_fricc .helper{
display:inline-block;
height:100%;
vertical-align:middle;
}
.hover_bkgr_fricc > div {
background-color: #fff;
box-shadow: 10px 10px 60px #555;
display: inline-block;
height: auto;
max-width: 551px;
min-height: 100px;
vertical-align: middle;
width: 60%;
position: relative;
border-radius: 8px;
padding: 15px 5%;
}
.popupCloseButton {
background-color: #fff;
border: 3px solid #999;
border-radius: 50px;
cursor: pointer;
display: inline-block;
font-family: arial;
font-weight: bold;
position: absolute;
top: -20px;
right: -20px;
font-size: 25px;
line-height: 30px;
width: 30px;
height: 30px;
text-align: center;
}
.popupCloseButton:hover {
background-color: #ccc;
}
.trigger_popup_fricc {
cursor: pointer;
font-size: 20px;
margin: 20px;
display: inline-block;
font-weight: bold;
}

.carousel-control.left, .carousel-control.right {background-image:none;}

.glyphicon-chevron-left:before {
    content: url("https://www.travpart.com/English/wp-content/themes/bali/images/left.png");
    background-size: 50px;
    width: 50px;
    height: 50px;
    color: black;
    /*margin-left: -7.4em;*/
}
.glyphicon-chevron-right:before {
    content: url("https://www.travpart.com/English/wp-content/themes/bali/images/right.png");
    background-size: 50px;
    width: 50px;
    height: 50px;
    color: black;
   /* margin-right: -7.4em; */
}

.multi-item-carousel{width:93%;margin:0 auto;}

.multi-item-carousel .carousel-inner > .item {
  transition: 500ms ease-in-out left;
}
.multi-item-carousel .carousel-inner .active.left {
  left: -33%;
}
.multi-item-carousel .carousel-inner .active.right {
  left: 33%;
}
.multi-item-carousel .carousel-inner .next {
  left: 33%;
}
.multi-item-carousel .carousel-inner .prev {
  left: -33%;
}
.topbarmenu{margin-right:7px;}

#hotelsection, #diningsection, #transportsection, #guidesection, #recommendsection, #spassection, #boatsection, #lastsection{cursor: pointer}

@media all and (transform-3d), (-webkit-transform-3d) {
  .multi-item-carousel .carousel-inner > .item {
    transition: 500ms ease-in-out left;
    transition: 500ms ease-in-out all;
    -webkit-backface-visibility: visible;
            backface-visibility: visible;
    -webkit-transform: none!important;
            transform: none!important;
  }
}
.multi-item-carousel .carouse-control.left,
.multi-item-carousel .carouse-control.right {
  background-image: none;
}
button.searchButton2{height: 50px !important; margin-top:68px !important}


@media only screen and (max-width: 600px) {
.activity {margin:40px;}
.mobileapp{display:none!important;}
.container-fluid{padding-top:0!important;}
#menuresize{left:-11px;}
.total-tour-submit-wrapper{bottom:0px!important;}
#AddAnotherTravelItemManually{margin:0 25px!important;}
.v2-list{border:1px solid #ccc;margin-top:-40px;}
.v2-list textarea{width: 95%!important}
.v2-list .item_qty{width: 78%!important}
.v2-list .item_price{width: 87%!important}
.v2-list .item_date{width: 16%!important}
.img_video_upload{margin-left:10px;}
.manual-item-trash{margin-right:10px;}
.add_manualitem{width: 100%}

/* slider */
.carousel-inner .item{z-index: -1}
.multi-item-carousel .carousel-inner > .item{z-index: -1}
.travcust-pg-box .carousel-control.left{margin-left:-12px!important;z-index:99999;}
.travcust-pg-box .carousel-control.left .fa{position: absolute;top:120px;font-size:50px;}

.travcust-pg-box .carousel-control.right{margin-right:30px!important;z-index:99999;}
.travcust-pg-box .carousel-control.right .fa{position: absolute;top:120px;font-size:50px;}

/* Added for google map */
 #map {
        height: 425px;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
</style>
<input id="cs_RMB" type="hidden" value="<?php echo get_option('_cs_currency_RMD'); ?>" />
<input id="cs_USD" type="hidden" value="<?php echo get_option('_cs_currency_USD'); ?>" />
<input id="cs_AUD" type="hidden" value="<?php echo get_option('_cs_currency_AUD'); ?>" />
<input id="cs_price_ration" type="hidden" value="<?php echo get_option('person_price_ration'); ?>" />

<?php
$show_top_button=false;
if(is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $urow=$wpdb->get_row("SELECT userid,access FROM `user` WHERE `username`='{$current_user->user_login}'");
    if(!empty($urow->access) && $urow->access==1)
        $show_top_button=true;
}
?>
<div style="position: fixed;top:20px;left:50%;transform: translate(-50%);" class="mobileonly">
    <img src="https://www.travpart.com/English/wp-content/uploads/2018/11/logo2.png" alt="travcust" style="width:150px; margin:0 auto;display:block">
</div>
<div class="topbarmenu-outer" style="position: fixed; top: 5px; left:130px;z-index: 199999;margin-left: 160px;">
    <div class="topbarmenu">
        <!--<a href="#" class="btn btn-primary" style="padding:0;height:46px;position:absolute;left:-160px;border:none !important;"><img src="https://www.travpart.com/English/wp-content/uploads/2018/11/logo2.png" alt="" style="height:100%"></a>-->
        <?php if($show_top_button) { ?>
        <a class="btn btn-primary firstbackbtn" style="border:3px solid #226d82!important; background:#4ec3b7!important;cursor:pointer"  data-toggle="tooltip" title="delete and/or add more travel item"><span>Modify this tour package</span> </a>
      
        <a href="<?php echo empty($tour_id)?'#':(site_url().'/tour-details/?bookingcode=BALI'.$tour_id); ?>" <?php echo empty($tour_id)?'data-toggle="tooltip" title="You have not added anything yet"':'data-toggle="tooltip" title="Check my journey"'; ?>  class="btn btn-primary firstbackbtn">
        <span><?php echo empty($tour_id)?'':("(Booking Code BALI{$tour_id})"); ?> <img src="https://www.travpart.com/English/wp-content/uploads/2018/12/search.png" alt="Check my Journey" width="20"></span><!--<span class="tooltiptext">Check my journey</span>-->
        </a>
         
        <a href="<?php echo get_template_directory_uri().'/newtour.php'; ?>" data-toggle="tooltip" title="Create a new tour package" class="btn btn-primary firstbackbtn" style="text-decoration: none">
        <img src="https://www.travpart.com/English/wp-content/uploads/2018/11/add-list.png"  width="20">  <!--<span class="tooltiptext">Create a new tour package</span>--> </a>
        <?php } ?>

        <div class="mobileonly">
    <div style="margin-top:90px;margin-left:-24px">
        <a class="btn btn-primary firstbackbtn" style="width:145px;display:block;border:3px solid #226d82!important; background:#4ec3b7!important;cursor:pointer;margin-bottom:10px"  data-toggle="tooltip" title="delete and/or add more travel item"><span>Modify Package</span> </a>
      
        <div class="tooltip">
        <a href="<?php echo empty($tour_id)?'#':(site_url().'/tour-details/?bookingcode=BALI'.$tour_id); ?>" <?php echo empty($tour_id)?'data-toggle="tooltip" ':'data-toggle="tooltip"'; ?> class="btn btn-primary firstbackbtn">
        <span><?php echo empty($tour_id)?'':("(Booking Code BALI{$tour_id})"); ?> <img src="https://www.travpart.com/English/wp-content/uploads/2018/12/search.png" alt="Check my Journey" width="20"></span><!--<span class="tooltiptext">Check my journey</span>-->
        </a>
        </div>
         
        <div class="tooltip">
        <a href="<?php echo get_template_directory_uri().'/newtour.php'; ?>" class="btn btn-primary firstbackbtn" style="text-decoration: none">
        <img src="https://www.travpart.com/English/wp-content/uploads/2018/11/add-list.png" width="20">  <!--<span class="tooltiptext">Create a new tour package</span>--> </a>
        </div>
  </div>
</div>

    </div>
</div>

<?php if(get_post_meta(get_the_ID(), '_cs_logo', true)) { ?>
<img style="float:right; position:relative; top:3px; left:20px; width:90px; height:auto;" class="attachment-custom-size size-custom-size wp-post-image lazy" src="<?php echo wp_get_attachment_url(get_post_meta(get_the_ID(), '_cs_logo', true)); ?>" />
<?php } ?>
<?php
if(have_posts()) {
  while(have_posts()) {
    the_post();
    the_content();
    //echo str_replace(array('[meal_part]','[car_part]'), array($meal_list_html, 'car_list_html'), get_the_content());
    ?>


<div class="hover_bkgr_fricc">
    <span class="helper"></span>
    <div class="mobilelogin">
        <?php echo do_shortcode('[ultimatemember form_id=14064]'); ?>

      <div class="row">
        <div class="col-lg-6">
          <a href="https://www.travpart.com/"><img style="width: 100%;" src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png" /></a>
        </div>
        <div class="col-lg-6">
          <a href="https://www.tourfrombali.com"><img style="width: 100%;" src="https://www.tourfrombali.com/wp-content/themes/bali/images/logo-white.png" /></a>
        </div>
      </div>
      </div>
  </div>
  
<?php if($show_top_button) { ?>
<div id="steps-widget" class="steps-widget">
    <a class="steps-widget-close"><i class="fa fa-times"></i> </a>
  <div class="steps-item active">
    <div class="steps-item-num">1</div>
    <div class="steps-item-title">Step 1</div>
    <div class="steps-item-des">This is the page where you add your travel items to the cart</div>
  </div>
  <div class="steps-item">
    <div class="steps-item-num">2</div>
    <div class="steps-item-title">Step 2</div>
    <div class="steps-item-des">This is the page where you can recheck your travel price detail</div>
  </div>
  <div class="steps-item">
    <div class="steps-item-num">3</div>
    <div class="steps-item-title">Step 3</div>
    <div class="steps-item-des">This is the page where you can write your itinerary schedule and add pictures/videos</div>
  </div>
</div>
<script>
    var sw = jQuery('#steps-widget');
    jQuery('body').append(sw);
    jQuery('.step-header').show();
    jQuery('.steps-widget-close').on('click',function (e) {
        e.preventDefault();
        jQuery('.steps-widget').hide();
    });
    jQuery('.step-header').on('click',function (e) {
        e.preventDefault();
        jQuery('.steps-widget').show();
    });
</script>
<?php } ?>


<script>
function generateString(oldString, index, newVal, splitchar){
  var arr=oldString.split(splitchar);
  var str='';
  if(arr.length>0) {
    arr[index]=newVal;
    for(var i=0; i<arr.length-1; i++) {
      str+=arr[i]+splitchar;
    }
    return str;
  }
  else {
    return oldString;
  }
}

function trimStr(str, c) {
  if(str[str.length-1]==c)
    return str.substring(0, str.length-1);
  else
    return str;
}

//Time limit for hotel section startdate and enddate
var date_now=new Date();
var date_month=date_now.getMonth()+1<10?"0"+(date_now.getMonth()+1):(date_now.getMonth()+1);
var date_day=date_now.getDate()<10?"0"+date_now.getDate():date_now.getDate();
$('#startdate').attr('min', date_now.getFullYear()+'-'+date_month+'-'+date_day);
$('#enddate').attr('min', date_now.getFullYear()+'-'+date_month+'-'+date_day);

$('#startdate').change(function() {
    var startdate_timestamp=Date.parse($('#startdate').val());
    var enddate_timestamp=Date.parse($('#enddate').val());
    if(enddate_timestamp<startdate_timestamp)
        $('#enddate').val('');
    $('#enddate').attr('min', $('#startdate').val());
});

//Time limit for car adivaha section pickdate and returndate
var date_now=new Date();
var date_month=date_now.getMonth()+1<10?"0"+(date_now.getMonth()+1):(date_now.getMonth()+1);
var date_day=date_now.getDate()<10?"0"+date_now.getDate():date_now.getDate();
$('#carPickupDate').attr('min', date_now.getFullYear()+'-'+date_month+'-'+date_day);
$('#carReturnDate').attr('min', date_now.getFullYear()+'-'+date_month+'-'+date_day);

$('#carPickupDate').change(function() {
    var startdate_timestamp=Date.parse($('#carPickupDate').val());
    var enddate_timestamp=Date.parse($('#carReturnDate').val());
    if(enddate_timestamp<startdate_timestamp)
        $('#carReturnDate').val('');
    $('#carReturnDate').attr('min', $('#carPickupDate').val());
});

<?php /*
$budget_used=$wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m')");
if( floatval($budget_used)<=floatval(get_option('budget_limit')) ) {
?>
    document.getElementById('howToEarn').style.display='block';
    new Tooltip(document.getElementById('howToEarn'),{title:'<h5><i class="fas fa-info-circle"></i> The submission criteria to earn $5/hour package is you must include at least:</h5>- 1 Hotel/Accomodation<br>- 1 any other activities.<br>',placement:'bottom',html:true,container:'body',template:'<div class="popper-tooltip popper-tooltip-style1" role="tooltip"><div class="tooltip__arrow"></div><div class="tooltip__inner"></div></div>'});
<?php } */?>

jQuery(document).ready(function(){
  
<?php if(!is_user_logged_in()) {
  echo 'callsignin();';
}
?>

$('.total-cost .change_price').attr('or_pic', parseInt($('.total-cost .change_price').last().attr('or_pic')) );

$('.delivery-step').on('click','.datepicker-tour-dates-select',function(){
    if($(this).find('option').length<1)
        $('.open-tour-day-popup').click();
});

jQuery('.mealimg').click(function() {
  jQuery('.mealimg').next().val(0);
  jQuery(this).next().val(1);
    jQuery('.mealimg').css('border', '5px solid white');
    jQuery(this).css('border', '5px solid #0cad83');
    jQuery('.mealimg').css('opacity', '0.7');
    jQuery(this).css('opacity', '1');
});
$('.typeChoose input').click(function(){
  var tbody=$(this).parent().parent().parent();
    if($(this).val()=='Meals')
  {
    tbody.find('.Meals').show();
    tbody.find('.Breakfast,.Beverage,.Appetizer').hide();
  }
  else if($(this).val()=='Breakfast')
  {
    tbody.find('.Breakfast').show();
    tbody.find('.Meals,.Beverage,.Appetizer').hide();
  }
  else if($(this).val()=='Beverage')
  {
    tbody.find('.Beverage').show();
    tbody.find('.Meals,.Breakfast,.Appetizer').hide();
  }
  else if($(this).val()=='Appetizer')
  {
    tbody.find('.Appetizer').show();
    tbody.find('.Meals,.Breakfast,.Beverage').hide();
  }
});
/*$('#meal_confirm').click(function(){
  console.log('Meal confirm');
  var i=1,subitem='';
  jQuery('.meal-subitem').each(function(){
    if(jQuery(this).parent().find('.meal_selected').val()==1)
    {
      jQuery(this).find('.Meals input,.Breakfast input,.Beverage input,.Appetizer input').each(function(){
        if(jQuery(this).prop('checked')===true)
          subitem+='-1'
        else
          subitem+='-0';
      });
    }
  });
  jQuery('.mealimg').next().each(function(){
    if(jQuery(this).val()==1)
    {
      $('#meal_type').val($('#meal_type').val()+$(this).attr('mealid')+subitem+',');
      $('#meal_date').val($('#meal_date').val()+$('#meal_date_choose').contents().find('#selected_date').text()+',');
      $('#meal_time').val($('#meal_time').val()+$('#meal_date_choose').contents().find('#selected_time').text()+',');
      $('#meal_alert_message').show();
      setTimeout("$('#meal_alert_message').hide()", 1800);
    }
    i++;
  });
});*/
$('#meal-add-trav').click(function(){  
  var i=1,subitem='';
  var meal_type='';
  var meal_date='';
  var meal_time='';
  var mealindex=-1;
  var meal_price=0;
  if( getCookie('meal_type')!='' && getCookie('meal_date')!='' && getCookie('meal_time')!='' ) {
      meal_type=getCookie('meal_type');
      meal_date=getCookie('meal_date');
      meal_time=getCookie('meal_time');
      if(getCookie('meal_price')!='')
        meal_price+=parseInt(getCookie('meal_price'));
  }
  jQuery('.meal-sub-item').each(function(){
    if(jQuery(this).parent().hasClass('active'))
    {
      jQuery(this).find('.Meals input,.Breakfast input,.Beverage input,.Appetizer input').each(function(){
        if(jQuery(this).prop('checked')===true) {
          subitem+='-1';
          meal_price+=parseInt($(this).parent().parent().find('.change_price').attr('or_pic'));
          $('.total-cost .change_price').attr('or_pic', (parseInt($('.total-cost .change_price').attr('or_pic'))+parseInt($(this).parent().parent().find('.change_price').attr('or_pic'))) );
        }
        else
          subitem+='-0';
      });

    if( getCookie('mealindex')=='' || parseInt(getCookie('mealindex'))<0) {
      meal_type+=parseInt($(this).attr('item'))+subitem+',';
      meal_date+=$('#meal-step-3 .datepicker-tour-dates-select').val()+',';
      meal_time+=$('#meal-step-3 .data-time').text()+',';
    }
    else {
      mealindex=parseInt(getCookie('mealindex'));
      meal_type=generateString(meal_type, mealindex, (parseInt($(this).attr('item'))+subitem), ',');
      meal_date=generateString(meal_date, mealindex, $('#meal-step-3 .datepicker-tour-dates-select').val(), ',');
      meal_time=generateString(meal_time, mealindex, $('#meal-step-3 .data-time').text(), ',');
      setCookie('mealindex', '', 1);
    }
    
    $('.mealsection .change_price').attr('or_pic', meal_price);
    $(".header_curreny").change();
    
    setCookie('meal_type', meal_type, 30);
    setCookie('meal_date', meal_date, 30);
    setCookie('meal_time', meal_time, 30);
    setCookie('meal_price', meal_price, 30);
    setCookie('update', 1, 1);
    //Skip the next items
    return false;
    }
  });
});

$('#restaurants-add-trav').click(function(){
    var restaurant_name='';
    var restaurant_local='';
    var restaurant_img='';
    var restaurant_price='';
    var restaurant_diners='';
    var restaurant_date='';
    var restaurant_time='';
    var current_price=0;
    if( getCookie('restaurant_name')!='' && getCookie('restaurant_local')!=''
        && getCookie('restaurant_price')!='' && getCookie('restaurant_diners')!=''
        && getCookie('restaurant_date')!='' && getCookie('restaurant_time')!='' ) {
        restaurant_name=getCookie('restaurant_name');
        restaurant_local=getCookie('restaurant_local');
        restaurant_img=getCookie('restaurant_img');
        restaurant_diners=getCookie('restaurant_diners');
        restaurant_price=getCookie('restaurant_price');
        restaurant_date=getCookie('restaurant_date');
        restaurant_time=getCookie('restaurant_time');
    }
    restaurant_name+=$.trim($('#restaurants-step-5 .rest-name').text())+'--';
    restaurant_local+=$.trim($('#restaurants-step-5 .rest-local').text())+'--';
    restaurant_img+=$('#restaurants-step-5 .restaurant-img').css('background-image').replace('url("','').replace('")','')+'--';
    current_price=parseInt($('#restaurants-step-5 .change_price').attr('or_pic'));
    restaurant_price+=(isNaN(current_price)?0:current_price)+',';
    restaurant_diners+=$('#restaurant-p-number').val()+',';
    restaurant_date+=$('#restaurants-step-4 .datepicker-tour-dates-select').val()+',';
    restaurant_time+=$('#restaurants-step-4 .data-time').text()+',';
    
    setCookie('restaurant_name', restaurant_name, 30);
    setCookie('restaurant_local', restaurant_local, 30);
    setCookie('restaurant_img', restaurant_img, 30);
    setCookie('restaurant_price', restaurant_price, 30);
    setCookie('restaurant_diners', restaurant_diners, 30);
    setCookie('restaurant_date', restaurant_date, 30);
    setCookie('restaurant_time', restaurant_time, 30);
    setCookie('update', 1, 1);
});
  
jQuery('.carimg').click(function() {
  jQuery('.carimg').next().val(0);
  jQuery(this).next().val(1);
  jQuery('.carimg').css('border', '5px solid white');
    jQuery(this).css('border', '5px solid #0cad83');
    jQuery('.carimg').css('opacity', '0.7');
    jQuery(this).css('opacity', '1');
});
/*
// silky
jQuery('.oncarpricefullday').click(function(){
  jQuery('.oncarpricefullday').find(".change_price").css('border', '2px solid #ccc');
  jQuery('.oncarpricehalfday').find(".change_price").css('border', '2px solid #ccc');
  jQuery(this).find(".change_price").css('border', '2px solid red').addClass("selectedCarPrice");
});

jQuery('.oncarpricehalfday').click(function(){
  jQuery('.oncarpricehalfday').find(".change_price").css('border', '2px solid #ccc');
  jQuery('.oncarpricefullday').find(".change_price").css('border', '2px solid #ccc');
  jQuery(this).find(".change_price").css('border', '2px solid red').addClass("selectedCarPrice");
});
// end silky
*/
/*jQuery('.car_full_price,.car_half_price').click(function() {
  $(this).parent().parent().parent().find('.carimg').click();
  $('.car_full_price,.car_half_price').css('background','#1076c8');
  $(this).css('background','#1dab84');
  $('.car_full_price,.car_half_price').attr('choosed','0');
  $(this).attr('choosed','1');
});
jQuery('#car_confirm').click(function(){
console.log("car selected");
  var i=1;
  jQuery('.carimg').next().each(function(){
    if(jQuery(this).val()==1)
    {
    if(jQuery(this).parent().find('.car_half_price').attr('choosed')=='1')
      $('#car_type').val($('#car_type').val()+(i+0.5)+',');
    else
      $('#car_type').val($('#car_type').val()+i+',');
      //jQuery('#selectedCarPricehiden').val(jQuery('#selectedCarPrice').attr("or_pic")+i+',');
      //jQuery('#car_price').val(jQuery('#car_price').val()+i+',');
      jQuery('#car_date').val(jQuery('#car_date').val()+jQuery('#car_date_choose').contents().find('#selected_date').text()+',');
      jQuery('#car_time').val(jQuery('#car_time').val()+jQuery('#car_date_choose').contents().find('#selected_time').text()+',');
      jQuery('#car_alert_message').show();
      setTimeout("$('#car_alert_message').hide()", 1800);
    }
    i++;
  });
});
*/
$('#car_confirm').click(function(){ 
  var i=1;
  var car_type='';
  var car_date='';
  var car_time='';
  var car_price=0;
  var carindex=-1;
  if( getCookie('car_type')!='' && getCookie('car_date')!='' && getCookie('car_time')!='' ) {
      car_type=getCookie('car_type');
      car_date=getCookie('car_date');
      car_time=getCookie('car_time');
      car_price+=parseInt(getCookie('car_price'));
      if(isNaN(car_price))
          car_price=0;
  }
  
  if( getCookie('carindex')=='' || parseInt(getCookie('carindex'))<0) {
  $('.car-block').each(function() {
      if($(this).find('.car-block-top').hasClass('active')) {
          if($(this).find('.delivery-right').hasClass('active')) {
              car_type+=(i+0.5)+',';
              car_price+=parseInt($(this).find('.car_half_price').attr('or_pic'));
              $('.total-cost .change_price').attr('or_pic', (parseInt($('.total-cost .change_price').attr('or_pic'))+parseInt($(this).find('.car_half_price').attr('or_pic')) ) );
          }
          else {
              car_type+=i+',';
              car_price+=parseInt($(this).find('.car_full_price').attr('or_pic'));
              $('.total-cost .change_price').attr('or_pic', (parseInt($('.total-cost .change_price').attr('or_pic'))+parseInt($(this).find('.car_full_price').attr('or_pic')) ) );
          }
          car_date+=$('#delivery-step-3 .datepicker-tour-dates-select').val()+',';
          car_time+=$('#delivery-step-3 .data-time').text()+',';
          $('#delivery-added-alert').show();
          setTimeout("$('#delivery-added-alert').hide()", 1800);
      }
      
      i++;
  });
  }
  else {
  carindex=parseInt(getCookie('carindex'));
  $('.car-block').each(function() {
      if($(this).find('.car-block-top').hasClass('active')) {
          if($(this).find('.delivery-right').hasClass('active')) {
              car_type=generateString(car_type, carindex, (i+0.5), ',');
              car_price+=parseInt($(this).find('.car_half_price').attr('or_pic'));
          }
          else {
              car_type=generateString(car_type, carindex, i, ',');
              car_price+=parseInt($(this).find('.car_full_price').attr('or_pic'));
          }
          car_date=generateString(car_date, carindex, $('#delivery-step-3 .datepicker-tour-dates-select').val(), ',');
          car_time=generateString(car_time, carindex, $('#delivery-step-3 .data-time').text(), ',');
          $('#delivery-added-alert').show();
          setTimeout("$('#delivery-added-alert').hide()", 1800);
      }
      
      i++;
  });
  setCookie('carindex', '', 1);
  }
  
  if(isNaN(car_price))
    car_price=0;
  $('.carsection .change_price').attr('or_pic', car_price);
  $(".header_curreny").change();
  
  setCookie('car_type', car_type, 30);
  setCookie('car_date', car_date, 30);
  setCookie('car_time', car_time, 30);
  setCookie('car_price', car_price, 30);
  setCookie('update', 1, 1);

});

jQuery("#zomato_confirm").click(function(){
      var zomatohoteldata = jQuery("#hiddenfieldforzomatorest").val();
      jQuery('#zomato_alert_message').show();
      setTimeout("jQuery('#zomato_alert_message').hide()", 1800);

});

jQuery('.popupguideimg').click(function() {
	jQuery('.popupguideimg').next().next().val(0);
	jQuery(this).next().next().val(1);
	jQuery('.popupguideimg').css('opacity', '0.7');
	jQuery(this).css('opacity', '1');
	jQuery('.popupguideimg').css('border', '5px solid white');
	jQuery(this).css('border', '5px solid #0cad83');
});
/*$('#popupguide_confirm').click(function(){  
  var i=1;
  jQuery('.popupguideimg').next().next().each(function(){
    if(jQuery(this).val()==1)
    {
      $('#popupguide_type').val($('#popupguide_type').val()+i+',');
      $('#popupguide_date').val($('#popupguide_date').val()+$('#popupguide_date_choose').contents().find('#selected_date').text()+',');
      $('#popupguide_time').val($('#popupguide_time').val()+$('#popupguide_date_choose').contents().find('#selected_time').text()+',');
      $('#popupguide_alert_message').show();
      setTimeout("$('#popupguide_alert_message').hide()", 1800);
    }
    i++;
  });
});*/
$('#popupguide_confirm').click(function(){
  var i=1;
  var popupguide_type='';
  var popupguide_date='';
  var popupguide_time='';
  var guide_price=0;
  var guideindex=-1;
  if( getCookie('popupguide_type')!='' && getCookie('popupguide_date')!='' && getCookie('popupguide_time')!='' ) {
      popupguide_type=getCookie('popupguide_type');
      popupguide_date=getCookie('popupguide_date');
      popupguide_time=getCookie('popupguide_time');
      if(getCookie('guide_price')!='')
          guide_price+=parseInt(getCookie('guide_price'));
  }
  
  $('.popupgui-block').each(function() {
      if($(this).hasClass('active')) {
      if( getCookie('guideindex')=='' || parseInt(getCookie('guideindex'))<0) {
        popupguide_type+=i+',';
        popupguide_date+=$('#guide-step-3 .datepicker-tour-dates-select').val()+',';
        popupguide_time+=$('#guide-step-3 .data-time').text()+',';
        guide_price+=parseInt($(this).find('.change_price').attr('or_pic'));
        $('.total-cost .change_price').attr('or_pic', (parseInt($('.total-cost .change_price').attr('or_pic'))+parseInt($(this).find('.change_price').attr('or_pic')) ) );
      }
      else {
        guideindex=parseInt(getCookie('guideindex'));
        popupguide_type=generateString(popupguide_type, guideindex, i, ',');
        popupguide_date=generateString(popupguide_date, guideindex, $('#guide-step-3 .datepicker-tour-dates-select').val(), ',');
        popupguide_time=generateString(popupguide_time, guideindex, $('#guide-step-3 .data-time').text(), ',');
        setCookie('guideindex', '', 1);
      }
          
          $('#guide-success-alert').show();
          setTimeout("$('#guide-success-alert').hide()", 1800);
      console.log('Add guide successfully');
      }
      
      i++;
  });
  
  $('.guidesection .change_price').attr('or_pic', guide_price);
  $(".header_curreny").change();
  
  setCookie('popupguide_type', popupguide_type, 30);
  setCookie('popupguide_date', popupguide_date, 30);
  setCookie('popupguide_time', popupguide_time, 30);
  setCookie('guide_price', guide_price, 30);
  setCookie('update', 1, 1);

});
  
jQuery('.spaimg').click(function() {
  jQuery('.spaimg').next().val(0);
  jQuery(this).next().val(1);
    jQuery('.spaimg').css('opacity', '0.7');
    jQuery(this).css('opacity', '1');
    jQuery('.spaimg').css('border', '5px solid white');
    jQuery(this).css('border', '5px solid #0cad83');
});

$('#spa-add-trav').click(function(){ 
    var i=1,datecount=0,np=0,subitem='';
    var spa_type='';
    var spa_num=[];
    var spa_date='';
    var spa_time='';
    var spaindex=-1;
    var alert=false;
    var spa_price=0;
  
  if( getCookie('spaindex')!='' && parseInt(getCookie('spaindex'))>=0) {
    spaindex=parseInt(getCookie('spaindex'));
  }
  
    if( getCookie('spa_type')!='' && getCookie('spa_date')!='' && getCookie('spa_time')!='' && spaindex<0 ) {
        spa_type=getCookie('spa_type');
        spa_date=getCookie('spa_date');
        spa_time=getCookie('spa_time');
        if(getCookie('spa_price')!='')
            spa_price+=parseInt(getCookie('spa_price'));
    }
  
	  $('#spa-step-2 .res-num-select .spa-num').each(function() {
	    spa_num.push($(this).val());
	  });

    $('#spa-step-1').find('.spa-block input').each(function() {
      if(parseInt($(this).attr('item'))>i) {
      if(alert) {
      spa_type+=i+subitem+',';
      alert=false;
      }
      i=parseInt($(this).attr('item'));
          subitem='';
          datecount++;
      }
      
      if(jQuery(this).prop('checked')===true) {
      subitem+='-'+spa_num[np];
      spa_price+=Math.round(parseInt($(this).parent().parent().parent().find('.change_price').attr('or_pic'))*spa_num[np]);
      $('.total-cost .change_price').attr('or_pic', (parseInt($('.total-cost .change_price').attr('or_pic'))+Math.round(parseInt($(this).parent().parent().parent().find('.change_price').attr('or_pic'))*spa_num[np]) ) );
      np++;
          alert=true;
      }
      else {
          subitem+='-0';
      }
    });
    
    if(alert==true) {
      $('#spa-success-alert').show();
      setTimeout("$('#spa-success-alert').hide()", 1800);
      spa_type+=i+subitem+',';
      datecount++;
    }
    
    for(var e=0; e<datecount; e++){
      spa_date+=$('#spa-step-3 .datepicker-tour-dates-select').val()+',';
      spa_time+=$('#spa-step-3 .data-time').text()+',';
    }

  if(spaindex>=0) {
    spa_type=generateString(getCookie('spa_type'), spaindex, trimStr(spa_type,','), ',');
    spa_date=generateString(getCookie('spa_date'), spaindex, trimStr(spa_date,','), ',');
    spa_time=generateString(getCookie('spa_time'), spaindex, trimStr(spa_time,','), ',');
    setCookie('spaindex', '', 1);
  }
    
    $('.spasection .change_price').attr('or_pic', spa_price);
    $(".header_curreny").change();
    
    setCookie('spa_type', spa_type, 30);
    setCookie('spa_date', spa_date, 30);
    setCookie('spa_time', spa_time, 30);
    setCookie('spa_price', spa_price, 30);
    setCookie('update', 1, 1);
    
});

$('#helicopter-add-trav').click(function(){ 
    var i=1,datecount=0,np=0,subitem='';
    var boat_type='';
    var boat_num=[];
    var boat_date='';
    var boat_time='';
    var boatindex=-1;
    var alert=false;
    var boat_price=0;
    
    if( getCookie('boatindex')!='' && parseInt(getCookie('boatindex'))>=0) {
        boatindex=parseInt(getCookie('boatindex'));
    }
    
    if( getCookie('boat_type')!='' && getCookie('boat_date')!='' && getCookie('boat_time')!='' && boatindex<0 ) {
        boat_type=getCookie('boat_type');
        boat_date=getCookie('boat_date');
        boat_time=getCookie('boat_time');
        if(getCookie('boat_price')!='')
            boat_price+=parseInt(getCookie('boat_price'));
    }
    
    $('#helicopter-step-2 .res-num-select .helicopter-num').each(function() {
        boat_num.push($(this).val());
    });

    $('#helicopter-step-1').find('.helicopter-block input').each(function() {
        if(parseInt($(this).attr('item'))>i) {
            if(alert) {
                boat_type+=i+subitem+',';
                alert=false;
            }
            i=parseInt($(this).attr('item'));
            subitem='';
            datecount++;
        }
        
        if(jQuery(this).prop('checked')===true) {
            subitem+='-'+boat_num[np];
            boat_price+=Math.round(parseInt($(this).parent().parent().parent().find('.change_price').attr('or_pic'))*boat_num[np]);
            $('.total-cost .change_price').attr('or_pic', (parseInt($('.total-cost .change_price').attr('or_pic'))+Math.round(parseInt($(this).parent().parent().parent().find('.change_price').attr('or_pic'))*boat_num[np]) ) );
            np++;
            alert=true;
        }
        else {
            subitem+='-0';
        }
    });
    
    if(alert==true) {
        boat_type+=i+subitem+',';
        datecount++;
    }
    
    for(var e=0; e<datecount; e++){
        boat_date+=$('#helicopter-step-3 .datepicker-tour-dates-select').val()+',';
        boat_time+=$('#helicopter-step-3 .data-time').text()+',';
    }
    
    if(boatindex>=0) {
        boat_type=generateString(getCookie('boat_type'), boatindex, trimStr(boat_type,','), ',');
        boat_date=generateString(getCookie('boat_date'), boatindex, trimStr(boat_date,','), ',');
        boat_time=generateString(getCookie('boat_time'), boatindex, trimStr(boat_time,','), ',');
        setCookie('boatindex', '', 1);
    }
    
    $('.boatsection .change_price').attr('or_pic', boat_price);
    $(".header_curreny").change();
    
    setCookie('boat_type', boat_type, 30);
    setCookie('boat_date', boat_date, 30);
    setCookie('boat_time', boat_time, 30);
    setCookie('boat_price', boat_price, 30);
    setCookie('update', 1, 1);
    
});

$('#ext-add-trav').click(function(){ 
    var np=0;
    var eaiitem='';
    var eainum='';
    var eainumarr=[];
    var eaidate='';
    var eaitime='';
    var activityindex=-1;
    var activity_price=0;
    
    if( getCookie('activityindex')!='' && parseInt(getCookie('activityindex'))>=0) {
        activityindex=parseInt(getCookie('activityindex'));
    }
    
    if( getCookie('eaiitem')!='' && getCookie('eainum')!='' && getCookie('eaidate')!='' && getCookie('eaitime')!='' && activityindex<0 ) {
        eaiitem=getCookie('eaiitem');
        eainum=getCookie('eainum');
        eaidate=getCookie('eaidate');
        eaitime=getCookie('eaitime');
        if(getCookie('activity_price')!='')
            activity_price+=parseInt(getCookie('activity_price'));
    }
    
    $('#ext-step-2 .res-num-select .ext-num').each(function() {
        eainum+=$(this).val()+',';
        eainumarr.push($(this).val());
    });
    
    $('#ext-step-1').find('.ext-block input').each(function() {     
        if($(this).prop('checked')===true) {
            eaiitem+=parseInt($(this).attr('item'))+',';
            activity_price+=Math.round(parseInt($(this).parent().parent().parent().find('.change_price').attr('or_pic'))*eainumarr[np]);
            $('.total-cost .change_price').attr('or_pic', (parseInt($('.total-cost .change_price').attr('or_pic'))+Math.round(parseInt($(this).parent().parent().parent().find('.change_price').attr('or_pic'))*eainumarr[np]) ) );
            np++;
        }
    });
    
    for(var e=0; e<np; e++){
        eaidate+=$('#ext-step-3 .datepicker-tour-dates-select').val()+',';
        eaitime+=$('#ext-step-3 .data-time').text()+',';
    }
    
    if(activityindex>=0) {
        eaiitem=generateString(getCookie('eaiitem'), activityindex, trimStr(eaiitem,','), ',');
        eainum=generateString(getCookie('eainum'), activityindex, trimStr(eainum,','), ',');
        eaidate=generateString(getCookie('eaidate'), activityindex, trimStr(eaidate,','), ',');
        eaitime=generateString(getCookie('eaitime'), activityindex, trimStr(eaitime,','), ',');
        setCookie('activityindex', '', 1);
    }
    
    $('.activitysection .change_price').attr('or_pic', activity_price);
    $(".header_curreny").change();
    
    setCookie('eaiitem', eaiitem, 30);
    setCookie('eainum', eainum, 30);
    setCookie('eaidate', eaidate, 30);
    setCookie('eaitime', eaitime, 30);
    setCookie('activity_price', activity_price, 30);
    setCookie('update', 1, 1);
    
});  
  
  
//recommended place
var media_urll=null;
var m=new Array();
var locationListInit=false;
var rmd_country_id='country:93';
$('#rmd_country_search').click(function () {
    if (!!$('#rmd_country_name').val()) {
        $.getJSON('/English/wp-content/themes/bali/api.php', {
            action: 'get_location_id',
            name: $('#rmd_country_name').val()
        }).done(function (data) {
            if (data[0] != undefined) {
                rmd_country_id = data[0].id;
                $('#place_categories').change();
            }
        });
    }
});
$('#place_categories').change(function () {

    var media_url2 = new Array();
    var temp = new Array();
    var desc = new Array();
    var ratingarr = new Array();

    $('#loading-image').show();

    jQuery.getJSON('/English/wp-content/themes/bali/api.php', {
        action: 'getlocation',
        category: $('#place_categories').val(),
        country_id: rmd_country_id
    }).done(function (data) {
        console.log(data);
        $('#loading-image').hide();
        if (data.error_msg == undefined || data.error_msg.length < 1) {
        $('#location_list>option').remove();
        $('#location_list').parent().css('display', 'block');

        $.each(data.data.places, function (index, place) {
            $('#location_list').append('<option class="recommendplaces" rating="' + place.rating_local + '" id="' + place.id + '" perex="' + place.perex + '" media_url="' + place.thumbnail_url + '" value="' + place.name + ', ' + place.name_suffix + '">' + place.name + ', ' + place.name_suffix + '</option>');

            temp.push(place.id);
            desc.push(place.perex);
            ratingarr.push(place.rating_local);

        });

        $('#loading-image').show();

        jQuery.getJSON('/English/wp-content/themes/bali/mediaapi.php', {
            action: 'getimages',
            placeid: temp[0]
        }).done(function (data) {
            $('#loading-image').hide();
            $.each(data.data.media, function (index, media) {
                media_url2.push(media.url);
                $.each(media_url2, function (k, v) {
                    if (k == 0) {
                        media_urll = v;
                        media_url = media_urll;
                        perex = desc[0];
                        rating = ratingarr[0];

                        if (media_url.indexOf('http') == -1) {
                            $('#location_img').attr('src', 'https://www.travpart.com/English/wp-content/uploads/2018/05/So-much-to-discover.jpg');
                        } else {
                            $('#location_img').attr('src', media_url);
                        }

                        if (perex == "null") {
                            $('#location_description').text('Description Content');
                        } else {
                            $('#location_description').text(perex);
                        }

                        if (rating == "null") {
                            $('#locRating').text('No Rating');
                        } else {
                            $('#locRating').text(rating);
                        }

                    }
                });
            });

            m = media_url2;
      var html;

            for (var i = 0; i < 6; i++) {
                if (i == 0) {
                    $('#recommendplace').empty();
                }

        if(m[i]!=undefined) {
                    html = '<div id="recommenditem" class="item"><div class="col-xs-12 col-sm-6 col-md-2"><img id="placeimg' + i + '" src="' + m[i] + '" style="width:200px;height:150px;"></div></div>';
                    $('#recommendplace').append(html);
                }

            }

            $('#recommenditem').first().addClass('active');

            for (var i = 0; i < 6; i++) {

                $('#placeimg' + i).on("click", function () {
                    var src = $(this).attr('src');
                    console.log(src);

                    $('#location_img').attr('src', src);

                });
            }


            (function () {
                $('.carousel-showmanymoveone .item').each(function () {
                    console.log('again working 631');
                    var itemToClone = $(this);

                    for (var i = 1; i < 6; i++) {
                        itemToClone = itemToClone.next();

                        // wrap around if at end of item collection
                        if (!itemToClone.length) {
                            itemToClone = $(this).siblings(':first');
                        }

                        // grab item, clone, add marker class, add to collection
                        itemToClone.children(':first-child').clone()
                            .addClass("cloneditem-" + (i))
                            .appendTo($(this));
                    }
                });
            }());

        });

        $('#location_list').change();
        }
    });

});
//init place list data
$('#recommendsection').click(function(){
    if(!locationListInit) {
        $('#place_categories').change();
        locationListInit=true;
    }
});


$('#location_list').change(function () {
    $('#loading-image').show();
    var media_url3 = new Array();
    var m2 = new Array();
    //getting images

    jQuery.getJSON('/English/wp-content/themes/bali/mediaapi.php', {
        action: 'getimages',
        placeid: $(this).find('option:selected').attr('id')
    }).done(function (data) {
        $('#loading-image').hide();

        $.each(data.data.media, function (index, media) {
            media_url3.push(media.url);

            $.each(media_url3, function (k, v) {
                if (k == 0) {
                    media_urll = v;
                    //console.log( "Key: " + k + ", Value: " + media_urll);
                    media_url = media_urll;
                    perex = $(this).find('option:selected').attr('perex')
                    rating = $(this).find('option:selected').attr('rating')
                    if (media_url.indexOf('http') == -1) {
                        $('#location_img').attr('src', 'https://www.travpart.com/English/wp-content/uploads/2018/05/So-much-to-discover.jpg');
                    }
                    else {
                        $('#location_img').attr('src', media_url);
                    }
                    if (perex == "null") {
                        $('#location_description').text('Description Content');
                    }
                    else {
                        $('#location_description').text(perex);
                    }
                }
                if (rating == "null") {
                    $('#locRating').text('No Rating');
                } else {
                    $('#locRating').text(rating);
                }
            });

        });

        m2 = media_url3;
    var html;

        for (var i = 0; i < 6; i++) {
            
            if (i == 0) {
                $('#recommendplace').empty();
            }

      if(m2[i]!=undefined) {
        html = '<div id="recommenditem" class="item"><div class="col-xs-12 col-sm-6 col-md-2"><img id="placeimg' + i + '" src="' + m2[i] + '" style="width:200px;height:150px;"></div></div>';
        $('#recommendplace').append(html);
      }

        }

        $('#recommenditem').first().addClass('active');

        for (var i = 0; i < 6; i++) {

            $('#placeimg' + i).on("click", function () {

                var src = $(this).attr('src');
                console.log(src);

                $('#location_img').attr('src', src);

            });

        }

        (function () {
            $('.carousel-showmanymoveone .item').each(function () {
                console.log('again working 781');
                var itemToClone = $(this);

                for (var i = 1; i < 6; i++) {
                    itemToClone = itemToClone.next();

                    // wrap around if at end of item collection
                    if (!itemToClone.length) {
                        itemToClone = $(this).siblings(':first');
                    }

                    // grab item, clone, add marker class, add to collection
                    itemToClone.children(':first-child').clone()
                        .addClass("cloneditem-" + (i))
                        .appendTo($(this));
                }
            });
        }());

    });

});

$('#travel-location-add').click(function(){
  var place_img='';
  var place_name='';
  var place_date='';
  var place_time='';
  var placeindex=-1;
  if( getCookie('place_img')!='' && getCookie('place_name')!='' && getCookie('place_date')!='' && getCookie('place_time')!='' ) {
      place_img=getCookie('place_img');
      place_name=getCookie('place_name');
      place_date=getCookie('place_date');
      place_time=getCookie('place_time');
  }
  if($('#location_list>option:selected').val()!="null")
  {
    if( getCookie('placeindex')=='' || parseInt(getCookie('placeindex'))<0) {
      place_img+=$('#location_img').attr('src')+',';
      place_name+=$('#location_list>option:selected').val()+'--';
      place_date+=$('#travel-location-step-2 .datepicker-tour-dates-select').val()+',';
      place_time+=$('#travel-location-step-2 .data-time').text()+',';
    }
    else {
      placeindex=parseInt(getCookie('placeindex'));
      place_img=generateString(place_img, placeindex, $('#location_img').attr('src'), ',');
      place_name=generateString(place_name, placeindex, $('#location_list>option:selected').val(), '--');
      place_date=generateString(place_date, placeindex, $('#travel-location-step-2 .datepicker-tour-dates-select').val(), ',');
      place_time=generateString(place_time, placeindex, $('#travel-location-step-2 .data-time').text(), ',');
      setCookie('placeindex', '', 1);
    }

    /*$('#rcmdplace_alert_message').show();
    setTimeout("$('#rcmdplace_alert_message').hide()", 1800);*/
  }
  
  setCookie('place_img', place_img, 30);
  setCookie('place_name', place_name, 30);
  setCookie('place_date', place_date, 30);
  setCookie('place_time', place_time, 30);
  setCookie('update', 1, 1);
  
});
/*$('#rcmdplace_confirm').click(function(){
  if($('#location_list>option:selected').val()!="null")
  {
    $('#place_img').val($('#place_img').val()+$('#location_img').attr('src')+',');
    $('#place_name').val($('#place_name').val()+$('#location_list>option:selected').val()+'--');
    $('#place_date').val($('#place_date').val()+$('#rcmdplace_date_choose').contents().find('#selected_date').text()+',');
    $('#place_time').val($('#place_time').val()+$('#rcmdplace_date_choose').contents().find('#selected_time').text()+',');
    $('#rcmdplace_alert_message').show();
    setTimeout("$('#rcmdplace_alert_message').hide()", 1800);
  }
});*/

// $('.button_zero').click(function(){
//    $('tr').toggle(1000);
// });
//  $('.button_one').click(function(){
//    $('tr').hide();
//    $('tr:nth-child(n+5):nth-child(-n+23)').toggle(1000);
//  });
//  $('.button_two').click(function(){
//    $('tr').hide();
//    $('tr:nth-child(n+24):nth-child(-n+37)').toggle(1000);
//  });
//   $('.button_three').click(function(){
//    $('tr').hide();
//    $('tr:nth-child(n+48):nth-child(-n+93)').toggle(1000);
//  });

/*$('.multi-item-carousel').carousel({
  interval: false
});
*/

$('.hideall').click(function(){
  $('.hideallsection').removeClass('defaultshow');
});

$('.showall').click(function(){
  $('.showallsection').addClass('defaultshow');
});

$('.showdate').click(function(){
  $('.datehide').toggleClass('defaultshow');
});

    $('#left_menu a').on('click', function () {

        var target = $(this).attr('href');
        $(target).addClass('defaultshow');
       
        var top = $(target).offset().top;
        top = top>100?(top-80):0;
        $('html, body').animate({
            scrollTop: top
        }, {
            duration: 500,
            complete: function () {
            }
        });

        // return false;
    });

$('#buttonMeal, #diningsection').click(function(e){
  // $('#collapseZomato').toggle(1000);
  // $('#collapseMeal').toggle(1000);
  // $('#meal_confirm').toggle(1000);
  // $('#zomato_confirm').toggle(1000);
  // $('.elementor-element-06114c3').toggle(1000);
  e.stopPropagation();
  $('.defaultsection').toggleClass('defaultshow');
});
$("#btnHotel, #hotelsection").on('click',function (e) {
    e.preventDefault();
  e.stopPropagation();
    $('#sectionzero').toggleClass('defaultshow');
})
$('#buttonCar, #transportsection').click(function(e){
  // $('#collapseCar').toggle(1000);
  e.stopPropagation();
  $('.defaultsection2').toggleClass('defaultshow');
});
$('#buttonPop, #guidesection').click(function(e){
  // $('#collapsepopup').toggle(1000);
  e.stopPropagation();
  $('.defaultsection3').toggleClass('defaultshow');
});
$('#buttonPlace, #recommendsection').click(function(e){
  console.log('place');
  e.stopPropagation();
  // $('#collapseplace').toggle(1000);
  $('.defaultsection4').toggleClass('defaultshow');
});
$('#buttonSpa, #spassection').click(function(e){
  // $('#collapsespa').toggle(1000);
  e.stopPropagation();
  $('.defaultsection5').toggleClass('defaultshow');
});
$('#buttonBoats, #boatsection').click(function(e){
  // $('#collapseBoats').toggle(1000);
  e.stopPropagation();
  $('.defaultsection6').toggleClass('defaultshow');
});
$('#buttonLast, #lastsection').click(function(e){
  // $('#collapselast').toggle(1000);
  e.stopPropagation();
  $('.defaultsection7').toggleClass('defaultshow');
});
$('.delivery-step a.bg-org-0').click(function (e) {
  var href = $(this).attr('href');
  if(!href || href=='#') {
    $(this).closest('.defaultshow').removeClass("defaultshow");
  }
})




$('.button_zero').click(function(){
  console.log('zero');
  $('tr').toggle(1000);
});
 $('.button_one').click(function(){
  console.log('one');
  $('tr').hide();
  $('tr:nth-child(n+5):nth-child(-n+23)').toggle(1000);
 });
 $('.button_two').click(function(){
  console.log('two');
  $('tr').hide();
  $('tr:nth-child(n+24):nth-child(-n+37)').toggle(1000);
 });
  $('.button_three').click(function(){
    console.log('threee');
    $('tr').hide();
  $('tr:nth-child(n+48):nth-child(-n+93)').toggle(1000);
 });



/*
$('.multi-item-carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  } else {
    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});*/

});
</script>

<!--
        <div id="choose-hotel-first" class="remodal" data-remodal-id="choose-hotel-first">
            <button data-remodal-action="close" class="remodal-close"></button>
            <h1>Please select a room first</h1>
            <br>
            <button data-remodal-action="cancel" class="remodal-cancel">cancel</button>
            <button data-remodal-action="confirm" class="remodal-confirm">ok</button>
        </div>
  
        <div id="make-the-booking">
            <p id="text1">??</p>
            <p id="text1a">????</p>
            <p id="text2">??1???</p>
        </div>
  -->
    
     <?php /*  <div class="row">
            <div class="col-md-6">
                <div class="row">
                     <div class="col-md-12">
                        <div id="tursldr"><?php
              $aAImg = explode('=', get_post_meta($post->ID, '_cs_sdr_img', true));
              $aATle = explode('=', get_post_meta($post->ID, '_cs_sdr_ttl', true));
              if( $aAImg ) {
                              echo '<ul>';
                foreach( $aAImg as $sK => $sV ) {
                  if($sV) {
                    $img = matthewruddy_image_resize( str_replace("https://www.soleattack.com/bali632/","https://www.travpart.com/Chinese/", clsImg($sV)), 569, 356, true, false );
                
                    echo '<li><img src="'.$img['url'].'" alt=""></li>';
                  }
                }
                echo '</ul>';
              } ?>
                        </div>
                    </div>
          
                    <div class="col-md-12">
                        <div class="bnrs row">
                            <div class="col-md-6 p15 p-r-7">
                                <img src="<?php bloginfo('template_url'); ?>/images/bnr-1-1.jpg" alt="">
                            </div>
                            <div class="col-md-6 p15 p-l-7 p-l-7-mobile">
                                <img src="<?php bloginfo('template_url'); ?>/images/bnr-2-2.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 tour_div p-l-7-mobile"> 
          <input type="hidden" class="cs_personsperroom" value="<?php if(isset($meta["_cs_personsperroom"][0])) echo $meta["_cs_personsperroom"][0]; ?>" />
            <input type="hidden" class="cs_adultprice" value="<?php if(isset($meta["_cs_adultprice"][0])) echo ceil($meta["_cs_adultprice"][0]*$person_price_ration); ?>" />
          <input type="hidden" class="cs_childrenprice" value="<?php if(isset($meta["_cs_childrenprice"][0])) echo ceil($meta["_cs_childrenprice"][0]*$person_price_ration); ?>" />
            <input type="hidden" id="cs_hoteldesti" value="<?php if(isset($meta["_cs_Destinationcontent"][0])) echo $meta["_cs_Destinationcontent"][0]; ?>" />
                  <input type="hidden" class="cs_hotelroomprice" value="<?php if(isset($meta["_cs_hotelroomprice"][0])) echo $meta["_cs_hotelroomprice"][0]; ?>" />
            <input type="hidden" class="cs_extrabedprice" value="<?php if(isset($meta["_cs_extrabedprice"][0])) echo $meta["_cs_extrabedprice"][0]; ?>" />
            <input type="hidden" class="cs_addnightroomprice" value="<?php if(isset($meta["_cs_addnightroomprice"][0])) echo $meta["_cs_addnightroomprice"][0]; ?>" />
            <input type="hidden" class="post_id" value="<?php echo get_the_ID() ?>" />
            <input type="hidden" class="site_url" value="<?php echo get_site_url() ?>" />
            <input type="hidden" class="_cs_currency_RMD" value="<?php echo get_option("_cs_currency_RMD") ?>" />

 
          <div class="tour_header">
               <div class="col-md-6 col-xs-6 information" >
              
               <?php if( trim($meta["_cs_Depart_content"][0])!=""){ ?>
                <div> <span>???  </span> : <span><?php if(isset($meta["_cs_Depart_content"][0])) echo $meta["_cs_Depart_content"][0]; ?>  </span> </div>
              <?php }?>
              
              <?php if( trim($meta["_cs_tourdays"][0])!=""){ ?>
                <div> <span>?      ?</span> :<span> <?php if(isset($meta["_cs_tourdays"][0])) echo $meta["_cs_tourdays"][0]; ?>?   </span> </div>
              <?php }?> 
              
              <?php if( trim($meta["_cs_Travel_type_content"][0])!=""){ ?>
                <div> <span> <?php if(isset($meta["_cs_Travel_type_heading"][0])) echo $meta["_cs_Travel_type_heading"][0]; ?></span> :<span> <?php if(isset($meta["_cs_Travel_type_content"][0])) echo $meta["_cs_Travel_type_content"][0]; ?>    </span> </div>
              <?php }?> 
              </div>
              
              
               <div class="col-md-6 col-xs-6  information" >
               
                <?php if( trim($meta["_cs_Destinationcontent"][0])!=""){ ?>
                  <div> <span>???  </span> :
                  <?php
                  $destin_arr=explode('|',get_post_meta($post->ID,'_cs_Destinationcontent',true));
                  $destin_arr_num=count($destin_arr);
                  for($i=0;$i<$destin_arr_num;$i++)
                  {
                  ?>
                  <span style="display:block;"><?php echo $destin_arr[$i]; ?></span>
                                     <?php } ?>
                  </div>
                <?php } ?>
                
                <?php if( trim($meta["_cs_Language_content"][0])!=""){ ?>
                <div> <span><?php if(isset($meta["_cs_Language_heading"][0])) echo $meta["_cs_Language_heading"][0]; ?> </span> :<span> <?php if(isset($meta["_cs_Language_content"][0])) echo $meta["_cs_Language_content"][0]; ?>   </span> </div>
                  <?php } ?>
                
               </div>   
                
              
          </div>
          <div class="tour_body" >

            <div class='tour_title'> 
                ?????????
            </div>
            <div class='input_div clearfix'> 
              <div class="col-md-6">
                <span>????: </span> <input type="text" id="datepicker" class="calender" /> 
              </div>
              
              <div class="col-md-6">
                <span>????: </span> <input type="text" id="end_date" class="calender" />
              </div>
              
              <input  type="hidden" class="cs_tourdays" value="<?php if(isset($meta["_cs_tourdays"][0])) echo $meta["_cs_tourdays"][0]; ?>" />
            </div>
            <div class='input_div clearfix'>    
               <div class="col-md-6"> 
                <span class="number_of_room_span">
                <?php if(isset($meta["_cs_tourdays"][0])&& $meta["_cs_tourdays"][0]>1){
                      echo " ? ?  ?:";
                    }else if(isset($meta["_cs_tourdays"][0]))
                    {
                    echo "??: ";
                    }
                    ?>
                 </span>
                <input type="text" class="number_of_room" value="????"/>
              
                <select class="number_of_room hidden"> 
                  <option value="1"> 1 </option>
                  <option value="2"> 2 </option>
                  <option value="3"> 3 </option>
                  <option value="4"> 4 </option>
                        <option value="5"> 5 </option>
                  <option value="6"> 6 </option>
                  <option value="7"> 7 </option>
                  <option value="8"> 8 </option>
                  <option value="9"> 9 </option>
                  <option value="10"> 10 </option>
                </select>
              </div>
            </div>
        <div class="hide_section hidden"> 
        
            <div class="sp_tour"></div>
      <div class="added_room">
        <div class='input_div booking_box_room_1'>
                <span>? ? 1: </span>
                
            <?php
            $cs_personsperroom=0;
             if(isset($meta["_cs_personsperroom"][0])) $cs_personsperroom=$meta["_cs_personsperroom"][0]; 
              
              
            ?>
            <select class="room_ppl_number Adult" name="Adult"> 
            <option value="0">??</option>
            <?php for($room=1;$room<=$cs_personsperroom;$room++){?>
                  <option value="<?php echo $room;?>"><?php echo $room;?> ?? </option><!-- Adult-->
            <?php } ?>
            </select> 
            <select class="room_ppl_number child"  name="child" > 
                <option value="0">??</option>
                <?php for($room=1;$room<=$cs_personsperroom;$room++){?>
                    <option value="<?php echo $room;?>"><?php echo $room;?> ?? </option><!-- child-->
                <?php } ?>
            </select> 
            
            <div id="hidden_adult" class="hidden"> 
            <option value="0">??</option>
            <?php for($room=1;$room<=$cs_personsperroom;$room++){?>
                  <option value="<?php echo $room;?>"><?php echo $room;?> ?? </option><!-- Adult-->
            <?php } ?>
            </div>
            
            <div id="hidden_child" class="hidden"> 
            <option value="0">??</option>
                <?php for($room=1;$room<=$cs_personsperroom;$room++){?>
                    <option value="<?php echo $room;?>"><?php echo $room;?> ?? </option><!-- child-->
                <?php } ?>
            </div>
            
        </div>  
      </div>
        
        <div class='input_div '>
                    <button class="confirm"> ?? &nbsp;&nbsp;</button>
        </div>
        
        <div class=' notice hidden'>  
          <span class="adult_span"> ??   <span class="change_price" or_pic=" <?php if(isset($meta["_cs_adultprice"][0])) echo $meta["_cs_adultprice"][0]; ?>"> Rp <span class="price_span"> <?php if(isset($meta["_cs_adultprice"][0])) echo $meta["_cs_adultprice"][0]; ?> </span>  </span> / ?, </span>
          <span class="child_span"> ??   <span class="change_price" or_pic="<?php if(isset($meta["_cs_childrenprice"][0])) echo $meta["_cs_childrenprice"][0]; ?>"> Rp <span class="price_span"> <?php if(isset($meta["_cs_childrenprice"][0])) echo $meta["_cs_childrenprice"][0]; ?> </span> </span>/ ?  </span>
        </div>
      </div>
    <div class="hide_section_one_day hidden"> 
        
            <div class="sp_tour"></div>
      <div class="">
        <div class='one_day_div '> 
                <span>?? </span> <input class="one_day adult_number valid_num " type="text" value="" />
                
                <span>?? </span> <input class="one_day child_number valid_num"  type="text" value="" />
          
        </div>  
      </div>
        
        <div class='input_div '> 
          <button class="confirm" > ?? &nbsp;&nbsp;</button>
        </div>
        
        <div class=' notice hidden'>  
          <span class="adult_span"> ??  <span class="change_price" or_pic=" <?php if(isset($meta["_cs_adultprice"][0])) echo $meta["_cs_adultprice"][0]; ?>"> Rp <span class="price_span"> <?php if(isset($meta["_cs_adultprice"][0])) echo $meta["_cs_adultprice"][0]; ?> </span>  </span> / ?,  </span>
          <span class="child_span"> ??  <span class="change_price" or_pic="<?php if(isset($meta["_cs_childrenprice"][0])) echo $meta["_cs_childrenprice"][0]; ?>"> Rp <span class="price_span"> <?php if(isset($meta["_cs_childrenprice"][0])) echo $meta["_cs_childrenprice"][0]; ?> </span> </span>/ ? <span>
        </div>
      </div>      
          </div>
          
          <div class="info">
          
          ????????????????,?????????? ??????????,???????????????
          

          </div>
      
              <div class="result"> 
          
          <div class="col-md-6 col-xs-6"> 
            <span class="price"> ?    ?: </span>  
            <span class="price_value"> </span> 
          </div>  
          <div class="col-md-6 col-xs-6 button_div"> 
            <button class="buy" > ???? &nbsp;&nbsp;</button>
          </div>
          <div class="price_detail hidden" style="text-align:center"> ??=(??/????? x ??/?????+????)x(1+10%???) </div> 
          </div>
            </div> 
        </div> */?>
  <?php 


function nl2br_save_html($string)
{
    if(! preg_match("#</.*>#", $string)) // avoid looping if no tags in the string.
        return nl2br($string);

    $string = str_replace(array("\r\n", "\r", "\n"), "\n", $string);

    $lines=explode("\n", $string);
    $output='';
    foreach($lines as $line)
    {
        $line = rtrim($line);
        if(! preg_match("#</?[^/<>]*>$#", $line)) // See if the line finished with has an html opening or closing tag
            $line .= '<br />';
        $output .= $line . "\n";
    }
  return $output;
}
  ?>      
        <div class="row tour_info">
            <div class="col-md-12" style="padding:0">
                <div style="overflow:hidden;">
        
          <?php if( trim(get_post_meta($post->ID,'_cs_overview_content',true))!="") { ?>
          <p name="overview" id="overview">&nbsp;</p>
                    <h2><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_overview_heading',true)); ?></h2>
                    <?php echo do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_overview_content',true))); 
          }
          ?>
          
         <?php
        /*
            $my_postid = 4640;//This is page id or post id
  			$content_post = get_post($my_postid);
            $content = $content_post->post_content;
            $title = $content_post->post_title;
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            echo "<hr> <h1> ".$title."</h1> <br> ";
            echo $content;
         */
         ?>                

                    <?php /* <div class="”table-responsive”">
                      <span class="tablepress-table-description tablepress-table-description-id-66"></span>

                      <table id="tablepress-66" class="tablepress tablepress-id-66">
                        <caption style="caption-side:bottom;text-align:left;border:none;background:none;margin:0;padding:0;">
                          <a href="https://www.travpart.com/Chinese/wp-admin/admin.php?page=tablepress&amp;action=edit&amp;table_id=66">Edit</a></caption>
                        <tbody class="row-hover">
                          <tr class="row-1 odd">
                            <td class="column-1"></td>
                            <td class="column-2"></td>
                            <td class="column-3"></td>
                            <td class="column-4"><strong>???</strong></td>
                            <td class="column-5">*<!--[wpfcNOT]-->*</td>
                            <td class="column-6"><strong>???</strong></td>
                            <td class="column-7"><strong>??</strong></td>
                            <td class="column-8"><strong>??(Rp)</strong></td>
                            <td class="column-9"><strong>??(¥)</strong></td>
                          </tr>
                          <tr class="row-2 even">
                            <td class="column-1">??(?????????)<br>
                            </td><td class="column-2"></td>
                            <td class="column-3"></td>
                            <td class="column-4">Rp120,000</td>
                            <td class="column-5"></td>
                            <td class="column-6">¥56</td>
                            <td class="column-7">
                              <input type="number" value="1" style="height:30px;width:80px;" data-rp="120000" data-rb="56" min="0"></td>
                            <td class="column-8">Rp0</td>
                            <td class="column-9">¥0</td>
                          </tr>
                          <tr class="row-3 odd">
                            <td class="column-1">??? (??)<br>
                            </td><td class="column-2"></td>
                            <td class="column-3"></td>
                            <td class="column-4">Rp400,000</td>
                            <td class="column-5"></td>
                            <td class="column-6">¥188</td>
                            <td class="column-7"><input type="number" value="1" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8">Rp0</td>
                            <td class="column-9">¥0</td>
                          </tr>
                          <tr class="row-4 even">
                            <td class="column-1">????:</td>
                            <td colspan="8" class="column-2"></td>
                          </tr>
                          <tr class="row-5 odd">
                            <td colspan="6" class="column-1"></td>
                            <td class="column-7">??:</td>
                            <td class="column-8"><span id="spanTotalRP">Rp0</span></td>
                            <td class="column-9"><span id="spanTotalRB">¥0</span></td>
                          </tr>
                          <tr class="row-6 even">
                            <td colspan="6" class="column-1"></td>
                            <td class="column-7">?????(10%)</td>
                            <td class="column-8"><span id="spanTaxAndServiceRP">Rp0</span></td>
                            <td class="column-9"><span id="spanTaxAndServiceRB">¥0</span></td>
                          </tr>
                          <tr class="row-7 odd">
                            <td colspan="6" class="column-1"></td>
                            <td class="column-7">??????</td>
                            <td class="column-8"><span id="spanTotalAndFeeRP">Rp0</span></td>
                            <td class="column-9"><span id="spanTotalAndFeeRb">¥0</span></td>
                          </tr>
                        </tbody>
                      </table>

                  </div> */ ?>
                  <?php
    echo '<input type="hidden" name="activepost" id="activepost" value="'.get_the_ID().'" />';
      echo '<input type="hidden" name="site_url" id="site_url" value="'.get_site_url().'" />';
      
      /*
                  <div class="”table-responsive”">
                      <span class="tablepress-table-description tablepress-table-description-id-109"></span>
                    <!-- <form name="pricelistform" method="get" action="https://www.travpart.com/English/wp-content/plugins/gwebpro-store-locator/spotregistration.php" onSubmit="window.location.reload()">   -->                       
                     <form action="https://www.tourfrombali.cn/test-box/tour-details/" method="post">
                       
                    <table id="tablepress-109" class="tablepress tablepress-id-109">
                        <caption style="caption-side:bottom;text-align:left;border:none;background:none;margin:0;padding:0;">
                          <a href="https://www.travpart.com/Chinese/wp-admin/admin.php?page=tablepress&amp;action=edit&amp;table_id=109">Edit</a></caption>
                        <tbody class="row-hover">
                          <tr class="row-1 odd">
                            <td class="column-1"></td>
                            <td class="column-2"></td>
                            <td class="column-3"></td>
                            <td class="column-4"><strong>???</strong></td>
                            <td class="column-5">*<!--[wpfcNOT]-->*</td>
                            <td class="column-6"><strong>???</strong></td>
                            <td class="column-7"><strong>??</strong></td>
                            <td class="column-8"><strong>??(Rp)</strong></td>
                            <td class="column-9"><strong>??(¥)</strong></td>
                          </tr>
                          <tr class="row-2 even">
                            <td class="column-1">Lunch per serve<br>                     
                            
                            <input type="hidden" name="lunch_per_serve" value="Lunch per serve">
                              </td>
                            <td class="column-2"></td>
                            <td class="column-3"></td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_lunch_per_serve_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_lunch_per_serve_dollar',true)); ?></td>
                            <td class="column-7">
                              <input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_lunch_per_serve_count',true)); ?>" style="height:30px;width:80px;" data-rp="4000" data-rb="100" min="0"></td>
                            
                            <td class="column-8">Rp0</td>
                             <input type="hidden" name="lunch_per_serve_total_rup" id="lunch_per_serve_total_rup" value="<?php echo htmlspecialchars($_SESSION['lunch_per_serve_total_rup']);?>" >
                          
                           
                            <td class="column-9">¥0</td>
                            <input type="hidden" name="lunch_per_serve_total_dollar" id="lunch_per_serve_total_dollar" value="1">
                            
                          </tr>
                          
                           <tr class="row-3 odd">
                            <td class="column-1">Tour Guide per day<br>
                              <input type="hidden" name="tourguide_per_day" value="Tour Guide per day">
                            </td>
                             
                             <td class="column-2"></td>
                            <td class="column-3"></td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_tourguide_per_day_rup',true)); ?><input type="hidden" name="tourguide_per_day_rup" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_tourguide_per_day_rup',true)); ?>"></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_tourguide_per_day_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_tourguide_per_day_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            
                             <td class="column-8">Rp0</td>
                             <input type="hidden" name="tourguide_per_day_total_rup" id="tourguide_per_day_total_rup" value="1">
                            <td class="column-9">¥0</td>
                             <input type="hidden" name="tourguide_per_day_total_dollar" id="tourguide_per_day_total_dollar" value="1">
                             
                          </tr>
                          
                          <tr class="row-4 even">
                           <td class="column-1">(Exclude guide's accomodation and tips)<br>
                            </td><td class="column-2"></td>
                          </tr>
                          <tr class="row-5 odd">
                            <td class="column-1">Insurance (per person)<br>
                            
                            <input type="hidden" name="insurance_per_person" value="Insurance (per person)"></td>
                            <td class="column-2"></td>
                            <td class="column-3"></td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_insurance_per_person_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_insurance_per_person_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_insurance_per_person_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8">Rp0</td>
                            <input type="hidden" name="insurance_per_person_rup_total" id="insurance_per_person_rup_total" value="1">
                            <td class="column-9">¥0</td>
                            <input type="hidden" name="insurance_per_person_dollar_total" id="insurance_per_person_dollar_total" value="1">
                          </tr>
                          <tr class="row-6 even">
                             <td class="column-1">Car/Bus (Per-Day)<br>
                            </td><td class="column-2"></td>
                            <td class="column-3"></td>
                            <td class="column-4">Daily Price</td>
                            <td class="column-5"></td>
                            <td class="column-6">Daily Price</td>
                            <td class="column-7"></td>
                            <td class="column-8"></td>
                            <td class="column-9"></td>
                          </tr>
                          <tr class="row-7 odd">
                            <td class="column-1"></td>
                           <td class="column-2">6 Seats
                            <input type="hidden" name="6seats" value="6 Seats"></td>
                            <td class="column-3">Half day or Airport Transfer
                            <input type="hidden" name="halfday" value="Half day or Airport Transfer"></td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_halfday_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_halfday_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_halfday_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="6seats_halfday_rup_total" id="6seats_halfday_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="6seats_halfday_dollar_total" id="6seats_halfday_dollar_total" value="1">
                          </tr>
                           <tr class="row-8 even">
                            <td class="column-1"></td>
                           <td class="column-2">12 Seats
                             <input type="hidden" name="12seats" value="12 Seats"></td>
                            <td class="column-3">Half day or Airport Transfer</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_12seats_halfday_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_12seats_halfday_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_12seats_halfday_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                             <input type="hidden" name="12seats_halfday_rup_total" id="12seats_halfday_rup_total" value="1">
                            <td class="column-9"></td>
                             <input type="hidden" name="12seats_halfday_dollar_total" id="12seats_halfday_dollar_total" value="1">
                          </tr>
                          <tr class="row-9 odd">
                            <td class="column-1"></td>
                           <td class="column-2">6 Seats</td>
                            <td class="column-3">Bali Island
                             <input type="hidden" name="baliisland" value="Bali Island"></td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_baliisland_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_baliisland_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_baliisland_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="6seats_baliisland_rup_total" id="6seats_baliisland_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="6seats_baliisland_dollar_total" id="6seats_baliisland_dollar_total" value="1">
                          </tr>
                           <tr class="row-10 even">
                            <td class="column-1"></td>
                           <td class="column-2">12 Seats</td>
                            <td class="column-3">Bali Island</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_12seats_baliisland_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_12seats_baliisland_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_12seats_baliisland_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                             <input type="hidden" name="12seats_baliisland_rup_total" id="12seats_baliisland_rup_total" value="1">
                            <td class="column-9"></td>
                             <input type="hidden" name="12seats_baliisland_dollar_total" id="12seats_baliisland_dollar_total" value="1">
                          </tr>
                          <tr class="row-11 odd">
                            <td class="column-1"></td>
                           <td class="column-2">16 Seats
                            <input type="hidden" name="16seats" value="16 Seats"></td>
                            <td class="column-3">Bali Island</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_16seats_baliisland_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_16seats_baliisland_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_16seats_baliisland_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="16seats_baliisland_rup_total" id="16seats_baliisland_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="16seats_baliisland_dollar_total" id="16seats_baliisland_dollar_total" value="1">
                          </tr>
                           <tr class="row-12 even">
                            <td class="column-1"></td>
                           <td class="column-2">29 Seats
                             <input type="hidden" name="29seats" value="29 Seats"></td>
                            <td class="column-3">Bali Island</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_29seats_baliisland_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_29seats_baliisland_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_29seats_baliisland_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                             <input type="hidden" name="29seats_baliisland_rup_total" id="29seats_baliisland_rup_total" value="1">
                            <td class="column-9"></td>
                             <input type="hidden" name="29seats_baliisland_dollar_total" id="29seats_baliisland_dollar_total" value="1">
                          </tr>
                          <tr class="row-13 odd">
                            <td class="column-1"></td>
                           <td class="column-2">35 Seats
                            <input type="hidden" name="35seats" value="35 Seats"></td>
                            <td class="column-3">Bali Island</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_35seats_baliisland_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_35seats_baliisland_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_35seats_baliisland_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="35seats_baliisland_rup_total" id="35seats_baliisland_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="35seats_baliisland_dollar_total" id="35seats_baliisland_dollar_total" value="1">
                          </tr>
                          <tr class="row-14 even">
                            <td class="column-1"></td>
                           <td class="column-2">45 Seats
                            <input type="hidden" name="45seats" value="45 Seats"></td>
                            <td class="column-3">Bali Island</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_45seats_baliisland_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_45seats_baliisland_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_45seats_baliisland_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="45seats_baliisland_rup_total" id="45seats_baliisland_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="45seats_baliisland_dollar_total" id="45seats_baliisland_dollar_total" value="1">
                          </tr>
                          <tr class="row-15 odd">
                            <td class="column-1"></td>
                           <td class="column-2">6 Seats</td>
                            <td class="column-3">Bali to Other Island Nearby
                            <input type="hidden" name="balitootherisland" value="Bali to Other Island Nearby"></td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_balitoother_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_balitoother_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_balitoother_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="6seats_balitoother_rup_total" id="6seats_balitoother_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="6seats_balitoother_dollar_total" id="6seats_balitoother_dollar_total" value="1">
                          </tr>
                          <tr class="row-16 even">
                            <td class="column-1"></td>
                           <td class="column-2">16 Seats
                            <input type="hidden" name="16seats" value="16 Seats"></td>
                            <td class="column-3">Bali to Other Island Nearby</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_16seats_balitoother_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_16seats_balitoother_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_16seats_balitoother_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="16seats_balitoother_rup_total" id="16seats_balitoother_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="16seats_balitoother_dollar_total" id="16seats_balitoother_dollar_total" value="1">
                          </tr>
                          <tr class="row-17 odd">
                            <td class="column-1"></td>
                           <td class="column-2">28 Seats
                             <input type="hidden" name="28seats" value="28 Seats"></td>
                            <td class="column-3">Bali to Other Island Nearby</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_28seats_balitoother_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_28seats_balitoother_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_28seats_balitoother_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="28seats_balitoother_rup_total" id="28seats_balitoother_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="28seats_balitoother_dollar_total" id="28seats_balitoother_dollar_total" value="1">
                          </tr>
                          <tr class="row-18 even">
                            <td class="column-1"></td>
                           <td class="column-2">35 Seats
                            <input type="hidden" name="35seats" value="35 Seats"></td>
                            <td class="column-3">Bali to Other Island Nearby</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_35seats_balitoother_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_35seats_balitoother_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_35seats_balitoother_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="35seats_balitoother_rup_total" id="35seats_balitoother_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="35seats_balitoother_dollar_total" id="35seats_balitoother_dollar_total" value="1">
                          </tr>
                          <tr class="row-19 odd">
                            <td class="column-1"></td>
                           <td class="column-2">45 Seats
                            <input type="hidden" name="45seats" value="45 Seats"></td>
                            <td class="column-3">Bali to Other Island Nearby</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_45seats_balitoother_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_45seats_balitoother_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_45seats_balitoother_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="45seats_balitoother_rup_total" id="45seats_balitoother_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="45seats_balitoother_dollar_total" id="45seats_balitoother_dollar_total" value="1">
                          </tr>
                           <tr class="row-20 even">
                            <td class="column-1"></td>
                           <td class="column-2">6 Seats</td>
                            <td class="column-3">Java Island
                             <input type="hidden" name="javaisland" value="Java Island"></td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_javaisland_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_javaisland_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_6seats_javaisland_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                             <input type="hidden" name="6seats_javaisland_rup_total" id="6seats_javaisland_rup_total" value="1">
                            <td class="column-9"></td>
                             <input type="hidden" name="6seats_javaisland_dollar_total" id="6seats_javaisland_dollar_total" value="1">
                          </tr>
                          <tr class="row-21 odd">
                            <td class="column-1"></td>
                           <td class="column-2">15 Seats
                            <input type="hidden" name="15seats" value="15 Seats"></td>
                            <td class="column-3">Java Island</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_15seats_javaisland_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_15seats_javaisland_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_15seats_javaisland_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="15seats_javaisland_rup_total" id="15seats_javaisland_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="15seats_javaisland_dollar_total" id="15seats_javaisland_dollar_total" value="1">
                            
                          </tr>
                          <tr class="row-22 even">
                            <td class="column-1"></td>
                           <td class="column-2">19 Seats
                            <input type="hidden" name="19seats" value="19 Seats"></td>
                            <td class="column-3">Java Island</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_19seats_javaisland_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_19seats_javaisland_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_19seats_javaisland_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="19seats_javaisland_rup_total" id="19seats_javaisland_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="19seats_javaisland_dollar_total" id="19seats_javaisland_dollar_total" value="1">
                          </tr>
                           <tr class="row-23 odd">
                            <td class="column-1"></td>
                           <td class="column-2">31 Seats
                              <input type="hidden" name="31seats" value="31 Seats"></td>
                            <td class="column-3">Java Island</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_31seats_javaisland_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_31seats_javaisland_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_31seats_javaisland_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                             <input type="hidden" name="31seats_javaisland_rup_total" id="31seats_javaisland_rup_total" value="1">
                            <td class="column-9"></td>
                             <input type="hidden" name="31seats_javaisland_dollar_total" id="31seats_javaisland_dollar_total" value="1">
                          </tr>
                          <tr class="row-24 even">
                            <td class="column-1"></td>
                           <td class="column-2">59 Seats
                            <input type="hidden" name="59seats" value="59 Seats"></td>
                            <td class="column-3">Java Island</td>
                            <td class="column-4"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_59seats_javaisland_rup',true)); ?></td>
                            <td class="column-5"></td>
                            <td class="column-6"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_59seats_javaisland_dollar',true)); ?></td>
                            <td class="column-7"><input type="number" value="<?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_59seats_javaisland_count',true)); ?>" style="height:30px;width:80px;" data-rp="400000" data-rb="188" min="0"></td>
                            <td class="column-8"></td>
                            <input type="hidden" name="59seats_javaisland_rup_total" id="59seats_javaisland_rup_total" value="1">
                            <td class="column-9"></td>
                            <input type="hidden" name="59seats_javaisland_dollar_total" id="59seats_javaisland_dollar_total" value="1">
                          </tr>
                          
                          <tr class="row-25 odd">
                            <td class="column-1">????:</td>
                            <td colspan="8" class="column-2"></td>
                          </tr>
                          <tr class="row-26 even">
                            <td colspan="6" class="column-1"></td>
                            <td class="column-7">??:</td>
                            <td class="column-8"><span id="spanTotalRP2">Rp0</span></td>
                            <td class="column-9"><span id="spanTotalRB2">¥0</span></td>
                          </tr>
                          <tr class="row-27 odd">
                            <td colspan="6" class="column-1"></td>
                            <td class="column-7">?????(10%)</td>
                            <td class="column-8"><span id="spanTaxAndServiceRP2">Rp0</span></td>
                            <td class="column-9"><span id="spanTaxAndServiceRB2">¥0</span></td>
                          </tr>
                          <tr class="row-28 even">
                            <td colspan="6" class="column-1"></td>
                            <td class="column-7">??????</td>
                            <td class="column-8"><span id="spanTotalAndFeeRP2">Rp0</span></td>
                            <td class="column-9"><span id="spanTotalAndFeeRb2">¥0</span></td>
                          </tr>
                        </tbody>
                      </table>
                       <input type="hidden" name="spanTotalRP2" id="inpspanTotalRP2" vlaue="">
                       <input type="hidden" name="spanTotalRB2" id="inpspanTotalRB2" vlaue="">
                       
                       <input type="hidden" name="spanTaxAndServiceRP2" id="inpspanTaxAndServiceRP2" vlaue="">
                       <input type="hidden" name="spanTaxAndServiceRB2" id="inpspanTaxAndServiceRB2" vlaue="">
                       
                       <input type="hidden" name="spanTotalAndFeeRP2" id="inpspanTotalAndFeeRP2" vlaue="">
                       <input type="hidden" name="spanTotalAndFeeRb2" id="inpspanTotalAndFeeRb2" vlaue="">
                                                                              
                                           
                      <span id="savednote"></span>
*/ ?>
<script>
function setHotelCookie(c_name,value,expiredays)
{
  var exdate=new Date();
  exdate.setDate(exdate.getDate()+expiredays);
  document.cookie=c_name+ "=" +escape(value)+ ((expiredays==null) ? "" : "; expires="+exdate.toGMTString())+ ";domain=www.travpart.com;path=/English/";
  document.cookie=c_name+ "=" +escape(value)+ ((expiredays==null) ? "" : "; expires="+exdate.toGMTString())+ ";domain=www.travpart.com;path=/hotel-and-flight-bookings/";
}
function setCookie(c_name,value,expiredays)
{
  var exdate=new Date();
  exdate.setDate(exdate.getDate()+expiredays);
  document.cookie=c_name+ "=" +escape(value)+ ((expiredays==null) ? "" : "; expires="+exdate.toGMTString())+ ";domain=www.travpart.com;path=/English/";
}
function getCookie(name) {
    var strCookie = document.cookie;
    var arrCookie = strCookie.split("; ");
    for (var i = 0; i < arrCookie.length; i++) {
        var arr = arrCookie[i].split("=");
        if (arr[0] == name)
            return unescape(arr[1]);
    }
    return "";
} 
jQuery(document).ready(function(){
  
    var roomnums=1;
  var i=0;
  $('#roomnum').on('change', function() {
    roomnums=$('.adult input').length;
    if($('#roomnum').val()<1)
      $('#roomnum').val(1);
  
    if($('#roomnum').val()>=roomnums)
    {
      for(i=roomnums; i<$('#roomnum').val(); i++)
      {
        $('.adult').append('<input type="number" value="1" min="0" max="3" class="searchTerm">');
        $('.child').append('<input type="number" value="0" min="0" max="3" class="searchTerm">');
      }
    }
    else
    {
      for(i=roomnums; i>$('#roomnum').val(); i--)
      {
        $('.adult input').last().remove();
        $('.child input').last().remove();
      }
    }

  });
  
  $(".header_curreny").change(function () {

        var full_name = $(this).find("option:selected").attr("full_name");
        var syml = $(this).find("option:selected").attr("syml");

        var curreny = $(this).val();

        if (curreny == "IDR") {

            $(".change_price").each(function () {
                $(this).html(syml + "<span class='price_span'>" + $(this).attr("or_pic") + " </span>");

            });

        } else {
            $(".change_price").each(function () {
        var amount = $(this).attr("or_pic");
        if (syml == '元')
        {
                    $(this).html("<span class='price_span'>" + Math.round($('#cs_RMB').val()*amount) + " </span>" + syml);
        }
        else
        {
                    $(this).html(syml + "<span class='price_span'>" + ($('#cs_'+curreny).val()*Math.round(amount)).toFixed(2) + " </span>");
        }
            });
        }
        $(".header_curreny").val($(this).val());
        
        $(".item_price_syml").text(syml);
    });
  $(".header_curreny").change();
  
    var price=0, eainum=0, ea_total=0;
  $('.eainum input').on('change', function() {
    price=parseInt($(this).parent().prev().find('.price_span').text());
    eainum=$(this).val();
    $(this).parent().next().html('<span>'+eainum*price+$('.header_curreny').find("option:selected").attr("syml")+'</span>');
    ea_total=0;
    $('.eaitotal span').each(function()
    {
      ea_total+=parseInt($(this).text());
    });
    $('#ea_total').text(ea_total+$('.header_curreny').find("option:selected").attr("syml"));
  });
  
  var start_date=new Date(), end_date=new Date(),requesttime=0;
  end_date.setDate(start_date.getDate()+1);
  var dataString = {
            'hotels': '',
            'tickets': '',
            'number_of_adult': 1,
            'number_of_child': 0,
      'number_of_room': 1,
      'number_of_extra_bed': 0,
      'cs_addnightroom': 0,
      'cs_addnightroomprice': '',
      'post_id': <?php echo get_the_ID() ?>,
      'start_date': start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear(),
      'end_date': end_date.getDate()+'-'+(end_date.getMonth()+1)+'-'+end_date.getFullYear(),
      'total': 0,
      'hiddenTripDetails': '',
      'hiddenTaxAndServiceRP': ''
    };
  $('#submittour').click(function(e) {
    var submit=$(this);
    if(parseInt(getCookie('tour_id'))>1)
    {
    }
    else {
    e.preventDefault();
    if(requesttime==0)
    {
      requesttime=1;
    $.ajax({
            type: "POST",
            url: '/English/wp-content/themes/bali/addrequest.php',
            data: dataString,
            success: function (data) {
                setCookie('tour_id', data, 1);
                submit.unbind('click');
                submit.click();
        }
            });
    }
    }
  });

});
</script>
<form action="https://www.travpart.com/English/tour-details/" method="post">
<input id="meal_type" name="meal_type" type="hidden" value="" />
<input id="meal_date" name="meal_date" type="hidden" value="" />
<input id="meal_time" name="meal_time" type="hidden" value="" />
<input id="car_type" name="car_type" type="hidden" value="" />
<input id="selectedCarPricehiden" name="car_price" type="hidden" value="" />
<input id="car_date" name="car_date" type="hidden" value="" />
<input id="car_time" name="car_time" type="hidden" value="" />
<input id="popupguide_type" name="popupguide_type" type="hidden" value="" />
<input id="popupguide_date" name="popupguide_date" type="hidden" value="" />
<input id="popupguide_time" name="popupguide_time" type="hidden" value="" />
<input id="spa_type" name="spa_type" type="hidden" value="" />
<input id="spa_date" name="spa_date" type="hidden" value="" />
<input id="spa_time" name="spa_time" type="hidden" value="" />
<input id="boat_type" name="boat_type" type="hidden" value="" />
<input id="boat_date" name="boat_date" type="hidden" value="" />
<input id="boat_time" name="boat_time" type="hidden" value="" />
<input id="place_img" name="place_img" type="hidden" value="" />
<input id="place_name" name="place_name" type="hidden" value="" />
<input id="place_date" name="place_date" type="hidden" value="" />
<input id="place_time" name="place_time" type="hidden" value="" />
<input id="tourlist_img" name="tourlist_img" type="hidden" value="" />
<input id="tourlist_name" name="tourlist_name" type="hidden" value="" />
<input id="tourlist_price" name="tourlist_price" type="hidden" value="" />
<input id="tourlist_date" name="tourlist_date" type="hidden" value="" />
<input id="tourlist_time" name="tourlist_time" type="hidden" value="" />
<input id="hiddenfieldforzomatorest" name="hiddenfieldforzomatorest" type="hidden" value="" />

    <?php
    function extraActivity() {
        $ExtraActivity=unserialize(get_option('_cs_extraactivity'));
        $ext_list_html = '';
        if(!empty($ExtraActivity))
        {
            $i = 0;

            foreach($ExtraActivity as $t){
                if($i==0)
                    $ext_list_html.='<div class="item active">';
                else
                    $ext_list_html.=($i%6==0)?'<div class="item">':'';
                $ext_list_html .='<div class="col-sm-4"><div class="ext-block">
<div class="ext-block-top" style="background-image: url('.$t['img'].')"><div class="c-checkbox"><input item="'.$i.'" type="checkbox" value="'.$t['item'].'"><label></label></div></div> 
<div class="ext-block-bottom"><div class="ext-name">'.$t['item'].'</div>
<div class="ext-price"><span class="change_price" or_pic="'.round($t['price']*get_option('person_price_ration')).'">Rp<span class="price_span">'.round($t['price']*get_option('person_price_ration')).'</span></span></div> 
</div>
</div></div>';
                $ext_list_html.=($i%6==5)?'</div>':'';
                $i++;
            }
            $ext_list_html .= ($i%6>0)?'</div>':'';
        }
        echo  $ext_list_html;
    }
    ?>

    <!--    start ExtraActivity-->

    <div id="sectionseven" class="defaultsection7 hideallsection showallsection">
        <div style="padding: 25px;">
            <div class="travcust-pg-box position-relative">
                <div id="ext-step-1" class="delivery-step delivery-step-active">
                    <div class="carousel slide multi-item-carousel" id="extCarousel">
                        <!--ext slide-->
                        <div class="carousel-inner"><?php extraActivity(); ?></div>
                        <!--ext slide-->
                        <a class="left carousel-control" href="#extCarousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                        <a class="right carousel-control" href="#extCarousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                    </div>
                    <div class="text-center mt-4">
                        <a id="btn-ext-select-ext" href="#ext-step-2" class="bg-org-0 p-2 pl-4 pr-4  d-flex-inline flex-column justify-content-center align-items-center text-white nblink mt-5 border-radius">
                            <span class="font-small">Next</span>
                        </a>
                    </div>
                </div>
                <div id="ext-step-2" class=" delivery-step">
                    <button onclick='jQuery("#ext-step-2").removeClass("delivery-step-active");jQuery("#ext-step-1").addClass("delivery-step-active");' class="btn bg-blue-3 text-white p-1  pl-3 pr-3  border-radius position-absolute btn-back"  ><i class="fas fa-arrow-alt-circle-left mr-1"></i> <span>Back</span></button>
                    <div class="pt-5 pb-5 gutter">
                        <div class="border-radius border  p-2 ">
                            <div class="mb-3"><i class="fa fa-tint  font-xl mr-3"></i> <span class="font-small">You have chosen <span class="text-blue-3 value-ext-selected-count"></span> Optional activities</span></div>
                            <div id="selected-ext" class=" clearfix"></div>
                        </div>
                        <a id="btn-ext-select-date" href="#ext-step-3" class="bg-blue-0 p-2 d-flex flex-column justify-content-center align-items-center text-white nblink mt-5 border-radius">
                            <i class="fa fa-calendar-alt font-xl mb-1"></i>
                            <span class="font-small">Which day of the journey?</span>
                        </a>
                    </div>

                </div>

                <div id="ext-step-3"  class="delivery-step">
                    <button onclick='jQuery("#ext-step-3").removeClass("delivery-step-active");jQuery("#ext-step-2").addClass("delivery-step-active");' class="btn bg-blue-3 text-white p-1  pl-3 pr-3  border-radius position-absolute btn-back"  ><i class="fas fa-arrow-alt-circle-left mr-1"></i> <span>Back</span></button>

                    <div class="pt-5 pb-5 gutter">
                        <div class="date-picker-container">
                            <div id="extDateTimeApp" class="deliver-app-container" ng-app="extDateTimeApp" ng-controller="extDateTimeCtrl as ctrl" ng-cloak>
                                <div date-picker submitfn ="ctrl.submitDate(newdate)"
                                     datepicker-title="Choose a date"
                                     picktime="true"
                                     pickdate="true"
                                     pickpast="false"
                                     mondayfirst="false"
                                     custom-message="You chose"
                                     selecteddate="ctrl.selected_date"
                                     updatefn="ctrl.updateDate(newdate)">
                                    <p id="selected_date" ng-if="localdate.getMonth()<9 && localdate.getDate()<10" style="display:none;" >{{ localdate.getFullYear() }}-0{{ localdate.getMonth()+1 }}-0{{ localdate.getDate() }}</p>
                                    <p id="selected_date" ng-if="localdate.getMonth()<9 && localdate.getDate()>=10" style="display:none;" >{{ localdate.getFullYear() }}-0{{ localdate.getMonth()+1 }}-{{ localdate.getDate() }}</p>
                                    <p id="selected_date" ng-if="localdate.getMonth()>=9 && localdate.getDate()<10" style="display:none;" >{{ localdate.getFullYear() }}-{{ localdate.getMonth()+1 }}-0{{ localdate.getDate() }}</p>
                                    <p id="selected_date" ng-if="localdate.getMonth()>=9 && localdate.getDate()>=10" style="display:none;" >{{ localdate.getFullYear() }}-{{ localdate.getMonth()+1 }}-{{ localdate.getDate() }}</p>
                                    <p id="selected_time" style="display:none;" >{{ hourandmin }}</p>

                                    <div class="datepicker"
                                         ng-class="{
        'am': timeframe == 'am',
        'pm': timeframe == 'pm',
        'compact': compact
      }">
                                        <div class="datepicker-header" style="display: none;">
                                            <div class="datepicker-title" ng-if="datepicker_title">{{ datepickerTitle }}</div>
                                            <div class="datepicker-subheader">{{ customMessage }} {{ selectedDay }} {{ monthNames[localdate.getMonth()] }} {{ localdate.getDate() }}, {{ localdate.getFullYear() }}</div>
                                        </div>
                                        <div class="datepicker-calendar">
                                            <div class="calendar-header">
                                                <div class="goback" ng-click="moveBack()" ng-if="pickdate">
                                                    <svg width="30" height="30">
                                                        <path fill="none" stroke="#0DAD83" stroke-width="3" d="M19,6 l-9,9 l9,9"/>
                                                    </svg>
                                                </div>
                                                <div class="current-month-container">{{ currentViewDate.getFullYear() }} {{ currentMonthName() }}</div>
                                                <div class="goforward" ng-click="moveForward()" ng-if="pickdate">
                                                    <svg width="30" height="30">
                                                        <path fill="none" stroke="#0DAD83" stroke-width="3" d="M11,6 l9,9 l-9,9" />
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="calendar-day-header">
                                                <span ng-repeat="day in days" class="day-label">{{ day.short }}</span>
                                            </div>
                                            <div class="calendar-grid" ng-class="{false: 'no-hover'}[pickdate]">
                                                <div
                                                        ng-class="{'no-hover': !day.showday}"
                                                        ng-repeat="day in month"
                                                        class="datecontainer"
                                                        ng-style="{'margin-left': calcOffset(day, $index)}"
                                                        track by $index>
                                                    <div class="datenumber" ng-class="{'day-selected': day.selected }" ng-click="selectDate(day)">
                                                        {{ day.daydate }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timepicker" ng-if="picktime == 'true'">
                                            <div ng-class="{'am': timeframe == 'am', 'pm': timeframe == 'pm' }">
                                                <div class="timepicker-container-outer" selectedtime="time" timetravel>
                                                    <div class="timepicker-container-inner">
                                                        <div class="timeline-container" ng-mousedown="timeSelectStart($event)" sm-touchstart="timeSelectStart($event)">
                                                            <div class="current-time">
                                                                <div class="actual-time">{{ time }}</div>
                                                            </div>
                                                            <div class="timeline">
                                                            </div>
                                                            <div class="hours-container">
                                                                <div class="hour-mark" ng-repeat="hour in getHours() track by $index"></div>
                                                            </div>
                                                        </div>
                                                        <div class="display-time">
                                                            <div class="decrement-time" ng-click="adjustTime('decrease')">
                                                                <svg width="24" height="24">
                                                                    <path stroke="white" stroke-width="2" d="M8,12 h8"/>
                                                                </svg>
                                                            </div>
                                                            <div class="time" ng-class="{'time-active': edittime.active}">
                                                                <input type="text" class="time-input" ng-model="edittime.input" ng-keydown="changeInputTime($event)" ng-focus="edittime.active = true; edittime.digits = [];" ng-blur="edittime.active = false"/>
                                                                <div class="formatted-time">{{ edittime.formatted }}</div>
                                                            </div>
                                                            <div class="increment-time" ng-click="adjustTime('increase')">
                                                                <svg width="24" height="24">
                                                                    <path stroke="white" stroke-width="2" d="M12,7 v10 M7,12 h10"/>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div class="am-pm-container">
                                                            <div class="am-pm-button" ng-click="changetime('am');">AM</div>
                                                            <div class="am-pm-button" ng-click="changetime('pm');">PM</div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="d-flex justify-content-end buttons-container align-items-center">
                                            <div class="datepicker-response mr-3">{{ customMessage }} {{getDateTime().format("MM/D/YYYY  hh:mm tt")}}</div>
                                            <div class="save-button bg-org-0 text-white"  ng-click="submitDate()">Confirm</div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="ext-step-4" class=" delivery-step">
                    <button onclick='jQuery("#ext-step-4").removeClass("delivery-step-active");jQuery("#ext-step-3").addClass("delivery-step-active");' class="btn bg-blue-3 text-white p-1  pl-3 pr-3  border-radius position-absolute btn-back"  ><i class="fas fa-arrow-alt-circle-left mr-1"></i> <span>Back</span></button>

                    <div class="pt-5 pb-5 gutter">
                        <div class="border-radius border  p-2 mb-3 ">
                            <div class="mb-3"><i class="fa fa-tint  font-xl mr-3"></i> <span class="font-small">You have chosen <span class="text-blue-3 value-ext-selected-count"></span> Optional activities</span></div>
                            <div id="selected-ext-details" class="clearfix"></div>
                        </div>
                        <div class="border-radius border d-flex align-items-center p-2 ">
                            <i class="fa fa-calendar-alt font-xl mr-3"></i> <span class="font-small">You have chosen <span id="value-ext-date" class="text-blue-1"></span></span>
                        </div>
                        <div  class="text-center">
                            <a id="ext-add-trav" href="#" class="bg-org-0 p-2 pl-4 pr-4 d-inline-flex  justify-content-center align-items-center text-white nblink mt-5 border-radius">
                                <i class="fa fa-calendar-alt font-small mr-3"></i>
                                <span class="font-small">Add to my journey</span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!--    end ExtraActivity-->

    
    <?php // js在travcust.js ?>

<?php
/*  //Extra activity price list function
  $ExtraActivity=unserialize(get_option('_cs_extraactivity'));
  if(!empty($ExtraActivity))
  {
    echo '<table class="extraactivity hideallsection showallsection"> <div class="col-md-12 buttonmargin hideallsection showallsection"></div>';
    echo '<tr><th class="firstcontent"> </th> <th class="secondcontent">unit price</th> <th class="thirdcontent">Quantity</th><th class="forthcontent">total</th><th></th></tr>';
    $i=0;
    foreach($ExtraActivity as $t)
    { 
      echo '<tr>
      <td>'.$t['item'].'<input name="eaiitem_'.$i.'" type="hidden" value="'.$t['item'].'">';
      if(!empty($t['img']))
        echo '<img class="mockupimage" src="'.$t['img'].'" />';
      echo '<input name="eaiimg_'.$i.'" type="hidden" value="'.$t['img'].'"></td> <td>';
      if(!empty($t['price']))
        echo '<span class="change_price" or_pic="'.round($t['price']*get_option('person_price_ration')).'">Rp<span class="price_span">'.round($t['price']*get_option('person_price_ration')).'</span></span>';
      echo '<input name="eaiprice_'.$i.'" type="hidden" value="'.$t['price'].'"></td>';
      echo '<td class="eainum"><input name="eainum_'.$i.'" type="'.(empty($t['price'])?'hidden':'number').'" value="0" style="height:30px;width:80px;" min="0"></td>
          <td class="eaitotal" '.(empty($t['price'])?'style="display:none;"':'').'><span>0</span></td>';
      echo '<td class="eaidate" '.(empty($t['price'])?'style="display:none;"':'').'><p style="display:none;"></p><div class="elementor-button-wrapper spu-open-14337">
          <a href="#" class="elementor-button-link elementor-button elementor-size-md" role="button">
            <span class="elementor-button-content-wrapper"><span class="elementor-button-text">Date selection</span></span>
          </a></div>
          <input class="eaidateval" name="eaidate_'.$i.'" type="hidden" value="0" /><input class="eaitimeval" name="eaitime_'.$i.'" type="hidden" value="0" /></td>
      <td item="'.$i.'" class="activity_confirm" '.(empty($t['price'])?'style="display:none;"':'').'>
      <a href="#" class="btn btn-default">Add</a>
      <p class="add_activity_fail" style="margin-top: 16px;font-size: 20px;color: red; display:none;">Added failed</p>
      <p class="add_activity_success" style="margin-top: 16px;font-size: 20px;color: green; display:none;">Added successfully</p>
      </td>
      </tr>';
      $i++;
    }
    echo '</table>';
    echo '<div class="totalprice">total: <span id="ea_total">0</span></div>';
  }
*/?><!--
<br/>
  
<script>
var eaitrigger;
jQuery(document).ready(function(){

$('.eaidate').click(function(){
    eaitrigger=$(this).find('input').first();
});
$('#eaidate_confirm').click(function(){
    eaitrigger.val($('#eaidate_choose').contents().find('#selected_date').text());
  eaitrigger.next().val($('#eaidate_choose').contents().find('#selected_time').text());
  eaitrigger.prev().prev().text($('#eaidate_choose').contents().find('#selected_date').text()+' '+$('#eaidate_choose').contents().find('#selected_time').text());
  eaitrigger.prev().prev().show();
});

});
</script>

<script>
    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
</script> -->

    <div class="for_responsive" style="padding: 25px;padding-top: 10px;"><div class="op-location d-flex bg-blue-3  text-white align-items-center border-radius" onclick="jQuery('.defaultsection8').toggleClass('defaultshow');return false;">
        <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl"><i class="fa fa-map-marker fa-fw"></i> </div>
        <div class="font-lg p-2 pr-3 pl-3 mr-auto">Optional location</div>
        <button id="buttonOptionalLocation" class="btn p-2 pr-3 pl-3 btn-none  text-white"><i class="fa fa-plus font-lg"></i></button>
    </div></div>
    <style>
        .defaultsection8 {
            display: none;
        }
        .search input:hover{       	
			//color: #ccc !important;
        }
        
        .searchTerm,.search input{
			//color: #000 !important;
		}
		.search input::placeholder,.searchTerm::placeholder {
        	color: #ccc !important;
        }
        @media (max-width:600px){
			.for_responsive{
				padding:10px !important;
			}  
			#hotel2 input, #hotel3 input{
				margin-top: 10px !important;
				width:100%;
			}
			#carReturnDate,#carReturnTime,#carPickupDate,#carPickupTime{
				position: inherit !important;
				width: 100% !important;
			}
			#AdivahaCartrawlerSubmit{
				  position: inherit !important;
  				  margin: 10px 0px !important;
			}
			.search{
				height:0px !important;
			}
			#desti{
				width:100%;
				margin-bottom: 0px !important;
			}
			.searchButton2{
				height: auto !important;
				margin-top: 10px !important;
				width: 100% !important;
			} 
			.align-items-end{
				display: list-item !important;
			}
		}
    </style>
    <section style="padding: 25px;" class="defaultsection8 hideallsection showallsection defaultshow">

<div class="col-md-12 col-sm-12 col-xs-12" style="padding:10px;">
<div class="col-md-12" style="background:#f0f0f0">
<div class="row div_d"></div>
<div class="baidumap pb-3">
    
  <div class="searchkw">
      <div class="col-md-12" style="margin:14px 0">
          <input id="search_itname" name="spottitleitname" type="text" placeholder="Item Name" value="" class="searchTerm for_m_bottom itname_txt" style="width:100%;"/>
          <input id="search_kw" name="spottitle2" type="text" placeholder="Search for the place you want to go" value="" class="searchTerm" style="width:100%;"/>
      </div>

      <input type="hidden" value="" name="spotlat" id="spotlat">
    <input type="hidden" value="" name="spotlong" id="spotlong">
    <p id="location_error_message" style="margin: 10px 14px;color: red; display:none;">You need select one location from list.</p>
    <ul id="placeList" class="dropdown-content">
    </ul>
      <!--<div class="col-md-8">
          <div>Travel at which day of the journey ?</div>
          <select class="datepicker-tour-dates-select form-control" style="width: 200px;">

          </select>

      </div>-->
    <div class="col-md-4">
      *Which day of the tour:&nbsp;
      	<select class="datepicker-tour-dates-select" name="bookingdate2" style="display:block;width:50%;padding-top:11px!important;padding-bottom:11px!important;float:none;height:auto !important;border: 3px solid #368a8c !important;">
      		
      	</select>
             
         </div>
         <div class="col-md-4">
      *Travel time :&nbsp;&nbsp;<input type="text" name="bookingtime2" class="timepicker searchTerm bookingtime2" onclick="calltime()" style="display:block;width:50%;float:none">
         </div>
        <div class="col-md-4 text-right">
            <div>&nbsp;</div>
            <a id="addlocation" class="addlocation btn bg-blue-0 p-2 pr-4 pl-4 text-white" style="cursor: pointer;margin: 0;">Add place</a>
        </div>
    </div>
      <script src="<?php bloginfo('template_url'); ?>/src/wickedpicker.js"></script>
      <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/stylesheets/wickedpicker.css"/>
     
      <script>
        function calltime()
        {
          //console.log("works");
        var timepickers = jQuery(".timepicker").wickedpicker();
        console.log(timepickers.wickedpicker('time'));
      jQuery(".timepicker").wickedpicker('time');
        }
      </script>
  
<div class="col-md-12">
<!--<button type="submit" id="submittour" class="locsubmit searchButton2" style="margin-top:0px!important;height:48px !important;">-->
<!--    <i class="fa fa-search"></i>-->
<!--</button>-->


<input type="hidden" value="Add location" name="addlocation">
<input type="hidden" name="spottitle" id="spottitle" value="">
<input type="hidden" id="bookingdate" name="bookingdate" value="">
<input type="hidden" id="bookingtime" name="bookingtime" value="">
</div>
  </div>
   </div>
  </div>
       <div class="col-md-12" style="height: 340px">
     <div id="pac-container">
        
  
    </div>
    <div id="map"></div>
    <div id="infowindow-content">
      <img src="" width="16" height="16" id="place-icon">
      <span id="place-name"  class="title"></span><br>
      <span id="place-address"></span>
    </div>

    <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('search_kw');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          $("#spotlat").val(place.geometry.location.lat());    	
		  $('#spotlong').val(place.geometry.location.lng());
          //alert(place.geometry.location.lat());
          //alert(place.geometry.location.lng());
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        //setupClickListener('changetype-all', []);
        //setupClickListener('changetype-address', ['address']);
        //setupClickListener('changetype-establishment', ['establishment']);
        //setupClickListener('changetype-geocode', ['geocode']);

        /*document.getElementById('use-strict-bounds')
            .addEventListener('click', function() {
              console.log('Checkbox clicked! New state=' + this.checked);
              autocomplete.setOptions({strictBounds: this.checked});
            });*/
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA069EQK3M96DRcza2xuQb0DZxmYYkVVw8&libraries=places&callback=initMap"
        async defer></script>

  </div>
  </div>
                </section>


</form>

    
    <!--Manually added items-->
    <?php
    $totalcost=0;
    if(current_user_can('upload_files')) {
        wp_enqueue_media();
        $manualitems=$wpdb->get_results("SELECT meta_id,meta_value FROM `wp_tourmeta` where meta_key='manualitem' and tour_id=".(int)$tour_id.' ORDER BY `meta_id` ASC');
        $manualitem_total_price=0;
    ?>
    <div id="AddAnotherTravelItemManually" class="d-flex bg-blue-3  text-white align-items-center border-radius mb-2">
        <div class="border-radius bg-blue-0 p-1 pr-2 pl-2 font-xl"><i class="fa fa-tint fa-fw"></i> </div>
        <div class="font-lg p-2 pr-3 pl-3 mr-auto">Add Another Travel Item Manually</div>
        <button id="addManualItem" class="btn p-2 pr-3 pl-3 btn-none text-white"><i class="fa fa-plus font-lg"></i></button>
    </div>
    <div class="manual-item-box activity v2-box mb-3">
    
        <div class="v2-list">
        <?php
        foreach($manualitems as $item) {
            $item_id=$item->meta_id;
            $item=unserialize($item->meta_value);
            $manualitem_total_price+=$item['price'];
            $totalcost+=$item['price'];
        ?>
            <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                <div class="v2-list-left mb-3 mr-sm-3">
                    <div class="thumb-wrap">
                    <?php if($item['attachment_type']=='image') { ?>
                        <img src="<?php echo wp_get_attachment_url($item['attachment']); ?>" width="180" alt="" class="img-responsive">
                    <?php } else if($item['attachment_type']=='video') { ?>
                        <video class="wp-video-shortcode" width="200" controls="controls" src="<?php echo wp_get_attachment_url($item['attachment']); ?>">
                            <source type="video/mp4" src="<?php echo wp_get_attachment_url($item['attachment']); ?>">
                        </video>
                    <?php } ?>
                    </div>
                </div>
                <div class="v2-list-right trav_cu_add_item">
                    <div class="d-flex flex-row">
                        <div class="v2-actions order-last">
                            <a item="<?php echo $item_id; ?>" class="manual-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                        </div>
                        <div class="mr-auto trav_cu_add_item_head">
                            <h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0 trav_cu_add_item_icon"></i> <?php echo $item['name']; ?>  </h4>
                        </div>
                    </div>
                    <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                        <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                            <div class="flex-fill p-2 border-right trav_cu_add_item_data_size ">
                                <i class="fa fa-calendar-alt mr-1 text-blue-0"></i>Day<?php echo $item['date']; ?> <?php echo $item['time']; ?>
                            </div>
                            <div class="flex-fill p-2 border-right trav_cu_add_item_data_size ">
                                Quantity: <?php echo $item['qty']; ?>
                            </div>
                            <div class="flex-fill p-2 border-right trav_cu_add_item_data_size ">
                                <span class="change_price" or_pic="<?php echo $item['price']; ?>"> Rp <span class="price_span"><?php echo $item['price']; ?> </span></span>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center trav_cu_item_desc">
                        <p><?php echo $item['description']; ?></p>
                    </div>

                </div>

            </div>
        <?php } ?>
        </div>
    </div>
    <script>
    var existedManualItemBox=false;
    jQuery(document).ready(function ($) {
    	 $('[data-toggle="tooltip"]').tooltip();
        new Tooltip(document.getElementById('AddAnotherTravelItemManually'), { title: 'Currently some travcust travel items pricing only displays south east asia local areas (but some price are global). If you\'d like to create tour packages beyond south east asia, please add your travel items here with your own price and pictures/videos.', placement: 'bottom', html: true, container: 'body', template: '<div class="popper-tooltip popper-tooltip-style1" role="tooltip"><div class="tooltip__arrow"></div><div class="tooltip__inner"></div></div>' });

        $('#AddAnotherTravelItemManually').click(function(e) {
            e.preventDefault();
            if(!existedManualItemBox)
                $(this).next().find('.v2-list').append('<div class="v2-list-item d-flex flex-column pb-3 mb-3 border-bottom-blue"><div class="v2-list-left mb-3"><div class="upload-wrap"><a class="img_video_upload"><i class="fa fa-cloud-upload-alt"></i><strong>Click to   Upload a Pictures or Video</strong></a><input class="item_attachment" type="hidden" value="" /><input                class="item_attachment_url" type="hidden" value="" /><input class="item_attachment_type" type="hidden"   value="" /></div></div><div class="v2-list-right"><div class="d-flex flex-row "><div class="v2-actions order-last fixed-width-80"><a item="0" class="manual-item-trash btn bg-org-0 text-white"><i class="fa fa-trash"></i> Delete</a></div><div class="flex-fill"><h4 class="v2-title mt-0 mb-3 d-flex pr-2 align-items-center"><i class="fa fa-flag text-gray-0"></i><input class="item_name flex-fill" type="text"  placeholder="travel item\'s name"     value="" /></h4></div></div><div class="d-flex flex-row"><div class="pt-5 pb-5 gutter"><div class="date-picker-container"><div class="deliver-app-container manual-item-date" ng-app="deliveryDateTimeApp" ng-controller="dateTimeCtrl as ctrl"></div></div></div></div><div class="d-flex flex-column justify-content-between align-items-stretch"><div class="row "><div class="col-md-4 manualmobile"><i class="fa fa-diagnoses text-gray-0"></i> Unit Price: <input id="item_unit_price" class="item_unit_price" type="text" value="0.00" /></div><div class="col-md-4 manualmobile">Quantity:<input class="item_qty" type="number" min="1" max="999" style="width:150px!important"/></div><div class="col-md-4 manualmobile">Total cost: <span class="item_price_syml">'+$(".header_curreny").find("option:selected").attr("syml")+'</span><input class="item_price" type="text" value="0.00" readonly /></div><div class="fixed-width-80"></div></div><div class="row manualmobile"><div class="flex-fill p-2"><textarea class="item_description" placeholder="Description" style="width: 100%;"></textarea></div><div class="fixed-width-80 pt-2 pb-2"><a class="btn bg-org-0 text-white add_manualitem"><i class="fa fa-plus"></i> Add</a></div></div></div></div></div>');

            else{
                $(this).next().find('.v2-list').toggle();
                    if ($('.v2-list').is(':visible')){
                       
                       $("#addManualItem > i").removeClass("fa-plus");
                       $("#addManualItem > i").addClass("fa-minus");
                    }
                    else{
                        
                        $("#addManualItem > i").removeClass("fa-minus");
                        $("#addManualItem > i").addClass("fa-plus");
                    }
            }
            new Tooltip(document.getElementById('item_unit_price'),{title:'Insert your net price ',trigger:'focus',placement:'top',container:'body',template:'<div class="popper-tooltip" role="tooltip"><div class="tooltip__arrow"></div><div class="tooltip__inner"></div></div>'});
            new tourDayPicker($('.manual-item-date'),function (piker) {});
            $('.manual-item-date .save-button').remove();
            $('.manual-item-box .v2-list').last().find('.item_name').focus();
            existedManualItemBox=true;
        });
        
        $(document).on('change', '.item_unit_price,.item_qty', function (event) {
            var unit_price=parseFloat($('.item_unit_price').val());
            var qty=parseFloat($('.item_qty').val());
            if(unit_price>0 && qty>0) {
                $('.item_price').val((unit_price*qty).toFixed(2));
            }
            else {
                $('.item_price').val('0.00');
            }
        });
        
        var additem;
        $(document).on('click', '.add_manualitem', function (event) {
            additem = $(this).parent().parent().parent().parent().parent();
            
            if (additem.find(".item_name").val() == '') {
                $('.item_name').focus();
                additem.find('.item_name').css('border-color', 'red');
                return;
            }
            additem.find('.item_name').css('border-color', '');
            
            if (additem.find(".datepicker-tour-dates-select").val() == '') {
                $('.datepicker-tour-dates-select').focus();
                additem.find(".datepicker-tour-dates-select").css('border-color', 'red');
                return;
            }
            additem.find(".datepicker-tour-dates-select").css('border-color', '');
            
            if (additem.find(".data-time").text() == '') {
                $('.data-time').focus();
                //additem.find(".data-time").css('border-color', 'red');
                return;
            }
            //additem.find(".item_time").css('border-color', '');
            
            if (!(parseInt(additem.find(".item_qty").val())>0)) {
                $('.item_qty').focus();
                additem.find(".item_qty").css('border-color', 'red');
                return;
            }
            additem.find(".item_qty").css('border-color', '');
            
            if (!(parseFloat(additem.find(".item_unit_price").val())>0)) {
                $('.item_unit_price').focus();
                additem.find(".item_unit_price").css('border-color', 'red');
                return;
            }
            additem.find(".item_unit_price").css('border-color', '');
            
            if (additem.find(".item_description").val() == '') {
                $('.item_description').focus();
                additem.find(".item_description").css('border-color', 'red');
                return;
            }
            additem.find(".item_description").css('border-color', '');

            var rp_price=0;
            if($('.header_curreny').val()=='IDR')
                rp_price=Math.round(additem.find(".item_price").val());
            else if($('.header_curreny').val()=='CNY')
                rp_price=Math.round(additem.find(".item_price").val() / $("#cs_RMB").val());
            else if($('.header_curreny').val()=='USD')
                rp_price=Math.round(additem.find(".item_price").val() / $("#cs_USD").val());
            else if($('.header_curreny').val()=='AUD')
                rp_price=Math.round(additem.find(".item_price").val() / $("#cs_AUD").val());
            else
                return;
            var contents = {
                name: additem.find(".item_name").val(),
                attachment: additem.find(".item_attachment").val(),
                attachment_type: additem.find(".item_attachment_type").val(),
                date: additem.find(".datepicker-tour-dates-select").val(),
                time: additem.find(".data-time").text(),
                qty: additem.find(".item_qty").val(),
                price: rp_price,
                description: additem.find(".item_description").val(),
            };
            $.ajax({
                type: "POST",
                url: '/English/wp-content/themes/bali/api.php?action=addManualItem&tour_id=<?php echo $tour_id; ?>',
                data: contents,
                success: function (data) {
                    console.log(data);
                    if (data != 0) {
                        existedManualItemBox=false;
                        var price = rp_price;
                        var newitem = '<div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue"><div class="v2-list-left mb-3 mr-sm-3"><div class="thumb-wrap">';

                        if (additem.find(".item_attachment_type").val() == 'image') {
                            newitem+='<img src="' + additem.find(".item_attachment_url").val() + '" width="180" alt="" class="img-responsive">';
                        }
                        else if (additem.find(".item_attachment_type").val() == 'video') {
                            var attachment_url = additem.find(".item_attachment_url").val();
                            newitem+='<video class="wp-video-shortcode" width="200" controls="controls" src="' + attachment_url + '"><source type="video/mp4" src="' + attachment_url + '"></video>';
                        }
                        
                        newitem+='</div></div><div class="v2-list-right trav_cu_add_item"><div class="d-flex flex-row"><div class="v2-actions order-last">';
            
                        newitem+='<a item="' + data + '" class="manual-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i></a>';
                        
                        newitem+='</div><div class="mr-auto trav_cu_add_item_head">';
                        
                        newitem+='<h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i>' + additem.find(".item_name").val() + '</h4>';
                        
                        newitem+='</div></div><div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch"><div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">';
                        
                        newitem+='<div class="flex-fill p-2 border-right trav_cu_add_item_data_size"><i class="fa fa-calendar-alt mr-1 text-blue-0"></i>Day' + additem.find(".datepicker-tour-dates-select").val() +' '+ additem.find(".data-time").text() + '</div>';

                        newitem+='<div class="flex-fill p-2 border-right trav_cu_add_item_data_size">Quantity: ' + additem.find(".item_qty").val() + '</div>';

                        newitem+='<div class="flex-fill p-2 border-right trav_cu_add_item_data_size"><span class="change_price" or_pic="' + price + '"> Rp <span class="price_span">' + price + ' </span></span></div>';
                        
                        newitem+='</div></div><div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center trav_cu_item_desc">';
                        
                        newitem+='<p>' + additem.find(".item_description").val() + '</p></div></div></div>';
                        
                        additem.after(newitem).remove();
                        
                        $('.total-cost .change_price').attr('or_pic', (parseInt($('.total-cost .change_price').attr('or_pic'))+price ) );
                        $('.manualitemsection .change_price').attr('or_pic', (parseInt($('.manualitemsection .change_price').attr('or_pic'))+price ));
                        $(".header_curreny").change();
                        setCookie('update', 1, 1);
                    }
                }
            });
        });
        
        $('.v2-list').on('click','.manual-item-trash',function(){
            var item=$(this).parent().parent().parent().parent();
            var price=parseInt(item.find('.change_price').attr('or_pic'));
            var i=parseInt($(this).attr('item'));
            
            if(i==0) {
                item.remove();
                existedManualItemBox=false;
                return;
            }

            $('.manualitemsection .change_price').attr('or_pic', (parseInt($('.manualitemsection .change_price').attr('or_pic'))-price ));
            $('.total-cost .change_price').attr('or_pic', (parseInt($('.total-cost .change_price').attr('or_pic'))-price ) );
            $(".header_curreny").change();
            item.remove();
    
            $.ajax({
                type: "GET",
                url: '/English/wp-content/themes/bali/api.php?action=delManualItem&tourid=<?php echo $tour_id; ?>&item='+i,
                success: function (data) {
                    console.log(data);
                }
            });
        });

        var upload_frame;
        var t;
        jQuery(document).on('click', '.img_video_upload', function (event) {
            t = jQuery(this);
            event.preventDefault();
            if (upload_frame) {
                upload_frame.open();
                return;
            }
            upload_frame = wp.media({
                title: 'Insert image',
                button: {
                    text: 'Insert',
                },
                multiple: false
            });
            upload_frame.on('select', function () {
                attachment = upload_frame.state().get('selection').first().toJSON();
                console.log(attachment);
                t.parent().find(".item_attachment").val(attachment.id);
                t.parent().find(".item_attachment_url").val(attachment.url);
                t.parent().find(".item_attachment_type").val(attachment.type);
            });
            upload_frame.open();

             // Added by CM on 26/11/2019 : START //
            var custom_element;
            if (t.parent().find(".item_attachment_type").val() == 'image') {
                custom_element='<img src="' + t.parent().find(".item_attachment_url").val() + '" width="180" alt="" class="img-responsive">';
           }else if (t.parent().find(".item_attachment_type").val() == 'video') {
                custom_element ='<video class="wp-video-shortcode" width="200" controls="controls" src="' + t.parent().find(".item_attachment_url").val() + '"><source type="video/mp4" src="' + t.parent().find(".item_attachment_url").val() + '"></video>';
           }
            t.after(custom_element);
            // Added by CM on 26/11/2019 : END //
        });
    });

    </script>
    <?php } ?>
    <!--Manually added items end-->

                    
<script>
    jQuery(document).ready(function(){
      
      jQuery(document).on('click','.custom_btn',function(){
      		var id = $(this).data('id');
      		var optional_location_d = getCookie('optional_location');			
			var op = JSON.parse(optional_location_d);
		
      		//console.log(op);
      		var data = $.grep(op, function(e){ 
			 return e.id !== id; 
			});
			 setCookie('optional_location',JSON.stringify(data));
      		$('.location_number').html(data.length);
      		$('.div_d').empty();
			$.each(data,function(key,val){
				$('.div_d').append('<div class="list_location"><div class="map_div"><a href="https://maps.google.com/maps?q='+val.custom_lat+','+val.custom_lng+'&hl=es;z=5&amp;output=embed" style="color:#0000FF;text-align:left" target="_blank"><iframe width="200" height="120" frameborder="0"scrolling="no" margin height="0"marginwidth="0" src="https://maps.google.com/maps?q='+val.custom_lat+','+val.custom_lng+'&hl=es&z=5&amp;output=embed"></iframe></a></div><div class="hotel_map"><h1><i class="fas fa-utensils fa-fw"></i> '+val.searchitemname+'</h1><p><i class="fas fa-map-pin"></i> '+val.spotnames+' </p></div><div class="date_d"><i class="fas fa-calendar-week"></i> '+val.spotdate+' <i class="fa fa-clock"></i><br >'+val.spottime+'</div><div class="close_btn"><span class="btn btn-default custom_btn" data-id="'+val.id+'">X</span></div></div>')
			});
      })
      
      //alert(getCookie('spotname'));
      if( getCookie('optional_location')!=='') {
    		var optional_location_d = getCookie('optional_location');			
			var op = JSON.parse(optional_location_d);
			$('.location_number').html(op.length);	
			$.each(op,function(key,val){
				//console.log(''+val.custom_lat+','+val.custom_lng+'');
				$('.div_d').append('<div class="list_location"><div class="map_div"><a href="https://maps.google.com/maps?q='+val.custom_lat+','+val.custom_lng+'&hl=es;z=5&amp;output=embed" style="color:#0000FF;text-align:left" target="_blank"><iframe width="200" height="120" frameborder="0"scrolling="no" margin height="0"marginwidth="0" src="https://maps.google.com/maps?q='+val.custom_lat+','+val.custom_lng+'&hl=es&z=5&amp;output=embed"></iframe></a></div><div class="hotel_map"><h1><i class="fas fa-utensils fa-fw"></i> '+val.searchitemname+'</h1><p><i class="fas fa-map-pin"></i> '+val.spotnames+' </p></div><div class="date_d"><i class="fas fa-calendar-week"></i> Day 2 <i class="fa fa-clock"></i><br >'+val.spottime+'</div><div class="close_btn"><span class="btn btn-default custom_btn" data-id="'+val.id+'">X</span></div></div>')
			});
	  }
      jQuery('.defaultsection8').removeClass('defaultshow');
      var destarr=new Array();
      var latarr=new Array();
      var longarr=new Array();
      var datearr=new Array();
      var timearr=new Array();
      
      var luncharrrup=new Array();
      var tourguidearrrup=new Array();
      var insurancearrrup=new Array();
      
      var luncharrdollar=new Array();
      var tourguidearrdollar=new Array();
      var insurancearrdollar=new Array();
      
      var  _6seats_halfday_rup_total=new Array();
      var _6seats_halfday_dollar_total=new Array();
      var _12seats_halfday_rup_total=new Array();
      var _12seats_halfday_dollar_total=new Array();
      var _6seats_baliisland_rup_total=new Array();
      var _6seats_baliisland_dollar_total=new Array();
      var _12seats_baliisland_rup_total=new Array();
      var _12seats_baliisland_dollar_total=new Array();
      var _16seats_baliisland_rup_total=new Array();
      var _16seats_baliisland_dollar_total=new Array();
      var _29seats_baliisland_rup_total=new Array();
      var _29seats_baliisland_dollar_total=new Array();
      var _35seats_baliisland_rup_total=new Array();
      var _35seats_baliisland_dollar_total=new Array();
      var _45seats_baliisland_rup_total=new Array();
      var _45seats_baliisland_dollar_total=new Array();
      var _6seats_balitoother_rup_total=new Array();
      var _6seats_balitoother_dollar_total=new Array();
      var _16seats_balitoother_rup_total=new Array();
      var _16seats_balitoother_dollar_total=new Array();
      var _28seats_balitoother_rup_total=new Array();
      var _28seats_balitoother_dollar_total=new Array();
      var _35seats_balitoother_rup_total=new Array();
      var _35seats_balitoother_dollar_total=new Array();
      var _45seats_balitoother_rup_total=new Array();
      var _45seats_balitoother_dollar_total=new Array();
      var _6seats_javaisland_rup_total=new Array();
      var _6seats_javaisland_dollar_total=new Array();
      var _15seats_javaisland_rup_total=new Array();
      var _15seats_javaisland_dollar_total=new Array();
      var _19seats_javaisland_rup_total=new Array();
      var _19seats_javaisland_dollar_total=new Array();
      var _31seats_javaisland_rup_total=new Array();
      var _31seats_javaisland_dollar_total=new Array();
      var _59seats_javaisland_rup_total=new Array();
      var _59seats_javaisland_dollar_total=new Array();
      
      var spanTotalRP2=new Array();
      var spanTotalRB2=new Array();
      
      var spanTaxAndServiceRP2=new Array();
      var spanTaxAndServiceRB2=new Array();
      
      var spanTotalAndFeeRP2=new Array();
      var spanTotalAndFeeRb2=new Array();
            
      
      
      var d,lat,long,date,time,last;
      /*
    jQuery('#search_kw').keyup(function(e){
          console.log('searchkw working');
            var des=jQuery(this);
    t=jQuery(this).val();
        last=e.timeStamp;
    setTimeout(function(){
    if(last-e.timeStamp===0){
    jQuery.getJSON('/English/wp-content/themes/bali/api2.php', {
      action: 'search_map',
      kw: t
    }).done(function(data) {
                         
      jQuery('#placeList>li').remove();
      jQuery('#placeList>input').remove();
  
      jQuery.each(data, function(index, dest) {
                                
        if(typeof(dest.names.poi)!='undefined' && typeof(dest.approximate_possition.lat)!='undefined' && typeof(dest.approximate_possition.lon)!='undefined')
        {
        jQuery('#placeList').append('<li>'+dest.names.poi+', '+dest.names.city+', '+dest.names.country+', '+(typeof(dest.names.poi_category_group)=='undefined'?'':', '+dest.names.poi_category_group)+'</li>');
        jQuery('#placeList').append('<input type="hidden" value="'+dest.approximate_possition.lat+'" />');
        jQuery('#placeList').append('<input type="hidden" value="'+dest.approximate_possition.lon+'" />');
        }
      });
  
      jQuery('#placeList>li').click(function(){
              
               d=jQuery(this).text();
               lat=jQuery(this).next().val();
               long=jQuery(this).next().next().val();
               date=jQuery('.bookingdate').val();
               time=jQuery('.timepicker').val();          
              
        //console.log(jQuery(this).text()+jQuery(this).next().val()+' - '+jQuery(this).next().next().val());
        des.val(jQuery(this).text());
        $('#spotlat').val(jQuery(this).next().val());
        $('#spotlong').val(jQuery(this).next().next().val());
        jQuery('#placeList').hide();
        L.marker([jQuery(this).next().val(), jQuery(this).next().next().val()]).addTo(map);
        map.setView([jQuery(this).next().val(), jQuery(this).next().next().val()], 13);
      });
  
           
        
      jQuery('#placeList').show();
    });
        } 
    },800); 
    });
     */
     
       var optional_locations = [];
       
jQuery('#addlocation').click(function(){
	
  if(jQuery('#search_kw').val()!=='')
  {
    var spotname= $('#search_kw').val();
        var spotlat='';
        var spotlong='';
        var spotdate='';
        var spottime='';
        var spotname2='';
        var spottime2='';
        var spotdate2='';
        var fieldEmpty=false;
		var custom_lat="";
		var  custom_lng="";
        if (jQuery('#search_kw').val() !== '') {
          var loc =$('#search_kw').val();
    
    /*

            if(jQuery('#spotlat').val()=='' || jQuery('#spotlong').val()=='') {
                $('#location_error_message').show();
                return;
            }
            else {
                $('#location_error_message').hide();
            }
            */
            
            if ($('.bookingtime2').val() == '') {
                $('.bookingtime2').focus();
                $('.bookingtime2').css('border-color', 'red');
                fieldEmpty=true;
            }
            if ($('.bookingdate').val() == '') {
                $('.bookingdate').focus();
                $('.bookingdate').css('border-color', 'red');
                fieldEmpty=true;
            }
            if(fieldEmpty)
                return;
            else
            {
                $('.bookingtime2').css('border-color', '#368a8c');
                $('.bookingdate').css('border-color', '#368a8c');
            }
           
            if(getCookie('optional_location')!='' &&  getCookie('spotname')!='' &&  getCookie('spotdate')!='' && getCookie('spottime')!='') {
                spotname=getCookie('spotname');

                spotdate=getCookie('spotdate');
                spottime=getCookie('spottime');
                custom_lat= getCookie('optinal_lat');
				custom_lng = getCookie('optinal_lng');
				
				
                spotname2=getCookie('spotname2');
				spotdate2=getCookie('spotdate2');
                spottime2=getCookie('spottime2');  
                custom_lat= getCookie('optinal_lat');
				custom_lng = getCookie('optinal_lng');
				optional_locations=[];
				var optional_location_d = getCookie('optional_location');
				$.each(JSON.parse(optional_location_d),function(k,v){
					optional_locations.push(v);
				})
				
				//console.log(JSON.parse(optional_location_d));
		    } 
            
           
            if(jQuery('#search_kw').val()!='' && jQuery('#search_itname').val()!='') {
                spotname+=jQuery('#search_kw').val() + ':,';
                spotdate+=jQuery('[name="bookingdate2"]').val() + ';,';
                spottime+=jQuery('.bookingtime2').val() + ';,';
			    
			    
			    spotname2  = jQuery('#search_kw').val();
                spotdate2  = jQuery('[name="bookingdate2"]').val();
                spottime2  = jQuery('.bookingtime2').val();
				custom_lat = $("#spotlat").val();
				custom_lng = $("#spotlong").val();
               	search_itname = $("#search_itname").val();
              	setCookie('spotname', spotname, 30);
				setCookie('spotdate', spotdate, 30);
				setCookie('spottime', spottime, 30);
				setCookie('optinal_lat', custom_lat, 30);
				setCookie('optinal_lng', custom_lng, 30);
				var r = Math.random().toString(36).substring(7);
				
				optional_locations.push({"id":r,"searchitemname":search_itname,"spotnames":spotname2,"spotdate":spotdate2,"spottime":spottime2,"custom_lat":custom_lat,"custom_lng":custom_lng});
			   	
			   	$('.div_d').html("");
			    $.each(optional_locations,function(key,val){
					//console.log(''+val.custom_lat+','+val.custom_lng+'');
					$('.div_d').append('<div class="list_location"><div class="map_div"><a href="https://maps.google.com/maps?q='+val.custom_lat+','+val.custom_lng+'&hl=es;z=5&amp;output=embed" style="color:#0000FF;text-align:left" target="_blank"><iframe width="200" height="120" frameborder="0"scrolling="no" margin height="0"marginwidth="0" src="https://maps.google.com/maps?q='+val.custom_lat+','+val.custom_lng+'&hl=es&z=5&amp;output=embed"></iframe></a></div><div class="hotel_map"><h1><i class="fas fa-utensils fa-fw"></i> '+val.searchitemname+'</h1><p><i class="fas fa-map-pin"></i> '+val.spotnames+' </p></div><div class="date_d"><i class="fas fa-calendar-week"></i> '+val.spotdate+' <i class="fa fa-clock"></i><br >'+val.spottime+'</div><div class="close_btn"><span class="btn btn-default custom_btn" data-id="'+val.id+'">X</span></div></div>')
				});
			    setCookie('optional_location',JSON.stringify(optional_locations));
                //setCookie('location_number', parseInt(optional_locations.length), 30);
                //setCookie('location_number', parseInt(jQuery('.location_number').text())+1, 30);
                $('.location_number').html(optional_locations.length);
                setCookie('update', 1, 1);

                jQuery('#search_kw').val("");
              //  jQuery('#search_kw').next().val("");
              //  jQuery('#search_kw').next().next().val("");
                jQuery('.bookingdate').val("");
                jQuery('.bookingtime').val("");
                $("#search_itname").val(""); 
                jQuery('.bookingtime2').val("");
               // jQuery('.location_number').text(parseInt(optional_locations.length)+1);
            }
    }
  }
      
    
        /*
        console.log(jQuery('#search_kw').next().val());
        console.log(jQuery('#search_kw').next().next().val());
        console.log(jQuery('.bookingdate').val());
        console.log(jQuery('.bookingtime').val());
          
          console.log(jQuery('#lunch_per_serve_total_rup').val());
          
          
          
          destarr.push(jQuery('#search_kw').val()+':');
          //console.log(destarr);
          latarr.push(jQuery('#search_kw').next().val()+':');
          //console.log(latarr);
          longarr.push(jQuery('#search_kw').next().next().val()+':');
          //console.log(longarr);
          datearr.push(jQuery('.bookingdate').val()+':');
          //console.log(datearr);
          timearr.push(jQuery('.timepicker').val()+':');
          //console.log(timearr);
          
          luncharrrup.push(jQuery('#lunch_per_serve_total_rup').val()+':');
          //console.log(luncharrrup);
          
          luncharrdollar.push(jQuery('#lunch_per_serve_total_dollar').val()+':');
          
          tourguidearrrup.push(jQuery('#tourguide_per_day_total_rup').val()+':');
          tourguidearrdollar.push(jQuery('#tourguide_per_day_total_dollar').val()+':'); 
          
          insurancearrrup.push(jQuery('#insurance_per_person_rup_total').val()+':');
          insurancearrdollar.push(jQuery('#insurance_per_person_dollar_total').val()+':');
          
          
          _6seats_halfday_rup_total.push(jQuery('#6seats_halfday_rup_total').val()+':');
          _6seats_halfday_dollar_total.push(jQuery('#6seats_halfday_dollar_total').val()+':'); 
          _12seats_halfday_rup_total.push(jQuery('#12seats_halfday_rup_total').val()+':'); 
          _12seats_halfday_dollar_total.push(jQuery('#12seats_halfday_dollar_total').val()+':'); 
          _6seats_baliisland_rup_total.push(jQuery('#6seats_baliisland_rup_total').val()+':'); 
          _6seats_baliisland_dollar_total.push(jQuery('#6seats_baliisland_dollar_total').val()+':'); 
          _12seats_baliisland_rup_total.push(jQuery('#12seats_baliisland_rup_total').val()+':'); 
          _12seats_baliisland_dollar_total.push(jQuery('#12seats_baliisland_dollar_total').val()+':'); 
          _16seats_baliisland_rup_total.push(jQuery('#16seats_baliisland_rup_total').val()+':'); 
          _16seats_baliisland_dollar_total.push(jQuery('#16seats_baliisland_dollar_total').val()+':'); 
          _29seats_baliisland_rup_total.push(jQuery('#29seats_baliisland_rup_total').val()+':'); 
          _29seats_baliisland_dollar_total.push(jQuery('#29seats_baliisland_dollar_total').val()+':'); 
          _35seats_baliisland_rup_total.push(jQuery('#35seats_baliisland_rup_total').val()+':'); 
          _35seats_baliisland_dollar_total.push(jQuery('#35seats_baliisland_dollar_total').val()+':'); 
          _45seats_baliisland_rup_total.push(jQuery('#45seats_baliisland_rup_total').val()+':'); 
          _45seats_baliisland_dollar_total.push(jQuery('#45seats_baliisland_dollar_total').val()+':'); 
          _6seats_balitoother_rup_total.push(jQuery('#6seats_balitoother_rup_total').val()+':'); 
          _6seats_balitoother_dollar_total.push(jQuery('#6seats_balitoother_dollar_total').val()+':'); 
          _16seats_balitoother_rup_total.push(jQuery('#16seats_balitoother_rup_total').val()+':'); 
          _16seats_balitoother_dollar_total.push(jQuery('#16seats_balitoother_dollar_total').val()+':'); 
          _28seats_balitoother_rup_total.push(jQuery('#28seats_balitoother_rup_total').val()+':'); 
          _28seats_balitoother_dollar_total.push(jQuery('#28seats_balitoother_dollar_total').val()+':'); 
          _35seats_balitoother_rup_total.push(jQuery('#35seats_balitoother_rup_total').val()+':'); 
          _35seats_balitoother_dollar_total.push(jQuery('#35seats_balitoother_dollar_total').val()+':'); 
          _45seats_balitoother_rup_total.push(jQuery('#45seats_balitoother_rup_total').val()+':'); 
          _45seats_balitoother_dollar_total.push(jQuery('#45seats_balitoother_dollar_total').val()+':'); 
          _6seats_javaisland_rup_total.push(jQuery('#6seats_javaisland_rup_total').val()+':'); 
          _6seats_javaisland_dollar_total.push(jQuery('#6seats_javaisland_dollar_total').val()+':'); 
          _15seats_javaisland_rup_total.push(jQuery('#15seats_javaisland_rup_total').val()+':'); 
          _15seats_javaisland_dollar_total.push(jQuery('#15seats_javaisland_dollar_total').val()+':'); 
          _19seats_javaisland_rup_total.push(jQuery('#19seats_javaisland_rup_total').val()+':'); 
          _19seats_javaisland_dollar_total.push(jQuery('#19seats_javaisland_dollar_total').val()+':'); 
          _31seats_javaisland_rup_total.push(jQuery('#31seats_javaisland_rup_total').val()+':'); 
          _31seats_javaisland_dollar_total.push(jQuery('#31seats_javaisland_dollar_total').val()+':'); 
          _59seats_javaisland_rup_total.push(jQuery('#59seats_javaisland_rup_total').val()+':'); 
          _59seats_javaisland_dollar_total.push(jQuery('#59seats_javaisland_dollar_total').val()+':'); 
          
          spanTotalRP2.push(jQuery('#inpspanTotalRP2').val()+':');
          spanTotalRB2.push(jQuery('#inpspanTotalRB2').val()+':');
          
          spanTaxAndServiceRP2.push(jQuery('#inpspanTaxAndServiceRP2').val()+':');
          spanTaxAndServiceRB2.push(jQuery('#inpspanTaxAndServiceRB2').val()+':');
          
          spanTotalAndFeeRP2.push(jQuery('#inpspanTotalAndFeeRP2').val()+':');
          spanTotalAndFeeRb2.push(jQuery('#inpspanTotalAndFeeRb2').val()+':');
          
          
          
          
          jQuery('#search_kw').val("");
          jQuery('#search_kw').next().val("");
          jQuery('#search_kw').next().next().val("");
          jQuery('.bookingdate').val("");
          jQuery('.bookingtime').val("");
           jQuery('.timepicker').val("");
          
           jQuery('#lunch_per_serve_total_rup').val("");
           jQuery('#lunch_per_serve_total_dollar').val("");
           jQuery('#tourguide_per_day_total_rup').val("");
           jQuery('#tourguide_per_day_total_dollar').val("");
           jQuery('#insurance_per_person_rup_total').val("");
           jQuery('#insurance_per_person_dollar_total').val("");
          
          
          
           jQuery('#6seats_halfday_rup_total').val("");
           jQuery('#6seats_halfday_dollar_total').val("");
           jQuery('#12seats_halfday_rup_total').val("");
           jQuery('#12seats_halfday_dollar_total').val("");
           jQuery('#6seats_baliisland_rup_total').val("");
           jQuery('#6seats_baliisland_dollar_total').val("");
          
          jQuery('#12seats_baliisland_rup_total').val("");
           jQuery('#12seats_baliisland_dollar_total').val("");
           jQuery('#16seats_baliisland_rup_total').val("");
           jQuery('#16seats_baliisland_dollar_total').val("");
           jQuery('#29seats_baliisland_rup_total').val("");
           jQuery('#29seats_baliisland_dollar_total').val("");
          
          jQuery('#35seats_baliisland_rup_total').val("");
           jQuery('#35seats_baliisland_dollar_total').val("");
           jQuery('#45seats_baliisland_rup_total').val("");
           jQuery('#45seats_baliisland_dollar_total').val("");
           jQuery('#6seats_balitoother_rup_total').val("");
           jQuery('#6seats_balitoother_dollar_total').val("");
          
          jQuery('#16seats_balitoother_rup_total').val("");
           jQuery('#16seats_balitoother_dollar_total').val("");
           jQuery('#28seats_balitoother_rup_total').val("");
           jQuery('#28seats_balitoother_dollar_total').val("");
           jQuery('#35seats_balitoother_rup_total').val("");
           jQuery('#35seats_balitoother_dollar_total').val("");
          
          jQuery('#45seats_balitoother_rup_total').val("");
           jQuery('#45seats_balitoother_dollar_total').val("");
           jQuery('#6seats_javaisland_rup_total').val("");
           jQuery('#6seats_javaisland_dollar_total').val("");
           jQuery('#15seats_javaisland_rup_total').val("");
           jQuery('#15seats_javaisland_dollar_total').val("");
          
          jQuery('#19seats_javaisland_rup_total').val("");
           jQuery('#19seats_javaisland_dollar_total').val("");
           jQuery('#31seats_javaisland_rup_total').val("");
           jQuery('#31seats_javaisland_dollar_total').val("");
           jQuery('#59seats_javaisland_rup_total').val("");
           jQuery('#59seats_javaisland_dollar_total').val("");
          
          
           jQuery('#inpspanTotalRP2').val("");
           jQuery('#inpspanTotalRB2').val("");
           jQuery('#inpspanTaxAndServiceRP2').val("");
           jQuery('#inpspanTaxAndServiceRB2').val("");
           jQuery('#inpspanTotalAndFeeRP2').val("");
           jQuery('#inpspanTotalAndFeeRb2').val("");
          
          
          
          
        }
        jQuery('#spottitle').val(destarr);
        jQuery('#spotlat').val(latarr);
        jQuery('#spotlong').val(longarr);
         jQuery('#bookingdate').val(datearr);
        jQuery('#bookingtime').val(timearr);
        jQuery('#lunch_per_serve_total_rup').val(luncharrrup);
        jQuery('#lunch_per_serve_total_dollar').val(luncharrdollar);
        
           jQuery('#tourguide_per_day_total_rup').val(tourguidearrrup);
           jQuery('#tourguide_per_day_total_dollar').val(tourguidearrdollar);
           jQuery('#insurance_per_person_rup_total').val(insurancearrrup);
           jQuery('#insurance_per_person_dollar_total').val(insurancearrdollar);
        
        jQuery('#6seats_halfday_rup_total').val(_6seats_halfday_rup_total);
           jQuery('#6seats_halfday_dollar_total').val(_6seats_halfday_dollar_total);
           jQuery('#12seats_halfday_rup_total').val(_12seats_halfday_rup_total);
           jQuery('#12seats_halfday_dollar_total').val(_12seats_halfday_dollar_total);
           jQuery('#6seats_baliisland_rup_total').val(_6seats_baliisland_rup_total);
           jQuery('#6seats_baliisland_dollar_total').val(_6seats_baliisland_dollar_total);
          
          jQuery('#12seats_baliisland_rup_total').val(_12seats_baliisland_rup_total);
           jQuery('#12seats_baliisland_dollar_total').val(_12seats_baliisland_dollar_total);
           jQuery('#16seats_baliisland_rup_total').val(_16seats_baliisland_rup_total);
           jQuery('#16seats_baliisland_dollar_total').val(_16seats_baliisland_dollar_total);
           jQuery('#29seats_baliisland_rup_total').val(_29seats_baliisland_rup_total);
           jQuery('#29seats_baliisland_dollar_total').val(_29seats_baliisland_dollar_total);
          
          jQuery('#35seats_baliisland_rup_total').val(_35seats_baliisland_rup_total);
           jQuery('#35seats_baliisland_dollar_total').val(_35seats_baliisland_dollar_total);
           jQuery('#45seats_baliisland_rup_total').val(_45seats_baliisland_rup_total);
           jQuery('#45seats_baliisland_dollar_total').val(_45seats_baliisland_dollar_total);
           jQuery('#6seats_balitoother_rup_total').val(_6seats_balitoother_rup_total);
           jQuery('#6seats_balitoother_dollar_total').val(_6seats_balitoother_dollar_total);
          
          jQuery('#16seats_balitoother_rup_total').val(_16seats_balitoother_rup_total);
           jQuery('#16seats_balitoother_dollar_total').val(_16seats_balitoother_dollar_total);
           jQuery('#28seats_balitoother_rup_total').val(_28seats_balitoother_rup_total);
           jQuery('#28seats_balitoother_dollar_total').val(_28seats_balitoother_dollar_total);
           jQuery('#35seats_balitoother_rup_total').val(_35seats_balitoother_rup_total);
           jQuery('#35seats_balitoother_dollar_total').val(_35seats_balitoother_dollar_total);
          
          jQuery('#45seats_balitoother_rup_total').val(_45seats_balitoother_rup_total);
           jQuery('#45seats_balitoother_dollar_total').val(_45seats_balitoother_dollar_total);
           jQuery('#6seats_javaisland_rup_total').val(_6seats_javaisland_rup_total);
           jQuery('#6seats_javaisland_dollar_total').val(_6seats_javaisland_dollar_total);
           jQuery('#15seats_javaisland_rup_total').val(_15seats_javaisland_rup_total);
           jQuery('#15seats_javaisland_dollar_total').val(_15seats_javaisland_dollar_total);
          
          jQuery('#19seats_javaisland_rup_total').val(_19seats_javaisland_rup_total);
           jQuery('#19seats_javaisland_dollar_total').val(_19seats_javaisland_dollar_total);
           jQuery('#31seats_javaisland_rup_total').val(_31seats_javaisland_rup_total);
           jQuery('#31seats_javaisland_dollar_total').val(_31seats_javaisland_dollar_total);
           jQuery('#59seats_javaisland_rup_total').val(_59seats_javaisland_rup_total);
           jQuery('#59seats_javaisland_dollar_total').val(_59seats_javaisland_dollar_total);
          
          jQuery('#inpspanTotalRP2').val(spanTotalRP2);
           jQuery('#inpspanTotalRB2').val(spanTotalRB2);
           jQuery('#inpspanTaxAndServiceRP2').val(spanTaxAndServiceRP2);
           jQuery('#inpspanTaxAndServiceRB2').val(spanTaxAndServiceRB2);
           jQuery('#inpspanTotalAndFeeRP2').val(spanTotalAndFeeRP2);
           jQuery('#inpspanTotalAndFeeRb2').val(spanTotalAndFeeRb2);*/
        
   });

}); 
</script>

              
                  </div>  
                  
              </div>
                  
     


                  
          <?php if( trim(get_post_meta($post->ID,'_cs_meetingplace_content',true))!=""){ ?>
                    <p name="meetingplace" id="meetingplace">&nbsp;</p>
                    <h2  class="ico2"><?php echo get_post_meta($post->ID,'_cs_meetingplace_heading',true); ?></h2>
                    <?php echo do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_meetingplace_content',true))); 
            }
          ?>
          
        
          <?php if( trim(get_post_meta($post->ID,'_cs_gps_url',true))!="" || trim(get_post_meta($post->ID,'_cs_gps_map',true))!=""){ ?>
                    <p name="gps" id="gps" >&nbsp;</p>
                    <h2 class="ico3">GPS-map/h2>
                    <?php
                    $sGps = nl2br(get_post_meta($post->ID,'_cs_gps_url',true));
          if($sGps) {
            $img = clsImg($sGps);
            echo '<img width="1100px" src="'.$img.'" alt="" class="img-responsive" />'; 
          }
                    $sGpsMap = nl2br(get_post_meta($post->ID,'_cs_gps_map',true));
          if($sGpsMap) {
            echo do_shortcode($sGpsMap);
          }
          }
          ?>
          
          <?php if( trim(get_post_meta($post->ID,'_cs_price_content',true))!=""){ ?>
                    <p name="Price"  id="Price">&nbsp;</p>
                    <h2 class="ico4"><?php echo nl2br(get_post_meta($post->ID,'_cs_Price_heading',true)); ?></h2>
          <?php 
            
            echo do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_price_content',true)));
          }
          ?>

          <?php/*<?php if( trim(get_post_meta($post->ID,'_cs_hotels',true))!=""){ ?>
                    <div id="tour_hotels">
                    <p name="Hotels"  id="Hotels" >&nbsp;</p>
                    <h2  class="ico5"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_Hotels_heading',true)); ?></h2>
                    <?php 
              echo  do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_hotels',true))); 
          }
          ?>
                    </div>*/?>
          <?php if( trim(get_post_meta($post->ID,'_cs_attractions',true))!=""){ ?>
                    <p name="attractions"  id="attractions" >&nbsp;</p>
                   <h2  class="ico6"><?php echo get_post_meta($post->ID,'_cs_attractions_heading',true); ?></h2>
                    <?php 

            echo  do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_attractions',true))); 
          } 
          ?>
          
          <?php if( trim(get_post_meta($post->ID,'_cs_Precautions_heading',true))!=""){ ?>
                    <p name="Precautions" id="Precautions">&nbsp;</p>
                   <h2  class="ico7"><?php echo get_post_meta($post->ID,'_cs_Precautions_heading',true); ?></h2>
                    <?php 

            echo do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_Precautions',true)));
            }
          ?>
                   
            </div>
        </div>

        <!-- zomato mockup html silky  -->

 
  


    <?php
  $MealCategories = unserialize(get_option('_cs_meal_categories'));
  $url="https://developers.zomato.com/api/v2.1/categories";
  $headers=array(
    'Content-type: application/json',
    'Accept: application/json',
    'user-key: 0138313f73553d4524409f5b600a1ac8'
    );
  $ch=curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
  $data=curl_exec($ch);
  if(!curl_error($ch))
  {
  $data=json_decode($data,TRUE);
    foreach($data['categories'] as &$t)
    {
    $key=md5($t['categories']['name']);
    $t['categories']['img']=$MealCategories[$key];
    }
    $data=json_encode($data);
    echo "<div id='zomatocatdata' style='display: none;'>".$data."</div>";
  }
  curl_close($ch);  
?>
<!--
<div class="container">
  <div class="row">
    <div class="form-group" style="text-align: center;">
        <input style="width: 40%; text-align: center; border: 3px solid #368a8c !important; padding: 5px !important; height: 50px !important; border-radius: 5px; outline: none; color: #9DBFAF !important;" id="usrFoodInput" type="text" class="form-control" name="usrFoodInput"  placeholder="Enter the food you love">
    </div>  
  </div>


  <div id="dataofZomatoCat">
    <div class="row"> </div>
  </div>
</div>
-->

<script>
jQuery( document ).ready(function() {

  $('.searchTerm').change(function(){
  	$(this).attr('style', 'color: #000 !important');
  	//$(this).css({"background-color": "yellow", "font-size": "200%","color":"#000"});
  })	

  // for categories section on zomato
  var zdata = jQuery("#zomatocatdata").html();
  var zomatoCatData = JSON.parse(zdata);

  for(var i of zomatoCatData["categories"]){
    // jQuery("#dataofZomatoCat .row").append("<div class='col-sm-4'><p onclick='getcatdata("+i["categories"]["id"]+")' id='zomatocat"+i["categories"]["id"]+"'>"+ i["categories"]["name"] +"</p> </div>" )

    jQuery("#dataofZomatoCat .row").append('<div class="col-sm-3"> <div  style="height: 150px; width:200px; background-color: #3b898a; margin: 5px auto; cursor: pointer;"  onclick="getcatdata('+i["categories"]["id"]+')"><a href="#" style="display: flex; justify-content: center;" class="btn btn-primary">'+i["categories"]["name"]+'</a><img src="'+i["categories"]["img"]+'" /></div></div>');
  }

  //for search function on zomato section
  jQuery("#usrFoodInput").keyup(function(){
    var searchText = jQuery(this).val();
    $('#loading-image').show();
    jQuery("#dataofZomatoCat").html('');
    jQuery.getJSON('/English/wp-content/themes/bali/api.php', {
        action: 'get_restaurants',
        kw: searchText
      }).done(function(data) {
        $('#loading-image').hide();
        $.each(data.restaurants, function(index, data){
          console.log(data.restaurant);
          var appendData = '<div style="height: 100px; width:700px; background-color: #42f4dc; margin: 15px auto;" class="zomatorestinfo" datainfo="'+data.restaurant.name+'" >';
          appendData += '<div class="col-sm-6">';
          if(data.restaurant.featured_image){
            appendData += '<img style="margin-top:5px; height:90px; width:190px;" src="'+data.restaurant.featured_image+'">';
          }else if(data.restaurant.thumb){
            appendData += '<img style="margin-top:5px; height:90px; width:190px;" src="'+data.restaurant.thumb+'">';
          }else{
            appendData += '<p>No Image Available</p>';
          }
          appendData += '</div>';
          appendData += '<div class="col-sm-6">';
          appendData += '<h5> '+data.restaurant.name+' </h5>';
          appendData += '<p style="background-color: #'+data.restaurant.user_rating.rating_color+'; padding: 4px 20px;">   '+ data.restaurant.user_rating.aggregate_rating +' </p>';
          appendData += '<h5> '+ data.restaurant.cuisines +'</h5>';
          appendData += '</div>';
          appendData += '</div>';
          jQuery("#dataofZomatoCat").append(appendData);    
        });
      });
  }) 

jQuery(document).on('click', '.zomatorestinfo', function () {
  jQuery('.zomatoselectedrest').css('border','none');
  jQuery(this).addClass('zomatoselectedrest').css('border','3px solid green');
   var ii =jQuery(this).attr('datainfo');
  jQuery('#hiddenfieldforzomatorest').val(ii);
});

});

function getcatdata(id){
  var olddata;
  $('#loading-image').show();
  jQuery.getJSON('/English/wp-content/themes/bali/api.php', {
        action: 'getSingleCatZomatoData',
        category: id
      }).done(function(data) {
        $('#loading-image').hide();
          jQuery("#dataofZomatoCat").html('');
          jQuery("#dataofZomatoCat").append('<a onclick="onClickBackZomato()" class="btn btn-primary backbtnzomato"> Back </a>');

          $.each(data.restaurants, function(index, data){
            console.log(data);

            var appendData = '<div style="height: 250px; width:850px; background-color: #42f4dc; margin: 15px auto;">';
            appendData += '<div class="row zomatorestinfo" datainfo="'+data.restaurant.name+','+data.restaurant.average_cost_for_two+'">';
            appendData += '<div class="col-sm-6">';
            if(data.restaurant.featured_image){
            appendData += '<img style="margin-top:16px; height:162px; width:350px;" src="'+data.restaurant.featured_image+'">';
          }else if (data.restaurant.thumb){
            appendData += '<img style="margin-top:16px; height:162px; width:350px;" src="'+data.restaurant.thumb+'">';
          }else{
            appendData += '<p>No Image Available</p>';
          }
            appendData += '</div>';
            appendData += '<div class="col-sm-6" style="margin-top: 35px;">';
            appendData += '<div style="display: inline-flex;width: 100%;">';
            appendData += '<p style="width: 80%;">'+ data.restaurant.name +' </p>';
            appendData += '<p style="background-color: #'+data.restaurant.user_rating.rating_color+'; padding: 4px 20px;">   '+ data.restaurant.user_rating.aggregate_rating +' </p>';
          appendData += '</div>';
            appendData += '<p>  '+ data.restaurant.location.locality_verbose +' </p>';
            appendData += '<p> '+ data.restaurant.cuisines +'</p>';
            appendData += '<p>   '+data.restaurant.currency +' '+   data.restaurant.average_cost_for_two +' for two people (approx) </p>';
            appendData += '</div>';
            appendData += '</div>';
            appendData += '</div>';

            jQuery("#dataofZomatoCat").append(appendData);
        });

      });
}



function onClickBackZomato(){
  var zdata = jQuery("#zomatocatdata").html();
  var zomatoCatData = JSON.parse(zdata);

  jQuery(".backbtnzomato").hide();
  jQuery("#dataofZomatoCat").html('');    

  for(var i of zomatoCatData["categories"]){
    jQuery("#dataofZomatoCat").append('<div class="col-sm-3"> <div  style="height: 150px; width:200px; background-color: #3b898a; margin: 5px auto; cursor: pointer;"  onclick="getcatdata('+i["categories"]["id"]+')"><a href="#" style="display: flex; justify-content: center;" class="btn btn-primary">'+i["categories"]["name"]+'</a><img src="'+i["categories"]["img"]+'" /></div></div>');
  };
  
}
</script>
        
<!-- END zomato mockup html silky -->

<!-- My code silky -->
<script type="text/javascript">
  //recommended place
  jQuery('#category_confirm').click(function(){
    $('#loading-image').show();
    jQuery.getJSON('/English/test-box/wp-content/themes/bali/api.php', {
        action: 'getlocation',
        category: jQuery('#place_categories').val()
      }).done(function(data) {
        $('#loading-image').hide();
        if (data.error_msg == undefined || data.error_msg.length < 1) {
          jQuery('#location_list>option').remove();
          $.each(data.data.places, function(index, place){
            //console.log(place.name+', '+place.name_suffix+'---'+place.thumbnail_url+place.location.lat+place.location.lng);
            jQuery('#location_list').append('<option perex="'+place.perex+'" media_url="'+place.thumbnail_url+'" value="'+place.name+', '+place.name_suffix+'">'+place.name+', '+place.name_suffix+'</option>');
          });
          jQuery('#location_list').change();
        }
      });
  });
  jQuery('#location_list').change(function(){
    media_url=jQuery(this).find('option:selected').attr('media_url');
    perex=jQuery(this).find('option:selected').attr('perex')
    if(media_url.indexOf('http')==-1)
    {
      jQuery('#location_img').attr('src','https://www.travpart.com/English/wp-content/uploads/2018/05/So-much-to-discover.jpg');
    }
    else
    {
      jQuery('#location_img').attr('src', media_url);
    }
    if(perex=="null")
      jQuery('#location_description').text('Description Content');
    else
      jQuery('#location_description').text(perex);
  });
  jQuery('#rcmdplace_confirm').click(function(){
    if(jQuery('#location_list>option:selected').val()!="null")
    {
      jQuery('#place_img').val(jQuery('#place_img').val()+jQuery('#location_img').attr('src')+',');
      jQuery('#place_name').val(jQuery('#place_name').val()+jQuery('#location_list>option:selected').val()+'--');
      jQuery('#place_date').val(jQuery('#place_date').val()+jQuery('#rcmdplace_date_choose').contents().find('#selected_date').text()+',');
      jQuery('#place_time').val(jQuery('#place_time').val()+jQuery('#rcmdplace_date_choose').contents().find('#selected_time').text()+',');
      jQuery('#rcmdplace_alert_message').show();
      setTimeout("$('#rcmdplace_alert_message').hide()", 1800);
    }
  });

  jQuery('.button_zero').click(function(){
    jQuery('tr').toggle(1000);
  });
   jQuery('.button_one').click(function(){
    jQuery('tr').hide();
    jQuery('tr:nth-child(n+5):nth-child(-n+23)').toggle(1000);
   });
   jQuery('.button_two').click(function(){
    jQuery('tr').hide();
    jQuery('tr:nth-child(n+24):nth-child(-n+37)').toggle(1000);
   });
    jQuery('.button_three').click(function(){
      jQuery('tr').hide();
    jQuery('tr:nth-child(n+48):nth-child(-n+93)').toggle(1000);
   });

// *************

jQuery(document).on('click',"#zomatobtn",function(){
  if(jQuery('#rest_name').val()!=""){
    jQuery.getJSON('/English/wp-content/themes/bali/api.php', {
        action: 'get_restaurants',
        kw: jQuery('#rest_name').val()
      }).done(function(data) {
        jQuery.each(data.restaurants , function(index, rest){
          console.log(rest.restaurant.id+" and "+rest.restaurant.name)
          // jQuery("#zomatoRestData ul").append('<li onclick="getIDonClick('+rest.restaurant.id+')" id="restBTN">'+rest.restaurant.name+'</li>');
          jQuery("#zomatoRestData select").append('<option value="'+rest.restaurant.id+'" onclick="getIDonClick('+rest.restaurant.id+')" id="restBTN">'+rest.restaurant.name+'</option>');
        });
      }); 
  }
});


// function getIDonClick(id){  
//  jQuery.getJSON('/test-box/wp-content/themes/bali/api.php', {
//    action: 'get_restaurant_detail',
//    res_id: id
//  }).done(function(data) {
//    jQuery("#restDetails #zomatooverview").html('');
//    jQuery("#restDetails #zomatopics").html('');
//    jQuery("#restDetails #zomatoreviews").html('');
//    if(data){
//      jQuery("#restDetails #zomatooverview").append('<p>Name: '+data.name+' </p> <p>address: '+data.location.address+' </p>');
//      jQuery("#restDetails #zomatopics").append('<img src="'+data.featured_image+'" /> ');
//      jQuery("#restDetails #zomatoreviews").append('<p>Avg Ratings : '+data.user_rating.aggregate_rating+'</p> <p> Ratings text : '+data.user_rating.rating_text+'</p>   ');  
//    }else{
//      jQuery("#restDetails").html('<p>no data</p>')
//    }
//  }); 
// }


$('#selectRest').change(function(){ 
    var id = $(this).val();
    jQuery.getJSON('/English/wp-content/themes/bali/api.php', {
    action: 'get_restaurant_detail',
    res_id: id
  }).done(function(data) {
    jQuery("#restDetails #zomatooverview").html('');
    jQuery("#restDetails #zomatopics").html('');
    jQuery("#restDetails #zomatorating").html('');
    if(data){
      jQuery("#restDetails #zomatooverview").append('<p>Name: '+data.name+' </p> <p>address: '+data.location.address+' </p>');
      if(data.featured_image){
        jQuery("#restDetails #zomatopics").append('<img src="'+data.featured_image+'" /> ');
      }else if(data.photos_url){
        jQuery("#restDetails #zomatopics").append('<p> No preview image else click on <a target="_blank" href="'+data.photos_url+'"> HERE </a> </p>');
      }
      jQuery("#restDetails #zomatorating").append('<p>Avg Ratings : '+data.user_rating.aggregate_rating+'</p> <p> Ratings text : '+data.user_rating.rating_text+'</p>   '); 
    }else{
      jQuery("#restDetails").html('<p>no data</p>')
    }
  }); 

  jQuery.getJSON('/English/wp-content/themes/bali/api.php', {
    action: 'get_restaurant_review',
    res_id: id
  }).done(function(data) {
    
    jQuery("#restDetails #zomatoreviews").html('');
    if(data){
      jQuery.each(data.user_reviews, function(index, i){
        jQuery("#restDetails #zomatoreviews").append('<p>review rating : '+i.review.rating+'</p> <p> Ratings text : '+i.review.rating_text+'</p>  <p>user : '+i.review.user.name+'</p>');   
      })
      
    }else{
      jQuery("#restDetails").html('<p>no data</p>')
    }
  }); 
});


</script>
<!--END my code silky -->

        <?php 
        // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
      comments_template();
    endif;
        ?>
           <?php    
$hotel_price=$_COOKIE['hotel_price'];

$lta_price=$_SESSION['ltaprice'];
  ?>

<!--   <div>
  <ul id="right_menu">  
        <div><a href="#sectionone"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/cutlery.png" alt="????" width="50"><br>????</a></div>
        <div><a href="#sectiontwo">????</a></div>
        <div><a href="#sectionthree">popup</a></div>
        <div><a href="#sectionfour">??</a></div>
  </ul>
  </div> -->
<div id="menuresize">
    <div id="menuresize-header" class="menuresize-header">
        <i class="fa fa-shopping-cart"></i>
        <i class="fa fa-caret-up"></i>
    </div>
</div>
  <div id="left_menu" style="width:130px;"> 
      <ul id="left_menu_content">  
        <?php                    
          if(is_user_logged_in()){ 
                ?>
                <li class="total"><div>&nbsp;</div> <a href="http://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=pages"> <i class="fa fa-user"></i><br>
                <span><?php echo $display_name; ?></span>
        <?php if(!empty($lta_price) && $lta_price>0) { ?>
        <span class="original" or_pic="<?php echo $lta_price; ?>"><span class="price_span"><b>Total：</b></span></span>
        <?php } ?>
        </a>
                </li>
                <li>
                    <?php
                    $my_balance = $wpdb->get_row("SELECT balance FROM `balance` WHERE `userid`={$urow->userid}")->balance;
                    $my_balance /= get_option("_cs_currency_USD");
                    ?>
                    <a href="<?php echo site_url().'/user/'.$current_user->user_login.'/?profiletab=settlement'; ?>"><i class="fas fa-wallet"></i> <br><b>My commission balance</b></a>
                    <span class="change_price original" or_pic="<?php echo round($my_balance); ?>">Rp <span class="price_span"><?php echo round($my_balance); ?></span></span>
                </li>
        <?php }else{ ?>
        <li onclick="callsignin()"><i class="fa fa-user"></i> <br /><div id="button_onescroll"><span>Login</span></div> </li>
        <?php  }  ?>
        
        <?php if(!empty($tour_id)) { ?>
        <a href="/English/tour-details/?bookingcode=BALI<?php echo $tour_id; ?>"><li><i class="fa fa-shopping-cart"></i>   <div id="button_threescroll"><span>Journey</span></div></li></a>
        <?php } ?>
        <li> <a href="#sectionzero"> <i class="fa fa-hotel"></i>  <br><b>Hotel</b>
                <?php  if(is_user_logged_in()){ ?>
                <span class="change_price original" or_pic="<?php echo round($hotel_price/get_option("_cs_currency_USD")); $totalcost+=round($hotel_price/get_option("_cs_currency_USD")); ?>"> Rp <span class="price_span"><?php echo number_format(round($hotel_price/get_option("_cs_currency_RMD")),2);?></span>
                </span>
                <?php }  ?>
                </a></li>      

        <a href="#sectionone" class="mealsection" style="text-decoration:none;">
        <li><i class="fas fa-utensils"></i> <br><b>Meal</b>
            <?php  if(is_user_logged_in()){ ?>
            <span class="change_price original" or_pic="<?php echo intval($_COOKIE['meal_price']); $totalcost+=intval($_COOKIE['meal_price']); ?>">Rp <span class="price_span"><?php echo number_format($_COOKIE['meal_price'],2);?></span></span>
            <?php }  ?>
        </li></a>       

        <a href="#sectiontwo" class="carsection" style="text-decoration:none;">
        <li> <i class="fa fa-truck"></i> <br><b>Vehicle</b>
            <?php  if(is_user_logged_in()){ ?>
            <span class="change_price original" or_pic="<?php echo $car_price; $totalcost+=$car_price; ?>">Rp <span class="price_span"><?php echo number_format($car_price,2);?></span></span>
            <?php }  ?>
        </li></a>

        <a href="#sectionthree" class="guidesection" style="text-decoration:none;">
        <li><i class="fa fa-child"></i> <br><b>Tour Guide</b>
            <?php  if(is_user_logged_in()){ ?>
            <span class="change_price original" or_pic="<?php echo intval($_COOKIE['guide_price']); $totalcost+=intval($_COOKIE['guide_price']); ?>">Rp <span class="price_span"><?php echo number_format($_COOKIE['guide_price'],2);?></span></span>
            <?php }  ?>
        </li></a>
        
        <a href="#sectionfour" class="rcmdplacesection" style="text-decoration:none;">
        <li><i class="fa fa-map"></i> <br><b>Recommended Places</b>
            <?php  if(is_user_logged_in()){ ?>
            <span class="change_price original" or_pic="<?php echo intval($_COOKIE['place_price']); $totalcost+=intval($_COOKIE['place_price']); ?>">Rp <span class="price_span"><?php echo number_format($_COOKIE['place_price'],2);?></span></span>
            <?php }  ?>
        </li></a>
        
        <a href="#sectionfive" class="spasection" style="text-decoration:none;">
        <li><i class="fa fa-fire"></i> <br><b>SPA</b>
            <?php  if(is_user_logged_in()){ ?>
            <span class="change_price original" or_pic="<?php echo intval($_COOKIE['spa_price']); $totalcost+=intval($_COOKIE['spa_price']); ?>">Rp <span class="price_span"><?php echo number_format($_COOKIE['spa_price'],2);?></span></span>
            <?php }  ?>
        </li></a>
        
        <a href="#sectionsix" class="boatsection" style="text-decoration:none;">
        <li><i class="fa fa-ship"></i> <br><b>Cruise ship and Helicopter</b>
            <?php  if(is_user_logged_in()){ ?>
            <span class="change_price original" or_pic="<?php echo intval($_COOKIE['boat_price']); $totalcost+=intval($_COOKIE['boat_price']); ?>">Rp <span class="price_span"><?php echo number_format($_COOKIE['boat_price'],2);?></span></span>
            <?php }  ?>
        </li></a>
        
        <a href="#sectionseven" class="activitysection" style="text-decoration:none;">
        <li><i class="fa fa-tint"></i> <br><b>Optional Activity</b>
            <?php  if(is_user_logged_in()){ ?>
            <span class="change_price original" or_pic="<?php echo intval($_COOKIE['activity_price']); $totalcost+=intval($_COOKIE['activity_price']); ?>">Rp <span class="price_span"><?php echo number_format($_COOKIE['activity_price'],2);?></span></span>
            <?php }  ?>
        </li></a>
        
        <a href="#sectioneight" class="optionallocation" style="text-decoration:none;">
        <li><i class="fa fa-map-marker fa-fw"></i> <br><b>Optional Location</b><br>
            <span class="location_number"><?php echo (intval($_COOKIE['location_number'])>0)?intval($_COOKIE['location_number']):0; ?></span> Location
        </li></a>
        
        <a href="#sectionnine" class="manualitemsection" style="text-decoration:none;">
        <li><i class="fa fa-tint"></i> <br><b>Manual Item</b>
            <?php  if(is_user_logged_in()){ ?>
            <span class="change_price original" or_pic="<?php echo intval($manualitem_total_price); ?>">Rp <span class="price_span"><?php echo number_format($manualitem_total_price,2);?></span></span>
            <?php }  ?>
        </li></a>

          <li class="total-cost-li">
              <div class="total-cost">
                  <strong>Total Cost:</strong>
                  <span class="change_price original" or_pic="<?php echo $totalcost; ?>">Rp <span class="price_span"><?php echo $totalcost; ?></span></span>
              </div>
          </li>
    
                
      </ul>
  </div>
  
  <div class="total-tour-submit-wrapper">
    <div class="total-tour-submit-total total-cost">Total Cost:<span class="change_price original" or_pic="<?php echo $totalcost; ?>">Rp <span class="price_span"><?php echo $totalcost; ?></span></span></div>
    <div class="btn-wrap"><a href="<?php echo empty($tour_id)?'#':(site_url().'/tour-details/?bookingcode=BALI'.$tour_id); ?>" class="<?php echo $totalcost>0?'':'disabled ';?> submit-tour-btn btn bg-org-0">Submit tour package</a> </div>
  </div>

	<div class="scroll_top">
		<button class="btn btn-default">
			<i class="fas fa-arrow-up"></i>
		</button>
	</div>
	
<div>
	
</div>


<script>
    var checkTotalCostValue=setInterval(function() { if(parseInt($('.total-cost .change_price').attr('or_pic'))>0) { $('.submit-tour-btn').removeClass('disabled'); clearInterval(checkTotalCostValue); } }, 1500);
    function mylogin()
            {
         $('.iconsquares').click(function(){      
       
       $('.hover_bkgr_fricc').show();
   });
    $('.hover_bkgr_fricc').click(function(){
        //$('.hover_bkgr_fricc').hide();
    });
    $('.popupCloseButton').click(function(){
        $('.hover_bkgr_fricc').hide();
    });
              
            }
function callsignin()
{
    console.log('working');
  $('.hover_bkgr_fricc').show();
  $('.hover_bkgr_fricc').click(function(){
    
  });
  /*$('.popupCloseButton').click(function(){
    $('.hover_bkgr_fricc').hide();
  });*/
}  
</script>
<?php
  }
} else {
  get_template_part('tpl-pg404', 'pg404');
}
get_footer();
        ?>
<script>
function resizeOpen() {
    $('#left_menu').show(1000);
    $('#closebutton').show(1000);
    $('#menuresize').width(130);
}

function resizeClose() {
    $('#left_menu').hide(1000);
    $('#closebutton').hide(1000);
    $('#menuresize').width(30);
}
/*
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})*/

</script>

  <script type="text/javascript">
     var $j = jQuery;
      function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
          x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
      }

      /*function calTotalRRRPPP() {
        var totalRp = 0;
        var totalRb = 0;
        $j("#tablepress-109").find("input[type=number]").each(function () {
          var rp = parseInt($j(this).val()) * parseInt($j(this).attr("data-rp"));
              var rb = parseInt($j(this).val()) * parseInt($j(this).attr("data-rb"));
                
                  $j(this).parents("td:first")
           .next().html("Rp" + addCommas(rp.toString()))
             .next().next().html("¥" + addCommas(rb.toString()));
              
              $j(this).parents("td").siblings("input[type=hidden]").val("Rp" + addCommas(rp.toString()))
                 .next().next().val("¥" + addCommas(rb.toString())); 
                     
          totalRp += rp;
          totalRb += rb;
               
        });
        $j("#spanTotalRP2").html("Rp" + addCommas(totalRp.toString()));
        $j("#spanTotalRB2").html("¥" + addCommas(totalRb.toString()));
            $j("#inpspanTotalRP2").val("Rp" + addCommas(totalRp.toString()));
        $j("#inpspanTotalRB2").val("¥" + addCommas(totalRb.toString()));

        var taxAndServiceRp = parseInt(totalRp * 0.1);
        var taxAndServiceRb = parseInt(totalRb * 0.1);
        $j("#spanTaxAndServiceRP2").html("Rp" + addCommas(taxAndServiceRp.toString()));
        $j("#spanTaxAndServiceRB2").html("¥" + addCommas(taxAndServiceRb.toString()));
            $j("#inpspanTaxAndServiceRP2").val("Rp" + addCommas(taxAndServiceRp.toString()));
        $j("#inpspanTaxAndServiceRB2").val("¥" + addCommas(taxAndServiceRb.toString()));
          
        var totalandfeerp = totalRp + taxAndServiceRp;
        var totalandfeerb = totalRb + taxAndServiceRb;
        $j("#spanTotalAndFeeRP2").html("Rp" + addCommas(totalandfeerp.toString()));
        $j("#spanTotalAndFeeRb2").html("¥" + addCommas(totalandfeerb.toString()));
            $j("#inpspanTotalAndFeeRP2").val("Rp" + addCommas(totalandfeerp.toString()));
        $j("#inpspanTotalAndFeeRb2").val("¥" + addCommas(totalandfeerb.toString()));
        saveformdata();
      }
     

      $j(document).ready(function () {
        $j("#tablepress-109").find("input[type=number]").each(function() {
          $j(this).val("0");
          $j(this).attr("min", "0");
          var datarp = $j(this).parents("td:first").prev().prev().prev().html().replace("Rp", "").replace(/,/g, "");
          var datarb = $j(this).parents("td:first").prev().html().replace("$", "").replace(/,/g, "");
          $j(this).attr("data-rp", datarp).attr("data-rb", datarb);
        });

        calTotalRRRPPP();

        $j("#tablepress-109").find("input[type=number]").change(function () {
          calTotalRRRPPP();
        });

        if( $j('.hotel_table .room_type').length == 0 ) {
            $j('.choose_the_hotel').remove();
            }

      });*/

  </script>
  
  