<?php
set_time_limit(0);
require_once('../../../wp-config.php');

extract($_POST);

global $wpdb; 

$sql = "SELECT * FROM wp_tour where id=".$product_id;

$result = $wpdb->get_results($sql) ;

$meta = get_post_meta( $result[0]->tour_id);

$post = get_post( $result[0]->tour_id ); 


$wpdb->query("UPDATE `wp_tour` SET `first_name` = '".$payer_fname."', 
`last_name` = '".$payer_lname."', 
 `addrs` = '".$payer_address."',  
 `zip` = '".$payer_zip."',  
 `city` = '".$payer_city."',  
 `state` = '".$payer_state."', 
 `country` ='".$payer_country."',  
 `email` = '".$payer_email."', 
 `confirm_payment` = '1' 
 WHERE `wp_tour`.`id` = '".$product_id."'");
 
//set coupon
$wpdb->query("UPDATE `wp_coupons` SET used=1 WHERE tour_id={$product_id}");

$tour_creator_id=$wpdb->get_var("SELECT meta_value FROM `wp_tourmeta` where meta_key='userid' and  tour_id=".intval($product_id));
if(!empty($tour_creator_id) && intval($tour_creator_id)>0) {
	$wpdb->insert('notification', array(
		'wp_user_id'=>$tour_creator_id,
		'content'=>'Congratulation! Some one just bought your booking code'),
		array('%d','%s'));
}

$usd_currency = get_currency('usd');
$usd_currency = empty($usd_currency)?1:$usd_currency;

$tour_link = home_url('/tour-details/?bookingcode=BALI'.$result[0]->tour_id);
$total_usd = number_format($result[0]->total*$usd_currency['rate'],2);
$email_content = <<<EOD
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tbody>
		<tr>
			<td align="center" valign="top" style="padding:20px 0 20px 0">
				<table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650"
					style="border:1px solid #e0e0e0">
					<tbody>
						<tr>
							<td valign="top">
								<a href="https://www.travpart.com/" rel="noreferrer" target="_blank"><img
										src="https://www.travpart.com/English/wp-content/uploads/2020/02/travpart-logo.png"
										alt="" style="margin-bottom:10px" border="0" class="CToWUd"></a>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Tour
									reservation confirm</h1>
							</td>
						</tr>
						<tr>
							<td>
								<h2 style="font-size:18px;font-weight:normal;margin:0"></h2>
							</td>
						</tr>
						<tr>
							<td>Thank you for ordering from us. We will process your reservation until we received your
								payment. Please read your reservation detail and if you chose to make the payment beside
								using your credit card, please read the payment guideline below : </td>
						</tr>
						<tr>
							<td>
								<table cellspacing="0" cellpadding="0" border="0" width="650">
									<thead>
										<tr>
											<th align="left" width="325" bgcolor="#EAEAEA"
												style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Customer
												Information :</th>
											<th width="10"></th>
											<th align="left" width="325" bgcolor="#EAEAEA"
												style="font-size:13px;padding:5px9px 6px 9px;line-height:1em">Tour
												Information:</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td valign="top"
												style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
												First Name : {$payer_fname}
												<br>Last Name : {$payer_lname}
												<br>Address: {$payer_address}
												<br>City : {$payer_city}
												<br>State : {$payer_state}
												<br>Zip : {$payer_zip}
												<br>Country : {$payer_country}
												<br>Email : <a href="mailto:{$payer_email}" rel="noreferrer"
													target="_blank">{$payer_email}</a>
												<br>Payment Type :
												<br>
											</td>
											<td>&nbsp;</td>
											<td valign="top"
												style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
												Tour Name: Travpart - be part of us | travcust
												<br>Tour Link: <a href="{$tour_link}">{$tour_link}</a>
												<br>Start Date : {$result[0]->start_date}
												<br>End Date : {$result[0]->end_date}
												<br>Total: ${$total_usd}
												<br>
											</td>
										</tr>
									</tbody>
								</table>
								<div>
									<h2 style="text-align:center"> Payment Guideline (for non-credit cards) </h2><b>
										Bank Wire Transfer </b>
									<br>Beneficiary Bank : （CIMB Niaga）
									<br>Beneficiary Account Number : 800135524000 (Non-USD Account)
									<br>800135713740 (USD Account)SwiftCode : BNIAIDJA
									<br>Bank Address : Jl. Prof. Dr. Supomo No. 15 A, Tebet, Daerah Khusus Ibukota
									Jakarta 12810, Indonesia
									<br>Company Name : PT Tur Indo Bali
									<br>Company Address : Wisma 46, Jl. Jend. Sudirman, Karet Tengsin, Tanah Abang,
									Central Jakarta City, Jakarta 10250, Indonesia
									<br>
									<br>
									<hr>
									<br><b> Check Deposit </b>
									<br>Pay to : PT Tur Indo Bali
									<br>Mail to : PT Tur Indo Bali
									<br>Address : Wisma 46, Jl. Jend. Sudirman, Karet Tengsin, Tanah Abang, Central
									Jakarta City, Jakarta 10250, Indonesia
									<br>
									<br>Deposited into: CIMB Niaga
									<br>
									<br>Check must be received into our office or deposited into our bank account 15
									days prior to tour departure date. If payment is not received 15 days prior to tour
									departure date, an alternative payment method will be requested from the customer.
									Mailed Check will be returned to the sender.
									<br>
									<br>We recommend that you mail or send your check into our office at least 25-30
									days prior to your tour departure date. We do not accept International Check.
									<br>
									<br>We will not process a reservation until we receive payment and the payment
									clears in our system.
									<br>
									<br>
									<hr>
									<br>
									<h2 style="text-align:center"> Confirmation Page</h2>Documents
									<br>There are some paperwork that you will need to arrange and bring before your
									trip. Therefore, don’t forget to bring the following paperwork:
									<br>Your Passport: If necessary
									<br>Visa: Please check if your nationality is necessary to have visa coming to
									Indonesia or not
									<br>Limitted Stay Visa (KITAS) : If necessary
									<br>Insurance Documents: You are advised to register for insurance before your
									departure
									<br>Enough Cash / Money in Indonesian Rupiah
									<br>
									<br>Luggage
									<br>- No broken Luggage
									<br>- Light Luggage
									<br>Most of our tour packages requires you to carry your luggage for hotel transfers
									every different cities. So don’t bring broken luggage. You will have people help you
									lift your luggage to every vehicles
									<br>Clothing
									<br>
									<br>Most part of Indonesia is hot so you will need summer clothing. But, if you are
									taking one of the Bromo tour package you will need some cold weather clothing and
									boots because you will be going through a dessert. If you took our beach packages
									you will need some swimming cloths.
									<br>
									<br>Others
									<br>Don’t forget to bring other things that are necessary such as cameras, phones,
									and shade. You can always send your picture to our company and we will post it in
									our website.
									<br>
									<br>Suggestion
									<br>We advise you to do deep research on where you are going either through our
									website or somewhere else. So you could prepare what you will do once you arrive.
									Check culinary spots and scenic spots you will be going to.
									<br><b>Don’t forget we will always be here to help you, so feel free to contact us
										at : <a href="mailto:customerservice@tourfrombali.com" rel="noreferrer"
											target="_blank">customerservice@tourfrombali.<wbr>com</a></b>
									<br>Sincerely
									<br>Customer Service
									<br>Tour from bali
									<br>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
EOD;


$headers  = 'From: contact@tourfrombali.com' . "\r\n" ;
$headers .= 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

$admin_email=get_option( 'admin_email' );
wp_mail($payer_email,"E-Ticket ".$result[0]->code,$email_content);
wp_mail($admin_email,"E-Ticket ".$result[0]->code,$email_content);
echo 'success';

/*
//send itinerary list email`
$url=home_url()."/wp-content/themes/bali/api.php?action=getitineray&tour_id=".$product_id;
$ch=curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
$data=curl_exec($ch);
$data.=$body;
if(!curl_error($ch))
{
	wp_mail($payer_email,"E-Ticket ".$result[0]->code,$data,$headers);
	wp_mail($admin_email,"E-Ticket ".$result[0]->code,$data,$headers);
}
curl_close($ch);
*/




// Send mail to the vendors for bidding 
// $query = "SELECT option_value FROM travpart.wp_options WHERE option_name = '_cs_travpart_vendorlist' ";
// global $wpdb;
// $resultsData = $wpdb->get_results($query);
// 		if(!empty($resultsData)){
// 			foreach ( $resultsData as $data ) {
// 				$arr = unserialize(unserialize($data->option_value)); 
// 					foreach ($arr as $res) {
// 						$vendor_Email = $res['email'];

// 						$htmlContentpath = get_theme_file_path("emailtemplateforvendor.html");
// 						$htmlContent = file_get_contents($htmlContentpath);
						
// 						sendmail($vendor_Email, 'Bidding Mail', $htmlContent);

// 					}
// 			}
// 		}


// function sendmail($to, $subject, $message){
// 	$key="Tt3At58P6ZoYJ0qhLvqdYyx21";
// 	$postdata=array('to'=>$to,'subject'=>$subject,'message'=>$message);
// 	$url="https://www.tourfrombali.com/api.php?action=sendmail&key={$key}";
// 	$ch=curl_init();
// 	curl_setopt($ch, CURLOPT_URL, $url);
// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// 	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
// 	curl_setopt($ch, CURLOPT_POST, 1);
// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
// 	$data=curl_exec($ch);
// 	if(curl_errno($ch) || $data==FALSE){
// 		curl_close($ch);
// 		return FALSE;
// 	}else{
// 		curl_close($ch);
// 		return TRUE;
// 	}
// }

//end Send mail to the vendors for bidding 


?>
