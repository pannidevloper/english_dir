<?php
  $lang['number']		= '参考号';
  $lang['date'] 		= '付款日期';
  $lang['time']			= '付款时间';
  $lang['due'] 			= '截至日期';
  $lang['to'] 			= '付款方';
  $lang['from'] 		= '收款方';
  $lang['product'] 		= '产品';
  $lang['qty'] 			= '数量';
  $lang['price'] 		= '价格';
  $lang['discount'] 	= '折扣';
  $lang['vat'] 			= '税';
  $lang['total'] 		= '总计';
  $lang['page'] 		= '页';
  $lang['page_of'] 		= '/';
?>