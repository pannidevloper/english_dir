<!-- Inline Css For vouchers -->
<style>
  #content h1{
       display: none;
    }
    @media (max-width: 600px){
        div#content {
            margin-top: 60px;
        }
        
        body{
            overflow-x: hidden;
        }
      }
	.voucher-area-left{

	}	

	.voucher-area h3{
	    text-align: center;
	    font-size: 24px;
	    color: #444444;
	    font-weight: 600;
	    margin-bottom: 30px;
	    margin-top: 12px;
	}
	h3 span.txt-point{
		color: #ff0000;
	}

	.voucher-area-right{
    height: 280px;
    overflow-y: scroll;
    border-radius: 5px;
    border: solid 1px #66cccc;
    padding: 20px;

	}	

	.voucher-area-right h3{
    font-size: 18px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.67;
    letter-spacing: -0.18px;
    text-align: left;
    color: #1cba9a;
    margin-top: 0;
    padding-bottom: 8px;
    margin-bottom: 8px;
    box-shadow: 0px 2px 0 0 rgba(93, 202, 197, 0.31);
	}
  .voucher-area-right p {
    box-shadow: 0px 2px 0 0 rgba(93, 202, 197, 0.31);
    background-color: #ffffff;
    padding-bottom: 8px;
    margin-bottom: 8px;
  }
  .voucher-area-right p span {
    font-weight: bold;
  }

	a.spend-point-a{
		display: block;
    border-radius: 5px;
    box-shadow: 0px 2px 0 0 rgba(93, 202, 197, 0.31);
    background-image: linear-gradient(to right, #66cccc, #1bba9a);
    padding: 10px 20px;
    width: 100%;
    text-align: center;
    font-size: 18px;
    font-weight: bold;
    color: #fff!important;
	}
  a.spend-point-a-org {
    box-shadow: 0px 2px 0 0 rgba(93, 202, 197, 0.31);
    background: #ff6633;
  }
	a.discount-right-txt{
		color: #333;
	}
  .p-4 {
    background: #fff;
  }
  .vs-box {
    border-radius: 5px;
    border: solid 1px #66cccc;
    background-color: #eff9f9;
    padding: 20px;
    margin-bottom: 8px;
  }
  .vs-title {
    font-size: 20px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: -0.14px;
    text-align: center;
    color: #1bba9a;
    text-align: center;
  }
  .vs-title .title-small {
    font-size: 14px;
    margin-bottom: 10px;
  }
  a.btn-redeem {
    display: inline-block;
    border-radius: 5px;
    box-shadow: 0px 2px 0 0 rgba(93, 202, 197, 0.31);
    background-image: linear-gradient(to right, #66cccc, #1bba9a);
    padding: 10px 20px;
    text-align: center;
    font-size: 18px;
    font-weight: bold;
    color: #fff!important;
    margin-top: 15px;
  }
  .um-profile .um-profile-body {
    padding-bottom: 0;
  }


	/* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
  
}

/*
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {


}

/*
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {


}

/*
  ##Device = Tablets, Ipads (landscape)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {


}

/*
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {


}

/*
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {

}
</style>
<!-- End Inline Css For vouchers -->
<div class="p-4 voucher-area clearfix">
  <h3>You have <span class="txt-point"><?php echo intval(get_user_meta(wp_get_current_user()->ID, 'game_points', true)); ?></span> points currently for play</h3>
  <div class="row">
    <div class="col-md-5">
      <!-- Start Vouchers Area Left-->
      <div class="voucher-area-left">

          <?php
          $posts=get_posts(array('post_type'=>'voucher_scheme'));
          $tp_cpn_valdty=intval(get_option('_travpart_vo_option')['tp_cpn_valdty']);
          foreach($posts as $row) {
              $vsicon_args=get_post_meta($row->ID, '_tp_vsicon_args', true);
              $availabilityCount=0;
              foreach($vsicon_args['icon'] as $x) {
                  $availabilityCount+=$x['availability'];
              }
              if($vsicon_args['status']==1 && strtotime($vsicon_args['start_date'])<=time()
                && strtotime($vsicon_args['end_date'])>=time() && $availabilityCount>0 ) {
                  ?>
                <div class="vs-box">
                  <div class="vs-title">
                    <?php $title_array = explode('(',$row->post_title); ?>
                    <div class="title-large"><?php echo $title_array[0]; ?></div>
                    <div class="title-small">(<?php echo $title_array[1]; ?></div>
                  </div>
                  <div class="vs-content">
                    <a class="spend-point-a <?php if($vsicon_args['pts_to_play']>10){ echo 'spend-point-a-org';}?>" href="<?php echo home_url().'/game/?type='.$row->ID; ?>">
                      Spend <?php echo $vsicon_args['pts_to_play']; ?> points to play
                    </a>
                  </div>
                </div>
                  <?php
              }
          }
          ?>
      </div>
      <!-- end voucher area -->
    </div>
    <div class="col-md-7">
      <!-- Start Vouchers Area Right-->
      <div class="voucher-area-right">
        <h3>List of your Coupons</h3>
          <?php foreach($couponList as $row) {
              if($row->discount=='$')
                  $discount=$row->discount.$row->figure;
              else
                  $discount=$row->figure.$row->discount;
              ?>
            <p><span class="discount-right-txt"><?php echo $discount.' '.ucfirst($row->type); ?> Discount</span> - Valid until <?php echo date('jS M Y', strtotime($row->get_time)+$tp_cpn_valdty*86400); ?></p>
          <?php } ?>

        <div class="btn-area">
          <a class="btn-redeem" href="https://www.travpart.com/English/toursearch/" target="_blank">Redeem Coupon and Search tour Package</a>
        </div>

      </div>
      <!-- End Vouchers Area Right -->
    </div>
  </div>
</div>
