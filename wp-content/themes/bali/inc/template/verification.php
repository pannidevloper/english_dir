<!-- Verified page-->
<style>
    .verification-page {
        font-size: 16px; }
    .verification-page h3 {
        font-size: 36px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: -0.4px;
        text-align: left;
        color: #333333;
        margin-top: 0;
        margin-bottom: 0.3em; }
    .verification-page p {
        font-size: 16px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.5;
        letter-spacing: normal;
        text-align: left;
        color: #42454e;
        margin-bottom: 1em; }
    .verification-page .form-wrap {
        border-radius: 2px;
        box-shadow: 0px 1px 0 0 rgba(93, 202, 197, 0.31);
        background-color: #ffffff;
        padding: 25px;
        border: 1px solid #dcdcdc; }
    .verification-page .message-danger {
        border-radius: 3px;
        background-color: #ffffff;
        padding: 25px;
        border: 2px solid #ff0000;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin-bottom: 40px; }
    .verification-page .message-danger .ico {
        font-size: 30px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 0.8;
        letter-spacing: normal;
        text-align: left;
        color: #ff0000;
        margin-right: 30px; }
    .verification-page label {
        color: #666666; }
    .verification-page label strong {
        display: block; }
    .verification-page label span {
        font-weight: normal; }
    .verification-page .col-sm-3 label {
        padding-top: 5px; }
    .verification-page .form-row {
        margin-bottom: 15px; }
    .verification-page .inpu-inline-row {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        margin-left: -5px !important;
        margin-right: -5px !important; }
    .verification-page .inpu-inline-row > input[type=text] {
        margin: 0 5px !important; }
    .verification-page .btn-verif {
        border-radius: 5px !important;
        box-shadow: 0px 2px 0 0 rgba(93, 202, 197, 0.31) !important;
        background-color: transparent !important;
        background-image: -webkit-linear-gradient(left, #66cccc, #1bba9a) !important;
        background-image: linear-gradient(to right, #66cccc, #1bba9a) !important;
        height: 50px !important;
        color: #ffffff !important;
        font-size: 18px !important;
        padding-left: 25px !important;
        padding-right: 25px !important; }
    .verification-page .btn-verif:hover, .verification-page .btn-verif:active, .verification-page .btn-verif:focus {
        color: #fff !important;
        background-color: transparent !important;
        background-image: -webkit-linear-gradient(left, #66cccc, #1bba9a) !important;
        background-image: linear-gradient(to right, #66cccc, #1bba9a) !important;
        box-shadow: 2px 2px 4px 0px rgba(93, 202, 197, 0.5) !important; }

    .tips {
        position: relative;
        z-index: 9;
        border-radius: 3px;
        background-color: #deeff0;
        font-size: 14px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.29;
        letter-spacing: normal;
        text-align: left;
        color: #42454e;
        box-shadow: 0 0 2px 0 #78cdd2;
        margin-bottom: 15px; }
    .tips p {
        font-size: 14px; }
    .tips .tips-inner {
        padding: 20px; }
    .tips .tips-arrow {
        border-color: #deeff0;
        border-style: solid;
        position: absolute;
        z-index: 2; }
    .tips .tips-arrow {
        border-width: 15px 15px 15px 0px;
        border-left-color: transparent;
        border-top-color: transparent;
        border-bottom-color: transparent;
        left: -15px;
        top: 30px;
        margin-left: 0;
        margin-right: 0; }
    .tips .fa-lightbulb {
        position: relative;
        font-size: 30px;
        color: #fff;
        margin-right: 15px;
        -ms-flex-item-align: center;
        -ms-grid-row-align: center;
        align-self: center;
        margin-bottom: 16px; }
    .tips .fa-lightbulb:before {
        display: block;
        width: 50px;
        height: 50px;
        line-height: 50px;
        background-color: #78cdd2;
        border-radius: 25px;
        text-align: center; }
    select.form-control:not([size]):not([multiple]) {
        height: calc(3.8rem + 2px);
    }
    input[type=datetime-local]{
        height: 40px;
    }

</style>
<style>
    .um-profile-body {
        border: none!important;
    }
    #content h1{
       display: none;
    }
    @media (max-width: 600px){
        div#content {
            margin-top: 60px;
        }
        
        body{
            overflow-x: hidden;
        }
    }

</style>
<div class="verification-page pt-4 pb-4">
    <div class="row">
        <div class="col-sm-8">
            <h3>Now, Please upload your ID</h3>
            <p>This is the final step. Your confidential information is protected by our <a href="https://www.travpart.com/English/privacy-policy-2/">privacy policy</a> .
                 </p>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <div class="form-wrap">
				<?php /*
                <div class="message-danger">
                    <div class="ico">!</div>
                    <div>Your request could not be processed. please contact <a href="#">support</a> .</div>
                </div>
				*/ ?>
				
				<?php
				$verification_status=get_user_meta($current_user->ID, 'verification_status', true);
				if($verification_status==1) { ?>
                <div class="message-danger" style="border-color: green;">
                    <div>Your have uploaded your ID information. Please wait for the administrator to review.</div>
                </div>
				<?php } else if($verification_status==2) { ?>
				<div class="message-danger" style="border-color: green;">
                    <div>
						Your identifications has been reviewed and accepted.<br>
						Thanks for helping us maintain a trusted workplace for all.<br>
						You can now get paid for your commissions.
					</div>
                </div>
				<?php } ?>
	
				<?php if(count($verification_error_msg)>0) { ?>
				<div class="message-danger">
                    <div>
						<?php foreach($verification_error_msg as $error_msg) { ?>
						<p style="color:red;"> <?php echo $error_msg; ?> </p>
						<?php } ?>
					</div>
                </div>
				<?php } ?>
				<?php if($verification_status!=2) { ?>
                <form action="<?php echo home_url("/user/{$username}/?profiletab=verification"); ?>" method="POST" enctype="multipart/form-data">
                    <div class="row form-row">
                        <div class="col-sm-3">
                            <label><strong>ID Type</strong><span>(optional)</span></label>
                        </div>
                        <div class="col-sm-9">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="IDType" value="passport" checked>
                                    Passport
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="IDType" value="national_id_card">
                                    National ID Card
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="IDType" value="driver_license">
                                    Driver’s licence
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="IDType" value="other">
                                    Other
                                </label>
                            </div>

                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-sm-3">
                            <label><strong>Scan of ID</strong></label>
                        </div>
                        <div class="col-sm-9">
                            <input type="file" name="ScanofID" />
                            <p class="desc">25MB maximum filesize</p>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-sm-3">
                            <label><strong>ID Issuing Country</strong></label>
                        </div>
                        <div class="col-sm-9">
                            <select class="form-control" name="IDIssuingCountry">
                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Åland Islands">Åland Islands</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                    <option value="Andorra">Andorra</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antarctica">Antarctica</option>
                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia, Plurinational State of">Bolivia, Plurinational State of</option>
                                    <option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
                                    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Bouvet Island">Bouvet Island</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">Burkina Faso</option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Cape Verde">Cape Verde</option>
                                    <option value="Cayman Islands">Cayman Islands</option>
                                    <option value="Central African Republic">Central African Republic</option>
                                    <option value="Chad">Chad</option>
                                    <option value="Chile">Chile</option>
                                    <option value="China">China</option>
                                    <option value="Christmas Island">Christmas Island</option>
                                    <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Comoros">Comoros</option>
                                    <option value="Congo">Congo</option>
                                    <option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
                                    <option value="Cook Islands">Cook Islands</option>
                                    <option value="Costa Rica">Costa Rica</option>
                                    <option value="Côte d'Ivoire">Côte d'Ivoire</option>
                                    <option value="Croatia">Croatia</option>
                                    <option value="Cuba">Cuba</option>
                                    <option value="Curaçao">Curaçao</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">Czech Republic</option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Djibouti">Djibouti</option>
                                    <option value="Dominica">Dominica</option>
                                    <option value="Dominican Republic">Dominican Republic</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">El Salvador</option>
                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                    <option value="Eritrea">Eritrea</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Ethiopia">Ethiopia</option>
                                    <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                    <option value="Faroe Islands">Faroe Islands</option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="French Guiana">French Guiana</option>
                                    <option value="French Polynesia">French Polynesia</option>
                                    <option value="French Southern Territories">French Southern Territories</option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Gibraltar">Gibraltar</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Greenland">Greenland</option>
                                    <option value="Grenada">Grenada</option>
                                    <option value="Guadeloupe">Guadeloupe</option>
                                    <option value="Guam">Guam</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guernsey">Guernsey</option>
                                    <option value="Guinea">Guinea</option>
                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                    <option value="Guyana">Guyana</option>
                                    <option value="Haiti">Haiti</option>
                                    <option value="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
                                    <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="Iceland">Iceland</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                    <option value="Iraq">Iraq</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Isle of Man">Isle of Man</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jersey">Jersey</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakhstan">Kazakhstan</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kiribati">Kiribati</option>
                                    <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                    <option value="Korea, Republic of">Korea, Republic of</option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                    <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Lesotho">Lesotho</option>
                                    <option value="Liberia">Liberia</option>
                                    <option value="Libya">Libya</option>
                                    <option value="Liechtenstein">Liechtenstein</option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">Luxembourg</option>
                                    <option value="Macao">Macao</option>
                                    <option value="Macedonia, the former Yugoslav Republic of">Macedonia, the former Yugoslav Republic of</option>
                                    <option value="Madagascar">Madagascar</option>
                                    <option value="Malawi">Malawi</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Maldives">Maldives</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Marshall Islands">Marshall Islands</option>
                                    <option value="Martinique">Martinique</option>
                                    <option value="Mauritania">Mauritania</option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mayotte">Mayotte</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                    <option value="Moldova, Republic of">Moldova, Republic of</option>
                                    <option value="Monaco">Monaco</option>
                                    <option value="Mongolia">Mongolia</option>
                                    <option value="Montenegro">Montenegro</option>
                                    <option value="Montserrat">Montserrat</option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Mozambique">Mozambique</option>
                                    <option value="Myanmar">Myanmar</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Nauru">Nauru</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherlands">Netherlands</option>
                                    <option value="New Caledonia">New Caledonia</option>
                                    <option value="New Zealand">New Zealand</option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Niue">Niue</option>
                                    <option value="Norfolk Island">Norfolk Island</option>
                                    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Palau">Palau</option>
                                    <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Philippines">Philippines</option>
                                    <option value="Pitcairn">Pitcairn</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Puerto Rico">Puerto Rico</option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Réunion">Réunion</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russian Federation">Russian Federation</option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="Saint Barthélemy">Saint Barthélemy</option>
                                    <option value="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option>
                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                    <option value="Saint Lucia">Saint Lucia</option>
                                    <option value="Saint Martin (French part)">Saint Martin (French part)</option>
                                    <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                    <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                    <option value="Samoa">Samoa</option>
                                    <option value="San Marino">San Marino</option>
                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Serbia">Serbia</option>
                                    <option value="Seychelles">Seychelles</option>
                                    <option value="Sierra Leone">Sierra Leone</option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option>
                                    <option value="Slovakia">Slovakia</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="Solomon Islands">Solomon Islands</option>
                                    <option value="Somalia">Somalia</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                                    <option value="South Sudan">South Sudan</option>
                                    <option value="Spain">Spain</option>
                                    <option value="Sri Lanka">Sri Lanka</option>
                                    <option value="Sudan">Sudan</option>
                                    <option value="Suriname">Suriname</option>
                                    <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                    <option value="Swaziland">Swaziland</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">Switzerland</option>
                                    <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                    <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                    <option value="Tajikistan">Tajikistan</option>
                                    <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Timor-Leste">Timor-Leste</option>
                                    <option value="Togo">Togo</option>
                                    <option value="Tokelau">Tokelau</option>
                                    <option value="Tonga">Tonga</option>
                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">Turkmenistan</option>
                                    <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                    <option value="Tuvalu">Tuvalu</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="United States">United States</option>
                                    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                    <option value="Uruguay">Uruguay</option>
                                    <option value="Uzbekistan">Uzbekistan</option>
                                    <option value="Vanuatu">Vanuatu</option>
                                    <option value="Venezuela, Bolivarian Republic of">Venezuela, Bolivarian Republic of</option>
                                    <option value="Viet Nam">Viet Nam</option>
                                    <option value="Virgin Islands, British">Virgin Islands, British</option>
                                    <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                    <option value="Wallis and Futuna">Wallis and Futuna</option>
                                    <option value="Western Sahara">Western Sahara</option>
                                    <option value="Yemen">Yemen</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                        </div>
                    </div>
					<div class="row form-row">
                        <div class="col-sm-3">
                            <label><strong>ID Card's Name</strong></label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="IDName">
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-sm-3">
                            <label><strong>ID Number</strong></label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="IDNumber">
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-sm-3">
                            <label><strong>ID Expires on</strong></label>
                        </div>
                        <div class='col-sm-9'>
                            <input type="datetime-local" class="form-control" name="IDExpireson" id="IDExpireson">
                            <!--<script>
                                var $input = jQuery('#IDExpireson').pickadate({
                                    selectYears:true,
                                    selectMonths: true,
                                })
                            </script>
                            <!--<div class="inpu-inline-row">
                                <input type="text" class="form-control" name="IDExpireson_year">
                                <input type="text" class="form-control" name="IDExpireson_month">
                                <input type="text" class="form-control" name="IDExpireson_day">
                            </div>-->
                        </div>
                    </div>
                    <div class="row form-submit">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-verif">Verify Now</button>
                        </div>
                    </div>
                </form>
				<?php } ?>
            </div>

        </div>
        <div class="col-sm-4">
            <div class="tips mt-5">
                <div class="tips-arrow"></div>
                <div class="tips-inner">
                    <div class="d-flex">
                        <i class="fas fa-lightbulb"></i>
                        <p>Acceptable ID types include your current passport, national ID card, driver's license or any other valid government-issued photo ID.</p>
                    </div>
                    <p> Students IDs are NOT accepted.<br>
                        <br>
                        This ID must include your picture, signature, name, date of birth and address, with the exception of passports, which do not need your address.</p>
                </div>
            </div>
            <div class="tips">
                <div class="tips-inner">
                    <strong>No scanner?</strong> Take a photo of your ID with digital camera, mobile phone or webcam.
                </div>
            </div>

        </div>
    </div>

</div>


<style>
    .mail-template table table td {
        text-align: left!important;
    }
    .mail-template img {
        display: inline-block!important;
    }
</style>
<!-- Verified Email template-->
<!--
<div style="margin: 0 auto;border-radius: 3px;  background-color: #deeff0;max-width: 600px;padding-top: 30px;padding-left: 10px; padding-right: 10px;padding-bottom: 25px;">
    <div class="mail-template">
        <table cellspacing="0" border="0" cellpadding="0" style="width: 100%;">
            <tr>
                <td style="text-align: center;padding-bottom: 18px;" align="center"><a href="https://www.travpart.com/English/"><img style="border: none;" src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/logo.jpg" width="232" height="69" alt="www.travpart.com"></a></td>
            </tr>
            <tr>
                <td style="background-color: #fff;padding: 60px 40px;">
                    <table cellspacing="0" border="0" cellpadding="0" style="width: 100%;">
                        <tr>
                            <td style="font-family: OpenSans;font-size: 24px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.5;  letter-spacing: -1.4px;text-align: left;  color: #42454e;padding-bottom: 36px;">Hi Helenmoon,</td>
                        </tr>
                        <tr>
                            <td style="font-family: OpenSans;font-size: 24px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.5;   letter-spacing: -1.4px;text-align: left;  color: #42454e;padding-bottom: 10px;">Congratulations! </td>
                        </tr>
                        <tr>
                            <td style="font-family: OpenSans;font-size: 18px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.5;  letter-spacing: -1.4px;text-align: left;  color: #42454e;">
                                Your identifications has been reviewed and accepted. Thanks for helping us maintain a trusted workplace for all.<br>
                                <br>
                                You can now get paid for your commissions.</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 60px;padding-top: 60px;">
                                <a href="#" style="width: 280px;height: 50px;line-height: 50px; font-family: OpenSans; font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;letter-spacing: -0.5px;color: #ffffff;border-radius: 5px;box-shadow: 0px 2px 0 0 rgba(93, 202, 197, 0.31);  background-color: #66cccc;display: block;text-align: center;text-decoration: none!important;">Start submitting proposals</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: OpenSans;font-size: 18px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.5;  letter-spacing: -1.4px;text-align: left;  color: #42454e;">Thanks,<br>
                                The Travpart Team</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="font-family: OpenSans;font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;padding-top: 30px;">Download our mobile app on</td>
            </tr>
            <tr>
                <td style="font-family: OpenSans;font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;padding-bottom: 40px;"><a href="https://www.travpart.com/English/download" style="color: #66cccc;text-decoration: none!important;">iPhone</a>  or <a href="https://www.travpart.com/English/download" style="color: #66cccc;text-decoration: none!important;">Android</a> </td>
            </tr>
            <tr>
                <td style="padding-bottom: 40px;text-align: center" align="center">
                    <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/1.jpg" width="27" height="24"></a>
                    <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/2.jpg" width="27" height="24"></a>
                    <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/3.jpg" width="27" height="24"></a>
                    <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/4.jpg" width="27" height="24"></a>
                </td>
            </tr>
            <tr>
                <td style="font-family: OpenSans;font-size: 14px;font-weight: normal;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;">&copy; 2018 Travpart Inc.</td>
            </tr>
        </table>
    </div>
</div>
-->
<!-- Unverified Email template -->
<!--
<div style="margin: 0 auto;border-radius: 3px;  background-color: #deeff0;max-width: 600px;padding-top: 30px;padding-left: 10px; padding-right: 10px;padding-bottom: 25px;">
    <div class="mail-template">
        <table cellspacing="0" border="0" cellpadding="0" style="width: 100%;">
            <tr>
                <td style="text-align: center;padding-bottom: 18px;" align="center"><a href="https://www.travpart.com/English/"><img style="border: none;" src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/logo.jpg" width="232" height="69" alt="www.travpart.com"></a></td>
            </tr>
            <tr>
                <td style="background-color: #fff;padding: 60px 40px;">
                    <table cellspacing="0" border="0" cellpadding="0" style="width: 100%;">
                        <tr>
                            <td style="font-family: OpenSans;font-size: 24px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.5;  letter-spacing: -1.4px;text-align: left;  color: #42454e;padding-bottom: 36px;">Hi Helenmoon,</td>
                        </tr>
                        <tr>
                            <td style="font-family: OpenSans;font-size: 18px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.5;  letter-spacing: -1.4px;text-align: left;  color: #42454e;padding-bottom: 103px;">
                                Your identifications has been reviewed and rejected. The reason of the rejection will remain confidential on our site.<br>
                                <br>
                                Thanks for helping us maintain a trusted workplace for all.<br>
                                <br>
                                If you have any quetions, please contact<br>
                                <a href="#" style="color: #61cbc9!important;text-decoration: none!important;"> contact@tourfrombali.com</a></td>
                        </tr>

                        <tr>
                            <td style="font-family: OpenSans;font-size: 18px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.5;  letter-spacing: -1.4px;text-align: left;  color: #42454e;">Thanks,<br>
                                The Travpart Team</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="font-family: OpenSans;font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;padding-top: 30px;">Download our mobile app on</td>
            </tr>
            <tr>
                <td style="font-family: OpenSans;font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;padding-bottom: 40px;"><a href="https://www.travpart.com/English/download" style="color: #66cccc;text-decoration: none!important;">iPhone</a>  or <a href="https://www.travpart.com/English/download" style="color: #66cccc;text-decoration: none!important;">Android</a> </td>
            </tr>
            <tr>
                <td style="padding-bottom: 40px;text-align: center" align="center">
                    <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/1.jpg" width="27" height="24"></a>
                    <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/2.jpg" width="27" height="24"></a>
                    <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/3.jpg" width="27" height="24"></a>
                    <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/4.jpg" width="27" height="24"></a>
                </td>
            </tr>
            <tr>
                <td style="font-family: OpenSans;font-size: 14px;font-weight: normal;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;">&copy; 2018 Travpart Inc.</td>
            </tr>
        </table>
    </div>
</div>
-->
<!-- Email Notification  Email template -->
<!--
<div class="mail-template">
        <table cellspacing="0" border="0" cellpadding="0" style="width: 100%;">
            <tr>
                <td style="text-align: center;padding-bottom: 18px;" align="center"><a href="https://www.travpart.com/English/"><img style="border: none;" src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/logo.jpg" width="232" height="69" alt="www.travpart.com"></a></td>
            </tr>
            <tr>
                <td style="background-color: #fff;padding: 60px 40px;">
                    <table cellspacing="0" border="0" cellpadding="0" style="width: 100%;">
                        <tr>
                            <td style="font-family: OpenSans;font-size: 18px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.7;  letter-spacing: -1.4px;text-align: left;  color: #42454e;padding-bottom: 0px;">Dear our partner,<br>
                                <stong style="font-weight: 600;">Sales Agent (xxxname)</stong><br>
                                <br>
                                We are contacting you regarding the tour package you’ve created with <stong style="font-weight: 600;">booking code (xxxxxbookingcode)</stong><br>
                                <br>
                                Thank you for creating the tour package in our travpart platform. However we found the content within the complete tour details section was not written according to a standardized tour package. Since the more details and pictures/video uploaded which described about the tour package will attract more buyers to your tour package. Therefore; we advise you to rewrite your complete tour detail page in the following standardized example format:<br>
                                <br>
                                <span style="font-size: 24px;">Day 1</span><br>
                                Visit Uluwatu Temple at 09:00<br>
                                - Bali Cultural Heritage Temple<br>
                                Lunch at Indonesian Restaurant at 12:00<br>
                                Rest in Hotel Jambu, Bali Area at 20:00<br>
                                <br>
                                <span style="font-size: 24px;">Day 2</span><br>
                                Depart to Kuta Beach at 08:00 from your hotel.<br>
                                Snorkeling at Kuta Beach at 09:00<br>
                                - View the great oceanic coral reef<br>
                                Dinner at American Restaurant at 18:00<br>
                                Rest in Hotel Pullman, Kuta Beach Area at 20:00<br>
                                We hope this is an useful information.<br>
                                <br>
                                If you have any other questions, please don’t hesitate to contact us at <a href="#" style="color: #61cbc9!important;text-decoration: none!important;"> contact@tourfrombali.com</a><br>
                                <br>
                                Sincerely,<br>
                                Travpart crew

                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td style="font-family: OpenSans;font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;padding-top: 30px;">Download our mobile app on</td>
            </tr>
            <tr>
                <td style="font-family: OpenSans;font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;padding-bottom: 40px;"><a href="https://www.travpart.com/English/download" style="color: #66cccc;text-decoration: none!important;">iPhone</a>  or <a href="https://www.travpart.com/English/download" style="color: #66cccc;text-decoration: none!important;">Android</a> </td>
            </tr>
            <tr>
                <td style="padding-bottom: 40px;text-align: center" align="center">
                    <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/1.jpg" width="27" height="24"></a> <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/2.jpg" width="27" height="24"></a> <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/3.jpg" width="27" height="24"></a> <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/4.jpg" width="27" height="24"></a>
                </td>
            </tr>
            <tr>
                <td style="font-family: OpenSans;font-size: 14px;font-weight: normal;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;">&copy; 2018 Travpart Inc.</td>
            </tr>
        </table>
    </div>
-->