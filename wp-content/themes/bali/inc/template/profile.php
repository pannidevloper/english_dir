
<?php
global $wpdb;
$current_user = wp_get_current_user();
$user_id = $current_user->ID;


$error = array();
$success=false;
$user_data = array();
$meta_data = array();

/*if($user_id) {
	if(empty(get_user_meta($user_id, 'phone_number', true))) {
		$phone=$wpdb->get_var("SELECT `phone` FROM `user` WHERE `username`='{$current_user->user_login}'");
	if(!empty($phone)
		update_user_meta($user_id,'phone_number',$phone);
	}
}*/

$careers = $wpdb->get_results("SELECT * FROM `wp_career` WHERE userid=" . um_profile_id());
$educations = $wpdb->get_results("SELECT * FROM `wp_education` WHERE userid=" . um_profile_id());

$phone_number=$wpdb->get_var("SELECT `phone` FROM `user` WHERE `username`='{$current_user->user_login}'");

$languages=array(
    'Afrikanns','Albanian','Arabic','Armenian','Basque','Bengali',
    'Bulgarian','Catalan','Cambodian','Chinese (Mandarin)','Croation',
    'Czech','Danish','Dutch','English','Estonian','Fiji','Finnish',
    'French','Georgian','German','Greek','Gujarati','Hebrew','Hindi',
    'Hungarian','Icelandic','Indonesian','Irish','Italian','Japanese',
    'Javanese','Korean','Latin','Latvian','Lithuanian','Macedonian',
    'Malay','Malayalam','Maltese','Maori','Marathi','Mongolian','Nepali',
    'Norwegian','Persian','Polish','Portuguese','Punjabi','Quechua',
    'Romanian','Russian','Samoan','Serbian','Slovak','Slovenian',
    'Spanish','Swahili','Swedish ','Tamil','Tatar','Telugu','Thai','Tibetan',
    'Tonga','Turkish','Ukranian','Urdu','Uzbek','Vietnamese','Welsh','Xhosa'
);

if($user_id && array_key_exists('profile_submit',$_POST)) {
    if(!empty(trim($_POST['password']))) {
        if($_POST['password']===$_POST['password_again']) {
            $user_data['user_pass'] = trim($_POST['password']);
        } else {
            $error[] ='Passwords do not match.';
        }
    }
    if(!empty($_POST['display_name']) || !empty($_POST['display_name_mobile'])) {
        $display_name_pc = filter_input(INPUT_POST, 'display_name', FILTER_SANITIZE_STRING);
        $display_name_mobile = filter_input(INPUT_POST, 'display_name_mobile', FILTER_SANITIZE_STRING);
        if(um_get_display_name($current_user->ID)!=$display_name_pc) {
            $display_name=$display_name_pc;
        }
        if(um_get_display_name($current_user->ID)!=$display_name_mobile) {
            $display_name=$display_name_mobile;
        }
        if(!empty($display_name)) {
            wp_update_user(
                array('ID' => $current_user->ID,
                'nickname' => $display_name,
                'display_name' => $display_name)
            );
            update_travchat_nickname($current_user->user_login, $display_name);
        }
    }
    if(empty($error)) {
        if(isset($_POST['gender'])&&is_string($_POST['gender'])) {
            $meta_data['gender'] = sanitize_text_field($_POST['gender']);
        }
        if(isset($_POST['user_email'])&&is_string($_POST['user_email'])) {
            $user_email = sanitize_text_field(wp_unslash($_POST['user_email']));
            if(!is_email($user_email)) {
//                The email address isn&#8217;t correct.
                $error[] ="The email address isn&#8217;t correct.";
            }  elseif ( $id = email_exists( $user_email ) ) {
//                This email is already registered, please choose another one.
                if($id != $user_id)
                    $error[] ="This email is already registered, please choose another one.";
            } else {
                $user_data['user_email'] = $user_email;
				$wpdb->update('user', array('email'=>$user_email), array('username'=>$current_user->user_login));
            }
        }
        if(!empty($_POST['phone_number'])) {
			$new_phone_number=filter_input(INPUT_POST, 'phone_number', FILTER_SANITIZE_STRING);
			if($wpdb->get_var("SELECT COUNT(*) FROM `user` WHERE `phone`='{$new_phone_number}'")>0) {
				if($wpdb->get_var("SELECT COUNT(*) FROM `user` WHERE `phone`='{$new_phone_number}' AND `username`='{$current_user->user_login}'")!=1)
					$error[]="The phone number has been used.";
			}
			else {
				if($wpdb->update('user', array('phone'=>$new_phone_number), array('username'=>$current_user->user_login)))
					$phone_number=$new_phone_number;
			}
            //$meta_data['phone_number'] = sanitize_text_field(strip_tags($_POST['phone_number']));
        }

        if(isset($_POST['description'])) {
            $meta_data['description'] = sanitize_textarea_field(strip_tags($_POST['description']));
        }

        if(!empty($_POST['hobby'])) {
            $meta_data['hobby'] = filter_input(INPUT_POST, 'hobby', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['date_of_birth'])) {
            $meta_data['date_of_birth'] = filter_input(INPUT_POST, 'date_of_birth', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['drinking'])) {
            $meta_data['drinking'] = filter_input(INPUT_POST, 'drinking', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['edu_level'])) {
            $meta_data['edu_level'] = filter_input(INPUT_POST, 'edu_level', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['smoking'])) {
            $meta_data['smoking'] = filter_input(INPUT_POST, 'smoking', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['political_view'])) {
            $meta_data['political_view'] = filter_input(INPUT_POST, 'political_view', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['seeking'])) {
            $meta_data['seeking'] = filter_input(INPUT_POST, 'seeking', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['religion'])) {
            $meta_data['religion'] = filter_input(INPUT_POST, 'religion', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['language'])) {
            $meta_data['language'] = $_POST['language'];
        }

        if(!empty($_POST['marital_status'])) {
            $meta_data['marital_status'] = filter_input(INPUT_POST, 'marital_status', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['current_city'])) {
            $meta_data['current_city'] = filter_input(INPUT_POST, 'current_city', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['hometown'])) {
            $meta_data['hometown'] = filter_input(INPUT_POST, 'hometown', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['social_status'])) {
            $meta_data['social_status'] = 'on';
        }
        else {
            $meta_data['social_status'] = 'off';
        }

        if (!empty($_POST['height'])) {
            $meta_data['height'] = filter_input(INPUT_POST, 'height', FILTER_SANITIZE_STRING);
            $meta_data['height_unit'] = filter_input(INPUT_POST, 'height_unit', FILTER_SANITIZE_STRING);
            if ($meta_data['height_unit'] != "INCH")
                $meta_data['height_unit'] = "CM";
        }

        if(!empty($_POST['location_visiable'])) {
            $meta_data['location_visiable'] = filter_input(INPUT_POST, 'location_visiable', FILTER_SANITIZE_STRING);
        }

        if(!empty($_POST['location_tracker'])) {
            $meta_data['location_tracker'] = 'on';
        }
        else {
            $meta_data['location_tracker'] = 'off';
        }

        if(!empty($_POST['family_member'])) {
            $meta_data['family_member'] = trim(filter_input(INPUT_POST, 'family_member', FILTER_SANITIZE_STRING),',');
        }

        if(empty($error)) {
            $user_data['ID'] = $user_id;
            $update_user = wp_update_user( $user_data );

            if ( is_wp_error( $update_user ) ) {
                $error[] = $update_user->get_error_message();
            } else {
                foreach ($meta_data as $k => $v) {
                    update_user_meta($user_id,$k,$v);
                }
                $success = "Profile has been updated..";
            }
        }


    }

}

if (empty($profile_completeness) || array_key_exists('profile_submit', $_POST)) {
    update_profile_completeness();
}

$location_visiable=get_user_meta($current_user->ID,'location_visiable',true);

function c_input_user_meta($field_name,$user) {
    if($_POST && array_key_exists($field_name,$_POST)) {
        echo esc_attr($_POST[$field_name]);
    } else {
        echo esc_attr($user->$field_name);
    }
}
function c_radio_user_meta($field_name,$checked,$user) {
    if($_POST && array_key_exists($field_name,$_POST)) {
        echo $_POST[$field_name]==$checked?'checked':'';
    } else {
        echo $user->$field_name==$checked?'checked':'';
    }
}

?>
<?php if(!$user_id):?>
    <?php
    wp_redirect('/user');
    ?>
<?php else:?>
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css'>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    
    <link href="<?php bloginfo('template_url'); ?>/css/select2.min.css" rel="stylesheet">
    <script src="<?php bloginfo('template_url'); ?>/js/select2.min.js" type="text/javascript"></script>
    
    <div class="button_area mobile-button">
        <div class="row">
            <div class="col-md-12 button-display">
                <div class="col-md-2 p_gen_space">
                    <a href="#" class="gen-btn-info gen-btn-active" role="button">General Profile</a>
                </div>
                <div class="col-md-10 p_car_space">
                    <a href="<?php echo add_query_arg(array('action' => 'career_education_setting')); ?>" class="gen-btn-info" role="button">Career & Education</a>
                    <img class="carr_edu_notify_img_tab_mob" src="https://www.travpart.com/English/wp-content/uploads/2020/03/exclamation.png">
                </div>
            </div>
        </div>
    </div>
    <div class="button_area">
        <div class="row">
            <div class="col-md-6">
                <a class="gen-btn-info gen-btn-active" role="button">General Profile</a>
                <a href="<?php echo add_query_arg(array('action' => 'career_education_setting')); ?>" class="gen-btn-info" role="button">Career & Education</a>
                <img class="carr_edu_notify_img_tab" src="https://www.travpart.com/English/wp-content/uploads/2020/03/exclamation.png">
            </div>
        </div>
    </div>
    <div class="p-4">

    <form class="form-horizontal" method="post" action="">
        
            

        <div class="row">
            
            <div class="col-md-6">
                <h4 class="profile-title pt-3 mb-4">Profile Setting</h4>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-4 col-sm-6 loc_so_dis">
                <div class="col-md-12 location_pos">
                    <div class="col-md-7 col-sm-6 location_text location-tracker" style="padding-right:0px;">
                        <p>Location Tracker<i class="fas fa-sort-down" style="font-size: 20px;color: black;padding-left: 5px;"></i></p>
                        <div class="location-tracker-option">
                            <p visiable="family" class="<?php echo empty($location_visiable)||$location_visiable=='family'?'selected':''; ?>">Only family can see my location</p>
                            <p visiable="family_friend" class="<?php echo $location_visiable=='family_friend'?'selected':''; ?>">Friends and family can see my location</p>
                            <p visiable="friend" class="<?php echo $location_visiable=='friend'?'selected':''; ?>">Only friends can see my location</p>
                            <p visiable="anyone" class="<?php echo $location_visiable=='anyone'?'selected':''; ?>">Anyone can see my location(not recommended)</p>
                            <input type="hidden" name="location_visiable" value="<?php c_input_user_meta('location_visiable',$current_user)?>" />
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-6 loc_track_pad">
                        <input name="location_tracker" <?php c_radio_user_meta('location_tracker','on',$current_user); ?> type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled">
                    </div>
                </div>
                <div class="col-md-12 location_pos">
                    <div class="col-md-7 col-sm-6">
                        <p class="so_sta_text">Social Status</p>
                    </div>
                    <div class="col-md-5 col-sm-6">
                        <input name="social_status" <?php c_radio_user_meta('social_status','on',$current_user); ?> type="checkbox" data-toggle="toggle" data-on="Active" data-off="Busy">
                    </div>
                </div>
                
                <!--div class="col-xs-12 col-sm-12 col-md-12 remove_padd forms">
						<div class="editfirstname"><a class="editlink">Edit</a></div>
						<div class="form-group mb-3 p_name_desk">
			                <div class="col-sm-12">
			               		<div class="row">
			               			<div class="col-md-6">
			               				Name
			               			</div>
			               			<div class="col-md-6 text-right">
			               				<button type="button"  class="open_drop_down_names btn"><i class="fas fa-pen"></i> Edit</button>
			               			</div>
			               		</div>
			                    <input type="text" placeholder="" class="form-control p_name_in" id="name" value="<?php echo $display_name; ?>" disabled>
			                </div>
			            </div>
			            
			        <div class="bg_grey_2">
				        <div class="bg_grey">
				        	
				        
							<div class="form-group mb-3">
								<div class="col-md-12 ">
								    <div class="col-md-4 remove_padd p-userid">
								        <label for="userId" class="control-label">First name</label>
								    </div>
								    <div class="col-sm-8">
								         <input type="text" class="form-control p_name_in" id="name" value="<?php echo $display_name; ?>" disabled>
								    </div>
								</div>
							</div>
						<div class="form-group mb-3">
						<div class="col-md-12">
						    <div class="col-md-4 remove_padd p-userid">
						        <label for="userId" class="control-label">Middle name</label>
						    </div>
						    <div class="col-sm-8">
						        <input type="text" class="form-control" id="userId" placeholder="(Optional)">
						    </div>
						</div>
						</div>
						<div class="form-group mb-3">
						<div class="col-md-12">
						    <div class="col-md-4 remove_padd p-userid">
						        <label for="userId" class="control-label">Surname</label>
						    </div>
						    <div class="col-sm-8">
						        <input type="text" class="form-control" id="userId" value="bharadhanvm">
						    </div>
						</div>
						</div>
					</div>
						<div class="text-right">
							<a href="#">Save</a>
						</div>
					</div>
				</div-->
                
            </div>
        </div>
        <?php foreach($error as $error_item) { ?>
            <div class="alert alert-danger"><?php echo $error_item;?></div>
        <?php } ?>
        <?php if($success):?>
            <div class="alert alert-success"><?php echo $success;?></div>
        <?php endif;?>
            <?php
                $display_name = um_get_display_name($current_user->ID); 
            ?>

            <div class="form-group mb-3 p_name_desk">
                <div class="col-md-12">
                    <div class="col-md-2 p_full_name">
                        <label for="name" class="control-label" >Full Name</label>
                    </div>
                    <div class="col-sm-10">
                        <!--First name is made editable on 14-02-2020 Starts Here-->
                        <input type="text" class="form-control p_name_in" id="name" name="display_name" value="<?php echo $display_name; ?>">
                        <!--First name is made editable on 14-02-2020 Ends Here-->
                    </div>
                </div>
            </div>
            	<?php //if($_GET['debug']==1){ ?>
            <!--<div class="col-xs-12 col-sm-12 col-md-12 forms">
					<div class="row hide_click">
						<div class="col-md-4 for_nameedit text-center">
							<p>
								<strong>
									<?php echo $display_name; ?>
								</strong>
							</p>
						</div>
						<div class="col-md-4">
							<button type="button" class="btn for_btnedit">
							 <i class="fa fa-pencil"></i> Edit
							</button>
						</div>
					</div>
					<div class="content_afterclick">
						<div class="form-group mb-3">
							<div class="col-md-6">
							    <div class="col-md-4 p-userid">
							        <label for="userId" class="control-label">First name</label>
							    </div>
							    <div class="col-sm-8">
							         <input type="text" class="form-control p_name_in" id="name1" value="<?php echo $display_name; ?>">
							    </div>
							</div>
						</div>
						<div class="form-group mb-3">
							<div class="col-md-6">
							    <div class="col-md-4 p-userid">
							        <label for="userId" class="control-label">Middle name</label>
							    </div>
							    <div class="col-sm-8">
							        <input type="text" class="form-control" id="userId" placeholder="(Optional)">
							    </div>
							</div>
						</div>
						<div class="form-group mb-3">
							<div class="col-md-6">
							    <div class="col-md-4 p-userid">
							        <label for="userId" class="control-label">Surname</label>
							    </div>
							    <div class="col-sm-8">
							        <input type="text" class="form-control" id="userId" value="bharadhanvm">
							    </div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6">
							    <div class="col-md-4">
							    </div>
							    <div class="col-md-4">
							        <button type="button" class="btn btn-default cancel_div"> Cancel </button>
							    </div>
							    <div class="col-sm-4">
							        <button type="button" class="btn btn-success save_div"> Save </button>							        
							    </div>
							</div>
						</div>
						
					</div>
					

			</div>-->
			<?php //} ?>
            <div class="form-group mb-3 p_name_mob">
                <div class="col-md-6">
                    <div class="col-md-4 p_name">
                        <label for="name" class="control-label">Full Name</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control p_name_in" name="display_name_mobile" id="name" value="<?php echo $display_name; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="col-md-6">
                    <div class="col-md-4 p-userid">
                        <label for="userId" class="control-label">User ID</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <input type="text" class="form-control" id="userId" value="<?php echo $current_user->user_login;?>" disabled>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-fammem">
                        <label for="familyMember" class="control-label">Family Member</label>
                    </div>
                    <div class="col-sm-8 user_padd p_input_dis">
                        <input type="text" class="form-control familymember-popup" id="familyMember" name="family_member" value="<?php c_input_user_meta('family_member',$current_user)?>">
                        <i class="fas fa-plus familymember-popup"></i>
                    </div>
                    <div class="hover_bkgr_fricc pop-familymember">
                        <span class="helper"></span>
                        <div>
                            <div class="popupClose-familymember">X</div>
                            <h4 class="pop_fam_txt">ADD A FAMILY MEMBER</h4>
                            <input id="familymember_name" type="text" placeholder="Enter a name">
                            <ul class="familymember-options">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="col-md-6">
                    <div class="col-md-4 p-sex">
                        <i aria-hidden="true" class="fas fa-venus-mars"></i>
                        <label  class="control-label">Sex</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <div class="radio">
                            <label>
                                <input value="Male" type="radio" name="gender" <?php c_radio_user_meta('gender','Male',$current_user); ?>> Male
                            </label>
                            <label>
                                <input value="Female" type="radio" name="gender" <?php c_radio_user_meta('gender','Female',$current_user); ?>> Female
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-mstatus">
                        <i aria-hidden="true" class="fas fa-ring"></i>
                        <label  class="control-label">Status</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <div class="radio">
                            <label>
                                <input value="single" type="radio" name="marital_status" <?php c_radio_user_meta('marital_status','single',$current_user); ?>> Single
                            </label>
                            <label>
                                <input value="married" type="radio" name="marital_status" <?php c_radio_user_meta('marital_status','married',$current_user); ?>> Married
                            </label>
                            <label>
                                <input value="divorced" type="radio" name="marital_status" <?php c_radio_user_meta('marital_status','divorced',$current_user); ?>> Divorced
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="col-md-6">
                    <div class="col-md-4 p-phone">
                        <i aria-hidden="true" class="fas fa-phone-volume"></i>
                        <label for="phoneNumber" class="control-label">Phone Number</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <input type="tel" class="form-control" id="phoneNumber" name="phone_number" value="<?php echo $phone_number; ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-dob">
                        <i aria-hidden="true" class="fas fa-table"></i>
                        <label for="dob" class="control-label">Date of Birth</label>
                    </div>
                    <div class="col-sm-8 user_padd p_dob_input">
                        <input type="date" class="form-control" id="dob" name="date_of_birth" value="<?php c_input_user_meta('date_of_birth',$current_user)?>">
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="col-md-6">
                    <div class="col-md-4 p-email">
                        <i aria-hidden="true" class="far fa-envelope"></i>
                        <label for="Email" class="control-label">Email</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <input type="text" class="form-control" id="email" value="<?php c_input_user_meta('user_email',$current_user)?>" name="user_email">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-hobby">
                        <i aria-hidden="true" class="fas fa-atom"></i>
                        <label for="Hobby" class="control-label">Hobby</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <input type="text" class="form-control" id="hobby" value="<?php c_input_user_meta('hobby',$current_user)?>" name="hobby">
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="col-md-6">
                    <div class="col-md-4 p-hometown">
                        <i aria-hidden="true" class="fas fa-home"></i>
                        <label for="Hometown" class="control-label">Hometown</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <select type="text" class="form-control world-cities-chosen" id="hometown" name="hometown">
                            <?php if(!empty($_POST['hometown']) || !empty($current_user->hometown)) { ?>
                                <option value="<?php c_input_user_meta('hometown',$current_user); ?>">
                                    <?php c_input_user_meta('hometown',$current_user); ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-current-city">
                        <i aria-hidden="true" class="fas fa-city"></i>
                        <label for="Current-city" class="control-label">Current City</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <select type="text" class="form-control world-cities-chosen" id="current_city" name="current_city">
                            <?php if(!empty($_POST['current_city']) || !empty($current_user->current_city)) { ?>
                                <option value="<?php c_input_user_meta('current_city',$current_user); ?>">
                                    <?php c_input_user_meta('current_city',$current_user); ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="col-md-6">
                    <div class="col-md-4 p-height">
                        <i aria-hidden="true" class="fas fa-ruler-vertical"></i>
                        <label for="Height" class="control-label">Height</label>
                    </div>
                    <div class="col-sm-8 user_padd d_height">
                        <div class="col-md-8">
                            <input type="text" class="form-control"  maxlength="3" id="height" value="<?php c_input_user_meta('height',$current_user)?>" name="height" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                            <input type="hidden" name="height_unit" value="<?php c_input_user_meta('height_unit',$current_user)?>">
                        </div>
                        <div class="col-md-4 cm_inch_pos">
                            <div id="selector"></div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-drinking">
                        <i aria-hidden="true" class="fas fa-wine-glass-alt"></i>
                        <label for="Drinking" class="control-label">Drinking</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <input type="text" class="form-control drinking-popup" id="drinking"  name="drinking" value="<?php c_input_user_meta('drinking',$current_user)?>">
                    </div>
                    <div class="hover_bkgr_fricc pop-drinking">
                        <span class="helper"></span>
                        <div>
                            <div class="popupClose-drinking">X</div>
                            <i aria-hidden="true" class="fas fa-wine-glass-alt"></i>
                            <h3 class="txt-head-color">Do you drink alcohol?</h3>
                            <div id="drink_select">
                                <p>Never</p>
                                <p>Socially</p>
                                <p>Seldom</p> 
                                <p>Alcoholic</p>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="col-md-6">
                    <div class="col-md-4 p-edu-level">
                        <i aria-hidden="true" class="fas fa-graduation-cap"></i>
                        <label for="Education-Level" class="control-label">Education level</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <input type="text" class="form-control education-popup" id="edu_level" name="edu_level" value="<?php c_input_user_meta('edu_level',$current_user)?>">
                    </div>
                    <div class="hover_bkgr_fricc pop-edu">
                        <span class="helper"></span>
                        <div >
                            <div class="popupClose-edu">X</div>
                            <i aria-hidden="true" class="fas fa-graduation-cap"></i>
                            <h3 class="txt-head-color">What's your education level?</h3>
                            <div id="edu_select">
                                <p>Technical College</p>
                                <p>Undergrade</p>
                                <p>Bachelor Degree</p>
                                <p>Postgrade Student</p>
                                <p>Postgraduate Degree</p>
                            </div>                           
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-smoking">
                        <i aria-hidden="true" class="fas fa-smoking"></i>
                        <label for="Drinking" class="control-label">Smoking</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <input type="text" class="form-control smoking-popup" id="smoking" name="smoking" value="<?php c_input_user_meta('smoking',$current_user)?>">
                    </div>
                    <div class="hover_bkgr_fricc pop-smoking">
                        <span class="helper"></span>
                        <div>
                            <div class="popupClose-smo">X</div>
                            <i aria-hidden="true" class="fas fa-smoking"></i>
                            <h3 class="txt-head-color">Do you smoke?</h3>
                            <div id="smoke_select">
                                <p>Never</p>
                                <p>Socially</p>
                                <p>Regularly</p>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="col-md-6">
                    <div class="col-md-4 p-political">
                        <i aria-hidden="true" class="fas fas fa-landmark"></i>
                        <label for="Political-View" class="control-label">Political View</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <input type="text" class="form-control political-popup" id="political_view" name="political_view" value="<?php c_input_user_meta('political_view',$current_user)?>">
                    </div>
                    <div class="hover_bkgr_fricc pop-political">
                        <span class="helper"></span>
                        <div>
                            <div class="popupClose-pol">X</div>
                            <i aria-hidden="true" class="fas fas fa-landmark"></i>
                            <h3 class="txt-head-color">What are your political views?</h3>
                            <div id="pol_select">
                                <p>Apolitical</p>
                                <p>Moderate</p>
                                <p>Liberal</p> 
                                <p>Conservative</p>      
                            </div>             
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-seeking">
                        <i aria-hidden="true" class="fas fa-universal-access"></i>
                        <label for="Seeking" class="control-label">Seeking</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <input type="text" class="form-control seeking-popup" id="seeking" name="seeking" value="<?php c_input_user_meta('seeking',$current_user)?>">
                    </div>
                    <div class="hover_bkgr_fricc pop-seek">
                        <span class="helper"></span>
                        <div>
                            <div class="popupClose-seek">X</div>
                            <i aria-hidden="true" class="fas fa-universal-access"></i>
                            <h3 class="txt-head-color">What do you seek in your travel?</h3>
                            <div id="seek_select">
                                <p>Travel Buddy</p>
                                <p>Connection</p>
                                <p>Experience</p> 
                                <p>Relationship</p> 
                                <p>Wedding</p> 
                                <p>Virtue Friends</p>                   
                                <p>Friends for Pleasure</p>
                                <p>Friends for Benefit</p>
                                <p>Other</p>
                            </div>                                          
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="col-md-6">
                    <div class="col-md-4 p-religion">
                        <i aria-hidden="true" class="fas fa-praying-hands"></i>
                        <label for="Religion" class="control-label">Religion</label>
                    </div>
                    <div class="col-sm-8 user_padd">
                        <input type="text" class="form-control religion-popup" id="religion" name="religion" value="<?php c_input_user_meta('religion',$current_user)?>">
                    </div>
                    <div class="hover_bkgr_fricc pop-religion">
                        <span class="helper"></span>
                        <div>
                            <div class="popupClose-religion">X</div>
                            <i aria-hidden="true" class="fas fa-praying-hands"></i>
                            <h3 class="txt-head-color">Do you believe in a religion?</h3>
                            <div id="religion_select">
                                <p>Muslim</p>
                                <p>Hindu</p>
                                <p>Jain</p> 
                                <p>Christian</p> 
                                <p>Buddhist</p> 
                                <p>Sikh</p>                   
                                <p>Zoroastrianism</p>
                                <p>Spiritism</p>
                                <p>Other</p>
                            </div>                                          
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 p-language">
                        <i aria-hidden="true" class="fa fa-language"></i>
                        <label for="Language" class="control-label">Language</label>
                    </div>
                    <div class="col-sm-8 user_padd p_sel_languge">
                        <select name="language[]" data-placeholder="Choose a Language..." multiple="multiple">
                          <?php $language=$current_user->language; ?>
                          <?php foreach($languages as $i) {
                            echo '<option value="' . $i . '" ' . ((in_array($i,$language))?'selected':'') . '>' . $i . '</option>';
                          }
                          ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group mb-3 p_desc">
                <div class="col-md-12">
                    <div class="col-md-2 p_bio_info">
                        <label for="description" class="control-label">Biographical Info</label>
                    </div>
                    <div class="col-sm-10">
                        <textarea  class="form-control" name="description" id="description" rows="5" cols="30"><?php c_input_user_meta('description',$current_user)?></textarea>
                    </div>
                </div>
            </div>
			<div class="form-group mb-3 p_pass">
                <div class="col-md-12">
                    <div class="col-md-2 p_pass_text">
                        <label for="password" class="control-label">Password</label>
                    </div>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password" value="">
                    </div>
                </div>
            </div>
            <div class="form-group mb-3 p_pass_again">
                <div class="col-md-12">
                    <div class="col-md-2 p_pass_again_text">
                        <label for="passwordAgain" class="control-label">Password again</label>
                    </div>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="passwordAgain" name="password_again" value="">
                    </div>
                </div>
            </div>

            <div class="form-group mb-3">
                <div class="col-sm-offset-2 col-sm-10 p_save_btn">
                    <button type="submit" class="btn btn-submit-blue" name="profile_submit">Save</button>
                </div>
            </div>
        </form>
    </div>
<?php endif;?>

<style type="text/css">
/*.for_nameedit{
	padding:10px;
}
.remove_padd{
	padding:0px;
}
.hide_click{
	margin-bottom: 20px;
}
.for_btnedit{
	font-size: 13px;
	color: #365899;
	margin-top: 5px;
}
.for_btnedit i{	
	font-size: 10px;
}
.content_afterclick .form-group{
	margin-bottom: 0px;
}
.content_afterclick{
	display:none;
	background: #f2f2f2;
	margin-bottom: 20px;
	padding: 15px 0px;
}*/
    .down-arr:after {
        content: '';
        display: inline-block;
        width: 8px;
        height: 8px;
        margin-left: 5px;
        border-right: 1px solid #888;
        border-top: 1px solid #888;
        transform: rotate(135deg);
    }
    .location-tracker-option {
        display: none;
        z-index: 1;
        position: absolute;
        left: 0px;
        width: 165%;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    }
    .location-tracker-option p {
        cursor: default;
        padding: 8px 10px;
    }
    .location-tracker-option .selected {
        color: white;
        background-color:#22b14c;
    }
    .location-tracker:hover .location-tracker-option{
        display: block;
    }
    ul.familymember-options {
        list-style: none;
        margin: 0;
        padding: 0;
        max-height: 200px;
        overflow-y: auto;
    }
    .familymember-options li {
        padding: 3px 7px 4px;
        display: list-item;
        margin: 5px 0px;
        text-align: left;
    }
    .familymember-options li:hover {
        background-color: #F4F4F4;
     }
    div#content {
        background: white;
    }
    h4.profile-title {
        font-size: 40px;
        margin-top: 25px;
        margin-left: 25px;
        width: 275px;
        background-color: white;
        padding-left: 10px;
        padding-top: 5px !important;
        padding-bottom: 5px;
    }
    .button_area {
        padding-top: 1.5rem;
        padding-bottom: 1rem;
        padding-left: 1.5rem;
        background-color: #d3d3d3;
    }

  .gen-btn-info {
    color: grey;
    background-color: white;
    border: solid 1px grey;
    padding: 7px;
    font-size: 14px;
  }

  .gen-btn-info:hover {
    color: green;
    text-decoration: none;
  }

  .gen-btn-active {
    color: green !important;
    border: solid 1px green;
  }

  .mobile-button {
    display: none;
  }
  .p-4 {
        background-color: #c8bfe7;
    }
    .p_full_name label.control-label,.p_bio_info label.control-label,.p_pass_text label.control-label,.p_pass_again_text label.control-label {
        padding-top: 8px;
        padding-bottom: 9px;
    }
    .p_name_desk div.col-sm-10,.p_desc div.col-sm-10,.p_pass div.col-sm-10,.p_pass_again div.col-sm-10{
        padding-left: 0px;
        padding-right: 11px;
    }
    .p_name_desk div.col-sm-10 input,.p_desc div.col-sm-10 textarea,.p_pass div.col-sm-10 input,.p_pass_again div.col-sm-10 input{
        margin-left: -4px !important;
    }

    .p_name_mob{
        display: none;
    }
    .p_name_in {
        text-transform: capitalize;
    }
    .p_dob_input input{
        height: 40px;
    }

    .p_full_name,.p-userid,.p-fammem,.p-sex,.p-phone,.p-email,.p-hometown,.p-current-city,.p-height,.p-drinking,.p-edu-level,.p-smoking,.p-political,.p-seeking,.p-religion,.p-language,.p-hobby,.p-dob,.p-mstatus,.p_bio_info,.p_pass_text,.p_pass_again_text{
        text-align: right;
        padding-left: 0;
        background-color: white;
    }
    .user_padd{
        padding-left: 0px;
    }
    .bg_grey_2{
  	display: none;
   }
    .bg_grey{
    	margin-bottom: 10px;
		background: #a9a2a224;
		padding: 15px 10px;
	}
    .p-userid label.control-label,.p-fammem label.control-label,.p-sex label.control-label,.p-phone label.control-label,.p-email label.control-label,.p-hometown label.control-label,.p-current-city label.control-label,.p-height label.control-label,.p-drinking label.control-label,.p-smoking label.control-label,.p-seeking label.control-label,.p-religion label.control-label,.p-language label.control-label,.p-hobby label.control-label,.p-dob label.control-label,.p-mstatus label.control-label {
        padding-left: 7px;
        padding-top: 8px;
        padding-bottom: 9px;
    }
    .p-edu-level label.control-label{
        padding-top: 8px;
        padding-bottom: 9px; 
    }
    .select2.select2-container .select2-selection.select2-selection--multiple{
        min-height: 40px !important;
    }
    .user_padd div.radio {
        background-color: white;
        border: 2px solid #ddd !important;
        padding-left: 10px;
        padding-bottom: 6px;
    }
    .col-sm-8.p_input_dis {
        display: flex;
        padding-right: 0px;
    }
    i.fas.fa-plus {
        margin-left: -22px;
        margin-top: 10px;
        font-size: 20px;
        color: #22b14c;
    }
    h4.pop_fam_txt {
        text-align: left;
        color: black;
    }
    .p-political label.control-label {
        padding-left: 5px;
        padding-top: 8px;
        padding-bottom: 9px;
    }
    .p_sel_languge select {
        color: #666666;
        font-weight: 500;
        font-size: 17px;
        width: 100%;
    }
    /* Popup box BEGIN */
.hover_bkgr_fricc{
    cursor:pointer;
    display:none;
    height:100%;
    position:absolute;
    text-align:center;
    top: 47px;
    left: 16%;
    width:100%;
    z-index:10000;
}
.hover_bkgr_fricc .helper{
    display:inline-block;
    height:100%;
    vertical-align:middle;
}
.hover_bkgr_fricc > div {
    background-color: #fff;
    box-shadow: 10px 10px 60px #555;
    display: inline-block;
    height: auto;
    max-width: 551px;
    min-height: 100px;
    vertical-align: middle;
    width: 60%;
    position: relative;
    border-radius: 8px;
    padding: 15px 5%;
}
.popupClose-edu {
    background-color: #fff;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: 1px;
    right: 5px;
    font-size: 15px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
}
.popupClose-edu:hover {
    background-color: #ccc;
}
.popupClose-smo {
    background-color: #fff;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: 1px;
    right: 5px;
    font-size: 15px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
}
.popupClose-smo:hover {
    background-color: #ccc;
}
.popupClose-drinking {
    background-color: #fff;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: 1px;
    right: 5px;
    font-size: 15px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
}
.popupClose-drinking:hover {
    background-color: #ccc;
}
.popupClose-pol {
    background-color: #fff;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: 1px;
    right: 5px;
    font-size: 15px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
}
.popupClose-pol:hover {
    background-color: #ccc;
}
.popupClose-seek {
    background-color: #fff;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: 1px;
    right: 5px;
    font-size: 15px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
}
.popupClose-seek:hover {
    background-color: #ccc;
}
.popupClose-religion {
    background-color: #fff;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: 1px;
    right: 5px;
    font-size: 15px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
}
.popupClose-religion:hover {
    background-color: #ccc;
}
.popupClose-familymember {
    background-color: #fff;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: 1px;
    right: 5px;
    font-size: 15px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
}
.popupClose-familymember:hover {
    background-color: #ccc;
}
.familymember-popup {
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
}
.education-popup {
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
}
.smoking-popup{
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
}
.drinking-popup{
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
}
.political-popup{
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
}
.seeking-popup{
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
}
.religion-popup{
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
}
h3.txt-head-color {
    color: #33b573;
    }
    span.toggle-handle.btn.btn-default {
        background-color: #c3c3c3;
        }
        .btn-default.active.toggle-off{
            background-color: white;
            -webkit-box-shadow: none;
            box-shadow: none;
    }
    label.btn.btn-primary.toggle-on{
            background-color: #22b14c !important;
            -webkit-box-shadow: none;
            box-shadow: none;
    }
    .location_pos {
        margin-top: 10px;
        margin-bottom: 10px;
        padding: 3px;
        border-style: solid;
        border-color: #eeeeee;
        width: 260px;
        background-color: white;
    }
    .location_pos select {
        border: none;
        font-size: 16px;
    }
    .toggle.btn.btn-default.off {
        border: solid 2px #3333;
    }
    .toggle.btn.btn-default.off {
        width: 81.5px !important;
    }
    .toggle.btn.btn-primary {
        width: 81.5px !important;
    }
    .loc_so_dis {
       padding-left: 30px;
    }

    #content h1{
       display: none;
    }
    
     .p_save_btn{
        text-align: end;
        right: 1.5%;
     } 
     img.carr_edu_notify_img_tab{
        position: absolute;
        left: 246px;
        top: -6px;
        width: 14px;
        display: none;
     } 
     img.carr_edu_notify_img_tab_mob{
        position: absolute;
        left: 134px;
        top: -6px;
        width: 14px;
        display: none;
     } 
        

    @media (max-width: 600px){
        div#content {
            margin-top: 60px;
        }
        
        body{
            overflow-x: hidden;
        }
        h4.profile-title {
            font-size: 25px;
            text-align: center !important;
            margin-left: 35px;
            margin-right: 35px;
            padding-bottom: 10px;
            padding-top: 10px !important;
            width: 208px;
            padding-left: 0px;
        }
        .loc_so_dis{
            display: flex;
            padding-left: 0px !important;
        }
        .location_text{
            padding-right: 0px;
        }
        .location_pos {
            padding-right: 0px; 
        }
        .mobile-button {
          display: block !important;
          width: 100%;
          margin-left: 0px;
          padding-top: 1.5rem;
          padding-bottom: 1rem;
          background-color: #d3d3d3;
        }
        .button_area {
          display: none;
        }

        .button-display {
          display: flex;
          padding-left: 0px;
          padding-right: 0px;
        }
        .p_car_space{
            padding-left: 0px;
            padding-right: 0px;
            text-align: center;
        }
        .p_gen_space{             
            padding-left: 0px;
            padding-right: 0px;
            text-align: center;
        }
        .p_name_desk{
            display: none;
        }
        .p_name_mob{
            display: block;
        }
        .p_name{
            padding-left: 7px;
        }
        .p-userid,.p-fammem,.p-sex,.p-phone,.p-email,.p-hometown,.p-current-city,.p-height,.p-drinking,.p-edu-level,.p-smoking,.p-political,.p-seeking,.p-religion,.p-language,.p-hobby,.p-dob,.p-mstatus,.p_bio_info,.p_pass_text,.p_pass_again_text{
            text-align:left;
        }
        .p-userid label.control-label,.p-fammem label.control-label,.p-sex label.control-label,.p-phone label.control-label,.p-email label.control-label,.p-hometown label.control-label,.p-current-city label.control-label,.p-height label.control-label,.p-drinking label.control-label,.p-smoking label.control-label,.p-seeking label.control-label,.p-religion label.control-label,.p-language label.control-label,.p-hobby label.control-label,.p-dob label.control-label,.p-mstatus label.control-label {
            padding-top: 0px;
            padding-bottom: 0px;
        }
        .user_padd{
            padding-left: 15px;
        }
        
        .loc_so_dis {
            padding-left: 0px;
            padding-right: 0px;
        }
        .loc_so_dis{
        margin-left: 30px;
    }
    .p_name,.p-fammem,.p-phone,.p-dob,.p-email,.p-hobby,.p-hometown,.p-current-city,.p-height,.p-drinking,.p-edu-level,.p-smoking,.p-political,.p-seeking,.p-religion,.p-language,.p-sex,.p-mstatus {
        background-color: white;
        margin-left: 16px;
        margin-top: 15px;
        margin-bottom: 0px;
    }
    .p-phone,.p-dob,.p-email,.p-hobby,.p-hometown,.p-current-city,.p-height,.p-drinking,.p-edu-level,.p-smoking,.p-political,.p-seeking,.p-religion,.p-language,.p-sex,.p-mstatus{
        padding-left: 5px;
    }
    .p-userid{
        background-color: white;
        margin-left: 16px;
        margin-bottom: 0px;
    }
    .p_desc div.col-sm-10 textarea,.p_pass div.col-sm-10 input,.p_pass_again div.col-sm-10 input{
        width: 89%;
        margin-left: 15px !important;
    }
    .p_bio_info,.p_pass_text,.p_pass_again_text {
        margin-left: 16px;
        margin-bottom: 0px;
        background-color: white;
        padding-left: 5px;
        padding-right: 0px;
    }
    .p_desc,.p_pass,.p_pass_again{
        margin-left: 15px;
    }
    .p_bio_info{       
        width: 125px;
    }
    .p_pass_text{
        width: 80px;
    }
    .p_pass_again_text{
        width: 120px;
    }
    .p_name{
        width: 80px;
    }
    .p-userid{        
        width: 62px;
    }
    .p-fammem,.p-hometown{
        width: 116px;
    }
    .p-phone {
        width: 133px;
    }
    .p-dob,.p-current-city{
        width: 120px;
    }
    .p-email,.p-height {
        width: 75px;
    }
    .p-hobby,.p-mstatus {
        width: 80px;
    }
    .p-drinking {
        width: 87px;
    }
    .p-edu-level{
        width: 140px;
    }
    .p-smoking {
        width: 100px;
    }
    .p-political{
        width: 125px;
    }
    .p-language{
        width: 110px;
    }
    .p-seeking,.p-religion {
        width: 95px;
    }
    .so_sta_text{
        padding-top: 6px;
    }
    .p-sex{
        width: 65px;
    }
    .col-md-4.cm_inch_pos div.selector div.selection {
        padding-left: 30px;
    }
    .p_save_btn{
        right: 4%;
     } 
    
}
@media (min-width: 180px) and (max-width: 320px) {
    h4.profile-title {
            font-size: 20px;
            margin-left: 30px;
            margin-right: 30px;
            width: 178px;
        }
    .location_text.location-tracker {
        padding-left: 0px;
    }
    .location_text.location-tracker p {
        font-size: 9px !important;
    }
    .so_sta_text {
        font-size: 8px !important;
    }
    .location_pos div.col-md-5.col-sm-6 {
        padding-left: 0px;
        padding-right: 24px;
    }
    .location_pos {
        width: 40%;
    } 
    .location_text p i.fas.fa-sort-down {
        font-size: 18px !important;
    }
    .p_save_btn{
        right: 1.5%;
     }
     img.carr_edu_notify_img_tab_mob {
        left: 124px;
    }
}
@media (min-width: 321px) and (max-width: 375px){
    .location_pos {
        width: 44%;
    } 
    .location_text.location-tracker {
        padding-left: 0px;
    }
    .location_pos div.loc_track_pad{
        padding-left: 3px;
    }  
    
}
@media (min-width: 361px) and (max-width: 375px){
    img.carr_edu_notify_img_tab_mob {
        left: 137px;
    } 
}
@media (min-width: 376px) and (max-width: 414px){
    .location_pos {
        width: 45%;
    }
    img.carr_edu_notify_img_tab_mob {
        left: 147px;
    } 

}
    
    
    
</style>
<script type="text/javascript">
    $(document).ready(function () {       
        $('.location-tracker-option p').click(function(){
            $('.location-tracker-option p').removeClass('selected');
            $(this).addClass('selected');
            $('input[name="location_visiable"]').val($(this).attr('visiable'));
        }); 
        
        $('.open_drop_down_names').click(function(){
        		$('.bg_grey_2').slideToggle();
        });

        var inputDelayTimer;
        $('#familymember_name').keyup(function(){
            clearTimeout(inputDelayTimer);
            var kw=$(this).val();
            inputDelayTimer=setTimeout(function() {
                $.ajax({
                    type: "POST",
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    data: {
                        kw: kw,
                        action: 'get_friends'
                    },
                    dataType: 'json',
                    success: function(data) {
                        $('.familymember-options').empty();
                        for(var i in data) {
                            $('.familymember-options').append('<li>'+data[i]['user_login']+'</li>');
                        }
                    }
                });
            }, 500);
        });

        $('.familymember-options').on('click', 'li', function(){
            var familymember=$('#familyMember').val();
            if(familymember.indexOf($(this).text())!=-1)
                return;
            if(!!familymember) {
                $('#familyMember').val(familymember+','+$(this).text());
            }
            else {
                $('#familyMember').val($(this).text());
            }
            $('.pop-familymember').hide();
        });

        $(".familymember-popup").click(function(){
           $('.pop-familymember').show();
        });
        $(".education-popup").click(function(){
           $('.pop-edu').show();
        });
        $(".smoking-popup").click(function(){
           $('.pop-smoking').show();
        });
        $(".drinking-popup").click(function(){
           $('.pop-drinking').show();
        });
        $(".political-popup").click(function(){
           $('.pop-political').show();
        });
        $(".seeking-popup").click(function(){
           $('.pop-seek').show();
        });
        $(".religion-popup").click(function(){
           $('.pop-religion').show();
        });
        $('.popupClose-familymember').click(function(){
            $('.pop-familymember').hide();
        });
        $('.popupClose-edu').click(function(){
            $('.pop-edu').hide();
        });
        $('.popupClose-drinking').click(function(){
            $('.pop-drinking').hide();
        });
        $('.popupClose-smo').click(function(){
            $('.pop-smoking').hide();
        });
        $('.popupClose-pol').click(function(){
            $('.pop-political').hide();
        });
        $('.popupClose-seek').click(function(){
            $('.pop-seek').hide();
        });
        $('.popupClose-religion').click(function(){
            $('.pop-religion').hide();
        });
		
		
		
        jQuery('.world-cities-chosen').select2({
            ajax: {
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                dataType: 'json',
                data: function(params) {
                    var query= {
                        action: 'get_world_cities',
                        page: params.page || 1,
                        search: params.term
                    }
                    return query;
                },
                delay: 1500,
                processResults: function(data) {
                    return data;
                }
            }
        });

        jQuery('[name="language[]"]').select2();

        <?php if ((empty($careers)) && (empty($educations))) { ?>
                $("img.carr_edu_notify_img_tab").show();
                $("img.carr_edu_notify_img_tab_mob").show();
        <?php } ?>
    });
</script>

<script type="text/javascript">
    $("#edu_select p").click(function(evt){
        var education = $(this).text();    
        $("#edu_level").val(education);
        $('.pop-edu').hide();
    });
    $("#drink_select p").click(function(evt){
        var drink = $(this).text();    
        $("#drinking").val(drink);
        $('.pop-drinking').hide();
    });
    $("#smoke_select p").click(function(evt){
        var smoke = $(this).text();    
        $("#smoking").val(smoke);
        $('.pop-smoking').hide();
    });
    $("#pol_select p").click(function(evt){
        var political = $(this).text();    
        $("#political_view").val(political);
        $('.pop-political').hide();
    });
    $("#seek_select p").click(function(evt){
        var seek = $(this).text();    
        $("#seeking").val(seek);
        $('.pop-seek').hide();
    });
    $("#religion_select p").click(function(evt){
        var religion = $(this).text();    
        $("#religion").val(religion);
        $('.pop-religion').hide();
    });
    var pbirthtodaysDate = new Date(); 
    var pbirthyear = pbirthtodaysDate.getFullYear();                        
    var pbirthmonth = ("0" + (pbirthtodaysDate.getMonth() + 1)).slice(-2); 
    var pbirthday = ("0" + pbirthtodaysDate.getDate()).slice(-2);           

    var pbirthmaxDate = (pbirthyear +"-"+ pbirthmonth +"-"+ pbirthday); 
    $('.p_dob_input input').attr('max',pbirthmaxDate);

</script>
<style type="text/css">
    .selector {
  width: 80px;
  margin-right: 10px;
  height: 40px;
  border: 1px #252222 solid;
  display: inline-block;
  vertical-align: top;
  text-align: center;
  line-height: 40px;
  color: #252222;
  border-radius: 5px;
  font-family: 'Lato', sans-serif;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.selection {
  animation-duration: 100ms;
}

.upArrow {
  width: 0;
  height: 0;
  border-style: solid;
  border-width: 0 5px 10px 5px;
  position: absolute;
  top: 16px;
  left: 20px;
  cursor: pointer;
  transform: rotate(270deg);
}

.downArrow {
  width: 0;
  height: 0;
  border-style: solid;
  border-width: 10px 5px 0 5px;
  position: absolute;
  top: 16px;
  left: 80px;
  transform: rotate(270deg);
  cursor: pointer;
}

.upWhiteArrow {
  border-color: transparent transparent #b33838 transparent;
}

.upGreyArrow {
  border-color: transparent transparent #777777 transparent;
}

.downWhiteArrow {
  border-color: #b33838 transparent transparent transparent;
}

.downGreyArrow {
  border-color: #777777 transparent transparent transparent;
}
@media (max-width: 600px){
    .cm_inch_pos{
        margin-top: -40px;
        position: relative;
        left: 50%;
    }
    .selector{
      border: none !important;  
    }
}
</style>
<script type="text/javascript">
    "use strict";

var _class, _temp;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Input = function () {
    function Input(input, placeholder) {
        _classCallCheck(this, Input);

        this.isFocused = false;
        this.size = 0;
        this.animation = "zoomIn";
        $(input).addClass("input");
        this.$element = $(document.createElement("div"));
        this.$element.addClass("textZone");
        this.$element.attr("tabindex", 0);
        $(input).append(this.$element);
        this.cursor = new Cursor(this);
        this.setEvents();
        Keyboard.readCharacters(this);
        Keyboard.readSpecialCharacters(this);
        this.placeholder = new Placeholder(placeholder, this);
    }

    Input.prototype.setEvents = function setEvents() {
        var input = this;

        this.$element.on("click", function (event) {
            input.focus();
            event.stopPropagation();
        });

        $(document).on("click", function (event) {
            input.unfocus();
        });
    };

    Input.prototype.focus = function focus() {
        if (this.size == 0) {
            this.$element.prepend(this.cursor.$element);
        } else {
            this.cursor.$element.insertAfter(this.$element.children().last());
        }
        this.cursor.show();
        this.isFocused = true;
    };

    Input.prototype.unfocus = function unfocus() {
        if (this.size == 0) {
            this.placeholder.show();
        }
        this.cursor.hide();
        this.isFocused = false;
    };

    Input.prototype.write = function write(character) {
        this.size++;
        this.placeholder.hide();
        character.setEvents(this);
        character.$element.insertAfter(this.cursor.$element);
        character.animate(this.animation);
        this.cursor.move("right");
    };

    Input.prototype.erase = function erase() {
        var last = this.cursor.$element.prev();
        if (last.length && this.size > 0) {
            this.size--;
            this.cursor.move("left");
            last.remove();
            if (this.size == 0) {
                this.placeholder.show();
            }
        }
    };

    Input.prototype.suppress = function suppress() {
        var next = this.cursor.$element.next();
        if (next.length && this.size > 0) {
            this.size--;
            next.remove();
            if (this.size == 0) {
                this.placeholder.show();
            }
        }
    };

    return Input;
}();

var Placeholder = function () {
    function Placeholder(placeholder, input) {
        _classCallCheck(this, Placeholder);

        this.input = input;
        this.$element = $(document.createElement("div"));
        this.$element.text(placeholder);
        this.$element.addClass("placeholder");
        this.show();
    }

    Placeholder.prototype.show = function show() {
        this.input.$element.append(this.$element);
    };

    Placeholder.prototype.hide = function hide() {
        this.$element.remove();
    };

    return Placeholder;
}();

var Keyboard = (_temp = _class = function () {
    function Keyboard() {
        _classCallCheck(this, Keyboard);
    }

    Keyboard.readCharacters = function readCharacters(input) {
        input.$element.on("keypress", function (event) {
            event.preventDefault();
            input.write(new Character(String.fromCharCode(event.which)));
        });
    };

    Keyboard.readSpecialCharacters = function readSpecialCharacters(input) {
        input.$element.on("keydown", function (event) {
            switch (event.keyCode) {
                case Keyboard.backspace:
                    event.preventDefault();
                    input.erase();
                    break;
                case Keyboard.leftArrow:
                    input.cursor.move("left");
                    break;
                case Keyboard.rightArrow:
                    input.cursor.move("right");
                    break;
                case Keyboard.suppress:
                    input.suppress();
                    break;
                case Keyboard.top:
                    input.cursor.goTo("top");
                    break;
                case Keyboard.end:
                    input.cursor.goTo("end");
                    break;
                default:
                    break;
            }
        });
    };

    return Keyboard;
}(), _class.space = 32, _class.backspace = 8, _class.leftArrow = 37, _class.rightArrow = 39, _class.suppress = 46, _class.top = 36, _class.end = 35, _temp);

var Cursor = function () {
    function Cursor(input) {
        _classCallCheck(this, Cursor);

        this.$element = $(document.createElement("div"));
        this.$element.addClass("cursor");
        this.$element.addClass("hidden");
        input.$element.prepend(this.$element);
    }

    Cursor.prototype.show = function show() {
        this.$element.removeClass("hidden");
    };

    Cursor.prototype.hide = function hide() {
        this.$element.addClass("hidden");
    };

    Cursor.prototype.move = function move(direction) {
        var offSet = this.$element.get(0).offsetLeft;
        var textZone = this.$element.parent();

        if (direction == "right") {
            var next = this.$element.next();
            this.$element.insertAfter(next);
            if (offSet > textZone.width() * 0.99) {
                var scroll = textZone.scrollLeft();
                textZone.animate({ scrollLeft: scroll + '100' }, 1000);
            }
        } else if (direction == "left") {
            var prev = this.$element.prev();
            this.$element.insertBefore(prev);
        }
    };

    Cursor.prototype.goTo = function goTo(point) {
        if (point == "top") {
            this.$element.parent().prepend(this.$element);
        } else if (point == "end") {
            this.$element.parent().append(this.$element);
        }
    };

    return Cursor;
}();

var Character = function () {
    function Character(character) {
        _classCallCheck(this, Character);

        this.$element = $(document.createElement("div"));
        if (character != " ") {
            this.$element.addClass("character");
            this.$element.text(character);
        } else {
            this.$element.addClass("space");
        }
    }

    Character.prototype.setEvents = function setEvents(input) {
        var character = this;
        this.$element.on("click", function (event) {
            input.cursor.$element.insertBefore(character.$element);
            if (!input.isFocused) {
                input.cursor.show();
            }
            event.stopPropagation();
        });
    };

    Character.prototype.animate = function animate(animation) {
        this.$element.css("animation", animation + " 500ms, colorTransition 500ms");
    };

    return Character;
}();

var Selector = function () {
    function Selector(selector, options, defaultOption, callback) {
        _classCallCheck(this, Selector);

        this.$element = $(selector);
        this.$element.addClass("selector");
        this.$selection = $(document.createElement("div"));
        this.$selection.addClass("selection");
        var i = 0;
        this.current = i;
        this.$selection.text(options[i]);
        for (i = 0; i < options.length; i++) {
            if (options[i] == defaultOption) {
                this.current = i;
                this.$selection.text(options[i]);
            }
        }
        this.$element.append(this.$selection);
        this.options = options;
        this.setEvents();
        this.setArrows();
        this.animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.callback = callback;
        this.selecting = false;
        callback(options[this.current]);
    }

    Selector.prototype.setArrows = function setArrows() {
        var selector = this;
        this.$upArrow = $(document.createElement("div"));
        this.$upArrow.addClass("upArrow");
        this.$element.append(this.$upArrow);
        this.$upArrow.on("click", function () {
            if (!selector.isFirst() && !selector.selecting) {
                selector.select("Down");
            }
        });
        this.$downArrow = $(document.createElement("div"));
        this.$downArrow.addClass("downArrow");
        this.$element.append(this.$downArrow);
        this.$downArrow.on("click", function () {
            if (!selector.isLast() && !selector.selecting) {
                selector.select("Up");
            }
        });
        this.updateArrows();
    };

    Selector.prototype.setEvents = function setEvents() {
        var selector = this;
        this.$element.on("wheel", function (event) {
            if (event.originalEvent.deltaY > 0) {
                if (!selector.isLast() && !selector.selecting) {
                    selector.select("Up");
                }
            } else {
                if (!selector.isFirst() && !selector.selecting) {
                    selector.select("Down");
                }
            }
        });
    };

    Selector.prototype.isFirst = function isFirst() {
        return this.current == 0;
    };

    Selector.prototype.isLast = function isLast() {
        return this.current == this.options.length - 1;
    };

    Selector.prototype.select = function select(direction) {
        this.selecting = true;
        this.current = direction == "Up" ? this.current + 1 : this.current - 1;
        var selector = this;
        this.$selection.addClass("fadeOut" + direction).on(this.animationEnd, function () {
            selector.$selection.removeClass("fadeOut" + direction);
            selector.$selection.text(selector.options[selector.current]);
            selector.$selection.addClass("fadeIn" + direction).on(selector.animationEnd, function () {
                selector.$selection.removeClass("fadeIn" + direction);
                selector.callback(selector.options[selector.current]);
                selector.selecting = false;
                selector.updateArrows();
            });
            $('input[name="height_unit"]').val($('#selector .selection').text());
        });
    };

    Selector.prototype.updateArrows = function updateArrows() {
        this.$upArrow.removeClass("upWhiteArrow");
        this.$upArrow.removeClass("upGreyArrow");
        this.$downArrow.removeClass("downWhiteArrow");
        this.$downArrow.removeClass("downGreyArrow");

        if (this.current == 0) {
            this.$upArrow.addClass("upGreyArrow");
            if (this.options.length < 2) {
                this.$downArrow.addClass("downGreyArrow");
            } else {
                this.$downArrow.addClass("downWhiteArrow");
            }
        } else if (this.current == this.options.length - 1) {
            this.$upArrow.addClass("upWhiteArrow");
            this.$downArrow.addClass("downGreyArrow");
        } else {
            this.$upArrow.addClass("upWhiteArrow");
            this.$downArrow.addClass("downWhiteArrow");
        }
    };

    return Selector;
}();

var input = new Input("#myInput", "Try me!");
new Selector("#selector", ["CM", "INCH"], "<?php echo ($_POST['height_unit']=='INCH' || $current_user->height_unit=='INCH')?'INCH':'CM'; ?>", function (selection) {
    input.animation = selection;
});
</script>

<script type="text/javascript"> 
	/*jQuery(document).ready(function($){
		$('.for_btnedit').click(function(){
			$('.content_afterclick').show();
			$('.hide_click').hide();
		});
		$('.cancel_div').click(function(){
			$('.content_afterclick').hide();
			$('.hide_click').show();
		});
	})*/
</script>

<style type="text/css">
    #footer {
        display: none;
    }
</style>