<div class="p-4">
    <h4 class="font-lg text-center pt-3 mb-4">Available: $<?php echo $balance;?></h4>	<?php if(empty($verified) || $verified!=2) { ?>	<div style="text-align: center;">		<a href="<?php echo home_url("/user/{$username}/?profiletab=verification"); ?>">Sorry, your account is not yet verified. Please verify your profile now.</a>	</div>	<?php } ?>
    <div class="d-flex flex-column flex-sm-row" style="max-width: 550px;margin: 0 auto;">
        <div class="flex-half p-2">
            <a id="GetPaidNow" style="cursor: pointer;" class="btn btn-submit-org btn-block">Get Paid Now </a>
        </div>
        <div class="flex-half p-2">
            <a href="<?php echo home_url("/user/{$username}/?profiletab=settlement&action=add_settlement_method"); ?>" class="btn btn-submit-blue btn-block">Add settlement method</a>
        </div>
    </div>
</div>
<script>
    new Tooltip(document.getElementById('GetPaidNow'),{title:'Your commission will be redeemed at the end of the next month.',trigger:'click',placement:'top',container:'body',template:'<div class="popper-tooltip" role="tooltip"><div class="tooltip__arrow"></div><div class="tooltip__inner"></div></div>'});
</script>
<style type="text/css">
    
    #content h1{
       display: none;
    }
    @media (max-width: 600px){
    div#content {
            margin-top: 60px;
        }
        
        body{
            overflow-x: hidden;
        }
    }
</style>
