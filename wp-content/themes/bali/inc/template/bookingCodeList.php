<style>

    .rating {
        display: inline-block;
        position: relative;
        height: 50px;
        line-height: 50px;
        font-size: 50px;
    }

    .rating label {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        cursor: pointer;
    }

    .rating label:last-child {
        position: static;
    }

    .rating label:nth-child(1) {
        z-index: 5;
    }

    .rating label:nth-child(2) {
        z-index: 4;
    }

    .rating label:nth-child(3) {
        z-index: 3;
    }

    .rating label:nth-child(4) {
        z-index: 2;
    }

    .rating label:nth-child(5) {
        z-index: 1;
    }

    .rating label input {
        position: absolute;
        top: 0;
        left: 0;
        opacity: 0;
    }

    .rating label .icon {
        float: left;
        color: transparent;
    }

    .rating label:last-child .icon {
        color: #000;
    }

    .rating:not(:hover) label input:checked ~ .icon,
    .rating:hover label:hover input ~ .icon {
        color: #09f;
    }

    .rating label input:focus:not(:checked) ~ .icon:last-child {
        color: #000;
        text-shadow: 0 0 5px #09f;
    }
    .star-checked{
        color: #fc8c14;
    }

    a.um-profile-photo-img{
        display: none!important;
    }
    .p-4 {
      background: #fff;
    }
  .booking-code-list-table tr th {
    background-color: rgba(93, 202, 197, 0.31)!important;
    color: #0d816a;
    font-size: 12px;
    font-weight: bold;
  }
    .booking-code-list-table tr td {
      border-bottom: 2px solid rgba(93, 202, 197, 0.31);
      vertical-align: baseline !important;
    }
  .booking-code-link {
    display: block;
    width: 100px;
    height: 100px;
    padding: 40px 5px;
    text-align: center;
    vertical-align: middle;
    border-radius: 3px;
    background-color: #eeeeee;
    font-size: 16px;
    font-weight: bold;
    color: #ff6633;

  }
  .btn-paid {
    padding: 5px 20px!important;
    display: block!important;
    width: 100%!important;
    border-radius: 3px!important;
    background-color: #ff6633!important;
    color: #fff!important;
    text-align: center;
    opacity: 1!important;
  }
    .btn-not-paid {
      padding: 5px 20px!important;
      display: block!important;
      width: 100%!important;
      border-radius: 3px!important;
      background-color:  #1cba9a!important;
      color: #fff!important;
      opacity: 1!important;
    }
    .btn-paid a ,.btn-not-paid a {
      color: #fff!important;
    }
    .booking-code-list-table tr td p {
      margin-bottom: 0;
      line-height: 1.2;
      white-space: nowrap;
      text-align: left;
    }
  .delete-bookingcode {
    display: inline-block;
    width: 32px;
    height: 32px;
    border-radius: 16px;
    box-shadow: 0px 2px 0 0 rgba(93, 202, 197, 0.31);
    background-color: #ff6633;
    text-decoration: none;
    text-align: center;
    line-height: 32px;
    color: #fff;
  }
    .delete-bookingcode:hover,.delete-bookingcode:focus,.delete-bookingcode:active {
      background-color: #e15931;
      color: #fff;
    }
    .copy_text{
      display:none;
    }
    img.copy_paste_img {
    width: 25px;
    position: relative;
    bottom: 30px;
    right: -65px;
    cursor: pointer;
}
.tooltip_copy .tooltip_copy_text {
  visibility: hidden;
  width: 100px;
  font-size: initial;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 40px;
  padding: 5px 0;
  position: absolute;
  z-index: 1;
  margin-top: -138px;
  margin-left: -20px;
  opacity: 0;
  transition: opacity 0.3s;
}
.tooltip_copy .tooltip_copy_text::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

.tooltip_copy:hover .tooltip_copy_text {
  visibility: visible;
  opacity: 1;
}
#content h1{
       display: none;
    }
button.pro_book_modify {
    margin-top: 15px;
    color: white;
    background-color: #22b14c;
    border-style: none;
    font-size: 16px;
}
.book_delete_tooltip .book_delete_tooltiptext {
  visibility: hidden;
  width: 200px;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;
  position: absolute;
  z-index: 1;
  margin-left: -107px;
  margin-top: -114px;
  opacity: 0;
  transition: opacity 0.3s;
}
.book_delete_tooltip .book_delete_tooltiptext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}
.book_delete_tooltip:hover .book_delete_tooltiptext {
  visibility: visible;
  opacity: 1;
}
@media (max-width: 600px) {
    div#content {
            margin-top: 60px;
        }
        
        body{
            overflow-x: hidden;
        }
        i.fa.fa-star {
          font-size: 7px;
        }
        .rating-stars.text-center a {
          font-size: 9px;
        }
        .success-box-2 {
          margin-top: 10px;
        }
        .booking-code-list-table tr td p {
          font-size: 13px;
        }
        table.table.booking-code-list-table tr th {
          vertical-align: middle;
       }

  }

</style>
<div class="p-4 clearfix">
  <div class="table-responsive">
    <table class="table booking-code-list-table">
      <tr>
        <th> Booking Code</th>
        <th> Journey Cost</th>
        <th> Commission</th>
        <th> Payment Status</th>
        <?php if(current_user_can('um_clients')) { 
        echo "<th> Feedback From Sellers</th>";
        }
        else{
          echo "<th> Feedback From Buyers</th>";
        }
        ?>
        <?php if(current_user_can('um_clients')) { 
        echo "<th> Feedback To Sellers</th>";
        }
        else{
          echo "<th> Feedback To Buyers</th>";
        }
        ?>
        <th> Activities</th>
        <th></th>
      </tr>

        <?php
        $current_user = wp_get_current_user();
        $userName = isset($current_user->user_login) ? $current_user->user_login
          : '';
        $userID = isset($current_user->ID) ? $current_user->ID : '';
        /* print_r($bookingCodeList);
          exit;
         */
        foreach ($bookingCodeList as $row) {
            $af_ratting = 0;
            $ac_ratting = 0;
            $rattingDataByTour = $wpdb->get_results(
              "SELECT * FROM agent_feedback WHERE af_tour_id = '$row->tour_id'",
              ARRAY_A
            );
            if (!empty($rattingDataByTour)) {
                $af_ratting = $rattingDataByTour[0]['af_ratting'];
            }

            $rattingDataByTourForMine = $wpdb->get_results(
              "SELECT * FROM agent_client_feedback WHERE ac_tour_id = '$row->tour_id'",
              ARRAY_A
            );
            if (!empty($rattingDataByTourForMine)) {
                $ac_ratting = $rattingDataByTour[0]['ac_ratting'];
            }


            $totalComment = $wpdb->get_var(
              "SELECT COUNT(*) FROM `social_comments` WHERE scm_tour_id = '$row->tour_id'"
            );

            $totalLikes = $wpdb->get_var(
              "SELECT COUNT(*) FROM `social_connect` WHERE sc_type = 0 AND sc_tour_id = '$row->tour_id'"
            );

            $totalDislikes = $wpdb->get_var(
              "SELECT COUNT(*) FROM `social_connect` WHERE sc_type = 1 AND sc_tour_id = '$row->tour_id'"
            );

            //get share count
            $share_count = $wpdb->get_var(
              "SELECT `meta_value` FROM `wp_tourmeta` WHERE `tour_id`='{$row->tour_id}' AND `meta_key`='sharecount'"
            );
            if (empty($share_count)) {
                $share_count = 0;
            }
            ?>
          <tr>
            <td  valign="middle">
                <p id="copy_paste<?php echo $row->tour_id; ?>" class="copy_text">
                <?php echo site_url().'/tour-details/?bookingcode=BALI'.$row->tour_id; ?></p>
              <a class="booking-code-link"  href="<?php echo site_url().'/tour-details/?bookingcode=BALI'
                .$row->tour_id; ?>" target="_blank"><?php echo 'BALI'
                    .$row->tour_id; ?></a>
                    <a class="tooltip_copy"><img class="copy_paste_img" src="https://www.travpart.com/English/wp-content/themes/bali/images/copy_paste.png" onclick="copyToClipboard('#copy_paste<?php echo $row->tour_id; ?>')">
                    <span class="tooltip_copy_text">Copy and paste it to your customers</span>
                    </a>

                <?php if (!empty($row->post_id)) { ?>
                  <br/>
                  <a href="<?php echo site_url()
                    .'/newpost/?fep_action=edit&fep_id='.$row->post_id; ?>"
                     target="_blank">(complete detail)</a>
                <?php } ?>
            </td>
            <td>
            <span class="change_price"
                  or_pic="<?php echo intval($row->total); ?>">Rp<span
                  class="price_span"><?php echo intval(
                      $row->total
                    ); ?></span></span>
            </td>
            <td valign="middle">
            <span class="change_price" or_pic="<?php echo round(
              intval($row->total) * 0.08
            ); ?>">Rp<span class="price_span"><?php echo round(
                      intval($row->total) * 0.08
                    ); ?></span></span>
            </td>
            <td valign="middle">
                <?php if ($row->confirm_payment == 1) { ?>
                  <span class="btn btn-primary btn-paid" disabled>Paid</span>
                <?php } else { ?>
                  <span class='btn btn-primary btn-not-paid' disabled> <a
                        href="<?php echo site_url()
                          .'/English/total-cost/?tourid='
                          .$row->tour_id; ?>"
                        target="_blank">Not Paid yet</a> </span>
                <?php } ?>
            </td>
            <td valign="middle" class='rating-widget'>
              <div class='rating-stars text-center'>
                <ul id='stars' data-id='<?php echo $bookcode; ?>'>
                    <?php if ($af_ratting == 1) { ?>
                      <i class="fa fa-star star-checked"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                    <?php } else {
                        if ($af_ratting == 2) { ?>
                          <i class="fa fa-star star-checked"></i>
                          <i class="fa fa-star star-checked"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                        <?php } else {
                            if ($af_ratting == 3) { ?>
                              <i class="fa fa-star star-checked"></i>
                              <i class="fa fa-star star-checked"></i>
                              <i class="fa fa-star star-checked"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                            <?php } else {
                                if ($af_ratting == 4) { ?>
                                  <i class="fa fa-star star-checked"></i>
                                  <i class="fa fa-star star-checked"></i>
                                  <i class="fa fa-star star-checked"></i>
                                  <i class="fa fa-star star-checked"></i>
                                  <i class="fa fa-star"></i>
                                <?php } else {
                                    if ($af_ratting == 5) { ?>
                                      <i class="fa fa-star star-checked"></i>
                                      <i class="fa fa-star star-checked"></i>
                                      <i class="fa fa-star star-checked"></i>
                                      <i class="fa fa-star star-checked"></i>
                                      <i class="fa fa-star star-checked"></i>
                                    <?php } else {
                                        ?>
                                      <h5>No Feedback received Yet</h5>
                                    <?php }
                                }
                            }
                        }
                    }
                    ?>
                </ul>
              </div>
              <div class='success-box'>
                <div class='clearfix'></div>
              </div>
            </td>

            <td valign="middle" class='rating-widget'>
              <div class='rating-stars text-center'>
                  <?php
                  if (empty($rattingDataByTour)) {
                      ?>
                    You are not able to give feedback yet
                      <?php
                  } else {
                      if (!empty($rattingDataByTourForMine)) {
                          ?>
                        <ul id='stars' data-id='<?php echo $bookcode; ?>'>
                            <?php if ($ac_ratting == 1) { ?>
                              <i class="fa fa-star star-checked"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                            <?php } else {
                                if ($ac_ratting == 2) { ?>
                                  <i class="fa fa-star star-checked"></i>
                                  <i class="fa fa-star star-checked"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                <?php } else {
                                    if ($ac_ratting == 3) { ?>
                                      <i class="fa fa-star star-checked"></i>
                                      <i class="fa fa-star star-checked"></i>
                                      <i class="fa fa-star star-checked"></i>
                                      <i class="fa fa-star"></i>
                                      <i class="fa fa-star"></i>
                                    <?php } else {
                                        if ($ac_ratting == 4) { ?>
                                          <i class="fa fa-star star-checked"></i>
                                          <i class="fa fa-star star-checked"></i>
                                          <i class="fa fa-star star-checked"></i>
                                          <i class="fa fa-star star-checked"></i>
                                          <i class="fa fa-star"></i>
                                        <?php } else {
                                            if ($ac_ratting == 5) { ?>
                                              <i class="fa fa-star star-checked"></i>
                                              <i class="fa fa-star star-checked"></i>
                                              <i class="fa fa-star star-checked"></i>
                                              <i class="fa fa-star star-checked"></i>
                                              <i class="fa fa-star star-checked"></i>
                                            <?php } else {
                                                ?>
                                              <h5>No Feedback received Yet</h5>
                                            <?php }
                                        }
                                    }
                                }
                            }
                            ?>
                        </ul>
                          <?php
                      } else {
                          ?>
                        <a href="<?php echo site_url()
                          .'/client-feedback-2/?tour_id='.$row->tour_id; ?>"
                           target="_blank">Give Feedback</a>
                          <?php
                      }
                  }
                  ?>

              </div>
              <div class='success-box-2'>
                <div class='clearfix'></div>
              </div>
            </td>
            <td valign="middle">
              <p>Comments : <?php echo $totalComment; ?></p>
              <p>Likes : <?php echo $totalLikes; ?></p>
              <p>Dislike : <?php echo $totalDislikes; ?></p>
              <p>Viewers : <?php echo empty($row->post_id)
                    ? '0'
                    : getPostViews(
                      $row->post_id
                    ); ?></p>
              <p>Shares : <?php echo $share_count; ?></p>
            </td>

            <td>
                <?php if ($row->confirm_payment != 1) { ?>
                  <a tour_id="<?php echo $row->tour_id; ?>"
                     class="delete-bookingcode">
                     <div class="book_delete_tooltip">
                    <i class="fa fa-trash"></i>
                    <span class="book_delete_tooltiptext">Are you sure you want to delete this tour ?; unpaid commission balance will be deleted</span>
                  </div>
                  </a>                 
                    
                  <p class="error_msg" style="color: red; display:none;"></p>
                <?php } ?>
                <button class="pro_book_modify">Modify</button>
            </td>
          </tr>
        <?php } ?>
    </table>
  </div>
  <div style="display: block;margin: 0 auto;text-align: center;">
      <?php if (current_user_can('um_travel-advisor')) { ?>
        <a href="<?php echo site_url().'/travcust/'; ?>" target="_blank"
           class="btn btn-default left-btnapp">Add a Tour</a>
      <?php } ?>
  </div>

  <input id="cs_RMB" type="hidden"
         value="<?php echo get_option('_cs_currency_RMD'); ?>"/>
  <input id="cs_USD" type="hidden"
         value="<?php echo get_option('_cs_currency_USD'); ?>"/>
  <input id="cs_AUD" type="hidden"
         value="<?php echo get_option('_cs_currency_AUD'); ?>"/>
  <script>
    jQuery(document).ready(function () {

      $(':radio').change(function () {
        console.log('New star rating: ' + this.value)
      })

      $('.header_curreny').change(function () {

        var full_name = $(this).find('option:selected').attr('full_name')
        var syml = $(this).find('option:selected').attr('syml')

        var curreny = $(this).val()

        if (curreny == 'IDR') {

          $('.change_price').each(function () {
            $(this).
              html(syml + '<span class=\'price_span\'>' + $(this).attr('or_pic') +
                ' </span>')

          })

        } else {
          $('.change_price').each(function () {
            var amount = $(this).attr('or_pic')
            if (syml == '?') {
              $(this).
                html('<span class=\'price_span\'>' +
                  Math.round($('#cs_RMB').val() * amount) + ' </span>' + syml)
            } else {
              $(this).
                html(syml + '<span class=\'price_span\'>' +
                  ($('#cs_' + curreny).val() * Math.round(amount)).toFixed(2) +
                  ' </span>')
            }
          })
        }
      })
      setTimeout('$(".header_curreny").change();', 300)

      var deleteitem
      $('.delete-bookingcode').click(function () {
        deleteitem = $(this)
        $(this).attr('disabled', 'disabled')
        var data = {
          'action': 'delete_bookingcode',
          'tour_id': $(this).attr('tour_id')
        }
        jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', data,
          function (response) {
            if (response.error_msg != undefined && response.error_msg.length >
              1) {
              deleteitem.parent().
                find('.error_msg').
                text('Error:' + response.error_msg)
              deleteitem.parent().find('.error_msg').show()
              deleteitem.removeAttr('disabled')
            } else {
              deleteitem.parent().html('<p>Deleted</p>')
            }
          }, 'json')
      })
    })
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
      }
  </script>

  <style>
    .btn.btn-default.left-btnapp {
      float: left;
      margin-top: 30px;
    }

    .btn.btn-default.right-btnapp {
      float: right;
      margin-top: 30px;
    }

    .wiretransfer {
      text-align: center;
      margin: 0 auto;
      margin-top: 30px;
    }

    .um-14065.um .um-profile-body {
      max-width: 100% !important;
    }

    .um-profile a, .um-profile a:hover {
      text-align: center;
    }

    .um-profile a p {
      font-size: 14px;
      font-weight: 400;
    }

    .um-14065.um .um-profile-body {
      max-width: 600px;
      border: 1px solid #ccc;
    }

    .checked {
      color: orange;
    }

    .clearfix {
      clear: both;
    }

    .text-center {
      text-align: center;
    }

    a {
      color: tomato;
      text-decoration: none;
    }

    a:hover {
      color: #2196f3;
    }

    pre {
      display: block;
      padding: 9.5px;
      margin: 0 0 10px;
      font-size: 13px;
      line-height: 1.42857143;
      color: #333;
      word-break: break-all;
      word-wrap: break-word;
      background-color: #F5F5F5;
      border: 1px solid #CCC;
      border-radius: 4px;
    }


    .success-box {
      padding: 10px 10px;
      border: 1px solid #eee;
      background: #f9f9f9;
    }
    .success-box-2 {
      padding: 10px 10px;
      border: 1px solid #eee;
      background: #f9f9f9;
    }
    .success-box-2 > div {
      vertical-align: top;
      display: inline-block;
      color: #888;
    }

    .success-box img {
      margin-right: 10px;
      display: inline-block;
      vertical-align: top;
    }

    .success-box > div {
      vertical-align: top;
      display: inline-block;
      color: #888;
    }


    /* Rating Star Widgets Style */
    .rating-stars ul {
      list-style-type: none;
      padding: 0;

      -moz-user-select: none;
      -webkit-user-select: none;
    }

    .rating-stars ul > li.star {
      display: inline-block;

    }

    /* Idle State of the stars */
    .rating-stars ul > li.star > i.fa {
      font-size: 1.0em; /* Change the size of the stars */
      color: #ccc; /* Color on idle state */
    }

    /* Hover state of the stars */
    .rating-stars ul > li.star.hover > i.fa {
      color: #FFCC36;
    }

    /* Selected state of the stars */
    .rating-stars ul > li.star.selected > i.fa {
      color: #FF912C;
    }

  </style>
</div>