<?php
global $wpdb;
$careers = $wpdb->get_results("SELECT * FROM `wp_career` WHERE userid=" . um_profile_id());
$educations = $wpdb->get_results("SELECT * FROM `wp_education` WHERE userid=" . um_profile_id());
?>
<div class="button_area">
    <div class="row">
        <div class="col-md-6">
            <a href="<?php echo add_query_arg(array('action' => 'general_setting')); ?>" class="gen-btn-info" role="button">General Profile</a>
            <a href="#" class="gen-btn-info gen-btn-active" role="button">Career &amp; Education</a>
        </div>
    </div>
</div>
<div class="button_area mobile-button">
    <div class="row">
        <div class="col-md-12 button-display">
            <div class="col-md-2 p_gen_space">
                <a href="<?php echo add_query_arg(array('action' => 'general_setting')); ?>" class="gen-btn-info" role="button">General Profile</a>
            </div>
            <div class="col-md-10 p_car_space">
                <a href="#" class="gen-btn-info gen-btn-active" role="button">Career &amp; Education</a>
            </div>
        </div>
    </div>
</div>

<div class="educonarea carrerwraptop">
    <div class="row">
        <div class="col-md-8 edutitle">
            <h3>Career</h3>
            <img class="carr_notify_img" src="https://www.travpart.com/English/wp-content/uploads/2020/03/exclamation.png">
        </div>
        <div class="col-md-4 triggerbtn"> <button popupheadtext="Add Experience" class="trigger triggercareer">+</button></div>
    </div>
    <div class="row">
        <?php foreach ($careers as $career) { ?>
            <div class="carrerwrap">
                <div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/Capture.jpg" /></div>
                <div class="carrerright">
                    <h4 class="job_title"><?php echo $career->job_title; ?></h4>
                    <div class="comname"><?php echo $career->company; ?></div>
                    <?php if ($career->working == 1) { ?>
                        <div class="extime">
                            <span class="start" date="<?php echo date('F j, Y', strtotime($career->start_date)); ?>"><?php echo date('M Y', strtotime($career->start_date)); ?></span>
                            - Present .
                            <?php echo (new DateTime())->diff(new DateTime($career->start_date))->format('%y years %m months'); ?>
                        </div>
                    <?php } else { ?>
                        <div class="extime">
                            <span class="start" date="<?php echo date('F j, Y', strtotime($career->start_date)); ?>"><?php echo date('M Y', strtotime($career->start_date)); ?></span>
                            -
                            <span class="end" date="<?php echo date('F j, Y', strtotime($career->end_date)); ?>"><?php echo date('M Y', strtotime($career->end_date)); ?></span></div>
                    <?php } ?>
                    <div class="exloc"><?php echo $career->location; ?></div>
                    <div class="description">
                        <p>
                            <?php echo str_replace("\n", "<br>", $career->description); ?>
                        </p>
                    </div>
                </div>
                <div class="editbtn editcareer" career="<?php echo $career->id; ?>" working="<?php echo $career->working; ?>">
                    <button popupheadtext="Edit Experience" action="edit_career" class="trigger triggercareer"><i class="fa fa-edit"></i></button>
                </div>
            </div>
        <?php } ?>
        <?php if (empty($careers)) { ?>
            <div class="carrerwrap">
                <p style="margin-left: 20px;">None</p>
            </div>
        <?php } ?>
    </div>

</div>

<div class="educonarea carrerwraptop">
    <div class="row">
        <div class="col-md-8 edutitle">
            <h3>Education</h3>
            <img class="edu_notify_img" src="https://www.travpart.com/English/wp-content/uploads/2020/03/exclamation.png">
        </div>
        <div class="col-md-4 triggerbtn"> <button class="trigger triggeredu" popupheadtext="Add Education">+</button></div>
    </div>
    <div class="row">

        <?php foreach ($educations as $edu) { ?>
            <div class="carrerwrap">
                <div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/Capture.jpg" /></div>
                <div class="carrerright">
                    <h4 class="school"><?php echo $edu->school; ?></h4>
                    <div class="comname">
                        <span class="degree"><?php echo $edu->degree; ?></span>,
                        <span class="area"><?php echo $edu->area; ?></span></div>
                    <div class="extime">
                        <span class="start"><?php echo $edu->startdate; ?></span> -
                        <span class="end"><?php echo $edu->enddate; ?></span>
                    </div>
                    <div class="description">
                        <p>
                            <?php echo str_replace("\n", "<br>", $edu->description); ?>
                        </p>
                    </div>
                </div>
                <div class="editbtn editedu" education="<?php echo $edu->id; ?>">
                    <button popupheadtext="Edit Education" action="edit_education" class="trigger triggeredu"><i class="fa fa-edit"></i></button>
                </div>
            </div>
        <?php } ?>
        <?php if (empty($educations)) { ?>
            <div class="carrerwrap">
                <p style="margin-left: 20px;">None</p>
            </div>
        <?php } ?>

    </div>

</div>

<div class="overlay careerpopup">
    <div class="popup" id="carpopup">
        <div class="popupheader">
            <h2>Add Experience</h2>
            <div class="close">X</div>
        </div>
        <div class="popupbody add-career">
            <div class="fieldwrap">
                <div class="popuplabel">Title *</div>
                <div class="popupfield"><input type="text" name="job_title" placeholder="Ex Manager" /></div>
            </div>
            <div class="fieldwrap">
                <div class="popuplabel">Company *</div>
                <div class="popupfield"><input type="text" name="company" placeholder="Ex Google" /></div>
            </div>
            <div class="fieldwrap">
                <div class="popuplabel">Location *</div>
                <div class="popupfield"><input type="text" name="location" placeholder="Ex NewYork USA" /></div>
            </div>
            <div class="fieldwrap">
                <div class="popupfield checkboxfield"><input type="checkbox" name="working" value="yes" /></div>
                <div class="popuplabel checkboxlabel">I am currently working in this role</div>
            </div>
            <div class="fieldwrap">
                <div class="edudatewrap">
                    <div class="timewrap">
                        <div class="popuplabel">Start Date *</div>
                        <div class="startdatewrap">
                            <input type='text' class="startdate" data-language='en' />
                        </div>
                    </div>
                    <div class="timewrap">
                        <div class="popuplabel">End Date *</div>
                        <div class="startdatewrap">
                            <input type='text' class="enddate" data-language='en' />
                        </div>
                    </div>
                </div>
            </div>
            <div class="fieldwrap">
                <div class="popuplabel">Description *</div>
                <div class="popupfield">
                    <textarea rows="8" name="description"></textarea>
                </div>


            </div>
            <div class="fieldwrap frmbtn">
                <input type="hidden" name="action" value="add_career" />
                <input type="hidden" name="career" />
                <button class="btn btn-primary submit-career">Save</button>
            </div>

        </div>
    </div>
</div>
<div class="overlay educationpopup">
    <div class="popup" id="edupopup">
        <div class="popupheader">
            <h2>Add Education</h2>
            <div class="close">X</div>
        </div>
        <div class="popupbody add-education">
            <div class="fieldwrap">
                <div class="popuplabel">School *</div>
                <div class="popupfield"><input type="text" name="school" placeholder="Ex Miami University" /></div>
            </div>
            <div class="fieldwrap">
                <div class="popuplabel">Degree *</div>
                <div class="popupfield"><input type="text" name="degree" placeholder="Ex Bachelor's" /></div>
            </div>
            <div class="fieldwrap">
                <div class="popuplabel">Area of study *</div>
                <div class="popupfield"><input type="text" name="area" placeholder="Ex Political Study" /></div>
            </div>

            <div class="fieldwrap">
                <div class="edudatewrap">
                    <div class="timewrap timewrapedu">
                        <div class="popuplabel edu_pop_st_dt">Start Date *</div>
                        <div class="startdatewrap">
                            <select name="startdate">
                                <option>Select Year</option>
                                <?php for ($y = 1990; $y <= date('Y'); $y++) { ?>
                                    <option value="<?php echo $y; ?>"><?php echo $y; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="timewrap timewrapedu">
                        <div class="popuplabel edu_pop_end_dt popupedulabel_desk">End Year (or expected) *</div>
                        <div class="popuplabel edu_pop_end_dt popupedulabel_mob">End Year </br> (or expected) *</div>
                        <div class="startdatewrap">
                            <select name="enddate">
                                <option>Select Year</option>
                                <?php for ($y = 1990; $y <= (date('Y') + 4); $y++) { ?>
                                    <option value="<?php echo $y; ?>"><?php echo $y; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div class="fieldwrap">
                <div class="popuplabel">Description *</div>
                <div class="popupfield">
                    <textarea rows="8" name="description"></textarea>
                </div>
            </div>
            <div class="fieldwrap frmbtn">
                <input type="hidden" name="action" value="add_education" />
                <input type="hidden" name="education" />
                <button class="btn btn-primary submit-education">Save</button>
            </div>

        </div>
    </div>
</div>


<style>
    .popupedulabel_mob{
        display: none;
    }

    div#content {
        background: white;
    }
    /* Education */
    img.carr_notify_img {
        position: absolute;
        left: 81px;
        top: -23px;
        width: 30px;
        display: none;
    }
    img.edu_notify_img {
        position: absolute;
        left: 112px;
        top: -23px;
        width: 30px;
        display: none;
    }
    .editbtn {
        color: rgba(0, 0, 0, 0.6);
        font-weight: normal;
        margin-right: 14px;
        margin-top: 18px;
    }

    .editbtn button.trigger {
        color: rgba(0, 0, 0, 0.6);
        font-size: 15px;
    }

    .carrerwrap {
        display: flex;
        width: 100%;
    }

    .educonarea {
        margin: 0 auto;
        background-color: #fff;
        padding: 15px;
        margin-bottom: 30px;
    }

    .edutitle h3 {
        font-weight: bold;
        font-size: 18px;
        text-transform: uppercase;
        font-family: 'Heebo', sans-serif !important;
    }

    .carrerleft {
        width: auto;
        margin-left: 15px;
    }

    .carrerright {
        margin-left: 15px;
        font-family: 'Heebo', sans-serif !important;
        font-size: 14px;
        color: rgba(0, 0, 0, 0.6);
        width: 92%;
        border-bottom: 1px solid #e6e9ec;
        padding-bottom: 6px;
        margin-bottom: 20px;
    }
    .elementor-element-be1587d{
		display:none;
	}

    .carrerright h4 {
        font-size: 16px;
        font-family: 'Heebo', sans-serif !important;
        font-weight: bold;
        color: rgba(0, 0, 0, .7);
        margin-bottom: 0;
    }

    .comname {
        color: rgba(0, 0, 0, 0.9);
        font-size: 14px;
    }

    .carrerright ul {
        margin-right: 15px;
        margin-top: 7px;
    }

    .popupbody {
        padding: 18px 15px 15px;
        font-size: 16px;
        font-family: 'Heebo', sans-serif !important;
    }

    .fieldwrap {
        margin-bottom: 8px;
    }

    .popupfield input[type="text"] {
        width: 100%;
        padding: 5px;
        border-radius: 4px;
        border: 1px solid rgba(0, 0, 0, .6);
        box-sizing: border-box;
    }

    .popupheader {
        font-family: 'Heebo', sans-serif !important;
    }

    .popupheader h2 {
        margin-left: 13px;
        font-size: 22px;
    }

    .checkboxfield {
        display: inline-block;
    }

    .checkboxlabel {
        display: inline-block;
        margin-left: 5px;
    }

    .startdatewrap select {
        color: #000;
        font-size: 17px;
        -webkit-appearance: button;
        font-family: 'Heebo', sans-serif !important;
        font-weight: normal;
        padding: 5px;
        border-radius: 4px;
        border: 1px solid rgba(0, 0, 0, .6);
        -moz-appearance: button;
        -webkit-appearance: button;
        width: 48%;
        box-sizing: border-box;
    }

    .edudatewrap {
        display: flex;
    }

    .timewrap {
        width: 50%;
    }

    .fieldwrap textarea {
        width: 100%;
        height: 80px;
        padding: 5px;
        border-radius: 4px;
        border: 1px solid rgba(0, 0, 0, .6);
        box-sizing: border-box;
    }

    .fieldwrap.frmbtn {
        float: right;
        margin-top: 9px;
    }

    .frmbtn input[type="submit"] {
        font-size: 18px;
        font-weight: normal;
    }

    .timewrap.timewrapedu .startdatewrap select {
        width: 98%;
    }

    /* end Eduction*/
    .triggerbtn {
        text-align: right;
    }

    button.trigger {
        background-color: transparent;
        color: #226D82;
        border: none;
        padding: 0;
        font-size: 34px;
        margin-top: -11px;
    }

    .overlay {
        position: fixed;
        height: 100%;
        width: 100%;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: rgba(0, 0, 0, 0.8);
        display: none;
        z-index: 9999;
    }

    .popup {
        max-width: 600px;
        width: 80%;
        max-height: 400px;
        height: 90%;
        padding: 20px 0;
        position: relative;
        background: #fff;
        margin: 20px auto;
        overflow: auto;
        top: 10%;
    }

    .popupheader {
        display: flex;
        flex-wrap: wrap;
        border-bottom: 1px solid #ccc;
    }

    .close {
        position: absolute;
        top: 24px;
        font-weight: normal;
        right: 10px;
        cursor: pointer;
        color: #000;
        font-size: 19px;
    }

    .profilecover {
        position: relative;
    }

    .profilecover img {
        width: 100%;
        max-height: 350px;
    }

    picture.avatar img {
        width: 150px;
        height: 150px;
        border-radius: 50%;
        position: absolute;
        bottom: 0%;

    }

    .p-follow-area-top {
        background: #cacaca;
        padding: 5px 10px;
        position: absolute;
        bottom: 8%;
        right: 15%;
    }

    .p-follow-area-top a {
        color: #000;
    }

    .p-follow-area-top:hover {
        background: #1abc9c;
        cursor: pointer;
    }

    .p-mess-area {
        background: #cacaca;
        padding: 5px 10px;
        position: absolute;
        bottom: 8%;
        right: 8%;
    }

    .p-mess-area a {
        color: #000;
    }

    .p-mess-area:hover {
        background: #1abc9c;
        cursor: pointer;
    }

    .p-connect-area {
        background: #cacaca;
        padding: 5px 10px;
        position: absolute;
        bottom: 8%;
        right: 21%;
    }

    .p-connect-area a {
        color: #000;
    }

    .p-connect-area:hover {
        background: #1abc9c;
        cursor: pointer;
    }

    .p-dots-area {
        background: #cacaca;
        padding: 0px 8px;
        position: absolute;
        height: 25px;
        bottom: 8%;
        right: 5%;
    }

    .p-dots-area a {
        color: #000;
    }

    .p-dots-area:hover {
        background: #1abc9c;
        cursor: pointer;
    }

    .p-dots-area p {
        margin-top: -8px;
        font-size: 20px
    }

    .profile-name {
        position: absolute;
        bottom: 4%;
        left: 16%;
    }

    .profile-name h2 {
        color: white !important;
    }

    i.fa-wifi {
        transform: rotate(55deg);
        position: relative;
        right: 3px;
        top: 0px;
    }

    i.far.fa-comment-alt {
        padding-right: 4px;
    }

    i.fa.fa-snowflake {
        padding-right: 4px;
    }

    .button_area {
        margin: 15px;
    }

    .gen-btn-info {
        color: grey;
        background-color: white;
        border: solid 1px grey;
        padding: 7px;
        font-size: 14px;
    }

    .gen-btn-info:hover {
        color: green;
        text-decoration: none;
    }

    .gen-btn-active {
        color: green;
        border: solid 1px green;
    }

    .mobile-button {
        display: none;
    }

    .p-v-label {
        text-align: right;
    }

    .p-v-text h3 {
        padding-top: 4px;
        font-size: 18px;
    }

    .p-v-label label.control-label {
        font-size: 16px;
        padding-top: 0px;
    }

    i.fas.fa-atom {
        font-size: 18px;
        padding-right: 5px;
    }

    i.fas.fa-home {
        font-size: 18px;
        padding-right: 5px;
    }

    i.fas.fa-city {
        font-size: 18px;
        padding-right: 5px;
    }

    i.fas.fa-ruler-vertical {
        font-size: 18px;
        padding-right: 5px;
    }

    i.fas.fa-wine-glass-alt {
        font-size: 18px;
        padding-right: 5px;
    }

    i.fas.fa-graduation-cap {
        font-size: 18px;
        padding-right: 5px;
    }

    i.fas.fa-smoking {
        font-size: 18px;
        padding-right: 5px;
    }

    i.fas.fas.fa-landmark {
        font-size: 18px;
        padding-right: 5px;
    }

    i.fas.fa-universal-access {
        font-size: 18px;
        padding-right: 5px;
    }

    i.fas.fa-praying-hands {
        font-size: 18px;
        padding-right: 5px;
    }

    i.fa.fa-language {
        font-size: 18px;
        padding-right: 5px;
    }

    i.fas.fa-table {
        font-size: 18px;
        padding-right: 5px;
    }

    .mobile-bio-age {
        display: none;
    }

    .lets_text {
        position: absolute;
        top: 55%;
        left: 38%;
        color: white;
        font-size: 27px;
        transform: translate(-50%, -50%);
    }

    .let_hang {
        margin-top: -31px;
    }

    i.far.fa-star {
        font-size: 25px;
        color: orange;
    }

    i.fas.fa-star {
        font-size: 25px;
        color: orange;
    }

    .social_text {
        margin-top: 25px;
    }

    .p_v_s_star {
        text-align: center;
        margin: 10px;
    }

    .p_v_so_rev {
        border: solid 2px #3333;
        padding-left: 0px;
        padding-right: 0px;
    }

    .social_avg_rate a {
        background-color: #7aa05f;
        font-family: "Roboto", Sans-serif;
        font-weight: 600;
        font-size: 15px;
        padding: 7px 21px;
        border-radius: 3px;
        color: #fff;
        text-decoration: none;
        display: inline-block;
    }

    .social_avg_rate {
        text-align: center;
        margin: 10px;
    }

    .all_rev_button {
        margin: 10px;
    }

    .all_rev_button a {
        background-color: white;
        font-family: "Roboto", Sans-serif;
        font-weight: 600;
        font-size: 15px;
        padding: 7px 21px;
        border: solid 1px green;
        color: #bae840;
        text-decoration: none;
        display: inline-block;
    }

    .all-review-area {
        margin-top: 10px;
    }

    .all-rev-box {
        border: solid 2px #3333;
    }

    .social-sec {
        border-bottom: solid 2px #3333;
    }

    .all-rev-text {
        margin-top: 15px;
    }

    .all-rev-reply p {
        color: green;
        text-align: right;
        margin-right: 60px;
        font-size: 20px;
    }

    .give_rev_button a {
        background-color: #22b14c;
        font-family: "Roboto", Sans-serif;
        font-weight: 600;
        font-size: 20px;
        padding: 7px 21px;
        border: solid 1px #22b14c;
        color: white;
        text-decoration: none;
        display: inline-block;
    }

    .give_rev_button {
        text-align: center;
        margin-bottom: 10px;
    }
    #content h1{
       display: none;
    }

    @media (max-width: 900px) {
        .educonarea {
            width: 100%;
        }
    }

    @media (max-width: 600px) {
        .popupedulabel_desk{
            display: none;
        }
        .popupedulabel_mob{
            display: block;
        }

        div#content {
            margin-top: 60px;
        }

        picture.avatar img {
            width: 100px;
            height: 100px;
        }

        .p-connect-area {
            right: 48%;
        }

        .p-follow-area-top {
            right: 32%;
        }

        .p-mess-area {
            right: 13%;
        }

        .profile-name {
            bottom: 17%;
            left: 34%;
        }

        .profile-name h2 {
            font-size: 25px !important;
        }

        .button_area.mobile-button {
            display: block;
        }

        .button_area {
            display: none;
        }

        .p_car_space{
            padding-left: 0px;
            padding-right: 0px;
            text-align: center;
        }
        
        .p_gen_space{
            padding-left: 0px;
            padding-right: 0px;
            text-align: center;
        }

        .button-display {
            display: flex;
            padding-left: 0px;
            padding-right: 0px;
        }

        .p-v-label {
            text-align: left;
        }

        .let_hang {
            margin-top: 0px;
        }

        .mobile-bio-age {
            display: block !important;
        }

        .desk-bio-age {
            display: none;
        }

        .social_text {
            margin-top: 0px;
            text-align: center;
        }

        .all-rev-reply p {
            text-align: center;
            margin-right: 0px;
        }

        .all-rev-text {
            text-align: center;
        }

        .all_rev_button {
            text-align: center;
        }
        .popuplabel.edu_pop_end_dt {
            text-align: center;
        }
        .popuplabel.edu_pop_st_dt {
            margin-top: 10px;
            margin-bottom: 14px;
            text-align: center;
        }

    }
</style>
<script type="text/javascript">
        jQuery(document).ready(function($) {
            <?php if (empty($careers)) { ?>
                $("img.carr_notify_img").show();
            <?php } ?>
            <?php if (empty($educations)) { ?>
                $("img.edu_notify_img").show();
            <?php } ?>
        });
    
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        jQuery('.startdate').datepicker({
		    maxDate: 0,
            onSelect: function(dateStr) {
            var min = $(this).datepicker('getDate'); // Selected date or null if none
            $('.enddate').datepicker('option', {minDate: min});
        }
		});
        jQuery('.enddate').datepicker({
		    maxDate: 0,
            onSelect: function(dateStr) {
            var max = $(this).datepicker('getDate'); // Selected date or null if none
            $('.startdate').datepicker('option', {maxDate: max});
        }
		});

        $('.add-career .submit-career').click(function() {
            var job_title = $('.add-career input[name="job_title"]').val(),
                company = $('.add-career input[name="company"]').val(),
                location = $('.add-career input[name="location"]').val(),
                working = $('.add-career input[name="working"]').prop('checked'),
                startdate = $('.add-career .startdate').val(),
                enddate = $('.add-career .enddate').val(),
                description = $('.add-career textarea[name="description"]').val();
            working = working ? 1 : 0;
            var action = ($('.add-career input[name="action"]').val() == 'edit_career') ? 'edit_career' : 'add_career';
            var career = $('.add-career input[name="career"]').val();

            $.ajax({
                type: 'POST',
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                dataType: 'json',
                data: {
                    action: action,
                    career: career,
                    job_title: job_title,
                    company: company,
                    location: location,
                    working: working,
                    startdate: startdate,
                    enddate: enddate,
                    description: description
                },
                success: function(data) {
                    if (data.success) {
                        window.location.href = "<?php echo add_query_arg(array()); ?>";
                    } else {
                        alert(data.msg);
                    }
                },
                error: function(MLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
        });

        $('.triggercareer').click(function() {
            $('.careerpopup').fadeIn(300);
            var popuptit = $(this).attr("popupheadtext");
            $("#carpopup h2").text(popuptit);
            if ($(this).attr('action') == 'edit_career') {
                var node = $(this).parent().parent().find('.carrerright');
                var job_title = node.find('.job_title').text();
                var company = node.find('.comname').text();
                var location = node.find('.exloc').text();
                var working = $(this).parent().attr('working');
                var startdate = node.find('.extime .start').attr('date');
                if (working != 1) {
                    var enddate = node.find('.extime .end').attr('date');
                    $('.add-career .enddate').val(enddate);
                }
                var description = node.find('.description p').html().trim().replace(new RegExp('<br>', 'g'), "\n");

                $('.add-career input[name="action"]').val('edit_career');
                $('.add-career input[name="career"]').val($(this).parent().attr('career'));
                $('.add-career input[name="job_title"]').val(job_title);
                $('.add-career input[name="company"]').val(company);
                $('.add-career input[name="location"]').val(location);
                $('.add-career input[name="working"]').prop('checked', (working == 1));
                $('.add-career .startdate').val(startdate);
                $('.add-career textarea[name="description"]').val(description);
            } else {
                $('.add-career input').val('');
                $('.add-career textarea').val('');
            }
        });

        $('.add-education .submit-education').click(function() {
            var school = $('.add-education input[name="school"]').val(),
                degree = $('.add-education input[name="degree"]').val(),
                area = $('.add-education input[name="area"]').val(),
                startdate = $('.add-education select[name="startdate"]').val(),
                enddate = $('.add-education select[name="enddate"]').val(),
                description = $('.add-education textarea[name="description"]').val();

            var action = ($('.add-education input[name="action"]').val() == 'edit_education') ? 'edit_education' : 'add_education';
            var education = $('.add-education input[name="education"]').val();

            $.ajax({
                type: 'POST',
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                dataType: 'json',
                data: {
                    action: action,
                    education: education,
                    school: school,
                    degree: degree,
                    area: area,
                    startdate: startdate,
                    enddate: enddate,
                    description: description
                },
                success: function(data) {
                    if (data.success) {
                        window.location.href = "<?php echo add_query_arg(array()); ?>";
                    } else {
                        alert(data.msg);
                    }
                },
                error: function(MLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
        });


        $('.close').click(function() {
            $('.careerpopup').fadeOut(300);
        });

        $('.triggeredu').click(function() {
            $('.educationpopup').fadeIn(300);
            var popuptit = $(this).attr("popupheadtext");
            $("#edupopup h2").text(popuptit);

            if ($(this).attr('action') == 'edit_education') {
                var node = $(this).parent().parent().find('.carrerright');
                var school = node.find('.school').text();
                var degree = node.find('.degree').text();
                var area = node.find('.area').text();
                var startdate = node.find('.extime .start').text();
                var enddate = node.find('.extime .end').text();

                var description = node.find('.description p').html().trim().replace(new RegExp('<br>', 'g'), "\n");

                $('.add-education input[name="action"]').val('edit_education');
                $('.add-education input[name="education"]').val($(this).parent().attr('education'));
                $('.add-education input[name="school"]').val(school);
                $('.add-education input[name="degree"]').val(degree);
                $('.add-education input[name="area"]').val(area);
                $('.add-education select[name="startdate"]').val(startdate);
                $('.add-education select[name="enddate"]').val(enddate);
                $('.add-education textarea[name="description"]').val(description);
            } else {
                $('.add-education input').val('');
                $('.add-education textarea').val('');
            }
        });
        $('.close').click(function() {
            $('.educationpopup').fadeOut(300);
        });
    });
 
</script>

<style type="text/css">
    #footer {
        display: none;
    }
</style>
<?php if ($_GET['device'] == 'app') { ?>
<style>
    @media screen and (max-width: 600px) {
        .popupedulabel_mob {
            margin-bottom: 4px;
        }
    }
</style>
  <?php } ?>