<div class="p-4">
    <h4 class="font-lg text-center pt-3 mb-4">Transaction History</h4>
    <div class="table-responsive">
        <table class="table table-condensed">
            <thead>
                <tr class="bg-gray">
                    <th class="bg-gray">Date</th>
                    <th class="bg-gray">Type</th>
                    <th class="bg-gray">Amount</th>
                    <th class="bg-gray">Balance</th>
                    <!--th class="bg-gray">Commission Balance</th-->
                </tr>
            </thead>
            <tbody>
                <?php if ($total > 0) {
                    $withdraw_log = $wpdb->get_results("SELECT withdraw_log.* FROM `withdraw_log`,`user` WHERE withdraw_log.userid=user.userid AND user.username='{$username}' ORDER BY `withdraw_log`.`wdate` DESC LIMIT " . ($page - 1) . ",{$number_per_page}");
                    if (count($withdraw_log) > 0) {
                        foreach ($withdraw_log as $log_item) { ?>
                            <tr>
                                <td> <?php echo $log_item->wdate; ?> </td>
                                <td> <?php echo $log_item->type; ?> </td>
                                <td>$ <?php echo $log_item->amount; ?> </td>
                                <td>$ <?php echo $log_item->balance; ?> </td>
                            </tr>
                        <?php }
                            } else { ?>
                        <tr>
                            <td colspan="4">You do not have any transaction.</td>
                        </tr>
                    <?php }
                    } else { ?>
                    <tr>
                        <td colspan="4">You do not have any transaction.</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div> <?php if ($total_page > 1) {
                echo '<p>' . (($page > 1) ? ('<a href="' . home_url("/user/{$username}/?profiletab=transaction&pp=" . ($page - 1)) . '">< Last</a>') : '') . '				' . (($page < $total_page) ? ('<a href="' . home_url("/user/{$username}/?profiletab=transaction&pp=" . ($page + 1)) . '">Next ></a>') : '') . '</p>';
            } else {
                echo '<p></p>';
            }    ?> </a>
</div>
<style type="text/css">
    #content h1{
       display: none;
    }
    @media (max-width: 600px){
        div#content {
            margin-top: 60px;
        }
        
        body{
            overflow-x: hidden;
        }
    }
</style>