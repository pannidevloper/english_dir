<div class="p-4">
    <h4 class="font-lg text-center pt-3 mb-4">Booking codes</h4>
    <div class="d-flex flex-column flex-sm-row" style="max-width: 550px;margin: 0 auto;">
        <div class="flex-half p-2">
            <a href="https://www.travpart.com/Chinese/travcust/" target="_blank" class="btn btn-submit-org btn-block">添加旅行计划</a>
        </div>
        <div class="flex-half p-2">
            <a href="https://www.travpart.com/Chinese/logout/" class="btn btn-submit-blue btn-block">注销</a>
        </div>
    </div>

    <div class="booking-code-list mt-4">
        <div class="border-top-blue w-100 pb-3"> </div>
        <div class="booking-code-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue ">
            <div class="booking-code-list-left mb-3 mb-sm-0 mr-sm-3">
                <div class="thumb-wrap">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/tour-details/48.jpg" width="180" alt="" class="img-responsive">
                </div>
            </div>
            <div class="booking-code-list-right flex-fill">
                <div class="d-flex flex-row">
                    <div class="v2-actions order-last">
                        <a href="#" class="cir bg-org-0 text-white"><i class="fa fa-trash"></i> </a>
                        <a href="#" class="cir bg-blue-1 text-white"><i class="fa fa-edit"></i> </a>
                    </div>
                    <div class="mr-auto">
                        <h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i> Kuta Beach </h4>
                        <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i>  巴厘岛·库塔 – <a href="#">显示在地图上</a>  (离中心地区100 m) </p>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1" style="max-width: 550px;">
                    <div class="flex-fill p-2">
                        <small>Booking codes：</small><br>
                        <strong class="font-lg">BALI3622</strong>

                    </div>
                    <div class="flex-fill p-2">
                        <small>Reservation number: </small><br>
                        <i class="fas fa-clock"></i> <span class="font-small">3 days</span>
                    </div>
                    <div class="flex-fill p-2">
                        <small>Date of the journey </small><br>
                        <i class="fas fa-calendar-alt"></i> <span class="font-small">2018/10/30 -11/1</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
foreach ($result as $singlepost) {
    echo '<a href="https://www.travpart.com/Chinese/tour-details/?bookingcode=' . $singlepost->bookingcode . '" target="_blank"><p>' .$singlepost->bookingcode. '</p></a>';
}
?>
