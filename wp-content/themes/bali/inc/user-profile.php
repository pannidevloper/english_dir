<?php

/* add a custom tab to show user pages for Client role */
add_filter('um_profile_tabs', 'add_profile_tabs', 1000);

function add_profile_tabs($tabs)
{
    global $wpdb;
    global $display_referal;
    $tabs['profile'] = array(
        'name' => 'Profile',
        'icon' => 'fa fa-user-cog',
        'custom' => true
    );

    /*
      $tabs['bookingCodeList'] = array(
      'name' => 'Tour Booking Codes',
      'icon' => 'fa fa-search-dollar',
      'custom' => true
      ); */

    $tabs['bookingCodeList'] = array(
        'name' => 'Tour Booking Codes',
        'icon' => 'fas fa-cart-plus',
        'custom' => true
    );

    if ($display_referal) {
        $tabs['referal'] = array(
            'name' => 'Referal',
            'icon' => 'fa fa-share',
            'custom' => true
        );
    }
    if (is_user_logged_in() && wp_get_current_user()->ID == um_profile_id()) {
        if (current_user_can('um_travel-advisor')) {
            $tabs['settlement'] = array(
                'name' => 'Settlement',
                'icon' => 'um-faicon-money',
                'custom' => true
            );
        }
        $tabs['billing_method'] = array(
            'name' => 'Billing Method',
            'icon' => 'fa fa-briefcase',
            'custom' => true
        );
        $tabs['transaction'] = array(
            'name' => 'Transactions',
            'icon' => 'fa fa-history',
            'custom' => true
        );

        $tabs['verification'] = array(
            'name' => 'Verification',
            'icon' => 'fa fa-credit-card',
            'custom' => true
        );

        if (current_user_can('um_clients')) {
            $tabs['vouchers'] = array(
                'name' => 'Vouchers',
                'icon' => 'fa fa-trophy',
                'custom' => true
            );
        }
    }
    return $tabs;
}

/* Tell the tab what to display */
add_action('um_profile_content_bookingCodeList_default', 'um_profile_content_bookingCodeList');

function um_profile_content_bookingCodeList()
{
    global $wpdb;

    $current_user = wp_get_current_user();

    if (!current_user_can('um_clients')) {
        $bookingCodeList = $wpdb->get_results("SELECT t.id tour_id,t.confirm_payment,t.total,p.post_id,p.meta_key FROM (wp_tour t LEFT JOIN wp_postmeta p
											ON (t.id=p.meta_value AND p.meta_key='tour_id')) INNER JOIN wp_tourmeta m
											ON (t.id=m.`tour_id` AND m.meta_key='userid' AND m.meta_value='{$current_user->ID}')");
    } else {
        $bookingCodeList = $wpdb->get_results("SELECT t.id tour_id,t.confirm_payment,t.total,p.post_id,p.meta_key FROM (wp_tour t LEFT JOIN wp_postmeta p
											ON (t.id=p.meta_value AND p.meta_key='tour_id')) INNER JOIN wp_client_tour ct
											ON (t.id=ct.`tour_id` AND ct.user_id='{$current_user->ID}')");
    }


    include("template/bookingCodeList.php");
}

/* function um_profile_content_bookingCodeListOld( $args ) {
  global $wpdb;
  $profile_id = um_profile_id();

  $counter= 0;
  $paid_by_paypal = true;
  $agent_commission = "Not Paid Yet";
  $total_sales_by_agent = 0;

  $query = "SELECT * from `finalitinerary` where userid='" . $profile_id . "' group by tourid"; // use finalitinerary table
  $result = $wpdb->get_results($query);

  $querytogettotalsales = "SELECT SUM(totalpricewithdiscount) FROM `finalitinerary` WHERE userid='" . $profile_id . "' group by tourid";
  $res = $wpdb->get_results($querytogettotalsales);
  foreach ($res as $t) {
  $a = json_encode($t);
  $aa =  json_decode($a,true);
  foreach ($aa as $val) {
  $total_sales_by_agent = $val;
  }
  }

  echo '<form action="" method="post">';
  echo '<table class="table" border = "1">';
  echo '<tr>';
  echo '<th> Booking Code </th>';
  echo '<th> Sales Amount </th>';
  echo '<th> Commission </th>';
  echo '<th> Payment Status </th>';
  echo '<th> Feedback </th>';
  echo '</tr>';

  foreach ($result as $singlepost) {

  $counter =  $counter + 1;
  $bookcode = $singlepost->bookingcode;

  if($dataoftour->confirm_payment != 0 ||  1==1){

  $total_price_paid_by_client = $singlepost->totalpricewithdiscount;
  $offered_price_by_vendor = $singlepost->vendorbid;
  $tourbali_cut = $total_price_paid_by_client - $offered_price_by_vendor;

  if($tourbali_cut >= 0 ){
  if ($total_sales_by_agent < 10000)
  {
  $agent_commission = (8 / 100) * $tourbali_cut;
  }
  else if($total_sales_by_agent >= 10000 && $total_sales_by_agent < 50000)
  {
  $agent_commission = (8.25 / 100) * $tourbali_cut;
  }
  else if($total_sales_by_agent >= 50000 && $total_sales_by_agent < 100000)
  {
  $agent_commission = (8.50 / 100) * $tourbali_cut;
  }
  else if($total_sales_by_agent >100000)
  {
  $agent_commission = (8.75 / 100) * $tourbali_cut;
  }
  }else{
  $agent_commission = 0;
  }

  }else{

  $agent_commission = "Not Paid Yet";

  }

  ?>

  <tr>
  <td> <a href="https://www.travpart.com/English/tour-details/?bookingusercode=<?php echo $singlepost->bookingcode ?>" target="_blank"><p><?php echo $singlepost->bookingcode ?> </p></a> </td>
  <td> <?php echo $singlepost->totalpricewithdiscount ?> </td>
  <td> <?php echo $agent_commission ?> </td>

  <td>
  <?php
  $sqlfortour = "SELECT * FROM `wp_tour` where code='".$bookcode."'" ;
  $resultoftour = $wpdb->get_results($sqlfortour);

  if($resultoftour != 0){
  foreach ($resultoftour as $dataoftour) {
  if($dataoftour->confirm_payment == 0){
  ?>
  <p class="btn btn-primary clickonwiretransfer" data-themeuri="<?php echo get_stylesheet_directory_uri() ?>"  data-bookcode="<?php echo $bookcode; ?>"> Paid by wire transfer </p>
  <?php
  }else{
  ?>
  <p class="btn btn-primary" disabled> Paid </p>
  <?php
  }
  }
  }else{
  echo "<p class='btn btn-primary' disabled> Not Paid yet </p>";
  }
  ?>
  </td>

  <td class='rating-widget'>
  <div class='rating-stars text-center'>
  <ul id='stars' data-id='<?php echo $bookcode; ?>'>
  <li class='star' title='Poor' data-value='1'>
  <i class='fa fa-star fa-fw'></i>
  </li>
  <li class='star' title='Fair' data-value='2'>
  <i class='fa fa-star fa-fw'></i>
  </li>
  <li class='star' title='Good' data-value='3'>
  <i class='fa fa-star fa-fw'></i>
  </li>
  <li class='star' title='Excellent' data-value='4'>
  <i class='fa fa-star fa-fw'></i>
  </li>
  <li class='star' title='WOW!!!' data-value='5'>
  <i class='fa fa-star fa-fw'></i>
  </li>
  </ul>
  </div>

  <div class='success-box'>
  <div class='clearfix'></div>
  </div>
  </td>
  </tr>

  <input type="hidden" name="counterValue" value="<?php echo $counter ?>" />
  <input type="hidden" name="bookcodehidden<?php echo $counter ?>" value="<?php echo $singlepost->bookingcode ?>" />

  <?php
  }

  echo '</table>';

  echo '<div style="display: block;margin: 0 auto;text-align: center;">';

  echo '<a href="https://www.travpart.com/English/travcust/" target="_blank" class="btn btn-default left-btnapp">Add a Tour</a>';
  echo '<a href="https://www.travpart.com/English/logout/" class="btn btn-default right-btnapp">Logout</a>';

  echo '</div>';

  include("template/bookingCodeList.php");

  } */


/* Tell the tab what to display 
  add_action('um_profile_content_bookingCodeList_default', 'um_profile_content_bookingCodeList');
  function um_profile_content_bookingCodeList( $args ) {

  $profile_id = um_profile_id();
  global $wpdb;
  $query = "SELECT bookingcode from `finalitinerary` where userid='" . $profile_id . "' group by bookingcode";
  $result = $wpdb->get_results($query);

  require_once( 'template/book-code.php');


  foreach ($result as $singlepost) {


  echo '<a href="https://www.travpart.com/Chinese/tour-details/?bookingcode=' . $singlepost->bookingcode . '" target="_blank"><p>' .$singlepost->bookingcode. '</p></a>';
  }

  echo '<a href="https://www.travpart.com/Chinese/travcust/" target="_blank" class="btn btn-default left-btnapp">添加旅行计划</a>';

  echo '<a href="https://www.travpart.com/Chinese/logout/" class="btn btn-default right-btnapp">注销</a>';

  }
 */

if (current_user_can('um_travel-advisor'))
    add_action('um_profile_content_referal_default', 'um_profile_content_referal');
else
    add_action('um_profile_content_referal_default', 'um_buyer_profile_content_referal');

function um_profile_content_referal($args)
{
    global $wpdb;
    $profile_id = um_profile_id();
    $username = $wpdb->get_row("SELECT * FROM `wp_users` WHERE `ID` = {$profile_id}")->user_login;
    $success_count = $wpdb->get_var("SELECT COUNT(*) FROM `rewards`,`user` WHERE rewards.userid=user.userid AND type='invite' AND user.username='{$username}'");

    $bar_class = "bar1";
    if ($success_count >= 50) {
        $bar_class = "bar4";
    } else if ($success_count >= 20) {
        $bar_class = "bar3";
    } else if ($success_count >= 5) {
        $bar_class = "bar1";
    }
    $share_url = "https://www.travpart.com/English/travchat/signup.php?code=$username";
    $encode_share_url = urlencode($share_url);

    echo '<div class="img-wrap"><img src="https://www.travpart.com/English/wp-content/uploads/2018/10/user-profiletab-referal-head-en.jpg" alt=""></div>
<div class="profile-content">
		<h2>share to your friends and earn $5 cash when they joined and created a tour package</h2>
		<p>You currently have ' . $success_count . ' Referals</p>
		<div class="sf-input-wrap"><input id="sf-input" type="text" value="https://www.travpart.com/English/travchat/signup.php?code=' . $username . '" /><button  data-clipboard-target="#sf-input" class="btn btn-primary sf-btn">Copy</button></div>
		
	    <p>share it on Facebook, Twitter, Google+, Pinterest, Whatsapp</p>
		<div class="share-sf-input-wrap">
		<input style="text-align: left;" id="share-sf-input" type="text" value="' . $share_url . '" />		
		<div class="share-buttons">
	        <a class="share-facebook" href="http://www.facebook.com/sharer/sharer.php?u=' . $encode_share_url . '" target="_blank"><i class="fab fa-facebook-f"></i> </a>
	        <a class="share-twitter" href="http://twitter.com/share?url=' . $encode_share_url . '&text=' . urlencode('I recommend you use the travcust app for making money to sell tour packages. check and get $5 credit.') . '" target="_blank"><i class="fab fa-twitter"></i> </a>
			<a href="#" style="background: #cb2027;" onclick="gotoshare(\'pinterest\')"><i class="fab fa-pinterest"></i></a>
	        <a href="#" class="share-google-plus" onclick="gotoshare(\'google.plus\')"><i class="fab fa-google-plus"></i> </a>
			<a href="#" style="background: #00e676;" onclick="gotoshare(\'whatsapp\')"><i class="fab fa-whatsapp"></i></a>
        </div>        
		</div>
		
		<div class="ref-progress-bar">
		    <div class="bar"><div class="bar-inner ' . $bar_class . '"></div></div>
		    <div class="desc">
		        <div class="d1"><h5>Join</h5></div>
		        <div class="d2">
		            <h5>Supporter</h5>
		            <p>5  Referrals</p>
		        </div>
		        <div class="d3">
		            <h5>Ambassador</h5>
		            <p>20  Referrals</p>
		        </div>
		        <div class="d4">
		            <h5>VIP</h5>
		            <p>50  Referrals</p>		            
		        </div>
            </div>
        </div>		
		</div>
	';
    if (is_user_logged_in()) {
        $budget_referral_user_used = $wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE type = 'invite' AND DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND `userid`=" . wp_get_current_user()->ID);
        if (floatval($budget_referral_user_used) >= floatval(get_option('budget_referral_person_limit')))
            echo '<p style="background: yellow; color: red; font-size: 18px; text-align: center;">The campaign is currently unavailable!</p>';
    }
    echo '<link rel="stylesheet" href="/English/wp-content/themes/bali/css/social-share-media.css">
		<script src="/English/wp-content/themes/bali/js/social-share-media.js"></script>
<script>
const socialmediaurls = GetSocialMediaSiteLinks_WithShareLinks({
	"url":"' . $share_url . '",
	"title": "I recommend you use the travcust app for making money to sell tour packages. Download and get $5 credit."
});
function gotoshare(medianame)
{
	if(medianame=="whatsapp") {
		window.open("https://web.whatsapp.com/send?text="+encodeURIComponent("I recommend you use the travcust app for making money to sell tour packages. Check ' . $share_url . ' and get $5 credit."));
	}
	else {
		window.open(socialmediaurls[medianame]);
	}
}
</script>';
}

function um_buyer_profile_content_referal($args)
{
    global $wpdb;
    $profile_id = um_profile_id();
    $username = $wpdb->get_row("SELECT * FROM `wp_users` WHERE `ID` = {$profile_id}")->user_login;
    $success_count = $wpdb->get_var("SELECT COUNT(*) FROM `rewards`,`user` WHERE rewards.userid=user.userid AND type='invite' AND user.username='{$username}'");

    $bar_class = "bar1";
    if ($success_count >= 50) {
        $bar_class = "bar4";
    } else if ($success_count >= 20) {
        $bar_class = "bar3";
    } else if ($success_count >= 5) {
        $bar_class = "bar1";
    }
    $share_url = "https://www.travpart.com/English/travchat/signup.php?code=$username";
    $encode_share_url = urlencode($share_url);

    echo '
<div class="profile-content">
		<p>You currently have ' . $success_count . ' Referals</p>
		<div class="sf-input-wrap"><input id="sf-input" type="text" value="https://www.travpart.com/English/travchat/signup.php?code=' . $username . '" /><button  data-clipboard-target="#sf-input" class="btn btn-primary sf-btn">Copy</button></div>
		
	    <p>share it on Facebook, Twitter, Google+, Pinterest, Whatsapp</p>
		<div class="share-sf-input-wrap">
		<input style="text-align: left;" id="share-sf-input" type="text" value="' . $share_url . '" />		
		<div class="share-buttons">
	        <a class="share-facebook" href="http://www.facebook.com/sharer/sharer.php?u=' . $encode_share_url . '" target="_blank"><i class="fab fa-facebook-f"></i> </a>
	        <a class="share-twitter" href="http://twitter.com/share?url=' . $encode_share_url . '&text=' . urlencode('I recommend you use the travcust app for making money to sell tour packages.') . '" target="_blank"><i class="fab fa-twitter"></i> </a>
			<a href="#" style="background: #cb2027;" onclick="gotoshare(\'pinterest\')"><i class="fab fa-pinterest"></i></a>
	        <a href="#" class="share-google-plus" onclick="gotoshare(\'google.plus\')"><i class="fab fa-google-plus"></i> </a>
			<a href="#" style="background: #00e676;" onclick="gotoshare(\'whatsapp\')"><i class="fab fa-whatsapp"></i></a>
        </div>        
		</div>
		
		<div class="ref-progress-bar">
		    <div class="bar"><div class="bar-inner ' . $bar_class . '"></div></div>
		    <div class="desc">
		        <div class="d1"><h5>Join</h5></div>
		        <div class="d2">
		            <h5>Supporter</h5>
		            <p>5  Referrals</p>
		        </div>
		        <div class="d3">
		            <h5>Ambassador</h5>
		            <p>20  Referrals</p>
		        </div>
		        <div class="d4">
		            <h5>VIP</h5>
		            <p>50  Referrals</p>		            
		        </div>
            </div>
        </div>		
		</div>
	';
    echo '<link rel="stylesheet" href="/English/wp-content/themes/bali/css/social-share-media.css">
		<script src="/English/wp-content/themes/bali/js/social-share-media.js"></script>
<script>
const socialmediaurls = GetSocialMediaSiteLinks_WithShareLinks({
	"url":"' . $share_url . '",
	"title": "I recommend you use the travcust app for making money to sell tour packages."
});
function gotoshare(medianame)
{
	if(medianame=="whatsapp") {
		window.open("https://web.whatsapp.com/send?text="+encodeURIComponent("I recommend you use the travcust app for making money to sell tour packages. Check it here: ' . $share_url . ' "));
	}
	else {
		window.open(socialmediaurls[medianame]);
	}
}
</script>';
}

add_action('um_profile_content_settlement_default', 'um_profile_content_settlement');

function um_profile_content_settlement($args)
{
    global $wpdb;
    $withdraw_log_limit = 5;
    $current_user = wp_get_current_user();
    $username = $current_user->user_login;
    $action = htmlspecialchars($_GET['action']);

    if (empty($action) || $action == 'default') {

        $balance = $wpdb->get_var("SELECT balance FROM `balance`,`user` WHERE balance.userid=user.userid AND user.username='{$username}'");
        if (empty($balance) || $balance < 0)
            $balance = '0.00';

        //$rewards = $wpdb->get_results("SELECT type FROM `rewards`,`user` WHERE rewards.userid=user.userid AND user.username='{$username}'");
        $verified = get_user_meta($current_user->ID, 'verification_status', true);

        require_once('template/settlement.php');

        /* echo '<p>Available: $' . $balance . '</p>';
          echo '<p><button>Get Paid Now</button>   <a href="' . home_url("/user/{$username}/?profiletab=settlement&action=add_settlement_method") . '">Add settlement method</a></p>';
          echo '<div><b>Recent transactions</b></div><hr/>';
          echo '<table>
          <tr> <th>Date</th> <th>Type</th> <th>Amount</th> <th>Balance</th> </tr>';
          if ($balance > 0) {
          $withdraw_log = $wpdb->get_results("SELECT withdraw_log.* FROM `withdraw_log`,`user` WHERE withdraw_log.userid=user.userid AND user.username='{$username}' ORDER BY `withdraw_log`.`wdate` DESC LIMIT {$withdraw_log_limit}");
          if (count($withdraw_log) > 0) {
          foreach ($withdraw_log as $log_item) {
          echo "<tr> <td>{$log_item->wdate}</td> <td>{$log_item->type}</td> <td>\${$log_item->amount}</td> <td>\${$log_item->balance}</td>  </tr>";
          }
          } else {
          echo '<tr> <td colspan="3">You don\'t have any transactions.</td> </tr>';
          }
          } else {
          echo '<tr> <td colspan="3">You don\'t have any transactions.</td> </tr>';
          }
          echo '</table>';
          echo '<p><a href="' . home_url("/user/{$username}/?profiletab=settlement&action=show_log") . '">View all transactions ></a>'; */
    } else if ($action == 'show_log') {
        if (empty($_GET['pp']) || intval($_GET['pp']) < 1)
            $page = 1;
        else
            $page = intval($_GET['pp']);
        $number_per_page = 10;
        $total = $wpdb->get_var("SELECT COUNT(*) FROM `withdraw_log`,`user` WHERE withdraw_log.userid=user.userid AND user.username='{$username}'");
        $total_page = ceil($total / $number_per_page);

        if ($page > $total_page)
            $page = $total_page;

        echo '<h4>Transactions</h4><hr/>';
        echo '<table>
				<tr> <th>Date</th> <th>Type</th> <th>Amount</th> <th>Balance</th> </tr>';
        if ($total > 0) {
            $withdraw_log = $wpdb->get_results("SELECT withdraw_log.* FROM `withdraw_log`,`user` WHERE withdraw_log.userid=user.userid AND user.username='{$username}' ORDER BY `withdraw_log`.`wdate` DESC LIMIT " . ($page - 1) . ",{$number_per_page}");
            foreach ($withdraw_log as $log_item) {
                echo "<tr> <td>{$log_item->wdate}</td> <td>{$log_item->type}</td> <td>\${$log_item->amount}</td> <td>\${$log_item->balance}</td>  </tr>";
            }
        } else {
            echo '<tr> <td colspan="3">You don\'t have any transactions.</td> </tr>';
        }
        echo '</table>';

        if ($total_page > 1) {
            echo '<p>
				' . (($page > 1) ? ('<a data-toggle="tooltip" title="Hooray!" href="' . home_url("/user/{$username}/?profiletab=settlement&action=show_log&pp=" . ($page - 1)) . '">< Last</a>') : '') . '
				' . (($page < $total_page) ? ('<a href="' . home_url("/user/{$username}/?profiletab=settlement&action=show_log&pp=" . ($page + 1)) . '">Next ></a>') : '') . '
			</p>';
        } else {
            echo '<p>End~</p>';
        }
    } else if ($action == 'add_settlement_method') {
        echo '<style>
.um-profile .um-profile-body.settlement-default {
padding: 15px!important;
background: #fff;
}
.segment-input-label > span {
  display: inline-block!important;
  width: 145px!important;
}
.segment-input-label > input[type=text],.segment-input-label > input[type=password]{
  width: 45%!important;
  margin-left: 8px!important;
  display: inline!important;
}

</style>';
        echo '<h4>Add Settlement Method</h4><hr/><br>';
        if (!empty($_POST['confirm_password'])) {
            if (wp_check_password($_POST['confirm_password'], $current_user->user_pass, $current_user->ID)) {
                $saved_status = false;
                if (!empty($_POST['paypal'])) {
                    update_user_meta($current_user->ID, 'paypal_payment', htmlspecialchars($_POST['paypal']));
                    $saved_status = true;
                }
                if (
                    !empty($_POST['bank_swift_code']) && !empty($_POST['account_number']) && !empty($_POST['bank_name']) &&
                    !empty($_POST['account_name']) && !empty($_POST['account_address']) &&
                    !empty($_POST['account_city']) && !empty($_POST['account_country'])
                ) {
                    // && !empty($_POST['account_phone_number'])
                    $bank_payment = array(
                        'bank_swift_code' => htmlspecialchars($_POST['bank_swift_code']),
                        'bank_name' => htmlspecialchars($_POST['bank_name']),
                        'account_number' => htmlspecialchars($_POST['account_number']),
                        'account_name' => htmlspecialchars($_POST['account_name']),
                        'account_address' => htmlspecialchars($_POST['account_address']),
                        'account_city' => htmlspecialchars($_POST['account_city']),
                        'account_country' => htmlspecialchars($_POST['account_country']),
                        //                        'account_phone_number'=>htmlspecialchars($_POST['account_phone_number'])
                    );
                    update_user_meta($current_user->ID, 'bank_payment', $bank_payment);
                    $saved_status = true;
                }

                if (!empty($_POST['hiredName']) && !empty($_POST['hiredPlatform'])) {
                    update_user_meta($current_user->ID, 'hiredPlatform', htmlspecialchars($_POST['hiredPlatform']));
                    update_user_meta($current_user->ID, 'verification_IDName', htmlspecialchars($_POST['hiredName']));
                    wp_update_user(array('ID' => $current_user->ID, 'nickname' => htmlspecialchars($_POST['hiredName']), 'display_name' => htmlspecialchars($_POST['hiredName'])));
                    update_travchat_nickname($current_user->user_login, htmlspecialchars($_POST['hiredName']));
                    update_user_meta($current_user->ID, 'verification_status', 2);
                    $saved_status = true;
                }
                if ($saved_status) {
                    echo '<p style="font-size: 18px; color: green;"><b>Successfully saved!</b></p>';
                } else {
                    echo '<p style="font-size: 18px; color: red;"><b>Save failed, please try again!</b></p>';
                }
            } else {
                echo '<p style="font-size: 18px; color: red;"><b>Authentication failed, please check your password!</b></p>';
            }
        }

        $bank_payment = get_user_meta($current_user->ID, 'bank_payment', true);

        echo '<form action="' . home_url("/user/{$username}/?profiletab=settlement&action=add_settlement_method") . '" method="POST" >
			  <p style="font-size: 17px" class="segment-input-label"><span><b>PayPal</b></span>
			  <input class="segment-input" name="paypal" type="text" value="' . (empty(get_user_meta($current_user->ID, 'paypal_payment', true)) ? (empty($_POST['paypal']) ? '' : htmlspecialchars($_POST['paypal'])) : get_user_meta($current_user->ID, 'paypal_payment', true)) . '" /></p><hr style="border: 1px solid;border-color: #0cad83;margin-top: 25px;"><br/>
			  <p style="font-size: 17px"><b>Wire transfer</b></p>
			  <p  class="segment-input-label"><span>Bank SWIFT Code</span> <input name="bank_swift_code" type="text" value="' . (empty($bank_payment) ? '' : ($bank_payment['bank_swift_code'])) . '" /></p>
			  <p  class="segment-input-label"><span>Bank Name</span> <input name="bank_name"  type="text" value="' . (empty($bank_payment) || empty($bank_payment['bank_name']) ? '' : ($bank_payment['bank_name'])) . '"></p>';
        if (!empty($_POST['bank_swift_code']) && empty($_POST['bank_name']))
            echo '<p style="color: red;">*Please input the Bank Name</p>';

        echo '<p  class="segment-input-label"><span>Account Number</span> <input name="account_number"  type="text" value="' . (empty($bank_payment) ? '' : ($bank_payment['account_number'])) . '" /></p>';
        if (!empty($_POST['bank_swift_code']) && empty($_POST['account_number']))
            echo '<p style="color: red;">*Please input the Bank Account number</p>';

        echo '<p  class="segment-input-label"><span>Name on Account</span> <input name="account_name"  type="text" value="' . (empty($bank_payment) ? '' : ($bank_payment['account_name'])) . '" /></p>';
        if (!empty($_POST['bank_swift_code']) && empty($_POST['account_name']))
            echo '<p style="color: red;">*Please input the Name on Account</p>';

        echo '<p  class="segment-input-label"><span>Address</span> <input name="account_address"  type="text" value="' . (empty($bank_payment) ? '' : ($bank_payment['account_address'])) . '" /></p>';
        if (!empty($_POST['bank_swift_code']) && empty($_POST['account_address']))
            echo '<p style="color: red;">*Please input the Address</p>';

        echo '<p  class="segment-input-label"><span>City and State/Province</span> <input name="account_city"  type="text" value="' . (empty($bank_payment) ? '' : ($bank_payment['account_city'])) . '" /></p>';
        if (!empty($_POST['bank_swift_code']) && empty($_POST['account_city']))
            echo '<p style="color: red;">*Please input the City and State/Province</p>';

        echo '<p  class="segment-input-label"><span>Country</span> <input name="account_country"  type="text" value="' . (empty($bank_payment) ? '' : ($bank_payment['account_country'])) . '" /><hr style="margin-bottom: 25px;border: 1px solid;border-color: #0cad83;margin-top: 25px;"></p>';
        if (!empty($_POST['bank_swift_code']) && empty($_POST['account_country']))
            echo '<p style="color: red;">*Please input the Country</p>';

        //        echo  '<p><span>Phone number<span> <input name="account_phone_number" style="width: 45%; display: inline !important; margin-left:65px !important;" type="text" value="' . (empty($bank_payment)?'':($bank_payment['account_phone_number'])) . '" /></p>';
        echo '<p><span id="openHiredModel" class="btn btn-primary btn-lg" data-toggle="model" data-target="#hiredModel">Hired at a freelancer\'s platform</span></p>
				<input type="hidden" id="hiredName" name="hiredName"/>
				<input type="hidden" id="hiredPlatform" name="hiredPlatform"/>';
        //        if(!empty($_POST['bank_swift_code']) && empty($_POST['account_phone_number']) )
        //            echo '<p style="color: red;">*Please input the Phone number</p>';
        //<!--input name="bank_account" type="text" value="' . (empty(get_user_meta($current_user->ID, 'bank_payment', true))? (empty($_POST['bank_account'])?'':htmlspecialchars($_POST['bank_account'])) : get_user_meta($current_user->ID, 'bank_payment', true)) . '" /--><br/>
        echo '<p  class="segment-input-label" style="font-size: 17px"><span><b>Confirm Password</b></span>
			  <input name="confirm_password" type="password" /></p>';
        if (!empty($_POST['save']) && empty($_POST['confirm_password']))
            echo '<p style="color: red;">*Please input your password</p>';
        echo '<br/><input name="save" type="submit" value="Save" style="font-size: 16px" />
			  </form>
			  <div class="modal fade" id="hiredModel" tabindex="-1" role="dialog" aria-labelledby="hiredModelLabel" aria-hidden="true">
			  <div class="modal-dialog">
			  <div class="modal-content">
			  <div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			  <h4 class="modal-title" id="hiredModelLabel">Hired at a freelancer\'s platform</h4>
			  </div>
			  <div class="modal-body">
			  <p>Username: <input id="hired_username" type="text" /></p>
			  <p>Platform: <select id="hired_platform">
					<option value="upwork">Upwork</option>
					<option value="guru">Guru</option>
					<option value="fiverr">Fiverr</option>
					<option value="freelancer">Freelancer</option>
				</select></p>
			  </div>
			  <div class="modal-footer">
			  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  <button id="hired_confirm" type="button" class="btn btn-primary">Confirm</button>
			  </div>
			  </div>
			  </div>
			  </div>
			  <script>
			  jQuery(document).ready(function() {
				  jQuery("#openHiredModel").click(function() {
					  jQuery("#hiredModel").modal("show");
				  });
				  jQuery("#hired_confirm").click(function() {
					  jQuery("#hiredName").val(jQuery("#hired_username").val());
					  jQuery("#hiredPlatform").val(jQuery("#hired_platform").val());
					  jQuery("#hiredModel").modal("hide");
				  });
			  });
			  </script>
			  ';
    }
}

add_action('um_profile_content_profile_default', 'um_profile_content_profile');

function um_profile_content_profile()
{
    if ($_GET['action'] == 'career_education_setting') {
        include_once('template/career_education.php');
    } else {
        include_once('template/profile.php');
    }
}

add_action('um_profile_content_transaction_default', 'um_profile_content_transaction');

function um_profile_content_transaction()
{
    global $wpdb;

    $current_user = wp_get_current_user();
    $username = $current_user->user_login;

    if (empty($_GET['pp']) || intval($_GET['pp']) < 1)
        $page = 1;
    else
        $page = intval($_GET['pp']);
    $number_per_page = 10;
    $total = $wpdb->get_var("SELECT COUNT(*) FROM `withdraw_log`,`user` WHERE withdraw_log.userid=user.userid AND user.username='{$username}'");
    $total_page = ceil($total / $number_per_page);
    if ($page > $total_page)
        $page = $total_page;

    require_once('template/transaction.php');
}

if (is_user_logged_in() && wp_get_current_user()->ID == um_profile_id())
    add_action('um_profile_content_verification_default', 'um_profile_content_verification');

function um_profile_content_verification()
{
    global $wpdb;
    $current_user = wp_get_current_user();
    $username = $current_user->user_login;
    $verification_error_msg = array();
    if (!empty($_POST) && !empty($_POST['IDName']) && !empty($_POST['IDNumber']) && !empty($_POST['IDExpireson'])) {
        if (($IDNumber = filter_input(INPUT_POST, 'IDNumber', FILTER_VALIDATE_REGEXP, array('default' => false, 'options' => array('regexp' => '/^[a-zA-Z0-9]*$/')))) == false)
            $verification_error_msg[] = "*Invaild ID Number";

        $IDExpireson = str_replace(array('/', ',', '-'), '', filter_input(INPUT_POST, 'IDExpireson', FILTER_SANITIZE_STRING));

        if (strtotime($IDExpireson) == false)
            $verification_error_msg[] = "*Invaild Expire date";
        else {
            $IDExpireson = strtotime($IDExpireson);
            if ($IDExpireson < time())
                $verification_error_msg[] = "*Your ID information has expired.";
        }

        $IDName = filter_input(INPUT_POST, 'IDName', FILTER_SANITIZE_STRING);
        $IDType = filter_input(INPUT_POST, 'IDType', FILTER_SANITIZE_STRING);
        $IDIssuingCountry = filter_input(INPUT_POST, 'IDIssuingCountry', FILTER_SANITIZE_STRING);

        //FILE upload
        if ($_FILES['ScanofID']['error'] == UPLOAD_ERR_NO_FILE) {
            $verification_error_msg[] = "*You must upload Scan of your ID file.";
        } else if ($_FILES['ScanofID']['error'] != 0) {
            $verification_error_msg[] = "*FILE upload failed. Please try again.";
        } else {
            $tmp_name = $_FILES['ScanofID']['tmp_name'];
            $file_name = md5(time() . $tmp_name);
            $size = $_FILES['ScanofID']['size'];
            $type = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $tmp_name);
            if (stripos($type, 'pdf') !== false)
                $file_name .= '.pdf';
            $upload_base_information = wp_upload_dir();
            $upload_dir = $upload_base_information['basedir'] . '/verification/' . date('Y/m');
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, 0700, true);
            }
            $upload_file_full_path = $upload_dir . "/" . $file_name;
            $file_url = 'verification/' . date('Y/m') . '/' . $file_name;
            if (!move_uploaded_file($tmp_name, $upload_file_full_path))
                $verification_error_msg[] = "*FILE upload failed. Please try again.";
        }

        if (count($verification_error_msg) == 0) {
            if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_usermeta` WHERE `user_id`!={$current_user->ID} AND ((`meta_key`='verification_IDName' AND `meta_value`='{$IDName}') OR (`meta_key`='verification_IDNumber' AND `meta_value`='{$IDNumber}'))") > 0) {
                update_user_meta($uid, 'verification_status', 3);
                auto_unverified($current_user->user_email, $IDName);
            } else {
                update_user_meta($current_user->ID, 'verification_file_url', $file_url);
                update_user_meta($current_user->ID, 'verification_file_type', $type);

                update_user_meta($current_user->ID, 'verification_IDType', $IDType);
                update_user_meta($current_user->ID, 'verification_IDIssuingCountry', $IDIssuingCountry);
                update_user_meta($current_user->ID, 'verification_IDName', $IDName);
                update_user_meta($current_user->ID, 'verification_IDNumber', $IDNumber);
                update_user_meta($current_user->ID, 'verification_IDExpireson', $IDExpireson);
                update_user_meta($current_user->ID, 'verification_status', 1);

                //send notification to admin_email 
                $notification_content = '<p>Dear Travpart,</p>
				<p>We just received a profile document for verification check request.</p>
				<p>Please check their profile document here <a href="' . admin_url() . 'options-general.php?page=sales-agent-list%2Fsales-agent-list.php&action=check-verification&uid=' . $current_user->ID . '">' . $IDName . '</a>';
                wp_mail(get_option('admin_email'), 'Travpart - Received a verification check request', $notification_content);
            }
        }

        //var_dump($_FILES['ScanofID']['error'],$IDType,$IDIssuingCountry,$IDNumber,$IDExpireson_year, $IDExpireson_month,$IDExpireson_day);
    }
    include_once('template/verification.php');
}

add_action('wp_ajax_delete_bookingcode', 'delete_bookingcode');

function delete_bookingcode()
{
    global $wpdb;
    $tour_id = filter_input(INPUT_POST, 'tour_id', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 999999)));
    if ($tour_id === false) {
        echo json_encode(array('error_msg' => 'Tour id is invaild.'));
    } else if (!is_user_logged_in()) {
        echo json_encode(array('error_msg' => 'You need login your account before you delete booking code.'));
    } else if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_tour` WHERE `id` = '{$tour_id}'") < 1) {
        echo json_encode(array('error_msg' => 'Tour is not existed.'));
    } else if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_tour` WHERE confirm_payment=1 AND `id` = '{$tour_id}'") == 1) {
        echo json_encode(array('error_msg' => 'The tour has been paid. You can not delete it.'));
    } else if (current_user_can('um_clients')) {
        $current_user = wp_get_current_user();
        $wpdb->query("DELETE FROM `wp_client_tour` WHERE tour_id='{$tour_id}' AND user_id='{$current_user->ID}'");
        echo json_encode(array('msg' => 'Deleted'));
    } else {
        $current_user = wp_get_current_user();
        if ($wpdb->get_var("SELECT COUNT(`wp_tourmeta`.tour_id) FROM `wp_tour`,`wp_tourmeta` where `wp_tour`.id=`wp_tourmeta`.`tour_id` AND `wp_tour`.id='{$tour_id}' AND meta_key='userid' AND meta_value='{$current_user->ID}'") != 1) {
            echo json_encode(array('error_msg' => 'The tour is not belong to you.'));
        } else {
            //if (!$wpdb->query("DELETE FROM `wp_tourmeta` WHERE `meta_key`='userid' AND `tour_id`='{$tour_id}'"))
            if (!$wpdb->query("UPDATE `wp_tourmeta` SET `meta_value`='-1' WHERE `meta_key`='userid' AND `tour_id`='{$tour_id}'"))
                echo json_encode(array('error_msg' => 'Deleted failed. Please try again.'));
            else {
                $wpdb->query("INSERT INTO `wp_tourmeta`(`tour_id`, `meta_key`, `meta_value`) VALUES ('{$tour_id}','original_userid','{$current_user->ID}')");
                $wpdb->query("DELETE FROM `wp_tour_sale` WHERE `tour_id`='{$tour_id}'");
                echo json_encode(array('msg' => 'Deleted'));
            }
        }
    }
    wp_die();
}

add_action('wp_ajax_saveitinerary', 'saveitinerary');

function saveitinerary()
{
    global $wpdb;
    $tour_id = filter_input(INPUT_POST, 'tour_id', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 999999)));
    if ($tour_id === false) {
        echo json_encode(array('error_msg' => 'Tour id is invaild.'));
    } else if (!is_user_logged_in()) {
        echo json_encode(array('error_msg' => 'You need login your account before you delete booking code.'));
    } else if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_tour` WHERE `id` = '{$tour_id}'") < 1) {
        echo json_encode(array('error_msg' => 'Tour is not existed.'));
    } else {
        if (current_user_can('um_clients')) {
            $current_user = wp_get_current_user();
            $wpdb->query("INSERT INTO `wp_client_tour` (`user_id`, `tour_id`) VALUES ('{$current_user->ID}', '{$tour_id}')");
        }
        echo json_encode(array('msg' => 'Success'));
    }
    wp_die();
}

add_action('um_profile_content_vouchers', 'um_profile_content_vouchers');
function um_profile_content_vouchers()
{
    global $wpdb;
    $current_user = wp_get_current_user();
    $couponList = $wpdb->get_results("SELECT * FROM `wp_coupons` WHERE user_id='{$current_user->ID}'");
    include_once('template/vouchers.php');
}

add_action('wp_ajax_add_coupons', 'add_coupons');

function add_coupons()
{
    global $wpdb;
    $res = filter_input(INPUT_POST, 'res', FILTER_SANITIZE_STRING);
    $post_id = filter_input(INPUT_POST, 'post_id', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 999999)));
    if (!empty($res) && $post_id > 1) {
        $current_user = wp_get_current_user();
        $scheme = get_post_meta($post_id, '_tp_vsicon_args', true);
        $user_points = intval(get_user_meta(wp_get_current_user()->ID, 'game_points', true));
        if ($user_points < $scheme['pts_to_play'])
            wp_die();
        $res = explode(':', $res);
        $i = 0;
        switch ($res[1]) {
            case 'flight':
                $i = 0;
                break;
            case 'meal':
                $i = 2;
                break;
            case 'vehicle':
                $i = 4;
                break;
            case 'massage':
                $i = 6;
                break;
            case 'helicopter':
                $i = 8;
                break;
            case 'hotel':
                $i = 10;
                break;
            default:
                $i = 0;
        }
        if ($res[0] == 'silver')
            $i++;

        $args = explode('|', $scheme['icon'][$i]['args']);
        $discount = $args[0];
        $figure = $args[1];

        if (intval($args[3]) > 0) {
            //decrease availability
            $scheme['icon'][$i]['args'] = $args[0] . '|' . $args[1] . '|' . $args[2] . '|' . intval($args[3] - 1);
            $scheme['icon'][$i]['availability'] = intval($args[3] - 1);
            update_post_meta($post_id, '_tp_vsicon_args', $scheme);

            $points = $user_points - $scheme['pts_to_play'];
            update_user_meta(wp_get_current_user()->ID, 'game_points', $points);

            $wpdb->insert('wp_coupons', array('user_id' => $current_user->ID, 'type' => htmlspecialchars($res[1]), 'discount' => $discount, 'figure' => $figure));
        }
    }
    echo '1';
    wp_die();
}

add_action('wp_ajax_get_world_cities', 'get_world_cities');

function get_world_cities()
{
    global $wpdb;
    $search = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
    $page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 999999)));
    $pagination = false;
    $sql = "SELECT city as id, concat(concat(city, ', '),country) as text FROM `world_cities`";
    $countSQL = "SELECT COUNT(*) FROM `world_cities`";
    if (!empty($search)) {
        $sql .= " WHERE city LIKE '%{$search}%' OR country LIKE '%{$search}%'";
        $countSQL .= " WHERE city LIKE '%{$search}%' OR country LIKE '%{$search}%'";
    }
    if (!empty($page)) {
        $sql .= " LIMIT " . (($page-1) * 10) . ",10";
    }
    $world_cities = $wpdb->get_results($sql, ARRAY_A);
    if ($wpdb->get_var($countSQL) > ($page * 10))
        $pagination = true;
    echo json_encode(array('results' => $world_cities, 'pagination' => array('more' => $pagination)));
    wp_die();
}

add_action('wp_ajax_add_career', 'add_career');

function add_career()
{
    global $wpdb;

    $job_title = filter_input(INPUT_POST, 'job_title', FILTER_SANITIZE_STRING);
    $company = filter_input(INPUT_POST, 'company', FILTER_SANITIZE_STRING);
    $location = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING);
    $working = filter_input(INPUT_POST, 'working', FILTER_SANITIZE_STRING)==1?1:0;
    $startdate = filter_input(INPUT_POST, 'startdate', FILTER_SANITIZE_STRING);
    $enddate = filter_input(INPUT_POST, 'enddate', FILTER_SANITIZE_STRING);
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);

    $userid = wp_get_current_user()->ID;

    if($working==1) {
        $enddate=date("Y-m-d");
    }

    if (!empty($job_title) && !empty($company) && !empty($location) && !empty($startdate) && !empty($enddate)) {
        $result = $wpdb->insert(
            'wp_career',
            array(
                'userid' => $userid,
                'job_title' => $job_title,
                'company' => $company,
                'location' => $location,
                'working' => $working,
                'start_date' => date('Y-m-d', strtotime($startdate)),
                'end_date' => date('Y-m-d', strtotime($enddate)),
                'description' => $description
            )
        );
        echo json_encode(array('success' => $result));
    }
    else {
        echo json_encode(array('success' => false, 'msg'=> 'Please fill required fields.'));
    }

    update_profile_completeness();

    wp_die();
}

add_action('wp_ajax_edit_career', 'edit_career');

function edit_career()
{
    global $wpdb;

    $job_title = filter_input(INPUT_POST, 'job_title', FILTER_SANITIZE_STRING);
    $company = filter_input(INPUT_POST, 'company', FILTER_SANITIZE_STRING);
    $location = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING);
    $working = filter_input(INPUT_POST, 'working', FILTER_SANITIZE_STRING)==1?1:0;
    $startdate = filter_input(INPUT_POST, 'startdate', FILTER_SANITIZE_STRING);
    $enddate = filter_input(INPUT_POST, 'enddate', FILTER_SANITIZE_STRING);
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);

    $career_id = filter_input(INPUT_POST, 'career', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1)));

    $userid = wp_get_current_user()->ID;

    if($working==1) {
        $enddate=date("Y-m-d");
    }

    if (!empty($job_title) && !empty($company) && !empty($location) && !empty($startdate) && !empty($enddate)) {
        $result = $wpdb->update(
            'wp_career',
            array(
                'job_title' => $job_title,
                'company' => $company,
                'location' => $location,
                'working' => $working,
                'start_date' => date('Y-m-d', strtotime($startdate)),
                'end_date' => date('Y-m-d', strtotime($enddate)),
                'description' => $description
            ),
            array('id' => $career_id, 'userid'=>$userid)
        );
        echo json_encode(array('success' => $result));
    }
    else {
        echo json_encode(array('success' => false, 'msg'=> 'Please fill required fields.'));
    }

    wp_die();
}

add_action('wp_ajax_add_education', 'add_education');

function add_education()
{
    global $wpdb;

    $school = filter_input(INPUT_POST, 'school', FILTER_SANITIZE_STRING);
    $degree = filter_input(INPUT_POST, 'degree', FILTER_SANITIZE_STRING);
    $area = filter_input(INPUT_POST, 'area', FILTER_SANITIZE_STRING);
    $startdate = filter_input(INPUT_POST, 'startdate', FILTER_SANITIZE_NUMBER_INT);
    $enddate = filter_input(INPUT_POST, 'enddate', FILTER_SANITIZE_NUMBER_INT);
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);

    $userid = wp_get_current_user()->ID;

    if (!empty($school) && !empty($degree) && !empty($area) && !empty($startdate) && !empty($enddate)) {
        $result = $wpdb->insert(
            'wp_education',
            array(
                'userid' => $userid,
                'school' => $school,
                'degree' => $degree,
                'area' => $area,
                'startdate' => $startdate,
                'enddate' => $enddate,
                'description' => $description
            )
        );
        echo json_encode(array('success' => $result));
    } else {
        echo json_encode(array('success' => false, 'msg' => 'Please fill required fields.'));
    }

    update_profile_completeness();

    wp_die();
}

add_action('wp_ajax_edit_education', 'edit_education');

function edit_education()
{
    global $wpdb;

    $school = filter_input(INPUT_POST, 'school', FILTER_SANITIZE_STRING);
    $degree = filter_input(INPUT_POST, 'degree', FILTER_SANITIZE_STRING);
    $area = filter_input(INPUT_POST, 'area', FILTER_SANITIZE_STRING);
    $startdate = filter_input(INPUT_POST, 'startdate', FILTER_SANITIZE_NUMBER_INT);
    $enddate = filter_input(INPUT_POST, 'enddate', FILTER_SANITIZE_NUMBER_INT);
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);

    $education_id = filter_input(INPUT_POST, 'education', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1)));

    $userid = wp_get_current_user()->ID;

    if (!empty($school) && !empty($degree) && !empty($area) && !empty($startdate) && !empty($enddate)) {
        $result = $wpdb->update(
            'wp_education',
            array(
                'school' => $school,
                'degree' => $degree,
                'area' => $area,
                'startdate' => $startdate,
                'enddate' => $enddate,
                'description' => $description
            ),
            array('id' => $education_id, 'userid' => $userid)
        );
        echo json_encode(array('success' => $result));
    } else {
        echo json_encode(array('success' => false, 'msg' => 'Please fill required fields.'));
    }

    wp_die();
}

add_action('wp_ajax_add_social_view', 'add_social_view');

function add_social_view()
{
    global $wpdb;

    $user_id = filter_input(INPUT_POST, 'user_id', FILTER_VALIDATE_INT, array('options'=>array('default'=>0, 'min_range'=>0)));
    $parent_id = filter_input(INPUT_POST, 'parent_id', FILTER_VALIDATE_INT, array('options'=>array('default'=>0, 'min_range'=>0)));
    $content = filter_input(INPUT_POST, 'content', FILTER_SANITIZE_STRING);
    $rating = filter_input(INPUT_POST, 'rating', FILTER_VALIDATE_INT, array('options'=>array('default'=>5, 'min_range'=>1, 'max_range'=>5)));

    $creator_id = wp_get_current_user()->ID;

    if($user_id==0 || $wpdb->get_var("SELECT COUNT(*) FROM wp_users WHERE ID='{$user_id}'")==0) {
        wp_die();
    }

    if($parent_id!=0 && $wpdb->get_var("SELECT COUNT(*) FROM social_reviews WHERE id='{$parent_id}'")==0) {
        wp_die();
    }

    if($parent_id!=0 && $wpdb->get_var("SELECT user_id FROM `social_reviews` WHERE id='{$parent_id}'")!=$creator_id) {
        wp_die();
    }

    if($parent_id==0 && $user_id == $creator_id) {
        echo json_encode(array('success' => false, 'msg' => "You can't review yourself."));
    }
    else if (!empty($content)) {
        $result = $wpdb->insert(
            'social_reviews',
            array(
                'user_id' => $user_id,
                'parent_id' => $parent_id,
                'creator_id' => $creator_id,
                'rating' => $rating,
                'review' => $content
            )
        );
        echo json_encode(array('success' => $result));
    } else {
        echo json_encode(array('success' => false, 'msg' => 'Please input your review.'));
    }

    wp_die();
}

add_action('wp_ajax_get_friends', 'ajax_get_friends');
function ajax_get_friends()
{
    global $wpdb;

    $kw = filter_input(INPUT_POST, 'kw', FILTER_SANITIZE_STRING);
    $userid = wp_get_current_user()->ID;

    $friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE user_login LIKE '%{$kw}%' AND (ID IN(
        SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$userid}')
        OR ID IN(
            SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$userid}'))", ARRAY_A);

    echo json_encode($friends);
    wp_die();
}

add_action('wp_ajax_upload_user_cover', 'ajax_upload_user_cover');
function ajax_upload_user_cover()
{
    $cover = filter_input(INPUT_GET, 'cover', FILTER_SANITIZE_NUMBER_INT);
    $userid = wp_get_current_user()->ID;

    update_user_meta($userid,'cover', $cover);
    update_user_meta($userid, 'cover_update_time', time());

    echo '1';
    wp_die();
}

add_action('wp_ajax_block_user', 'ajax_block_user');
function ajax_block_user()
{
    global $wpdb;

    $block_userid = filter_input(INPUT_GET, 'userid', FILTER_SANITIZE_NUMBER_INT);
    $userid = wp_get_current_user()->ID;

    if($block_userid==$userid) {
        return;
    }

    if($wpdb->get_var("SELECT COUNT(*) FROM wp_users_block WHERE userid='{$userid}' AND block_userid='{$block_userid}'")==0) {
        $wpdb->insert('wp_users_block', array('userid'=>$userid, 'block_userid'=>$block_userid));
        echo json_encode(array('status'=>1, 'msg'=>'blocked'));
    }
    else {
        $wpdb->delete('wp_users_block', array('userid'=>$userid, 'block_userid'=>$block_userid));
        echo json_encode(array('status'=>0, 'msg'=>'unblocked'));
    }

    wp_die();
}

add_action('wp_ajax_report_user', 'ajax_report_user');
function ajax_report_user()
{
    global $wpdb;

    $report_userid = filter_input(INPUT_POST, 'userid', FILTER_SANITIZE_NUMBER_INT);
    $content = filter_input(INPUT_POST, 'content', FILTER_SANITIZE_STRING);
    $userid = wp_get_current_user()->ID;

    if(empty($report_userid) || empty($content)) {
        wp_die();
    }

    if($report_userid==$userid) {
        wp_die();
    }

    $content='<a href="'.home_url('/my-time-line/?user='.get_user_by('id',$userid)->user_login).'">'.um_get_display_name($userid).'</a> report '.um_get_display_name($report_userid). ' has follow issue: <br><br>'.$content.'<br><br><a href="'.home_url('/my-time-line/?user='.get_user_by('id',$report_userid)->user_login).'">Check the profile</a>';

    wp_mail('tourfrombali@gmail.com', 'Report profile', $content);

    echo json_encode(array('success'=>true));

    wp_die();
}

function disable_deleted_user_login($username)
{
    if (is_wp_error($username)) {
        return $username;
    }
    $user = get_user_by('login', $username);
    $deleted = get_user_meta($user->ID, 'deleted', true);
    if ($deleted) {
        return new WP_Error('user_deleted', __('<strong>ERROR</strong>:Invalid username.'));
    }

    return $username;
}

//update Profile Completeness value
function update_profile_completeness()
{
    global $wpdb;
    $current_user = wp_get_current_user();
    $profile_completeness = get_user_meta($current_user->ID, 'profile_completeness', true);

    $complete_count = 0;
    $profile_meta = array(
        'gender', 'marital_status', 'date_of_birth', 'hobby', 'hometown', 'current_city',
        'height', 'drinking', 'edu_level', 'smoking', 'political_view', 'seeking', 'religion',
        'language', 'description'
    );
    if (!empty($wpdb->get_var("SELECT phone FROM `user` WHERE `username`='{$current_user->user_login}'")))
        $complete_count++;
    if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_career` WHERE userid={$current_user->ID}") > 0) {
        $complete_count++;
    }
    if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_education` WHERE userid={$current_user->ID}") > 0) {
        $complete_count++;
    }
    foreach ($profile_meta as $m) {
        if (!empty(get_user_meta($current_user->ID, $m, true))) {
            $complete_count++;
        }
    }
    $profile_completeness = round(($complete_count / (count($profile_meta) + 3)), 4);
    update_user_meta($current_user->ID, 'profile_completeness', $profile_completeness);
}

add_filter('wp_authenticate_user', 'disable_deleted_user_login', 1, 2);

add_action('wp_enqueue_scripts', 'add_fontsome5_and_utilities');

function add_fontsome5_and_utilities()
{
    wp_deregister_style('font-awesome');
    wp_register_style("font-awesome", '//use.fontawesome.com/releases/v5.4.1/css/all.css', null, '5.4.1');
    wp_enqueue_style("font-awesome");
    $template = basename(get_page_template());
    if ($template != "tpl-mytimeline.php") {
        wp_enqueue_style("utilities", get_template_directory_uri() . '/css/utilities.css', null, '1.0.1');
    }
    wp_enqueue_style("user-profile", get_template_directory_uri() . '/css/user-profile.css', null, '1.0.5');
}
/*
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});
*/
