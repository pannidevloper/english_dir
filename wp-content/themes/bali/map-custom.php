<?php
get_header(); 
/* Template Name: map-custom */

if(isset($_GET['lat']) && $_GET['lng'] ){

  $Lat = $_GET['lat'];
  $Lng = $_GET['lng'];

}
else{
  $Lat = "";
  $Lng = "";
}
?>
    <div id="map_2"></div>

   <style>
      #map_2{
        height: 500px;
      	width: 100%;
      	margin-top: 50px;
      }
      
    </style>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwYmgCVEm-e3EfduQ3RBBK8oZIVJKgSPA&callback=initMap"></script>
    <script>

      // This example displays a marker at the center of Australia.
      // When the user clicks the marker, an info window opens.

      function initMap() {
        var loc = {lat: <?php echo $Lat  ?>, lng: <?php echo $Lng  ?> };
        var map = new google.maps.Map(document.getElementById('map_2'), {
          zoom: 14,
          center: loc
        });

    

        var marker = new google.maps.Marker({
          position: loc,
          map: map,
          title:"Location Traced by Travpart"
        
        });
       
      }
    </script>
    
<?php
get_footer();
?>