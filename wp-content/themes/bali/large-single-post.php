<?php
/*
Template Name: Large single Post
*/
?>
	
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">


<style>
    body{
        font-family: "Times New Roman", Georgia, Serif;
    }
    .single-comment-s button{
	    position: absolute;
	    right: 7px;
	    top: 1px;
		border: 1px solid #ddd;
	    background: #fff;
	    padding: 2px 6px;
	    margin-left: 5px;
	    color: #737373;
    }

    .single-comment-s{
        margin-top: 15px;
        margin-bottom: 15px;
        position: relative;
    }

    .single-comment-s input{
        width: 100%;
        border-radius: 5px;
        border: none;
        background-color: #fff;
        border: 1px solid #ccd0d5;
        border-radius: 16px;
        padding: 3px 15px;
        font-size: 14px;
        position: relative;
        top: 2px;
    }

    .single-comment-s img{
	    border-radius: 100%;
	    width: 30px;
	    margin-left: 10px;
	    margin-right: 5px;
        position: absolute;
    }

    .singthumbs-up{
        display: inline-block;
        width: 32%;
        text-align: center;
        padding: 10px;
    }

    .singthumbs-up a{
       color: #4c4c4c;
       font-size: 13px;
    }

    .single-thumbs-up{
        margin-top: 15px;
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }

    a.moreOpt{
        margin-left: 5px;
    }
    .SuserT-1row a, .SuserT-2row a{
        color: #194aa5;
    }

    .SuserT-1row span{
        margin: 5px;
    }

    .SuserT-extraTxt{
        margin-left: 48px;
        margin-top: 2px;
    }

    .Suser-thumbnails a{
        position: relative;
        bottom: 5px;
        left: 5px;
        font-size: 11px;
        color: #114bb5;
        font-weight: 500;
    }   

    .Suser-thumbnails p{
	    display: inline-block;
	    right: 25px;
	    top: 12px;
	    position: relative;
	    margin-bottom: 0;
    }

    .Suser-thumbnails img{
        width: 35px;
        border-radius: 100%;
        padding: 2px;
        margin-top: 7px;
        border: 1px solid #b1b1b163;
        margin-left: 5px;
    }

    .single-post-body img{
        width: 100%;
    }

    .single-post-header h2{
        font-size: 18px!important;
        display: inline-block;
        margin-bottom: 0;
        color: #fff!important;
    }
    .sPback{
        display: inline-block;
        cursor: pointer;
        float: left;
    }

    .sPback i{
        font-size: 18px;
        color: #fff;
    }

    .single-post-header{
        background: #1abc9c;
        padding: 10px;
        text-align: center;
    }

    .single-post-mobile{
        position: fixed;
        background: #fff;
        width: 100%;
        height: 100%;
        z-index: 99999;
        top: 0;
        bottom: 0;
        left: 0;
        overflow-y: scroll;
    }

	.commnet-area{
		margin: 0px 60px 0 50px;
	}

</style>

</head>
<body>
	
<div class="single-post-mobile">
	<div class="single-post-header">
		<div class="sPback">
			<a href="javascript:close_window();">
			<i class="fas fa-arrow-left"></i>
			</a>
		</div>

		<h2>Timeline Photos</h2>
	</div>

	<div class="single-post-body">
	    <img src="https://www.travpart.com/English/wp-content/themes/bali/assets/img/rome.jpg">
	</div>	

	<div class="single-user-postSection">
		<div class="Suser-thumbnails">
			<img alt="" src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/1120/profile_photo-80x80.jpg">
			<a href="#">Nitish </a>
			<p> hiiiii</p>
		</div>

		<div class="SuserT-extraTxt">
			<div class="SuserT-1row">
				<a href="#">Timeline Photos</a>
				<span> Wednesday at 8:11 PM </span>
				<i class="fas fa-globe-europe"></i>
			</div>
			<div class="SuserT-2row">
				<a href="#">View Full Size</a>
				<a class="moreOpt" href="#">More Options</a>
			</div>

		</div>

		<div class="single-thumbs-up">
        	<div class="singthumbs-up">
				<a href="#">
					<i class="far fa-thumbs-up"></i>
					<span>Like</span>
				</a>
			</div>

			<div class="singthumbs-up">
				<a href="#">
					<i class="far fa-comment-alt"></i>
					<span>Comment</span>
				</a>
			</div>

			<div class="singthumbs-up">
				<a href="#">
					<i class="fas fa-share"></i>
					<span>Share</span>
				</a>
			</div>
    	</div>

		<div class="single-comment-s">
			<img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/261/profile_photo-40x40.jpg?1578072028">
			<div class="commnet-area">
				<input placeholder="Write a Comment" type="text" required="">
			</div>
			<button> Post </button>
		</div>


	</div>	
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>




<script>
function close_window() {
  if (confirm("Close Window?")) {
    close();
  }
}
</script>

</body>
</html>

