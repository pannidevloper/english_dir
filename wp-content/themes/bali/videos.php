<?php
/*
Template Name: videospage
*/

global $wpdb;

if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
}

$pageNum = filter_input(INPUT_GET, 'pageNum', FILTER_SANITIZE_NUMBER_INT);
$pageNum = ($pageNum > 0) ? ($pageNum - 1) : 0;
$post_per_page = 15;
$postOffset = $pageNum * $post_per_page;
$hide_sql = '';
if(is_user_logged_in()) {
    $hide_sql = " AND wp_posts.ID NOT IN(SELECT short_post_id FROM `short_posts_hide_from_timeline` WHERE user_id='{$current_user->ID}') ";
}
$videoPosts = $wpdb->get_results("SELECT wp_posts.ID,post_author,user_login,post_title,post_date,post_content FROM `wp_posts`,`wp_postmeta`,`wp_users`
WHERE wp_posts.ID=wp_postmeta.post_id AND post_author=wp_users.ID AND `meta_key`='attachment' AND post_type='shortpost' AND post_status='publish'
AND meta_value IN (SELECT ID FROM `wp_posts` WHERE `post_mime_type` LIKE '%video%')
{$hide_sql}
ORDER BY post_date DESC LIMIT {$postOffset},{$post_per_page}");

get_header();
?>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.masonry.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/video.css?v15" />
<input id="website-url" type="hidden" value="<?php echo home_url(); ?>" />
<input id="admin-ajax-url" type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" />
<script src="<?php echo get_template_directory_uri(); ?>/js/videoposts.js?v30"></script>
<?php if ($_GET['device'] == 'app') { ?>

      <style>
      .main_wrapper_video{
	  	margin: 0px !important;
	  }
      .mobileapp{
      
	  	display: none !important; 
	  }
	  #header{
	  	display: none !important;	  	
	  }
	  </style>
	 <?php } ?>
<div class="main_wrapper_video">
	<!-- Main wrapper balouch khan-->

	<div id="loading_next_page" class="row">

		<?php
		foreach ($videoPosts as $item) {
            if (is_user_logged_in()) {
                $liked = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE user_id = '{$current_user->ID}' AND feed_id = '$item->ID'");
			}
			else {
				$liked = 0;
			}
			$like_count = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE feed_id = '$item->ID'");
		?>

			<div class="col-md-4 post-md-4 box">
				<div class="post-area-content">
					<div class="post-content">
						<div class="post_header">
							<div class="post_profile_img">
								<div class="for_img">
									<a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>">
										<img alt="" src="<?php echo um_get_avatar_url(um_get_avatar('', $item->post_author, 52)); ?>">
									</a>
								</div>
							</div>
							<?php
							$display_name = um_user('display_name'); ?>
							<div class="header_user_content">
								<div class="content_mobile">
									<h4 class="mobile_agent_name" style="margin-bottom: 20px;margin-top: 5px;">
										<a class="title_post_username" href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>"><?php echo $display_name; ?> </a>
										<span class="date_pub_mobile">
											<p style="margin: 0px; font-size: 10px">
												<?php echo human_time_diff(strtotime($item->post_date), current_time('timestamp')); ?>
											</p>
										</span>
										<?php if (get_post_meta($item->ID, 'lookingfriend', true) == 1) { ?>
											<!--<p class="changes_for_mob button_green" style="background: #22B14C;color: #fff;padding: 5px 10px;border-radius: 3px;position: absolute;">Looking For A Travel Friend</p>-->
											<a href="https://www.travpart.com/English/people/" class="changes_for_mob  button_green">Looking For A Travel Friend</a>
										<?php } ?>
									</h4>
									<p class="mobiel_feeling"></p>

									<!-- Right dot dropdown -->
									<div class="right-section-dot dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<a href="#" class="Pdeltebtn"><i class="fas fa-ellipsis-h"></i></a>
									</div>
									<!-- Right dot dropdown -->
									<?php
									$post_edit_text = preg_replace('/[^A-Za-z0-9 ]/', '', $item->post_content);
									?>
									<!-- Post Delete Option PC-->
									<div id="PostDId" class="PostDelete dropdown-menu" role="menu" aria-labelledby="menu1">
	<?php if (is_user_logged_in()) {
		$author_id = get_post_field('post_author', $item->ID);
			if ($author_id == get_current_user_id()) { ?>
												<li class="drp-edit" onclick="editid('<?php echo $item->ID; ?>','<?php echo $post_edit_text; ?>')"><a href="#">Edit Post</a></li>
												<li class="drp-delete" onclick="getid('<?php echo $item->ID; ?>')"><a href="#">Delete</a></li>
										<?php
											}
										} ?>
										<li><a href="#">Turn On notifications For this post</a></li>
										<li class="ShowITc"><a href="#">Show in tab</a></li>
										<li class="hide-from-timeline" postid="<?php echo $item->ID ?>"><a href="#">Hide From timeline</a></li>
										<li><a target="_blank" href="https://www.travpart.com/English/large-single-post/">Copy link to this post</a></li>

									</div>
									<!--End Post Delete Option -->
								</div>
							</div>
							<div class="clearfix"></div>
							<?php if (!empty(get_post_meta($item->ID, 'feeling', true))) { ?>
								<span class="short-post-feeling">
									<?php echo $item->user_login . ' is feeling ' . base64_decode(get_post_meta($item->ID, 'feeling', true)); ?>
								</span>
							<?php } else if (!empty(get_post_meta($item->ID, 'tag_user_id', true))) { ?>
								<span class="short-post-tagfriend">
									<?php echo $item->user_login . ' is with ' . um_get_display_name(get_post_meta($item->ID, 'tag_user_id', true)); ?>
								</span>
							<?php } ?>
						</div>

						<?php
						$attachment = get_post_thumbnail_id($value->ID);
						$attachment_url = '';
						if (empty($attachment)) {
							$attachment = get_post_meta($item->ID, 'attachment', true);
						}
						if (!empty($attachment)) {
							if (wp_attachment_is('image', $attachment)) {
								$attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
							} else {
								$attachment_url = wp_get_attachment_url($attachment);
							}
						} else if (!empty(get_post_meta($item->ID, 'tour_img', true))) {
							$attachment_url = get_post_meta($item->ID, 'tour_img', true);
						}
						$tour_post_id = get_post_meta($item->ID, 'tour_post', true);
						if (!empty($attachment_url)) {
						?>
							<div class="post-image">
								<?php if (!is_array(get_post_meta($item->ID, 'attachment'))) { ?>
									<?php if (empty($attachment) || wp_attachment_is('image', $attachment)) { ?>
										<a href="<?php echo !empty($tour_post_id) ? get_permalink($tour_post_url) : '#'; ?>">
											<img src="<?php echo $attachment_url; ?>">
										</a>
									<?php } else {
										$video_thumbnail_url = get_post_meta($attachment, 'kgvid-poster-url', true) ?>
										<!--<video width="400" preload="metadata" controls="controls">
											<source src="<?php //echo $attachment_url; ?>#t=0.5" type="video/mp4" >
										</video>-->
										 
								<video controls="controls" poster="<?php echo $video_thumbnail_url ?>">
								 <source src="<?php echo $attachment_url; ?>" type="video/mp4"></source>
								 <source src="<?php echo $attachment_url; ?>" type="video/webm"></source>
								 <source src="<?php echo $attachment_url; ?>" type="video/ogg"></source>
								 <p class="vjs-no-js">
								 To view this video please consider upgrading to a web browser that
								 <a href="http://videojs.com/html5-video-support/" target="_blank">
								 supports HTML5 video
								 </a>
								 </p>
								</video>
									<?php } ?>
									<?php } else {
									foreach (get_post_meta($item->ID, 'attachment') as $row) {
										$attachment_url = wp_get_attachment_url($row);
									?>
										<?php if (wp_attachment_is('image', $row)) { ?>
											<div class="image_div" style="background: url('<?php echo $attachment_url; ?>');background-size: cover;background-position: center center;">
												<a href="<?php echo $attachment_url; ?>" data-fancybox="gallery"></a>
											</div>
										<?php } else {
											$video_thumbnail_url = get_post_meta($row, 'kgvid-poster-url', true); ?>
											<!--<video width="400" controls="controls" poster="<?php //echo $video_thumbnail_url ?>">
												<source src="<?php //echo $attachment_url; ?>" type="video/mp4" >
											</video>-->
											
											<video controls
												 poster="<?php echo $video_thumbnail_url ?>"
											 >
											 <source src="<?php echo $attachment_url; ?>" type="video/mp4"></source>
											 <source src="<?php echo $attachment_url; ?>" type="video/webm"></source>
											 <source src="<?php echo $attachment_url; ?>" type="video/ogg"></source>
											 <p class="vjs-no-js">
											 To view this video please consider upgrading to a web browser that
											 <a href="http://videojs.com/html5-video-support/" target="_blank">
											 supports HTML5 video
											 </a>
											 </p>
											</video>
											
										<?php } ?>
								<?php
									}
								} ?>
							</div>
						<?php } ?>

						<?php if (!empty(get_post_meta($item->ID, 'location', true))) { ?>
							<div class="post-category">
								<a href="#"><?php echo get_post_meta($item->ID, 'location', true); ?></a>
							</div>
						<?php } ?>

						<div class="post-content-txt">
							<div class="more">
								<?php echo strip_tags($item->post_content); ?>
							</div>
						</div>

						<?php if (!empty(get_post_meta($item->ID, 'tour_post', true))) { ?>
							<div class="post-category">
								<a href="<?php echo get_permalink(get_post_meta($item->ID, 'tour_post', true)); ?>">
									<?php echo get_post_meta($item->ID, 'tour_title', true); ?>
								</a>
							</div>
							<br>
						<?php } ?>
						<div class="buttons_for_mobilepost">
							<hr class="style-mobile-six" />
							<div class="main_buttons_div">
								<div class="like_mobile <?php echo is_user_logged_in()?'like-the-post':'login-alert' ?>">
									<a href="#" feedid="<?php echo $item->ID; ?>">
										<i class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>
										<span><?php echo $like_count; ?></span>
									</a>
								</div>
								<div class="comment_mobile">
									<a href="#">
										<i class="far fa-comment-alt"></i>
										Comment
									</a>
								</div>
								<div class="share_mobile">
									<!-- pc view -->
									<!-- <a href="#" onclick="share_feed(<?php //echo $item->ID; 
																			?>)">
							<i class="fas fa-share"></i> Share </a> -->

									<a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
										<i class="fas fa-share"></i> Share</a>


									<!--Share My DropDown PC-->
									<div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2" aria-labelledby="menu2" style="right: 40px;left: inherit;">
										<li onclick="share_feed(<?php echo $item->ID; ?>)">Share To Feed</li>
										<li><a target="_blank" href="https://web.whatsapp.com/send?text=<?php echo substr($value->post_content, 0, 80) . "  ( see-more ) "; ?>https://www.travpart.com/English/my-time-line/?user=<?php echo esc_html($username); ?>"><div class="wh_video_icon_div"><i class="fab fa-whatsapp share_wh_video_i"></i></div>Share to whats app</a></li>

										<li><a  href="#" onclick="copy_link('<?php echo $p->ID; ?>')"><i class="far fa-copy share_copy_link_video_i"></i>Copy link to this post</a></li>

										<li class="mores"><a class="a2a_dd" href="#"><i class="fas fa-plus-square"></i> More...</a></li>

									</div>

									<!--End My DropDown-->


									<!-- End Pc view -->
								</div>
							</div>
							<?php
							$comments = $wpdb->get_results("SELECT comment,user_id,user_login FROM `user_feed_comments`,`wp_users` WHERE wp_users.ID=user_id AND `feed_id` = '{$item->ID}' ORDER BY user_feed_comments.id DESC");
							if (!empty($comments)) {
								$a=0;
								foreach ($comments as $comment) {
									$a++;
							?>		
							<?php if($_GET['debug']!=1){ ?>
									<div class="user-comment-area">
										<?php echo get_avatar($comment->user_id, 50);  ?>
										<div class="another-user-section">
											<a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $comment->user_login; ?>"><?php echo um_get_display_name($comment->user_id); ?></a>
											<span><?php echo $comment->comment;  ?></span>
										</div> 
									</div>
							<?php } ?>		
							<?php if($_GET['debug']==1){ ?>
								
								<div class="user-comment-area_2">
									<div class="image_user_comment">
										<?php echo get_avatar($comment->user_id, 50);  ?>
									</div>
									<div class="another-user-section_1">
										<div class="another-user-section_2">
											<a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $comment->user_login; ?>"><?php echo um_get_display_name($comment->user_id); ?></a>
											<span><?php echo $comment->comment;  ?></span>
										</div> 
										<div class="like_div_2">
											<ul class="lst_like_rpl">
												<li class="llike">
													<a href="#">Like</a>
												</li>
												<li>-</li>
												<li class="lrepky click_repl" data-cmid="comid<?php echo $item->ID; ?><?php echo $a; ?>">
													<a href="#">Reply</a>
												</li>
												<li>-</li>
												<li class="lmin">1m</li>
											</ul>
											<?php if(is_user_logged_in()) { ?>  
												<div class="user-comment-area_reply comid<?php echo $item->ID; ?><?php echo $a; ?>">
													<?php echo get_avatar($current_user->ID, 50);  ?>
													<input placeholder="Write a Comment" type="text" name="feed_comment" required="">
													<input type="hidden" name="feed_id" value="<?php echo $item->ID; ?>">
												</div>
											<?php } ?> 
										</div>	
									</div> 
								</div>
							<?php } ?>
							<?php }
							} ?>

							<?php if(is_user_logged_in()) { ?>
							<div class=" user-comment-area submit-comment">
								<?php echo get_avatar($current_user->ID, 50);  ?>
								<input placeholder="Write a Comment" type="text" name="feed_comment" required="">
								<input type="hidden" name="feed_id" value="<?php echo $item->ID; ?>">
							</div>
							<?php } ?>
						</div>
					</div>

				</div>
			</div>
		<?php } ?>
	</div>
</div>
<style type="text/css">
    #footer {
        display: none;
    }
    .wh_video_icon_div{
        display: inline-block;
        background-color: green;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 5px;
    }
    i.fab.fa-whatsapp.share_wh_video_i{
        color: white;
        font-size: 17px;
        position: relative;
        top: 4px;
        left: 6px;
    }
    i.far.fa-copy.share_copy_link_video_i{
        position: relative;
        margin-right: 5px;
        top: 3px;
        font-size: 22px;
        margin-left: 3px;
        margin-bottom: 5px;
    }
    .right-section-dot i.fa-ellipsis-h{
    	text-rendering: optimizeLegibility;
    }
    .main_buttons_div div.like_mobile a i.far.fa-thumbs-up, .main_buttons_div div.comment_mobile a i.far.fa-comment-alt, .main_buttons_div div.share_mobile a i.fas.fa-share{
    	text-rendering: optimizeLegibility;
    }
    .main_buttons_div div a{
    	color: gray;
    }
</style>
<?php
get_footer();
