<?php
/*
Template Name: Following
*/

global $wpdb;
get_header();

if (!is_user_logged_in()) {
   // exit(wp_redirect(home_url('timeline')));
}
if (isset($_GET['user'])) {

        $username = htmlspecialchars( $_GET['user'] );
        $user = get_user_by('login', htmlspecialchars($_GET['user']));
        if (is_numeric($username) AND $username>0) {

        $user = get_user_by('id',  htmlspecialchars ($_GET['user']));
        $username = $user->user_login;
        }

        if($user==NULL){

        exit(wp_redirect(home_url('my-time-line?user='.wp_get_current_user()->user_login)));

        }
        $wp_user_id = $user->ID;
    }
    else{

        $wp_user_id =  wp_get_current_user()->ID;
    }


$get_followers = $wpdb->get_results("SELECT  sf_agent_id FROM `social_follow`  WHERE sf_user_id = '$wp_user_id'");
?>
 
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/friends_connect.js?1694"></script>
<div class="following_main_area">
    <div class="row">
        <div class="col-md-12">
            <div class="following_header_txt">
                <h2>FOLLOWING</h2>
            </div>
        </div>
    </div>

<?php foreach ($get_followers as $single) { 

$logged_in = wp_get_current_user();
$Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '{$single->sf_agent_id}' AND user1 = '{$logged_in->ID}'");
if($Data==NULL){

   $Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user1 = '{$single->sf_agent_id}' AND user2 = '{$logged_in->ID}'"); 
}
$btn_icon = $Data->status;
if (empty($Data)) {
    $button_text = '<i class="fas fa-user-plus"></i>'.'Connect';
    $_button_class= 'connect_button';
} elseif ($Data->status == 0) {
    $button_text = '<i class="fas fa-user-minus"></i>'.'Request <br /> sent';
    $_button_class = 'connect_button_sent';
} elseif ($Data->status == 1) {
    $button_text = 'Connected';
    $_button_class = 'connect_button_connected';
}

$followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$current_user->ID' AND sf_agent_id = '$single->sf_agent_id'", ARRAY_A);

          if (!empty($followData)) {
 
            $button_following ="Following";
            $following=1;
            //$color= "#dad8d8";
            //$fcolor='#000';
            //$font_weight='bold';
            $classs = "following_color";
            $connIcon = ' <i class="fas fa-wifi" id="plus"></i>';
          //  continue;
        }
        else{
            $following=0;
            $classs = "";
            $connIcon = ' <i class="fas fa-wifi" id="plus"></i>';
            $button_following ="Follow";
 }

$author_obj = get_user_by('id', $single->sf_agent_id);
$user_meta = get_userdata($single->sf_agent_id);
    $user_roles = $user_meta->roles;

    if ( in_array( 'um_travel-advisor', $user_roles, true ) ) {
      $role ="Seller";
    }
    elseif ( in_array( 'um_clients', $user_roles, true ) ) {
      $role = "Buyer";
    }
    else
    {
      $role="";
    }
?>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 following_area">
            <div class="following_con_area">
                <div class="following_con_area_img">
                <input type="hidden" name="userid" id='userid' value= "<?php echo $single->sf_agent_id ?>" />
                    <a href="">
                            <?php echo get_avatar( $single->sf_agent_id,150 );  ?>
                        </a>
                </div>
                <?php if ($Data->status == 1) { ?>
                <div class="following_con_area_text button_con_position">
                    <span>
                        <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $author_obj->user_login; ?>">
                            <?php echo um_get_display_name($single->sf_agent_id );  ?>
                        </a>
                    </span>
                </div>
                <?php } else { ?>
                   <div class="following_con_area_text">
                    <span>
                        <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $author_obj->user_login; ?>">
                            <?php echo um_get_display_name($single->sf_agent_id );  ?>
                        </a>
                    </span>
                   </div> 
                 <?php } ?>
                 <?php
                 if (is_user_logged_in()) {?>
                <div class="following_follow_button">
                    <?php if($wp_user_id !=  wp_get_current_user()->ID){ ?>
                     <?php if($single->sf_agent_id !=  wp_get_current_user()->ID) {?>
                        <?php if($role=="Seller"){ ?>  
                            <button  class="btn_follow follow_button1 <?php echo $classs; ?>  color<?php  echo $single->sf_agent_id; ?>" id="<?php  echo $single->sf_agent_id; ?> ">
                                <i class="fas fa-wifi"></i> 
                                <span class="tx"><?php echo $button_following;  ?></span>
                            </button>
                        <?php } ?>  
                    <?php }} ?>
                </div>
            <?php if($single->sf_agent_id !=  wp_get_current_user()->ID) {?>
                <?php if ($Data->status == 1) { ?>
                <div class="following_connect_button cent<?php echo $single->sf_user_id;?> button_con_position">
                 <button class="following_con_btn btn_change_text<?php echo $single->sf_agent_id;?> <?php echo $_button_class; ?> " id= "<?php echo $single->sf_agent_id;?>" >
                        
                     <span class ='buttonid p-connect-area-text' id="<?php echo $single->sf_agent_id;?>"><?php echo $button_text;  ?></span>
                    </button>
                    

                         <button class="p-dots-area_1 hide_<?php echo $single->sf_agent_id;?>">
                            <a class="dropdown-toggle" id="dropdownMenuButton_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdownMenuButton_1" aria-labelledby="dropdownMenuButton_1" id= "<?php echo $single->sf_agent_id;?>">
                              <?php if( $button_text=='Connected' ){ ?> 
                              <!--<a id="<?php echo $single->sf_agent_id;?>" class="dropdown-item connect_button_sent" href="#" >Unconnected</a>-->
                                <a data-id="<?php echo $single->sf_agent_id;?>" class="unconnect dropdown-item" href="#" >Unconnected</a>
                            <?php } ?>
                            </div>
                          </button>
                      </div>
                      <?php } else { ?>
                    <div class="following_connect_button">
                        <button class="following_con_btn <?php echo $_button_class; ?>" id= "<?php echo $single->sf_agent_id;?>" >
                            
                         <span class ='buttonid p-connect-area-text' id= "<?php echo $single->sf_agent_id;?>"><?php echo $button_text;  ?></span>
                        </button>
                    </div>
                    <?php } ?>
                    <?php


                    $get_user_data =$wpdb->get_row("SELECT user_email FROM `wp_users` where  ID= " . $single->sf_agent_id);
                    $user_email = $get_user_data->user_email;

                    if(!empty($user_email)) {

                    $get_user_data_tabl2 = $wpdb->get_row("SELECT * FROM `user` where  email = '{$user_email}'");
                    $user_idd = $get_user_data_tabl2->userid;
                    }
                    $author_obj = get_user_by('id', $single->sf_agent_id);
                    ?>
                
                <div class="following_message_button">
                    <a href="https://www.travpart.com/English/travchat/user/addnewmember.php?user=<?php echo $user_idd; ?>" style="color: white"><button class="btn_msg_profile"><i class="fas fa-comment-dots"></i><span>Message</span></button></a>
                </div>
            
        <?php } }?>
        </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>

    
<?php } ?>

        <?php
        if (empty($get_followers)) { ?>
            <div class="timeline_no_following">This person does not <br>following anyone</div>
        <?php  } ?>

</div>

            
<style type="text/css">
    .button_con_position{
        width: 21% !important;
    }
    .following_color {
        background-color: #dad8d8 !important;
        color:#000 !important;
        font-weight:bold;
    }
    .following_con_area_text span a{
        text-decoration: none;
    }  
    .timeline_no_following{
        text-align: center;
        font-size: 40px;
        color: grey;
        margin-top: 50px;
    }

    .dropdownMenuButton_1{
      right: 0px  !important;
      min-width: 35% !important;
      float: none !important;
      left: auto !important;
    }
    .connect_button_connected{
        border-radius: 5px 0px 0px 5px !important;
        background: #cacaca !important;
        color: #000 !important;
        position: relative;
        padding: 12px 10px;
    }
    .connect_button_connected .p-connect-area-text{
         color: #000 !important;
    }
    
    .greencolor{
         background: #008a66!important;
    }
    .p-dots-area_1 {
        background: #bab1b1;
        padding: 7px 7px;
        outline: none !important;
        border-radius: 0px 5px 5px 0px;
        font-size: 15px;
        position: absolute;
        border: 1.5px solid #bab1b1;
    }
    .following_area{
        box-shadow: 0 0px 5px 0px #afacac;
        padding-top: 15px;
        padding-bottom: 15px;
    }
    .following_con_area{
        width: 100%;
    }
    .following_con_area_img{
        width: 22%;
        display: inline-block;
        vertical-align: middle;
    }
    .following_con_area_img img{
        border-radius: 50%;
        width: 150px;
        height: 150px;
    }
    .following_con_area_text{
        width: 25%;
        display: inline-block;
        vertical-align: middle;
    }
    .following_con_area_text span{
        font-size: 20px;
        text-transform: capitalize;
    }
    .following_connect_button{
        width: 17%;
        display: inline-block;
        vertical-align: middle;
    }
    .following_follow_button{
        width: 17%;
        display: inline-block;
        vertical-align: middle;
    }
    .following_message_button{
        width: 14%;
        display: inline-block;
        vertical-align: middle;
    }
    .btn_msg_profile i{
        font-size: 20px;
        padding-right: 3px;
        position: relative;
        top: 2px;
    }
    .connect_button{
        padding: 12px 10px;
    }
    .connect_button_sent {
        font-size: 12px;
        padding: 8px 10px;
    }
    .following_con_btn {
        width: 120px;
        background: #008a66;
        border: 0px;
        letter-spacing: 1px;
        border-radius: 5px;
        color: #fff;
        line-height: 15px;
        font-size: 13px;
        font-family: "Roboto", Sans-serif;
        cursor: pointer;
    }

    .following_con_btn i {
        font-size: 13px !important;
        padding-right: 3px;
        margin-right: 0px !important;
        padding-left: 0px !important;
    }
    .btn_follow {
        width: 120px;
        background: #008a66;
        padding: 10px 10px;
        border: 0px;
        letter-spacing: 1px;
        right: 13px;
        border-radius: 5px;
        color: #fff;
        font-size: 13px;
        font-family: "Roboto", Sans-serif;
        cursor: pointer;
    }
    .btn_follow i.fa-wifi {
        transform: rotate(55deg);
        position: relative;
        right: 3px;
        top: 0px;
    }
    .btn_msg_profile {
        width: 120px;
        background: #20a5ca;
        padding: 8px 10px;
        border: 0px;
        letter-spacing: 1px;
        border-radius: 5px;
        color: #fff;
        font-size: 13px;
        font-family: "Roboto", Sans-serif;
        cursor: pointer;
    }
    .following_header_txt h2 {
        color: #1A6D84;
        font-size: 30px;
        font-weight: 500;
        line-height: 1.1;
        margin-top: 20px;
        text-align: center;
        margin-bottom: 20px;
    }
    .connect_button_sent {
        background: #dad8d8 !important;
        color: #000 !important;
        font-weight: bold;
        line-height: 10px;
    }
    .connect_button_sent span i {
        position: relative;
        top: 5px;
        right: 5px;
    }
    .following_follow_button button:focus{
        outline: none;
    }
    .following_connect_button button:focus{
       outline: none; 
    } 
    @media (max-width: 600px){
        .connect_button_connected{
            padding: 9px 10px;
            right: 17px;
        }
        .p-dots-area_1 {
            position: relative;
            top: -33px;
            right: -103px;
            padding: 3.8px 7px;
        }
        .following_message_button {
            right: 78px;
        }
        .following_follow_button{
            position: absolute;
            right: 67px;
        }
        .btn_follow {
            padding: 6px 10px !important;
        }
        .timeline_no_following{
            font-size: 35px;
        }
        
        .following_con_area_text{
            width: 33%;
        }
        .following_con_area_img{
            width: 33%;
        }
        .following_con_area_img img{       
            width: 100px;
            height: 100px;
        }
        .following_connect_button{
            position: absolute;
            top: 50px;
            right: 67px;
        }
        .connect_button_sent {
            font-size: 12px;
            padding: 5px 10px;
        }
        .connect_button{
            padding: 9px 10px;
        }
        .following_message_button{
            position: absolute;
            top: 87px;
        }
        .btn_msg_profile{
            padding: 5px 10px;
        }
        .following_con_area_text span{
            font-size: 15px;
        }
        .following_header_txt h2{
            margin-top: 100px;
        }
        body{
            overflow-x: hidden;
        }
    }
</style>

<style type="text/css">
    #footer {
        display: none;
    }
</style>

<?php
get_footer();
?>