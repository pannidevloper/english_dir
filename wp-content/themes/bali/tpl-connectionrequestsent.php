<?php
/*
Template Name: Connection Request Sent
*/


get_header();
?>
<div class="main_area">
        <div class="container new-short-post">
            <div class="row">
                <div class="col-md-12">
                    <div class="header_txt">
                        <h2>Connection Request Sent</h2>
                    </div>
                </div>
            </div>
         <div class="row">
                <div class="col-md-12">   
		<div class="connectionlistwrap">
		 <div class="connectionlist">
		 	<div class="connectionlistitem">
		   	<div class="connectionlistiteminner connectionlistbtninner clearfix">
		   		<div class="conlistleftbtn"><a href="#">You have requested 3 connection </a></div>
		   		<div class="conlistrightbtn"><a href="">Check my received connection</a></div>
		   	</div>
		   </div><!-- End of listitem-->
		   <div class="connectionlistitem">
		   	<div class="connectionlistiteminner clearfix">
		    <div class="connectionphoto"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/100x100.png"/></div>
		     <div class="connectiondetail">
		      <div class="connectionname">Franksiskus</div>
		      <div class="mutualdetail"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/30x30.png"/><b>Azzy</b> and 29 other common <span>connection</span></div>     
		      </div>
		     <div class="connectionbtns">
		    <div class="accbtn"><a href="#">Accept</a></div>
		    <div class="removebtn"><a href="#">Remove <span>x</span></a></div>
		   </div>
		</div>
		  </div><!-- End of listitem-->
		  <div class="connectionlistitem">
		   	<div class="connectionlistiteminner clearfix">
		    <div class="connectionphoto"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/100x100.png"/></div>
		     <div class="connectiondetail">
		      <div class="connectionname">Franksiskus</div>
		      <div class="mutualdetail"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/30x30.png"/><b>Azzy</b> and 29 other common <span>connection</span></div>     
		      </div>
		     <div class="connectionbtns">
		    <div class="accbtn"><a href="#">Accept</a></div>
		    <div class="removebtn"><a href="#">Remove <span>x</span></a></div>
		   </div>
		</div>
		  </div><!-- End of listitem-->
		  <div class="connectionlistitem">
		   	<div class="connectionlistiteminner clearfix">
		    <div class="connectionphoto"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/100x100.png"/></div>
		     <div class="connectiondetail">
		      <div class="connectionname">Franksiskus</div>
		      <div class="mutualdetail"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/30x30.png"/><b>Azzy</b> and 29 other common <span>connection</span></div>     
		      </div>
		     <div class="connectionbtns">
		    <div class="accbtn"><a href="#">Accept</a></div>
		    <div class="removebtn"><a href="#">Remove <span>x</span></a></div>
		   </div>
		</div>
		  </div><!-- End of listitem-->
		  <div class="connectionlistitem">
		   	<div class="connectionlistiteminner clearfix">
		    <div class="connectionphoto"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/100x100.png"/></div>
		     <div class="connectiondetail">
		      <div class="connectionname">Franksiskus</div>
		      <div class="mutualdetail"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/30x30.png"/><b>Azzy</b> and 29 other common <span>Connections</span></div>     
		      </div>
		     <div class="connectionbtns">
		    <div class="accbtn"><a href="#">Accept</a></div>
		    <div class="removebtn"><a href="#">Remove <span>x</span></a></div>
		   </div>
		</div>
		  </div><!-- End of listitem-->
		 </div>
		</div>
	</div>
</div>
</div>
</div>
<style>
.clearfix:after {
  visibility: hidden;
  display: block;
  font-size: 0;
  content: " ";
  clear: both;
  height: 0;
}
.clearfix { display: inline-block; }	
.connectionlist {
    font-family: "Open Sans", Arial, Helvetica, sans-serif;
    font-size: 18px;
}	
.connectionphoto {
    float: left;
    width: 100px;
}
.connectiondetail {
    width: 500px;
    float: left;
    margin: 0 27px;
}
.connectionbtns {
    width: 240px;
    float: left;
}
.connectionname {
    color: #385898;
    font-weight: bold;
    padding-top: 17px;
}	
.connectionlist {
    background-color: #fff;
    box-sizing: border-box;
    padding: 0;
    box-shadow: 0 0px 5px 0px #ccc;
}
.connectionlistitem {
    border-bottom: 1px solid #ccc;
    text-align: center;
    padding-bottom: 10px;
    padding-top: 16px;
}
.connectionlistiteminner {
	text-align: left;
}
.accbtn {
    display: inline-block;
    margin-right: 10px;
}
.accbtn a {
    background-color: #22b1b1;
    color: #fff;
    padding: 6px 28px;
}
.removebtn {
    display: inline-block;
}
.removebtn a {
    border: 1px solid #ccc;
    color: #000;
    padding: 5px 17px;
    font-size: 15px;
}
.mutualdetail {
    font-size: 16px;
}
.connectionbtns {
    width: 240px;
    float: left;
    margin-top: 32px;
}
.mutualdetail img {
    border-radius: 52px;
}
.header_txt h2 {
    text-align: center;
    margin-bottom: 50px;
        color: #1A6D84;
    font-size: 30px;
    text-transform: uppercase;
}
.connectionlistbtninner{
	width: 87%;
}
.conlistleftbtn {
    float: left;
    width: 58%;
}
.conlistleftbtn a {
       background-color: #385898;
    color: #fff;
    padding: 8px 15px;
    border-radius: 4px;
}
.conlistrightbtn {
    float: right;
    width: 30%;
}
.conlistrightbtn a {
    color: #000;
    border: 1px solid #000;
    font-size: 17px;
    padding: 4px 17px;
    box-sizing: border-box;
}
@media only screen and (max-width: 960px) {
.connectiondetail {
    width: 330px;
}
.conlistrightbtn {
    float: right;
    width: 39%;
}
.conlistrightbtn a {  
    font-size: 15px;
    padding: 4px 4px;
}
.conlistleftbtn a {
    padding: 8px 8px;
    font-size: 15px;
}
.conlistleftbtn {
    float: left;
    width: 55%;
}

}
@media only screen and (max-width: 770px) {
.connectionbtns {
    width: 110px;
        margin-top: 20px;
}
.accbtn {
    display: block;
    margin-right: 0;
    margin-bottom: 9px;
}
.accbtn a {
    padding: 6px 23px;
}

}
@media only screen and (max-width: 767px) {
.connectionlistiteminner {
    text-align: center;
}
.connectionphoto {
    float: none;
    width: 100px;
    margin: 0 auto;
}
.connectiondetail {
    width: 100%;
    float: none;
    margin: 0 auto;
}
.connectionbtns {
    width: 240px;
    float: none;
    margin-top: 16px;
    text-align: center;
}
.connectionbtns {
    width: 100%;
    float: none;
    margin-top: 16px;
    text-align: center;
}
.accbtn {
    display: inline-block;
    margin-right: 0;
    margin-bottom: 10px;
}
.conlistrightbtn {
    float: none;
    width: 100%;
    margin-top: 12px;
}
.conlistleftbtn {
    float: none;
    width: 100%;
}
.connectionlistbtninner {
    width: 100%;
}
}


</style>
<style type="text/css">
    #footer {
        display: none;
    }
</style>
<?php
get_footer();
?>