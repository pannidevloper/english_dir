<?php
/* Template Name: Game scheme */
get_header();

$travpart_vo_option=get_option('_travpart_vo_option');
$voucher_scheme=get_posts(array('post_type'=>'voucher_scheme'));

?>

<div style="max-width: 1100px;margin: 0 auto;min-height: 1500px;">
    <!--game scheme start-->
    <style>
        .game-scheme {
            margin-bottom: 60px;
        }

        .game-scheme .scheme-title {
            border-radius: 5px;
            font-size: 30px;
            font-weight: bold;
            letter-spacing: -0.8px;
            text-align: left;
            color: #ffffff;
            padding: 18px 30px;
        }

        .game-scheme .scheme-header {
            list-style: none;
            padding: 0;
            margin: 0;
            display: flex;
            font-size: 16px;
            font-weight: 600;
            margin-top: 10px;
            margin-bottom: 10px;
        }
		
		.game-scheme .scheme-header li:nth-child(1) {
            flex: 1 1 auto;
            text-align: right;
            padding-right: 70px;
        }
		
		.game-scheme .scheme-header li:nth-child(2) {
            flex: 0 0 100px;
            text-align: left;
        }

        .game-scheme .scheme-header li:nth-child(3) {
            flex: 0 0 200px;
            text-align: center;
        }

        .game-scheme .scheme-header li:nth-child(4) {
            flex: 0 0 200px;
            text-align: center;
        }

        .game-scheme .scheme-list {
            list-style: none;
            padding: 0;
            margin: 0;
        }

        .game-scheme .scheme-list li {
            display: flex;
            font-size: 24px;
            border: solid 1px #eeeeee;
            background-color: #ffffff;
            border-top: 0;
            padding-top: 10px;
            padding-bottom: 10px;
            align-items: center;
        }

        .game-scheme .scheme-list li:first-child {
            border-top: 1px solid #eee;
            border-top-right-radius: 5px;
            border-top-left-radius: 5px;
        }

        .game-scheme .scheme-list li:last-child {
            border-top: 1px solid #eee;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
        }

        .game-scheme .scheme-list li>div:nth-child(1) {
            flex: 1 1 auto;
            text-align: left;
            padding-left: 15px;
        }
		
		.game-scheme .scheme-list li>div:nth-child(2) {
            flex: 0 0 100px;
        }

        .game-scheme .scheme-list li>div:nth-child(3) {
            flex: 0 0 200px;
            text-align: center;
        }

        .game-scheme .scheme-list li>div:nth-child(4) {
            flex: 0 0 200px;
            text-align: center;
        }

        .game-scheme i.circle {
            width: 72px;
            height: 72px;
            border: solid 5px transparent;
            background-color: #fff;
            display: inline-flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            margin-right: 25px;
        }

        .game-scheme i.circle .fa,
        .game-scheme i.circle .fas {
            font-size: 36px;
        }

        .game-scheme .scheme-title {
            background-color: #1a6d84;
        }

        .game-gold i.circle {
            border-color: #ffcc00;
            color: #ffcc00;
        }

        .game-sliver i.circle {
            border-color: #c7c7c7;
            color: #999999;
        }

        @media (max-width: 767px) {
            .game-scheme .scheme-header {
                font-size: 12px;
            }

            .game-scheme .scheme-header li:nth-child(1) {
                flex: 1 1 auto;
                text-align: right;
                padding-right: 10px;
            }

            .game-scheme .scheme-header li:nth-child(2) {
                flex: 1 1 33.3%;
                text-align: center;
            }

            .game-scheme .scheme-header li:nth-child(3) {
                flex: 1 1 33.3%;
                text-align: center;
            }

            .game-scheme .scheme-list li {
                display: flex;
                font-size: 16px;
            }

            .game-scheme .scheme-list li>div:nth-child(1) {
                flex: 1 1 33.3%;
                padding-left: 10px;
                text-align: left;
            }

            .game-scheme .scheme-list li>div:nth-child(2) {
                flex: 1 1 33.3%;
                text-align: center;
            }

            .game-scheme .scheme-list li>div:nth-child(3) {
                flex: 1 1 33.3%;
                text-align: center;
            }

            .game-scheme i.circle {
                width: 40px;
                height: 40px;
                border-width: 2px;
                margin-right: 0px;
            }

            .game-scheme i.circle .fa,
            .game-scheme i.circle .fas {
                font-size: 18px;
            }
        }

        .game-scheme-summary {
            margin-bottom: 40px;
        }

        .game-scheme-summary .summary-area:first-child {
            border-radius: 5px;
            background-color: #1a6d84;
            color: #fff;
            padding: 15px;
            margin-bottom: 8px;
        }

        .game-scheme-summary .summary-area:first-child header {
            font-size: 24px;
            font-weight: 300;
            letter-spacing: -0.6px;
        }

        .game-scheme-summary .summary-area:last-child {
            border-radius: 5px;
            border: solid 1px #e5e5e5;
            background-color: #eeeeee;
            color: #333333;
            padding: 30px;
        }

        .game-scheme-summary .summary-area .summary-title {
            font-size: 30px;
            font-weight: bold;
            letter-spacing: -0.8px;
            text-align: left;
            line-height: 1.2;
            color: #1a6d84;
        }

        .game-scheme-summary .summary-area .summary-title-1 {
            font-size: 18px;
            font-weight: 300;
            line-height: normal;
            letter-spacing: -0.5px;
            text-align: left;
            margin-bottom: 10px;
        }

        .game-scheme-summary .summary-star-list {
            list-style: none;
            margin: 0;
            padding: 0;
            font-size: 16px;
            display: flex;
            flex-wrap: wrap;
            margin-left: -15px;
            margin-right: -15px;
        }

        .game-scheme-summary .summary-star-list li {
            padding: 15px;
        }

        .game-scheme-summary .summary-star-list li>i,
        .game-scheme-summary .summary-star-list li>strong,
        .game-scheme-summary .summary-star-list li>span {
            display: block;
        }

        .game-scheme-summary .summary-star-list li>strong {
            margin-top: 10px;
        }

        .game-scheme-summary .summary-star-list li>strong span {
            font-weight: 300 !important;
        }

        .game-scheme-summary .summary-star-list .fa,
        .game-scheme-summary .summary-star-list .fas {
            font-size: 36px;
            color: #f8b136;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 51px;
            height: 51px;
        }

        .game-scheme-summary .summary-star-list .icon-summary-star {
            background: url("https://www.travpart.com/English/wp-content/themes/bali/images/5-star.png") center center no-repeat;
            background-size: contain;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 51px;
            height: 51px;
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            padding-top: 6px;
        }

        .game-scheme-summary .points-list {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .game-scheme-summary .points-list li {
            font-size: 16px;
            display: block;
            margin-top: 3px;
            border-radius: 5px;
            padding: 6px 15px;
        }

        .game-scheme-summary .points-list li .circle {
            border: solid 3px #e5e5e5;
            display: inline-flex;
            border-radius: 50%;
            padding: 2px;
            width: 40px;
            height: 40px;
            align-items: center;
            justify-content: center;
            float: left;
            margin-right: 8px;
        }

        .game-scheme-summary .points-list li .circle .fa {
            color: #e5e5e5;
            font-size: 20px;
        }

        .game-scheme-summary .points-list li.gold-holidays {
            background-color: #4993a7;
        }

        .game-scheme-summary .points-list li.silver-holidays {
            background-color: rgba(73, 147, 167, 0.5);
        }

        .game-scheme-summary .points-list li.bronze-holidays {
            background-color: rgba(73, 147, 167, 0.3);
        }

        .game-scheme-summary .holidays-title {
            font-weight: bold;
        }

        @media (min-width: 768px) {
            .game-scheme-summary {
                display: flex;
            }

            .game-scheme-summary .sep {
                border-right: 1px solid rgba(0, 0, 0, 0.2);
            }

            .game-scheme-summary .summary-area:first-child {
                margin-bottom: 0px;
                margin-right: 8px;
                flex: 0 1 300px;
            }

            .game-scheme-summary .summary-area:last-child {
                flex: 1 1 auto;
            }
        }
        
    </style>
    <div class="game-scheme-summary">
        <div class="summary-area">
            <header>Number of Points needed <strong>to Play</strong></header>
            <ul class="points-list">
			<?php
			$i=0;
			foreach($voucher_scheme as $row) {
				$vsicon_args=get_post_meta($row->ID, '_tp_vsicon_args', true);
				if($i>1)
					break;
				if($vsicon_args['status']==1 && strtotime($vsicon_args['start_date'])<=time()
					&& strtotime($vsicon_args['end_date'])>=time() ) {
					$i++;
			?>
                <li class="gold-holidays">
                    <i class="circle"><i class="fa fa-trophy"></i> </i>
                    <div class="holidays-title"><?php echo $row->post_title; ?></div>
                    <div class="holidays-points"><?php echo $vsicon_args['pts_to_play']; ?> Points</div>
                </li>
			<?php
				}
			}
			?>
            </ul>
        </div>
        <div class="summary-area">
            <div class="summary-title">How to obtain points for vouchers</div>
            <div class="summary-title-1">Feedback from Travel Advisor:</div>
            <ul class="summary-star-list">
                <li>
                    <i class="icon-summary-star">5</i>
                    <strong>5 Star</strong>
                    <span><?php echo intval($travpart_vo_option['tp_pts_fb_5_str']); ?> Points</span>
                </li>
                <li>
                    <i class="icon-summary-star">4</i>
                    <strong>4 Star</strong>
                    <span><?php echo intval($travpart_vo_option['tp_pts_fb_4_str']); ?> Points</span>
                </li>
                <li>
                    <i class="icon-summary-star">3</i>
                    <strong>3 Star</strong>
                    <span><?php echo intval($travpart_vo_option['tp_pts_fb_3_str']); ?> Points</span>
                </li>
                <li>
                    <i class="icon-summary-star">2</i>
                    <strong>2 Star</strong>
                    <span><?php echo intval($travpart_vo_option['tp_pts_fb_2_str']); ?> Points</span>
                </li>
                <li class="sep">
                    <i class="icon-summary-star">1</i>
                    <strong>1 Star</strong>
                    <span><?php echo intval($travpart_vo_option['tp_pts_fb_1_str']); ?> Points</span>
                </li>
                <li>
                    <i class="fa fa-user fa-fw"></i>
                    <strong>Login <span>Daily</span></strong>
                    <span><?php echo intval($travpart_vo_option['tp_pts_lg_dly']); ?> Points</span>
                </li>
                <li>
                    <i class="fas fa-user-clock  fa-fw"></i>
                    <strong>Login <span>Weekly</span></strong>
                    <span><?php echo intval($travpart_vo_option['tp_pts_lg_wkly']); ?> Points</span>
                </li>

            </ul>
			<ul class="summary-star-list">
				<li class="sep">
                    <i class="fas fa-plane-departure"></i>
                    <strong>Request a tour from agent</strong>
                    <span><?php echo intval($travpart_vo_option['tp_pts_tor_pkg']); ?> Points</span>
                </li>
                <li>
                    <i class="fas fa-user-plus"></i>
                    <strong>Invite a friend to become a buyer</strong>
                    <span><?php echo intval($travpart_vo_option['tp_pts_refr']); ?> Points</span>
                </li>
			</ul>
        </div>
    </div>
	<?php
	foreach($voucher_scheme as $row) {
		$vsicon_args=get_post_meta($row->ID, '_tp_vsicon_args', true);
		if($vsicon_args['status']==1 && strtotime($vsicon_args['start_date'])<=time()
			&& strtotime($vsicon_args['end_date'])>=time() ) {
	?>
    <div class="game-scheme">
        <div class="scheme-title"><?php echo $row->post_title; ?></div>
        <ul class="scheme-header">
            <li><a href="<?php echo home_url().'/game/?type='.$row->ID; ?>"><?php echo $vsicon_args['pts_to_play']; ?> Points needed to play</a> </li>
            <li>Discount</li>
			<li>No. of Vouchers available </li>
            <li>Probability</li>
        </ul>
        <ul class="scheme-list game-gold">
            <li>
                <div><i class="circle"><i class="fa fa-plane fa-fw"></i> </i> Flight</div>
				<div><?php echo $vsicon_args['icon'][0]['discount']=='$'?$vsicon_args['icon'][0]['discount'].$vsicon_args['icon'][0]['figure']:$vsicon_args['icon'][0]['figure'].$vsicon_args['icon'][0]['discount']; ?></div>
                <div><?php echo $vsicon_args['icon'][0]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][0]['probability']; ?>%</div>
            </li>
            <li>
                <div><i class="circle"><i class="fas fa-utensils fa-fw"></i> </i> Meals</div>
				<div><?php echo $vsicon_args['icon'][2]['discount']=='$'?$vsicon_args['icon'][2]['discount'].$vsicon_args['icon'][2]['figure']:$vsicon_args['icon'][2]['figure'].$vsicon_args['icon'][2]['discount']; ?></div>
                <div><?php echo $vsicon_args['icon'][2]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][2]['probability']; ?>%</div>
            </li>
			<li>
                <div><i class="circle"><i class="fas fa-car fa-fw"></i> </i> Transportation</div>
				<div><?php echo $vsicon_args['icon'][4]['discount']=='$'?$vsicon_args['icon'][4]['discount'].$vsicon_args['icon'][4]['figure']:$vsicon_args['icon'][4]['figure'].$vsicon_args['icon'][4]['discount']; ?></div>
                <div><?php echo $vsicon_args['icon'][4]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][4]['probability']; ?>%</div>
            </li>
            <li>
                <div><i class="circle"><i class="fa fa-spa fa-fw"></i> </i> Massage</div>
				<div><?php echo $vsicon_args['icon'][6]['discount']=='$'?$vsicon_args['icon'][6]['discount'].$vsicon_args['icon'][6]['figure']:$vsicon_args['icon'][6]['figure'].$vsicon_args['icon'][6]['discount']; ?></div>
                <div><?php echo $vsicon_args['icon'][6]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][6]['probability']; ?>%</div>
            </li>
            <li>
                <div><i class="circle"><i class="fas fa-helicopter fa-fw"></i> </i> Helicopters</div>
                <div><?php echo $vsicon_args['icon'][8]['discount']=='$'?$vsicon_args['icon'][8]['discount'].$vsicon_args['icon'][8]['figure']:$vsicon_args['icon'][8]['figure'].$vsicon_args['icon'][8]['discount']; ?></div>
                <div><?php echo $vsicon_args['icon'][8]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][8]['probability']; ?>%</div>
            </li>
            <li>
                <div><i class="circle"><i class="fa fa-hotel fa-fw"></i> </i> Hotel</div>
                <div><?php echo $vsicon_args['icon'][10]['discount']=='$'?$vsicon_args['icon'][10]['discount'].$vsicon_args['icon'][10]['figure']:$vsicon_args['icon'][10]['figure'].$vsicon_args['icon'][10]['discount']; ?></div>
                <div><?php echo $vsicon_args['icon'][10]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][10]['probability']; ?>%</div>
            </li>
        </ul>
		<ul class="scheme-list game-sliver">
            <li>
                <div><i class="circle"><i class="fa fa-plane fa-fw"></i> </i> Flight</div>
                <div><?php echo $vsicon_args['icon'][1]['discount']=='$'?$vsicon_args['icon'][1]['discount'].$vsicon_args['icon'][1]['figure']:$vsicon_args['icon'][1]['figure'].$vsicon_args['icon'][1]['discount']; ?></div>
                <div><?php echo $vsicon_args['icon'][1]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][1]['probability']; ?>%</div>
            </li>
            <li>
                <div><i class="circle"><i class="fas fa-utensils fa-fw"></i> </i> Meals</div>
                <div><?php echo $vsicon_args['icon'][3]['discount']=='$'?$vsicon_args['icon'][3]['discount'].$vsicon_args['icon'][3]['figure']:$vsicon_args['icon'][3]['figure'].$vsicon_args['icon'][3]['discount']; ?></div>
                <div><?php echo $vsicon_args['icon'][3]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][3]['probability']; ?>%</div>
            </li>
			<li>
                <div><i class="circle"><i class="fas fa-car fa-fw"></i> </i> Transportation</div>
                <div><?php echo $vsicon_args['icon'][5]['discount']=='$'?$vsicon_args['icon'][5]['discount'].$vsicon_args['icon'][5]['figure']:$vsicon_args['icon'][5]['figure'].$vsicon_args['icon'][5]['discount']; ?></div>
				<div><?php echo $vsicon_args['icon'][5]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][5]['probability']; ?>%</div>
            </li>
            <li>
                <div><i class="circle"><i class="fa fa-spa fa-fw"></i> </i> Massage</div>
                <div><?php echo $vsicon_args['icon'][7]['discount']=='$'?$vsicon_args['icon'][7]['discount'].$vsicon_args['icon'][7]['figure']:$vsicon_args['icon'][7]['figure'].$vsicon_args['icon'][7]['discount']; ?></div>
                <div><?php echo $vsicon_args['icon'][7]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][7]['probability']; ?>%</div>
            </li>
            <li>
                <div><i class="circle"><i class="fas fa-helicopter fa-fw"></i> </i> Helicopters</div>
                <div><?php echo $vsicon_args['icon'][9]['discount']=='$'?$vsicon_args['icon'][9]['discount'].$vsicon_args['icon'][9]['figure']:$vsicon_args['icon'][9]['figure'].$vsicon_args['icon'][9]['discount']; ?></div>
                <div><?php echo $vsicon_args['icon'][9]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][9]['probability']; ?>%</div>
            </li>
			<li>
                <div><i class="circle"><i class="fa fa-hotel fa-fw"></i> </i> Hotel</div>
                <div><?php echo $vsicon_args['icon'][11]['discount']=='$'?$vsicon_args['icon'][11]['discount'].$vsicon_args['icon'][11]['figure']:$vsicon_args['icon'][11]['figure'].$vsicon_args['icon'][11]['discount']; ?></div>
                <div><?php echo $vsicon_args['icon'][11]['availability']; ?></div>
                <div><?php echo $vsicon_args['icon'][11]['probability']; ?>%</div>
            </li>
        </ul>
    </div>
	<?php
		}
	}
	?>
    <!--game scheme end-->
</div>

<?php
get_footer();
?>