<?php
/*
Template Name: People
*/

get_header();
$i=0;
// if search  variabel is set
if(isset($_GET['friend'])){

	$search = filter_input(INPUT_GET, 'friend', FILTER_SANITIZE_STRING);
	$search = trim($search);
}

?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/friends_connect.js?1157"></script>
<div class="ppl_area">
  <div class="row">
	<div class="col-md-2">
</div>
	  <div class="col-md-8 custom_bg_people">
		 <div class="col-md-12">

<?php

	// Perfrom suggestion operation here
	if ( is_user_logged_in() ) {
	global $wpdb;
	$realted_users_id = array();
	$current_user = wp_get_current_user();
	$user_id = $current_user->ID;
	$edu_level = get_user_meta( $user_id, 'edu_level' , true );
	$religion = get_user_meta( $user_id, 'religion' , true );
	$hometown = get_user_meta( $user_id, 'hometown' , true );
	$current_city = get_user_meta( $user_id, 'current_city' , true );
	$seeking = get_user_meta( $user_id, 'seeking' , true );
	$gender = get_user_meta( $user_id, 'gender' , true );
    $marital_status = get_user_meta( $user_id, 'marital_status' , true );
    if($marital_status=='single')
     $suggestions_gender = ( $gender=='Male') ? "Female": "Male"; 

	$user_education = $wpdb->get_results("SELECT school,degree FROM wp_education 
	WHERE  userid  ='$user_id' ");


	// IF its being searched

	  if(!empty($search)){

		$suggestions=get_users(
		array('search' =>'*'.$search.'*',
		'search_columns'=>array('ID','user_login','user_nicename','display_name'),
		'exclude'=>array($user_id)));

			//record user search

			$wpdb->insert(
			'user_friends_search_keywords',
			array(
			'user_id' => $user_id,
			'kw' => $search,
			),
			array(
			'%d',
			'%s',
			)
			);
	}
	 else {

		$query="SELECT kw FROM user_friends_search_keywords WHERE user_id = {$user_id}";
		$searchs = $wpdb->get_results($query);
		$search_ids = array();

				foreach($searchs as $row){

				$user = get_user_by('login',$row->kw);

				if( $user) {
				if($user->ID!=$user_id){
				$search_ids[] = $user->ID;
				}
			}
		}

		//education based suggestions
		foreach ($user_education as $edu) {
		$same_school = $wpdb->get_row("SELECT userid FROM wp_education 
				WHERE  school Like '%{$edu->school}%' OR  degree Like '%{$edu->degree}%' AND userid!=$user_id");
		if($same_school->userid!=$user_id){
				$realted_users_id[] = $same_school->userid;
			}
		}

		$phone_contact_users = $wpdb->get_col("SELECT wp_users.ID FROM `user`,`wp_users` WHERE wp_users.user_login=user.username
			AND phone IN ( SELECT contact FROM `user_contacts` WHERE wp_user_id = {$user_id} )");

		$search_ids = array_merge ( $search_ids,$realted_users_id, $phone_contact_users);


/// here will perform the wp_query
if(empty ($search_ids) OR count($search_ids)<10){

	$args = 
	
	array(
	'meta_query' => array(
	'relation' => 'OR',
	array(
	'key'     => 'edu_level',
	'value'   => $edu_level,
	'compare' => '='
	),
	array(
	'key'     => 'hometown',
	'value'   => $hometown,
	'compare' => '='
	),
	array(
	'key'     => 'current_city',
	'value'   => $current_city,
	'compare' => 'LIKE'
	),array(
	'key'     => 'seeking',
	'value'   => $seeking,
	'compare' => 'LIKE'
	),array(
    'key'     => 'gender',
    'value'   => $suggestions_gender,
    'compare' => 'LIKE'
    )
	),
	
	'exclude' => array($user_id),
);

}
	elseif(!empty($search_ids) AND count($search_ids)>10) {
	$args = array(
	'exclude' => array($user_id),
	'include'=>$search_ids
	);

}

     // Main Query to fetch results

	$wp_user_query = new WP_User_Query( $args );

	if(function_exists('front_end_suggestions')){
		$suggestions = front_end_suggestions ($current_user->ID);

	}

	if(empty($suggestions) || count ($suggestions) <5 ){

		$suggestions = $wp_user_query->get_results();
	}
	

	

}
	}
	else {

	
	wp_redirect('/');
}
	?>
			<?php if($suggestions): ?>
			<div class="col-md-6 ppl_text">
				<p>People You might be Interested</p>
			</div>

			</div>
			<?php foreach ($suggestions as $value) { 
				//profile pic
				if( get_avatar_url( $value->ID) =="https://www.travpart.com/English/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg" )
					continue;
				?>
			<?php

			// few checks for data
			$data_check_profile_home_town	= get_user_meta( $value->ID, 'hometown' , true );
			$data_check_profile_edu_lvl	= get_user_meta( $value->ID, 'edu_level' , true );
			$i++;



		// calculations for status,either they have sent friend request before/blocked
			$Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user1 = '$user_id' AND user2 = '{$value->ID}'");
			if($Data==NULL){

			$Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '$user_id' AND user1 = '{$value->ID}'");

			}
			if ($Data==NULL){
				$btn_1 =0;
				$button_text ="Connect";
				$button_class ="connect_button";
			}
			else if($Data->status==0 AND $Data->user1==$user_id){
				$btn_1 =1;
				$button_text ="Cancel <br /> Request";
				$button_class ="connect_button_sent";
			}
			else if($Data->status==1) {
				$btn_1 = 1;
				$button_text = "Connected";
				if($Data->user1==$user_id) {
					$button_class = "connect_button_sent";
				}
				else {
					$button_class = 'connect_button_connected';
				}
			}
			elseif($Data->status==0 AND $Data->user1==$value->ID){
				
				continue;
			}
			else{
					continue;
			}
			
			?>

			
				
		
			<div class="col-md-12 remove_pad profile_sec"> 
				<div class="col-md-2 remove_pad profile_img text-center">
				<a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $value->user_login ?>">	 
					<?php echo get_avatar( $value->ID,60 );  ?>
				</a>
				</div>
				<div class="col-md-5 remove_pad profile_info ">
					<h3>
						<a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $value->user_login ?>">
							<?php echo um_get_display_name ($value->ID)  ?>
						</a>
					</h3>
				<p><?php 
	

				
			if(!empty( $same_hometown))
			echo "Hometown: ". $same_hometown."<br>";

			//$same_edu_level	= get_user_meta( $value->ID, 'edu_level' , true );
			$user_education = $wpdb->get_row("SELECT school FROM wp_education 
			WHERE  userid  ='$value->ID' ");
			if(!empty( $user_education->school))
			echo "Education: ". $user_education->school."<br>";
			?></p>
			<?php

			?>

				</div>
				<div class="col-md-5 remove_pad connect_ppls text-right">
					<!-- Split button -->
					<!--<button class="<?php //echo $button_class;  ?>" id="<?php //echo $value->ID; ?>">
						<?php //echo $button_text; ?>
					</button>-->
					<?php //if($_GET['debug']==1){ ?>
					<button class="btn_profile_con btn_connect <?php echo $button_class;  ?>" id="<?php echo $value->ID; ?>"> 
						<div class="col-md-33 rm_d hide-m"> 
							<i class="icon_btn fas  <?php echo(($btn_1==1)?"fa-user-minus":'fa-user-plus'); ?>" id="plus"></i> 
						</div> 
						<div class="col-md-99 rm_d"> 
							<a href="#" class="p-connect-area-text" class="p-connect-area-text"> 
								<?php echo $button_text; ?> 
							</a>
						</div> 
					</button>
					<?php //} ?>
					<!--<div class="btn-group">
					  <button type="button" class="btn <?php echo $button_class; ?>  btn_req btn-default"><i class="fas fa-user-friends"></i> <?php echo $button_text ?></button>
					  <button type="button" class="btn btn_req btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    <span class="fas fa-ellipsis-h"></span>
					    <span class="sr-only">Toggle Dropdown</span>
					  </button>
					  <ul class="dropdown-menu">
					    <li><a href="#">Send message</a></li>
					  </ul>
					</div>-->
					<!--<div class="col-md-8">
						<div class="connect-area">
							<i class="fas fa-check"></i>

					      	<?php echo $button_text ?>
					    </div>
					</div>
					<div class="col-md-4">
					    <div class="dots-area">
				          <a href="#"><p>...</p></a>
				        </div>
				    </div>-->
				</div>
			</div>
			<?php  }
			else: echo "<h2 class='no_us_fn'>No User found</h2>";
			endif; ?>
			<?php
			if($i==0){
				//echo "<h2>No User found</h2>";
			}
			?>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>


<style type="text/css">
.custom_bg_people h2.no_us_fn {
	height: 150px;
}
.custom_bg_people{
background: #fff;
    padding: 10px 0px;
    margin-top: 10px;
    padding-bottom: 0px;
}
	.ppl_text p{
		flex: 1 1 auto;
		font-size: 16px;
		font-weight: 600;
	}
	.remove_pad{
		padding: 0px;
	}
	.see_text{
		text-align: right;
	}
	.see_text a{
		font-size: 16px;		
	}
	.profile_sec{
		padding-bottom: 15px !important;
	    margin-top: 15px;
	    margin-bottom: 0px;
	    border-bottom: #ededed30 1px outset;
	}
	.col-md-3.profile_img picture img {
	    //width: 150px;
	    //height: 150px;
	    border-radius: 50%;
	    margin-bottom: 10px;
	}
	.profile_img img{
		border-radius:50%;
	}

	.profile_info h3 {
		font-size: 14px;
		color: #385898;
		font-weight: 600;
		text-transform: capitalize;
		font-family: 'Heebo', sans-serif!important;
	}
	.profile_info p {
	    font-size: 12px;
	    color: #606770;
	}
	.connect-area {
	    background: #cacaca;
	    padding: 5px 15px;
	    display: flex;
	    width: 132px;
	}
	.connect-area a{
      color: #000;
      font-size: 20px;
 	 }

    .connect-area:hover{
      background: #1abc9c;
      cursor: pointer;
  	}
  	i.fas.fa-check {
	    margin-top: 5px;
	    font-size: 18px;
	    margin-right: 5px;
	}
	.btn_profile_con div .col-md-99 i.fas.fa-user-plus
	{
		padding-right: 10px;
	}
  	.dots-area {
	    background: #cacaca;
	    padding: 3px 8px;
	    height: 40px;
	    margin-top: 0px;
	    border-left: solid 1px grey;
	}

  	.dots-area a{
      color: #000;
  	}

  	.dots-area:hover{
      background: #1abc9c;
      cursor: pointer;
  	}
  	.dots-area p{
	  margin-top: -4px;
	    font-size: 20px
	}
	.connect_ppls{
		padding-right: 15px !important;
	}
	.connect_ppl {
	    margin-top: 50px;
	}
	
	@media (max-width: 600px) {
		/*.ppl_area {
		    margin-top: 74px;
		}*/
		.custom_bg_people h2.no_us_fn {
		    text-align: center;
		}
		.connect_button ,.requestsent_button, .connected_button{
		    width: 85px !important;
		    min-height: 21px !important;
		    line-height: 15px !important;
		    font-size: 10px !important;
		}
		.connected_button a {
		    font-size: 15px !important;
		}
		.connect_button a{
			font-size: 15px;
		}
		.remove_pad.connect_ppls.text-right {
		    text-align: center !important;
		}
		.profile_sec {
		    display: flex;
		    margin-top: 15px !important;
		}
		.profile_info p {
		    font-size: 12px;
		}
		.profile_info h3 {
		    font-size: 13px;
		}
		.connect-area {
		    width: 95px;
		}
		.connect_ppl {
	    	margin-top: 0px;
	    	display: flex;
		}
		.dots-area {		    		    
		    height: 32px;
		    margin-left: -30px;
		}
		i.fas.fa-check {	    
		    font-size: 12px;		    
		}
		.connect-area a{	      
	      font-size: 15px;
	 	 }
	 	.profile_img a img {
		    width: 60px;
		    height: 60px;
		}
		.profile_info {
		    padding-right: 0px;
		}
		.profile_img {
		    padding-right: 0px;
		    padding-left: 0px;
		}
		body{
	      overflow-x: hidden;
	    }
}
.btn_req a{
	color: #4b4f56;
	text-decoration: none;
}
/*.btn_req{
	background-color: #f5f6f7;border-color: #ccd0d5;font-family: 'Heebo', sans-serif;font-size: 11px;
}
.dropdown-menu>li>a{
	padding: 2px 5px;
}*/
.icon_d{
	color: #666;
}
.requestsent_button a,.connected_button a{
	color: #666;
	font-size: 12px;
    font-weight: 600;
    text-decoration: none;
}
.connected_button a{font-size: 20px;}
.connect_button a{
	color:#fff;
    font-weight: 600;
    text-decoration: none;
}

.pending_button{
	width: 150px;
	min-height: 30px;
	background: #22b14c;
	box-shadow: 0px 1px 5px 0px #a29b9b78;
	line-height: 25px;
	font-family: 'Heebo', sans-serif;
	font-size: 15px;
	border: 0px solid #22b14c;
	border-radius: 0px;
}
.requestsent_button,.connected_button{
	width: 150px;
	min-height: 30px;
	background: #fff;
	line-height: 25px;
	font-family: 'Heebo', sans-serif;
	font-size: 15px;
	border: 0px solid #666;
	box-shadow: 0px 1px 5px 0px #a29b9b78;
	border-radius: 0px;
}
/*.icon_d,.text_d{
	display: table-cell;
	vertical-align: middle;
}*/

.requestsent_button{
	line-height: 17px;
}
</style>
<style>
	.connect_button_sent, .connect_button_connected{
	    color: #666;
	    font-weight: 600;
	    text-decoration: none;
	}
	
	.connect_button,.connect_button_sent,.connect_button_connected{
		width: 130px;
	    background: #008a66;
	    line-height: 15px;
	    font-family: 'Heebo', sans-serif;
	    font-size: 13px;
	    border: 0px solid #008a66;
	    border-radius: 5px;
	    padding: 12px 10px;
	}

@media(max-width:600px){
	.connect_button_sent, .connect_button_connected{
		width: 95%;
		padding: 0px;
	}
	.connect_ppls {
		padding: 20px 5px !important;
	}
	.profile_info {
		padding: 25px 0px !important;
	}
	.hide-m{
		display: none;
	}
	.btn_profile_con i{
		font-size:10px;	
	}
	.connect_button_sent a {
	    font-size: 10px;
	}
}
</style>
<?php if($_GET['device']=='app'){ ?>
<style>
	
	@media (max-width: 600px) {
		.ppl_area {
		    margin-top:0px;
		}
	}
</style>
<?php }else{ ?>
<style>
	
	@media (max-width: 600px) {
		.ppl_area {
		    margin-top: 74px;
		}
	}
</style>
<?php } ?>

<style type="text/css">
    #footer {
        display: none;
    }
</style>


<?php
get_footer();
?>