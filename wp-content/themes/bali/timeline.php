<?php
/*
Template Name: Time Line
*/

global $wpdb;
if (!is_user_logged_in()) {
    exit(wp_redirect(home_url('login')));
}

get_header();
//get  user fb sharing option
$fb_sharing_option = get_user_meta(wp_get_current_user()->ID, 'fb_sharing_option', true);
if (!empty($fb_sharing_option)) {
    $fb_sharing_option = $fb_sharing_option;
} else {
    $fb_sharing_option = 0;
}
$current_user = wp_get_current_user();
$avatar = $wpdb->get_var("SELECT `photo` FROM `user`,wp_users WHERE wp_users.user_login=user.username AND wp_users.ID='{$current_user->ID}'");
if (empty($avatar)) {
    $avatar = um_get_user_avatar_data($current_user->ID)['url'];
} else {
    $avatar = home_url() . '/travchat/' . $chatUser->photo;
}

$friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE ID IN(
    SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$current_user->ID}')
    OR ID IN(
        SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$current_user->ID}')");

$tourPackages = $wpdb->get_results("SELECT ID,post_title, meta_value as tour_id FROM `wp_posts`,`wp_postmeta`
        WHERE post_type='post' AND post_status='publish' AND `meta_key`='tour_id'
        AND wp_posts.ID=wp_postmeta.post_id AND post_author='{$current_user->ID}'");

$friends_ids_query = $wpdb->get_results("SELECT ID FROM `wp_users` WHERE ID IN(
    SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$current_user->ID}')
    OR ID IN(
        SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$current_user->ID}')");
foreach ($friends_ids_query as $id) {

    $friends_ids[] = $id->ID;
}

$List = trim(implode(',', $friends_ids) . ',' . $current_user->ID, ',');
$pageNum = filter_input(INPUT_GET, 'pageNum', FILTER_SANITIZE_NUMBER_INT);
$pageNum = ($pageNum > 0) ? ($pageNum - 1) : 0;
$post_per_page = 15;
$postOffset = $pageNum * $post_per_page;
$hide_sql = '';
if(is_user_logged_in()) {
    $hide_sql = " AND wp_posts.ID NOT IN(SELECT short_post_id FROM `short_posts_hide_from_timeline` WHERE user_id='{$current_user->ID}') ";
}
$timeLinePosts = $wpdb->get_results("SELECT wp_posts.ID,post_author,user_login,post_title,post_date,post_type,post_content
                                        FROM `wp_posts`,`wp_users` WHERE
                                        ((post_author IN ($List) AND (`post_type`='shortpost' OR `post_type`='eventpost')) OR `post_type`='blog')
                                        AND post_status='publish' AND post_author=wp_users.ID
                                        $hide_sql
                                        ORDER BY post_date DESC LIMIT {$postOffset},{$post_per_page}");
wp_enqueue_media();
?>

<link async="async" href="https://www.travpart.com/English/wp-content/themes/bali/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

<link async="async" href="https://www.travpart.com/English/wp-content/themes/bali/css/tokenize2.css" rel="stylesheet" />

<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" /> -->

<input id="website-url" type="hidden" value="<?php echo home_url(); ?>" />
<input id="admin-ajax-url" type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" />
<input id="has_profile" type="hidden" value="<?php echo !um_profile('profile_photo') ? '0' : '1'; ?>" />

<script type="text/javascript" src="https://www.travpart.com/English/wp-content/themes/bali/js/tokenize2.js"></script>


<!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script> -->
<script src="<?php echo get_template_directory_uri(); ?>/js/timeline.js?v=<?= time()?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/friends_connect.js?252"></script>
 
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.masonry.js"></script>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css" />
<script src="<?php bloginfo('template_url'); ?>/js/owl.carousel.js?v1"></script>
<script>
    jQuery(document).ready(function($){
      var owl =  $('.owl-carousel').owlCarousel({
            loop:true,
            nav: false,
            margin:10,
            navigation: true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:2,
                    margin:10,
                    nav:true,
                },
                600:{
                    items:3,
                    nav:true
                },
                1000:{
                    items:7,
                    nav:true,
                    loop:false
                }
            }
        });
        // Go to the next item
        $('.customNextBtn').click(function() {
            owl.trigger('next.owl.carousel');
        })
        // Go to the previous item
        $('.customPreBtn').click(function(){
            owl.trigger('prev.owl.carousel', [300]);
        })
        
    }); 
</script>

<?php if (isset($_GET['event-post'])) { ?>
    <style>
        .normal-tab {
            display: none;
        }

        .event-tab {
            display: block;
        }
    </style>
<?php } ?>

<div class="loaderMain">

    <div class="mobile-popup-test" style="z-index:100;">
        <div class="mobile-popup-m" style="z-index:100000;">
            <div class="row">
                <div class="col-md-12 for_bg_cr">
                    <div class="mobile-create-post">
                        <div class="back-post">
                            <i class="fas fa-arrow-left"></i>
                        </div>

                        <h5>Create Post </h5>
                        <div class="m-post1">
                            <span></span>
                            <a href="#">Post</a>
                        </div>
                    </div>

                    <div class="mobile-pc-scetion" style="position: relative;">
                        <div class="table_main">
                            <div class="row_table">
                                <div class="for_vector_div">
                                    <?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?>
                                </div>
                                <div class="for_name_div text-left">
                                    <a href="#">
                                        <?php echo um_get_display_name($current_user->ID); ?>
                                    </a>
                                    <span id="mobile-short-post-feeling-text"></span>
                                </div>
                                <!--<div class="for_createevent_div">
                                    <div class="create_event">
                                        <a href="#">Create an <br /> Event</a>
                                    </div>
                                </div>
                                <div class="for_looking_travel_div looking-travel-friend">
                                    <div class="looking_travil">
                                        <a href="#">Looking for a travel friend</a>
                                        <span>[notify your friend]</span>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>

                    <div class="m-post-area">
                        <div class="Timelinewrap">
                            <div>
                                <div class="search">
                                    <input type="text" class="searchTerm" placeholder="Add a location tag (required)" id="short-post-location-mobile">
                                </div>
                            </div>

                            <!-- Date Time Picker -->
                            <div class="Search-DatePicker">
                                <div class='date-area'>
                                    <div class='date-container'>
                                        <label class="start-date">Start Date: </label>
                                        <div class="short-post-datepicker">
                                            <input id="short-post-startdate-mobile" type="text" placeholder="required" />
                                            <span class="calendar-btn"><span class="fas fa-calendar-alt"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class='date-area'>
                                    <div class='date-container'>
                                        <label class="start-date">End Date: </label>
                                        <div class="short-post-datepicker">
                                            <input id="short-post-enddate-mobile" type="text" placeholder="required" />
                                            <span class="calendar-btn"><span class="fas fa-calendar-alt"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Date Time Picker -->

                            <div class="m-text-area">
                                <textarea id="short-post-content-mobile" placeholder="What's going on <?php echo um_get_display_name($current_user->ID); ?>?"></textarea>
                            </div>
                            <input class="post-featured-image-id" type="hidden" value="-1">
                        </div>
                    </div>
                </div>
            </div>

            <div class="post-mobile-area">

                <? //if($_GET['debug']==1){ 
                ?>
                <div class="custom_photos">
                    <div class="buttons_row_add">


                        <div class="add_img  image_d add-photo-video">
                            <div class="border_d">
                                <i class="fas fa-plus"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row_buttons">
                    <div class="row1">
                        <div class="row1_col_1 add-photo-video">
                            <div class="m-box-icon">
                                <span class="icon-box"></span>
                                <i class="fas fa-images"></i>
                            </div>

                            <div class="icon-box-content">
                                <h4>
                                    Photo/Video
                                </h4>
                            </div>
                        </div>
                        <div class="row1_col_2">
                            <div class="m-box-icon">
                                <span class="icon-box"></span>
                                <i aria-hidden="true" class="far fa-smile"></i>
                            </div>

                            <div class="icon-box-content">
                                <h4 class="feeling-t">
                                    Feeling/Activity
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row_buttons">
                    <div class="row1">
                        <div class="row1_col_1">
                            <div class="m-box-icon">
                                <span class="icon-box"></span>
                                <i aria-hidden="true" class="fas fa-user-tag"></i>
                            </div>

                            <div class="icon-box-content">
                                <h4 class="tags-t">
                                    Tag Peoples
                                </h4>
                            </div>
                        </div>
                        <div class="row1_col_2 ">
                            <div class="m-box-icon">
                                <span class="icon-box"></span>
                                <i aria-hidden="true" class="fas fa-calendar-alt"></i>
                            </div>

                            <div class="icon-box-content">
                                <h4>
                                    Create Event
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row_buttons">
                    <div class="row1">
                        <div class="row1_col_1" <?php echo !current_user_can('um_travel-advisor') && !current_user_can('administrator') ? 'style="display:none;"' : ''; ?>>

                            <div class="m-box-icon">
                                <span class="icon-box"></span>
                                <i aria-hidden="true" class="fas fa-plane"></i>
                            </div>

                            <div class="icon-box-content">
                                <h4 class="mytour-p">
                                    My Tour Packages
                                </h4>
                            </div>
                        </div>

                    </div>
                </div>

                <?php //} 
                ?>
                <div class="row">
                    <!--<div class="col-md-3 w-md-20">
                        <div class="add-photo-video widget_area_mobile">
                            <div class="m-box-icon">
                                <span calss="icon-box"></span>
                                <i class="fas fa-images"></i>
                            </div>

                            <div class="icon-box-content">
                                <h4>
                                    Photo/Video
                                </h4>
                            </div>

                        </div>
                    </div>-->


                    <!--<div class="col-md-3 w-md-20">
                        <div class="widget_area_mobile">
                            <div class="m-box-icon">
                                <span calss="icon-box"></span>
                                <i aria-hidden="true" class="far fa-smile"></i>
                            </div>

                            <div class="icon-box-content">
                                <h4 class="feeling-t">
                                    Feeling/Activity
                                </h4>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-3 w-md-20">
                        <div class="widget_area_mobile">
                            <div class="m-box-icon">
                                <span calss="icon-box"></span>
                                <i aria-hidden="true" class="fas fa-user-tag"></i>
                            </div>

                            <div class="icon-box-content">
                                <h4 class="tags-t">
                                    Tag Peoples
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 w-md-20" <?php echo !current_user_can('um_travel-advisor') && !current_user_can('administrator') ? 'style="display:none;"' : ''; ?>>
                        <div class="widget_area_mobile" style="border-bottom: 0px;">
                            <div class="m-box-icon">
                                <span calss="icon-box"></span>
                                <i aria-hidden="true" class="fas fa-plane"></i>
                            </div>

                            <div class="icon-box-content">
                                <h4 class="mytour-p">
                                    My Tour Packages
                                </h4>
                            </div>
                        </div>
                    </div>-->

                    <!--<div class="div_trave">
                        <div class="trav_checkbox">
                            <input type="checkbox" name="optiona" id="opta" />
                            <span class="checkboxtext">
                                <i class="fas fa-users"></i>
                            </span>
                        </div>
                        <div class="div_mobile_travel looking-travel-friend">
                            Looking for a travel friend
                            <br><span>(You will be listed in TravMatch)</span>
                        </div>
                    </div>-->
                    <?php //if($_GET['debug']==1){ 
                    ?>
                    <div class="travmatch_wrapper">
                        <div class="trvmatch_heading">
                            <div class="cold col_tr1">
                                <hr />
                            </div>
                            <div class="cold col_tr2">
                                <h3>TravMatch</h3>
                            </div>
                            <div class="cold col_tr3">
                                <hr />
                            </div>
                        </div>
                    </div>


                    <div class="looking-travel-friend">
                        <div class="iconsdd icon33">
                            <div class="cr_event">
                                <label>
                                    <div class="for_r event_check_icon cent_d">
                                        <input type="radio" name="event_n" atype="1" />
                                    </div>
                                    <div class="for_r event_image_icon icon_m">
                                        <i class="fas fa-users"></i>
                                    </div>
                                    <div class="for_r event_text_icon size_m">
                                        <p>
                                            Looking for a Travel Friend
                                        </p>
                                    </div>
                                </label>
                            </div>
                        </div>


                        <div class="iconsdd icon44">
                            <div class="cr_event">
                                <label>
                                    <div class="for_r event_check_icon cent_d">
                                        <input type="radio" name="event_n" atype="2" />
                                    </div>
                                    <div class="for_r event_image_icon icon_m">
                                        <i class="fas fa-coffee"></i>
                                    </div>
                                    <div class="for_r event_text_icon size_m">
                                        <p>
                                            Want to Hangout
                                        </p>
                                    </div>
                                </label>
                            </div>
                        </div>

                        <div class="iconsdd icon55">
                            <div class="cr_event">
                                <label>
                                    <div class="for_r event_check_icon cent_d">
                                        <input type="radio" name="event_n" atype="3" />
                                    </div>
                                    <div class="for_r event_image_icon icon_m">
                                        <i class="fas fa-wine-bottle"></i>
                                    </div>
                                    <div class="for_r event_text_icon size_m">
                                        <p>
                                            Going for Party
                                        </p>
                                    </div>
                                </label>
                            </div>
                        </div>


                        <div class="iconsdd icon66">
                            <div class="cr_event">
                                <label>
                                    <div class="for_r event_check_icon cent_d">
                                        <input type="radio" name="event_n" atype="4" />
                                    </div>
                                    <div class="for_r event_image_icon icon_m">
                                        <i class="fas fa-futbol"></i>
                                    </div>
                                    <div class="for_r event_text_icon size_m">
                                        <p>
                                            Going for Sport
                                        </p>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="iconsd_d">
                        <div class="">
                            <div class="dis_b w1">
                                <strong>
                                    Also Post to Facebook
                                </strong>
                            </div>
                            <div class="dis_b w2">
                               
                            </div>
                            <div class="dis_b w3">
                                <label class="switch">
                                    <input type="checkbox" <?php echo ($fb_sharing_option == 1) ? "checked" : ""  ?>  name="toggle" id="toggle">
                                    <span class="slider round"></span>
                                </label>
                            </div>

                        </div>
                    </div>
-->
                    <?php //} 
                    ?>
                    <div class="mobile-button-link submitShortPost">
                        <a href="#"><span class="button-text">Post</span></a>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Feeling popup for mobile -->
    <div class="feeling-activity-m">
        <div class="filing-section-main">
            <div class="Fill-header-top h-top-new">
                <a class="cancel-f" href="#">Cancel</a>
                <h4>What are you doing?</h4>
            </div>

            <form action="method" class="form-emoji">
                <?php include(get_template_directory() . '/inc/emoji.php'); ?>
            </form>
        </div>

    </div>
    <!--End Feeling popup for mobile -->

    <!-- Tags Friends popup for mobile -->
    <div class="tags-friend-m">
        <div class="tags-section-main">
            <div class="Fill-header-top h-top-new">
                <a class="cancel-tags" href="#">Cancel</a>
                <h4>Who are you with?</h4>
            </div>

            <div class="tagBodyM">
                <div class="typing_name">
                    <input type="text" placeholder="Start typing a name...">
                </div>

                <div class="suggestion_name">
                    <input type="text" placeholder="SUGGESTION">
                </div>

                <div class="userTagArea">
                    <?php foreach ($friends as $friend) { ?>
                        <div class="user-1 tag_friend" userid="<?php echo $friend->ID; ?>">
                            <img src="<?php echo um_get_user_avatar_data($friend->ID)['url']; ?>">
                            <p class="username"><?php echo um_get_display_name($friend->ID); ?></p>
                            <div class="border-b-u"></div>
                        </div>
                    <?php } ?>

                </div>
            </div>

        </div>

    </div>
    <!--End Tags Friends popup for mobile -->

    <!-- Tag People Popup HTML -->
    <div class="tag-people-Popup tags-p">
        <div class="tag-people-main">
            <div class="tags-header-top">
                <i class="fas fa-times-circle tag-close"></i>
            </div>

            <div class="tagBodyM">
                <div class="typing_name">
                    <input type="text" placeholder="Start typing a name...">
                </div>

                <div class="suggestion_name">
                    <input type="text" placeholder="SUGGESTION">
                </div>

                <div class="userTagArea">
                    <?php foreach ($friends as $friend) { ?>
                        <div class="user-1 tag_friend" userid="<?php echo $friend->ID; ?>">
                            <img src="<?php echo um_get_user_avatar_data($friend->ID)['url']; ?>">
                            <p class="username"><?php echo um_get_display_name($friend->ID); ?></p>
                            <div class="border-b-u"></div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
    <!--End Tag People Popup HTML -->


    <!-- Tours packages Popup mobile -->
    <div class="tour-packages-Popup tours-p">
        <div class="tag-people-main">
            <div class="tour-header-top">
                <div class="col-md-12">
                    <a class="cancel-tours" href="#">Cancel</a>
                    <h4>My Tour Packages</h4>
                </div>
            </div>

            <div class="tagBodyM tourBodyM">

                <div class="input-group md-form form-sm form-1 pl-0" style="left:0">

                    <div class="input-group-prepend">
                        <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="basic-text1">
                            <i class="fas fa-search text-white" aria-hidden="true"></i>
                        </button>
                    </div>

                    <form class="search_hea" action="" method="get">
                        <input class="form-control my-0 py-1" type="text" placeholder="Search for tour packages" aria-label="Search" name="friend">
                    </form>

                </div>

                <div class="tour-ps">
                    <p class="sugg">Tour Packages Name</p>
                </div>
                <?php foreach ($tourPackages as $tour) { ?>
                    <div class="col-md-12 feel_display tourpackage" tour_id="<?php echo $tour->tour_id; ?>">
                        <i class="fa fa-calendar feel_icon"></i>
                        <p class="feel_text"><?php echo $tour->post_title; ?></p>
                    </div>
                <?php } ?>
                <?php if (empty($tourPackages)) { ?>
                    <div class="col-md-12 feel_display">
                        <p class="feel_text" style="margin:0;">You have not created any tour package.</p>
                    </div>
                <?php } ?>

            </div>


        </div>
    </div>
</div>
<!--End Tag People Popup HTML -->


<!--My Tour Packages Popup HTML -->
<div class="hover_bkgr_fricc pop-timetourpackages">
    <span class="helper"></span>
    <div>
        <div class="popupClose-timetourpackages popupClose-close">X</div>

        <div class="input-group md-form form-sm form-1 pl-0" style="left:0">

            <div class="input-group-prepend">
                <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="basic-text1">
                    <i class="fas fa-search text-white" aria-hidden="true"></i>
                </button>
            </div>

            <form class="search_hea" action="" method="get">
                <input class="form-control my-0 py-1" type="text" placeholder="Search for tour packages" aria-label="Search" name="friend">
            </form>

        </div>

        <div class="col-md-12">
            <p class="sugg">Tour Packages Name</p>
        </div>
        <?php foreach ($tourPackages as $tour) { ?>
            <div class="col-md-12 feel_display tourpackage" tour_id="<?php echo $tour->tour_id; ?>">
                <i class="fa fa-calendar feel_icon"></i>
                <p class="feel_text"><?php echo $tour->post_title; ?></p>
            </div>
        <?php } ?>
        <?php if (empty($tourPackages)) { ?>
            <div class="col-md-12 feel_display">
                <p class="feel_text" style="margin:0;">You have not created any tour package.</p>
            </div>
        <?php } ?>

    </div>
</div>
<!--End My Tour Packages Popup HTML -->

<div class="normal-tab">
    <div class="main_area">
        <header>
            <div class="new-short-post">

                <div class="header_txt">
                    <h2>Timeline</h2>
                </div>

                <div class="create_post">
                    <!--<h4>Create a Post</h4>
                    <?php echo um_get_avatar('', $current_user->ID, 40); ?>

                    <div class="for_createevent_div_desktop">
                        <div class="create_event_desktop">
                            <a href="#">Create an <br /> Event</a>
                        </div>
                    </div>
                    
                    
                    
                    <div class="for_looking_travel_div_desktop looking-travel-friend_desktop" style="width: 178px;padding-left: 5px;padding-right: 5px;">
                        <div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
                            <div class="col-md-2" style="padding-left: 0px;padding-right: 0px;float: left;">
                                <i class="fas fa-circle"></i>
                                <i class="far fa-check-circle"></i>
                            </div>
                            <div class="looking_travil_desktop col-md-10" style="padding-left: 0px;padding-right: 0px; float: left;">
                                <a href="#">Looking for a travel friend</a>
                                <br><span>[notify your friend]</span>
                            </div>
                        </div>
                    </div>-->


                    <div class="row2">
                        <div class="iconsd icon1">
                            Create a Post - <?php echo um_get_avatar('', $current_user->ID, 40); ?>
                        </div>
                        <div class="iconsd icon2 create_event_desktop">
                            <div class="cr_event cr_vd">
                                <div class="for_r event_image_icon event_icon_desktop">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                <div class="for_r event_text_icon event_text_desktop">
                                    <p>
                                        Create an <br /> Event
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="looking-travel-friend_desktop">
                            <div class="iconsd icon3">
                                <div class="cr_event">
                                    <label>
                                        <div class="for_r event_check_icon">
                                            <input type="radio" name="event_n" atype="1" />
                                        </div>
                                        <div class="for_r event_image_icon">
                                            <i class="fas fa-user-friends"></i>
                                        </div>
                                        <div class="for_r event_text_icon">
                                            <p>
                                                Looking for <br /> a Travel Friend
                                            </p>
                                        </div>
                                    </label>
                                </div>
                            </div>


                            <div class="iconsd icon4">
                                <div class="cr_event">
                                    <label>
                                        <div class="for_r event_check_icon">
                                            <input type="radio" name="event_n" atype="2" />
                                        </div>
                                        <div class="for_r event_image_icon">
                                            <i class="fas fa-coffee"></i>
                                        </div>
                                        <div class="for_r event_text_icon">
                                            <p>
                                                Want to <br /> Hangout
                                            </p>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="iconsd icon5">
                                <div class="cr_event">
                                    <label>
                                        <div class="for_r event_check_icon">
                                            <input type="radio" name="event_n" atype="3" />
                                        </div>
                                        <div class="for_r event_image_icon">
                                            <i class="fas fa-wine-bottle"></i>
                                        </div>
                                        <div class="for_r event_text_icon">
                                            <p>
                                                Going for<br /> Party
                                            </p>
                                        </div>
                                    </label>
                                </div>
                            </div>


                            <div class="iconsd icon6">
                                <div class="cr_event">
                                    <label>
                                        <div class="for_r event_check_icon">
                                            <input type="radio" name="event_n" atype="4" />
                                        </div>
                                        <div class="for_r event_image_icon">
                                            <i class="fas fa-futbol"></i>
                                        </div>
                                        <div class="for_r event_text_icon">
                                            <p>
                                                Going for<br /> Sport
                                            </p>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="iconsd_d">
                            <div class="">
                                <div class="dis_b w1">
                                    <strong>
                                        Share to <br /> Facebook
                                    </strong>
                                </div>
                                <div class="dis_b w2">
                                    <!--<p>Ilham Kaka Bachkiar</p>-->
                                </div>
                                <div class="dis_b w3">
                                    <label class="switch">
                                        <input type="checkbox" <?php echo ($fb_sharing_option == 1) ? "checked" : ""  ?> name="toggle" id="toggle">
                                        <span class="slider round"></span>
                                    </label>
                                </div>

                            </div>
                        </div>


                    </div>

                </div>



                <div class="add_locations">
                    <div class="Timelinewrap">
                        <div class="search">
                            <input id="short-post-location" type="text" class="searchTerm searchW" placeholder="Add a location tag (optional)">
                            <input type="hidden" id="short-post-lat" />
                            <input type="hidden" id="short-post-lng" />
                            <button type="submit" class="searchButton">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>


                        <div class="Search-DatePicker">
                            <div class='date-area'>
                                <div class='date-container'>
                                    <label class="start-date">Start Date: </label>
                                    <div class="short-post-datepicker">
                                        <input id="short-post-startdate" type="text" placeholder="required" />
                                        <span class="calendar-btn"><span class="fas fa-calendar-alt"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class='date-area'>
                                <div class='date-container'>
                                    <label class="start-date">End Date: </label>
                                    <div class="short-post-datepicker">
                                        <input id="short-post-enddate" type="text" placeholder="required" />
                                        <span class="calendar-btn"><span class="fas fa-calendar-alt"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="text_and_image_div">
                            <div class="text-area add_c">
                                <textarea id="short-post-content" placeholder="What's going on <?php echo um_get_display_name($current_user->ID); ?>?"></textarea>
                            </div>
                            <?php //if($_GET['debug']==1){ 
                            ?>
                            <div class="custom_photos">
                                <div class="buttons_row_add">


                                    <div class="add_img  image_d add-photo-video">
                                        <div class="border_d">
                                            <i class="fas fa-plus"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php //} 
                            ?>

                        </div>
                        <input id="short-post-feeling" type="hidden" value="" />
                        <input id="short-post-tagfriend" type="hidden" value="" />
                        <input id="short-post-tourpackage" type="hidden" value="" />
                        <input id="looking-for-travel-friend" type="hidden" value="0" />
                        <input class="post-featured-image-id" type="hidden" value="-1" />
                        <?php wp_nonce_field('short_post_nonce_action', 'shortpostnonce'); ?>
                    </div>
                </div>

                <div class="post_area">
                    <div class="row">
                        <div class="col-md-3 w-md-20">
                            <div class="add-photo-video widget_area">
                                <div class="icon-box-icon">
                                    <span calss="icon-box"></span>
                                    <i class="fas fa-images"></i>
                                </div>

                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">
                                        <span>Photo/Video</span>
                                    </h4>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-3 w-md-20">
                            <div class="widget_area">
                                <div class="icon-box-icon icon-box-i">
                                    <span calss="icon-box"></span>
                                    <i aria-hidden="true" class="far fa-smile"></i>
                                </div>

                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">
                                        <span class="timefeeling-popup">Feeling/Activity</span>
                                    </h4>
                                </div>
                            </div>

                        </div>

                        <!-- Tags Friends  -->
                        <div class="col-md-3 w-md-20">
                            <div class="widget_area">
                                <div class="icon-box-icon">
                                    <span calss="icon-box"></span>
                                    <i aria-hidden="true" class="fas fa-user-tag"></i>
                                </div>

                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">
                                        <span class="tagPeople-popup">Tag Peoples</span>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <!-- Tags Friends  -->

                        <div class="col-md-3 w-md-20">
                            <div class="widget_area widget-extra-a" <?php echo !current_user_can('um_travel-advisor') && !current_user_can('administrator') ? 'style="display:none;"' : ''; ?>>
                                <div class="icon-box-icon">
                                    <span calss="icon-box"></span>
                                    <i aria-hidden="true" class="fas fa-plane"></i>
                                </div>

                                <div class="icon-box-content">
                                    <h4 class="icon-box-title">
                                        <span class="timetourpackages-popup">My Tour Packages</span>
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="button-link submitShortPost">
                            <a href="#"><span class="button-text">Post</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>

    <div class="mobile-version-post" <?php echo $_GET['device'] == app ? 'style="display:none;"' : ''; ?>>
        <div class="m-user-profile">
            <!--<img src="<?php echo $avatar; ?>" width="35" height="35">-->
            <?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?>
        </div>

        <div class="m-user-input">
            <input class="user-input" type="text" placeholder="What's going on <?php echo um_get_display_name($current_user->ID); ?>?">
        </div>

        <div class="m-add-photo">
            <i class="fas fa-images"></i>
            <span>Photo</span>
        </div>
    </div>
    
    <?php //if($_GET['debug']==1){ ?>
        <div class="bg_suggestion">
            <h3>Suggested People</h3>
            <hr />
            <?php

    if(function_exists('front_end_suggestions')){
        $suggestions = front_end_suggestions ($current_user->ID);

    }else{
        $suggestions =   friends_suggestions_for_timeline();

    }

    if(empty($suggestions) || count ($suggestions) <5 ){

        $suggestions = friends_suggestions_for_timeline();
    }
             
            ?>
            <div class="owl_items">
                
                <!-- Set up your HTML -->
                <div class="owl-carousel">
                  
           
         <?php   foreach ($suggestions as $value) {  ?>
            <!-- insideloop-->
         <?php

         $avatar_url = get_avatar_url( $value->ID );
         if($avatar_url=='https://www.travpart.com/English/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg')
            continue;

            //check user role

        $user_meta = get_userdata($value->ID);
        $user_roles = $user_meta->roles;

        if (in_array('um_travel-advisor', $user_roles, true)) {

            $role = "Seller";
        } 
        elseif (in_array('um_clients', $user_roles, true)) {

            $role = "Buyer";
        }
         else
        {
            $role = "";
        }
        
         if($role=="Buyer"){


            // calculations for status,either they have sent friend request before/blocked
            $Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user1 = '$current_user->ID' AND user2 = '{$value->ID}'");
            if($Data==NULL){

            $Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '$current_user->ID' AND user1 = '{$value->ID}'");

            }
            if ($Data==NULL){
                $connIcon = ' <i class="fas fa-user-plus" id="plus"></i>';
                $button_text =" Connect";
                $button_class ="connect_button";
                $following=0;
            }
            else if($Data->status==0){
                $connIcon = ' <i class="fas fa-user-minus" id="plus"></i>';
                $following=1;
                $button_text =" Cancel Request";
                $button_class ="connect_button_sent";
            }
            else{
                  continue;
            }
        }
        else{

         $followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$current_user->ID' AND sf_agent_id = '$value->ID'", ARRAY_A);

          if (!empty($followData)) {
 
            $button_text ="Following";
            $following=1;
            $cl = 'following_class';
            $button_class ="follow_button";
            $connIcon = ' <i class="fas fa-wifi" id="plus"></i>';
          //  continue;
        }
        else{
            $cl = '';
            $following=0;
            $connIcon = ' <i class="fas fa-wifi" id="plus"></i>';
            $button_text ="Follow";
            $button_class ="follow_button";

             }
            }


            ?>

            <div> 
                    <div class="people_div  remove_element<?php  echo $value->ID; ?>">
                        <div class="people_img">
                            <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $value->user_login ?>">  
                                <?php echo get_avatar( $value->ID,140);  ?>
                            </a>
                            <button class="btn custom_btn_times" id="<?php  echo $value->ID; ?>">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                        <div class="people_nme">
                           <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $value->user_login ?>">
                            <?php echo um_get_display_name ($value->ID);  ?>
                        </a>
                        </div>
                        <div class="people_btn">
                            <button class="btn_profile_con <?php echo $cl; ?> btn_connect <?php echo $button_class;  ?>" id="<?php  echo $value->ID; ?>"  <?php echo(($following==1)?'style=""':''); ?> > 
                                <div class="col-md-33 rm_d"> 
                                    <?php echo $connIcon;?>
                                </div> 
                                <div class="col-md-99 rm_d"> 
                                    <a href="#" class="p-connect-area-text"><?php  echo $button_text; ?></a> 
                                </div> 
                            </button>
                        </div>
                        </div>
                    </div>

                    <!-- loop -->
                <?php } ?>
                  

                  
                </div>
                <button class="customPreBtn btn btn-defaul">
                    <i class="fas fa-arrow-left"></i>
                </button>
                <button class="customNextBtn btn btn-defaul">
                    <i class="fas fa-arrow-right"></i>
                </button>
            </div>
            

        </div>
    <?php //} ?>
    
    <div class="post-area-main">
        
        <section>
            <div class="container_2" id="container connect-friend">

                <!-- time line row -->
                <div id="loading_next_page" class="row">
                    <?php if (count($timeLinePosts) == 0) { ?>
                        <div class="ifnotpost">
                            <h4><strong>Welcome to Travpart</strong></h4>
                            <p>Get Started by connecting your friends to your most desired destination. You'll see their location, travel plans, videos, photos, post in this platform.</p>
                            <a href="https://www.travpart.com/English/people/" class="btn change_url btn-default">Connect Friends</a>
                        </div>

                    <?php } else { ?>
                        <?php include_once('timeline-post-content.php'); ?>
                    <?php } ?>
                </div>


            </div>
        </section>

    </div>
</div>

<div class="event-tab">
    <div class="shedule_event_main_wrapper">
        <div class="container_shedule">
            <div class="shedule_pageheading">
                <h1>Shedule an Event</h1>
                <span><i class="fa fa-close"></i></span>
            </div>

            <?php wp_nonce_field('event_post_nonce_action', 'eventpostnonce'); ?>
            <div class="content_row">
                <div class="name_text_row">
                    <div class="event_name_shedule">
                        <p>Event Name</p>
                    </div>
                    <div class="event_text_shedule">
                        <input class="form-control" name="event_name" placeholder="Add a short, clear name" />
                    </div>
                </div>

                <div class="invite_connection_div">
                    <div class="invite_connection_title">
                        <p>Invite your connection</p>
                    </div>
                    <select name="event_invited_people" class="event-invited-people" multiple>
                        <?php foreach ($friends as $f) { ?>
                            <option value="<?php echo $f->ID; ?>"><?php echo um_get_display_name($f->ID); ?></option>
                        <?php } ?>
                    </select>

                    <!--p class="for_mututal"><span class="for_mut_color">Mutual friends</span> <a href="#">Balouch</a>,<a href="#">Lix</a></p-->
                </div>

                <div class="event_type">
                    <p>Event Type</p>
                </div>
                <div class="cusomt_private_dropdown">
                    <div class="dropdown">
                        <div class="dropdown-toggle" id="event-type-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <div class="inline_t">
                                <i class="fas fa-lock"></i>
                            </div>
                            <div class="inline_t event-type-chosen">
                                <p><strong>Private</strong></p>
                                <p>The event's members are the only people whom can see the post and other event's members</p>
                            </div>
                            <input name="event_type" type="hidden" />

                        </div>
                        <ul class="dropdown-menu" aria-labelledby="event-type-dropdown">
                            <li>
                                <div class="inline_t">
                                    <i class="fas fa-lock"></i>
                                </div>
                                <div class="inline_t event-type-option" type="private">
                                    <p><strong>Private</strong></p>
                                    <p>The event's members are the only people whom can see the post and other event's members</p>
                                </div>
                            </li>
                            <li>
                                <div class="inline_t">
                                    <i class="fas fa-globe-americas"></i>
                                </div>
                                <div class="inline_t event-type-option" type="public">
                                    <p><strong>Public</strong></p>
                                    <p>Everyone can see the event's member and post</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>


                <div class="locaion_shedule_div">
                    <div class="title_location">
                        <p>Location</p>
                    </div>
                    <div class="width_25 location_profile">
                        <div class="custom_round">
                            <p>location <br /> picture<br /> or video</p>
                            <button class="btn btn-primary event-location-image">Upload</button>
                            <input name="event_location_image_id" type="hidden" />
                        </div>
                        <div class="text-normal text-center">
                            <p>Upload a picture of the <br />location's unique <br />landmark</p>
                        </div>
                    </div>
                    <div class="width_69 location_profile">
                        <div class="input_record">
                            <input id="event-location" name="event_location" type="text" class="cus form-control" placeholder="(Required)" style="width: 60%" />
                            <div class="inputs_data">
                                <div class="text_dates width15">
                                    Starts
                                </div>
                                <div class="text_dates width40">
                                    <div id="datetimepicker1" class="input-append date">
                                        <div class="input-wrapper">
                                            <label for="stuff" class="far fa-calendar-alt add-on input-icon"></label>
                                            <input name="start_date" class="form-control" data-format="MM/dd/yyyy" type="text" readonly></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="text_dates width40">
                                    <div id="datetimepicker3" class="input-append date">
                                        <div class="input-wrapper">
                                            <label for="stuff" class="fas fa-stopwatch add-on input-icon"></label>
                                            <input name="start_time" class="form-control" data-format="hh:mm:ss" type="text" readonly></input>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="inputs_data">
                                <div class="text_dates width15">
                                    Ends
                                </div>
                                <div class="text_dates width40">
                                    <div id="datetimepicker2" class="input-append date">
                                        <div class="input-wrapper">
                                            <label for="stuff" class="far fa-calendar-alt add-on input-icon"></label>
                                            <input name="end_date" class="form-control" data-format="MM/dd/yyyy" type="text" readonly></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="text_dates width40">
                                    <div id="datetimepicker4" class="input-append date">
                                        <div class="input-wrapper">
                                            <label for="stuff" class="fas fa-stopwatch add-on input-icon"></label>
                                            <input name="end_time" class="form-control" data-format="hh:mm:ss" type="text" readonly></input>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="event-tour-package-row">
                    <div class="event_name_shedule">
                        <p>Tour package</p>
                    </div>
                    <div class="event_text_shedule">
                        <select name="event_tour_package" class="event-tour-package" style="width: 60%;">
                            <option></option>
                            <?php foreach ($tourPackages as $t) { ?>
                                <option value="<?php echo $t->tour_id; ?>">BALI<?php echo $t->tour_id; ?></option>
                            <?php } ?>
                            <?php if (empty($tourPackages)) { ?>
                                <option disabled="disabled">You have not created any tour package.</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="event-submit-row">
                    <button id="create-event-submit" class="btn btn-lg btn-success">Create</button>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="Filling-section feeling" style="top:14.1%;left: 24.5%;">
    <div class="filing-section-main">
        <div class="Fill-header-top">
            <i class="fas fa-times-circle"></i>
        </div>

        <form action="method" class="form-emoji">
            <?php include(get_template_directory() . '/inc/emoji.php'); ?>
        </form>
    </div>

</div>

<!-- End Filing-->

<!-- Show Edit Popup -->
<div class="editPost">
    <div class="EditConte">
        <div class="edit_post_header">
            <div class="Eheader-left">
                <h4>Edit Post</h4>
            </div>

            <div class="Epostright">
                <i class="fas fa-times"></i>
            </div>
        </div>

        <div class="Edit_content_body">
                <div class="profile_area_edit">
                    <textarea name="posttext"></textarea>
                    <input type="hidden" name="postid" />
                    <input type="hidden" name="feeling" />
                    <input type="hidden" name="tag_user_id" />
                    <input type="hidden" name="edit_tourpackage" />
                    <input type="hidden" name="edit_featured_image_id" />
                </div>

                <div class="editbodyexcerpt">

                    <p><span class="edit-feeling"></span> <span class="edit-tag-user"></span></p>

                    <div class="userPhThumbnail"></div>

                    <div class="userPhThumbnail2 edit-photo-video-button">
                        <i class="fas fa-plus"></i>
                    </div>
                </div>

                <div class="edit-all-btn">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="edit-photo-video edit-photo-video-button">
                                <div class="e-box-icon">
                                    <span calss="icon-box"></span>
                                    <i class="fas fa-images"></i>
                                </div>

                                <div class="e-box-content">
                                    <h4 class="icon-box-title">
                                        <span>Photo/Video</span>
                                    </h4>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="edit-photo-video edit-feeling-activity">
                                <div class="e-box-icon">
                                    <span calss="icon-box"></span>
                                    <i class="far fa-smile"></i>
                                </div>

                                <div class="e-box-content">
                                    <h4 class="icon-box-title">
                                        <span>Feeling/Activity</span>
                                    </h4>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="edit-photo-video edit-tag-people">
                                <div class="e-box-icon">
                                    <span calss="icon-box"></span>
                                    <i class="fas fa-user-tag"></i>
                                </div>

                                <div class="e-box-content">
                                    <h4 class="icon-box-title">
                                        <span>Tag Peoples</span>
                                    </h4>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="edit-photo-video edit-tour-package-button">
                                <div class="e-box-icon">
                                    <span calss="icon-box"></span>
                                    <i class="fas fa-plane"></i>
                                </div>

                                <div class="e-box-content">
                                    <h4 class="icon-box-title">
                                        <span>My Tour Packages</span>
                                    </h4>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="edit-delete-footer">
            <div class="btn-myTsave">
                <input id="update-post-submit" type="submit" value="Save">
            </div>
        </div>

        <div class="Filling-section edit-feeling-choose" style="top:35%; left:40%; position:fixed; display:none;">
            <div class="filing-section-main">
                <div class="Fill-header-top">
                    <i class="fas fa-times-circle"></i>
                </div>
                <form action="method" class="form-emoji">
                    <?php include(get_template_directory() . '/inc/emoji.php'); ?>
                </form>
            </div>
        </div>

        <div class="tag-people-Popup" style="top:35%; left:40%; position:fixed; display:none;">
            <div class="tag-people-main">
                <div class="tags-header-top">
                    <i class="fas fa-times-circle tag-close"></i>
                </div>

                <div class="tagBodyM">
                    <div class="userTagArea">
                        <?php foreach ($friends as $friend) { ?>
                        <div class="user-1 edit_tag_friend" userid="<?php echo $friend->ID; ?>">
                            <img src="<?php echo um_get_user_avatar_data($friend->ID)['url']; ?>">
                            <p class="username"><?php echo um_get_display_name($friend->ID); ?></p>
                            <div class="border-b-u"></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="hover_bkgr_fricc edit-tour-packages" style="left:5%;">
            <span class="helper"></span>
            <div>
                <div class="popupClose-close">X</div>
                <div class="input-group md-form form-sm form-1 pl-0" style="left:0">
                    <div class="input-group-prepend">
                        <button class="input-group-textpurple lighten-3 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="basic-text1">
                            <i class="fas fa-search text-white" aria-hidden="true"></i>
                        </button>
                    </div>

                    <form class="search_hea" action="" method="get">
                        <input class="form-control my-0 py-1" type="text" placeholder="Search for tour packages" aria-label="Search" name="friend">
                    </form>
                </div>
                
                <div class="col-md-12">
                    <p class="sugg">Tour Packages Name</p>
                </div>
                <?php foreach ($tourPackages as $tour) { ?>
                <div class="col-md-12 feel_display edit-tourpackage" tour_id="<?php echo $tour->tour_id; ?>">
                    <i class="fa fa-calendar feel_icon"></i>
                    <p class="feel_text"><?php echo $tour->post_title; ?></p>
                </div>
                <?php } ?>
                <?php if (empty($tourPackages)) { ?>
                <div class="col-md-12 feel_display">
                    <p class="feel_text" style="margin:0;">You have not created any tour package.</p>
                </div>
                <?php } ?>
            </div>
        </div>

    </div>
</div>
<!-- End Show Edit Popup -->

<!-- Show Delete Popup -->

<div class="DeletePost">
    <div class="DeleteConte">

        <div class="edit_post_header">
            <div class="Eheader-left">
                <h4>Delete Post</h4>
            </div>

            <div class="Dpostright">
                <i class="fas fa-times"></i>
            </div>
        </div>


        <div class="Edit_content_body">
            <p> You can Edit it if you just need to change something. if you didn't create this post, we can help you <a href="#">secure your account</a> </p>
        </div>

        <div class="edit-delete-footer">
            <div class="btn-myTsave">
                <input class="delete_post" type="submit" value="Delete" postid="" />
            </div>
        </div>
    </div>
</div> 
<!-- End Delete Popup -->

</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA069EQK3M96DRcza2xuQb0DZxmYYkVVw8&libraries=places"></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js' charset="UTF-8"></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js' charset="UTF-8"></script>

<!-- End Edit show tabs and delete post JS -->
<style type="text/css">
    .looking-travel-friend_desktop i.fas.fa-circle {
        margin-top: 13px;
        font-size: 20px;
        color: white;
    }
    
    .following_class{
        background: rgb(218, 216, 216);
        color: #000;
        font-weight: bold;
    }
    .following_class a{
        color: #000;
        font-weight: bold;
    }
    
    .looking-travel-friend_desktop i.far.fa-check-circle {
        display: none;
        margin-top: 7px;
        font-size: 20px;
        color: #3f48cc;
    }

    .looking_travil_desktop span {
        color: #f4f4f4;
        font-size: 12px;
        font-family: 'Roboto', sans-serif;
    }

    .active_desktop span {
        color: #333;
    }

    .another-user-section span {
        word-break: break-word;
    }

    .user-comment-area {
        margin-bottom: 10px;
        display: flex;
    }

    .create_post {
        background: #c3c3c3;
    }

    .add_locations {
        background-color: #c3c3c3;
    }

    .create_post h4 {
        color: black;
    }

    .widget_area {
        background-color: white;
    }

    .ifnotpost {
        margin-bottom: 20px;
    }
    .iconsd.icon2.create_event_desktop {
        background-color: lightblue;
    }
    .for_r.event_image_icon.event_icon_desktop {
        color: black;
    }
    .for_r.event_text_icon.event_text_desktop {
        color: black;
        font-weight: 600;
    }
    .button-link.submitShortPost a {
        box-shadow: 0 0 0 5px hsl(0, 0%, 87%);
        border-radius: 0px;
    }
   
    @media (max-width: 600px) {
        .container.for_remove_ad {
            margin-top: 75px;
        }
    }
</style>
<?php if ($_GET['device'] == 'app') { ?>

    <style>
        @media screen and (max-width: 600px) {
            .container.for_remove_ad {
                margin-top: 0px;
            }
        }
    </style>
<?php } ?>
<style type="text/css">
    #footer {
        display: none;
    }
</style>
<script>

function copy_link(id) {
  var copyText = document.getElementById("copytext"+id).href;
  document.addEventListener('copy', function(event) {
    event.clipboardData.setData('text/plain', copyText);
    event.preventDefault();
    document.removeEventListener('copy', handler, true);
  }, true);
  document.execCommand('copy');
  alert("Link Copied: ");
}
function changeDate()
{
    console.log("on Change");
}
</script>

<?php
get_footer();
?>;