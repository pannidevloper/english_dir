<?php
get_header();
if(have_posts()) {
	while(have_posts()) {
		the_post(); ?>
        <div class="left-sidebarforum">
            <?php if ( is_active_sidebar( 'forum-side-bar' ) ) : ?>
                <?php dynamic_sidebar( 'forum-side-bar' ); ?>
            <?php endif; ?>
        </div>
        </div>
        <div class="right-forum">
            <ul class="nav nav-tabs">
                <li role="presentation" class="active"><a href="#">Most Recent</a></li>
                <li role="presentation"><a href="/most-viewed">Most Viewed</a></li>
                <li role="presentation"><a href="/travel-hub">Travel Hub</a></li>
            </ul>
            <hr>
            <?php the_content();?>
        </div>

        <?php
	}
} else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>