<?php
/* Template Name: bidPage tpl */
get_header();

function trim_value(&$value)
{
    $value = trim($value, ":,");
}

function generate_revnum($tourid)
{
	$revnum='';	
	$tourid=strval($tourid);
	
	if(strlen($tourid)<4)
	{
		for($i=0;$i<4-strlen($tourid); $i++)
			$tmp.='0';
		$tourid=$tmp.$tourid;
	}

	$n1=decoct($tourid);
	if($tourid<8)
		$n1.='9';
	$revnum='R'.$n1[0].$tourid[2].$n1[1].$tourid[1].$tourid[0].$tourid[3];

	return $revnum;
}

global $wpdb;
$notecontents = array();
$nindex = 0;
$total_price = 0;

if (isset($_GET['vendor_id']) && isset($_GET['book_code']) && isset($_GET['agent_name'])) {
    $vendor_id = $_GET['vendor_id'];
    $bookingcode = $_GET['book_code'];
    $agent_name = $_GET['agent_name'];
} else {
    echo "Something is wrong in URl please click on link in email again";
}

if (!empty($bookingcode))
    $tour_id = intval(substr($bookingcode, 4));
else
    $tour_id = '';

if (!empty($tour_id) && $tour_id > 1) {
    if (!empty($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=" . $tour_id)->meta_value)) {
        extract(unserialize($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='bookingdata' and tour_id=" . $tour_id)->meta_value), EXTR_OVERWRITE);
    }


    if (!empty($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='notecontents' and tour_id=" . $tour_id)->meta_value))
        $notecontents = unserialize($wpdb->get_row("SELECT * FROM `wp_tourmeta` where meta_key='notecontents' and tour_id=" . $tour_id)->meta_value);

    $locationnames = explode(':,', $spotname);
    array_walk($locationnames, 'trim_value');

    $locationlats = explode(':,', $spotlat);
    array_walk($locationlats, 'trim_value');

    $locationlongs = explode(':,', $spotlong);
    array_walk($locationlongs, 'trim_value');

    $locationdates = explode(':,', $spotdate);
    array_walk($locationdates, 'trim_value');

    $locationtimes = explode(':,', $spottime);
    array_walk($locationtimes, 'trim_value');

}

if (isset($_POST["submit"])) {
    $bidprice = $_POST["booking_code_input"];
    if ($wpdb->update('finalitinerary', array('vendorid' => $vendor_id, 'vendorbid' => $bidprice), array('bookingcode' => $bookingcode))) {
        echo "Thanks for the Bidding !!!";
    } else {
        echo "Something went wrong !!!";
    }
}

if (have_posts()) {
    while (have_posts()) {
        the_post();
        the_content();
    }
    ?>
	
	<div class="v2-tour-page">
                <!--Place-->
				<?php
    for ($i = 0; !empty($locationnames[$i]) && !empty($locationdates[$i]) && !empty($locationtimes[$i]); $i++) {
        ?>
                <div class="v2-box mb-3">
                    <div class="d-flex flex-column flex-sm-row align-items-center">
                        <div class="mb-2 box-grid-50">
                            <img src="/English/wp-content/themes/bali/images/tour-details/ayana-resort-bali.jpg" width="538" alt="" class="img-responsive">
                        </div>
                        <div class="box-grid-50  pl-sm-3">
                            <h4 class="v2-title"><?php echo $locationnames[$i]; ?></h4>
                            <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i> <?php echo $locationnames[$i]; ?> – <a href="#"> Displayed on the map</a> (100 m away from central area)</p>
                            <p class="mb-3"><?php if (!empty($notecontents[$nindex])) echo 'Note:' . $notecontents[$nindex++]; ?></p>
                            <p class="mb-2"><i class="fa fa-calendar-alt mr-1 text-blue-0"></i> <?php echo $locationdates[$i] . ' ' . $locationtimes[$i]; ?></p>

                        </div>
                    </div>
                </div>
				<?php 
} ?>

<!--Hotel accommodation-->
<?php
	$tourinfos = $wpdb->get_results("SELECT * FROM `wp_hotel` WHERE `tour_id` = " . (int)$tour_id);
    $hotel_total_price = 0;
	if(!empty($tourinfos)) {
?>
                <div class="v2-header d-flex bg-blue-3 align-items-center mb-2">
                    <a href="#" class="header-left bg-blue-0 icon-wrap mr-3 text-white"><i class="fa fa-plus fa-2x"></i> </a>
                    <div class="text-white font-lg">Hotel accommodation</div>
                </div>
                <div class="v2-box mb-3">

                    <div class="v2-list">
						<?php
        foreach ($tourinfos as $tourinfo) {
            $has_hotel = true;
            $night = round((strtotime($tourinfo->end_date) - strtotime($tourinfo->start_date)) / 86400);
            $hotel_total_price += $tourinfo->total;
            $total_price += round($tourinfo->total / get_option("_cs_currency_USD"));
            ?>
                        <div class="v2-list-item d-flex flex-column flex-sm-row">
                            <div class="v2-list-left mb-3 mr-sm-3">
                                <div class="thumb-wrap">
                                    <img src="<?php echo $tourinfo->img; ?>" width="180" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="v2-list-right flex-grow-1">
                                <div class="d-flex flex-row">
                                    <div class="mr-auto">
                                        <h4 class="v2-title mt-0 mb-3"><i class="fa fa-landmark text-gray-0"></i> <?php echo $tourinfo->name; ?></h4>
                                    </div>
                                </div>

                                <div class="v2-tb font-xsmall">
                                    <div class="v2-tb-row p-1">
                                        <div class="d-flex flex-row ">
                                            <div class="p-2 flex-grow-1  max-250"><a href="#" class="fa fa-trash text-blue-0"></a> <?php echo $tourinfo->room_type; ?></div>
                                            <div class="flex-grow-1">
                                                <div class="d-flex flex-row justify-content-between">
                                                    <div class="p-2"><a href="#" class="fa fa-calendar-alt text-blue-0"></a> <?php echo date("M j, Y", strtotime($tourinfo->start_date)) . " - " . date("M j, Y", strtotime($tourinfo->end_date)); ?></div>

                                                </div>
												<div class="v2-input d-flex flex-row align-items-center mb-1">
                                                   <p><?php if (!empty($notecontents[$nindex])) echo 'Note:' . $notecontents[$nindex++]; ?></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
						<?php 
    } ?>
                    </div>
                </div>
	<?php } ?>
				
		<!--Meal-->
				<?php
    if (!empty($meal_type)) {
        $meal_types = explode(',', $meal_type);
        $meal_dates = explode(',', $meal_date);
        $meal_times = explode(',', $meal_time);
        $meal_total_price = 0;
        ?>
                <div class="v2-header d-flex bg-blue-3 align-items-center mb-2">
                    <a href="#" class="header-left bg-blue-0 icon-wrap mr-3 text-white"><i class="fa fa-plus fa-2x"></i> </a>
                    <div class="text-white font-lg">Meal</div>
                </div>
                <div class="meal v2-box mb-3">

                    <div class="v2-list">
					<?php
    for ($i = 0; $i < count($meal_types) - 1; $i++) {
        $meal_sub_items = explode('-', $meal_types[$i]);
        if (!empty($meal_sub_items[0])) {
            $mealid = intval($meal_sub_items[0]);
            $TravcustMealItem = unserialize($wpdb->get_row("SELECT * FROM `travcust_meal_new` WHERE `id` = {$mealid}")->_cs_travcust_meal);
            if (!empty($TravcustMealItem)) {
                ?>
                        <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                            <div class="v2-list-left mb-3 mr-sm-3">
                                <div class="thumb-wrap">
                                    <img src="<?php echo $TravcustMealItem['img']; ?>" width="180" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="v2-list-right">
                                <div class="d-flex flex-row">
                                    <div class="mr-auto">
                                        <h4 class="v2-title mt-0 mb-3"><i class="fa fa-utensils text-gray-0"></i> <?php echo $TravcustMealItem['item']; ?></h4>
                                        <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i>  <?php echo $TravcustMealItem['area']; ?> – <a href="#">Displayed on the map</a>  (100 m away from central area) </p>
                                    </div>
                                </div>
								<?php
        if (count($meal_sub_items) > 1) {
            $subitems = $TravcustMealItem['subitem'];
            for ($subi = 1; $subi < count($meal_sub_items); $subi++) {
                if ($meal_sub_items[$subi] == 1) {
                    $total_price += $subitems[$subi - 1]['price'];
                    $meal_total_price += $subitems[$subi - 1]['price'];
                    ?>
                                <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                    <div class="mb-4 mb-sm-0 border-sm-right mx-40"><p><?php if (!empty($notecontents[$nindex])) echo 'Note:' . $notecontents[$nindex++]; ?></p></div>
									<div class="mb-4 mb-sm-0 border-sm-right mx-40"><?php echo $subitems[$subi - 1]['name']; ?></div>
                                    <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                        <div class="flex-fill p-2 border-right ">
                                            <i class="fa fa-calendar-alt mr-1"></i> <?php echo date('M j, Y', strtotime($meal_dates[$i])); ?> <i class="fas fa-clock mr-1"></i> <?php echo $meal_times[$i]; ?>
                                        </div>
                                    </div>
                                </div>
								<?php

    }
}
}
?>

                            </div>
                        </div>
						<?php

    }
}
}
?>
                    </div>
                </div>
				<?php 
} ?>
				
				
		<!--Scenic spot-->
				<?php
    if (!empty($tourlist_name) || !empty($place_name)) {
        ?>
                <div class="v2-header d-flex bg-blue-3 align-items-center mb-2">
                    <a href="#" class="header-left bg-blue-0 icon-wrap mr-3 text-white"><i class="fa fa-plus fa-2x"></i> </a>
                    <div class="text-white font-lg">Scenic spot</div>
                </div>
                <div class="place v2-box mb-3">

                    <div class="v2-list">
				<?php
    if (!empty($place_name)) {
        $place_names = explode('--', $place_name);
        $place_imgs = explode(',', $place_img);
        $place_dates = explode(',', $place_date);
        $place_times = explode(',', $place_time);
        for ($i = 0; $i < count($place_names) - 1; $i++) {
            ?>
				   <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                           <div class="v2-list-left mb-3 mr-sm-3">
                               <div class="thumb-wrap">
                                   <img src="<?php echo $place_imgs[$i]; ?>" width="180" alt="" class="img-responsive">
                               </div>
                           </div>
                           <div class="v2-list-right">
                               <div class="d-flex flex-row">
                                   <div class="mr-auto">
                                       <h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i> <?php echo urldecode($place_names[$i]); ?></h4>
                                   </div>
                               </div>
                               <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                   <div class="mb-4 mb-sm-0 border-sm-right mx-40"><p><?php if (!empty($notecontents[$nindex])) echo 'Note:' . $notecontents[$nindex++]; ?></p></div>
                                   <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                       <div class="flex-fill p-2 border-right ">
                                           <i class="fa fa-calendar-alt mr-1"></i> <?php echo date("M j, Y", strtotime($place_dates[$i])); ?> <i class="fas fa-clock mr-1"></i> <?php echo $place_times[$i]; ?>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
			   <?php 
    }
}
?>
				
				<?php
    if (!empty($tourlist_name)) {
        $tourlist_names = explode('--', $tourlist_name);
        $tourlist_imgs = explode(',', $tourlist_img);
        $tourlist_prices = explode('--', $tourlist_price);
        $tourlist_dates = explode(',', $tourlist_date);
        $tourlist_times = explode(',', $tourlist_time);
        $tourlist_total_price = 0;

        for ($i = 0; $i < count($tourlist_names) - 1; $i++) {
            $total_price += ceil($tourlist_prices[$i] / get_option("_cs_currency_USD"));
            $tourlist_total_price += ceil($tourlist_prices[$i] / get_option("_cs_currency_USD"));
            ?>
                        <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                            <div class="v2-list-left mb-3 mr-sm-3">
                                <div class="thumb-wrap">
                                    <img src="<?php echo $tourlist_imgs[$i]; ?>" width="180" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="v2-list-right">
                                <div class="d-flex flex-row">
                                    <div class="mr-auto">
                                        <h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i> <?php echo $tourlist_names[$i]; ?></h4>
                                    </div>
                                </div>
                                <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                    <div class="mb-4 mb-sm-0 border-sm-right mx-40"><p><?php if (!empty($notecontents[$nindex])) echo 'Note:' . $notecontents[$nindex++]; ?></p></div>
                                    <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                        <div class="flex-fill p-2 border-right ">
                                            <i class="fa fa-calendar-alt mr-1"></i> <?php echo date("M j, Y", strtotime($tourlist_dates[$i])); ?> <i class="fas fa-clock mr-1"></i> <?php echo $tourlist_times[$i]; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<?php 
    } ?>
                    </div>
                </div>
				<?php 
}
} ?>
			
			
			<!--Hydrotherapy and health preservation-->
				<?php
    if (!empty($spa_type)) {
        $TravcustSpa = unserialize(get_option('_cs_travcust_spa'));
        $spa_types = explode(',', $spa_type);
        $spa_dates = explode(',', $spa_date);
        $spa_times = explode(',', $spa_time);
        $spa_total_price = 0;
        ?>
                <div class="v2-header d-flex bg-blue-3 align-items-center mb-2">
                    <a href="#" class="header-left bg-blue-0 icon-wrap mr-3 text-white"><i class="fa fa-plus fa-2x"></i> </a>
                    <div class="text-white font-lg">Hydrotherapy and health preservation</div>
                </div>
                <div class="spa v2-box mb-3">

                    <div class="v2-list">
					<?php
    for ($i = 0; $i < count($spa_types) - 1; $i++) {
        ?>
                        <div class="v2-list-item d-flex flex-column flex-sm-row">
                            <div class="v2-list-left mb-3 mr-sm-3">
                                <div class="thumb-wrap">
                                    <img src="<?php echo $TravcustSpa[$spa_types[$i] - 1]['img']; ?>" width="180" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="v2-list-right flex-grow-1">
                                <div class="d-flex flex-row">
                                    <div class="mr-auto">
                                        <h4 class="v2-title mt-0 mb-3"><i class="fa fa-spa text-gray-0"></i> <?php echo $TravcustSpa[$spa_types[$i] - 1]['item']; ?></h4>
                                        <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i>  <?php echo $TravcustSpa[$spa_types[$i] - 1]['area']; ?> – <a href="#">Displayed on the map</a>  (100 m away from central area) </p>
                                    </div>
                                </div>

                                <?php
                                if (count(explode('-', $spa_types[$i])) > 1) {
                                    ?>
								<div class="v2-tb font-xsmall">
								<?php
        $subitems = $TravcustSpa[$spa_types[$i] - 1]['subitem'];
        $subitem_types = explode('-', $spa_types[$i]);
        for ($subi = 1; $subi < count($subitem_types); $subi++) {
            if ($subitem_types[$subi] >= 1) {
                $spa_numi = $subitem_types[$subi];
                $total_price += $subitems[$subi - 1]['price'] * $spa_numi;
                $spa_total_price += $subitems[$subi - 1]['price'] * $spa_numi;
                ?>
                                    <div class="v2-tb-row p-1">
                                        <div class="d-flex flex-row ">
                                            <div class="p-2 flex-grow-1  max-250"><a href="#" class="fa fa-trash text-blue-0"></a><?php echo $subitems[$subi - 1]['name']; ?></div>
                                            <div class="flex-grow-1">
                                                <div class="d-flex flex-row justify-content-between">
                                                    <div class="p-2"><a href="#" class="fa fa-calendar-alt text-blue-0"></a> <span class="mr-3"><?php echo date("M j, Y", strtotime($spa_dates[$i])) . ' ' . $spa_times[$i]; ?></span><i class="fas fa-clock"></i> </div>
                                                    <div class="p-2"><?php echo $spa_numi . ' Person'; ?></div>
                                                </div>
												
												<div class="v2-input d-flex flex-row align-items-center mb-1">
                                                   <p><?php if (!empty($notecontents[$nindex])) echo 'Note:' . $notecontents[$nindex++]; ?></p>
												</div>

                                            </div>
                                        </div>
                                    </div>
									<?php

    }
}
?>
								</div>
								<?php 
    } ?>
                            </div>
                        </div>
						<?php 
    } ?>
                    </div>
                </div>
				<?php 
} ?>
				
		<!--Tour Guide Service-->
				<?php
    if (!empty($popupguide_type)) {
        $TravcustPopupguide = unserialize(get_option('_cs_travcust_popupguide'));
        $popupguide_types = explode(',', $popupguide_type);
        $popupguide_dates = explode(',', $popupguide_date);
        $popupguide_times = explode(',', $popupguide_time);
        $guide_total_price = 0;
        ?>
                <div class="v2-header d-flex bg-blue-3 align-items-center mb-2">
                    <a href="#" class="header-left bg-blue-0 icon-wrap mr-3 text-white"><i class="fa fa-plus fa-2x"></i> </a>
                    <div class="text-white font-lg">Tour Guide Service</div>
                </div>
                <div class="guide v2-box mb-3">

                    <div class="v2-list">
					<?php
    for ($i = 0; $i < count($popupguide_types) - 1; $i++) {
        $total_price += $TravcustPopupguide[$popupguide_types[$i] - 1]['price'];
        $guide_total_price += $TravcustPopupguide[$popupguide_types[$i] - 1]['price'];
        ?>
                        <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                            <div class="v2-list-left mb-3 mr-sm-3">
                                <div class="thumb-wrap">
                                    <img src="<?php echo $TravcustPopupguide[$popupguide_types[$i] - 1]['img']; ?>" width="180" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="v2-list-right">
                                <div class="d-flex flex-row">
                                    <div class="mr-auto">
                                        <h4 class="v2-title mt-0 mb-3"><i class="fa fa-running text-gray-0"></i> <?php echo $TravcustPopupguide[$popupguide_types[$i] - 1]['item']; ?></h4>
                                        <p class="v2-address mb-3"><i class="fa fa-mobile-alt mr-1 text-blue-0"></i> <?php echo $TravcustPopupguide[$popupguide_types[$i] - 1]['phone']; ?> </p>
										<p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i> <?php echo $TravcustPopupguide[$popupguide_types[$i] - 1]['area']; ?> </p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                    <div class="mb-4 mb-sm-0 border-sm-right mx-40"><p><?php if (!empty($notecontents[$nindex])) echo 'Note:' . $notecontents[$nindex++]; ?></p></div>
                                    <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                        <div class="flex-fill p-2 border-right ">
                                            <i class="fa fa-calendar-alt mr-1 text-blue-0"></i> <?php echo date("M j, Y", strtotime($popupguide_dates[$i])) . ' ' . $popupguide_times[$i]; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<?php 
    } ?>
                    </div>
                </div>
				<?php 
} ?>

		<!--Shuttle bus service-->
				<?php
    if (!empty($car_type)) {
        $has_vehicles = true;
        $TravcustCar = unserialize(get_option('_cs_travcust_car'));
        $car_types = explode(',', $car_type);
        $car_dates = explode(',', $car_date);
        $car_times = explode(',', $car_time);
        $car_total_price = 0;
        ?>
                <div class="v2-header d-flex bg-blue-3 align-items-center mb-2">
                    <a href="#" class="header-left bg-blue-0 icon-wrap mr-3 text-white"><i class="fa fa-plus fa-2x"></i> </a>
                    <div class="text-white font-lg">Shuttle bus service</div>
                </div>
                <div class="car v2-box mb-3">

                    <div class="v2-list">
					<?php
    for ($i = 0; $i < count($car_types) - 1; $i++) {
        if (floor($car_types[$i]) + 0.5 == $car_types[$i]) {
            $half_price = 0.5;
            $car_types[$i] = floor($car_types[$i]);
        } else
            $half_price = 1;
        $total_price += $TravcustCar[$car_types[$i] - 1]['price'] * get_option('person_price_ration') * $half_price;
        $car_total_price += $TravcustCar[$car_types[$i] - 1]['price'] * get_option('person_price_ration') * $half_price;
        ?>
                        <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                            <div class="v2-list-left mb-3 mr-sm-3">
                                <div class="thumb-wrap">
                                    <img src="<?php echo $TravcustCar[$car_types[$i] - 1]['img']; ?>" width="180" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="v2-list-right">
                                <div class="d-flex flex-row">
                                    <div class="mr-auto">
                                        <h4 class="v2-title mt-0 mb-3"><i class="fa fa-car text-gray-0"></i> <?php echo $TravcustCar[$car_types[$i] - 1]['item'];
                                                                                                            if ($half_price == 0.5) echo ' (half day or airport transfer)'; ?></h4>
                                        <p class="v2-address mb-3"><i class="fa fa-mobile-alt mr-1 text-blue-0"></i> <?php echo $TravcustCar[$car_types[$i] - 1]['phone']; ?> </p>
                                    </div>
                                </div>
                                <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                    <div class="mb-4 mb-sm-0 border-sm-right mx-40"><p><?php if (!empty($notecontents[$nindex])) echo 'Note:' . $notecontents[$nindex++]; ?></p></div>
                                    <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                        <div class="flex-fill p-2 border-right ">
                                            <i class="fa fa-calendar-alt mr-1 text-blue-0"></i> <?php echo date("M j, Y", strtotime($car_dates[$i])) . ' ' . $car_times[$i]; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<?php 
    } ?>
                    </div>
                </div>
				<?php 
} ?>


<!--Boat & Helicopters-->
				<?php
    if (!empty($boat_type)) {
        $TravcustBoat = unserialize(get_option('_cs_travcust_boat'));
        $boat_types = explode(',', $boat_type);
        $boat_dates = explode(',', $boat_date);
        $boat_times = explode(',', $boat_time);
        $boat_total_price = 0;
        ?>
				<div class="v2-header d-flex bg-blue-3 align-items-center mb-2">
					<a href="#" class="header-left bg-blue-0 icon-wrap mr-3 text-white"><i class="fa fa-plus fa-2x"></i> </a>
					<div class="text-white font-lg">Boat & Helicopters</div>
				</div>
				<div class="boat v2-box mb-3">

                   <div class="v2-list">
					<?php
    for ($i = 0; $i < count($boat_types) - 1; $i++) {
        ?>
                       <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                           <div class="v2-list-left mb-3 mr-sm-3">
                               <div class="thumb-wrap">
                                   <img src="<?php echo $TravcustBoat[$boat_types[$i] - 1]['img']; ?>" width="180" alt="" class="img-responsive">
                               </div>
                           </div>
                           <div class="v2-list-right">
                               <div class="d-flex flex-row">
                                   <div class="mr-auto">
                                       <h4 class="v2-title mt-0 mb-3"><i class="fa fa-utensils text-gray-0"></i> <?php echo $TravcustBoat[$boat_types[$i] - 1]['item']; ?></h4>
                                       <p class="v2-address mb-3"><i class="fa fa-map-marker mr-1 text-blue-0"></i>  <?php echo $TravcustBoat[$boat_types[$i] - 1]['area']; ?> </p>
                                   </div>
                               </div>
							   <?php
            if (count(explode('-', $boat_types[$i])) > 1) {
                $subitems = $TravcustBoat[$boat_types[$i] - 1]['subitem'];
                $subitem_types = explode('-', $boat_types[$i]);
                for ($subi = 1; $subi < count($subitem_types); $subi++) {
                    if ($subitem_types[$subi] == 1) {
                        $total_price += $subitems[$subi - 1]['price'];
                        $boat_total_price += $subitems[$subi - 1]['price'];
                        ?>
                               <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                   <div class="mb-4 mb-sm-0 border-sm-right mx-40" style="width: 500px;"><?php echo $subitems[$subi - 1]['name']; ?></div>
								   <div class="flex-grow-1">
                                   <div class="d-flex flex-row justify-content-between align-items-center text-center">
                                       <div class="flex-fill p-2 border-right ">
                                           <i class="fa fa-calendar-alt mr-1"></i> <?php echo date("M j, Y", strtotime($boat_dates[$i])); ?> <i class="fas fa-clock mr-1"></i> <?php echo $boat_times[$i]; ?>
                                       </div>
                                   </div>
								   <div class="v2-input d-flex flex-row align-items-center mb-1">
									<p><?php if (!empty($notecontents[$nindex])) echo 'Note:' . $notecontents[$nindex++]; ?></p>
                                   </div>
								   </div>
                               </div>
							   <?php

        }
    }
}
?>
                           </div>

                       </div>
					   <?php 
    } ?>
                   </div>
               </div>
			   <?php 
    } ?>
			   
			   <!--Extra Activity-->
			   <?php
        $ExtraActivity = unserialize(get_option('_cs_extraactivity'));
        if (!empty($ExtraActivity) && !empty($eaiitem) && !empty($eainum) && !empty($eaidate) && !empty($eaitime)) {
            $activity_total_price = 0;
            ?>
               <div class="v2-header d-flex bg-blue-3 align-items-center mb-2">
                   <a href="#" class="header-left bg-blue-0 icon-wrap mr-3 text-white"><i class="fa fa-plus fa-2x"></i> </a>
                   <div class="text-white font-lg">Optional Activity</div>
               </div>
               <div class="activity v2-box mb-3">

                   <div class="v2-list">
				   <?php
        $eaiitems = explode(',', $eaiitem);
        $eainums = explode(',', $eainum);
        $eaidates = explode(',', $eaidate);
        $eaitimes = explode(',', $eaitime);
        for ($i = 0; $i < count($eaiitems) - 1; $i++) {
            $p = $eaiitems[$i];
            $has_activity = true;
            $total_price += $ExtraActivity[$p]['price'] * $eainums[$i];
            $activity_total_price += $ExtraActivity[$p]['price'] * $eainums[$i];
            ?>
                       <div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue">
                           <div class="v2-list-left mb-3 mr-sm-3">
                               <div class="thumb-wrap">
                                   <img src="<?php echo $ExtraActivity[$p]['img']; ?>" width="180" alt="" class="img-responsive">
                               </div>
                           </div>
                           <div class="v2-list-right">
                               <div class="d-flex flex-row">
                                   <div class="mr-auto">
                                       <h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i> <?php echo $ExtraActivity[$p]['item']; ?> </h4>
                                   </div>
                               </div>
                               <div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch">
                                   <div class="mb-4 mb-sm-0 border-sm-right mx-40"><p><?php if (!empty($notecontents[$nindex])) echo 'Note:' . $notecontents[$nindex++]; ?></p></div>
                                   <div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">
                                       <div class="flex-fill p-2 border-right ">
                                           <i class="fa fa-calendar-alt mr-1 text-blue-0"></i> <?php echo date("M j, Y", strtotime($eaidates[$i])) . ' ' . $eaitimes[$i]; ?>
                                       </div>
                                       <div class="flex-fill p-2 border-right ">
                                           Quantity: <?php echo $eainums[$i]; ?>
                                       </div>
                                   </div>
                               </div>

                           </div>

                       </div>
				<?php

}
?>
                   </div>
               </div>
			   <?php 
    } ?>
			   
<!--map-->
				<?php
    $total_price *= get_option('person_price_ration');
    $discount = 0;
    $total_discount = $total_price - $discount;
    $tax = $total_discount * (1 / 100);
    $net_price = $total_discount + $tax;
    ?>
                <div class="d-flex flex-column flex-sm-row mb-3">
                    <div id="sygicmap" class="v2-map pr-sm-3 mb-3 mb-sm-0 may-view" style="width:50%; height:350px; border: none;background: none;padding-top: 0;">
                    </div>
                    <div class="v2-total bg-blue-3 pl-sm-3 border-radius  flex-fill">
                        <div class="d-flex justify-content-end p-4 text-white font-sm text-right">
                            <div class="pr-2">
                                <div class="mb-2">TOTAL COST: </div>
                                <div class="mb-2">APPLICABLE DISCOUNT:</div>
                                <div class="mb-2">TOTAL COST AFTER DISCOUNT: </div>
                                <div class="mb-2">TAX 1%: </div>
                                <div class="mb-2">NET PRICE: </div>
                            </div>
                            <div class="font-weight-bold">
                                <div class="mb-2 total_price"><span class="change_price"or_pic="<?php echo $total_price; ?>">Rp<span class="price_span"><?php echo $total_price; ?></span></span></div>
                                <div class="mb-2 discount"><span class="change_price"or_pic="<?php echo $discount; ?>">Rp<span class="price_span"><?php echo $discount; ?></span></span></div>
                                <div class="mb-2 total_discount"><span class="change_price"or_pic="<?php echo $total_discount; ?>">Rp<span class="price_span"><?php echo $total_discount; ?></div>
                                <div class="mb-2 tax"><span class="change_price"or_pic="<?php echo $tax; ?>">Rp<span class="price_span"><?php echo $tax; ?></span></span></div>
                                <div class="mb-2 net_price"><span class="change_price"or_pic="<?php echo $net_price; ?>">Rp<span class="price_span"><?php echo $net_price; ?></span></span></div>
                            </div>
                        </div>

                    </div>
                </div>
				
	</div>
	

<!-- Related article content -->
<?php
if(!empty($tour_id) && $wpdb->get_var("SELECT COUNT(*) FROM `wp_postmeta` WHERE `meta_key`='tour_id' AND `meta_value`='{$tour_id}'")==1 )
{
	$post_id=$wpdb->get_row("SELECT post_id FROM `wp_postmeta` WHERE `meta_key`='tour_id' AND `meta_value`='{$tour_id}'")->post_id;
	?>
	<div>
		<p><?php echo get_post($post_id)->post_content; ?></p>
	</div>
	<?php
}
?>
				
				
<!-- Foot content -->
<section data-id="13ab0e2" class="elementor-element elementor-element-13ab0e2 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="e8ecac4" class="elementor-element elementor-element-e8ecac4 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="8067f38" class="elementor-element elementor-element-8067f38 elementor-widget elementor-widget-divider" data-element_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator"></span>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
		<section data-id="8708bb0" class="elementor-element elementor-element-8708bb0 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="d5f1a19" class="elementor-element elementor-element-d5f1a19 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="2cecb7b" class="elementor-element elementor-element-2cecb7b elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-large">Powered by</p>		</div>
				</div>
				<section data-id="4e6375f" class="elementor-element elementor-element-4e6375f elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-wider">
				<div class="elementor-row">
				<div data-id="62672e8" class="elementor-element elementor-element-62672e8 elementor-sm-50 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="14e9ce0" class="elementor-element elementor-element-14e9ce0 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
										<img width="177" height="197" src="https://www.travpart.com/English/wp-content/uploads/2018/09/new-logo.png" class="attachment-full size-full" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="9b85a69" class="elementor-element elementor-element-9b85a69 elementor-sm-50 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="ccc0cd4" class="elementor-element elementor-element-ccc0cd4 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
										<img width="515" height="144" src="https://www.travpart.com/English/wp-content/uploads/2018/09/logo-2.jpg" class="attachment-full size-full" alt="" srcset="https://www.travpart.com/English/wp-content/uploads/2018/09/logo-2.jpg 515w, https://www.travpart.com/English/wp-content/uploads/2018/09/logo-2-300x84.jpg 300w" sizes="(max-width: 515px) 100vw, 515px">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>

<input id="cs_RMB" type="hidden" value="<?php echo get_option('_cs_currency_RMD'); ?>" />
<input id="cs_USD" type="hidden" value="<?php echo get_option('_cs_currency_USD'); ?>" />
<input id="cs_AUD" type="hidden" value="<?php echo get_option('_cs_currency_AUD'); ?>" />
		
<script>		
jQuery(document).ready(function(){
$(".header_curreny").change(function () {

        var full_name = $(this).find("option:selected").attr("full_name");
        var syml = $(this).find("option:selected").attr("syml");

        var curreny = $(this).val();

        if (curreny == "IDR") {

            $(".change_price").each(function () {
                $(this).html(syml + "<span class='price_span'>" + $(this).attr("or_pic") + " </span>");

            });

        } else {
            $(".change_price").each(function () {
				var amount = $(this).attr("or_pic");
				if (syml == '元')
				{
                    $(this).html("<span class='price_span'>" + Math.round($('#cs_RMB').val()*amount) + " </span>" + syml);
				}
				else
				{
                    $(this).html(syml + "<span class='price_span'>" + ($('#cs_'+curreny).val()*Math.round(amount)).toFixed(2) + " </span>");
				}
            });
        }
    });
	setTimeout('$(".header_curreny").change()',800);
});


	var vendor_id = getQueryParam("vendor_id");
	var bookingcode = '<?php echo generate_revnum($tour_id); ?>';
	var agent_name = getQueryParam("agent_name");

	document.getElementById("bookingcodeid").innerHTML = bookingcode;
	document.getElementById("agentnameid").innerHTML = agent_name;

	function getQueryParam(param) {
    location.search.substr(1)
        .split("&")
        .some(function(item) {
            return item.split("=")[0] == param && (param = item.split("=")[1])
        })
    return param
}

</script>
				
<?php

} else {
    get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>

