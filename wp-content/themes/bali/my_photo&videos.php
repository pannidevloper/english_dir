<?php
/*
Template Name: My Photos & Videos
*/

global $wpdb;
if (isset($_GET['user'])) {
    $username = filter_input(INPUT_GET, 'user', FILTER_SANITIZE_STRING);
    $user = get_user_by('login', $username);
    if (is_numeric($_GET['user'])) {
        $user = get_user_by('id', intval($_GET['user']));
        $username = $user->user_login;
    }

    if ($user) {
        $user_id = $user->ID;
        $verified = get_user_meta($user_id, 'verification_status', true);
        $gender = get_user_meta($user_id, 'gender', true);
        $bio = get_user_meta($user_id, 'description', true);
        $follow_count = $wpdb->get_var("SELECT COUNT(*) FROM `social_follow` WHERE `sf_agent_id` = '{$user_id}'");
        um_fetch_user($user_id);

        //connecton count

        $connection_count = $wpdb->get_var(
            "SELECT COUNT(*) FROM friends_requests WHERE  user2 = '{$user_id}' OR user1 = '{$user_id}' AND status=1"
        );

    if(function_exists('visitor_profile_logs') AND is_user_logged_in()) {
    visitor_profile_logs ('my_photo_videos',$user_id,$current_user->ID );
}

        // user role

        $user_meta = get_userdata($user_id);
        $user_roles = $user_meta->roles;

        if (in_array('um_travel-advisor', $user_roles, true)) {
            $role = "Seller";
        } elseif (in_array('um_clients', $user_roles, true)) {

            $role = "Buyer";
        } else {
            $role = "";
        }

        $user_photos = $wpdb->get_results("SELECT guid as src,post_mime_type,ID FROM `wp_posts` WHERE `post_author` = '{$user->ID}'
        AND post_status='inherit' AND `post_type` = 'attachment'
        ORDER BY `post_date` DESC");


        $user_location = $wpdb->get_row("SELECT region,country FROM `user` WHERE username='{$user->user_login}'");
        if (!empty($user_location)) {
            $user_location = trim($user_location->region . ', ' . $user_location->country, ',');
        }
        $location_visiable = get_user_meta($user->ID, 'location_visiable', true);
        if ($location_visiable == 'anyone' || $user->ID == wp_get_current_user()->ID) {
            $location_visiable = true;
        } else if (empty($location_visiable) || $location_visiable == 'family') {
            if (strpos(get_user_meta($user->ID, 'family_member', true), wp_get_current_user()->user_login) !== false) {
                $location_visiable = true;
            } else {
                $location_visiable = false;
            }
        } else if ($location_visiable == 'friend') {
            $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}' AND user2=" . wp_get_current_user()->ID);
            if ($is_friend == 0) {
                $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}' AND user1=" . wp_get_current_user()->ID);
            }
            if ($is_friend > 0) {
                $location_visiable = true;
            } else {
                $location_visiable = false;
            }
        } else if ($location_visiable == 'family_friend') {
            if (strpos(get_user_meta($user->ID, 'family_member', true), wp_get_current_user()->user_login) !== false) {
                $location_visiable = true;
            } else {
                $location_visiable = false;
            }
            if (!$location_visiable) {
                $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}' AND user2=" . wp_get_current_user()->ID);
                if ($is_friend == 0) {
                    $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}' AND user1=" . wp_get_current_user()->ID);
                }
                if ($is_friend > 0) {
                    $location_visiable = true;
                } else {
                    $location_visiable = false;
                }
            }
        } else {
            $location_visiable = false;
        }
    } else {
        $user_id = false;
    }
} else {
    exit(wp_redirect(home_url('login')));
}
get_header();

?>
<script src="<?php echo get_template_directory_uri(); ?>/js/my_photo_video.js?v=<?=time()?>"></script>
<link async="async" href="<?php bloginfo('template_url'); ?>/css/tpl-mytimeline.css?v=<?=time()?>" rel="stylesheet" type="text/css" />
<input id="admin-ajax-url" type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" />

<style>
    @media only screen and (max-width: 992px) {
        .header_txt {
            display: none;
        }

        .button-link a {
            padding: 7px 11px;
        }

        .connectionre {
            margin: -6px 0px 0px 0px;
        }

        .connectionre a {
            font-size: 14px;
        }

        .button-text-mobile {
            font-size: 14px !important;
        }
    }
</style>
<?php
$display_name = um_get_display_name($user_id);
?>

<div class="space_top_onm">
    <div class="hide_mobile_dsa">
        <?php include 'timeline-common-section.php'; ?>
    </div>

    <div class="row hide-app-header">
        <div class="col-md-12">
            <div class="ph_vd_head">

                <div class="row mobile_r">
                    <div class="make-left">
                        <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $username; ?>" class="btn btn-info">
                            <i class="fas fa-arrow-left"></i>
                            Back
                        </a>
                    </div>
                    <div class="make-right">
                        <h3><?php echo $display_name; ?> Photos and Videos</h3>
                    </div>
                </div>
                <div class="row desktop_r">
                    <div class="col-xs-12">
                        <h3><?php echo $display_name; ?> Photos and Videos</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row for_mobile">
        <?php foreach ($user_photos as $p) {
            $shortpost_id = $wpdb->get_var("SELECT post_id FROM `wp_postmeta` WHERE `meta_key` = 'attachment' AND `meta_value`='{$p->ID}'");
            if(!empty($shortpost_id)) {
                $shortpost_link = get_permalink($shortpost_id);
            }
            else {
                $shortpost_link = $p->src;
            }
            ?>
            <div class="col-md-3 col-xs-6 photo-img">
                
                    <?php if (stripos($p->post_mime_type, 'image') !== false) { ?>
                    <a href="#" class="photo-video-popup" data-toggle="modal" data-target="#photo-video-popup" postid="<?php echo $p->ID; ?>">
                        <img src="<?php echo $p->src; ?>">
                        <div class="pv_post_video_overlay pv_post_photo_over2">
                                                               <div class="post_vd_ovlay_btn"> 
                                                                   <!--<i class="fas fa-volume-up"></i>-->
                                                                        
                                                                <div class="share-area" style="float: right;padding: 0px;">
                                                                    <a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
                                                                        <i class="fas fa-share"></i> Share
                                                                    </a>

                                                                    <!--Share My DropDown PC-->
                                                                    <div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2" aria-labelledby="menu2">
                                                                    <!--
                                                                        <li onclick="share_feed(<?php echo $p->ID; ?>)">Share Now</li>
                                                                    -->
                                                                        <li><a target="_blank" onclick="record_share_data('<?php echo $shortpost_link; ?>','whatsapp')" href="https://web.whatsapp.com/send?text=<?php  echo $p->ID; ?>"><div class="wh_ph_vd_icon_div"><i class="fab fa-whatsapp share_wh_ph_vd_i"></i></div>Share to whats app</a></li>
                                                                        <a style="display: none" id="copytext<?php echo $p->ID; ?>" href="<?php echo $shortpost_link; ?>"></a>
                                                                        <li><a href="#" onclick="copy_link('<?php echo $p->ID;  ?>')"><i class="far fa-copy share_copy_link_ph_vd_i"></i>Copy link to this post</a></li>
                                                                      <!--  <li class="mores"><a class="a2a_dd" href="#"><i class="fas fa-plus-square"></i> More...</a></li>
                                                                      -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="<?php echo $shortpost_link ?>" data-a2a-title="<?php esc_html( get_the_title() )  ?>">
    <a class="a2a_button_facebook" onclick="record_share_data('<?php echo $shortpost_link; ?>','fb')"></a>
    <a class="a2a_button_twitter" onclick="record_share_data('<?php echo $shortpost_link; ?>','twitter')"></a>
    <a class="a2a_dd" href="https://www.addtoany.com/share" onclick="record_share_data('<?php echo $item->ID; ?>','other')"></a>
</div>

<script async src="https://static.addtoany.com/menu/page.js"></script>
                                                                    </div>

                                                                    <!--End My DropDown-->

                                                                </div>
                                                                <div style="clear: both;"> </div>
                                                               </div>
                                                            </div>

                    </a>
                    <?php } else {
                        $video_thumbnail_url = get_post_meta($p->ID, 'kgvid-poster-url', true); ?>
                        <video poster="<?php echo $video_thumbnail_url ?>" src="<?php echo $p->src; ?>"></video>
                        <div class="pv_post_video_overlay">
                                                               <div class="post_vd_ovlay_btn">
                                                               <div style="float: left;">
                                                                   <a href="<?php echo $p->src; ?>" class="photo-video-popup" data-toggle="modal" data-target="#photo-video-popup" postid="<?php echo $p->ID; ?>">
                                                                        <i class="fas fa-play"></i>
                                                                   </a>
                                                               </div>  
                                                                   <!--<i class="fas fa-volume-up"></i>-->
                                                                        
                                                                <div class="share-area" style="float: right;padding: 0px;">
                                                                    <a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
                                                                        <i class="fas fa-share"></i> Share
                                                                    </a>

                                                                    <!--Share My DropDown PC-->
                                                                    <div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2" aria-labelledby="menu2">
                                                    <!--
                                                                        <li onclick="share_feed(<?php echo $p->ID; ?>)">Share Now</li>
                                                                        --->
                                                                        <li><a target="_blank" onclick="record_share_data('<?php echo $shortpost_link; ?>','whatsapp')" href="https://web.whatsapp.com/send?text=<?php  echo $shortpost_link ?>"><div class="wh_ph_vd_icon_div"><i class="fab fa-whatsapp share_wh_ph_vd_i"></i></div>Share to whats app</a></li>

                                                                        <a  style="display: none" id="copytext<?php echo $p->ID; ?>" href="<?php echo $shortpost_link ?>">
                                                                        </a>

                                                                        <li><a href="<?php echo $p->ID;  ?>" onclick="copy_link('<?php echo $p->ID; ?>')"><i class="far fa-copy share_copy_link_ph_vd_i"></i>Copy link to this post</a></li>
                                                                      <!--  <li class="mores"><a class="a2a_dd" href="#"><i class="fas fa-plus-square"></i> More...</a></li>
                                                                      -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="<?php echo $shortpost_link ?>" data-a2a-title="<?php esc_html( get_the_title() )  ?>">
    <a class="a2a_button_facebook" onclick="record_share_data('<?php echo $shortpost_link; ?>','fb')"></a>
    <a class="a2a_button_twitter" onclick="record_share_data('<?php echo $shortpost_link; ?>','twitter')"></a>
    <a class="a2a_dd" href="https://www.addtoany.com/share" onclick="record_share_data('<?php echo $shortpost_link; ?>','other')"></a>
</div>

<script async src="https://static.addtoany.com/menu/page.js"></script>
                                                                    </div>

                                                                    <!--End My DropDown-->

                                                                </div>
                                                                <div style="clear: both;"> </div>
                                                               </div>
                                                            </div>
                    <?php } ?>
            </div>
        <?php } ?>

    </div>
</div>



<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade  bs-example-modal-lg" id="photo-video-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog height_width" role="document">
        <div class="modal-content modal-content_2">
            <div class="pupup_user_img main_wrapper">
                <div class="wrapper_row">
                    <div class="maing">
                        <div class="img_div">
                            <img src="https://www.travpart.com/English/wp-content/uploads/2020/05/13076748_983512255090176_8918979386910627918_n.jpg" />
                        </div>
                        <div class="video_div">
                            <video src="" controls="controls" id="video1"></video>
                        </div>
                        <div class="buttons_tools">
                            <div class="row">
                                <div class="col-md-7">

                                </div>
                                <div class="col-md-5">
                                    <div class="btn_default">
                                        <div class="dropdown">
                                            <button class="btn btn-default" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Options
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" id="menu2" aria-labelledby="drop5">
                                                <?php if (is_user_logged_in() && get_current_user_id() == $user_id) { ?>
                                                    <li><a href="#" class="delete_attachment" postid="">Delete This <span>Photo</span></a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Make Cover Photo</a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($_GET['device'] == 'app') { ?>

    <style>
        @media screen and (max-width: 600px) {
            .photo_video_mobile {
                margin-top: 0px !important;
            }

            .hide-app-header {
                display: none;
            }

            .custom_profile.cProfile-mobile {
                display: block !important;
            }

            .custom_profile.cProfile-mobile .ph_vd_mob_back {
                display: none;
            }
        }
    </style>
<?php } ?>
<style type="text/css">
    .pv_post_video_overlay {
        transition: .5s ease;
        position: absolute;
        top: 87%;
        background: rgba(0,0,0,0.7);
        left: 52.5%;
        width: 85%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
        z-index: 9999;
    }

    .ShareDropdown {
        z-index: 99999!important;
        top: 30px;
    }

    .pv_post_video_overlay.pv_post_photo_over2{
        top: 88%;
        left: 53%;
    }
    .vd_ovlay_btn, .post_vd_ovlay_btn {
        color: white;
        font-size: 16px;
        padding: 6px 15px;
    }
    .Cphotohide {
        display: none !important;
    }

    .mobile_r {
        display: none;
    }

    .btn_default {
        padding: 10px;
    }

    .btn_default .btn {
        background: none !important;
        border: 0px;
        font-size: 15px;
        color: #e4dfdfd1 !important;
        outline: none;
        font-weight: bold;
    }

    .buttons_tools {
        position: absolute;
        background: #0000009e;
        bottom: 0;
        width: 100%;
    }

    .maing {
        position: relative;
    }

    .img_div img {
        width: 100%;
    }

    .video_div {
        height: 500px;
    }

    .video_div video {
        height: 92%;
    }

    .height_width {
        max-width: 50% !important;
    }

    .close_ntm {
        border: 1px solid #ada9a9;
        padding: 3px 13px;
        font-size: 13px;
        color: #333333bf;
        background: #f5f6f7;
        font-weight: bold;
    }

    .save_ntm {
        border: 1px solid #28a745;
        background: #28a745;
        padding: 3px 13px;
        font-size: 13px;
        color: #fff;
        font-weight: bold;
    }

    .font_size_d {
        font-size: 13px;
        font-weight: bold;
        color: #333333d1;
    }

    .modal-backdrop {
        display: none;
    }

    .modal-content_2 {
        width: 100%;
    }
    .wh_ph_vd_icon_div{
        display: inline-flex;
        background-color: green;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        margin-right: 5px;
        justify-content: center;
        align-items: center;
    }
    i.fab.fa-whatsapp.share_wh_ph_vd_i{
        color: white;
        font-size: 17px;
        /* position: relative;
        top: 3px;
        left: 5px; */
    }

    i.far.fa-copy.share_copy_link_ph_vd_i{
        position: relative;
        margin-right: 5px;
        top: 3px;
        font-size: 22px;
        margin-left: 3px;
        margin-bottom: 5px;
        color: black;
    }
    @media(max-width: 600px) {
        .photo_video_mobile {
            display: block !important;
            margin-top: 50px;
        }

        .height_width {
            max-width: 100% !important;
        }

        .btn_default {
            padding: 0px;
        }

        #menu2 li a {
            color: #000000ab;
            padding: 10px;
            font-weight: bold;
        }

        .fade {
            opacity: initial !important;
        }

        .make-left {
            display: inline-block;
            vertical-align: middle;
            width: 15%;
        }

        .make-left .btn {
            text-decoration: none;
            color: #fff;
            font-size: 15px;
            padding: 5px 15px;
        }

        .ph_vd_head h3 {
            font-size: 14px !important;
            font-weight: bold;
        }

        .make-right {
            display: inline-block;
            vertical-align: middle;
            width: 80%;
        }

        .mobile_r {
            display: block;
        }

        .desktop_r {
            display: none;
        }

        .photo-img img,
        .photo-img video {
            width: 100% !important;
        }

        .space_top_onm {
            margin-top: 50px;
        }

        .photos_mob_header {
            box-shadow: 0 1px 1px 0 rgba(60, 64, 67, .08), 0 1px 3px 1px rgba(60, 64, 67, .16);
            padding: 10px;
            text-align: center;
        }

        .ph_vd_mob_gallery {
            margin: 5px;
            border: 1px solid #ccc;
            float: left;
            width: 30%;
        }

        .ph_vd_mob_gallery img {
            width: 100%;
            height: 95px;
        }

        .ph_vd_mob_gallery video {
            width: 100%;
            height: 95px;
        }

        .ph_vd_mob_back {
            margin: 15px;
        }

        .ph_vd_mob_back a {
            background-color: #28a745;
            font-family: "Roboto", Sans-serif;
            font-weight: 600;
            padding: 7px 7px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            color: #fff;
            text-decoration: none;
            display: inline-block;
        }
        .wh_ph_vd_icon_div{
            width: 20px;
            height: 20px;
        }
        i.fab.fa-whatsapp.share_wh_ph_vd_i{
            font-size: 12px;
        }
        i.far.fa-copy.share_copy_link_ph_vd_i{
            font-size: 16px;
        }
    }




    .photo_video_mobile {
        display: none;
    }

    .updateprofilepic {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 5px;
        margin-left: auto;
        margin-right: auto;
        width: 100px;
        text-align: center;
        background: #cacaca;
        padding: 5px;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
        box-shadow: 2px 2px 5px #666;
        color: #444444;
    }

    .updateprofilepic:hover {
        background: #1abc9c;
        cursor: pointer;
    }

    .custom_height_bio {
        text-align: center;
        background-color: white;
        padding-bottom: 15px;
    }

    .editprofile {
        text-align: center;
        background: #cacaca;
        padding: 5px;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
        color: #444444;
        box-shadow: 2px 2px 5px #666;
        cursor: pointer;
        margin: 15px;
    }

    .pro_photo_bio_sec {
        background-color: lightgrey;
    }

    .header_txt h2 {
        text-transform: uppercase;
    }

    .photo-img img,
    .photo-img video {
        width: 250px;
        height: 250px;
    }

    .col-md-3.photo-img video {
        border: solid 1px lightgrey;
        margin: 13px;
    }

    .ph_vd_button_time {
        text-align: center;
        margin-top: 10px;
    }

    .ph_vd_button_time a {
        background-color: #28a745;
        font-family: "Roboto", Sans-serif;
        font-weight: 600;
        padding: 7px 7px;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        color: #fff;
        text-decoration: none;
        display: inline-block;
    }

    .ph_vd_del_button {
        text-align: center;
        margin-bottom: 10px;
    }

    .delete_btn {
        background-color: #28a745;
        font-family: "Roboto", Sans-serif;
        font-weight: 600;
        padding: 5px 10px;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        color: #fff;
    }

    .ph_vd_del_button a {
        background-color: #28a745;
        font-family: "Roboto", Sans-serif;
        font-weight: 600;
        padding: 5px 10px;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        color: #fff;
        text-decoration: none;
        display: inline-block;
    }

    .button-text-tp {
        font-size: 12px;
        color: #fff;
    }

    a.morelink {
        text-decoration: none;
        outline: none;
    }

    .more {
        font-size: 14px;
        font-family: 'Roboto', sans-serif;
    }

    .morecontent span {
        display: none;
    }

    .comment {
        width: 400px;
        background-color: #f0f0f0;
        margin: 10px;
    }

    .um-dropdown {
        top: 60px !important;
    }

    .btn_connect {
        right: 10px;
        top: 48px;
    }

    .btn_dots {
        top: 10px;
        left: 116px;
    }

    .btn_profile_dots {
        position: absolute;
        background: #20a5ca;
        padding: 11px 5px;
        border: 0px;
        letter-spacing: 1px;
        border-radius: 5px;
        color: #fff;
        font-size: 13px;
        font-family: "Roboto", Sans-serif;
        cursor: pointer;
    }

    .btn_profile_dots i {
        font-size: 6px;
        padding: 1px;
    }


    button.btn_profile_con:hover {
        opacity: inherit;
    }

    button.btn_profile_con:focus {
        outline: none;
    }

    .dots_box {
        width: 124px;
        height: 49px;
        background-color: #ffffff;
        color: #8e9094;
        padding-top: 10px;
        padding-bottom: 10px;
        float: left;
        display: none;
    }

    .dots_box.dots_arrow_top {
        position: absolute;
        left: 30px;
        top: 49px;
    }

    .dots_box.dots_arrow_top:after {
        content: " ";
        position: absolute;
        right: 15px;
        top: -6px;
        border-top: none;
        border-right: 6px solid transparent;
        border-left: 6px solid transparent;
        border-bottom: 6px solid #ffffff;
    }

    .cCovercommentR {
        background: #F2F3F5;
    }

    .cCovercommentR img {
        width: 35px !important;
        border-radius: 50% !important;
        display: inline-block !important;
        height: 35px !important;
        left: inherit !important;
        margin-left: 15px;
        position: inherit !important;
        margin: 5px 10px;
        margin-right: 5px;
    }

    .cCovercommentR input {
        width: 82%;
        border-radius: 5px;
        border: none;
        background-color: #fff;
        border: 1px solid #ccd0d5;
        border-radius: 16px;
        padding: 5px 15px;
        font-size: 14px;
        position: relative;
        top: 2px;
    }

    .cCovercommentR input:focus {
        outline: none;
    }

    .Csocial-area-righ {
        margin-top: 50px;
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }

    .CsocialThumbs {
        padding: 10px 19px !important;
    }

    .CsocialThumbs a {
        color: #616161 !important;
    }

    .cover-user-r {
        position: relative;
    }

    .C-date a {
        font-size: 14px !important;
        position: absolute !important;
        top: 34px !important;
        left: 18% !important;
        color: #90949c !important;
    }

    .C-date i {
        font-size: 13px !important;
        position: relative;
        top: -12px !important;
        left: 45% !important;
        color: #90949c !important;
    }

    .Coverdots-right {
        position: absolute;
        right: 0;
        top: 0px;
        padding: 10px;
    }

    .Coverdots-right i {
        color: #8c8c8c;
        font-size: 15px;
        cursor: pointer;
    }

    .cover-user-r img {
        width: 40px !important;
        border-radius: 50% !important;
        display: inline-block !important;
        height: 40px !important;
        left: inherit !important;
        margin-top: 10px;
        margin-left: 15px;
        position: inherit !important;
    }

    .cover-user-r a {
        font-size: 22px;
        margin-left: 10px;
        position: relative;
        top: 3px;
        text-decoration: none;
        color: #385898;
    }

    .coverP-right {
        float: right;
        width: 26.7%;
        overflow: hidden;
    }

    .cover-user-r img {
        width: 40px;
        border-radius: 50%;
        display: inline-block;
    }

    .cover-close-m {
        position: fixed;
        z-index: 999999;
        right: 2%;
        top: 2%;
    }

    .cover-close-m i {
        font-size: 20px;
        color: #777;
    }

    .bottom-right-txt a {
        display: inline-block;
        text-align: right;
        color: #bbb;
        margin: 10px;
        font-size: 15px;
    }

    .bottom-right-txt {
        display: inline-block;
        width: 69%;
        text-align: right;
    }

    .thumbs-up {
        padding: 10px;
        display: inline-block;
    }

    .coverP-bottom {
        position: absolute;
        bottom: 0;
        width: 73.3%;
        background: #131212;
    }

    .thumbs-up a {
        color: #bbb;
        font-size: 15px;
    }

    .thumbs-up i {
        margin-right: 4px !important;

    }

    .CoverPic img {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        height: 100%;
        cursor: pointer;
        width: 73.3%;
    }

    .CoverPage {
        display: none;
    }

    .Cheader-left h4 {
        color: #fff;
        position: absolute;
        z-index: 9999;
        font-size: 26px;
        left: 30px;
        top: 15px;
    }

    .CoverPic {
        position: fixed;
        top: 10%;
        width: 90%;
        background: #fff;
        height: 80%;
        margin: 0px auto;
        display: block;
        z-index: 999999;
        left: 0;
        box-shadow: 0 2px 26px rgba(0, 0, 0, .3), 0 0 0 1px rgba(0, 0, 0, .1);
        left: 5%;
    }

    .CoverPage:after {
        content: "";
        width: 100%;
        height: 100%;
        bottom: 0;
        left: 0;
        position: fixed;
        right: 0;
        top: 0;
        background-color: rgba(0, 0, 0, .9);
        z-index: 99999;
    }

    .Cphotohide {
        display: block;
    }

    /*
    End Add cover image change css
*/



    /*
    Update Cover photo dropdown Css
*/

    .updateCP {
        position: absolute;
        background-color: white !important;
        min-width: 135px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1000 !important;
        border: 1px solid #ddd;
        margin-top: 4px;
        bottom: 75px;
        right: 41px;
        left: inherit;
    }

    .updateCP::before {
        content: "";
        position: absolute;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-bottom: 9px solid #ffffff;
        top: -6px;
        right: 5px;
        z-index: 9999;
    }

    .updateCP::after {
        content: "";
        position: absolute;
        width: 10px;
        height: 10px;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-bottom: 8px solid #ddd;
        top: -10px;
        right: 5px;
    }

    .updateCP li {
        display: block;
        margin: 5px;
    }

    .updateCP li a {
        padding: 3px;
        color: #444;
        text-decoration: none;
        font-size: 15px;
    }

    .updateCP li:hover a {
        color: #fff;
    }

    .updateCP li:hover {
        background: #1ABC9C;
        color: #fff;
        cursor: pointer;
    }

    /*
    End Update Cover photo dropdown Css
*/

    .sectit {
        background-color: #1abc9c;
        text-align: center;

    }

    .ph_vd_head {
        background-color: #1abc9c;
        text-align: center;
        margin-top: 10px;
    }

    .ph_vd_head h3 {
        color: #fff;
        font-family: "Roboto", Sans-serif;
        font-size: 20px;
        padding: 17px 0;
        margin: 0;
        text-transform: capitalize
    }

    p.pro_name_caps {
        text-transform: capitalize;
    }

    .profile_bio h3 {
        text-transform: capitalize;
    }

    .sectit h3 {
        color: #fff;
        font-family: "Roboto", Sans-serif;
        font-size: 20px;
        padding: 17px 0;
        border-bottom: 1px solid #000;
        margin: 0;
        text-transform: capitalize
    }

    .elementor-element.elementor-element-4d74689.elementor-column.elementor-col-50.elementor-top-column {
        width: 20%;
        display: flex;
        position: relative;
        min-height: 1px;
        border-right: 1px solid #000;
    }

    .elementor-element.elementor-element-5e67e44.elementor-column.elementor-col-50.elementor-top-column {
        width: 80%;
        display: flex;
        position: relative;
        min-height: 1px;
    }

    .elementor.elementor-22017 {
        border-bottom: 1px solid #000;
    }

    .elementor-row.elementor-rowsec {
        display: flex;
        width: 100%;
    }

    .elementor-element-populatedsec {
        padding: 0px;
    }

    .elementor-widget-wrap-sec2 {
        padding: 10px;
    }

    .elementor-column-wrapsec {
        padding: 10px;
    }

    .elementor:not(.elementor-bc-flex-widget) .elementor-widget-wrap {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }

    .follow-area-top {
        position: absolute;
        top: 5px;
        background: #cacaca;
        left: 85px;
        border-radius: 15px;
        padding: 5px 13px;

    }

    .follow-area-top a {
        color: #000;

    }


    .follow-area-top:hover {
        background: #1abc9c;
        cursor: pointer;
    }


    .mess-area {
        position: absolute;
        top: 5px;
        background: #cacaca;
        padding: 5px 13px;
        left: 5px;
        border-radius: 15px;
    }

    .mess-area a {
        color: #000;
    }

    .mess-area:hover {
        background: #1abc9c;
        cursor: pointer;
    }

    i.fa-wifi {
        transform: rotate(55deg);
        position: relative;
        right: 3px;
        top: 0px;
    }

    .elementor-text-editor.elementor-clearfix p {
        font-size: 19px;
    }

    .user-location i.fa-map-marker-alt {
        color: #1abc9c;
    }

    .user-location p a {
        text-decoration: none;
        color: #000;
    }

    .timelineprofilepic {
        max-height: 200px;
        position: relative;
    }

    .timelineprofilepic img {
        max-height: 200px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        border: 2px solid #1abc9c;
    }


    i.fas.fa-map-marker-alt {
        font-size: 18px;
        margin-right: 10px;
    }


    .timelinecover {
        position: relative;
    }

    .timelinecover img {
        width: 885px;
        height: 350px;
        border-radius: 5px;
        border: 2px solid #1abc9c;
    }

    .mytime_photo_opened {
        background-color: #333;
        background-color: rgba(51, 51, 51, 0.9);
        cursor: pointer;
        height: 100%;
        left: 0;
        overflow-y: scroll;
        padding: 24px;
        position: fixed;
        text-align: center;
        top: 0;
        width: 100%;
        z-index: 9999;
    }

    .timelinecontentwrap {
        z-index: auto;
    }

    .mytime_photo_opened:before {
        background-color: #333;
        background-color: rgba(51, 51, 51, 0.9);
        color: #eee;
        content: "x";
        font-family: sans-serif;
        padding: 6px 12px;
        position: fixed;
        text-transform: uppercase;
    }

    .mytime_photo_opened img {
        box-shadow: 0 0 6px 3px #333;
        width: 498px;
        height: 486px;
    }

    .no-scroll {
        overflow: hidden;
    }

    .mytime_photo_vd_opened {
        background-color: #333;
        background-color: rgba(51, 51, 51, 0.9);
        cursor: pointer;
        height: 100%;
        left: 0;
        overflow-y: scroll;
        padding: 24px;
        position: fixed;
        text-align: center;
        top: 0;
        width: 100%;
        z-index: 9999;
    }

    .mytime_photo_vd_opened:before {
        background-color: #333;
        background-color: rgba(51, 51, 51, 0.9);
        color: #eee;
        content: "x";
        font-family: sans-serif;
        padding: 6px 12px;
        position: fixed;
        text-transform: uppercase;
    }

    .mytime_photo_vd_opened img {
        box-shadow: 0 0 6px 3px #333;
        width: 498px;
        height: 486px;
    }

    @media screen and (max-width: 800px) {
        .hide_mobile_dsa {
            display: none;
        }
    }

    @media (max-width: 576px) {
        .pv_post_video_overlay {
            top: 83%;
            left: 58.5%;
            width: 86%;
        }

        .pv_post_video_overlay.pv_post_photo_over2 {
            top: 85%;
            left: 57%;
        }
    }

    #footer {
        display: none;
    }
    .modal{
        z-index: 999999;
    }
</style>
<script >
        function record_share_data(id,type){
        var post_id = id;
        var type=type;
        $.ajax({
        type: "POST",
        url: '<?php echo site_url() ?>/submit-ticket/',
        data: {
        feed_id: post_id,
        type:type,
        action: 'social_share_record'
        }, // serializes the form's elements.
        success: function(data) {
        }
        });
        }
        </script>
<script>

function copy_link(id) {
  var copyText = document.getElementById("copytext"+id).href;
  document.addEventListener('copy', function(event) {
    event.clipboardData.setData('text/plain', copyText);
    event.preventDefault();
    document.removeEventListener('copy', handler, true);
  }, true);
  document.execCommand('copy');
  alert("Link Copied!");
}

</script>
<script type="text/javascript">
$(document).ready(function () {

    $('.photo-video-popup').click(function () {
        $('.delete_attachment').attr('postid',$(this).attr('postid'));
        $('#photo-video-popup .img_div').hide();
        $('#photo-video-popup .video_div').hide();
        if($(this).find('img').length>0) {
            $('.delete_attachment span').text('Photo');
            $('#photo-video-popup .img_div').show();
            $('#photo-video-popup .img_div img').attr('src', $(this).find('img').attr('src'));
        }
        else {
            $('.delete_attachment span').text('Video');
            $('#photo-video-popup .video_div').show();
            $('#photo-video-popup .video_div video').attr('src', $(this).attr('href'));
        }
    });
    });
    $('body').on('click', function () {
      $('#video1')[0].pause();
    });
</script>
<?php
get_footer();
?>