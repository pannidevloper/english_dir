<?php
require_once('../../../wp-config.php');

function search_map($kw)
{
    $url='https://search.api.sygic.com/v0/api/autocomplete?query='.urlencode($kw).'&key=VaXzBGJne0Cipjsq5IVPv81cI&countryFilter=idn';
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    $data=curl_exec($ch);
    if (curl_errno($ch)) {
        echo json_encode(array('status'=>false));
    } else {
        echo $data;
    }
    curl_close($ch);
}

if ($_GET['action']=='search_map' && !empty($_GET['kw'])) {
    search_map($_GET['kw']);
} elseif ($_GET['action']=='save_this' && !empty($_GET['tour_id']) && !empty($_GET['userid'])) {
     save_itinerary(intval($_GET['tour_id']), intval($_GET['userid']));
 }
 function save_itinerary($tourid, $userid)
 {
     global $wpdb;
     if (empty($wpdb->get_row("SELECT * FROM `itinerary` WHERE tourid='{$tourid}'"))) {
         echo 0;
     } else {
         if ($wpdb->query("INSERT INTO `finalitinerary` SELECT * FROM `itinerary` WHERE tourid='{$tourid}'")) {
             $wpdb->query("UPDATE `finalitinerary` SET userid='{$userid}' WHERE tourid='{$tourid}'");
             $wpdb->query("DELETE FROM `itinerary` WHERE tourid='{$tourid}' AND userid='{$userid}'");
             echo 1;
         } else {
             echo 0;
         }
     }
 }
