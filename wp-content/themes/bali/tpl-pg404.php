<div id="hco">
    <div id="hci">
        <div id="hc2">
            <div id="hcc">
				<h2><?php _e('Error 404 - Not Found', 'bali'); ?></h2>
                <p><?php _e('Sorry, but you are looking for something that is not here.', 'bali'); ?></p>
                <?php get_search_form(); ?>
            </div><!--/hcc-->
        </div><!--/hc2-->
        <?php get_sidebar(); ?>
        <div class="clr"></div>
    </div><!--/hci-->
</div><!--/hco-->

