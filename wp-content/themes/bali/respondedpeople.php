<?php
/*
Template Name: respondedpeople
*/
get_header(); 
?>
<style>
div,h1,h2,h3,h4,h5,h6,p,a{
	font-family: 'Roboto', sans-serif;
}
.respondedpage_content{
	background: #fff;
	border-bottom: 6px solid #138967;
	padding: 20px 10px;
}
.main_wrapper_responded_page{
    box-shadow: 0 1px 1px 0 rgba(60, 64, 67, .08), 0 1px 3px 1px rgba(60, 64, 67, .16);	
}
	.well_desktop h3{
		margin: 0px;
		font-weight: bold;
	}
	.well_desktop{
		padding: 8px 10px;
		background: #e4e4e4;
		border-bottom: 1px solid #ccc;
		margin-top: 15px;
		font-family: 'Roboto', sans-serif;
	}
	.responded_profile,.responded_personal_detail,.responded_status,.responded_buttons{
		display: inline-block;
		vertical-align: middle;
	}
	.responded_profile img{
		border-radius:100%;
		width: auto;
		height: 60px;
	}
	.responded_profile{
		width:7%;
	}
	.responded_personal_detail h3 {
	    font-size: 14px;
	    color: #385898;
	    font-weight: 600;
	    text-transform: capitalize;
	    font-family: 'Heebo', sans-serif!important;
	}
	.responded_personal_detail p{
		margin: 0px;
		font-size: 12px;
		color: #606770;
	}
	.responded_personal_detail{
		width:40%;
	}
	.responded_status{
		font-size: 14px;
		color: grey;
	}
	.responded_status{
		width:20%;
	}
	.responded_buttons{
		width:20%;
	}
	.requestsent_button {
	    line-height: 17px;
	}
	.icon_d, .text_d {
	    display: table-cell;
	    vertical-align: middle;
	}
	.responded_buttons {
	    width: 30%;
	    text-align: center;
	}
	.icon_d {
	    color: #666;
	}
	.requestsent_button a, .connected_button a {
	    color: #666;
	    font-weight: 600;
	    text-decoration: none;
	}
	.requestsent_button, .connected_button {
	    width: 150px;
	    min-height: 30px;
	    background: #fff;
	    line-height: 16px;
	    font-family: 'Heebo', sans-serif;
	    font-size: 15px;
	    border: 2px solid #666;
	    border-radius: 0px;
	}
	.respondedpage_list{
		margin-top:10px;
	}
	.connect_button a {
	    color: #fff;
	    font-weight: 600;
	    text-decoration: none;
	}
	.connect_button {
	    width: 150px;
	    min-height: 30px;
	    background: #22b14c;
	    line-height: 16px;
	    font-family: 'Heebo', sans-serif;
	    font-size: 15px;
	    border: 2px solid #22b14c;
	    border-radius: 0px;
	}
	
	@media (max-width: 240px){
		.responded_personal_detail {
		    width: 57%;
		    padding-left: 10px;
		}
		.responded_personal_detail h3,.responded_personal_detail p{
			font-size: 10px;
			margin: 0px;
		}
		.requestsent_button, .connected_button,.connect_button{
			width:100%;
			line-height: 11px;
		}
		.requestsent_button a, .connected_button a,.connect_button a{
			font-size:11px;
		}
		.responded_buttons {
		    width: 100%;
		    margin-top: 5px;
		    text-align: center;
		}
		.responded_status {
		    width: 30%;
		    font-size: 11px;
		}
		.responded_profile {
		    width: 10%;
		}
		.icon_d, .text_d {
		    padding-left: 10px;
		}
		.responded_profile img {
		    height: 30px;
		}
	}
	
	@media (min-width: 280px) and (max-width: 640px) {
		.responded_personal_detail h3,.responded_personal_detail p{
			font-size: 10px;
			margin: 0px;
		}
		.responded_profile img {
		    height: 30px;
		}
		.responded_personal_detail{
			padding-left:10px;
		}
		.responded_status{
			font-size:12px;
		}
		.well_desktop{
			margin-top: 35px;
		}
		.requestsent_button, .connected_button,.connect_button{
			width:100%;
			line-height: 11px;
		}
		.requestsent_button a, .connected_button a,.connect_button a{
			font-size:11px;
		}
	}
	
	@media (min-width: 760px) and (max-width: 1024px) {
		.responded_personal_detail h3,.responded_personal_detail p{
			font-size: 10px;
			margin: 0px;
		}
		.responded_profile img {
		    height: 50px;
		}
		.responded_personal_detail{
			padding-left:15px;
		} 
		.responded_status{
			font-size:12px;
		}
		.well_desktop{
			margin-top: 35px;
		}
		.requestsent_button, .connected_button,.connect_button{
			width:100%;
			line-height: 13px;
		}
		.requestsent_button a, .connected_button a,.connect_button a{
			font-size:13px;
		}
	}
	
</style>

<div class="main_wrapper_responded_page">
	<div class="respondedpage_header">
		<div class="well_desktop">
			<h3>Responded People</h3>
		</div>
	</div>
	
	<div class="respondedpage_content">
		<div class="respondedpage_list">
			
			<div class="responded_list_row">
				<div class="responded_profile">
					<img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/1143/profile_photo-80x80.jpg?1578581947"/>
				</div>
				<div class="responded_personal_detail">
					<h3>First_Last Name</h3>
					<p>Personal Detail</p>
				</div>
				<div class="responded_status">
					<span>
						<i class="fas fa-times"></i> Not Going
					</span>
				</div>
				
				<div class="responded_buttons">
					<div class="inner_button">
						<button class="requestsent_button">
							<div class="icon_d"> 
								<i class="fas fa-link"></i> 
							</div>  
							<div class="text_d">
								<a href="#">  Connection Request sent</a>
							</div> 
						</button>
					</div>
				</div>
				
			</div>
			
		</div>
		
		
		
		<div class="respondedpage_list">
			
			<div class="responded_list_row">
				<div class="responded_profile">
					<img src="https://www.travpart.com/English/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg"/>
				</div>
				<div class="responded_personal_detail">
					<h3>First_Last Name</h3>
					<p>Personal Detail</p>
				</div>
				<div class="responded_status">
					<span>
						<i class="fas fa-check"></i> Going
					</span>
				</div>
				
				<div class="responded_buttons">
					<div class="inner_button">
						<button class="connect_button">
							<a href="#">
								<i class="fas fa-link"></i> 
								Connect
							</a>
						</button> 
					</div>
				</div>
				
			</div>
			
		</div>
		
		
		<div class="respondedpage_list">
			
			<div class="responded_list_row">
				<div class="responded_profile">
					<img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/1145/profile_photo-80x80.jpg?1578581947"/>
				</div>
				<div class="responded_personal_detail">
					<h3>First_Last Name</h3>
					<p>Personal Detail</p>
				</div>
				<div class="responded_status">
					<span>
						<i class="fas fa-check"></i> Going
					</span>
				</div>
				
				<div class="responded_buttons">
					<div class="inner_button">
						<button class="connected_button">
							<a href="#">
								<i class="fas fa-link"></i> 
								Connected
							</a>
						</button>
					</div>
				</div>
				
			</div>
			
		</div>
		
	</div>
	
</div>


<?php
get_footer();
?>
