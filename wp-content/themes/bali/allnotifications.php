<?php
/*
Template Name: see all notifications
*/
get_header();

?>

<style>
.padd_ta h4{
	margin: 0px;
	color: #333333bf;
	letter-spacing: 1px;
}
.padd_ta{
	background: #fff;
	padding: 10px;
	box-shadow: 0 1px 1px 0 rgba(60, 64, 67, .08), 0 1px 3px 1px rgba(60, 64, 67, .16);
	margin: 20px 0px;
}
.data_dot i{
	color: #8889d9;
}
.data_dot{
	position: absolute;
	left:-112px;
	font-size: 11px;
	    top: 50%;
    margin-top: -8px;
}
.data_dot span{
	color: #847b7b;
}
.data_dot i{
	margin-left: 10px;
}
.list_div{
	position: relative;
	background: #fff;
	border-radius: 3px;
	padding: 10px;
	margin: 10px 0px;
	box-shadow: 0 1px 1px 0 rgba(60, 64, 67, .08), 0 1px 3px 1px rgba(60, 64, 67, .16);	
}
.content_d {
    display: inline-block;
    vertical-align: middle;
    width: 91%;
    padding: 10px;
    font-size: 12px;
    font-weight: bold;
    color: #635b5b;
    letter-spacing: 1px;
}
.media_img img{
	width: auto;
	height: 50px;
	border-radius: 50%;
}
.media_img{
	display:inline-block;
	vertical-align:middle;
	width:8%;
	text-align: center;
}
.content_d p{
	margin: 0px;
}
.imgs img {
    width: auto;
    height: 100px;
    margin: 3px;
    border-radius: 5px;
}
.follower_div.follower_div1 {
    padding: 10px 14px;
    display: inline-block;
    background: #ec5757;
    border-radius: 50%;
    vertical-align: middle;
    cursor: pointer;
    color: #fff;
}
.follower_div.follower_div2 {
    padding: 10px 14px;
    vertical-align: middle;
    display: inline-block;
    background: #22b14c;
    cursor: pointer;
    border-radius: 50%;
    color: #fff;
}
.follower_div.follower_div3 {
	
	cursor: pointer;
    padding: 10px 14px;
    vertical-align: middle;
    display: inline-block;
    background: #57c3ec;
    border-radius: 50%;
    color: #fff;
}
.followers_d,.imgs{
	margin: 5px 0px;
}
.high_20{
	width:10%;
	display: inline-block;
	vertical-align: middle;
}
.high_80{
	vertical-align: middle;
	width:89%;
	display: inline-block;
}
.for_cc{
	color: #bbb7b7;
} 
.media_img i{
	color:#22b14c;
}
.nm_v{
	color:#000;
	text-decoration: none !important;
	font-weight: bold;
}
.follower_div:hover{
	transition:1s ease;
	background:#0ca98d;
}
.list_div_wrapper{
	padding-bottom: 20px;
}
body{
	background: #f2f7fa;
}
.data_dot:after{
	content: '';
	height: 100%;
	border-left:1px solid #ccc;
	position: absolute;
	margin-top:13px;
	right: 5px;
}
.data_dot{
	height:100% !important;
	top:0% !important;
	margin-top: 2px;
}
<?php

if (!is_user_logged_in()) {
    exit(wp_redirect(home_url('login')));
}

$current_user = wp_get_current_user();
$wp_user_ID = $current_user->ID;

global $wpdb;
 $notifications_count = $wpdb->get_var("SELECT COUNT(*) FROM notification WHERE wp_user_id='{$wp_user_ID}' AND read_status=0");

?>
</style>
<div class="row">
	<div class="col-md-12">
	 	<div class="padd_ta">
	 		<div class="text-center">
	 			<div class="row">
	 				<div class="col-md-11 text-center">
	 					<h4><strong>Notifications ( <?php echo $notifications_count  ?> )</strong></h4>
	 				</div>
	 				<div class="col-md-1 text-right">
	 					<a href="#">
	 						<strong>
	 							Mark all read
	 						</strong>
	 					</a>
	 				</div>
	 			</div>
	 			
	 		</div>
	 	</div>
	 	
	 	<div class="list_div_wrapper">
	 		
	 		<div class="high_20">
	 			
	 		</div>
	 		<div class="high_80">

	 		<?php

	 $notificationslist = $wpdb->get_results("SELECT * FROM notification WHERE wp_user_id='{$wp_user_ID}' AND read_status=0 ORDER BY id DESC");
	 	 foreach ($notificationslist as $single) {

	 	 	// processing on data
	 		$string = $single->content;
  
            
         
         preg_match('~<a .*?href=[\'"]+(.*?)[\'"]+.*?>(.*?)</a>~ims', $string, $result);

         $noti_text = $result[2];
           //get time and date from time in db

           $timestamp = $single->create_time;
           $date_and_time =  explode(" ", $timestamp);

	 		?>

	 			<div class="list_div">
	 					<div class="data_dot">
		 					<span> <?php echo $date_and_time[0] ?>	</span> <i class="fas fa-dot-circle"></i>
		 				</div>
		 			<div class="media_div">
		 				<!--
		 				<div class="media_img">
		 				 <i class="fas fa-dot-circle"></i>	<img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/1296/profile_photo-80.jpg" />
		 				</div>
		 				
		 				-->
		 				<div class="content_d">
		 					<p>
		 						<?php
		 							echo substr($m[2], 0, -2);
		 						?>
		 						<span class="pull-left"><a href="<?php  echo $result[1] ?>" class="nm_v"><?php echo !empty($noti_text) ? $noti_text : $string; ?>   </a></span>
		 						<span class="pull-right for_cc"><?php echo $date_and_time[1] ?></span> 
		 						<div class="clearfix"></div>
		 					</p>
		 					<!--
		 					<p> 
		 						"Can't  wait to go on this adventure with you".
		 					</p>
		 				-->
		 				</div>
		 			</div>
		 		</div>
		 	<?php } ?>
		 		<!--
		 		
		 		<div class="list_div">
		 				<div class="data_dot">
		 					<span> May, 21-2015	</span> <i class="fas fa-dot-circle"></i>
		 				</div>
		 			<div class="media_div">
		 				
		 				<div class="media_img">
		 					<i class="fas fa-dot-circle"></i> <img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/1296/profile_photo-80.jpg" />
		 				</div>
		 				
		 				<div class="content_d">
		 					<p>
		 						<span class="pull-left"><a href="#" class="nm_v">  Farhan </a> Uploaded 4 new photos <a href="#"> Event or Post name</a>. </span>
		 						<span class="pull-right for_cc">10:00 am</span>
		 						<div class="clearfix"></div> 
		 					</p>
		 					<div class="imgs">
		 						<img src="https://www.travpart.com/English/wp-content/uploads/2019/12/globe.jpg" />
		 						<img src="https://www.travpart.com/English/wp-content/uploads/2019/12/globe.jpg" />
		 						<img src="https://www.travpart.com/English/wp-content/uploads/2019/12/globe.jpg" />
		 						<img src="https://www.travpart.com/English/wp-content/uploads/2019/12/globe.jpg" />
		 					</div>
		 				</div>
		 			</div>
		 		</div>
		 		
		 		
		 		<div class="list_div">
		 				<div class="data_dot">
		 					<span> May, 21-2015	</span> <i class="fas fa-dot-circle"></i>
		 				</div>
		 			<div class="media_div">
		 				<div class="media_img">
		 					<img src="https://www.travpart.com/English/wp-content/uploads/ultimatemember/1219/profile_photo-80x80.jpg" />
		 				</div>
		 				
		 				<div class="content_d">
		 					<p>
		 						<span class="pull-left"><a href="#" class="nm_v">  Samad </a> Added 3 new followers.</span>
		 						<span class="pull-right for_cc">05:01 am</span> 
		 						<div class="clearfix"></div>
		 					</p>
		 					<p> 
		 						<div class="followers_d">
		 							<div class="follower_div follower_div1">
		 								D
		 							</div>
		 							<div class="follower_div follower_div2">
		 								U
		 							</div>
		 							<div class="follower_div follower_div3">
		 								P
		 							</div>
		 						</div>
		 					</p>
		 				</div>
		 			</div>
		 		-->
		 		</div>
		 		
		 		
		 		<div class="text-center">
		 			<button class="btn btn-default" style="padding: 5px 20px;box-shadow: 0 1px 1px 0 rgba(60, 64, 67, .08), 0 1px 3px 1px rgba(60, 64, 67, .16);font-size: 14px;margin: 10px;">Load More Notifications</button>
		 		</div>
		 		
	 		</div>
	 		
	 	</div>
	 	
		
	</div>
</div>
<style type="text/css">
    #footer {
        display: none;
    }
</style>

<?php
get_footer();
?>
