<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, height=device-height, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="keywords" content="Bali travel, Bali parent-child travel, Bali group travel, Bali group quotation, tujing Bali travel">
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
        <title><?php
            if (is_home() || is_front_page()) {
                bloginfo('name');
            } else {
                wp_title('');
                echo ' | ';
                bloginfo('name');
            }
            ?></title>
        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
        <link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>
        <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css">
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.js" type="text/javascript"></script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQ4N2X6');</script>
<!-- End Google Tag Manager -->



        <?php wp_head(); ?>
        <style>
        	.elementor-widget-text-editor,.elementor-19120 .elementor-element.elementor-element-a75a75e .elementor-accordion .elementor-tab-title,div,p,a,button{
				font-size: 14px;
			}
			div#logo img {
			    width: auto;
			    height: 65px;
			}
        </style>
        
        
        <style>
        @media (max-width: 640px) {
        	 #mega-menu-wrap-main-menu{
			  	padding-top	:0px !important 
			  }
			  div#logo img{
				    height: 40px !important;
			  }
        }
        </style>
        
		<?php if(is_user_logged_in()){ ?>
			<style>
					.input-group.md-form.form-sm.form-1.pl-0{
						left: 0px !important;
					}
					.English-logo img{
						height: 49px;
						margin-top: 6px;
					}
			</style>	
		<?php } ?>
    </head>
    <body id="<?php
    if (is_home() || is_front_page()) {
        echo 'hp';
    }
    ?>" <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ4N2X6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <input type="hidden" class="site_url" value="<?php echo get_site_url() ?>" />
        <div id="header" style="background: white">
            <div class="containe/r-fluid" style="background:white;padding-top:10px;">
                <div class="container">
                    <div class="row">
                    	<div class="col-md-3 mobileshow">
                            <div id="logo" style="top:0"><a href="<?php bloginfo('url'); ?>/"><img src="https://www.travpart.com/English/wp-content/uploads/2019/12/travpart-main.png" alt="å?°å°¼æµ·å²›æ—…æ¸¸" width="200" style="margin-top:20px"></a></div>
                        </div>
                        <div id="h-t" class="col-md-9" style="background:white;width:100%;background-size:100%;">
                            <div class="main-menu pull-right" style="margin: 7px;margin-top: 8px;">
                                <?php
                                ob_start();
                                wp_nav_menu(array('theme_location' => 'main-menu'));
                                $main_menu = ob_get_contents();
                                ob_end_clean();
                                ?>
                                <?php ob_start(); ?>
                                 <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/about-us/">ABOUT</a></li>
                                <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/travchat">MESSAGE</a></li>
                                <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="https://www.travpart.com/English/contact/">CONTACT</a></li>
                                <div class="dropdown">
                                    <div class="dropbtn"> <!--  Login start --> 
                                        <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                            <?php if (is_user_logged_in()) { ?>
                                                <a style="background: #226d82; text-align: center!important; padding: 18px; font-size:15px;" class="mega-menu-link loginstyle" href="<?php echo wp_logout_url(get_permalink()); ?>" class="buttype">LOGOUT <i class="fa fa-caret-down" style="display: inline;"></i></a>
                                            <?php } else { ?>
                                                <a  class="mega-menu-link loginstyle" href="/English/login/" class="buttype">LOGIN <i class="fa fa-caret-down" style="display: inline;"></i></a>
                                                <style>
                                                    .dropdown-content{display:none!important;}
                                                </style>
                                            <?php } ?>
                                        </li>   
                                        <!--  Login end --></div>
                                    <div class="dropdown-content">
                                        <!--  User name start -->
                                        <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                            <?php
                                            $display_name = um_user('display_name');
                                            $current_user = wp_get_current_user();
                                            if (is_user_logged_in()) {
                                                ?>
                                                <strong><a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=pages"" >
                                                        <?php echo $display_name; ?></a></strong><br>
                                                <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=profile"><i class="fas fa-user-circle"></i> MY PROFILE
                                                </a><br>
                                                <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=referal"><i class="fas fa-undo-alt"></i> MY REFERRAL
                                                </a><br>
                                                <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=settlement"><i class="fas fa-calendar-check"></i> SETTLEMENT
                                                </a><br>
                                                <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=transaction"><i class="fas fa-dollar-sign"></i> TRANSACTION
                                                </a><br>
                                                <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=verification"><i class="fas fa-calendar-check"></i> VERIFICATION
                                                </a><br>
                                                <a class="mega-menu-link" href="https://www.travpart.com/English/user/<?php echo $current_user->user_login;?>/?profiletab=pages2"><i class="fas fa-book-open"></i> BOOKING CODES
                                                </a>

                                            <?php } else { ?>
                                            <?php } ?>
                                        </li>
                                        <!--  User name end -->  
                                    </div>
                                </div>

                                <li  class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link" href="https://www.travpart.com/Chinese"><img src="<?php bloginfo('template_url'); ?>/images/flg-cn.png" alt="中文" class="mobilenotshow"> 中文</a></li>
                                <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout">
                                    <select class="header_curreny search_select" style="padding:0px;height:20px;background-image:none;">

                                        <option full_name="IDR" syml="Rp"  value="IDR"> IDR </option>
                                        <option full_name="RMB" syml="元"  value="CNY"> RMB </option>
                                        <option full_name="USD" syml="USD" value="USD"> USD </option>
                                        <option full_name="AUD" syml="AUD" value="AUD"> AUD </option>
                                    </select>
                                </li>



                                <!--<li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a  class="mega-menu-link" href="http://www.travpart.com/tutorial/Partner_s_Guidance.pdf" target="_blank"><img src="http://www.travpart.com/tutorial/tutorial-downloadpdf.png" alt="Download Tutorial" style="width:107px;"></a></li>-->
                                <li class="mobilenone mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout"><a class="mega-menu-link headerfont" href="http://www.travpart.com/tutorial/Partner_s_Guidance.pdf" target="_blank">HOW IT WORKS</a></li>
                                <?php
                                global $user_login, $current_user;
                                get_currentuserinfo();
                                $user_info = get_userdata($current_user->ID);
                                $roles = array(
                                    'administrator',
                                    'um_travel-advisor',
                                );

                                if (is_user_logged_in() && array_intersect($roles, $user_info->roles)) {

                                    echo '<li class="mega-menu-link changecolor"><a href="https://www.travpart.com/English/travcust/"><span style="padding:5px">Create a tour package</span></a></li>';
                                } else {

                                    echo '<li class="mega-menu-link changecolor"><a href="https://www.travpart.com/English/contact/"><span style="padding:5px">Contact us</span></a></li>';
                                }
                                ?>
                                <?php
                                $append_menu_items = ob_get_contents();
                                ob_end_clean();

                                $main_menu = substr(trim($main_menu), 0, strlen($main_menu) - 11);
                                echo $main_menu . $append_menu_items . '</ul></div></div>';
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
           <!--<div class="container-fluid" style="background:white;padding-top:7px">
                <div class="container" style="background:white;height:80px">
                    <div class="row">
                        <div class="col-md-3 mobileshow">
                            <div id="logo" style="top:0"><a href="<?php bloginfo('url'); ?>/"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/2019-logo.png" alt="å?°å°¼æµ·å²›æ—…æ¸¸" width="200" style="margin-top:20px"></a></div>
                        </div>
                        <div id="h-r" class="col-md-9">
                            <div style="float:right;margin-top:14px"><a href="https://www.travpart.com/English/travchat" target="_blank"><img src="https://www.travpart.com/English/wp-content/uploads/2018/11/travchat.jpg" alt="Travchat" width="200"></a></div>
                             <div id="menu">
                                    <div class="navbar navbar-default" role="navigation">                        
                            <?php wp_nav_menu(array('theme_location' => 'main-menu')); ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>-->
        </div>

        <?php
        /* if( is_home() || is_front_page() ) {
          //    if ( current_user_can('administrator') ) {
          echo '<div id="search_price_box" class="container">';
          echo '<div id="search_price_box_form" class="animated flash">';
          echo search_price_box();
          echo '</div>';
          // echo do_shortcode("[bmap id='homemap' w='100%' h='450px' lon='115.150' lat='-8.426' address='è¿™é‡Œè¾“å…¥åœ°å?€']");
          echo '</div>';
          //    }
          } */
        ?><?php
        /* if(is_home() || is_front_page()) {
          putRevSlider( 'homepage' );
          } */
        ?>

        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <?php if (!is_home() && !is_front_page()) { ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php getBcrumbs(); ?>
                                </div>
                            </div>
                        <?php } ?>