<?php
/* Template Name: Customer Request Details */
if(!is_user_logged_in() || !(current_user_can('administrator')||current_user_can('um_travel-advisor')) ) {
	wp_redirect(site_url('/login'));
	exit;
}
if(empty($_GET['csa_id']) || intval($_GET['csa_id'])<1 ) {
	wp_redirect(site_url('/customers-request/'));
	exit;
}
$csa_id=intval($_GET['csa_id']);
$request_details=$wpdb->get_row("SELECT contact_sales_agent.*,user.userid FROM `contact_sales_agent`,`user` WHERE `csa_user_name`=`user`.`username` AND csa_id={$csa_id} AND DATE_SUB(CURDATE(), INTERVAL 1 MONTH)<=DATE(csa_created)");
if(empty($request_details->csa_user_name)) {
	wp_redirect(site_url('/customers-request/'));
	exit;
}
$next_request_id=$wpdb->get_var("SELECT csa_id FROM `contact_sales_agent`,`user` WHERE `csa_user_name`=`user`.`username` AND csa_id>{$csa_id} AND DATE_SUB(CURDATE(), INTERVAL 1 MONTH)<=DATE(csa_created) ORDER BY csa_id ASC");
get_header();
if(have_posts()) {
	while(have_posts()) {
		the_post();
		echo '<h1>'.get_the_title().'</h1>';
		$content=get_the_content();
		$content=apply_filters('the_content', $content);
		$content.='
		<div data-elementor-type="post" data-elementor-id="19341" class="elementor elementor-19341 elementor-bc-flex-widget"
    data-elementor-settings="[]">
    <div class="elementor-inner">
        <div class="elementor-section-wrap">
            <section
                class="elementor-element elementor-element-1cbba0d elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                data-id="1cbba0d" data-element_type="section"
                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div class="elementor-element elementor-element-c0cb453 elementor-column elementor-col-100 elementor-top-column"
                            data-id="c0cb453" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-5965b80 elementor-widget elementor-widget-heading"
                                        data-id="5965b80" data-element_type="widget" data-widget_type="heading.default">
                                        <div class="elementor-widget-container">
                                            <p class="elementor-heading-title elementor-size-default">
                                                '.$request_details->csa_user_name.' Request</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section
                class="elementor-element elementor-element-efc7fb8 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                data-id="efc7fb8" data-element_type="section"
                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div class="elementor-element elementor-element-6fb16f7 elementor-column elementor-col-100 elementor-top-column"
                            data-id="6fb16f7" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-0df10f1 elementor-widget elementor-widget-heading"
                                        data-id="0df10f1" data-element_type="widget" data-widget_type="heading.default">
                                        <div class="elementor-widget-container">
                                            <p class="elementor-heading-title elementor-size-default">
                                                '.$request_details->csa_subject.' ​</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section
                class="elementor-element elementor-element-8a95c0c elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section animated fadeInUp"
                data-id="8a95c0c" data-element_type="section"
                data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div class="elementor-element elementor-element-0ed2a72 requestlink elementor-column elementor-col-50 elementor-top-column"
                            data-id="0ed2a72" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <!--div class="elementor-element elementor-element-20de2b4 elementor-view-stacked elementor-position-left elementor-vertical-align-middle elementor-shape-circle elementor-widget elementor-widget-icon-box"
                                        data-id="20de2b4" data-element_type="widget"
                                        data-widget_type="icon-box.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-icon-box-wrapper">
                                                <div class="elementor-icon-box-icon">
                                                    <span class="elementor-icon elementor-animation-">
                                                        <i class="fa fa-user" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                                <div class="elementor-icon-box-content">
                                                    <h3 class="elementor-icon-box-title">
                                                        <span>Adam ★★★★★</span>
                                                    </h3>
                                                    <p class="elementor-icon-box-description">Deal Expert | <a
                                                            href="mailto:customer@gmail.com">customer@gmail.com</a> </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div-->
                                    <div class="elementor-element elementor-element-fbd3100 elementor-widget elementor-widget-text-editor"
                                        data-id="fbd3100" data-element_type="widget"
                                        data-widget_type="text-editor.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-text-editor elementor-clearfix">
                                                <p>'.$request_details->csa_message.'</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="elementor-element elementor-element-d5e35c0 elementor-align-center elementor-widget elementor-widget-button"
                                        data-id="d5e35c0" data-element_type="widget" data-widget_type="button.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-button-wrapper">
                                                <a href="'.site_url().'/travchat/user/addnewmember.php?user='.$request_details->userid.'"
                                                    class="elementor-button-link elementor-button elementor-size-md"
                                                    target="_blank" role="button">
                                                    <span class="elementor-button-content-wrapper">
                                                        <span class="elementor-button-icon elementor-align-icon-left">
                                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                                        </span>
                                                        <span class="elementor-button-text">Message</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-ff8047a requestlink elementor-column elementor-col-50 elementor-top-column"
                            data-id="ff8047a" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-649d877 elementor-view-stacked elementor-position-left elementor-vertical-align-middle elementor-shape-circle elementor-widget elementor-widget-icon-box"
                                        data-id="649d877" data-element_type="widget"
                                        data-widget_type="icon-box.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-icon-box-wrapper">
                                                <div class="elementor-icon-box-icon">
                                                    <span class="elementor-icon elementor-animation-">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                                <div class="elementor-icon-box-content">
                                                    <h3 class="elementor-icon-box-title">
                                                        <span>Request Date</span>
                                                    </h3>
                                                    <p class="elementor-icon-box-description">
													'.$request_details->csa_created.'
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="elementor-element elementor-element-1c09507 elementor-view-stacked elementor-position-left elementor-vertical-align-middle elementor-shape-circle elementor-widget elementor-widget-icon-box"
                                        data-id="1c09507" data-element_type="widget"
                                        data-widget_type="icon-box.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-icon-box-wrapper">
                                                <div class="elementor-icon-box-icon">
                                                    <span class="elementor-icon elementor-animation-">
                                                        <i class="fa fa-plane" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                                <div class="elementor-icon-box-content">
                                                    <h3 class="elementor-icon-box-title">
                                                        <span>Destination Plan</span>
                                                    </h3>
                                                    <p class="elementor-icon-box-description">'.$request_details->csa_destination_plan.'</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="elementor-element elementor-element-161b6f7 elementor-view-stacked elementor-position-left elementor-vertical-align-middle elementor-shape-circle elementor-widget elementor-widget-icon-box"
                                        data-id="161b6f7" data-element_type="widget"
                                        data-widget_type="icon-box.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-icon-box-wrapper">
                                                <div class="elementor-icon-box-icon">
                                                    <a class="elementor-icon elementor-animation-"
                                                        href="https://www.travpart.com/English/travcust">
                                                        <i class="fa fa-bed" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="elementor-icon-box-content">
                                                    <h3 class="elementor-icon-box-title">
                                                        <a href="'.site_url().'/travcust">Travcust</a>
                                                    </h3>
                                                    <p class="elementor-icon-box-description"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section
                class="elementor-element elementor-element-2590049 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                data-id="2590049" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div class="elementor-element elementor-element-97392ed elementor-column elementor-col-50 elementor-top-column"
                            data-id="97392ed" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-3026db1 elementor-position-left elementor-vertical-align-bottom elementor-view-default elementor-widget elementor-widget-icon-box"
                                        data-id="3026db1" data-element_type="widget"
                                        data-widget_type="icon-box.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-icon-box-wrapper">
                                                <div class="elementor-icon-box-icon">
                                                    <a class="elementor-icon elementor-animation-"
                                                        href="'.site_url().'/customers-request/">
                                                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="elementor-icon-box-content">
                                                    <h3 class="elementor-icon-box-title">
                                                        <a href="'.site_url().'/customers-request/">Back
                                                            to Customer\'s Request</a>
                                                    </h3>
                                                    <p class="elementor-icon-box-description"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
						if(!empty($next_request_id)) {
							$content.='
                        <div class="elementor-element elementor-element-f94def3 elementor-column elementor-col-50 elementor-top-column"
                            data-id="f94def3" data-element_type="column">
                            <div class="elementor-column-wrap  elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-976dcc9 elementor-position-right elementor-vertical-align-bottom elementor-view-default elementor-widget elementor-widget-icon-box"
                                        data-id="976dcc9" data-element_type="widget"
                                        data-widget_type="icon-box.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-icon-box-wrapper">
                                                <div class="elementor-icon-box-icon">
                                                    <a class="elementor-icon elementor-animation-"
                                                        href="'.site_url().'/customers-request-details/?csa_id='.$next_request_id.'">
                                                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="elementor-icon-box-content">
                                                    <h3 class="elementor-icon-box-title">
                                                        <a
                                                            href="'.site_url().'/customers-request-details/?csa_id='.$next_request_id.'">Next
                                                            Request</a>
                                                    </h3>
                                                    <p class="elementor-icon-box-description"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
						}
         $content.='</div>
                </div>
            </section>
        </div>
    </div>
</div>';
		$content = str_replace( ']]>', ']]&gt;', $content );
		echo $content;
	}
} else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>