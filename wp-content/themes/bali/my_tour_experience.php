<?php
/*
Template Name: My Tour Experience
*/

if (!is_user_logged_in()) {
    exit(wp_redirect(home_url('login')));
}

if (!empty($_GET['user']) && ($user_id = get_user_by('login', htmlspecialchars($_GET['user']))->ID)) { } else if (empty($user_id)) {
    $user_id = wp_get_current_user()->ID;
}

global $wpdb;
get_header();
?>

<style type="text/css">
    .header_txt h2 {
        text-align: center;
        margin-bottom: 50px;
    }

    .dest_img {
        width: 100%;
    }

    .dest_tour_title {
        margin-left: 15px;
        margin-right: 15px;
    }

    .dest_tourcity {
        font-size: 23px;
    }

    .singlepage-label span {
        display: block;
        font-size: 12px;
        font-weight: bold;
        color: #333333;
    }

    .singleinput0 {
        text-align: center;
        margin-top: 5px;
    }

    .dest_share span {
        font-size: 16px;
        font-weight: normal;
        color: #999999;
        text-align: center;
    }

    .dest_views i {
        display: block;
        font-size: 36px;
        color: #c0c3ca;
        text-align: center;
    }

    .dest_views span {
        display: block;
        font-size: 12px;
        font-weight: bold;
        color: #333333;
        margin: 0 !important;
        text-align: center;
    }

    .dest_views span span {
        font-size: 16px;
        font-weight: normal;
        color: #999999;
        text-align: center;
    }

    .dest_follow i {
        display: block;
        font-size: 36px;
        color: #c0c3ca;
        text-align: center;
    }

    .dest_follow span {
        display: block;
        font-size: 12px;
        font-weight: bold;
        color: #333333;
        margin: 0 !important;
        text-align: center;
    }

    .dest_follow span span {
        font-size: 16px;
        font-weight: normal;
        color: #999999;
        text-align: center;
    }

    .dest_comments i {
        display: block;
        font-size: 36px;
        color: #c0c3ca;
        text-align: center;
    }

    .dest_comments span {
        display: block;
        font-size: 12px;
        font-weight: bold;
        color: #333333;
        margin: 0 !important;
        text-align: center;
    }

    .dest_comments span span {
        font-size: 16px;
        font-weight: normal;
        color: #999999;
        text-align: center;
    }

    .heading {
        font-size: 25px;
        margin-right: 25px;
    }

    .r-star .fa {
        font-size: 35px;
        color: grey;
        margin-left: 2px;
    }

    .r-star .checked {
        color: orange;
    }

    .r-star img {
        width: 100%;
        height: 70px;
    }

    .centered {
        position: absolute;
        top: 70%;
        left: 50%;
        color: white;
        font-size: 27px;
        transform: translate(-50%, -50%);
    }

    @media (max-width: 600px) {
        .dest_svfc {
            display: flex;
        }

        .header_txt h2 {
            margin-top: 100px;
        }

        .r-star .fa {
            left: 20%;
            position: relative;
        }
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="header_txt">
            <h2>MY TOUR EXPERIENCE</h2>
        </div>
    </div>
</div>

<?php
$tourList = $wpdb->get_results("SELECT wp_tour.id,wp_posts.ID as post_id,wp_posts.post_title,wp_posts.post_author
                    FROM wp_tour,wp_posts,wp_postmeta
                    WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id'
                    AND wp_postmeta.meta_value=wp_tour.id
                    AND wp_tour.confirm_payment=1 AND wp_tour.buyer={$user_id}");
foreach ($tourList as $tour) {
    $hotelinfo = $wpdb->get_row("SELECT img,location FROM wp_hotel WHERE tour_id='{$tour->id}' LIMIT 1");
    if (!empty($hotelinfo->img)) {
        if (is_array(unserialize($hotelinfo->img))) {
            $hotelimg = unserialize($hotelinfo->img)[0];
        } else
            $hotelimg = $hotelinfo->img;
    }
    $shareCount = $wpdb->get_var("SELECT `meta_value` FROM `wp_tourmeta` WHERE `tour_id`='{$tour->id}' AND `meta_key`='sharecount'");
    $ratting = $wpdb->get_var("SELECT af_ratting FROM agent_feedback WHERE af_tour_id='{$tour->id}'");
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <a href="">
                    <img class="dest_img" src="<?php echo $hotelimg; ?>" alt="travpart.com">
                </a>
            </div>
            <div class="col-md-6">
                <div class="dest_tour_title">
                    <h1><a target="_blank" href="<?php echo get_permalink($tour->post_id); ?>"><?php echo $tour->post_title; ?></a></h1>
                    <p class="dest_tourcity"><b>Location: </b><?php echo $hotelinfo->location; ?></p>
                </div>
                <div class="col-md-8 dest_svfc">
                    <div class="col-md-3">
                        <div class="singleinput0">
                            <label class="singlepage-label" for="s1">
                                <a target="_blank" class="a2a_dd addtoany_share_save addtoany_share" href="">
                                    <picture>
                                        <source type="image/webp" srcset="https://www.travpart.com/English/wp-content/uploads/2019/10/share2.png.webp">
                                        <picture>
                                            <source type="image/webp" srcset="https://www.travpart.com/English/wp-content/uploads/2019/10/share2.png.webp">
                                            <img src="https://www.travpart.com/English/wp-content/uploads/2019/10/share2.png" alt="Share">
                                        </picture>
                                    </picture>
                                </a>
                                <span class="dest_share radio" style="margin-top: 0px;"><span><?php echo intval($shareCount); ?></span> Share</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="dest_views">
                            <i class="fa fa-eye"></i>
                            <span><span><?php echo getPostViews($tour->post_id); ?></span>views</span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="dest_follow">
                            <i class="fa fa-user-plus"></i>
                            <span><span><?php echo $wpdb->get_var("SELECT COUNT(*) FROM `social_follow` WHERE `sf_agent_id` = '{$tour->post_author}'"); ?></span>Followers</span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="dest_comments">
                            <i class="fa fa-comment"></i>
                            <span><span><?php echo $wpdb->get_var("SELECT COUNT(*) FROM `social_comments` WHERE scm_tour_id='{$tour->id}'"); ?></span>Comments</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 r-star">
                <?php for ($i = 0; $i < $ratting; $i++) { ?>
                    <span class="fa fa-star checked"></span>
                <?php } ?>
                <?php for ($i = $ratting; $i < 5; $i++) { ?>
                    <span class="fa fa-star"></span>
                <?php } ?>
                <img src="https://www.travpart.com/English/wp-content/uploads/2019/11/rating_image_new.png" alt="">
                <div class="centered">Ratings <?php echo $ratting; ?></div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if(empty($tourList)) { ?>
    <div class="row">
        <div class="col-md-12">
            <p style="text-align:center;font-size:12px;">
                <?php echo um_get_display_name($user_id); ?> have not purchased any tour package yet.
            </p>
        </div>
    </div>
<?php } ?>

<style type="text/css">
    #footer {
        display: none;
    }
</style>
<?php
get_footer();
?>