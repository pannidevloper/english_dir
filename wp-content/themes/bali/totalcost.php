<?php
/* Template Name: Totalcostpage */
global $wpdb;
session_start();

get_header();



//$totalprice=$_SESSION['totalprice'];
$tourid=intval($_GET['tourid']);
$totalprice=$wpdb->get_var("SELECT total FROM wp_tour WHERE id={$tourid}")/1.01;

$adults=$_COOKIE['hotel_adults'];
$children=$_COOKIE['hotel_childs'];

$noofguest=$adults+$children;

$locnames=$_SESSION['locationnames'];

$hotel_price=$_COOKIE['hotel_price'];

$lta_price=$_SESSION['ltaprice'];

$savestatus=$_SESSION['savestatus'];



$disbaligroup1=(preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disbaligroup1', true)))/100;
$disbaligroup2=(preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disbaligroup2', true)))/100;
$disbaligroup3=(preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disbaligroup3', true)))/100;
$disbaligroup4=(preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disbaligroup4', true)))/100;
$disbaligroup5=(preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disbaligroup5', true)))/100;


$disnonbaligroup1=(preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disnonbaligroup1', true)))/100;
$disnonbaligroup2=(preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disnonbaligroup2', true)))/100;
$disnonbaligroup3=(preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disnonbaligroup3', true)))/100;
$disnonbaligroup4=(preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disnonbaligroup4', true)))/100;
$disnonbaligroup5=(preg_replace('#[^0-9\.]+#', '', get_post_meta($post->ID, '_cs_disnonbaligroup5', true)))/100;


if (is_user_logged_in()) {
    $tp_cpn_valdty=intval(get_option('_travpart_vo_option')['tp_cpn_valdty']);
    $discount_rp=$wpdb->get_var("SELECT discount_rp FROM `wp_coupons` WHERE used=0 AND tour_id={$tourid} AND DATE_SUB(CURDATE(), INTERVAL {$tp_cpn_valdty} DAY)<=date(get_time) AND user_id=".wp_get_current_user()->ID);
} else {
    $discount_rp=0;
}


$bali=0;
foreach ($locnames as $locval) {
    //echo $locval;
    if ((strpos($locval, 'Bali') !== false) || (strpos($locval, 'BALI') !== false) || (strpos($locval, 'bali') !== false)) {
        if ($noofguest>=1 && $noofguest<=2) {
            $discount=$totalprice * $disbaligroup1;
        }
        if ($noofguest>=3 && $noofguest<=5) {
            $discount=$totalprice * $disbaligroup2;
        }
        if ($noofguest>=6 && $noofguest<=10) {
            $discount=$totalprice * $disbaligroup3;
        }
        if ($noofguest>=11 && $noofguest<=15) {
            $discount=$totalprice * $disbaligroup4;
        }
        if ($noofguest>15) {
            $discount=$totalprice * $disbaligroup5;
        }
        $bali=$discount;
    } elseif ($bali!=0) {
        $discount=$bali;
    } else {
        if ($noofguest>=1 && $noofguest<=2) {
            $discount=$totalprice * $disnonbaligroup1;
        }
        if ($noofguest>=3 && $noofguest<=5) {
            $discount=$totalprice * $disnonbaligroup2;
        }
        if ($noofguest>=6 && $noofguest<=10) {
            $discount=$totalprice * $disnonbaligroup3;
        }
        if ($noofguest>=11 && $noofguest<=15) {
            $discount=$totalprice * $disnonbaligroup4;
        }
        if ($noofguest>15) {
            $discount=$totalprice * $disnonbaligroup5;
        }
    }
}

$total_discount=$totalprice-$discount-$discount_rp*get_option('person_price_ration');
$tax=$total_discount * (1/100);
$net_price=$total_discount+$tax;

?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/remodal/remodal.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/remodal/remodal-default-theme.css" />
<link href="<?php bloginfo('template_url'); ?>/css/tour.css" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/jquery-ui.css" rel="stylesheet" type="text/css">

<script src="<?php bloginfo('template_url'); ?>/js/jquery-ui.js"></script>
<script src="<?php bloginfo('template_url'); ?>/remodal/remodal.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/tour.js?version=10"></script>
<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/tableresponsive.css?v1" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<input id="cs_RMB" type="hidden" value="<?php echo get_currency('rmb')['rate']; ?>" />
<input id="cs_USD" type="hidden" value="<?php echo get_currency('usd')['rate']; ?>" />
<input id="cs_AUD" type="hidden" value="<?php echo get_currency('aud')['rate']; ?>" />

<div id="left_menu" style="width:130px; display: none">
  <ul id="left_menu_content">
    <?php
  $display_name = um_user('display_name');
  echo $display_name; // prints the user's display name
  if (is_user_logged_in()) { ?>

    <li> <a
        href="https://www.travpart.com/English/user/<?php echo $display_name; ?>/?profiletab=pages">
        <i class="fa fa-shopping-cart"></i> <br><span></span></a> </li>
    <?php  } else { ?>
    <li> <a href="#"> <i class="fa fa-shopping-cart"></i> <br><span></span></a> </li>

    <?php } ?>

    <?php /*if( trim(get_post_meta($post->ID,'_cs_meetingplace_content',true))!=""){ */?>
    <?php /* <li> <a href="#meetingplace"> <i class="fa fa-plane"></i>  <br><span>0</span></a></li> */ ?>
    <?php/* } */ ?>


    <?php /*if( trim(get_post_meta($post->ID,'_cs_gps_url',true))!="" || trim(get_post_meta($post->ID,'_cs_gps_map',true))!="") { */?>
    <li> <a href="#gps"> <i class="fa fa-hotel"></i> <br><span class="change_price"
          or_pic="<?php echo round($hotel_price/get_option("_cs_currency_RMD"));?>">
          Rp <span class="price_span"><?php echo number_format(round($hotel_price/get_option("_cs_currency_RMD")), 2);?></span></span></a>
    </li>
    <?php /* } */ ?>

    <?php /* if( trim(get_post_meta($post->ID,'_cs_price_content',true))!=""){  */?>
    <li> <a href="#"> <i class="fa fa-location-arrow"></i> <br>
        <?php /* if(is_user_logged_in()){ */ ?>
        <span class="change_price"
          or_pic="<?php echo $lta_price; ?>">Rp <span
            class="price_span"><?php echo number_format($lta_price, 2);?></span></span>
        <?php /* } */ ?>
      </a></li>

    <?php /* } */ ?>

    <?php /* if( trim(get_post_meta($post->ID,'_cs_price_content',true))!=""){  */?>
    <li> <a href="#Price"> <i class="fa fa-truck"></i> <br><span class="change_price"
          or_pic="<?php echo $lta_price; ?>">Rp <span
            class="price_span"><?php echo number_format($lta_price, 2);?></span></span></a>
    </li>
    <?php /* } */ ?>

  </ul>
</div>

<div class="col-xs-12 col-md-12 col-sm-12 payment-marvelappoption1">

  <div class="backtoform" style="float:right;margin-top:20px">
    <a href="/English/tour-details/?bookingcode=BALI<?php echo $tour_id; ?>"
      class="btn btn-default" onclick="">Back to Booking</a>

  </div>
  <div class="table-payment-marvelapp">
    <div class="table-responsive">
      <table class="table table-bordered table-condensed totalcosttable">
        <tbody>
          <tr>
            <td>Total <span>:</span></td>
            <td><span class="change_price"
                or_pic="<?php echo $totalprice; ?>">Rp <span
                  class="price_span"><?php //echo number_format($totalprice,2);?></span></span>
            </td>
          </tr>
          <tr>
            <td>Discount <span>:</span></td>
            <td>
              <p>-<span class="change_price"
                  or_pic="<?php echo $discount+$discount_rp*get_option('person_price_ration'); ?>">Rp
                  <span class="price_span"><?php echo number_format($discount, 2);?></span></span>
              </p>
            </td>
          </tr>
          <tr>
            <td>Total After Disc<span>:</span></td>
            <?php $finalprice=$totalprice-$discount-$discount_rp*get_option('person_price_ration');?>
            <td>
              <p><span class="change_price"
                  or_pic="<?php echo($totalprice-$discount-$discount_rp*get_option('person_price_ration')); ?>">Rp
                  <span class="price_span"><?php echo number_format(($totalprice-$discount-$discount_rp*get_option('person_price_ration')), 2);?></span></span>
              </p>
            </td>
          </tr>
          <tr>
            <td>Tax 1%<span>:</span></td>
            <td>
              <p><span class="change_price"
                  or_pic="<?php echo $tax; ?>">Rp <span
                    class="price_span"><?php echo number_format($tax, 2);?></span></span></p>
            </td>
          </tr>
          <tr>
            <td>Net Price<span>:</span></td>
            <td>
              <p><span class="change_price"
                  or_pic="<?php echo $net_price; ?>">Rp <span
                    class="price_span"><?php echo number_format($net_price, 2);?></span></span>
              </p>
            </td>
          </tr>
        </tbody>
      </table>
      <?php
           if (is_user_logged_in()) {
               $profile_id = um_profile_id();
               $sql5="UPDATE `itinerary` SET totalprice='{$totalprice}',totalpricewithdiscount='{$net_price}',userid='{$profile_id}' WHERE tourid='{$tourid}'";
               $wpdb->query($sql5);
           } else {
               $sql5="UPDATE `itinerary` SET totalprice='{$totalprice}',totalpricewithdiscount='{$net_price}' WHERE tourid='{$tourid}'";
               $wpdb->query($sql5);
           }
          ?>
    </div>
    <!--table-responsive--->

    <div class="payment-marvelapp-btn">
      <!-- <form action="" method="post">-->

      <button class="btn btn-default savemyitinerary-mavelapp" type="submit" name="savemyitinerary" id="savemyitinerary"
        onclick="trial()"><i class="fas fa-save"></i>Save My Itinerary</button>

      <!--</form> -->

      <?php
          $userstatus=is_user_logged_in();
          ?>
      <script>
        function trial() {
          $.ajax({
            type: "POST",
            url: 'https://www.travpart.com/English/total-cost/',
            data: {
              action: 'call_this'
            },
            success: function(html) {
              console.log("ok working");
            }

          });
          var logged = '<?php echo $userstatus ?>';
          console.log(logged);
          if (!logged) {
            $('.hover_bkgr_fricc2').show();
            $('.hover_bkgr_fricc2').click(function() {
              //  $('.hover_bkgr_fricc2').hide();
            });
            $('.popupCloseButton2').click(function() {
              $('.hover_bkgr_fricc2').hide();
            });
          }

          myfun();
        }


        function myfun() {
          $('.hover_bkgr_fricc').show();
          $('.hover_bkgr_fricc').click(function() {
            $('.hover_bkgr_fricc').hide();
          });
          $('.popupCloseButton').click(function() {
            $('.hover_bkgr_fricc').hide();
          });

        }
      </script>

      <?php
        $bookingcode=$wpdb->get_var("SELECT bookingcode from `itinerary` where tourid='{$tourid}'");
        if ($_POST['action'] == 'call_this') {
            if ($savestatus==0) {
                $wpdb->query("INSERT INTO `finalitinerary` SELECT * FROM `itinerary`");
                $_SESSION['savestatus']=1;
               
                $wpdb->query("DELETE FROM `itinerary`");
            }
        }
        ?>

      <div class="hover_bkgr_fricc">
        <span class="helper"></span>
        <div style="width: 35%;">
          <div class="popupCloseButton2"><i class="fas fa-times-circle"></i></div>
          <?php if ($bookingcode) { ?>

          <h3 style="text-align:center;">Your Itinerary Has been Saved</h3><br />
          <h3 style="text-align:center;">Your Booking Code</h3> <br />
          <h3 style="text-align:center;"> ( <?php echo $bookingcode;?> ) </h3>
          <?php } else { ?>
          <h3 style="text-align:center;">Your Itinerary Has Already been Saved</h3><br />
          <h3 style="text-align:center;"></h3> <br />
          <h3 style="text-align:center;"></h3>
          <?php } ?>
        </div>
      </div>

      <div class="hover_bkgr_fricc2">
        <span class="helper2"></span>
        <div>
          <div class="popupCloseButton2"><i class="fas fa-times-circle"></i></div>
          <?php echo do_shortcode('[ultimatemember form_id=14250]');?>
        </div>
      </div>

      <a
        href="https://www.travpart.com/English/confirm-payment/<?php echo $tourid;?>/"><button
          class="btn btn-default paynow-mavelapp paynow_cost" type="submit">Pay now</button></a>
      <h4><a href="https://www.travpart.com/English/discount-trial-page/">How does the discount schemes work?</a></h4>
      <?php if (is_user_logged_in()) { ?>
      <a class="logoutB" href="https://www.travpart.com/English/logout/"><button
          class="btn btn-default paynow-mavelapp pay_log" type="submit">Logout</button></a>
      <?php } ?>
    </div>
  </div>
  <!--table-payment-marvelapp-->
</div>


<style>
  #left_menu_content {
    font-size: 16px !important;
  }

  #left_menu_content li i {
    font-size: 24px !important;
  }

  #left_menu_content li {
    font-size: 16px !important;
    padding: 10px !important;
  }

  .totalcosttable td {
    text-align: right !important;
    padding: 10px !important;
  }

  /* Popup box BEGIN */
  .hover_bkgr_fricc {
    background: rgba(0, 0, 0, .4);
    cursor: pointer;
    display: none;
    height: 100%;
    position: fixed;
    text-align: center;
    top: 0;
    width: 100%;
    z-index: 10000;
    left: 0;
  }

  .hover_bkgr_fricc .helper {
    display: inline-block;
    height: 100%;
    vertical-align: middle;
  }

  .hover_bkgr_fricc>div {
    background-color: #fff;
    box-shadow: 10px 10px 60px #555;
    display: inline-block;
    height: auto;
    max-width: 551px;
    min-height: 100px;
    vertical-align: middle;
    width: 60%;
    position: relative;
    border-radius: 8px;
    padding: 15px 5%;
  }

  .popupCloseButton {
    background-color: #fff;
    border: 3px solid #999;
    border-radius: 50px;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: -20px;
    right: -20px;
    font-size: 25px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
  }

  .popupCloseButton:hover {
    background-color: #ccc;
  }

  .trigger_popup_fricc {
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
  }


  .hover_bkgr_fricc2 {
    background: rgba(0, 0, 0, .4);
    cursor: pointer;
    display: none;
    height: 100%;
    position: fixed;
    text-align: center;
    top: 0;
    width: 100%;
    z-index: 10000;
    left: 0;
  }

  .hover_bkgr_fricc2 .helper2 {
    display: inline-block;
    height: 100%;
    vertical-align: middle;
  }

  .hover_bkgr_fricc2>div {
    background-color: #fff;
    box-shadow: 10px 10px 60px #555;
    display: inline-block;
    height: auto;
    max-width: 551px;
    min-height: 100px;
    vertical-align: middle;
    width: 60%;
    position: relative;
    border-radius: 8px;
    padding: 15px 5%;
  }

  .popupCloseButton2 {
    background-color: #fff;
    border-radius: 50px;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: -20px;
    right: -20px;
    font-size: 25px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
  }

  .popupCloseButton2:hover {
    background-color: #f3f3f3;
  }

  .popupCloseButton2 i.fa-times-circle {
    color: #dc5454;
  }

  .trigger_popup_fricc2 {
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
  }

  .backtoform {
    display: none;
  }



  /* Popup box BEGIN */

  .payment-marvelappoption {
    width: 1200px;
  }

  .payment-marvelappoption1 {
    text-align: center;
    margin: 40px 0px;
  }

  .table-payment-marvelapp {
    display: inline-block;
    width: 700px;
    padding: 10px;
    background-color: #f2f5f7;
  }

  .table-payment-marvelapp table {
    margin-bottom: 0px;
  }

  .table-payment-marvelapp table tbody {}

  .table-payment-marvelapp table tbody tr {}

  .table-payment-marvelapp table tbody tr td {
    font-size: 30px;
    font-weight: 400;
    width: 50%;
    text-align: left;
    color: #282828;
    border-top: inherit !important;
    border-bottom: 1px solid #ddd;
  }

  .table-payment-marvelapp table tbody tr th {
    font-size: 30px;
    font-weight: 400;
    width: 50%;
    text-align: left;
    color: #282828;
    border-top: inherit !important;
    border-bottom: 1px solid #ddd;
  }

  .table-payment-marvelapp table tbody tr th span {
    float: right;
    display: inline-block;
  }

  .payment-marvelapp-btn {
    margin-top: 20px;
    margin-bottom: 20px;
    text-align: center;
    width: 100%;
    display: inline-block;
    float: left;
  }

  .payment-marvelapp-btn h4 {
    display: inline-block;
    width: 100%;
    text-align: right;
    padding-top: 10px;
  }

  .payment-marvelapp-btn h4 a {
    display: inline-block;
    color: #1a74f0;
    font-size: 16px;
    font-weight: 400;
  }

  .payment-marvelapp-btn button {
    border-radius: 5px;
    border-color: transparent !important;
    font-size: 18px;
    text-transform: uppercase;
    font-weight: 400;
    height: 46px;
    box-shadow: 2px 10px 10px #bdbdbe;
  }

  button.btn.btn-default.paynow-mavelapp.paynow_cost,
  .pay_log {
    background-color: #22b14c !important;
    color: white !important;
  }


  .savemyitinerary-mavelapp {
    float: left;
    color: #22b14c !important;
  }

  i.fas.fa-save {
    color: black;
    margin-right: 5px;
  }

  .paynow-mavelapp {
    float: right;
  }


  .list-dayscontrol button {
    background-color: transparent !important;
    border: none !important;
    color: #34778a !important;
    font-size: 40px;
    font-weight: 900;
  }



  .main-headingmarvel-content th a {

    display: inline-block;
    color: #ccc;
    font-size: 18px;

  }

  .marvelapp-table tbody tr td a {

    display: inline-block;
    color: #ccc;
    font-size: 18px;

  }

  @media only screen and (max-width: 600px) {
    .table-payment-marvelapp table tbody tr td p {
        font-size: 20px !important;
    }
    .table-responsive {
      border: none !important;
    }

    .table-payment-marvelapp {
      width: 100% !important;
    }

    .totalcosttable td {
      border: none !important;
      width: 100% !important
    }

    .totalcosttable td:nth-child(odd) {
      background: #3b898a !important;
      color: white !important;
    }

    .totalcosttable td:nth-child(even) {
      background: #e9e9e9 !important;
      color: #666 !important;
    }

    #left_menu {
      display: none !important;
    }

    .table-payment-marvelapp {
      margin-top: 60px;
    }

    .backtoform {
      margin-right: 10px;
      margin-top: 50px !important;
    }

  }


  /* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

  @media (min-width: 1281px) {}

  /* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

  @media (min-width: 1025px) and (max-width: 1280px) {}

  /* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

  @media (min-width: 768px) and (max-width: 1024px) {}

  /* 
  ##Device = Tablets, Ipads (landscape)
  ##Screen = B/w 768px to 1024px
*/

  @media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {}

  /* 
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

  @media (min-width: 481px) and (max-width: 767px) {}

  /* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

  @media (min-width: 320px) and (max-width: 480px) {

    .backtoform {
      display: none;
    }

    .logoutB {
      display: none;
    }

    .mobileonly.mobileapp {
      display: none;
    }

    .totalcosttable td:nth-child(even) {}

    .totalcosttable tbody tr {
      display: j;
    }

    .totalcosttable td {
      text-align: left !important;
      width: 50% !important;
    }

    .table-payment-marvelapp table tbody tr td {
      font-size: 24px;
    }

    .payment-marvelapp-btn button {
      font-size: 16px;
      height: 35px;
    }

    .payment-marvelapp-btn h4 {
      text-align: center;
      padding-top: 20px;
    }

    .payment-marvelappoption1 {
      text-align: center;
      margin: 45px 0 0 0;
    }

    .table-payment-marvelapp {
      padding-bottom: 0;
    }

    .payment-marvelapp-btn {
      margin-bottom: 0px;

    }

    span.price_span {
      margin-left: 10px;
    }


  }
</style>

<?php
get_footer();
