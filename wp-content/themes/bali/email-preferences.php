<?php
/* Template Name: email-preferences*/ 
get_header();

global $wpdb;

if(is_user_logged_in()) {
	$email=wp_get_current_user()->user_email;
}
else if(isset($_COOKIE['email'])) {
	$email=htmlspecialchars($_COOKIE['email']);
}
else {
	exit(wp_redirect(home_url('login/?from=email-preferences')));
}

$is_potential_seller=$wpdb->get_var("SELECT COUNT(*) FROM `wp_potential_sale_agent` WHERE email='{$email}'")>0;

$is_seller=current_user_can('um_travel-advisor');

/*$enable_email_notification=$wpdb->get_var("SELECT SUM(subscribe) FROM `unsubscribed_emails` WHERE email='{$email}'");

if($enable_email_notification==0) {
	$enable_email_notification=false;
}
else {
	$enable_email_notification=true;
}*/

$enable_email_notification=true;

$eBlog=$wpdb->get_row("SELECT `subscribe`,`modify_date` FROM `unsubscribed_emails` WHERE email='{$email}' AND type=1");
$eTravchat=$wpdb->get_row("SELECT `subscribe`,`modify_date` FROM `unsubscribed_emails` WHERE email='{$email}' AND type=2");
$eUserNotification=$wpdb->get_row("SELECT `subscribe`,`modify_date` FROM `unsubscribed_emails` WHERE email='{$email}' AND type=3");
$ePotential=$wpdb->get_row("SELECT `subscribe`,`modify_date` FROM `unsubscribed_emails` WHERE email='{$email}' AND type=4");
$eService=$wpdb->get_row("SELECT `subscribe`,`modify_date` FROM `unsubscribed_emails` WHERE email='{$email}' AND type=5");
$eFollower=$wpdb->get_row("SELECT `subscribe`,`modify_date` FROM `unsubscribed_emails` WHERE email='{$email}' AND type=6");
$eConnection=$wpdb->get_row("SELECT `subscribe`,`modify_date` FROM `unsubscribed_emails` WHERE email='{$email}' AND type=7");

if($eBlog->subscribe==='0' && $eTravchat->subscribe==='0' && $eUserNotification->subscribe==='0' && $eConnection->subscribe==='0') {
	$enable_email_notification=false;
}

if($is_potential_seller && !$enable_email_notification && $ePotential->subscribe==='0') {
	$enable_email_notification=false;
}

if($is_seller && !$enable_email_notification && $eService->subscribe==='0' && $eFollower->subscribe==='0') {
	$enable_email_notification=false;
}

?>

<style>
.pref_email_wrapper{
		background: #fff;
		-webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
    	box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
    	padding: 25px 20px;
    	margin: 25px 0px;
    	border-radius:5px; 
}
.email_div{
	padding: 25px 20px;
	border-radius:5px;
	background: #fff;
	-webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
}

.email_div_2{
	margin-top: 25px;
	padding: 25px 20px;
	border-radius:5px;
	background: #fff;
	-webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
}

.email_div h3{
    font-size: 23px;
}
.email_div_2 h3{
    font-size: 23px;
}
.email_div p {
    font-size: 14px;
    letter-spacing: 1px;
    color: #a0a0a0;
}
.mchkbox{
	display: inline-block;
	vertical-align: middle;
	font-size:20px		
}
.mchkboxtext{
	display: inline-block;
    vertical-align: middle;
    font-size: 15px;
    color: #0606068f;
    margin-left: 20px;
    font-weight: 400;
}
.chkbox{
	margin-top: 50px;
}
 input.largerCheckbox { 
    width: 30px; 
    height: 30px; 
}
.font_s{
	text-align: left !important;
	color:#676767;
	font-weight: bold;
	font-size: 17px;
	padding: 15px !important;
	vertical-align: middle !important;
}

 
.ctds tr{
	padding: 10px;
}
.ctds tr td {
	letter-spacing: 1px;
    padding: 15px !important;
    text-align: left !important;
	 vertical-align: middle;
	font-size: 14px;
	color: #979797
}
.ctds tr td:first-child{
	width:70%; vertical-align: middle;
}
.fsze{
	color: #818181;
	letter-spacing: 1px;
	font-size:15px;
}

@media(max-width:600px){
	.mchkboxtext{
		font-size:13px;
	}
	.email_div h3 {
	    font-size: 15px;
	    font-weight: bold;
	}
	input.largerCheckbox {
	    width: 20px;
	    height: 20px;
	}
.email_div_2{
	padding: 5px;
}
.dropdown{
	z-index: auto !important;
}
.dropdown-menu li a {
    padding: 10px;
    color: #333;
    font-weight: bold;
}
	.ctds tr td{
		padding: 1px !important;
		font-size:12px;
	}
	.fsze{
		font-size:10px;
	}
	.email_div_2 h3 {
	    font-size: 15px;
	    font-weight: bold;
	}
	.font_s{
		font-size: 12px;
		padding: 0px !important;
	}
}

</style>

<script>
$(document).ready(function () {
	var ajax_process=false;
	function update_subscribe(type, subscribe) {
		if(ajax_process) {
			return;
		}
		ajax_process=true;
		$.ajax({
            type: 'GET',
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            dataType: 'json',
            data: {
                action: 'unsubscribe_email',
				'type': type,
				subscribe: subscribe
            },
            success: function (data) {
				ajax_process=false;
				window.location.reload();
            }
        });
	}
	$('#enable_email_notification').change(function() {
		var subscribe=1;
		if(!$(this).prop('checked')) {
			subscribe=0;
		}
		update_subscribe(0,subscribe);
	});
	$('.subscrible-choose li').click(function() {
		var subscribe=$(this).attr('s');
		var type=$(this).parent().attr('type');
		var now=$(this).parent().attr('now');
		if(subscribe==now) {
			return;
		}
		update_subscribe(type,subscribe);
	});
});
</script>

<div class="pref_email_wrapper">
	<div class="email_div">
	  <h3>Email Preference</h3>
	  <p>Change apply to all emails for all of your properties.</p>
	  <div class="chkbox"> 
	   <div class="mchkbox">
	   	 <input id="enable_email_notification" type="checkbox" class="largerCheckbox"
            name="checkBox1" <?php echo $enable_email_notification?'checked':''; ?> > 
	   </div>
	   <div class="mchkboxtext">
	   	<strong>Enable notification by email</strong>
	   </div>
	  </div>
	</div>
	
	<div class="email_div_2">
		
		<h3>Modified Email Preference</h3>
	
		<table class="table table-condensed table-responsive">
	      <thead>
	        <tr>
	          <td class="font_s">Subject</td>
	          <td class="font_s">Status</td>
	          <td class="font_s">Date modified</td>
	        </tr>
	      </thead>
	      <tbody class="ctds">
	        <tr>
	          <td>Blog Notifications</td>
	          <td>
	          	<div class="dropdown">
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    <strong class="fsze"><?php echo empty($eBlog)||($eBlog->subscribe==1)?'Subscribed':'Unsubscribe'; ?></strong>
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu subscrible-choose" type="1" now="<?php echo empty($eBlog)||($eBlog->subscribe==1)?'1':'0'; ?>" aria-labelledby="dropdownMenu1">
				    <li s="1"><a href="#">Subscribed</a></li>
				    <li s="0"><a href="#">Unsubscribe</a></li>
				  </ul>
				 </div>
	          </td>
	          <td><?php echo empty($eBlog)?'':date('d M Y',strtotime($eBlog->modify_date)); ?></td>
	        </tr>
	        <tr>
	          <td>TravChat Messages</td>
	          <td>
	          	<div class="dropdown">
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    <strong class="fsze"><?php echo empty($eTravchat)||($eTravchat->subscribe==1)?'Subscribed':'Unsubscribe'; ?></strong>
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu subscrible-choose" type="2" now="<?php echo empty($eTravchat)||($eTravchat->subscribe==1)?'1':'0'; ?>" aria-labelledby="dropdownMenu1">
				    <li s="1"><a href="#">Subscribed</a></li>
				    <li s="0"><a href="#">Unsubscribe</a></li>
				  </ul>
				 </div>
	          </td>
	          <td><?php echo empty($eTravchat)?'':date('d M Y',strtotime($eTravchat->modify_date)); ?></td>
	        </tr>
	        
	        <tr>
	          <td>User's Notification</td>
	          <td>
	          	<div class="dropdown">
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    <strong class="fsze"><?php echo empty($eUserNotification)||($eUserNotification->subscribe==1)?'Subscribed':'Unsubscribe'; ?></strong>
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu subscrible-choose" type="3" now="<?php echo empty($eUserNotification)||($eUserNotification->subscribe==1)?'1':'0'; ?>" aria-labelledby="dropdownMenu1">
				    <li s="1"><a href="#">Subscribed</a></li>
				    <li s="0"><a href="#">Unsubscribe</a></li>
				  </ul>
				 </div>
	          </td>
	          <td><?php echo empty($eUserNotification)?'':date('d M Y',strtotime($eUserNotification->modify_date)); ?></td>
	        </tr>
	        
	        <?php if($is_potential_seller) { ?>
	        <tr>
	          <td>Potential Seller</td>
	          <td>
	          	<div class="dropdown">
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    <strong class="fsze"><?php echo empty($ePotential)||($ePotential->subscribe==1)?'Subscribed':'Unsubscribe'; ?></strong>
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu subscrible-choose" type="4" now="<?php echo empty($ePotential)||($ePotential->subscribe==1)?'1':'0'; ?>" aria-labelledby="dropdownMenu1">
				    <li s="1"><a href="#">Subscribed</a></li>
				    <li s="0"><a href="#">Unsubscribe</a></li>
				  </ul>
				 </div>
	          </td>
	          <td><?php echo empty($ePotential)?'':date('d M Y',strtotime($ePotential->modify_date)); ?></td>
	        </tr>
			<?php } ?>
			
			<?php if($is_seller) { ?>
	        <tr>
	          <td>Service Provider Performance</td>
	          <td>
	          	<div class="dropdown">
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    <strong class="fsze"><?php echo empty($eService)||($eService->subscribe==1)?'Subscribed':'Unsubscribe'; ?></strong>
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu subscrible-choose" type="5" now="<?php echo empty($eService)||($eService->subscribe==1)?'1':'0'; ?>" aria-labelledby="dropdownMenu1">
				    <li s="1"><a href="#">Subscribed</a></li>
				    <li s="0"><a href="#">Unsubscribe</a></li>
				  </ul>
				 </div>
	          </td>
	          <td><?php echo empty($eService)?'':date('d M Y', strtotime($eService->modify_date)); ?></td>
	        </tr>

			<tr>
	          <td>Follower Notification</td>
	          <td>
	          	<div class="dropdown">
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    <strong class="fsze"><?php echo empty($eFollower)||($eFollower->subscribe==1)?'Subscribed':'Unsubscribe'; ?></strong>
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu subscrible-choose" type="6" now="<?php echo empty($eFollower)||($eFollower->subscribe==1)?'1':'0'; ?>" aria-labelledby="dropdownMenu1">
				    <li s="1"><a href="#">Subscribed</a></li>
				    <li s="0"><a href="#">Unsubscribe</a></li>
				  </ul>
				 </div>
	          </td>
	          <td><?php echo empty($eFollower)?'':date('d M Y', strtotime($eFollower->modify_date)); ?></td>
			</tr>
			<?php } ?>

			<tr>
	          <td>Connection Suggestions</td>
	          <td>
	          	<div class="dropdown">
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    <strong class="fsze"><?php echo empty($eConnection)||($eConnection->subscribe==1)?'Subscribed':'Unsubscribe'; ?></strong>
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu subscrible-choose" type="7" now="<?php echo empty($eConnection)||($eConnection->subscribe==1)?'1':'0'; ?>" aria-labelledby="dropdownMenu1">
				    <li s="1"><a href="#">Subscribed</a></li>
				    <li s="0"><a href="#">Unsubscribe</a></li>
				  </ul>
				 </div>
	          </td>
	          <td><?php echo empty($eConnection)?'':date('d M Y',strtotime($eConnection->modify_date)); ?></td>
	        </tr>
	        
	        
	      </tbody>
	    </table>	
	</div>
	
</div>

<?php get_footer(); ?>