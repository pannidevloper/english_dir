<?php
wp_enqueue_media();


$page_name =  basename(get_permalink());

if (isset($_GET['user'])) {

    $username = htmlspecialchars( $_GET['user'] );
    $user = get_user_by('login', htmlspecialchars($_GET['user']));
    if (is_numeric($username) AND $username>0) {

        $user = get_user_by('id',  htmlspecialchars ($_GET['user']));
        $username = $user->user_login;
    }
    
    if($user==NULL){

    //    exit(wp_redirect(home_url('my-time-line?user='.wp_get_current_user()->user_login)));

        }

    if ($user) {
        $user_id = $user->ID;
        $verified = get_user_meta($user_id, 'verification_status', true);
        $gender = get_user_meta($user_id, 'gender', true);
        $bio = get_user_meta($user_id, 'description', true);
        $follow_count = $wpdb->get_var("SELECT COUNT(*) FROM `social_follow` WHERE `sf_agent_id` = '{$user_id}'");
        um_fetch_user($user_id);

        //connecton count

        $connection_count = $wpdb->get_var(
            "SELECT COUNT(*) FROM friends_requests WHERE ( user2 = '{$user_id}' OR user1 = '{$user_id}') AND status=1"
        );

    

        // user role

        $user_meta = get_userdata($user_id);
        $user_roles = $user_meta->roles;

        if (in_array('um_travel-advisor', $user_roles, true)) {
            $role = "Seller";
        } elseif (in_array('um_clients', $user_roles, true)) {

            $role = "Buyer";
        } else {
            $role = "";
        }
    } else {

        $user_id = false;
    }
} else {
    wp_redirect('/');
    //exit();
}
if (is_email($username)) {
    $username_or_email = "email='$username'";
} else {
    $username_or_email = "username='$username'";
}
$row = $wpdb->get_row("SELECT wp_users.ID as wp_user_id, user.* from `wp_users`,`user` WHERE {$username_or_email} AND user.username=wp_users.user_login", ARRAY_A);

$travpart_user_id = $row['userid'];

//if (is_user_logged_in() and get_current_user_id() == $user_id) {
if (!empty($user_id)) {
    $cover_img = wp_get_attachment_url(get_user_meta($user_id, 'cover', true));
    $cover_update_time = get_user_meta($user_id, 'cover_update_time', true);
}


//if (empty($cover_img)) {
    //$cover_img = "https://www.bulgarihotels.com/.imaging/bhr-wide-small-jpg/dam/BALI/Foto-Bali_ultime-caricate/15-(3).jpg/jcr%3Acontent";
//}

if ($wpdb->get_var("SELECT COUNT(*) FROM wp_users_block WHERE userid='" . get_current_user_id() . "' AND block_userid='{$user_id}'") == 0) {
    $block_text = 'Block';
} else {
    $block_text = 'Unblock';
}

//Cover comments
$cover_comments = $wpdb->get_results("SELECT * FROM `user_cover_comment` WHERE owner='{$user_id}' ORDER BY time ASC");

?>
<link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet" />
<script>
    jQuery(document).ready(function($) {
        var website_url ="https://www.travpart.com/English";
        var showChar = 150;
        var ellipsestext = "...";
        var moretext = "more";
        var lesstext = "less";
        $('.more').each(function() {
            var content = $(this).html();
            if (content.length < 1) return false;
            if (content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar, content.length - showChar);

                var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="https://www.travpart.com/English/user/<?php echo esc_html($username); ?>" class="morelink">' + moretext + '</a></span>';

                $(this).html(html);
            }

        });

        //$(".morelink").click(function() {
            //if ($(this).hasClass("less")) {
               // $(this).removeClass("less");
                //$(this).html(moretext);
            //} else {
               // $(this).addClass("less");
               // $(this).html(lesstext);
           // }
            //$(this).parent().prev().toggle();
            //$(this).prev().toggle();
           // return false;
        //});
        $(".btn_profile_dots btn_dots").click(function(){
            $(".dots_arrow_top").toggle();
        });
        $(".report_Click-here").on('click', function() {
          $(".report_custom-model-main").addClass('report_model-open');
          $(".dots_arrow_top").hide();
        }); 
        $(".report_close-btn").click(function(){
          $(".report_custom-model-main").removeClass('report_model-open');
        });
        var $report_submit = $('.report_send_btn input[type="submit"]');
        $report_submit.prop('disabled', true);
        $(".report_input_btn button.report_input_btn_inappro").click(function(){
            $report_submit.prop('disabled', false);
            $(".report_input_btn input").val('Posting inappropriate things');
            $(".report_send_btn input").css("opacity","1");
        });
        $(".report_input_btn button.report_input_btn_fake").click(function(){
            $report_submit.prop('disabled', false);
            $(".report_input_btn input").val('Fake account');
            $(".report_send_btn input").css("opacity","1");
        });
        $('.report_input_other_btn input').keyup(function(){
            $report_submit.prop('disabled', false);
             if($(this).val() != ''){
                $(".report_send_btn input").css("opacity","1");
            }
        });
    });
</script>
<?php if (empty($cover_img)) { ?>
    <style type="text/css">
        .timelinecover{
            height: 350px;
            background: linear-gradient(to bottom, #F0F2F5 75%, grey 100%);
            border-radius: 0px 0px 10px 0px;
            box-shadow: 0 0px 5px 0px #afacac;
        }
        .timelinecover img{
            display: none;

        }
        .profile_main_cover{
           width: 100%;
           height: 202px; 
           background: linear-gradient(to bottom, #F0F2F5 75%, grey 100%);
           border-radius: 10px;
           box-shadow: 0 0px 5px 0px #afacac;
        }
        .profile_main_cover img{
            display: none;
        }
    </style>
<?php } ?>


<style>

.capture_photo{
    text-decoration: none !important;
}
.dropdownMenuButton_1{
  right: 8px  !important;
  top: 40px;
  float: none !important;
  left: auto !important;
}

.connect_button_connected .p-connect-area-text{
     color: #000 !important;
}
.connect_button_connected{
    background: #cacaca !important;
    color: #000 !important;
    width: 130px !important;
}
.p-dots-area_1 {
    background: #bab1b1;
    padding: 4px 7px;
    outline: none !important;
    border-radius: 0px 5px 5px 0px;
    font-size: 2em;
    z-index: 20000;
    border: 1px solid #bab1b1;
    vertical-align: middle;
  }
    .timelineprofilepic div.um-profile-photo.um-trigger-menu-on-click {
        margin-left: 0px;
        width:206px;
    }
    .profile_main_cover img{
        width:100%;
        height: 202px;
    }
    .timelinecover img{
        height: 350px;
    }
    .custom_height_bio {
        text-align: center;
        background-color: white;
    }

    a.morelink {
        text-decoration: none;
        outline: none;
    }

    .more {
        font-size: 14px;
        font-family: 'Roboto', sans-serif;
    }

    .morecontent span {
        display: none;
    }

    .comment {
        width: 400px;
        background-color: #f0f0f0;
        margin: 10px;
    }

    .um-dropdown {
        top: 60px !important;
    }


    .btn_dots {
        top: 10px;
        left: 116px;
    }

    .btn_profile_dots {
        position: absolute;
        background: #20a5ca;
        padding-top: 4px;
        padding-bottom: 6px;
        border: 0px;
        letter-spacing: 1px;
        border-radius: 5px;
        color: #fff;
        font-size: 13px;
        font-family: "Roboto", Sans-serif;
        cursor: pointer;
    }
 
    .btn_profile_dots i {
        font-size: 6px;
        padding: 1px;
        vertical-align: middle;
    }
    .btn_profile_con i {
        font-size: 13px !important;
        padding-right: 3px;
        margin-right: 0px !important;
        padding-left: 0px !important;
    }
    .btn_profile_con {
        width: 130px;
        background: #008a66;
        padding: 12px 10px;
        border: 0px;
        letter-spacing: 1px;
        border-radius: 5px;
        color: #fff;
        line-height: 15px;
        font-size: 13px;
        font-family: "Roboto", Sans-serif;
        cursor: pointer;
    }
    
     .btn_block {
        width: 130px;
        background: #e0e0e0;
        padding: 12px 10px;
        border: 0px;
        letter-spacing: 1px;
        border-radius: 5px;
        
        line-height: 15px;
        font-size: 13px;
        font-family: "Roboto", Sans-serif;
        cursor: pointer;
    }
    .btn_block a{
        color: #000;
        font-weight: bold;
        text-decoration: none;
    }

    .btn_profile_con i {
        font-size: 13px !important;
        padding-right: 3px;
        margin-right: 0px !important;
        padding-left: 0px !important;
    }
    .btn_profile_con a{
        text-decoration: none;
    }
   .btn_follow a{
    color: #fff;
    text-decoration: none;
   }
   .btn_follow {
    top: 0px;
    }
   .btn_follow{
        width: 130px;
        background: #008a66;
        padding: 11px 10px;
        border: 0px;
        letter-spacing: 1px;
        right: 13px;
        border-radius: 5px;
        color: #fff;
        font-size: 13px;
        font-family: "Roboto", Sans-serif;
        cursor: pointer;
    }

    button.btn_profile_con:hover {
        opacity: inherit;
    }

    button.btn_profile_con:focus {
        outline: none;
    }

    button.btn_follow:hover {
        opacity: inherit;
    }

    button.btn_follow:focus {
        outline: none;
    }
    button.btn_profile_dots:focus{
        outline: none;
    }

    .dots_box {
        width: 124px;
        background-color: #ffffff;
        display: none;
    }

    .dots_box.dots_arrow_top {
        position: absolute;
        left: 30px;
        top: 49px;
    }

    .dots_box.dots_arrow_top:after {
        content: " ";
        position: absolute;
        right: 15px;
        top: -6px;
        border-top: none;
        border-right: 6px solid transparent;
        border-left: 6px solid transparent;
        border-bottom: 6px solid #ffffff;
    }

    .dots_box.dots_arrow_top span {
        display: block;
        text-align: left;
        padding: 5px 8px;
    }

    .dots_box.dots_arrow_top span:hover {
        background-color: #ccc;
        cursor: pointer;
    }

    .timelineprofilepic .um-dropdown {
        top: 145px !important;
        left: 0px !important;
    }
    .report_custom-model-main {
      text-align: center;
      overflow: hidden;
      position: absolute;
      top: 10%;
      right: 20%;
      bottom: 0;
      left: 20%; /* z-index: 1050; */
      -webkit-overflow-scrolling: touch;
      outline: 0;
      opacity: 0;
      -webkit-transition: opacity 0.15s linear, z-index 0.15;
      -o-transition: opacity 0.15s linear, z-index 0.15;
      transition: opacity 0.15s linear, z-index 0.15;
      z-index: -1;
      overflow-x: hidden;
      overflow-y: auto;
    }
    .report_model-open {
      z-index: 99999;
      opacity: 1;
      overflow: hidden;
    }
    .report_custom-model-inner {
      -webkit-transform: translate(0, -25%);
      -ms-transform: translate(0, -25%);
      transform: translate(0, -25%);
      -webkit-transition: -webkit-transform 0.3s ease-out;
      -o-transition: -o-transform 0.3s ease-out;
      transition: -webkit-transform 0.3s ease-out;
      -o-transition: transform 0.3s ease-out;
      transition: transform 0.3s ease-out;
      transition: transform 0.3s ease-out, -webkit-transform 0.3s ease-out;
      display: inline-block;
      vertical-align: middle;
      margin: 12px auto;
      width: 100%;
    }
    .report_custom-model-wrap {
      display: block;
      width: 100%;
      position: relative;
      background-color: #fff;
      border: 1px solid #999;
      border: 1px solid rgba(0, 0, 0, 0.2);
      border-radius: 6px;
      -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
      box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
      background-clip: padding-box;
      outline: 0;
      text-align: left;
      padding: 20px;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .report_model-open .report_custom-model-inner {
      -webkit-transform: translate(0, 0);
      -ms-transform: translate(0, 0);
      transform: translate(0, 0);
      position: relative;
      z-index: 999;
    }

    .report_close-btn {
      position: absolute;
      right: 7px;
      cursor: pointer;
      z-index: 99;
      font-size: 25px;
      color: black;
    }
    .report_heading{
      position: absolute;
      left: 7px;
      cursor: pointer;
      z-index: 99;
      font-size: 20px;
      color: black;  
    }
    .report_pop-up-content-wrap img {
        width: 60px;
        height: 60px;
        border:none;
        box-shadow: none;
        padding-top: 15px;
    }
    .report_pop-up-content-wrap span.report_heading_content {
        position: relative;
        top: 10px;
        left: -15px;
        font-size: 15px;
    }
    .report_input_btn button{
        font-size: 15px;
        border-radius: 15px;
        border: 1px solid grey;
        padding: 5px;
        background-color: #F5F6F7;
    }
    .report_input_btn_inappro{
        margin-left: 40px;
    }
    .report_input_btn_fake{
        margin-left: 10px;
    }
    .report_input_other_btn{
        margin-left: 40px;
    }
    .report_input_other_btn {
        margin-left: 40px;
        margin-top: 10px;
    }
    .report_input_other_btn span{
        font-size: 18px;
    }
    .report_input_other_btn input{
        border-bottom: 2px dashed lightgrey;
        border-left: none;
        border-right: none;
        border-top: none;
    }
    .report_send_btn{
        margin-top: 10px;
        text-align: end;
    }
    .report_send_btn input{
        background-color: green;
        color: white;
        font-size: 15px;
        opacity: 0.3;
        border: none;
        border-radius: 2px;
    }
    .report_send_btn input:hover {
        opacity: 0.3 !important;
        color: white !important;
    }
@media(max-width: 600px){
    .report_custom-model-wrap {
        padding: 10px !important;
    }
    
    .report_custom-model-main{
        top: 0 !important;
        right: 2% !important;
        left: 2% !important;
    }
    .report_pop-up-content-wrap span.report_heading_content {
        font-size: 11px !important;
    }
    .report_input_btn button{
        font-size: 12px !important;
    }
    .report_input_other_btn span{
        font-size: 15px !important;
    }
    .report_send_btn input{
       font-size: 15px !important; 
    }
    .btn_profile_dots{
        background: #9E9E9F;
        border-radius: 0px 5px 5px 0px;
        padding-bottom: 5px;
    }
    .btn_dots {
        left: 109px;
    }
}
.um-profile-photo{
    float: none !important;
    width: auto !important;
    margin: auto !important;
}
</style>

<?php
$button_data = $user->ID;
$logged_in = wp_get_current_user();
$Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '$button_data' AND user1 = '{$logged_in->ID}'");
$btn_icon = $Data->status;
if (empty($Data)) {
    $button_text = 'Connect';
    $connect_button = 'connect_button';
} elseif ($Data->status == 0) {
    $button_text = 'Request <br /> sent';
    $connect_button = 'connect_button_sent';
} elseif ($Data->status == 1) {
    $button_text = 'Connected';
    $connect_button = 'connect_button_connected';
}
elseif ($Data->status == 2) {
    $button_text = 'Blocked';
    $connect_button = 'connect_button_blocked';
}
?>
<?php

//check if user is blocked

$check_block = $wpdb->get_var("SELECT id FROM `friends_requests` WHERE user2 = '$logged_in->ID' AND user1 = '{$user->ID}' AND status=2");


$current_user = wp_get_current_user();
$current_user_id = $current_user->ID;
$followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$current_user_id' AND sf_agent_id = '$user_id'", ARRAY_A);
if (!empty($followData)) {

    $fellow = "Following";
} else {
    $fellow = "Follow";
}
$display_name = um_user('display_name'); 

// check either this user is blocked by other user,if blocked hide the connect button

$query_to_check_block = $wpdb->get_var("SELECT * FROM `friends_requests` where user1='$user_id' AND user2='$current_user_id' AND status=2");

?>
<div class="timeline_for_mobile" style="display: none;">

    <input type="file"  id="capt_camera" accept="image/*;capture=camera">
    <div class="custom_profile cProfile-mobile" <?php echo ($_GET['device'] == 'app') ? 'style="margin-top:0"' : ''; ?>>
        <div class="profile_row">
            <div class='profile_main_cover'>
                <img src="<?php echo $cover_img; ?>">
                <?php if (is_user_logged_in() AND get_current_user_id() != $user_id AND empty($query_to_check_block)) {

                ?>
                    <button class="btn_profile btn_messages"><a target="_blank" style="color: white" href="<?php echo home_url('travchat/user/addnewmember.php?user='.$travpart_user_id) ?>"><i class="far fa-comment-alt"></i> Messages</a></button>
                    <!--<button class="btn_profile_con btn_connect <?php echo $connect_button; ?>">
                        <i class="fas fa-user-plus"></i>
                        <a href="#" style="color:white" class="p-connect-area-text" data-connect_friend="<?php echo $button_data; ?>">
                            <?php echo $button_text; ?>
                        </a>
                    </button>-->
                    
                    <button class="btn_profile_con btn_connect <?php echo $connect_button; ?>">
                        <div class="col-md-33 rm_d">
                            <?php if(empty($Data)){ ?>
                                <i class="fas fa-user-plus"></i>
                            <?php }elseif($Data->status == 0){ ?>
                                <i class="fas fa-user-minus"></i>   
                            <?php } ?>
                        </div>
                        <div class="col-md-99 rm_d">
                            <a href="#" style="color:white" class="p-connect-area-text" data-connect_friend="<?php echo $button_data; ?>">
                                <?php echo $button_text; ?>
                            </a>
                        </div>
                    </button>
                    <?php if ($Data->status == 1) { ?>
                    <button class="p-dots-area_1_mob">
                    <a class="dropdown-toggle" id="dropdownMenuButton_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu dropdownMenuButton_1" aria-labelledby="dropdownMenuButton_1">
                      <?php if( $button_text=='Connected' ){ ?>
                      <a id='unconnect' class="dropdown-item connect_button_connected" href="#" >Unconnected</a>
                    <?php } ?>

                
                    </div>
                  </button>

              <?php } ?>
                    
                    <div class="dots_box dots_arrow_top">
                        <span class="report_Click-here">Report Profile</span>
                        <span class="block-user" userid="<?php echo $user_id; ?>">
                            <?php echo $block_text; ?>
                        </span>
                    </div>
                    <div class="report_custom-model-main">
                        <div class="report_custom-model-inner"> 
                        <span class="report_heading">Please Select</span>       
                        <div class="report_close-btn">×</div>
                            <div class="report_custom-model-wrap">
                                <div class="report_pop-up-content-wrap">
                                    <img src="https://www.travpart.com/wp-content/uploads/2020/04/pro_report.png">
                                    <span class="report_heading_content">You can report the profile after selecting a problem</span>
                                    <div class="report_input_btn">
                                        <button class="report_input_btn_inappro">Posting inappropriate things</button>
                                        <button class="report_input_btn_fake">Fake account</button>
                                        <input type="hidden" value="" />
                                    </div>
                                    <div class="report_input_other_btn">
                                        <span>Others:</span>
                                        <input type="text" \>
                                    </div>
                                    <div class="report_send_btn">
                                        <input userid="<?php echo $user_id; ?>" type="submit" value="Send">
                                    </div>
                                </div>
                            </div>  
                        </div> 
                    </div> 
                    <?php if ($role == 'Seller' && $Data->status != 1) { ?>
                        <button class="btn_profile_dots btn_dots"><i class="fa fa-square" aria-hidden="true"></i><i class="fa fa-square" aria-hidden="true"></i><i class="fa fa-square" aria-hidden="true"></i></button>
                        <button id="fellow_press_m" class="btn_follow"><i class="fas fa-wifi"></i><a><?php echo $fellow; ?></a></button>
                    <?php } ?>



                <?php } ?>


                <?php if (is_user_logged_in() and get_current_user_id() == $user_id) { ?>
                    <div class="updatecoverpic dropdown-toggle" data-toggle="dropdown-menu" aria-expanded="true">
                        <i class="fas fa-photo-video"></i> Update Cover Pic
                    </div>
                <?php  } ?>


                <div id="PostDId" class="updateCP dropdown-menu" role="menu" aria-labelledby="updateC">
                    <li><a href="#" class="upload-cover">Select Photo</a></li>
                    <li><a href="#" class="upload-cover upload-direct">Upload</a></li>
                    <li><a href="#" class="remove-cover">Remove</a></li>
                </div>
            </div>
            <div class="profile_image um-profile-photo" data-user_id="<?php echo um_profile_id(); ?>" style="width: 100%;float: none;">
                <span class="um-profile-photo-img" style="display: block!important;">
                    <?php echo get_avatar($user_id, 190);  ?>
                </span>
                
                <?php
                if ($user_id==wp_get_current_user()->ID && !isset(UM()->user()->cannot_edit)) {
                    UM()->fields()->add_hidden_field('profile_photo');
                    if (!um_profile('profile_photo')) { // has profile photo
                        $items = array(
                            '<a href="#" class="um-manual-trigger" data-parent=".um-profile-photo" data-child=".um-btn-auto-width">' . __('Upload photo', 'ultimate-member') . '</a>',
                            '<a href="#" class="capture_photo" data-user_id="' . um_profile_id() . '" data-default_src="' . um_get_default_avatar_uri() . '">' . __('Capture photo', 'ultimate-member') . '</a>',
                            '<a href="#" class="um-dropdown-hide">' . __('Cancel', 'ultimate-member') . '</a>',
                            
                        );
                        $items = apply_filters('um_user_photo_menu_view', $items);
                        echo UM()->profile()->new_ui('bc', 'div.um-profile-photo', 'click', $items);
                    } else {
                        $items = array(
                            '<a href="#" class="um-manual-trigger" data-parent=".um-profile-photo" data-child=".um-btn-auto-width">' . __('Change photo', 'ultimate-member') . '</a>',
                            '<a href="#" class="um-reset-profile-photo" data-user_id="' . um_profile_id() . '" data-default_src="' . um_get_default_avatar_uri() . '">' . __('Remove photo', 'ultimate-member') . '</a>',
                            '<a href="#" class="capture_photo" data-user_id="' . um_profile_id() . '" data-default_src="' . um_get_default_avatar_uri() . '">' . __('Capture photo', 'ultimate-member') . '</a>',
                            '<a href="#" class="um-dropdown-hide">' . __('Cancel', 'ultimate-member') . '</a>',
                        );
                        $items = apply_filters('um_user_photo_menu_edit', $items);
                        echo UM()->profile()->new_ui('bc', 'div.um-profile-photo', 'click', $items);
                    }
                }
                ?>
            </div>
        </div>
        <div class="profile_bio">
            <h3><?php echo $display_name; ?></h3>
        </div>
    </div>

    <?php if (!empty($user_location) && $location_visiable) { ?>
        <div class="mobile-user-location">
            <a target="_blank" href="<?php echo $map_url; ?>">
                <div class="map_loc">
                    <i class="fas fa-map-marker-alt"></i>
                    <p><?php echo $user_location; ?></p>
                </div>
            </a>
        </div>
    <?php } ?>
</div>

<div class="timeline_for_desktop mytimelineDesktop">
    <div class="section1">
        <div class="row">
            <div class="col-md-12">
                <div class="sectit">
                    <h3><?php echo $display_name; ?></h3>
                </div>
            </div>
        </div>
    </div>
    <div data-elementor-type="wp-page" data-elementor-id="22017" class="elementor elementor-22017">
        <div class="elementor-inner">
            <section class="elementor-element elementor-element-ca67378 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="ca67378" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row elementor-rowsec">
                        <div class="elementor-element elementor-element-4d74689 elementor-column elementor-col-50 elementor-top-column " data-id="4d74689" data-element_type="column">
                            <div class="elementor-element-populatedsec pro_photo_bio_sec">
                                <div class="elementor-widget-wrap-sec2">
                                    <div class="elementor-element elementor-element-8e28a31 elementor-widget elementor-widget-html" data-id="8e28a31" data-element_type="widget" data-widget_type="html.default">
                                        <div class="elementor-widget-container">
                                            <div class="timelineprofilepic">
                                                <div class="um-profile-photo" data-user_id="<?php echo um_profile_id(); ?>">
                                                    <span class="um-profile-photo-img" style="display: block!important;">
                                                        <?php echo get_avatar($user_id, 150);  ?>
                                                        <?php if (is_user_logged_in() and get_current_user_id() == $user_id) { ?>
                                                            <div class="updateprofilepic">
                                                                <i class="fa fa-picture-o"></i> <a onclick="$('.um-profile-photo-img').click();">Update Profile</a>
                                                            </div>
                                                        <?php  } ?>
                                                    
                                                    </span>
                                                    <?php
                                                    if($user_id==wp_get_current_user()->ID){
                                                        if (!isset(UM()->user()->cannot_edit)) {
                                                            UM()->fields()->add_hidden_field('profile_photo');
                                                            if (!um_profile('profile_photo')) { // has profile photo
                                                                $items = array(
                                                                    '<a href="#" class="um-manual-trigger" data-parent=".um-profile-photo" data-child=".um-btn-auto-width">' . __('Upload photo', 'ultimate-member') . '</a>',
                                                                    '<a href="#" class="um-dropdown-hide">' . __('Cancel', 'ultimate-member') . '</a>',
                                                                );
                                                                $items = apply_filters('um_user_photo_menu_view', $items);
                                                                echo UM()->profile()->new_ui('bc', 'div.um-profile-photo', 'click', $items);
                                                            } else {
                                                                $items = array(
                                                                    '<a href="#" class="um-manual-trigger" data-parent=".um-profile-photo" data-child=".um-btn-auto-width">' . __('Change photo', 'ultimate-member') . '</a>',
                                                                    '<a href="#" class="um-reset-profile-photo" data-user_id="' . um_profile_id() . '" data-default_src="' . um_get_default_avatar_uri() . '">' . __('Remove photo', 'ultimate-member') . '</a>',
                                                                    '<a href="#" class="um-dropdown-hide">' . __('Cancel', 'ultimate-member') . '</a>',
                                                                );
                                                                $items = apply_filters('um_user_photo_menu_edit', $items);
                                                                echo UM()->profile()->new_ui('bc', 'div.um-profile-photo', 'click', $items);
                                                            }
                                                        }
                                                    }
                                                    
                                                    ?>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="elementor-element elementor-element-e634715 zshow elementor-widget elementor-widget-text-editor" data-id="e634715" data-element_type="widget" data-widget_type="text-editor.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-text-editor elementor-clearfix  custom_height_bio">
                                                <p class="pro_name_caps">
                                                    <?php echo $display_name; ?><br>
                                                    <?php
                                                    if (!empty($gender)) {
                                                    ?>
                                                       <div style="display: none;">
                                                        <strong>Sex: </strong>
                                                        <?php echo  $gender  ?>
                                                       </div> 
                                                        <br>
                                                    <?php } ?>
                                                    <?php if (!empty($bio)) { ?>
                                                        <div class="more">
                                                            <?php echo  $bio  ?><br>
                                                        </div>
                                                    <?php } ?>
                                                    
                                                </p>




                                                <?php if (is_user_logged_in() and get_current_user_id() == $user_id) { ?>
                                                    <div class="editprofile"><a href="https://www.travpart.com/English/user/<?php echo $username ?>/?profiletab=profile">Edit Profile</a>

                                                    </div>
                                                <?php  } ?>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-5e67e44 elementor-column elementor-col-50 elementor-top-column" data-id="5e67e44" data-element_type="column">
                            <div class="elementor-column-wrapsec">
                                <div class="elementor-widget-wrap">
                                    <div class="elementor-element custom_widas elementor-element-2789d29 elementor-widget elementor-widget-html" data-id="2789d29" data-element_type="widget" data-widget_type="html.default">
                                        <div class="elementor-widget-container">

                                            <!-- Start Time Line Cover page  -->
                                            <div class="timelinecover">
                                                <a href="#">
                                                    <img class="coverIm" src="<?php echo $cover_img; ?>">
                                                </a>

                                                    
                                                    <!--<button class="btn_profile_dots btn_dots"><i class="fa fa-square" aria-hidden="true"></i><i class="fa fa-square" aria-hidden="true"></i><i class="fa fa-square" aria-hidden="true"></i></button>
                                                    <!--<button class="btn_profile_con btn_connect <?php echo $connect_button; ?>"><i class="fas fa-user-plus"></i><a href="#" style="color:white" class="p-connect-area-text" data-connect_friend="<?php echo $button_data; ?>"><?php echo $button_text; ?></a></button>-->
                                                    
                                                    
                                                    <div class="dots_box dots_arrow_top">
                                                        <span class="report_Click-here">Report Profile</span>
                                                        <span class="block-user" userid="<?php echo $user_id; ?>">
                                                            <?php echo $block_text; ?>
                                                        </span>
                                                    </div>
                                                    <div class="report_custom-model-main">
                                                        <div class="report_custom-model-inner"> 
                                                        <span class="report_heading">Please Select</span>       
                                                        <div class="report_close-btn">×</div>
                                                            <div class="report_custom-model-wrap">
                                                                <div class="report_pop-up-content-wrap">
                                                                    <img src="https://www.travpart.com/wp-content/uploads/2020/04/pro_report.png">
                                                                    <span class="report_heading_content">You can report the profile after selecting a problem</span>
                                                                    <div class="report_input_btn">
                                                                        <button class="report_input_btn_inappro">Posting inappropriate things</button>
                                                                        <button class="report_input_btn_fake">Fake account</button>
                                                                        <input type="hidden" value="" />
                                                                    </div>
                                                                    <div class="report_input_other_btn">
                                                                        <span>Others:</span>
                                                                        <input type="text" \>
                                                                    </div>
                                                                    <div class="report_send_btn">
                                                                        <input userid="<?php echo $user_id; ?>" type="submit" value="Send">
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                        </div> 
                                                    </div> 
                                                <!--<?php if (is_user_logged_in() and get_current_user_id() != $user_id) { ?>
     <div class="follow-area-top">
        <i class="fas fa-wifi"></i><a href="#" class="follow_link_top">
        Follow</a>
    </div>
    
    <div class="mess-area">
        <a href="https://www.travpart.com/English/travchat/user/"><i class="far fa-comment-alt"></i>
        Messages</a>
    </div>
    <?php  } ?>-->

                                                <!-- Update Cover Pic -->
                                                <?php if (is_user_logged_in() and get_current_user_id() == $user_id) { ?>
                                                    <div <?php echo(empty($cover_img)?'style="bottom:60px;"':''); ?> class="updatecoverpic dropdown-toggle updatecoverp_desk" data-toggle="dropdown-menu" aria-expanded="true">
                                                        <i class="fas fa-camera"></i> Update Cover Photo
                                                    </div> 
                                                <?php  } ?>


                                                <div id="PostDId" class="updateCP dropdown-menu" role="menu" aria-labelledby="updateC">
                                                    <li><a href="#" class="upload-cover">Select Photo</a></li>
                                                    <li><a href="#" class="upload-cover upload-direct">Upload</a></li>
                                                    <?php if(!empty($cover_img)){ ?>
                                                        <li>
                                                            <a href="#" class="remove-cover">  Remove</a>
                                                        </li>
                                                    <?php } ?>
                                                </div>
                                                <!-- End Update Cover Pic -->
                                                <script type="text/javascript">
                                                    jQuery(document).ready(function($) {
                                                    
                                                        var website_url ="https://www.travpart.com/English";
                                                        var clicked = false;
                                                        $("#fellow_press").on('click', function() {
                                                            var txt = $(this).find('.tx').text();
                                                            if(txt=="Follow"){
                                                                $("#fellow_press").css('background','#dad8d8');
                                                                $("#fellow_press a,#fellow_press").css('color','#000');
                                                                $("#fellow_press").html('<i class="fas fa-wifi"></i><span class="tx">Following</span>');
                                                            }
                                                            if(txt=="Following"){
                                                                $("#fellow_press").css('background','#008a66');
                                                                $("#fellow_press a,#fellow_press").css('color','#fff');
                                                                $("#fellow_press").html('<i class="fas fa-wifi"></i><span class="tx">Follow</span>');
                                                            }
                                                            var user_id = <?php echo $user_id  ?>;
                                                            if (clicked) {
                                                                return;
                                                            }
                                                            clicked = true;
                                                            $.ajax({
                                                                type: "POST",
                                                                url: website_url + '/submit-ticket/',
                                                                data: {
                                                                    user_id: user_id,
                                                                    action: 'social_follow'
                                                                },
                                                                success: function(data) {
                                                                    var result = JSON.parse(data);
                                                                    if (result.status) {} else {
                                                                        if (result.action == 'unfollow') {
                                                                            $("#fellow_press").css('background','#008a66');
                                                                            $("#fellow_press a,#fellow_press").css('color','#fff');
                                                                            $("#fellow_press").html('<i class="fas fa-wifi"></i><span class="tx">Follow</span>');
                                                                        } else if (result.action == 'follow') {
                                                                            $("#fellow_press").css('background','#dad8d8');
                                                                            $("#fellow_press a,#fellow_press").css('color','#000');
                                                                            $("#fellow_press").html('<i class="fas fa-wifi"></i><span class="tx">Following</span>');
                                                                        }
                                                                    }
                                                                    //$('.btn_follow').removeAttr('style');
                                                                    clicked = false;
                                                                }
                                                            });
                                                        });

                                                        $("#fellow_press_m").on('click', function() {
                                                            var website_url ="https://www.travpart.com/English";
                                                            var user_id = <?php echo $user_id  ?>;
                                                            if (clicked) {
                                                                return;
                                                            }
                                                            clicked = true;
                                                            $.ajax({
                                                                type: "POST",
                                                                url: website_url + '/submit-ticket/',
                                                                data: {
                                                                    user_id: user_id,
                                                                    action: 'social_follow'
                                                                },
                                                                success: function(data) {
                                                                    var result = JSON.parse(data);
                                                                    if (result.status) {} else {
                                                                        if (result.action == 'unfollow') {
                                                                            $("#fellow_press_m a").text("Follow");

                                                                        } else if (result.action == 'follow') {
                                                                            $("#fellow_press_m a").text("Following");
                                                                        }
                                                                    }
                                                                    $('.btn_follow').removeAttr('style');
                                                                    clicked = false;
                                                                }
                                                            });
                                                        });
                                                    });
                                                </script>
                                                <div class="verifiedseller">
                                                    <?php
                                                    if (empty($verified))
                                                        echo "Unverified " . $role;
                                                    else if ($verified == 1)
                                                        echo 'Pending ';
                                                    else if ($verified == 2)
                                                        echo " Verified " . $role;
                                                    else
                                                        echo 'Unverified ' . $role;
                                                    ?>
                                                </div>
                                            </div>
                                            
                                            <?php if($page_name=='my-connection' & get_current_user_id() == $user_id  & $_GET['debug']==1){ ?>
                                                <div class="connection_main_user">
                                                    <div class="btns_divs">
                                                        <div class="btn-1">
                                                            <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo esc_html($username); ?>" class="btn btn-default">
                                                                Timeline
                                                            </a>
                                                        </div>
                                                        <div class="btn-2">
                                                            <button class="btn btn-default">
                                                                Photos & Videos
                                                            </button>
                                                        </div>
                                                        <div class="btn-3">
                                                            <button class="btn connections">
                                                                Connections
                                                            </button>
                                                        </div>
                                                        <div class="btn-4">
                                                            <button class="btn btn-default">
                                                                Profile Page
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            
                                            <!-- End Time Line Cover page -->

                                            <!-- TimeLine Popup -->
                                            <div class="CoverPage">
                                                <div class="cover-close-m">
                                                    <a href="#"><i class="fas fa-times"></i></a>
                                                </div>

                                                <div class="CoverPic">
                                                    <div class="edit_cover_header">
                                                        <div class="Cheader-left">
                                                            <h4>Cover Photos</h4>
                                                        </div>

                                                        <img src="<?php echo $cover_img; ?>">
                                                        
                                                        <div class="coverP-bottom">
                                                            <div class="thumbs-up">
                                                                <a href="#">
                                                                    <i class="far fa-thumbs-up"></i>
                                                                    <span>Like</span>
                                                                </a>
                                                            </div>

                                                            <div class="thumbs-up">
                                                                <a href="#">
                                                                    <i class="far fa-comment-alt"></i>
                                                                    <span>Comment</span>
                                                                </a>
                                                            </div>

                                                            <div class="thumbs-up">
                                                                <a href="#">
                                                                    <i class="fas fa-share"></i>
                                                                    <span>Share</span>
                                                                </a>
                                                            </div>

                                                            <div class="bottom-right-txt">
                                                                <a href="#">Tag Photo</a>
                                                                <a href="#">Options</a>
                                                                <a href="#">Send in Messenger</a>
                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="coverP-right">
                                                        <div class="cover-user-r">
                                                            <?php echo get_avatar($user_id, 40);  ?>
                                                            <a href="#"><?php echo $display_name; ?></a>
                                                            <div class="C-date">
                                                                <a href="#"><?php echo empty($cover_update_time)?'':date('M d,Y',$cover_update_time) ?></a>
                                                                <i class="fas fa-globe-europe"></i>
                                                            </div>
                                                        </div>

                                                        <div class="Coverdots-right">
                                                            <i class="fas fa-ellipsis-h"></i>
                                                        </div>
                                                        <div class="row ttp">
                                                                <div class="col-md-6 pd_w">
                                                                    <i class="far fa-thumbs-up"></i> 5
                                                                </div>
                                                                <div class="col-md-6 pd_w text-right">
                                                                    Comments <?php echo count($cover_comments); ?>
                                                                </div>
                                                            </div>
                                                        <div class="Csocial-area-righ">
                                                            
                                                            <div class="thumbs-up CsocialThumbs">
                                                                <a href="#">
                                                                    <i class="far fa-thumbs-up"></i>
                                                                    <span>Like</span>
                                                                </a>
                                                            </div>

                                                            <div class="thumbs-up CsocialThumbs">
                                                                <a href="#">
                                                                    <i class="far fa-comment-alt"></i>
                                                                    <span>Comment</span>
                                                                </a>
                                                            </div>

                                                            <div class="thumbs-up CsocialThumbs">
                                                                <a href="#">
                                                                    <i class="fas fa-share"></i>
                                                                    <span>Share</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="comment_box">
                                                            <?php foreach($cover_comments as $c) { ?>
                                                            <div class="comment_list">
                                                                <div class="img_user">
                                                                    <img style="height:40px;" src="<?php echo um_get_user_avatar_data($c->commentator)['url']; ?>"/>
                                                                </div>
                                                                <div class="user_content">
                                                                    <a href="#"><?php echo um_get_display_name($c->commentator); ?></a>
                                                                    <p><?php echo $c->comment; ?></p>
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                        
                                                        
                                                        <div class="cCovercommentR user-cover-comment" owner="<?php echo $user_id; ?>" c_name="<?php echo um_get_display_name(wp_get_current_user()->ID); ?>">
                                                            <?php echo um_get_avatar('', wp_get_current_user()->ID, 40); ?>
                                                            <input placeholder="Write a Comment" type="text" required="">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End TimeLine Popup -->

                                            <script>
                                                jQuery(document).ready(function($) {
                                                    var sented = false;
                                                //   $(".connect_button").one('click', function() {
                                                    var website_url ="https://www.travpart.com/English";    
                                                    jQuery(document).one("click",".connect_button",function(){
                                                       
                                                        $(".connect_button").addClass('connect_button_sent');
                                                        $("#plus").addClass('fa-user-minus');
                                                        $("#plus").removeClass("fa-user-plus");
                                                        $(".p-connect-area-text").html('<div class="col-md-99 rm_d"><a href="#" style="color:white">Request <br> sent</a></div>');
                                                        var user_id = <?php echo $user_id  ?>;
                                                        if (sented) {
                                                            return;
                                                        }
                                                        sented = true;
                                                        $.ajax({
                                                            type: "POST",
                                                            url: website_url + '/submit-ticket/',
                                                            data: {
                                                                friend_id: user_id,
                                                                action: 'connect_friend'
                                                            }, // serializes the form's elements.
                                                            success: function(data) {
                                                                var result = JSON.parse(data);
                                                                if (result.status) {
                                                                    $(".connect_button").addClass('connect_button_sent');
                                                                    $("#plus").addClass('fa-user-minus');
                                                                    $("#plus").removeClass("fa-user-plus");
                                                                    $(".p-connect-area-text").html('<div class="col-md-99 rm_d"><a href="#" style="color:white">Request <br> sent</a></div>');
                                                                    //alert(result.message);
                                                                    //window.location.reload()// show response from the php script.
                                                                } else {
                                                                    //alert(result.message);
                                                                    location.reload();
                                                                    //window.location.reload()// show response from the php script.
                                                                }
                                                            }
                                                        });
                                                    });
                                                });
                                            </script>

                                        </div>
                                        <div class="visitor_btn_container">
                                            <div class="custom_viewp_btn">
                                                <?php
                                                if (is_user_logged_in()) {
                                                    if ($user_id == get_current_user_id()) {
                                                    } else {
                                                        echo '<a target="_blank" class="btn btn-info btn_sze" href="https://www.travpart.com/English/user/' . $username . '/?profiletab=profile"><i class="fas fa-eye"></i> View Profile</a>';  
                                                    }
                                                }
                                                ?>
                        
                                            </div>
                                            
                                            
                                            <div class="timeline_con_btn">
                                                <?php 
                                                
                                                if (is_user_logged_in() and get_current_user_id() != $user_id and  get_page_template_slug()!="tpl-mytimeline.php") { ?>
                                                <div class="time_btn_connec_page_dis">
                                                    <div class="btn_time">
                                                        <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo esc_html($username); ?>"><i class="far fa-clock"></i><span class="button-text">TIMELINE</span></a>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                            

                                            
                                            
                                                <div class="user-location">
                                                    <?php if (is_user_logged_in() and get_current_user_id() != $user_id) {  ?>
                                                    <?php if (!empty($user_location) && $location_visiable) { ?>
                                                        <p>
                                                            <a target="_blank" href="<?php echo $map_url; ?>">
                                                                <i class="fas fa-map-marker-alt"></i>
                                                                <?php echo $user_location; ?>
                                                            </a>
                                                        </p>
                                                        <a class="btn btn-small report_btn" id="myBtn">
                                                            <i class="fas fa-flag"></i> Report
                                                        </a>
                                                    <?php } } ?>
                                                 </div>
                                            
                                           
                                            
                                                <div class="visitor_follow_btn">
                                                    <?php if (is_user_logged_in() and get_current_user_id() != $user_id) { ?>
                                                <?php if ($role == 'Seller' && $Data->status != 1) { ?>
                                                        <button id="fellow_press" class="btn_follow" <?php echo(($fellow=="Following")?"style='background:rgb(218, 216, 216);color:#000'":""); ?>>
                                                            <i class="fas fa-wifi"></i><a> 
                                                            <span class="tx"><?php echo $fellow; ?></span>
                                                        </button>
                                                        <?php } ?>      
                                                <?php } ?>
                                                 </div> 
                                                
                                            
                                               
                                            
                                            <?php if (is_user_logged_in() AND get_current_user_id() != $user_id) {
                                                if(empty($check_block)){
                                                  ?>
                                                
                                            <div class="visitor_message_btn">
                                                <div class="msg_dots_display">
                                                    <button class="btn_profile btn_messages"><a target="_blank" style="color: white" href="<?php echo home_url('travchat/user/addnewmember.php?user='.$travpart_user_id) ?>"><i class="far fa-comment-alt"></i> Messages</a></button>
                                                    <button class="p-dots-area_1">
                                                    <a class="dropdown-toggle" id="dropdownMenuButton_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      <i class="fas fa-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdownMenuButton_1" aria-labelledby="dropdownMenuButton_1">
                                                          <?php if( $button_text=='Blocked' ){ ?>
                                                        <a  class="dropdown-item block_method" href="#" data-toggle="modal" id="<?php echo $user_id; ?>">UnBlock</a>
                                                    <?php }else{?>
                                                        <a  class="dropdown-item block_method" href="#" data-toggle="modal" id="<?php echo $user_id; ?>">Block</a>
                                                   <?php  } ?>
                                                        <a  class="dropdown-item" href="#" data-toggle="modal" data-target="#ReportModalCenter" id='report'>Report</a>
                                                      <?php if( $button_text=='Connected' ){ ?>
                                                      <a id='unconnect' class="dropdown-item connect_button_connected" href="#">Unconnected</a>
                                                    <?php } ?>
                                                    </div>
                                                  </button>
                                                </div>
                                            </div>
                                            <?php }} ?>
                                            
                                            <?php if($Data->status==2 ){ ?>
                                                <div class="visitor_con_btn">
                                                <button class="btn_block">
                                                    <div class="col-md-33 rm_d">
                                                          <i class="fas fa-ban"></i>
                                                    </div>
                                                    <div class="col-md-99 rm_d">
                                                        <a href="#" class="p-connect-area-text" data-connect_friend="<?php echo $button_data; ?>">
                                                            Blocked
                                                        </a>
                                                    </div>
                                                </button>
                                            </div>
                                            <?php } ?>
                                            <?php 
                                               if (is_user_logged_in() and get_current_user_id() != $user_id and $Data->status!=2) { 
                                                if(empty($check_block)){?>
                                                <div class="visitor_con_btn">
                                                <button class="btn_profile_con btn_connect <?php echo $connect_button; ?>">
                                                    <div class="col-md-33 rm_d">
                                                        <?php if(empty($Data)){ ?>
                                                            <i class="fas fa-user-plus" id="plus"></i>
                                                        <?php }elseif($Data->status == 0){ ?>
                                                            <i class="fas fa-user-minus" id="minus"></i>   
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-99 rm_d">
                                                        <a href="#" style="color:white" class="p-connect-area-text" data-connect_friend="<?php echo $button_data; ?>">
                                                            <?php echo $button_text; ?>
                                                        </a>
                                                    </div>
                                                </button>
                                            </div>
                                            <?php }} ?>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>

<!-- Button trigger modal -->

<!-- Modal -->
<!-- The Modal -->
<div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content eddit_re">
        <div class="modal-header modal-header-custom">
            <h4 class="modal-title">Report User</h4>
            <button type="button" class="close_btn">
                <i class="fas fa-times"></i>
            </button>
        </div>
        <div class="modal-body font_s">
            <p><strong>Why would you like to report user??</strong></p>
            <p class="anonymous">Your Report will be keep anonymous</p>
            <div class="space_top">
                <div class="radio">
                    <label>
                        <input type="radio" name="report_v" value="1" />
                        The User's profile is misleading.
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="report_v" value="2" />
                        The User's Content and/or behaviour is inappropriate
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="report_v" value="3" />
                        The User's sends me spam messages
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" value="0" name="report_v" />
                        Other
                    </label>
                </div>
            </div>
            <div class="other_message_text">
                <textarea class="form-control" rows="6" id="text_area_comment"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success close_btn">Submit</button>
        </div>
    </div><!-- /.modal-content -->
</div>
<?php //} 
?>

<script type="text/javascript">
    jQuery(document).ready(function($) {


        $('input[type=radio][name=report_v]').change(function() {
            if (this.value == 0) {
                $('.other_message_text').show();
            } else {
                $('.other_message_text').hide();
            }
        });

        $(".follow-area-top").click(function() {
            var x = $('.follow_link_top').text();
            x = $.trim(x);
            if (x == 'Follow') {
                alert('You have followed successfully');
                $(this).find('a').text('Unfollow');
            } else {
                alert('You have unfollowed successfully');
                $(this).find('a').text('Follow');
            }
        });
    });
</script>
<style type="text/css">
    #capt_camera{
        display: none;
    }
    
    .capture_photo{
        display: none !important;
        text-decoration: none;
    }
    .updatecoverp_desk{
        top:auto !important;
        bottom: 30px;
        right: 40px;
        background: #fff;
        padding: 7px;
        color: black;
        font-size: 13px;
        font-weight: 600;
        box-shadow: none;
        border-radius: 5px;
    }
    .updatecoverp_desk i{
        font-size: 15px;
        padding: 4px;
    }
    .visitor_btn_container{
        width: 100%;
        padding-top: 20px;
        padding-bottom: 10px;
    }
    .custom_viewp_btn,.timeline_con_btn,.user-location,.visitor_follow_btn,.visitor_con_btn,.visitor_message_btn {
        display: inline-block;
        vertical-align: middle;
    }
    .custom_viewp_btn{
        width: 17.5%;
    }
    .timeline_con_btn {
        width: 16.5%;
    }
    .user-location{
        width: 17.8%;
    }
    .visitor_follow_btn{
      width: 15.2%;  
    }
    .visitor_con_btn {
        width: 15.1%;
    }
    .visitor_message_btn {
        width: 16.4%;
        float: right;
    }
    .custom_viewp_btn{
        padding: 0px;
    }
    .msg_dots_display{
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        position: relative;
    }

    .btn_profile {
        position: relative;
        padding: 11px 13px;
        border-radius: 5px 0px 0px 5px !important;
    }
    .btn_messages i, .btn_follow i, .btn_connect  i{
        text-rendering: optimizeLegibility;
    }
    .btn_messages {
        top: 0px;
        left: 0px;
        vertical-align: middle;
    }
    .btn_time a{
        background-color: #28a745;
        font-family: "Roboto", Sans-serif;
        font-weight: 600;
        font-size: 15px;
        padding: 6.5px 19px;
        color: #fff;
        text-decoration: none;
        display: inline-block;
        border-radius: 3px;
    }
    .btn_time a i{
        padding-right: 5px;
        font-size: 16px;
    }
    .btn_time a span.button-text{
        font-size: 18px;
    }
    .user-location {
        margin-top: 0px; 
        text-align: center;
    }
    .user-location p a {
        font-size: 12px;
    }
 .rm_d{
    padding: 0px !important;
       display: inline-block;
    vertical-align: middle;
 }
 .connect_button_sent {
    background: #dad8d8 !important;
    color: #000 !important;
    font-weight: bold;
    line-height: 10px;
}
 
 .connect_button_sent a{
    text-decoration: none;
    color: #000 !important;
 }
    .space_top {
        margin-top: 25px;
    }

    .font_s {
        font-size: 12px;
        margin-top: 5px;
        letter-spacing: 1px;
    }

    .radio {
        margin-top: 10px !important;
    }

    button.btn.btn-success.close_btn {
        font-size: 15px;
        padding: 5px 15px;
        letter-spacing: 1px;
    }

    .anonymous {
        font-weight: 400;
        color: #999;
        margin-top: 5px;
    }

    .modal-header-custom {
        background: #cccccc2e;
    }

    .other_message_text {
        display: none;
    }

    .eddit_re input[type=radio] {
        margin: 2px 2px 2px -20px !important;
    }

    .report_btn i {
        color: #f77b7bfa;
        margin-right: 3px;
    }

    .report_btn {
        font-size: 10px;
        background: #cccccc73;
        letter-spacing: 1px;
        padding: 0px 5px;
    }

    .report_btn:hover {
        background: #ccc !important;
    }

    /*
    Add cover image change css
*/

    .cCovercommentR {
        background: #F2F3F5;
    }

    .cCovercommentR img {
        width: 35px !important;
        border-radius: 50% !important;
        display: inline-block !important;
        height: 35px !important;
        left: inherit !important;
        margin-left: 15px;
        position: inherit !important;
        margin: 5px 10px;
        margin-right: 5px;
    }

    .cCovercommentR input {
        width: 82%;
        border-radius: 5px;
        border: none;
        background-color: #fff;
        border: 1px solid #ccd0d5;
        border-radius: 16px;
        padding: 5px 15px;
        font-size: 14px;
        position: relative;
        top: 2px;
    }

    .cCovercommentR input:focus {
        outline: none;
    }
    .ttp{
        margin-top: 50px;
        font-size:12px;
        color: #8e8989;
        margin-bottom: 5px;
    }
    .comment_box{
        padding:10px;
    }
    .img_user {
        width: 10%;
        display: inline-block;
        vertical-align: middle;
    }
    .img_user img{
        border-radius: 100%;
        width: auto !important;
        position: inherit !important;
    }
    .user_content{
        width: 85%;
        padding: 10px;
        display: inline-block;
        vertical-align: middle;
    }
    .user_content a{
        font-size: 12px;
        text-decoration: none;
        color: #116fee
    }
    .user_content p{
        font-size: 11px;
        margin: 0px;
    }
    .pd_w{
        padding: 0px 25px;
    }
    .ttp i{
        color:#7f7ffb;
    }
    
    .Csocial-area-righ {
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }

    .CsocialThumbs {
        padding: 10px 19px !important;
    }

    .CsocialThumbs a {
        color: #616161 !important;
    }

    .cover-user-r {
        position: relative;
    }

    .C-date a {
        font-size: 12px !important;
        position: absolute !important;
        top: 34px !important;
        left: 18% !important;
        color: #90949c !important;
    }

    .C-date i {
        font-size: 13px !important;
        position: relative;
        top: -12px !important;
        left: 45% !important;
        color: #90949c !important;
    }

    .Coverdots-right {
        position: absolute;
        right: 0;
        top: 0px;
        padding: 10px;
    }

    .Coverdots-right i {
        color: #8c8c8c;
        font-size: 15px;
        cursor: pointer;
    }

    .cover-user-r img {
        width: 40px !important;
        border-radius: 50% !important;
        display: inline-block !important;
        height: 40px !important;
        left: inherit !important;
        margin-top: 10px;
        margin-left: 15px;
        position: inherit !important;
    }

    .cover-user-r a {
        font-size: 18px;
        margin-left: 10px;
        position: relative;
        top: 3px;
        text-decoration: none;
        color: #385898;
        font-weight: bold;
    }

    .coverP-right {
        float: right;
        width: 26.7%;
        overflow: hidden;
    }

    .cover-user-r img {
        width: 40px;
        border-radius: 50%;
        display: inline-block;
    }

    .cover-close-m {
        position: fixed;
        z-index: 999999;
        right: 2%;
        top: 2%;
    }

    .cover-close-m i {
        font-size: 20px;
        color: #777;
    }

    .bottom-right-txt a {
        display: inline-block;
        text-align: right;
        color: #bbb;
        margin: 10px;
        font-size: 12px;
    }
    .bottom-right-txt a:hover{
        color: #fff;
    }

    .bottom-right-txt {
        display: inline-block;
        width: 69%;
        text-align: right;
    }

    .thumbs-up {
        padding: 10px;
        display: inline-block;
    }

    .coverP-bottom {
        position: absolute;
        bottom: 0;
        width: 73.3%;
        background:#131212ba;
    }

    .thumbs-up a {
        color: #bbb;
        font-size: 12px;
    }
    .thumbs-up a:hover{
        color: #fff;
    }
    .thumbs-up i {
        margin-right: 4px !important;

    }

    .CoverPic img {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        height: 100%;
        cursor: pointer;
        width: 73.3%;
    }

    .CoverPage {
        display: none;
    }

    .Cheader-left h4 {
        color: #fff;
        position: absolute;
        z-index: 9999;
        font-size: 26px;
        left: 30px;
        top: 15px;
    }

    .CoverPic {
        position: fixed;
        top: 10%;
        width: 90%;
        background: #fff;
        height: 80%;
        margin: 0px auto;
        display: block;
        z-index: 999999;
        left: 0;
        box-shadow: 0 2px 26px rgba(0, 0, 0, .3), 0 0 0 1px rgba(0, 0, 0, .1);
        left: 5%;
    }

    .CoverPage:after {
        content: "";
        width: 100%;
        height: 100%;
        bottom: 0;
        left: 0;
        position: fixed;
        right: 0;
        top: 0;
        background-color: rgba(0, 0, 0, .9);
        z-index: 99999;
    }

    .Cphotohide {
        display: block;
    }

    /*
    End Add cover image change css
*/



    /*
    Update Cover photo dropdown Css
*/

    .updateCP {
        position: absolute;
        background-color: white !important;
        min-width: 135px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1000 !important;
        border: 1px solid #ddd;
        margin-top: 4px;
        top: 158px;
        right: 42px;
        left: inherit;
    }

    .updateCP::before {
        content: "";
        position: absolute;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-top: 9px solid #ffffff;
        bottom: -9px;
        right: 5px;
        z-index: 9999;
    }

    .updateCP::after {
        content: "";
        position: absolute;
        width: 10px;
        height: 10px;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-top: 8px solid #ddd;
        bottom: -10px;
        right: 5px;
    }

    .updateCP li {
        display: block;
        margin: 5px;
    }

    .updateCP li a {
        padding: 3px;
        color: #444;
        text-decoration: none;
        font-size: 15px;
    }

    .updateCP li:hover a {
        color: #fff;
    }

    .updateCP li:hover {
        background: #1ABC9C;
        color: #fff;
        cursor: pointer;
    }

    /*
    End Update Cover photo dropdown Css
*/

    .sectit {
        background-color: #1abc9c;
        text-align: center;

    }

    p.pro_name_caps {
        text-transform: capitalize;
    }

    .profile_bio h3 {
        text-transform: capitalize;
    }

    .sectit h3 {
        color: #fff;
        font-family: "Roboto", Sans-serif;
        font-size: 20px;
        padding: 17px 0;
        border-bottom: 1px solid #000;
        margin: 0;
        text-transform: capitalize
    }

    .elementor-element.elementor-element-4d74689.elementor-column.elementor-col-50.elementor-top-column {
        width: 20%;
        display: flex;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        position: relative;
        min-height: 1px;
        border-right: 1px solid #000;
    }

    .elementor-element.elementor-element-5e67e44.elementor-column.elementor-col-50.elementor-top-column {
        width: 80%;
        display: flex;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        position: relative;
        min-height: 1px;
    }

    .elementor.elementor-22017 {
        border-bottom: 1px solid #000;
        background: #fff;
    }

    .elementor-row.elementor-rowsec {
        //width: 100%;
        display: -webkit-box;   /* OLD - iOS 6-, Safari 3.1-6, BB7 */
        display: -ms-flexbox;  /* TWEENER - IE 10 */
        display: -webkit-flex; /* NEW - Safari 6.1+. iOS 7.1+, BB10 */
        display: flex;         /* NEW, Spec - Firefox, Chrome, Opera */
    }

    .elementor-element-populatedsec {
        padding: 0px;
    }

    .elementor-widget-wrap-sec2 {
        padding: 10px;
    }
    .pro_photo_bio_sec{
        background-color: lightgrey;
        width: 100%;
    }

    .elementor-column-wrapsec {
        padding: 10px;
        width: 100%;
    }
    .custom_widas{      
        width: 100%;
    }

    .elementor:not(.elementor-bc-flex-widget) .elementor-widget-wrap {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }

    .follow-area-top {
        position: absolute;
        top: 5px;
        background: #cacaca;
        left: 85px;
        border-radius: 15px;
        padding: 5px 13px;

    }

    .follow-area-top a {
        color: #000;

    }


    .follow-area-top:hover {
        background: #1abc9c;
        cursor: pointer;
    }


    .mess-area {
        position: absolute;
        top: 5px;
        background: #cacaca;
        padding: 5px 13px;
        left: 5px;
        border-radius: 15px;
    }

    .mess-area a {
        color: #000;
    }

    .mess-area:hover {
        background: #1abc9c;
        cursor: pointer;
    }

    i.fa-wifi {
        transform: rotate(55deg);
        position: relative;
        right: 3px;
        top: 0px;
    }

    .elementor-text-editor.elementor-clearfix p {
        font-size: 19px;
    }


    .user-location i.fa-map-marker-alt {
        color: #1abc9c;
    }

    .user-location p a {
        text-decoration: none;
        color: #000;
    }
    .media-frame-title {
        text-align: center;
    }
    .media-modal-content .media-frame select.attachment-filters{
        margin-left: 10px;
    }
    h2.media-frame-actions-heading.screen-reader-text {
        padding: 10px;
    }
    .media-modal-close span.media-modal-icon span.screen-reader-text{
        display: none;
    }

    @media (max-width: 600px) {
        .updateCP{
            top:35px;
            right: 15px;
        }
        .updateCP::before{
            border-bottom: 9px solid #ffffff;
            border-top: none;
            bottom: 95px;
        }
        .updateCP::after{
            border-bottom: 9px solid #ffffff;
            border-top: none;
            bottom: 95px;
        }
        .media-frame{
            top:70px !important;
        }
        .media-frame-content{
            top: 110px !important;
        }
        .media-frame-router{
            top: 76px !important;
        }
        h2.media-frame-actions-heading.screen-reader-text {
            font-size: 17px;
        }
        .media-modal-close{
            top: 70px !important;
        }
        .map_loc {
            display: inline-flex;
        }
        .capture_photo{
            //display: block ;
        }
        .btn_connect {
            right: 10px;
            top: 10px;
            position: absolute;
        }
        .btn_profile_con{
            padding: 8px 10px;
        }
        .btn_messages {
            top: 10px;
            left: 10px;
            position: absolute;
            border-radius: 5px 0px 0px 5px;
        }
        .btn_profile{
            padding: 5px 10px;
            border-radius: 5px;
        }
        .btn_follow {
            top: 48px;
            right: 10px;
            position: absolute;
            padding: 7px 10px;
        }
        .connect_button_connected{
            right: 44px !important;;
            border-radius: 5px 0px 0px 5px !important;
        }
        .p-dots-area_1_mob {
            background: #bab1b1;
            padding: 4px 7px;
            outline: none !important;
            border-radius: 0px 5px 5px 0px;
            font-size: 15px;
            position: absolute;
            right: 14px;
            top: 10px;
            z-index: 20000;
            border: 1px solid #cacaca;
          }
    }

    @media only screen and (max-width: 767px) {
        .elementor-row.elementor-rowsec {
            display: block;

        }

        .elementor-element.elementor-element-4d74689.elementor-column.elementor-col-50.elementor-top-column {
            width: 100%;
            display: block;
        }

        .elementor-element.elementor-element-5e67e44.elementor-column.elementor-col-50.elementor-top-column {
            width: 100%;
            display: block;
        }
    }



    /* The Modal (background) */
    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 2;
        /* Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 0px;
        border: 1px solid #888;
        width: 30%;
        padding-top: 0px;
    }

    /* The Close Button */

    .close_btn:hover,
    .close_btn:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    .btn-3 .btn{
        border: 1px solid #28a745;
        background: none;
        color: #28a745;
        width: 80%;
        position: relative;
        margin: auto;
        font-size: 14px;
        font-weight: bold;
        padding: 8px;
    }
    
    .btn-3 .btn:before {
    content: ' ';
    position: absolute;
    width: 0;
    height: 0;
    left: auto;
    right: 17px;
    bottom: -26px;
    border: 13px solid;
    border-color: #28a745b0 #28a745ad transparent transparent;
}
    .btn-3 .btn:after{
    content: ' ';
    position: absolute;
    width: 0;
    height: 0;
    left: auto;
    right: 19px;
    bottom: -22px;
    border: 11px solid;
    border-color: lightyellow lightyellow transparent transparent;
}
    
   .btn-1 .btn, .btn-2 .btn, .btn-4 .btn{
        background: #28a745;
        width: 80%;
        margin: auto;
        font-size: 14px;
        font-weight: bold;
        color: #fff;
        padding: 8px;
    }
   .btn-1, .btn-2, .btn-3, .btn-4 {
        width: 24%;
        display: inline-block;
        text-align: center;
        vertical-align: middle;
        padding: 10px;
    } 
    .btns_divs {
        margin-top: 15px;
        text-align: center;
    }
</style>



<script>
    jQuery(document).ready(function($) {
        $(".coverIm").click(function() {
            $(".CoverPage").addClass("Cphotohide");
        });

        $(".cover-close-m").click(function() {
            $(".CoverPage").removeClass("Cphotohide");
        });
    });
</script>

<script>
    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close_btn")[0];

    // When the user clicks the button, open the modal
    if (!!btn) {
        btn.onclick = function() {
            modal.style.display = "block";
        }
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>

<style type="text/css">
  
  .connect_button_color{
    background :#008a66!important;
  }
</style>
<script>
    jQuery(document).ready(function($) {
        
        $('.capture_photo').click(function(){
            $('#capt_camera').trigger('click');         
        });
        
        
    var website_url ="https://www.travpart.com/English";
    var sented = false;
    //$(".connect_button_sent,.connect_button_connected").one('click', function() {

    jQuery(document).one("click",".connect_button_sent,.connect_button_connected",function(){

    var user_id = <?php echo $user_id  ?>;
    if (sented) {
    return;
    }
    sented = true;
    $.ajax({
    type: "POST",
    url: website_url + '/submit-ticket/',
    data: {
    friend_id: user_id,
    action: 'connection_request_cancel'
    }, // serializes the form's elements.
    success: function(data) {
    var result = JSON.parse(data);
    if (result.status==1) {
    $(".connect_button_sent").addClass('connect_button_color');
    $("#minus").addClass('fa-user-plus');
     $("#minus").removeClass("fa-user-minus");
    $(".p-connect-area-text").html('<div class="col-md-99"><a href="#" style="color:white!important">Connect</a></div>');
    $("/").removeClass("intro");
    // alert(result.message);
    //window.location.reload()// show response from the php script.
    } 

    else {
    //alert(result.message);
    location.reload();
    //window.location.reload()// show response from the php script.
    }
    }
    });
    });
    });

     </script>