<?php
/*
Template Name: My Connection
*/

global $wpdb;
wp_enqueue_media();
$confirm=0;
if (isset($_GET['user'])) {

    $username = htmlspecialchars( $_GET['user'] );
    $user = get_user_by('login', htmlspecialchars($_GET['user']));
    if (is_numeric($username) AND $username>0) {

        $user = get_user_by('id',  htmlspecialchars ($_GET['user']));
        $username = $user->user_login;
    }
}

$user_location = $wpdb->get_row("SELECT region,country FROM `user` WHERE username='{$user->user_login}'");
if (!empty($user_location)) {
    $user_location = trim($user_location->region . ', ' . $user_location->country, ',');
}
$location_visiable = get_user_meta($user->ID, 'location_visiable', true);
if ($location_visiable == 'anyone' || $user->ID == wp_get_current_user()->ID) {
    $location_visiable = true;
} else if (empty($location_visiable) || $location_visiable == 'family') {
    if (strpos(get_user_meta($user->ID, 'family_member', true), wp_get_current_user()->user_login) !== false) {
        $location_visiable = true;
    } else {
        $location_visiable = false;
    }
} else if ($location_visiable == 'friend') {
    $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}' AND user2=" . wp_get_current_user()->ID);
    if ($is_friend == 0) {
        $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}' AND user1=" . wp_get_current_user()->ID);
    }
    if ($is_friend > 0) {
        $location_visiable = true;
    } else {
        $location_visiable = false;
    }
} else if ($location_visiable == 'family_friend') {
    if (strpos(get_user_meta($user->ID, 'family_member', true), wp_get_current_user()->user_login) !== false) {
        $location_visiable = true;
    } else {
        $location_visiable = false;
    }
    if (!$location_visiable) {
        $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}' AND user2=" . wp_get_current_user()->ID);
        if ($is_friend == 0) {
            $is_friend = $wpdb->get_var("SELECT count(*) FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}' AND user1=" . wp_get_current_user()->ID);
        }
        if ($is_friend > 0) {
            $location_visiable = true;
        } else {
            $location_visiable = false;
        }
    }
} else {
    $location_visiable = false;
}
get_header();
$current_user_id = wp_get_current_user()->ID;
$connection_count  = $wpdb->get_var(
        "SELECT COUNT (*) FROM friends_requests WHERE  (user2 = '{$current_user_id}' OR  user1 = '{$current_user_id}') AND status=1" );




?>
<script src="<?php echo get_template_directory_uri(); ?>/js/friends_connect.js?57"></script>
<div class="main_area">
        <?php include 'timeline-common-section.php';?>

               <!--  </div>
            </div> -->
    </div>
            
             <?php
      global $wpdb;
      if ( is_user_logged_in() ) {
      $current_user = wp_get_current_user();
      $logged_in_user_id = get_current_user_id();

    $friends_requests = $wpdb->get_var(
    "SELECT COUNT(*) FROM friends_requests WHERE  user2 = '{$user->ID}' AND status=0" );

    }
    else{

      //exit(wp_redirect(home_url('login')));

    }
    ?>
                  
             <div class="desktop_hide">
                <div class="main_mobile">
                  
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon custom_addon"><i class="fa fa-search"></i></div>
                      <input type="text" class="form-control" id="exampleInputAmount"  placeholder="Search for your Connections" name="search">
                    </div>
                  </div>
                   <?php if (is_user_logged_in() and get_current_user_id() == $user_id) { ?>
                  <div class="user_name_connections">
                    <h4><strong><?php echo $user->user_nicename; ?> new connection requests: <?php echo $friends_requests;  ?></strong></h4>
                  </div>
                <?php } ?>         
                       
                 </div>  
             </div>
               
            <div class="mobile_hide">
              <div class="for_pd_mg">
                <div class="col-md-12 connection_box">
                    <div class="button-link1 leftside">
                        <a href="#"><span class="button-text"><i class="fas fa-user-friends"></i>CONNECTIONS</span></a>
                    </div>
                    <!--<div class="button-link btn_time">
                        <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo esc_html($username); ?>"><i class="far fa-clock"></i><span class="button-text">TIMELINE</span></a>
                    </div>-->
                    <?php if (is_user_logged_in() and get_current_user_id() == $user_id) { ?>
                      <div class="connectionre">
                          <a class="btn btn-default connect_request_co" href="https://www.travpart.com/English/connection-request-list/?user=<?php echo $current_user->user_login;  ?>">
                          <span class="button-text">Connection Request <span class="btn_nm"><?php echo $friends_requests; ?></span> </span></a>
                      </div>
                     <?php  } ?>
                </div>

                <div class="col-md-12 all_con_search">
                  <div class="col-md-6" style="padding-left: 5px;">
                      <span style="padding-right: 10px;color:#000000;font-size: 15px;">All Connections</span><span style="color:grey;font-size: 15px;"> <?php echo $connection_count;  ?></span>
                  </div>
                  <div class="col-md-6" style="padding-left: 0px;text-align: end;">
                      <div class="search-container">
                        <form>
                          <input type="text" placeholder="Search for your friends" name="search">
                          <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                      </div>
                    </div>
                    <div class="all_cons_arrow">
                      <div class="all_cons_triangle"></div>
                    </div>
                  </div>
                  
                <div class="row for_col_ng">            
                <?php
 $mut1 = "

  SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$user->ID}'

  UNION

  SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$user->ID}'";

  $rt1 = $wpdb->get_results($mut1);

  foreach ($rt1 as $value) {

  if ($value->user2 == $user->ID)
  continue;
  $friends_list1[] = $value->user2;
  }

  $connections = $wpdb->get_results(
"SELECT * FROM friends_requests WHERE  (user2 = '{$user->ID}' OR  user1 = '{$user->ID}') AND status=1" );


 foreach ($connections as $value) {
  // some calaclations
  if($value->user1 == $user->ID){
 $Data = $wpdb->get_row("SELECT * FROM `wp_users` WHERE ID = '$value->user2'");
}
elseif($value->user1==$current_user->ID){
   $Data = $wpdb->get_row("SELECT * FROM `wp_users` WHERE ID = '$value->user2'");

}

else
{
  $Data = $wpdb->get_row("SELECT * FROM `wp_users` WHERE ID = '$value->user1'");
}
$author_obj = get_user_by('id', $Data->ID);
$get_hometown  = get_user_meta( $Data->ID, 'hometown' , true );

// mutual friends counting
$mut2 = " SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$Data->ID}'";

$rt2 = $wpdb->get_results($mut2);
foreach ($rt2 as $value) {
 $friends_part1[] = $value->user2;
  }
$mut3 = " SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$Data->ID}'";

$rt3 = $wpdb->get_results($mut3);
foreach ($rt3 as $value3) {
 $friends_part2[] = $value3->user1;
 }

///print_r(array_merge($friends_part1,$friends_part2));

$match = array_intersect($friends_part1, $friends_part2);

   $user_meta = get_userdata($Data->ID);
    $user_roles = $user_meta->roles;

    if ( in_array( 'um_travel-advisor', $user_roles, true ) ) {
      $role ="Seller";
    }
    elseif ( in_array( 'um_clients', $user_roles, true ) ) {
      $role = "Buyer";
    }
    else
    {
      $role="Buyer";
    }

     if($role=="Buyer"){


            // calculations for status,either they have sent friend request before/blocked

            $Data1 = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user1 = '{$current_user->ID}' AND user2 = '{$Data->ID}'");
            if($Data1==NULL){

            $Data1 = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '$current_user->ID' AND user1 = '{$Data->ID}'");

            }
            if ($Data1==NULL){
                $connIcon = ' <i class="fas fa-user-plus" id="plus"></i>';
                $button_text =" Connect";
                $button_class ="connect_button1";
                $following=0;
            }
            else if($Data1->status==0){
                $connIcon = ' <i class="fas fa-user-minus" id="plus"></i>';
                $following=1;
                $button_text =" Cancel Request";
                $button_class ="connect_button_sent_1";
            }
             else if($Data1->status==1){
                $connIcon = ' <i class="fas fa-user-minus" id="minus"></i>';
                $following=1;
                $button_text ="Connected";
                $button_class ="connect_button_sent_1";
            }
            else{
                //  continue;
            }
        }
        else{

         $followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$current_user->ID' AND sf_agent_id = '$Data->ID'", ARRAY_A);

          if (!empty($followData)) {
 
            $button_text ="Following";
            $following=1;
            $btn_cs = 'following_class';
            $button_class ="follow_button_2";
            $connIcon = ' <i class="fas fa-wifi" id="plus"></i>';
          //  continue;
        }
        else{
          $btn_cs = '';
            $following=0;
            $connIcon = ' <i class="fas fa-wifi" id="plus"></i>';
            $button_text ="Follow";
            $button_class ="follow_button_2";

             }
            }

  ?>
                
                   <div class="col-md-6 friend_box_row">
                    <div class="friends_box">
                        <div class="friends_img">
                              <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $Data->ID; ?>"><?php echo get_avatar( $Data->ID,150 );  ?></a>
                          </div>
                            <div class="friends_details">
                            <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $author_obj->user_login; ?>">
                                    <?php echo um_get_display_name( $Data->ID); ?>
                                  </a>
                            </br>
                            <?php
                          //  print_r($friends_list2);
                            ?>
                              <span><?php  if(!empty($get_hometown))
                               echo $get_hometown; ?></span></br>
                              <?php if(count ($match)>0) {?>
                              <span style="color:grey">mutual friends: <?php echo count ($match)  ?></span>
                            <?php } ?>
                            </div>
                                
                
             <!--
                        <div class="friends_connect" >
                          <button class="connection_con_btn <?php  echo $_button_class ?>" id= "<?php echo $Data->ID ?>">
                             <span><?php echo $button_text;  ?></span>
                        </button>
                        </div>  
                      -->
                      <?php
                      if (is_user_logged_in()) { 
                        ?>
                      <div class="friends_connect" >
                     <button class="btn_profile_con <?php echo $button_class;  ?> <?php echo $btn_cs; ?>" id="<?php  echo $Data->ID; ?>" > 
                                <div class="rm_d_i" > 
                                    <?php echo $connIcon;?>
                                </div> 
                                <div class="rm_d_text"> 
                                    <a href="#" class="p-connect-area-text"><?php  echo $button_text; ?></a> 
                                </div> 
                            </button>

                    </div>
                <?php } ?>










                        
                    </div>
                   </div>
                   <?php 
                   unset ($friends_part1);
                   unset($friends_part2);
                 } ?>
        
                 </div>
                 </div>
                </div>


        

<?php
get_footer();
?>

<style type="text/css">
.connect_button_sent_1 .rm_d_text a{
  color: #000 !important;
}

.following_class .rm_d_text a{
  color: #000 !important;
}
i.fa-wifi{
  top:3px !important; 
}

.follow_button_2 .rm_d_text a{
  color: #fff;
}
.follow_button_2{
  background: #008a66;
  color: #fff !important;
  font-weight: bold;
}
.following_class{
  background: #dad8d8;
  color: #000 !important;
}
.connect_button_sent_1{
  background: #dad8d8 !important;
    color: #000 !important;
    font-weight: bold;
}

.friends_details a {
    font-size: 16px;
    font-weight: bold;
}
.connection_con_btn {
        width: 120px;
        background: #008a66;
        padding: 8px 10px;
        border: 0px;
        letter-spacing: 1px;
        border-radius: 5px;
        color: #fff;
        line-height: 15px;
        font-size: 13px;
        font-family: "Roboto", Sans-serif;
        cursor: pointer;
    }

    .connection_con_btn i {
        font-size: 13px !important;
        padding-right: 3px;
        margin-right: 0px !important;
        padding-left: 0px !important;
    }
  .friends_details span{
    font-size: 15px;
  }
  .friend_box_row{
    padding-top: 10px;
    background-color: white;
  }
  .friends_box{
    width: 100%
  }
  .friends_img{
    width: 33%;
    vertical-align: middle;
    display: inline-block;
  }
  .friends_img img{
    width: 130px;
    height: 130px;
  }
  .friends_details{
    width: 31%;
    vertical-align: middle;
    display: inline-block;
  }
  .friends_connect{
    width: 33%;
    vertical-align: middle;
    display: inline-block;
    text-align: end;
  }
    .time_btn_connec_page_dis{
        display: block;
    } 
    .request_sent_btn{
    line-height: 13px;
  }
  .connect_btn{
    background: #018462 !important;
    color:#fff !important;
  }
    button.friends-connected.request_sent_btn,.connect_btn{
      border-radius: 5px !important;
      width: 150px !important;
      position: relative;
      font-weight: bold;
  }
    .request_sent_btn i{
    position: absolute;
      left: 12px;
      top: 50%;
      margin-top: -8px;
  }
    .timeline_no_connections{
        text-align: center;
        font-size: 40px;
        color: grey;
        margin-top: 50px;
    }
    #footer {
        display: none;
    }
</style>

<style>
    .Cphotohide {
        display: none;
    }
    .btn_time a i{
        padding-right: 5px;
        font-size: 18px;
    }
    .button-link{
        margin-top: 0px;
    }
    
  .updatecoverpic {
    position: absolute;
    top: 5px;
    right: 5px;
    background: #cacaca;
    padding: 5px;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    box-shadow: 2px 2px 5px #666;
    color: #444444;
}
.timelinecover {
    position: relative;
}
.timelinecover img {
    width: 885px;
    height: 350px;
    border-radius: 5px;
    border: 2px solid #1abc9c;
}
.verifiedseller {
    background: #1abc9c;
    color: white;
    text-align: center;
    position: absolute;
    left: 0;
    bottom: 0;
    padding: 5px;
}
.timelineprofilepic {
    max-height: 200px;
    position: relative;
}
.timelineprofilepic img {
    width: 100%;
    max-height: 200px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    border: 2px solid #1abc9c;
}
.updateprofilepic {
    position: absolute;
    left: 0;
    right: 0;
    bottom: 5px;
    margin-left: auto;
    margin-right: auto;
    width: 100px;
    text-align: center;
    background: #cacaca;
    padding: 5px;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    box-shadow: 2px 2px 5px #666;
    color: #444444;
}
.updateprofilepic:hover {
    background: #1abc9c;
    cursor: pointer;
}
.editprofile {
    text-align: center;
    background: #cacaca;
    padding: 5px;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    color: #444444;
    box-shadow: 2px 2px 5px #666;
    cursor: pointer;
    margin: 15px;
}
.btn_t_pv_c_pp{
  display: flex;
}

.btn_time a span.button-text{
    font-size: 18px;
}
.button-link.btn_con a {
    border: 1px solid #28a745;
    background-color: white;
    color: #28a745;
    padding-top: 6px;
    padding-bottom: 6px;
}
.btn_con_arrow {
    width: 15px;
    height: 20px;
    right: -110px;
    top: -4px;
    position: relative;
    border-top: 4px solid white;
}

.btn_con_triangle {
    position: absolute;
    margin: auto;
    top: -7px;
    left: 0;
    right: 0;
    width: 12px;
    height: 12px;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    -moz-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    border-right: 1px solid #28a745;
    border-bottom: 1px solid #28a745;
}
.post_area {
    border-bottom: 1px solid black;
    border-left: 1px solid black;
    border-right: 1px solid black;
    border-radius: 0px;
    display: none;
}
.button-link1.leftside a span i {
    color: grey;
    padding-right: 10px;
    padding-left: 10px;
}
.button-link1.leftside a span {
    color: #28a745;
    font-weight: 600;
}
.button-link1.leftside a:hover {
    color: #28a745;
    text-decoration: none;
}
.mobile_hide{
    border-bottom: 1px solid black;
    border-left: 1px solid black;
    border-right: 1px solid black;
    border-radius: 0px;
    padding-top: 10px;
}
.all_con_search {
    padding-left: 0px;
    padding-right: 0px;
    padding-bottom: 10px;
    padding-top: 10px;
    border-bottom:1px solid #80808036;
}
.search-container form input {
    width: 225px;
    font-size: 13px;
    margin-right: -4px;
}
.search-container button{
    padding-top: 4px;
    padding-bottom: 2px;
}
.all_cons_arrow {
    width: 15px;
    height: 20px;
    right: -50px;
    top: 32px;
    position: relative;
    border-top: 4px solid #f6f6f6;
}

.all_cons_triangle {
    position: absolute;
    margin: auto;
    top: -7px;
    left: 0;
    right: 0;
    width: 12px;
    height: 12px;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    -moz-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    border-left: 1px solid #80808036;
    border-top: 1px solid #80808036;
}
.frnd_dots{
    border: 1px solid #80808036;
    padding-left: 5px;
    padding-right: 5px;
}
  .section1 {
    border-left: 1px solid #000;
    border-top: 1px solid #000;
    border-right: 1px solid #000;
}


.elementor.elementor-22017 {
    border-bottom: 1px solid #000;
    border-left: 1px solid #000;
    border-right: 1px solid #000;
}

.header_txt h2{
    text-align: center;
    margin-bottom: 50px;
}

.button-link a {
    background-color: #28a745;
}
.leftside {
    float: left;
}
span.request {
    background: red;
}
.connectionre{
    float: right;
    font-size: 18px;
    padding: 5px;
    color: #6249C8;
}
.post_area {
    background: #fff;
}
.button-link1.leftside {
    margin-top: 10px;
    margin-right: 19px;
    font-size: 17px;
}

.friends-box {
    font-size: 11px;
    text-transform: capitalize;
}
.friends-details {
    float: left;
}
.friends-img {
    position: relative;
    float: left;
}
.friends-name {
    float: left;
    font-weight: 700;
    padding-top: 30px;
    padding-left: 16px;
}
.friends-details-right {
    float: right;
    padding-top: 32px;
    display: flex;
}

img.friendsImg {
    height: 92px;
    width: 92px;
}
.friends-connected {
    background: #cacaca;
    color: #000;
    font-weight: bold;
    width: 120px;
    letter-spacing: 1px;
    padding: 8px 10px;
    font-size: 13px;
    font-family: "Roboto", Sans-serif;
    cursor: pointer;
    border-radius: 5px 0px 0px 5px;
    border: 0px;
}
a#dropdownMenuButton_1 {
    color: #333;
}
.my_dropdownMenuButton_1{
      right: 0px  !important;
      min-width: 35% !important;
      float: none !important;
      left: auto !important;
}
.my_p-dots-area_1 {
    background: #bab1b1;
    padding: 3.4px 7px;
    outline: none !important;
    border-radius: 0px 5px 5px 0px;
    font-size: 15px;
    position: relative;
    top: 0px;
    right: 0px;
    z-index: 20000;
    border: 1.5px solid #cacaca;
}
.bottonline{
        border-style: solid;
    border-width: 0px 0px 2px 0px;
    border-color: #686868;
}
.friends-name p {
    font-size: 14px;
    color: #afabab;
}

.topnav {
  overflow: hidden;
  background-color: #e9e9e9;
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #2196F3;
  color: white;
}

.topnav .search-container {
  float: right;
}

.topnav input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}

.topnav .search-container button {
  float: right;
  padding: 6px 10px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 17px;
  border: none;
  cursor: pointer;
}

.topnav .search-container button:hover {
  background: #ccc;
}
.button-link {
    text-align: left;
}
.rm_d_i {
    display: inline-block;
    vertical-align: middle;
    width: 25%;
}
.rm_d_text {
    display: inline-block;
    vertical-align: middle;
    width: 65%;
}


@media screen and (max-width: 600px) {
    .friends_img{
      width: 28%;
    }
    .btn_profile_con {
      padding: 10px 10px;
    }
    .friends_details {
      width: 32%;
    }
    .friends_details a {
      font-size: 14px;
    }
    .friend_box_row{
      padding: 5px;
    }
    .for_pd_mg {
          margin: 0px !important;
      }
      .friends_img img {
        width: 90px;
        height: 90px;
        border-radius: 50%;
    }
    .mobile_hide{
      border: none;
    }
    .timeline_no_connections{
        font-size: 35px;
    }
    .connection_box{
        display: none;
    }
    .all_con_search{
        display: none;
    }
    .main_area{
        display: none;
    }
    .desktop_hide{
        display: block !important;
    }
  .header_txt h2{
        margin-top: 100px;
      }
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
  .button-text {
    font-size: 12px;
}

.col-md-button{
    width: 15%;
    display: table-cell;
    vertical-align: middle;
    text-align: right;
}
.col-md-content{
    width: 15%;
    display: table-cell;
    vertical-align: middle; 
}
.col-md-avatar img{
    border-radius: 50%;
}
.col-md-avatar{
    width: 2%;
    display: table-cell;
    vertical-align: middle;
}
.mob_dot_btn {
    width: 2%;
    display: table-cell;
    vertical-align: middle;
}
p.mob_frnd_dots {
    text-align: center;
    border: 1px solid grey;
    padding-top: 1px;
    padding-bottom: 7px;
    padding-left: 3px;
    padding-right: 3px;
}
}
.custom_addon{
    padding: 10px;
    background: #fff;
    border-radius: 20px 0px  0px 20px;
    background: #e2dededb;
}
.main_mobile{
    padding: 10px;
    background: #fff;
}
.desktop_hide{
    padding-top: 70px;
    background: #fff;
}
#exampleInputAmount{
    border:0px;
    font-size: 12px;
    font-family: 'Heebo', sans-serif;
    background: #e2dededb;
    border-radius: 0px 20px  20px 0px;
}
.user_name_connections h4{
    font-family: 'Heebo', sans-serif;
    text-transform: uppercase;
}
input :focus{
    outline: none !important;
}
.desktop_hide{
    display: none;
}
.row_req{
    margin-bottom:10px;
}
.font_si{
    font-size: 12px;
    margin: 0;
}
.custom_btn_connected{
    background: white;
    color: black;
    font-family: 'Heebo', sans-serif;
    border-radius: 1px;
    font-size: 11px;
    font-weight: 700;
    border: 1px solid grey;
}
.user_name_connections{
    margin: 20px 0px;
}

.connect_request_co {
    background: #28a745 !important;
    color: #fff;
    padding: 5px 20px !important;
}

.btn_nm {
    background: red;
    color: #fff;
    padding: 4px 4px;
    border-radius: 50%;
}

.for_pd_mg{
    margin: 20px;
    background: #afaeae1f;
    border: 1px solid #ddddddd9;
}
.friends-img img{
  width: auto;
  height: 100px;
  border-radius: 5px;
}
.for_col_ng{
    margin: -1px;
    border: 1px solid #ddddddd9;
    background: #fff;
    padding: 10px 0px 20px 0px;
}
</style> 
<script type="text/javascript">
    $(document).ready(function(){
        //$(".timelinecover a").attr("href", "https://www.travpart.com/English/my-time-line/?user=<?php echo esc_html($username); ?>");
    });
</script>