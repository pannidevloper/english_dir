<?php
/* Template Name: Discount price page */
get_header();
?>

<div class="col-md-12 col-sm-12 marvelapp-flighttab1a">
            	<div class="marvelapp-taboption-link">
                  <?php
if(isset($_SERVER['HTTP_REFERER']))
{
$a=$_SERVER['HTTP_REFERER'];

}
                  ?>
                	<a href="<?php echo $a; ?>"><span><b>&#8678;</b> Back</span>
                   
                    </a>
                    <div class="tab-optionlink-flight">
                    	<ul class="nav nav-tabs">
                        	<li class="active">
                            	<a href="#" data-toggle="tab" >flight<span></span></a>                                
                            </li>
                            <li><strong>|</strong></li>
                        	<li>
                            	<a href="#" data-toggle="tab" >Hotel<span></span></a>                                
                            </li>
                            <li><strong>|</strong></li>
                            <li>
                            	<a href="#" data-toggle="tab" >Train<span></span></a>                                
                            </li>
                            <li><strong>|</strong></li>
                            <li>
                            	<a href="#" data-toggle="tab" >Car Rental<span></span></a>                                
                            </li>
                            <li><strong>|</strong></li>
                        	<li>
                            	<a href="#" data-toggle="tab" >Entertainment<span></span></a>
                            </li>
                            <li><strong>|</strong></li>
                            <li>
                            	<a href="#" data-toggle="tab" >Tours<span></span></a>                               
                            </li>
                        </ul>
                        
                        <div class="tab-content">
    <div id="flight" class="tab-pane fade in active">
      <div class="flighttab">
      	<table class="table table-bordered">
        	 <thead>
            <tr>
             
                  <th colspan="3">Discount Table</th>
              
            </tr>
            
            <tr>
            	<th>Number Of Guest</th>
                <th>Discount Bali</th>
                <th>Discount Non-bali</th>
            </tr>
            </thead>
            
            <tbody>
            	<tr>
                	<td>
                    	1-2
                    </td>
                    <td>
                    	<?php echo get_post_meta($post->ID,'_cs_disbaligroup1',true);?>
                    </td>
                    <td>
                    <?php echo get_post_meta($post->ID,'_cs_disnonbaligroup1',true);?>
                    </td>
                </tr>
                <tr>
                	<td>
                    	3-5
                    </td>
                   <td>
                    	<?php echo get_post_meta($post->ID,'_cs_disbaligroup2',true);?>
                    </td>
                    <td>
                    <?php echo get_post_meta($post->ID,'_cs_disnonbaligroup2',true);?>
                    </td>
                </tr>
                
                <tr>
                	<td>5-10</td>
                   <td>
                    	<?php echo get_post_meta($post->ID,'_cs_disbaligroup3',true);?>
                    </td>
                    <td>
                    <?php echo get_post_meta($post->ID,'_cs_disnonbaligroup3',true);?>
                    </td>
                </tr>
                
                <tr>
                	<td>10-15</td>
                    <td>
                    	<?php echo get_post_meta($post->ID,'_cs_disbaligroup4',true);?>
                    </td>
                    <td>
                    <?php echo get_post_meta($post->ID,'_cs_disnonbaligroup4',true);?>
                    </td>
                </tr>
                <tr>
                	<td>>15</td>
                    <td>
                    	<?php echo get_post_meta($post->ID,'_cs_disbaligroup5',true);?>
                    </td>
                    <td>
                    <?php echo get_post_meta($post->ID,'_cs_disnonbaligroup5',true);?>
                    </td>
                </tr>
            
            </tbody>
            
        </table>
      </div><!--table-responsive flighttab-->
      
      <div class="traveldiscount-status">
      	<div class="hex-shape">
        	<a href="#">The discounts only apply if you buy more than 1 Travel Service</a>
        </div><!--hex-shape-->
      
      <div class="booking-applicable">
      <p> Book Fight only = no discount</p><br>
<p>Book Hotel only = no discount</p><br>
<p>Book Tour only  = no discount</p><br>
<p>Book Fight+Hotel= discount is applicable</p><br>
<p>Book Fight+Tour =  discount is applicable</p><br>
<p>Book Fight+Tour+Hotel = discount is applicable</p><br>
<p>Book Fight+Tour ==  discount is applicable</p><br>
	</div><!--booking-applicable-->
      </div><!--traveldiscount-status-->
      
      
      
    </div>
    <div id="Hotel" class="tab-pane fade">
     
    </div>
    <div id="Train" class="tab-pane fade">
      
    </div>
    <div id="carrental" class="tab-pane fade">
      
    </div>
    <div id="Entertainment" class="tab-pane fade">
      
    </div>
     <div id="Tours" class="tab-pane fade">
      
    </div>
  </div>
</div>
                    </div>
                    
                    
                </div>
            

<style>
  /* CSS Document */

  <!------------11-7-2018 marvelapp------------------>
.marvelapp-flighttab1{

    display: inline-block;
    float: left;
    width: 1200px;
	overflow-x:hidden;

}
  .marvelapp-flighttab1a
  {
	  text-align: center;
  }
  
  .marvelapp-taboption-link {
    display: inline-block;
    float: left;
    width: 1170px;
}
.marvelapp-taboption-link a {
    display: inline-block;
    float: left;
	position:relative;
}
.marvelapp-taboption-link a span {
    display: inline-block;
    float: left;
    font-size: 15px;
    color: black;
    font-weight: 900;
}
.marvelapp-taboption-link a span b {

    display: inherit;
    width: 100%;
    font-size: 60px;
    height: 60px;

}
.tab-optionlink-flight {
    display: inline-block;
    float: left;
    margin-left: 20px;
    width: 90%;
	padding-top: 25px;
}
.tab-optionlink-flight > .nav.nav-tabs li.active {

}
.tab-optionlink-flight > .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {

   color: #5d64d2;

cursor: default;

background-color: transparent !important;

border-right: none !important;

border-bottom: 3px solid #ff7200 !important;

border-radius: 0px !important;

border-top: none !important;

border-left: none !important;

}
.tab-optionlink-flight > .nav.nav-tabs {
}
.tab-optionlink-flight > .tab-content {

    display: inline-block;
    float: left;
    width: 100%;
	margin-top: 10px;

}

.tab-optionlink-flight > .tab-content > .tab-pane {

    display: inline-block;
    float: left;
    width:100%;

}
.tab-optionlink-flight > .nav.nav-tabs li
{
}
.tab-optionlink-flight > .nav.nav-tabs li a
{
	color: #008de3 !important;

font-size: 16px;

font-weight: 600;

text-transform: capitalize;
}
.tab-optionlink-flight > .nav.nav-tabs li strong {

    padding-top: 10px;
    display: inline-block;
    color: #d3d3d3;

}
.flighttab
{
	display: inline-block;
    float: left;
    width:50%;
}

.flighttab > .table {

    margin-bottom: 0px;
	background-color: #f1f0ec !important;
	border: 3px solid #000;

}

.flighttab > .table thead {

}

.flighttab > .table thead tr {

}
.flighttab > .table thead tr th {

    text-align: center;
    font-size: 16px;
    font-weight: 600;
	  color: #282828;
	  border: 2px solid #fff !important;

}
.flighttab > .table tbody {

    text-align: center;

}
.flighttab > .table tbody tr {

}

.flighttab > .table tbody tr td {

    font-size: 16px;
    font-weight: 400;
    color: #282828;
	border: 2px solid #fff !important;

}
  .traveldiscount-status {

    display: inline-block;
    float: left;
	width: 48%;
text-align: left;
margin-left: 20px;

}
.hex-shape {

    display: inline-block;
    position: relative;
	text-align: center;
	/*margin: 40px 0px;*/
	background:url(https://tourfrombali.cn/test-box/wp-content/themes/bali/images/hexshape.png) top center no-repeat;
	background-size:cover;
	width: 207px;
height: 207px;
padding: 60px 10px;

}



.hex-shape a {
display: inline-block;
float: none;
width: 200px;
font-size: 15px;
font-weight: 600;
color: #000;
text-align: left;
word-break: keep-all;
padding-top: 10px;
padding-bottom: 10px;
padding-left: 20px;
padding-right: 20px;

}

.booking-applicable {

    width: auto;
    display: inline-block;
    text-align: left;

}
.booking-applicable p {

    display: inline-block;
    width: auto;
    text-align: left;
    font-size: 14px;
    font-weight: 600;

}


  
  </style>






<?php
get_footer();
?>