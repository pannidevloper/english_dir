<?php
/**
 * @link http://webdevstudios.com/2015/03/30/use-cmb2-to-create-a-new-post-submission-form/ Original tutorial
 */
/**
 * Register the form and fields for our front-end submission form
 */
function wds_frontend_form_register() {
    $cmb = new_cmb2_box( array(
        'id'           => 'front-end-user-profile-map-form',
        // 'object_types' => array( 'post' ),
        'hookup'       => false,
        'save_fields'  => false,
    ) );
    $cmb->add_field( array(
        'name'    => __( 'has_lived_here', 'wds-post-submit' ),
        'id'      => 'has_been_here',
        'type'    => 'hidden',
        'default' => __( '', 'wds-post-submit' )
    ) );
    $cmb->add_field( array(
        'name'    => __( 'has_lived_here', 'wds-post-submit' ),
        'id'      => 'has_lived_here',
        'type'    => 'hidden',
        'default' => __( '', 'wds-post-submit' )
    ) );
    $cmb->add_field( array(
        'name'    => __( 'want_to_go_here', 'wds-post-submit' ),
        'id'      => 'want_to_go_here',
        'type'    => 'hidden',
        'default' => __( '', 'wds-post-submit' )
    ) );
}
add_action( 'cmb2_init', 'wds_frontend_form_register' );
/**
 * Gets the front-end-post-form cmb instance
 *
 * @return CMB2 object
 */
function wds_frontend_cmb2_get() {
    // Use ID of metabox in wds_frontend_form_register
    $metabox_id = 'front-end-user-profile-map-form';
    // Post/object ID is not applicable since we're using this form for submission
    $object_id  = 'fake-oject-id';
    // Get CMB2 metabox object
    return cmb2_get_metabox( $metabox_id, $object_id );
}
/**
 * Handle the cmb-frontend-form shortcode
 *
 * @param  array  $atts Array of shortcode attributes
 * @return string       Form html
 */
function wds_do_frontend_form_submission_shortcode( $atts = array() ) {
    // Get CMB2 metabox object
    $cmb = wds_frontend_cmb2_get();
    // Get $cmb object_types

    // Current buddypress user
    $user_id = bp_displayed_user_id();

    // Parse attributes
    $atts = shortcode_atts( array(
        'current_buddypress_user' => $user_id ? $user_id : null, // Current user, or null
    ), $atts, 'cmb-frontend-form' );
    /*
     * Let's add these attributes as hidden fields to our cmb form
     * so that they will be passed through to our form submission
     */
    foreach ( $atts as $key => $value ) {
        $cmb->add_hidden_field( array(
            'field_args'  => array(
                'id'    => "atts[$key]",
                'type'  => 'hidden',
                'default' => $value,
            ),
        ) );
    }
    // Initiate our output variable
    $output = '';
    // Get any submission errors
    if ( ( $error = $cmb->prop( 'submission_error' ) ) && is_wp_error( $error ) ) {
        // If there was an error with the submission, add it to our ouput.
        $output .= '<h3>' . sprintf( __( 'There was an error in the submission: %s', 'wds-post-submit' ), '<strong>'. $error->get_error_message() .'</strong>' ) . '</h3>';
    }
    // If the post was submitted successfully, notify the user.
    // if ( isset( $_GET['post_submitted'] ) && ( $post = get_post( absint( $_GET['post_submitted'] ) ) ) ) {
    //     // Get submitter's name
    //     $name = get_post_meta( $post->ID, 'submitted_author_name', 1 );
    //     $name = $name ? ' '. $name : '';
    //     // Add notice of submission to our output
    //     $output .= '<h3>' . sprintf( __( 'Thank you%s, your new post has been submitted and is pending review by a site administrator.', 'wds-post-submit' ), esc_html( $name ) ) . '</h3>';
    // }
    // Get our form
    $output .= cmb2_get_metabox_form( $cmb, 'fake-oject-id', array( 'save_button' => __( 'Save Changes', 'wds-post-submit' ) ) );
    return $output;
}
add_shortcode( 'cmb-frontend-form', 'wds_do_frontend_form_submission_shortcode' );

/**
 * Handles form submission on save. Redirects if save is successful, otherwise sets an error message as a cmb property
 *
 * @return void
 */
function wds_handle_frontend_new_post_form_submission() {

    // If no form submission, bail
    // if ( empty( $_POST ) || ! isset( $_POST['submit-cmb'], $_POST['object_id'] ) ) {
    //     return false;
    // }
    if ( empty( $_POST ) || ! isset( $_POST['object_id'] ) ) {
        return false;
    }

    // Get CMB2 metabox object
    $cmb = wds_frontend_cmb2_get();
    $map_data = array();

    // Get our shortcode attributes and set them as our initial post_data args
    if ( isset( $_POST['atts'] ) ) {
        foreach ( (array) $_POST['atts'] as $key => $value ) {
            $map_data[ $key ] = sanitize_text_field( $value );
        }
        unset( $_POST['atts'] );
    }

    // Check security nonce
    if ( ! isset( $_POST[ $cmb->nonce() ] ) || ! wp_verify_nonce( $_POST[ $cmb->nonce() ], $cmb->nonce() ) ) {
        return $cmb->prop( 'submission_error', new WP_Error( 'security_fail', __( 'Security check failed.' ) ) );
    }

    /**
     * Fetch sanitized values
     */
    $sanitized_values = $cmb->get_sanitized_values( $_POST );
    // Set our post data arguments
    $map_data['has_lived_here']   = $sanitized_values['has_lived_here'];
    unset( $sanitized_values['has_lived_here'] );
    $map_data['has_been_here'] = $sanitized_values['has_been_here'];
    unset( $sanitized_values['has_been_here'] );
    $map_data['want_to_go_here'] = $sanitized_values['want_to_go_here'];
    unset( $sanitized_values['want_to_go_here'] );

    // echo $map_data['has_lived_here'] . '<br />';
    // echo $map_data['has_been_here'] . '<br />';
    // echo $map_data['want_to_go_here'] . '<br />';

    // print_r( json_encode( $map_data ) );
    //update_option( 'buddypress_user_profile_map_user_' . $map_data['current_buddypress_user'], json_encode( $map_data ) );
    update_option( 'buddypress_user_profile_map_user_' . $map_data['current_buddypress_user'],  $map_data );

}
add_action( 'cmb2_after_init', 'wds_handle_frontend_new_post_form_submission' );


/*
function wds_frontend_form_register() {
    $cmb = new_cmb2_box( array(
        'id'           => 'front-end-user-profile-map-form',
        // 'object_types' => array( 'post' ),
        'hookup'       => false,
        'save_fields'  => false,
    ) );
    $cmb->add_field( array(
        'name'    => __( 'Has lived here', 'wds-post-submit' ),
        'id'      => 'has_lived_here',
        'type'    => 'text',
        'default' => __( '', 'wds-post-submit' ),
        'attributes' => array( 'placeholder' => __( "Add Place", 'your_textdomain' ) ),
        'repeatable' => true,
    ) );
    $cmb->add_field( array(
        'name'    => __( 'Has been here', 'wds-post-submit' ),
        'id'      => 'has_been_here',
        'type'    => 'text',
        'default' => __( '', 'wds-post-submit' ),
        'attributes' => array( 'placeholder' => __( "Add Place", 'your_textdomain' ) ),
        'repeatable' => true,
    ) );
    $cmb->add_field( array(
        'name'    => __( 'Want to go here', 'wds-post-submit' ),
        'id'      => 'want_to_go_here',
        'type'    => 'text',
        'default' => __( '', 'wds-post-submit' ),
        'attributes' => array( 'placeholder' => __( "Add Place", 'your_textdomain' ) ),
        'repeatable' => true,
    ) );
}
add_action( 'cmb2_init', 'wds_frontend_form_register' );

function wds_frontend_cmb2_get() {
    // Use ID of metabox in wds_frontend_form_register
    $metabox_id = 'front-end-user-profile-map-form';
    // Post/object ID is not applicable since we're using this form for submission
    $object_id  = 'fake-oject-id';
    // Get CMB2 metabox object
    return cmb2_get_metabox( $metabox_id, $object_id );
}

function wds_do_frontend_form_submission_shortcode( $atts = array() ) {
    // Get CMB2 metabox object
    $cmb = wds_frontend_cmb2_get();
    // Get $cmb object_types

    // Current buddypress user
    $user_id = bp_displayed_user_id();

    // Parse attributes
    $atts = shortcode_atts( array(
        'current_buddypress_user' => $user_id ? $user_id : null, // Current user, or null
    ), $atts, 'cmb-frontend-form' );

    foreach ( $atts as $key => $value ) {
        $cmb->add_hidden_field( array(
            'field_args'  => array(
                'id'    => "atts[$key]",
                'type'  => 'hidden',
                'default' => $value,
            ),
        ) );
    }
    // Initiate our output variable
    $output = '';
    // Get any submission errors
    if ( ( $error = $cmb->prop( 'submission_error' ) ) && is_wp_error( $error ) ) {
        // If there was an error with the submission, add it to our ouput.
        $output .= '<h3>' . sprintf( __( 'There was an error in the submission: %s', 'wds-post-submit' ), '<strong>'. $error->get_error_message() .'</strong>' ) . '</h3>';
    }
    // If the post was submitted successfully, notify the user.
    // if ( isset( $_GET['post_submitted'] ) && ( $post = get_post( absint( $_GET['post_submitted'] ) ) ) ) {
    //     // Get submitter's name
    //     $name = get_post_meta( $post->ID, 'submitted_author_name', 1 );
    //     $name = $name ? ' '. $name : '';
    //     // Add notice of submission to our output
    //     $output .= '<h3>' . sprintf( __( 'Thank you%s, your new post has been submitted and is pending review by a site administrator.', 'wds-post-submit' ), esc_html( $name ) ) . '</h3>';
    // }
    // Get our form
    $output .= cmb2_get_metabox_form( $cmb, 'fake-oject-id', array( 'save_button' => __( 'Save Changes', 'wds-post-submit' ) ) );
    return $output;
}
add_shortcode( 'cmb-frontend-form', 'wds_do_frontend_form_submission_shortcode' );

function wds_handle_frontend_new_post_form_submission() {
    // If no form submission, bail
    if ( empty( $_POST ) || ! isset( $_POST['submit-cmb'], $_POST['object_id'] ) ) {
        return false;
    }
    // Get CMB2 metabox object
    $cmb = wds_frontend_cmb2_get();
    $map_data = array();

    // Get our shortcode attributes and set them as our initial post_data args
    if ( isset( $_POST['atts'] ) ) {
        foreach ( (array) $_POST['atts'] as $key => $value ) {
            $map_data[ $key ] = sanitize_text_field( $value );
        }
        unset( $_POST['atts'] );
    }

    // Check security nonce
    if ( ! isset( $_POST[ $cmb->nonce() ] ) || ! wp_verify_nonce( $_POST[ $cmb->nonce() ], $cmb->nonce() ) ) {
        return $cmb->prop( 'submission_error', new WP_Error( 'security_fail', __( 'Security check failed.' ) ) );
    }

    $sanitized_values = $cmb->get_sanitized_values( $_POST );
    // Set our post data arguments
    $map_data['has_lived_here']   = $sanitized_values['has_lived_here'];
    unset( $sanitized_values['has_lived_here'] );
    $map_data['has_been_here'] = $sanitized_values['has_been_here'];
    unset( $sanitized_values['has_been_here'] );
    $map_data['want_to_go_here'] = $sanitized_values['want_to_go_here'];
    unset( $sanitized_values['want_to_go_here'] );

    // print_r( json_encode( $map_data ) );
    update_option( 'buddypress_user_profile_map_user_' . $map_data['current_buddypress_user'], json_encode( $map_data ) );

}
add_action( 'cmb2_after_init', 'wds_handle_frontend_new_post_form_submission' );

