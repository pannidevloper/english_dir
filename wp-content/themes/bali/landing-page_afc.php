<?php
/* Template Name: Custom Landing Page */
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, height=device-height, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title(''); echo ' | '; bloginfo('name');  ?></title>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_head(); ?>
<link href="<?php bloginfo('template_url'); ?>/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/bxslider.css" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css">

<script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bxslider.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/js/default.js" type="text/javascript"></script>

		<link href="<?php bloginfo('template_url'); ?>/css/colorbox.css" rel="stylesheet" type="text/css">
		<link href="<?php bloginfo('template_url'); ?>/css/custom.css" rel="stylesheet" type="text/css">
		
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
    <script src="<?php bloginfo('template_url'); ?>/js/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.colorbox.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/js/currency.js" type="text/javascript"></script>
		<script>
			$(document).ready(function(){
				var width=$(document).width();	

				if(width>=992){
					width=992;
				}else if(width>=768 && width<=992){
				
					width=768;
				}else{
					width=300;
				}
				//$(".contact_us").colorbox({iframe:true, innerWidth:width, innerHeight:490});
			});
		</script>
		
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65718700-3', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body id="<?php if(is_home() || is_front_page()) { echo 'hp'; } ?>">

<div id="header">
	<div class="container">
    	<div class="row">
        	<div id="h-t" class="col-md-12">
                <ul id="h-l1" class="list-inline pull-left">
                    <li><a href="/">Home</a></li>
                    <li><a href="/about-us">About us</a></li>
                    <li><a href="/contact">Contact</a></li>
                </ul>
                <ul id="h-l2" class="list-inline pull-right">
                    <li><a href="http://www.tourfrombali.cn"><img src="<?php bloginfo('template_url'); ?>/images/flg-cn.png" alt="中文"> 中文</a></li>
                    <li>
						<!--<a href="#"><span class="badge">￥</span> RMB</a>-->
						<!--  style="border-radius: 19px;text-align: center;padding: 0px 0px 0px 17px;" -->
						<select class="header_curreny search_select" style="padding:0px;height:20px;">
								
								<option full_name="IDR"      syml="Rp"   value="IDR"> IDR </option>
								<option full_name="RMB"  syml="元"    value="CNY"> RMB </option>
								<option full_name="USD"        syml="USD"     value="USD"> USD </option>
								<option full_name="AUD"  syml="AUD"    value="AUD"> AUD </option>
						</select>
					</li>
                </ul>
            </div>
           
        </div>
    	<div class="row">
        	<div class="col-md-3">
            	<div id="logo"><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/logo.jpg" alt=""></a></div>
			</div>
            <div id="h-r" class="col-md-9">
                <div id="menu">
                	<div class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                        	<button class="navbar-toggle" data-target="#menu-lnk" data-toggle="collapse" type="button">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse" id="menu-lnk">
                            <ul class="nav navbar-nav">
							
							   <?php
								$parents=array();
								$items = wp_get_nav_menu_items("3");
								$child = wp_get_nav_menu_items("3");

								foreach ( $items as $item ) {
									if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
										$parents[] = $item->menu_item_parent;
									}
								}
								foreach($items as $item): ?>
									
									<?php  if ( $item->menu_item_parent==0 ) { ?>
									<li >
                                    <a href="<?php echo $item->url; ?>"  <?php if ( in_array( $item->ID, $parents ) ) { ?> class="dropdown-toggle" data-toggle="dropdown" <?php }?>><?php echo $item->title; ?></a>
									<?php }  
									if ( in_array( $item->ID, $parents ) ) { ?>
									 <ul class="dropdown-menu">
									<?php foreach ( $child as $child_row ) {
										if($child_row->menu_item_parent==$item->ID){
										echo " <li><a href='".$child_row->url."'>".$child_row->title."</a></li> ";
										}}
									?>
									</ul> 
									<?php } ?> </li>
									<?php 
								endforeach;

							?> 
                                <li class="last"><a class='contact_us' href="<?php bloginfo('url'); ?>/contact-form">Customize journey</a></li>
								
                            </ul>
                        </div>
                    </div>
                	
                </div>
			</div>
        </div>
    </div>
</div><?php

if(is_home() || is_front_page()) {
	$aAImg = explode('=', get_post_meta($post->ID, '_cs_hpsdr_img', true));
	$aATle = explode('=', get_post_meta($post->ID, '_cs_hpsdr_ttl', true));
	if( $aAImg ) {
		echo '<div id="hpslider">
			<div id="hpsdfrm">
				<div class="container">
				<form role="search" method="get" id="searchform " class="above_search searchform" action="'.esc_url( home_url( '/' ) ).'">
					<div id="hpsdfrmc" class="col-md-5">
						<h2>Where to go?</h2>
						<div class="bkblk col-md-12"><input type="text" name="s" id="s" placeholder="Enter your keywords..." class="col-md-8 tbx"><input type="submit" value="Search" class="btn col-md-3"></div>
						<div class="bkblk link-white text-white">Popular searches: <a href="http://www.tourfrombali.com/bali">Bali</a> <a href="http://www.tourfrombali.com/east-java/">East Java</a> <a href="http://www.tourfrombali.com/borobudur-prambanan-tour-indonesia/">Prambanan Temple</a> >></div>
					</div>
				</form>
				</div>
			</div>
			<div id="hpsldr">
				<ul>';
			foreach( $aAImg as $sK => $sV ) {
			if(trim($sV)!="")
				echo '<li><img style="width:100%" src="'.$sV.'" alt=""></li>';
			}
		echo '</ul>
			</div>
		</div>';
	}
}

?>            
            
  <div class="container">
      <div class="row">
          <div class="row top-header">
          <script src="//www.travelpayouts.com/widgets/36e39a14b2e97a75ed7ce54ff8f2039d.js?v=705" async="" charset="utf-8"></script>
          </div>
       </div>
       
      <div class="row">      
          <div class="col-sm-8 blog-main">

              <div class="blog-post">
                <?php echo '<h1 class="blog-post-title">'.get_the_title().'</h1>'; ?>
                
                <?php the_content(); ?>

              </div><!-- /.blog-post -->
              
<?php if( have_rows('destination') ): ?>     

	<?php while( have_rows('destination') ): the_row(); 

		// vars
		$number= get_sub_field('number');
		$name = get_sub_field('name');
		$location = get_sub_field('location');
		$duration = get_sub_field('duration');
		$image = get_sub_field('image');
		$link = get_sub_field('link');
		$description = get_sub_field('description');

		?>
        <div class="blog-post">
        
                <div class="top10_item_wrapper">
                    <div class="top10_item_head_wrapper">
            <div class="top10_item_detail_wrapper">
                              <div class="top_item_number"><?php echo $number; ?></div>
                              <div class="top10_item_sub_detail">
                                    <span class="top_itemListing_title">
                                        
                                            <a href="<?php echo $link; ?>" target="_blank">
                                               <?php echo $name; ?>
                                            </a>
                                     </span> 
                                    <br>                               
                                    <span class="top10_item_location_detail"><?php echo $location; ?></span>
                                    <br> 
                                    <span class="hotel_rooms">Duration: <?php echo $duration; ?></span>
                            </div>
            </div>

            <div class="top10_item_image_wrapper">
                
                    <a href="<?php echo $link; ?>" target="_blank">
                        <img class="img-responsive" alt="Highlight of Bali Tour" class="top10_item_image" src="<?php echo $image; ?>">
                    </a>
            </div>
    </div>

    <div class="top10_item_info_wrapper">

        <p><?php echo $description; ?> <a class="" target="" href="<?php echo $link; ?>">Read More...</a></p>
<!-- Info -->

        
        <div class="clear"></div>
        </div>
        <div class="clear"></div>
        </div>
   </div><!-- /.blog-post -->    
    <?php endwhile; ?>    
<?php endif; ?>
     </div>
        <div class="col-sm-3 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <img class="img-responsive" src="http://www.tourfrombali.com/wp-content/themes/bali/images/bnr-1-1.jpg">
          </div>
          <div class="sidebar-module">
            <h4>Elsewhere</h4>
            <ol class="list-unstyled">
              <li><a href="#">GitHub</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Facebook</a></li>
            </ol>
          </div>
        </div>
    </div>
      <hr>
    <div class="row">
          <div class="col-md-12 text-center">
              <h2 class="main-title">Related Pages</h2>
          </div>
      </div>      
      <div class="row bottom-boxes">
        <div class="col-md-4">
          <img src="<?php the_field('image_1'); ?>" class="img-responsive bottom-lined">
          <h4><?php the_field('title_1'); ?></h4>
            <p class="details"><?php the_field('description_1'); ?> <a href="<?php the_field('link_1'); ?>">Read More...</a></p>
        </div>
        <div class="col-md-4">
          <img src="<?php the_field('image_2'); ?>" class="img-responsive bottom-lined">
          <h4><?php the_field('title_2'); ?></h4>
          <p class="details"><?php the_field('description_2'); ?> <a href="<?php the_field('link_2'); ?>">Read More...</a></p>
       </div>
        <div class="col-md-4">
         <img src="<?php the_field('image_3'); ?>" class="img-responsive bottom-lined">
         <h4><?php the_field('title_3'); ?></h4>
          <p class="details"><?php the_field('description_3'); ?> <a href="<?php the_field('link_3'); ?>">Read More...</a></p>
        </div>
      </div>

    </div> <!-- /container -->


<div id="footertop">
	<div class="container">
    	<div class="row">
        	<div class="col-md-8 social_icon col-xs-12">
				<ul id="ft-shr" class="list-inline">
                    <li><a href=""><img src="<?php bloginfo('template_url'); ?>/images/ico-rss.png" alt=""></a></li>
                    <li><a href="https://www.facebook.com/Tour-From-Bali-893609220714010/"><img src="<?php bloginfo('template_url'); ?>/images/ico-fb.png" alt=""></a></li>
                    <li><a href="https://twitter.com/tourfrombali?lang=en"><img src="<?php bloginfo('template_url'); ?>/images/ico-tw.png" alt=""></a></li>                            
                    <li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/ico-g.png" alt=""></a></li>
                    <li><a href="https://youtu.be/I248XKe-Pfg"><img src="<?php bloginfo('template_url'); ?>/images/ico-y.png" alt=""></a></li>                    
				</ul>
            </div>
            
			<?php  echo do_shortcode( '[contact-form-7 id="1978" title="Registration"]' ); ?>
        </div>
    </div>
</div>

<div id="footer">
	<div class="container">
    	<div class="row">
        	<div class="col-md-8">
            	<div class="row">
                	<div class="col-md-3">
                    	<ul class="list-unstyled">
                        	<li><a href="/bali/">Bali</a></li>
				<li><a href="/west-java/">West Java</a></li>
                            <li><a href="/east-java/">East Java</a></li>
                            <li><a href="/central-java/">Central Java</a></li>
                            <li><a href="/indonesia-islands/">Islands</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                    	<ul class="list-unstyled">
                        	<li><a href="/blog/">Blog</a></li>
                            <li><a class="contact_us" href="http://tourfrombali.com/contact-form/" >Custom</a></li>
                            <li><a href="/all-reviews">All reviews</a></li>
                            <li><a href="/leave-review">Leave review</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                    	<ul class="list-unstyled">
                        	<li><a href="/about-us/">About us</a></li>
                            <li><a href="/partners">Partner</a></li>
                            <li><a href="/contact">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                    	<ul class="list-unstyled">
                            <li><a href="/terms-conditions">Terms and conditions</a></li>
                            <li><a href="#">Sitemap</a></li>
<li><a href="insurance">Insurance</a></li>
                        </ul>
                    </div>
                </div>
                <div id="f-cp" class="row" >
                	<div class="col-md-12">
                    	<div><span id="f-ph" style="font-size:21px">
<div id="footer_skype">
<script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
<div id="SkypeButton_Call_ilham.bachtiar205_1"  >
 <script type="text/javascript">
 Skype.ui({
 "name": "call",
 "element": "SkypeButton_Call_ilham.bachtiar205_1",
 "participants": ["ilham.bachtiar205"],
 "imageSize": 32
 });
 </script>
</div>												 		
</div>					
<span id="footer_phone"> +62-812-8775-0954 Indonesia | +86-21-63139592 China | </span> <br> 
+217-6364689 International | </span> <br>
<br><span id="footer_q">  </span> <br>
 | QQ : 3197867671 | customerservice@tourfrombali.com </span>  <br>
INDONESIA Office: Jl. Dewi Sartika No.133, RT.5/RW.2, Cawang, Jakarta, Indonesia <br>
CHINA Office: 9th Floor, Office 960, No. 525 Xizang Middle Road, Huangpu District, Shanghai</div>
      
              
<div id="f-cpy">All rights reserved © 2015 TourfromBali.com</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center">
            	<ul id="f-qr" class="list-inline">
                	
            </div>
        </div>
        
        <div class="row clr80">
           
        </div>
        
        <div class="row clr80">
            <div class="col-md-12">
               
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <ul id="f-lgs" class="list-inline">
                    <li><img src="<?php bloginfo('template_url'); ?>/images/indonesia.jpg" alt=""></li>
                    <li><a href="http://www.aig.co.id/en/home"><img src="<?php bloginfo('template_url'); ?>/images/aig.jpg" alt=""></a></li>
                    <li><a href="http://www.worldnomads.com/travel-insurance/?gclid=Cj0KEQiAm-CyBRDx65nBhcmVtbIBEiQA7zm8lVwLUC-tqBVMi9DDJKlLm4EFEJmIrn1LPaqlo1mfYPMaAmuG8P8HAQ"><img src="<?php bloginfo('template_url'); ?>/images/world-nomads.png" alt=""></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1044452724;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1044452724/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>