<?php
$potential_sale_agent_email_content='
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting"> 
    <title>Potential Travel advisor</title>

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet">

<style>
html,
body {
    margin: 0 auto !important;
    padding: 0 !important;
    height: 100% !important;
    width: 100% !important;
    background: #f1f1f1;
}

* {
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
}

div[style*="margin: 16px 0"] {
    margin: 0 !important;
}

table,
td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
}

table {
    border-spacing: 0 !important;
    border-collapse: collapse !important;
    table-layout: fixed !important;
    margin: 0 auto !important;
}

img {
    -ms-interpolation-mode:bicubic;
}

a {
    text-decoration: none;
}

*[x-apple-data-detectors],  /* iOS */
.unstyle-auto-detected-links *,
.aBn {
    border-bottom: 0 !important;
    cursor: default !important;
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
}

.a6S {
    display: none !important;
    opacity: 0.01 !important;
}

.im {
    color: inherit !important;
}

img.g-img + div {
    display: none !important;
}

@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
    u ~ div .email-container {
        min-width: 320px !important;
    }
}

@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
    u ~ div .email-container {
        min-width: 375px !important;
    }
}

@media only screen and (min-device-width: 414px) {
    u ~ div .email-container {
        min-width: 414px !important;
    }
}
</style>

<style>
.primary{
	background: #f3a333;
}
.bg_white{
	background: #ffffff;
}
.bg_light{
	background: #e2f5f3;
}
.bg_green{
	background: #3b898a;
}
.bg_dark{
	background: rgba(0,0,0,.8);
}
.email-section{
	padding:2.5em;
}
/*BUTTON*/
.btn{
	padding: 10px 15px;
}
.btn.btn-primary{
	border-radius: 30px;
	background: #3b898a;
	color: #ffffff;
}
h1,h2,h3,h4,h5,h6{
	font-family: "Playfair Display", serif;
	color: #000000;
	margin-top: 0;
}
body{
	font-family: "Montserrat", sans-serif;
	font-weight: 400;
	font-size: 15px;
	line-height: 1.8;
	color: rgba(0,0,0,.4);
}
a{
	color: #f3a333;
}
table{
}
/*LOGO*/
.logo h1{
	margin: 0;
}
.logo h1 a{
	color: #000;
	font-size: 20px;
	font-weight: 700;
	text-transform: uppercase;
	font-family: "Montserrat", sans-serif;
}

.hero{
	position: relative;
}
.hero img{
}
.hero .text{
	color: rgba(255,255,255,.8);
}
.hero .text h2{
	color: #ffffff;
	font-size: 30px;
	margin-bottom: 0;
}
/*HEADING SECTION*/
.heading-section{
}
.heading-section h2{
	color: #000000;
	font-size: 28px;
	margin-top: 0;
	line-height: 1.4;
}
.heading-section .subheading{
	margin-bottom: 20px !important;
	display: inline-block;
	font-size: 13px;
	text-transform: uppercase;
	letter-spacing: 2px;
	color: rgba(0,0,0,.4);
	position: relative;
}
.heading-section .subheading::after{
	position: absolute;
	left: 0;
	right: 0;
	bottom: -10px;
	width: 100%;
	height: 2px;
	background: #3b898a;
	margin: 0 auto;
}
.heading-section-white{
	color: rgba(255,255,255,.8);
}
.heading-section-white h2{
	font-size: 28px;
	font-family: 
	line-height: 1;
	padding-bottom: 0;
}
.heading-section-white h2{
	color: #ffffff;
}
.heading-section-white .subheading{
	margin-bottom: 0;
	display: inline-block;
	font-size: 13px;
	text-transform: uppercase;
	letter-spacing: 2px;
	color: rgba(255,255,255,.4);
}
.icon{
	text-align: center;
}
.icon img{
}

.text-services{
	padding: 10px 10px 0; 
	text-align: center;
}
.text-services h3{
	font-size: 20px;
}

.text-services .meta{
	text-transform: uppercase;
	font-size: 14px;
}

.text-testimony .name{
	margin: 0;
}
.text-testimony .position{
	color: rgba(0,0,0,.3);
}

.img{
	width: 100%;
	height: auto;
	position: relative;
}
.img .icon{
	position: absolute;
	top: 50%;
	left: 0;
	right: 0;
	bottom: 0;
	margin-top: -25px;
}
.img .icon a{
	display: block;
	width: 60px;
	position: absolute;
	top: 0;
	left: 50%;
	margin-left: -25px;
}

.counter-text{
	text-align: center;
}
.counter-text .num{
	display: block;
	color: #ffffff;
	font-size: 34px;
	font-weight: 700;
}
.counter-text .name{
	display: block;
	color: rgba(255,255,255,.9);
	font-size: 13px;
}
/*FOOTER*/
.footer{
	color: rgba(255,255,255,.5);
}
.footer .heading{
	color: #ffffff;
	font-size: 20px;
}
.footer ul{
	margin: 0;
	padding: 0;
}
.footer ul li{
	list-style: none;
	margin-bottom: 10px;
}
.footer ul li a{
	color: rgba(255,255,255,1);
}
@media screen and (max-width: 500px) {
	.icon{
		text-align: left;
	}
	.text-services{
		padding-left: 0;
		padding-right: 20px;
		text-align: left;
	}
}
</style>


</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #222222;">
	<center style="width: 100%; background-color: #f1f1f1;">
    <div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
      &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
    </div>
    <div style="max-width: 600px; margin: 0 auto;" class="email-container">
    	<!-- BEGIN BODY -->
      <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
      	<tr>
          <td class="bg_white logo" style="padding: 1em 2.5em; text-align: center">
            <a href="https://www.travpart.com/English/travchat" target="_blank"><img src="https://www.travpart.com/English/wp-content/uploads/2018/11/newlogo.jpg" alt="travchat" width="250"></a>
          </td>
	      </tr>
	      <tr>
		      <td class="bg_white">
		        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		          <tr>
		            <td class="bg_dark email-section" style="text-align:center;">
		            	<div class="heading-section heading-section-white">
		            		<span class="subheading">Dear [name]</span>
		              	<h2>Greetings from Travpart.com!</h2>
		              	<div style="text-align:left;">
		              	<p>We are the first online travel agencies market place in the world that empowers anyone to create a tour package for sale.</p>
		              	<p>Hereby, we\'d like to gives you customers for <span style="font-size:20px;color:#3b898a"><b>FREE</b></span>!</p>
		              	<p>We just received a customer\'s request with the following request below:</p>
		              	</div>
		            	</div>
		            </td>
		          </tr><!-- end: tr -->
		          <tr>
		            <td class="bg_white email-section">
		            	<div class="heading-section" style="text-align: center; padding: 0 30px;">
		              	<p style="text-align: left">'.$csa_message.'</p>
		            	</div>
		            </td>
		          </tr><!-- end: tr -->
		          <tr>
		            <td class="bg_light email-section" style="text-align:left;color:#3b898a">
		            	<table>
		            		<tr>
		            			<td class="img">
		            				<table>
		            					<tr>
		            						<td>
		            							<p>Click <a href="'.site_url().'/travchat/user/addnewmember.php?user='.$chatUserId.'" class="btn btn-primary">here</a> and speak directly to the customers or you can visit our chatting platform to communicate with them directly at 
		            							<a href="www.travpart.com/English/travchat" target="_blank">www.travpart.com/English/travchat</a></p>
		            							<p>You can earn more customers by visiting and registering <b>as a travel advisor</b> in our platform at <a href="www.travpart.com" target="_blank">www.travpart.com</a></p>
		            							<p>Close a sales deal with our potential customers and you will receive 80% of our gross profit!</p>
		            							<p>If you want to know more about us, please contact us at <a href="mailto:contact@tourfrombali.com">contact@tourfrombali.com</a> or WA +6281287750954!</p>		            							
		            							<p>Sincerely,<br>Travpart Team</p>
		            						</td>
		            					</tr>
		            				</table>		            				
		            			</td>
		            		</tr>		            	
		            	</table>
		            </td>
		          </tr><!-- end: tr -->
		          
		        </table>

		      </td>
		    </tr><!-- end:tr -->
      <!-- 1 Column Text + Button : END -->
      </table>
      <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
      	<tr>
          <td valign="middle" class="bg_green footer email-section">
            <table>
            	<tr>
                <td valign="top" width="66.666%" style="padding-top: 20px;">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                      <td style="text-align: left; padding-right: 10px;">
                      	<h3 class="heading"><a href="https://www.travpart.com/English/register2/" target="_blank">Register Now</a></h3>
                      </td>
                    </tr>
                  </table>
                </td>               
                <td valign="top" width="33.333%" style="padding-top: 20px;">
                </td>
              </tr>
            </table>
          </td>
        </tr><!-- end: tr -->
        <tr>
        	<td valign="middle" class="bg_green footer email-section">
        		<table>
        		 <tr>
		          	<td><a href="https://play.google.com/store/apps/details?id=com.travpart.english" target="_blank"><img src="https://www.travpart.com/wp-content/uploads/2019/03/download-our-free-app.jpg" alt="download app" style="width: 100%"></a></td>
		          	</tr>
            	<tr>
                <td valign="top" width="100%">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                      <td style="text-align: left; padding-right: 10px;color:white">
                      	<p>&copy; 2019 Travchat. All Rights Reserved</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
        	</td>
        </tr>
      </table>

    </div>
  </center>
  <hr />
        <p style="text-align: center;">
            <span style="color: #999999;">
                Jl. Dewi Sartika Jakarta, RT.5/RW.2, Indonesia 13630<br />
                <a href="http://www.travpart.com">www.travpart.com</a>
            </span>
        </p>
        <p style="text-align: center;">
            <span style="color: #999999;">
                You are receiving this email because of your relationship with us.<br />
                <a href="UNSUBSCRIBE_LINK" target="_blank" rel="noopener">Unsubscribe</a> &nbsp;from travpart.<br />
            </span>
        </p>
        <p style="text-align: center;">
            &nbsp;<a href="https://www.travpart.com/English/contact/">Contact us</a>
            &nbsp;&bull; <a href="https://www.travpart.com/English/termsofuse/">Privacy&amp;&nbsp;Policy</a>
        </p>
</body>
</html>';
?>
