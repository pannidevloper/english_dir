<?php
/*
Template Name: event shedule
*/
get_header();
?>


<link href="https://www.travpart.com/English/wp-content/themes/bali/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<link href="https://www.travpart.com/English/wp-content/themes/bali/css/tokenize2.css" rel="stylesheet" />
<script type="text/javascript" src="https://www.travpart.com/English/wp-content/themes/bali/js/tokenize2.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://www.travpart.com/English/wp-content/themes/bali/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function($) {

		/*$('#datetimepicker3,#datetimepicker4').click(function(){
			$('.icon-chevron-up').addClass('fas fa-arrow-up')
			$('.icon-chevron-down').addClass('fas fa-arrow-down')
		})*/
	
		
		
		$('#datetimepicker1').datetimepicker({
			language: 'pt-BR'
		});
		$('#datetimepicker3').datetimepicker({
			pickDate: false,
			pick12HourFormat: true,
		});

		$('#datetimepicker2').datetimepicker({
			language: 'pt-BR'
		});
		$('#datetimepicker4').datetimepicker({
			pickDate: false,
			pick12HourFormat: true,
		});

		$('.tokenize-sample-demo1').tokenize2();
		$('.icon-chevron-up').addClass('fas fa-arrow-up');
		$('.icon-chevron-down').addClass('fas fa-arrow-down');
	})
</script>

<style>
	.add-on {
		cursor: pointer;
	}

	#dropdownMenu1 {
		cursor: pointer;
	}

	p {
		margin: 0px;
	}

	.width40 {
		width: 23%;
	}

	.width15 {
		text-align: center;
		font-size: 17px;
		color: grey;
		width: 13%;
	}

	.text-normal {
		color: grey;
	}

	.inline_t {
		display: inline-table;
		font-size: 12px;
		margin-left: 10px;
		vertical-align: middle;
	}

	.invite_connection_title,
	.event_type,
	.title_location {
		margin: 15px 0px;
	}

	.for_mututal {
		font-size: 12px;
		font-weight: bold
	}

	.for_mut_color {
		color: grey
	}

	.tokenize>.tokens-container {
		min-height: 65px;
		font-size: 15px;
	}

	.inputs_data {
		margin-top: 10px;
	}

	a,
	p,
	div,
	h1,
	h2,
	h3,
	h4,
	button,
	input {
		font-family: "Roboto", Sans-serif;
	}

	.shedule_event_main_wrapper {
		background: #fff;
		border: 1px solid #3333338a;
		margin-top: 10px;
	}

	.shedule_pageheading {
		background: #F5F6F8;
		padding: 10px 5px;
		border-bottom: 1px solid #ccc6;
	}

	.content_row {
		padding: 15px;
	}

	.shedule_pageheading h1 {
		color: #000 !important;
		margin: 0px;
	}

	.event_name_shedule {
		display: inline-table;
		width: 20%;
	}

	#shedule_title {
		font-size: 14px;
	}

	.event_name_shedule p,
	.invite_connection_title p,
	.event_type p,
	.title_location p {
		font-size: 17px;
		margin: 0px;
		font-weight: bold;
	}

	.event_text_shedule {
		display: inline-table;
		width: 76%;
	}

	.event_text_shedule input {
		width: 60%;
	}

	.cusomt_private_dropdown,
	.cusomt_private_dropdown .dropdown,
	.cusomt_private_dropdown .dropdown .btn {
		width: 100%;
		background: #f4f4f4;
		text-align: left;
	}

	.inline_t i {
		font-size: 23px;
	}

	.cusomt_private_dropdown .dropdown-menu li {
		margin-bottom: 10px;
		cursor: pointer;
	}

	.cusomt_private_dropdown .dropdown-menu {
		width: 100%;
	}

	.width_25 {
		display: inline-table;
		width: 25%;
		vertical-align: middle;
	}

	.width_69 {
		display: inline-table;
		vertical-align: middle;
		width: 69%;
	}

	.custom_round .btn {
		padding: 3px 10px !important;
	}

	.custom_round {
		font-weight: bold;
		text-align: center;
		border: 1px solid #333;
		width: 55%;
		margin: auto;
		border-radius: 50%;
		padding: 34px 31px;
	}

	.text_dates {
		display: inline-table;
		vertical-align: middle;
	}

	.input-icon {
		position: absolute;
		right: 6px;
		color: #80808094;
		font-size: 20px;
		top: 7px;
		/* Keep icon in center of input, regardless of the input height */
	}

	.input-wrapper {
		position: relative;
	}

	.bootstrap-datetimepicker-widget ul {
		padding: 0px;
	}

	/* On screens that are 992px or less, set the background color to blue */
	@media screen and (max-width: 992px) {
		.width_69 {
			width: 59%;
		}

		.custom_round {
			width: 80%;
		}

		.input-icon {
			top: 2px;
		}

		.custom_round {
			// padding: 60px 31px;
		}

		.width_25,
		.width40 {
			width: 40%;
		}

		.cus {
			width: 100% !important;
		}
	}

	@media(max-width: 600px) {

		.width_25,
		.width_69,
		.cus,
		.event_text_shedule,
		.event_name_shedule,
		.event_text_shedule input {
			width: 100% !important;
			margin-top: 10px;
		}

		.custom_round {
			width: 55%;
		}

		.dropdown {
			z-index: 1 !important;
		}

		.width40 {
			width: 40%;
		}

		.shedule_pageheading h1 {
			color: #000 !important;
			margin: 0px;
			font-size: 15px !important;
			margin-top: 11px;
			font-weight: bold;
		}

		.text_dates {
			font-size: 12px !important;
		}

		p,
		.text_dates {
			font-size: 11px !important;
		}

		.input-icon {
			font-size: 16px;
			top: 4px;
		}
	}
</style>

<div class="shedule_event_main_wrapper">
	<div class="container_shedule">
		<div class="shedule_pageheading">
			<h1>Shedule an Event</h1>
		</div>

		<div class="content_row">
			<div class="name_text_row">
				<div class="event_name_shedule">
					<p>Event Name</p>
				</div>
				<div class="event_text_shedule">
					<input class="form-control" id="shedule_title" name="shedule_title" placeholder="Add a short, clear name" />
				</div>
			</div>

			<div class="invite_connection_div">
				<div class="invite_connection_title">
					<p>Invite your connection</p>
				</div>
				<select class="tokenize-sample-demo1" multiple>
					<option value="Farhan" selected="selected">Farhan</option>
					<option value="Balouch">Balouch</option>
					<option value="Lix">lix</option>
					<option value="Ilhamb">Ilhamb</option>

				</select>

				<p class="for_mututal"><span class="for_mut_color">Mutual friends</span> <a href="#">Balouch</a>,<a href="#">Lix</a></p>
			</div>

			<div class="event_type">
				<p>Event Type</p>
			</div>
			<div class="cusomt_private_dropdown">
				<div class="dropdown">
					<div class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<div class="inline_t">
							<i class="fas fa-lock"></i>
						</div>
						<div class="inline_t">
							<p><strong>Private</strong></p>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>

					</div>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<li>
							<div class="inline_t">
								<i class="fas fa-lock"></i>
							</div>
							<div class="inline_t">
								<p><strong>Private</strong></p>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
							</div>
						</li>
						<li>
							<div class="inline_t">
								<i class="fas fa-globe-americas"></i>
							</div>
							<div class="inline_t">
								<p><strong>Public</strong></p>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
							</div>
						</li>
					</ul>
				</div>
			</div>


			<div class="locaion_shedule_div">
				<div class="title_location">
					<p>Location</p>
				</div>
				<div class="width_25 location_profile">
					<div class="custom_round">
						<p>location <br /> picture<br /> of video</p>
						<button class="btn btn-primary">Upload</button>
					</div>
					<div class="text-normal text-center">
						<p>Upload a picture of the <br />location's unique <br />landmark</p>
					</div>
				</div>
				<div class="width_69 location_profile">
					<div class="input_record">
						<input type="text" class="cus form-control" placeholder="(Required)" style="width: 60%" />
						<div class="inputs_data">
							<div class="text_dates width15">
								start
							</div>
							<div class="text_dates width40">
								<div id="datetimepicker1" class="input-append date">
									<div class="input-wrapper">
										<label for="stuff" class="far fa-calendar-alt add-on input-icon"></label>
										<input class="form-control" data-format="MM/dd/yyyy" type="text"></input>
									</div>
								</div>
							</div>
							<div class="text_dates width40">
								<div id="datetimepicker3" class="input-append date">
									<div class="input-wrapper">
										<label for="stuff" class="fas fa-stopwatch add-on input-icon"></label>
										<input class="form-control" data-format="hh:mm:ss" type="text"></input>
									</div>
								</div>
							</div>
						</div>

						<div class="inputs_data">
							<div class="text_dates width15">
								Ends
							</div>
							<div class="text_dates width40">
								<div id="datetimepicker2" class="input-append date">
									<div class="input-wrapper">
										<label for="stuff" class="far fa-calendar-alt add-on input-icon"></label>
										<input class="form-control" data-format="MM/dd/yyyy" type="text"></input>
									</div>
								</div>
							</div>
							<div class="text_dates width40">
								<div id="datetimepicker4" class="input-append date">
									<div class="input-wrapper">
										<label for="stuff" class="fas fa-stopwatch add-on input-icon"></label>
										<input class="form-control" data-format="hh:mm:ss" type="text"></input>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="name_text_row">
				<div class="event_name_shedule">
					<p>Tour package</p>
				</div>
				<div class="event_text_shedule">
					<input class="form-control 	cus" id="shedule_title" name="shedule_title" placeholder="(Optional)" />
				</div>
			</div>

		</div>

	</div>
</div>
<style type="text/css">
    #footer {
        display: none;
    }
</style>

<?php
get_footer();
?>