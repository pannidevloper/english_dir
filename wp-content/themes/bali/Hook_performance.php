<?php
/* Template Name: Hook Performance */
get_header();
?>
<style type="text/css">
	table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
  background-color: #f5f5f5;
}
th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
  background-color:  #ccff99;
}
.table-th td{
  background-color:  #ccff99;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<?php
function show_users_signupp(){

global $wpdb;
$dt_today= date('Y-m-d');
$dt_yesterday = date('Y-m-d',strtotime("-1 days"));

$dt_2 = date('Y-m-d',strtotime("-2 days"));
$dt_3 = date('Y-m-d',strtotime("-3 days"));
$dt_4 = date('Y-m-d',strtotime("-4 days"));
$dt_5 = date('Y-m-d',strtotime("-5 days"));
$dt_6 = date('Y-m-d',strtotime("-6 days"));
$dt_7 = date('Y-m-d',strtotime("-7 days"));


//for sellers signup

$today_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_today}%' AND username!='Guest' AND access=1");

$yesterday_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_yesterday}%' AND username!='Guest' AND access=1");

$two_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_2}%' AND username!='Guest' AND access=1");

$three_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_3}%' AND username!='Guest' AND access=1");

$four_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_4}%' AND username!='Guest' AND access=1");

$five_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_5}%' AND username!='Guest' AND access=1");

$six_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_6}%' AND username!='Guest' AND access=1");

$seven_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_7}%' AND username!='Guest' AND access=1");

// for buyers signup

$today_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_today}%' AND username!='Guest' AND access=2");

$yesterday_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_yesterday}%' AND username!='Guest' AND access=2");

$two_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_2}%' AND username!='Guest' AND access=2");

$three_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_3}%' AND username!='Guest' AND access=2");

$four_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_4}%' AND username!='Guest' AND access=2");

$five_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_5}%' AND username!='Guest' AND access=2");

$six_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_6}%' AND username!='Guest' AND access=2");

$seven_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_7}%' AND username!='Guest' AND access=2");



    ?>
<table id="UserHookPerformance">
<tr class="table-th"><td> Sellers Register </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>

  <tr>
    <td><?php echo $dt_7?></td>
    <td><?php echo $dt_6?></td>
    <td><?php echo $dt_5?></td>
    <td><?php echo $dt_4?></td>
    <td><?php echo $dt_3?></td>
    <td><?php echo $dt_2?></td>
    <td><?php echo $dt_yesterday?></td>
    <td><?php echo $dt_today ?></td>
  </tr>
  <tr>
  
      <td>
    <?php echo ($seven_days_seller); ?>
  </td>
    
    <td>
       <?php echo ($six_days_seller); ?>
    </td>
    <td>
      <?php echo ($five_days_seller); ?>
    </td>
    <td>
     <?php echo ($four_days_seller); ?>
    </td>
    <td>
      <?php echo ($three_days_seller); ?>
    </td>
    <td>
      <?php echo ($two_days_seller); ?>
    </td>
    <td>
      <?php echo ($yesterday_seller); ?>
    </td>
    <td>
     <?php echo ($today_seller); ?>
    </td>
  </tr>
  <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
  <tr class="table-th"><td> Buyers Register </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
  <tr>
    <td><?php echo $dt_7?></td>
    <td><?php echo $dt_6?></td>
    <td><?php echo $dt_5?></td>
    <td><?php echo $dt_4?></td>
    <td><?php echo $dt_3?></td>
    <td><?php echo $dt_2?></td>
    <td><?php echo $dt_yesterday?></td>
    <td><?php echo $dt_today ?></td>
   
  </tr>
  <tr>
    <td>
      <?php echo ($seven_days_buyer); ?>
    </td>
  
    <td>
     <?php echo ($six_days_buyer); ?> 
    </td>
  
    <td>
     <?php echo ($five_days_buyer); ?>
    </td>
  
    <td>
     <?php echo ($four_days_buyer); ?> 
    </td>
    
    <td>
      <?php echo ($three_days_buyer); ?> 
    </td>
    <td>
      <?php echo ($two_days_buyer); ?>
    </td>
    <td>
      <?php echo ($yesterday_buyer); ?>
    </td>
    <td>
      <?php echo ($today_buyer); ?>
    </td>
  </tr>
  <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>


<?php
}
function show_users_loginn(){

global $wpdb;
$dt_today= date('Y-m-d');
$dt_yesterday = date('Y-m-d',strtotime("-1 days"));

$dt_2 = date('Y-m-d',strtotime("-2 days"));
$dt_3 = date('Y-m-d',strtotime("-3 days"));
$dt_4 = date('Y-m-d',strtotime("-4 days"));
$dt_5 = date('Y-m-d',strtotime("-5 days"));
$dt_6 = date('Y-m-d',strtotime("-6 days"));
$dt_7 = date('Y-m-d',strtotime("-7 days"));


//for sellers signup

$today_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_today}%' AND username!='Guest' AND access=1");

$yesterday_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_yesterday}%' AND username!='Guest' AND access=1");

$two_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_2}%' AND username!='Guest' AND access=1");

$three_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_3}%' AND username!='Guest' AND access=1");

$four_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_4}%' AND username!='Guest' AND access=1");

$five_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_5}%' AND username!='Guest' AND access=1");

$six_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_6}%' AND username!='Guest' AND access=1");

$seven_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_7}%' AND username!='Guest' AND access=1");

// for buyers signup

$today_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_today}%' AND username!='Guest' AND access=2");

$yesterday_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_yesterday}%' AND username!='Guest' AND access=2");

$two_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_2}%' AND username!='Guest' AND access=2");

$three_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_3}%' AND username!='Guest' AND access=2");

$four_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_4}%' AND username!='Guest' AND access=2");

$five_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_5}%' AND username!='Guest' AND access=2");

$six_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_6}%' AND username!='Guest' AND access=2");

$seven_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE  online_time LIKE  '%{$dt_7}%' AND username!='Guest' AND access=2");



    ?>
<table id="UserHookPerformance">
  <tr class="table-th"><td> Sellers Login </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
  <tr>
    <td><?php echo $dt_7?></td>
    <td><?php echo $dt_6?></td>
    <td><?php echo $dt_5?></td>
    <td><?php echo $dt_4?></td>
    <td><?php echo $dt_3?></td>
    <td><?php echo $dt_2?></td>
    <td><?php echo $dt_yesterday?></td>
    <td><?php echo $dt_today ?></td>
   
  </tr>
  <tr>
    <td>
    <?php echo ($seven_days_seller); ?>
  </td>
    <td>
     <?php echo ($six_days_seller); ?> 
    </td>
    <td>
        <?php echo ($five_days_seller); ?>
    </td>
    <td>
      <?php echo ($four_days_seller); ?>
    </td>
    <td>
     <?php echo ($three_days_seller); ?>
    </td>
    <td>
      <?php echo ($two_days_seller); ?>
    </td>
    <td>
      <?php echo ($yesterday_seller); ?>
  
    <td>
      <?php echo ($today_seller); ?>
    </td>
  </tr>
  <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
  <tr class="table-th"><td> Buyers Login </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
  <tr>
    <td><?php echo $dt_7?></td>
    <td><?php echo $dt_6?></td>
    <td><?php echo $dt_5?></td>
    <td><?php echo $dt_4?></td>
    <td><?php echo $dt_3?></td>
    <td><?php echo $dt_2?></td>
    <td><?php echo $dt_yesterday?></td>
    <td><?php echo $dt_today ?></td>
   
  </tr>
  <tr>
    <td>
     <?php echo ($seven_days_buyer); ?>
    </td>
    <td>
      <?php echo ($six_days_buyer); ?>
    </td>
    <td>
   <?php echo ($five_days_buyer); ?>
    </td>
    <td>
      <?php echo ($four_days_buyer); ?>
    </td>
    <td>
      <?php echo ($three_days_buyer); ?>
    </td>
    <td>
      <?php echo ($two_days_buyer); ?>
    </td>
    <td>
     <?php echo ($yesterday_buyer); ?>
    </td>
    <td>
      <?php echo ($today_buyer); ?>
    </td>
  </tr>



<?php
}
if ( ! post_password_required( $post ) ) {
show_users_signupp();
show_users_loginn();
}
else{
	echo get_the_password_form();
}
//add_shortcode('hook_performace','show_users_signup');
