<?php
get_header();
/* Template Name: BIO info */

global $wpdb;

function getImgIcon($type) {
    if ($type == '0') {
        return '<i class="fa fa-plane fa-fw"></i>';
    } else if ($type == '1') {
        return '<i class="fa fa-hotel fa-fw"></i>';
    } else if ($type == '2') {
        return '<i class="fas fa-car fa-fw"></i>';
    } else if ($type == '3') {
        return '<i class="fa fa-spa fa-fw"></i>';
    } else {
        return '<i class="fas fa-helicopter fa-fw"></i>';
    }
}

$goldVoucher = $wpdb->get_results("SELECT * FROM vouchers WHERE v_type = '0'", ARRAY_A);
$silverVoucher = $wpdb->get_results("SELECT * FROM vouchers WHERE v_type = '1'", ARRAY_A);
$bronzeVoucher = $wpdb->get_results("SELECT * FROM vouchers WHERE v_type = '2'", ARRAY_A);
?>
<style>
    .game-scheme {
        margin-bottom: 60px; }
    .game-scheme .scheme-title {
        border-radius: 5px;
        font-size: 30px;
        font-weight: bold;
        letter-spacing: -0.8px;
        text-align: left;
        color: #ffffff;
        padding: 18px 30px; }
    .game-scheme .scheme-header {
        list-style: none;
        padding: 0;
        margin: 0;
        display: flex;
        font-size: 16px;
        font-weight: 600;
        margin-top: 10px;
        margin-bottom: 10px; }
    .game-scheme .scheme-header li:nth-child(1) {
        flex: 1 1 auto;
        text-align: right;
        padding-right: 70px; }
    .game-scheme .scheme-header li:nth-child(2) {
        flex: 0 0 200px;
        text-align: center; }
    .game-scheme .scheme-header li:nth-child(3) {
        flex: 0 0 200px;
        text-align: center; }
    .game-scheme .scheme-list {
        list-style: none;
        padding: 0;
        margin: 0; }
    .game-scheme .scheme-list li {
        display: flex;
        font-size: 24px;
        border: solid 1px #eeeeee;
        background-color: #ffffff;
        border-top: 0;
        padding-top: 10px;
        padding-bottom: 10px;
        align-items: center; }
    .game-scheme .scheme-list li:first-child {
        border-top: 1px solid #eee;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px; }
    .game-scheme .scheme-list li:last-child {
        border-top: 1px solid #eee;
        border-bottom-right-radius: 5px;
        border-bottom-left-radius: 5px; }
    .game-scheme .scheme-list li > div:nth-child(1) {
        flex: 1 1 auto;
        text-align: left;
        padding-left: 15px; }
    .game-scheme .scheme-list li > div:nth-child(2) {
        flex: 0 0 200px;
        text-align: center; }
    .game-scheme .scheme-list li > div:nth-child(3) {
        flex: 0 0 200px;
        text-align: center; }
    .game-scheme i.circle {
        width: 72px;
        height: 72px;
        border: solid 5px transparent;
        background-color: #fff;
        display: inline-flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        margin-right: 25px; }
    .game-scheme i.circle .fa, .game-scheme i.circle .fas {
        font-size: 36px; }

    .game-gold .scheme-title {
        background-color: #ffcc00; }

    .game-gold i.circle {
        border-color: #ffcc00;
        color: #ffcc00; }

    .game-sliver .scheme-title {
        background-color: #999999; }

    .game-sliver i.circle {
        border-color: #c7c7c7;
        color: #999999; }

    .game-bronze .scheme-title {
        background-color: #dc8f3f; }

    .game-bronze i.circle {
        border-color: #e0c2a3;
        color: #dc8f3f; }


    @media(max-width: 1199px)
    {
        .game-scheme {
            margin-bottom:50px;
        }
        #content h1 {
            color: #1A6D84;
            font-size: 28px;
        }
        .game-scheme .scheme-title
        {
            font-size: 26px;
        }
        .game-scheme .scheme-list li
        {
            font-size: 21px;
        }
        .game-scheme .scheme-title {
            padding: 16px 30px;
        }
    }

    @media(max-width: 1024px)
    {
        .game-scheme {
            margin-bottom:45px;
        }
        #content h1 {
            color: #1A6D84;
            font-size: 26px;
        }
        .game-scheme .scheme-title {
            font-size: 24px;
        }
        .game-scheme .scheme-title {
            padding: 14px 30px;
        }
        .game-scheme .scheme-list li {
            font-size: 19px;
        }
        .game-scheme i.circle {
            width: 68px;
            height: 68px;
        }
        .game-scheme .scheme-list li {
            padding-top: 7px;
            padding-bottom: 7px;
        }


    }
@media (max-width:992px)
{
    #content h1 {
        color: #1A6D84;
        font-size: 25px;
    }
    .game-scheme .scheme-title {
        font-size: 22px;
    }
    .game-scheme {
        margin-bottom: 35px;
    }
    .game-scheme .scheme-list li {
        font-size: 20px;
    }
}



    @media (max-width: 767px) {
        .game-scheme .scheme-header {
            font-size: 12px; }
        .game-scheme .scheme-header li:nth-child(1) {
            flex: 1 1 auto;
            text-align: right;
            padding-right: 10px; }
        .game-scheme .scheme-header li:nth-child(2) {
            flex: 1 1 33.3%;
            text-align: center; }
        .game-scheme .scheme-header li:nth-child(3) {
            flex: 1 1 33.3%;
            text-align: center; }
        .game-scheme .scheme-list li {
            display: flex;
            font-size: 16px; }
        .game-scheme .scheme-list li > div:nth-child(1) {
            flex: 1 1 33.3%;
            padding-left: 10px;
            text-align: left; }
        .game-scheme .scheme-list li > div:nth-child(2) {
            flex: 1 1 33.3%;
            text-align: center; }
        .game-scheme .scheme-list li > div:nth-child(3) {
            flex: 1 1 33.3%;
            text-align: center; }
        .game-scheme i.circle {
            width: 40px;
            height: 40px;
            border-width: 2px;
            margin-right: 0px; }
        .game-scheme i.circle .fa, .game-scheme i.circle .fas {
            font-size: 18px; } }
    @media only screen and (max-width: 600px)
    {
        #content .container {
            padding: 0px 15px 15px!important;
        }
    }
    @media (max-width: 576px) {
        .game-scheme {
            margin-bottom: 25px;
        }
        .game-scheme .scheme-title {
            padding: 14px 22px;
        }
        .game-scheme .scheme-title {
            font-size: 20px;
        }
        #content h1 {
            color: #1A6D84;
            font-size: 25px;
            margin-bottom: 20px;
        }
    }
    @media(max-width: 414px)
    {
        .txt_icon {
            display: grid;
        }
    }
    .game-scheme-summary {
      margin-bottom: 40px; }
    .game-scheme-summary .summary-area:first-child {
      border-radius: 5px;
      background-color: #1a6d84;
      color: #fff;
      padding: 15px;
      margin-bottom: 8px; }
    .game-scheme-summary .summary-area:first-child header {
      font-size: 24px;
      font-weight: 300;
      letter-spacing: -0.6px; }
    .game-scheme-summary .summary-area:last-child {
      border-radius: 5px;
      border: solid 1px #e5e5e5;
      background-color: #eeeeee;
      color: #333333;
      padding: 30px; }
    .game-scheme-summary .summary-area .summary-title {
      font-size: 30px;
      font-weight: bold;
      letter-spacing: -0.8px;
      text-align: left;
      line-height: 1.2;
      color: #1a6d84; }
    .game-scheme-summary .summary-area .summary-title-1 {
      font-size: 18px;
      font-weight: 300;
      line-height: normal;
      letter-spacing: -0.5px;
      text-align: left;
      margin-bottom: 10px; }
    .game-scheme-summary .summary-star-list {
      list-style: none;
      margin: 0;
      padding: 0;
      font-size: 16px;
      display: flex;
      flex-wrap: wrap;
      margin-left: -15px;
      margin-right: -15px; }
    .game-scheme-summary .summary-star-list li {
      padding: 15px; }
    .game-scheme-summary .summary-star-list li > i, .game-scheme-summary .summary-star-list li > strong, .game-scheme-summary .summary-star-list li > span {
      display: block; }
    .game-scheme-summary .summary-star-list li > strong {
      margin-top: 10px; }
    .game-scheme-summary .summary-star-list li > strong span {
      font-weight: 300 !important; }
    .game-scheme-summary .summary-star-list .fa, .game-scheme-summary .summary-star-list .fas {
      font-size: 36px;
      color: #f8b136;
      display: flex;
      align-items: center;
      justify-content: center;
      width: 51px;
      height: 51px; }
    .game-scheme-summary .summary-star-list .icon-summary-star {
      background: url("https://www.travpart.com/English/wp-content/themes/bali/images/5-star.png") center center no-repeat;
      background-size: contain;
      display: flex;
      align-items: center;
      justify-content: center;
      width: 51px;
      height: 51px;
      font-size: 14px;
      font-weight: bold;
      color: #fff;
      padding-top: 6px; }
    .game-scheme-summary .points-list {
      list-style: none;
      margin: 0;
      padding: 0; }
    .game-scheme-summary .points-list li {
      font-size: 16px;
      display: block;
      margin-top: 3px;
      border-radius: 5px;
      padding: 6px 15px; }
    .game-scheme-summary .points-list li .circle {
      border: solid 3px #e5e5e5;
      display: inline-flex;
      border-radius: 50%;
      padding: 2px;
      width: 40px;
      height: 40px;
      align-items: center;
      justify-content: center;
      float: left;
      margin-right: 8px; }
    .game-scheme-summary .points-list li .circle .fa {
      color: #e5e5e5;
      font-size: 20px; }
    .game-scheme-summary .points-list li.gold-holidays {
      background-color: #4993a7; }
    .game-scheme-summary .points-list li.silver-holidays {
      background-color: rgba(73, 147, 167, 0.5); }
    .game-scheme-summary .points-list li.bronze-holidays {
      background-color: rgba(73, 147, 167, 0.3); }
    .game-scheme-summary .holidays-title {
      font-weight: bold; }

    @media (min-width: 768px) {
      .game-scheme-summary {
        display: flex; }
      .game-scheme-summary .sep {
        border-right: 1px solid rgba(0, 0, 0, 0.2); }
      .game-scheme-summary .summary-area:first-child {
        margin-bottom: 0px;
        margin-right: 8px;
        flex: 0 1 300px; }
      .game-scheme-summary .summary-area:last-child {
        flex: 1 1 auto; }
    }

</style>
<h1>Game Scheme</h1>
  <div class="game-scheme-summary">
    <div class="summary-area">
      <header>Number of Points needed <strong>to Play</strong></header>
      <ul class="points-list">
        <li class="gold-holidays">
          <i class="circle"><i class="fa fa-trophy"></i> </i>
          <div class="holidays-title">Gold Holidays:</div>
          <div class="holidays-points">40 Points</div>
        </li>
        <li class="silver-holidays">
          <i class="circle"><i class="fa fa-trophy"></i> </i>
          <div class="holidays-title">Silver Holidays:</div>
          <div class="holidays-points">30 Points</div>
        </li>
        <li class="bronze-holidays">
          <i class="circle"><i class="fa fa-trophy"></i> </i>
          <div class="holidays-title">Bronze Holidays:</div>
          <div class="holidays-points">20 Points</div>
        </li>
      </ul>
    </div>
    <div class="summary-area">
      <div class="summary-title">How to obtain points for vouchers</div>
      <div class="summary-title-1">Feedback form Travel Advisor:</div>
      <ul class="summary-star-list">
        <li>
          <i class="icon-summary-star">5</i>
          <strong>5 Star</strong>
          <span>50 Points</span>
        </li>
        <li>
          <i class="icon-summary-star">4</i>
          <strong>4 Star</strong>
          <span>40 Points</span>
        </li>
        <li>
          <i class="icon-summary-star">3</i>
          <strong>3 Star</strong>
          <span>30 Points</span>
        </li>
        <li>
          <i class="icon-summary-star">2</i>
          <strong>2 Star</strong>
          <span>20 Points</span>
        </li>
        <li class="sep">
          <i class="icon-summary-star">1</i>
          <strong>1 Star</strong>
          <span>10 Points</span>
        </li>
        <li>
          <i class="fa fa-user fa-fw"></i>
          <strong>Login <span>Daily</span></strong>
          <span>2 Points</span>
        </li>
        <li>
          <i class="fas fa-user-clock  fa-fw"></i>
          <strong>Login <span>Weekly</span></strong>
          <span>2 Points</span>
        </li>

      </ul>
    </div>
  </div>
<?php if (!empty($goldVoucher)) { ?>
    <div class="game-scheme game-gold">
        <div class="scheme-title">Gold Scheme</div>
        <ul class="scheme-header">
            <li><a href="#">40 Points needed to play</a> </li>
            <li>No. of Vouchers available </li>
            <li>Probability</li>
        </ul>
        <ul class="scheme-list">
            <?php
            foreach ($goldVoucher as $goldV) {
                ?>
                <li>
                    <div class="txt_icon"><i class="circle">
                            <?php echo getImgIcon($goldV['v_is_for']); ?>
                        </i> <?php echo getVoucherByTypeFor($goldV['v_is_for']); ?>
                    </div>
                    <div><?php echo isset($goldV['v_redeem_limit']) ? $goldV['v_redeem_limit'] : '00' ?></div>
                    <div><?php echo isset($goldV['v_probability']) ? $goldV['v_probability'] . '%' : '00%' ?></div>
                </li>
                <?php
            }
            ?>

        </ul>
    </div>
<?php } 
if(!empty($silverVoucher)){
?>
<div class="game-scheme game-sliver">
    <div class="scheme-title">Sliver Scheme</div>
    <ul class="scheme-header">
        <li><a href="#">30 Points needed to play</a> </li>
        <li>No. of Vouchers available </li>
        <li>Probability</li>
    </ul>
    <ul class="scheme-list">

        <?php
        foreach ($silverVoucher as $silverV) {
            ?>
            <li>
                <div class="txt_icon">
                    <i class="circle">
                        <?php echo getImgIcon($silverV['v_is_for']); ?>
                    </i> <?php echo getVoucherByTypeFor($silverV['v_is_for']); ?>
                </div>
                <div><?php echo isset($silverV['v_redeem_limit']) ? $silverV['v_redeem_limit'] : '00' ?></div>
                <div><?php echo isset($silverV['v_probability']) ? $silverV['v_probability'] . '%' : '00%' ?></div>
            </li>
            <?php
        }
        ?>

       
    </ul>
</div>
<?php } 
if(!empty($bronzeVoucher)){
?>
<div class="game-scheme game-bronze">
    <div class="scheme-title">Bronze Scheme</div>
    <ul class="scheme-header">
        <li><a href="#">20 Points needed to play</a> </li>
        <li>No. of Vouchers available </li>
        <li>Probability</li>
    </ul>
    <ul class="scheme-list">
        
         <?php
        foreach ($bronzeVoucher as $bronzeV) {
            ?>
            <li>
                <div class="txt_icon"><i class="circle">
                        <?php echo getImgIcon($bronzeV['v_is_for']); ?>
                    </i> <?php echo getVoucherByTypeFor($bronzeV['v_is_for']); ?>
                </div>
                <div><?php echo isset($bronzeV['v_redeem_limit']) ? $bronzeV['v_redeem_limit'] : '00' ?></div>
                <div><?php echo isset($bronzeV['v_probability']) ? $bronzeV['v_probability'] . '%' : '00%' ?></div>
            </li>
            <?php
        }
        ?>
        
    </ul>
</div>

<?php
}
get_footer();
?>