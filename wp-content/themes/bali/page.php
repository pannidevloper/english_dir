<?php
get_header();
if(have_posts()) {
	while(have_posts()) {
		the_post();
		echo '<h1>'.get_the_title().'</h1>';
		the_content();
		
		//echo "hello";

		
		
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
	}
} else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>