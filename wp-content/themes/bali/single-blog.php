<?php get_header(); ?>
<style>
    .vote-inner {
        display: flex;
        justify-content: center;
        max-width: 500px;
        margin-left: auto;
        margin-right: auto;
    }

    .vote-inner>div {
        text-align: center;
        cursor: pointer;
        padding: 10px;
        border-right: 1px solid rgba(192, 195, 202, 0.22);
        flex: 1 1 auto;
    }

    .vote-inner>div:last-child {
        border: none;
    }

    .vote-inner>div.views {
        cursor: default;
    }

    .vote-inner>div label {
        cursor: pointer;
    }

    .vote-inner>div input {
        opacity: 0 !important;
        position: absolute;
    }

    .vote-inner>div i {
        display: block;
        font-size: 36px;
        color: #c0c3ca;
        height: 50px;
    }

    .vote-inner>div:not(.views):hover i {
        color: #1a6d84;
    }

    .vote-inner>div span {
        display: block;
        font-size: 12px;
        font-weight: bold;
        color: #333333;
        margin: 0 !important;
    }

    .vote-inner>div span span {
        font-size: 16px;
        font-weight: normal;
        color: #999999;
    }

    .commentbox {
        position: relative;
        padding: 12px 24px 12px 104px;
        border: solid 1px #d2d2d2;
        border-bottom: none;
        border-top: none;
    }

    .commentbox:first-child {
        border-top: 1px solid #d2d2d2;
    }

    .commentbox .c-name {
        font-size: 18px;
        color: #333;
    }

    .commentbox .c-date {
        font-size: 14px;
        color: #999999;
    }

    .commentbox .c-txt {
        font-size: 14px;
        font-weight: normal;
        color: #666666;
    }

    .commentbox .comment-thumb {
        position: absolute;
        left: 24px;
        top: 12px;
        width: 60px;
        height: 60px;
        border-radius: 50%;
        background-size: cover;
        background-position: center center;
    }

    .comment-section {
        max-width: 960px;
        margin-left: auto;
        margin-right: auto;
    }

    .commentbox_wrapper hr {
        margin: 0 !important;
        border-top: solid 1px #d2d2d2;
    }

    .comment-title {
        font-size: 24px;
        font-weight: bold;
        color: #333333;
        margin-bottom: 15px;
        margin-top: 40px;
    }

    .comment-form-outer textarea {
        width: 100%;
        max-width: 400px;
        min-height: 100px;
    }

    .comment-form-outer .btn-default {
        margin-top: 30px;
        color: #368a8b;
    }
</style>
<?php
if (have_posts()) {
    while (have_posts()) {
        the_post();
        //update view count
        setPostViews(get_the_ID());
        ?>
        <div class="row">
            <div class="col-sm-6 col-md-8">
                <?php
                        $post_id = get_the_ID();
                        $content = get_the_content();
                        $content = apply_filters('the_content', $content);
                        $content = str_replace(']]>', ']]&gt;', $content);
                        $content = preg_replace_callback('!(<a)(.*?href=["\']{1}http[s]?://)(.*?)([/"\'])!i', function ($matches) {
                            return stripos($matches[3], 'travpart.com') === false ? ($matches[1] . ' target="_blank" ' . $matches[2] . $matches[3] . $matches[4]) : $matches[0];
                        }, $content);
                        echo $content;

                        if (is_user_logged_in()) {
                            $current_user = wp_get_current_user();
                            $sc_user_id = isset($current_user->ID) ? $current_user->ID : '';
                            $getDataForCheck = $wpdb->get_results("SELECT * FROM `blog_social_connect` WHERE post_id='{$post_id}' AND sc_user_id = '{$sc_user_id}'", ARRAY_A);
                            $sc_type = isset($getDataForCheck[0]['sc_type']) ? $getDataForCheck[0]['sc_type'] : '';
                            //Get the count of like and dislike
                            $likecount = $wpdb->get_var("SELECT COUNT(*) FROM `blog_social_connect` WHERE `post_id` = '{$post_id}' AND `sc_type` = 0");
                            $dislikecount = $wpdb->get_var("SELECT COUNT(*) FROM `blog_social_connect` WHERE `post_id` = '{$post_id}' AND `sc_type` = 1");
                        } else {
                            $sc_user_id = 0;
                            $sc_type = '';
                            $likecount = 0;
                            $dislikecount = 0;
                        }
                        ?>

                <div class="vote-outer">
                    <form method="post" id="social_connect_form">
                        <input type="hidden" name="action" value="blog_social_connect_l_d">
                        <input type="hidden" name="post_id" value="<?= $post_id ?>">
                        <div class="vote-inner">

                            <div class="singleinput0">
                                <label class="singlepage-label" for="s1">
                                    <a target="_blank" class="a2a_dd addtoany_share_save addtoany_share" href="">
                                        <picture>
                                            <source type="image/webp" srcset="https://www.travpart.com/English/wp-content/uploads/2019/10/share2.png.webp">
                                            <img src="https://www.travpart.com/English/wp-content/uploads/2019/10/share2.png" alt="Share">
                                        </picture>
                                    </a>
                                    <span class="radio" style="margin-top: 0px;"><span>0</span> Share</span></label>
                            </div>

                            <div class="singleinput1" <?php if ($sc_type == '0') echo "style='color:red;'"; ?>>
                                <input type="radio" id="s1" name="sc_type" value="0" style="width: auto;" <?php if ($sc_type == '0') echo "checked"; ?>>
                                <label class="singlepage-label" for="s1"><i class="fa fa-thumbs-up"></i><span class="radio" style="margin-top: 0px;"><span><?php echo number_format($likecount); ?></span> Like</span></label>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $(".fa-thumbs-up").click(function() {
                                            var white = $(this).css('color');
                                            if (white === 'rgb(192, 195, 202)') {
                                                $(this).css("color", "rgb(26,106,129)");
                                            } else {
                                                $(this).css("color", 'rgb(192, 195, 202)');
                                            }
                                            var blue = $('.fa-thumbs-down').css('color');

                                            if (blue === 'rgb(26,106,129)') {
                                                $('.fa-thumbs-down').css("color", "rgb(192, 195, 202)");
                                            } else {
                                                $('.fa-thumbs-down').css("color", 'rgb(192, 195, 202)');
                                            }
                                        });


                                        $(".fa-thumbs-down").click(function() {
                                            var white = $(this).css('color');
                                            if (white === 'rgb(192, 195, 202)') {
                                                $(this).css("color", "rgb(26,106,129)");
                                            } else {
                                                $(this).css("color", 'rgb(192, 195, 202)');
                                            }
                                            var blue = $('.fa-thumbs-up').css('color');
                                            if (blue === 'rgb(26,106,129)') {
                                                $('.fa-thumbs-up').css("color", "rgb(192, 195, 202)");
                                            } else {
                                                $('.fa-thumbs-up').css("color", 'rgb(192, 195, 202)');
                                            }
                                        });
                                    });
                                </script>

                            </div>
                            <div class="singleinput2" <?php if ($sc_type == '1') echo "style='color:red;'"; ?>>
                                <input type="radio" id="s2" name="sc_type" value="1" style="width: auto;" <?php if ($sc_type == '1') echo "checked"; ?>>
                                <label class="singlepage-label" for="s2"><i class="fa fa-thumbs-down"></i><span class="radio1"><span><?php echo number_format($dislikecount); ?></span> Dislike</span></label>
                            </div>
                            <div class="views">
                                <i class="fa fa-eye"></i>
                                <span><span><?php echo number_format(getPostViews(get_the_ID())); ?></span> views</span>
                            </div>

                            <div class="button-flw">
                                <?php
                                        $followData = [];
                                        if ($sc_user_id > 0) {
                                            $followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$sc_user_id' AND sf_agent_id = '{$authordata->ID}'", ARRAY_A);
                                        }
                                        //Get follow count
                                        $follow_count = $wpdb->get_var("SELECT COUNT(*) FROM `social_follow` WHERE `sf_agent_id` = '{$authordata->ID}'");
                                        if (!empty($followData)) {
                                            ?>
                                    <div id="submitFollowAgent" class="b-follow" title="Unfollow Travel Advisor">
                                        <i class="fa fa-user-minus"></i>
                                        <span><span><?php echo number_format($follow_count); ?></span>Followers</span>
                                    </div>
                                <?php } else { ?>
                                    <div id="submitFollowAgent" class="b-follow" title="Follow Travel Advisor">
                                        <i class="fa fa-user-plus"></i>
                                        <span><span><?php echo number_format($follow_count); ?></span>Followers</span>
                                    </div>
                                <?php } ?>


                            </div>
                            <?php
                                    $blogCommentData = $wpdb->get_results("SELECT * FROM `blog_social_comments` LEFT JOIN wp_users ON wp_users.ID = scm_user_id WHERE post_id = '{$post_id}'", ARRAY_A);
                                    ?>

                            <div class="views">
                                <i class="fa fa-comment"></i>
                                <span><span><?php echo $blogCommentData ? count($blogCommentData) : '0'; ?></span> Comments</span>
                            </div>
                        </div>
                    </form>

                    <div id="fb-root"></div>
                    <script>
                        window.fbAsyncInit = function() {
                            FB.init({
                                appId: '2346029709012370',
                                status: true,
                                cookie: true,
                                xfbml: true
                            });
                        };
                        (function() {
                            var e = document.createElement('script');
                            e.async = true;
                            e.src = document.location.protocol +
                                '//connect.facebook.net/en_US/all.js';
                            document.getElementById('fb-root').appendChild(e);
                        }());

                        jQuery(document).ready(function() {
                            $('#submitFollowAgent').click(function(e) {
                                e.preventDefault();
                                var url = '<?php echo site_url() ?>/submit-ticket/';
                                var post_id = '<?php echo $post_id ?>';
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: {
                                        post_id: post_id,
                                        action: 'post_social_follow'
                                    }, // serializes the form's elements.
                                    success: function(data) {
                                        var result = JSON.parse(data);
                                        if (result.status) {
                                            alert(result.message);
                                            //window.location.reload()// show response from the php script.
                                        } else {
                                            alert(result.message);
                                            window.location.reload() // show response from the php script.
                                        }
                                    }
                                });
                            });

                            $("input[name=sc_type]").change(function() {
                                var url = 'https://www.travpart.com/English/submit-ticket/'
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: jQuery('#social_connect_form').serialize(), // serializes the form's elements.
                                    success: function(data) {
                                        var result = JSON.parse(data);
                                        if (result.status) {
                                            alert(result.message);
                                            window.location.reload()
                                        } else {
                                            window.location.reload()
                                        }
                                    }
                                });
                            });

                        });
                    </script>
                </div>

                <!-- Comment Section Start -->
                <div class="comment-section">
                    <!-- For List of comments START -->
                    <div class="commentbox_wrapper_outer">
                        <?php if (!empty($blogCommentData)) : ?>
                            <div class="comment-title"><?php echo count($blogCommentData); ?> Comments</div>
                        <?php endif; ?>
                        <div class="commentbox_wrapper">
                            <?php
                                    if (!empty($blogCommentData)) {
                                        foreach ($blogCommentData as $comment) {
                                            ?>
                                    <div class="commentbox">
                                        <div class="comment-thumb" style="background-image: url(https://www.travpart.com/Chinese/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg)"></div>
                                        <div class=" c-name">
                                            <p>
                                                <?php echo isset($comment['user_login']) ? ucfirst($comment['user_login']) : ''; ?>
                                            </p>
                                        </div>
                                        <div class="c-txt">
                                            <p>
                                                <?php echo isset($comment['scm_text']) ? $comment['scm_text'] : ''; ?>
                                            </p>
                                        </div>
                                        <div class="c-date">
                                            <?php
                                                            echo date('d M Y', strtotime($comment['scm_created']));
                                                            ?>
                                        </div>
                                    </div>
                                    <hr />
                                <?php
                                            }
                                        } else {
                                            ?>
                                <div class="comment-title">No Comment posted yet!</div>
                            <?php
                                    }
                                    ?>
                        </div>
                    </div>
                    <!-- For List of comments END -->
                    <div class="comment-form-outer">
                        <div class="comment-title">Leave a comment</div>
                        <div class="comment-form-inner">
                            <form method="POST" id="commentForm">
                                <input type="hidden" name="post_id" value="<?= $post_id ?>">
                                <input type="hidden" name="action" value="blog_social_comments_form">
                                <textarea name="scm_text" required="true" class="singletextarea"></textarea>
                                <br />
                                <button type="button" onclick="submitComments()" class="btn btn-lg btn-default">Post Comment</button>
                            </form>
                        </div>
                    </div>
                </div>
                <script>
                    function submitComments() {
                        var form = $("#commentForm");
                        var url = '<?php echo site_url() ?>/submit-ticket/';

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: form.serialize(), // serializes the form's elements.
                            success: function(data) {
                                var result = JSON.parse(data);
                                if (result.status) {
                                    alert(result.message);
                                    window.location.reload() // show response from the php script.
                                } else {
                                    alert(result.message);
                                    window.location.reload() // show response from the php script.
                                }
                            }
                        });
                    }
                </script>
                <!-- Comment Section End -->

            </div>

            <div class="col-md-4">
                <?php get_sidebar(); ?>
            </div>
        </div>

<?php }
} else {
    get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>