balitour = jQuery.noConflict();
balitour(function($) {



   //$( ".calender" ).datepicker({ dateFormat: 'dd-mm-yy' });
  if($(".custom_price").length===1)
     $(".price_value").parent().append('<input class="manual_price" type="text" value="" style="width: 80px; height: 36px;">');


   $("#left_menu_content li").click(function() {

       $("#left_menu_content li").removeClass("active");
       $(this).addClass("active");

   });
   $("#datepicker").datepicker({
       onClose: function(selectedDate) {
           var tourdays = parseInt($(".cs_tourdays").val());

           var res = selectedDate.split("-");

           var date = new Date(res[1] + "/" + res[0] + "/" + res[2]);

           date.setDate((date.getDate()) + (tourdays - 1));

           var dd = date.getDate();
           var mm = date.getMonth() + 1;
           var y = date.getFullYear();


           var someFormattedDate = dd + '-' + mm + '-' + y;

           $("#end_date").val(someFormattedDate);
           $("#end_date").datepicker("option", "minDate", someFormattedDate);


           if (!($(".hide_section").hasClass("hidden") && $(".hide_section_one_day").hasClass("hidden"))) {
               days = calc_day();
               if (days > 1) {

                   $(".hide_section").removeClass("hidden");
                   $(".hide_section_one_day").addClass("hidden");
                   $(".number_of_room").removeClass("hidden");
                   $(".number_of_room_span").removeClass("hidden");
                   $(".number_of_room_span").html("Number of rooms: ");
               } else {

                   $(".hide_section").addClass("hidden");
                   $(".hide_section_one_day").removeClass("hidden");
                   $(".number_of_room").addClass("hidden");
                   $(".number_of_room_span").addClass("hidden");
                   $(".number_of_room_span").html("Number of people: ");

               }
           }
       },
       dateFormat: 'dd-mm-yy'
   });
   $("#end_date").datepicker({
       onClose: function(selectedDate) {
           $("#datepicker").datepicker("option", "maxDate", selectedDate);


           if (!($(".hide_section").hasClass("hidden") && $(".hide_section_one_day").hasClass("hidden"))) {
               days = calc_day();
               if (days > 1) {
                   $(".hide_section").removeClass("hidden");
                   $(".hide_section_one_day").addClass("hidden");
                   $(".number_of_room_span").html("Number of rooms: ");
                   $(".number_of_room_span").removeClass("hidden");
                   $(".number_of_room").removeClass("hidden");
               } else {

                   $(".hide_section").addClass("hidden");
                   $(".hide_section_one_day").removeClass("hidden");
                   $(".number_of_room").addClass("hidden");
                   $(".number_of_room_span").addClass("hidden");
                   $(".number_of_room_span").html("Number of people: ");


               }
           }

       },
       dateFormat: 'dd-mm-yy'
   });


   var hidden_adult = $("#hidden_adult").html();
   var hidden_child = $("#hidden_child").html();

   $("input.number_of_room").click(function() {

       var datepicker = $("#datepicker").val().trim();

       if (datepicker != "") {

           days = calc_day();

           $("#datepicker").attr("style", "");

           if (days > 1) {
               $(".number_of_room_span").removeClass("hidden");
               $(".number_of_room_span").html("Number of rooms: ");
               $(".number_of_room").removeClass("hidden");
               $("select.number_of_room").removeClass("hidden");
               $(".tour_body").attr("style", "min-height:350px !important;");
               $(".hide_section").removeClass("hidden");
               $(this).remove();
           } else {

               $("select.number_of_room").removeClass("hidden");
               $(".tour_body").attr("style", "min-height:350px !important;");
               $(".hide_section_one_day").removeClass("hidden");
               $(".number_of_room").addClass("hidden");
               $(".number_of_room_span").addClass("hidden");
               $(".number_of_room_span").html("Number of people: ");
               $(this).remove();


           }

       } else {

           $("#datepicker").attr("style", "border:1px solid #ff9177");
           $("#datepicker").focus();
       }


   });

   $(".number_of_room").change(function() {


       var number_of_room = parseInt($(this).val());


       var txt = "";
       for (var i = 1; i <= number_of_room; i++) {
           txt = txt + "<div class='input_div '> <span>room " + i + ": </span>";
           txt = txt + "<select class='room_ppl_number Adult' name='Adult' >";
           txt = txt + hidden_adult;
           txt = txt + "</select>";
           txt = txt + "<select class='room_ppl_number child' name='child' >";
           txt = txt + hidden_child;
           txt = txt + "</select></div>";
       }



       $(".added_room").html(txt);

   });

   $("#datepicker").change(function() {

       var tourdays = parseInt($(".cs_tourdays").val());
       var date_val = $(this).val();
       var res = date_val.split("-");

       var date = new Date(res[1] + "/" + res[0] + "/" + res[2]);
       date.setDate(date.getDate() + (tourdays - 1));
       var dd = date.getDate();
       var mm = date.getMonth() + 1;
       var y = date.getFullYear();

       var someFormattedDate = dd + '/' + mm + '/' + y;


       $(".end_date").html(someFormattedDate);
       $(".end_date_div").removeClass("hidden");
   });

   $(document).on("change", ".Adult", function() {

       var personsperroom = $(".cs_personsperroom").val();

       var child = parseInt(personsperroom) - parseInt($(this).val());


       var txt = "<option value='0'>child</option>";
       for (var i = 1; i <= child; i++)
           txt = txt + "<option value='" + i + "'>" + i + " child </option>";

       $(this).next().html(txt);


   });

   $(".header_curreny").change(function () {

       var full_name = $(this).find("option:selected").attr("full_name");
       var syml = $(this).find("option:selected").attr("syml");

       var curreny = $(this).val();

       if (curreny == "IDR") {

           $(".change_price").each(function () {
               $(this).html(syml + "<span class='price_span'>" + $(this).attr("or_pic") + " </span>");

           });

       } else {
           $(".change_price").each(function () {
               var amount = $(this).attr("or_pic");
               if (syml == '元')
               {
                   $(this).html("<span class='price_span'>" + Math.round($('#cs_RMB').val() * amount) + " </span>" + syml);
               } else
               {
                   $(this).html(syml + "<span class='price_span'>" + ($('#cs_' + curreny).val() * Math.round(amount)).toFixed(2) + " </span>");
               }
           });
       }
   });

   $(".valid_num").keydown(function(e) {

       // Allow: backspace, delete, tab, escape, enter and .
       if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           // Allow: Ctrl+A, Command+A
           (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: home, end, left, right, down, up
           (e.keyCode >= 35 && e.keyCode <= 40)) {
           // let it happen, don't do anything
           return;
       }
       // Ensure that it is a number and stop the keypress
       if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
           e.preventDefault();
       }
   });



   $(".search_select").val("USD");
   $('.header_curreny').change();
   
});




function calc_day() {

   var datepicker = $("#datepicker").val().trim();

   var days = 0;

   if ($("#end_date").length > 0) {

       var end_date = $("#end_date").val().trim();

       if (datepicker != "") {
           var res = datepicker.split("-");
           var date = new Date(res[1] + "/" + res[0] + "/" + res[2]);
           date.setDate(date.getDate());
           var dd = date.getDate();
           var mm = date.getMonth();
           var y = date.getFullYear();

           var res1 = end_date.split("-");
           var date1 = new Date(res1[1] + "/" + res1[0] + "/" + res1[2]);
           date1.setDate(date1.getDate());
           var dd1 = date1.getDate();
           var mm1 = date1.getMonth();
           var y1 = date1.getFullYear();

           var start = new Date(y, mm, dd),
               end = new Date(y1, mm1, dd1),
               diff = new Date(end - start),
               days = Math.floor(diff / 1000 / 60 / 60 / 24);
           days = days + 1;
       }
   } else {

       days = parseInt($(".cs_tourdays").val());
   }

   return days;
}