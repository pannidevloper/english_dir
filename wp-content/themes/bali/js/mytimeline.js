window.fbAsyncInit = function () {
    FB.init({
        appId: '219814049329181',
        autoLogAppEvents: true,
        xfbml: true,
        version: 'v6.0'
    });
};
function share(href) {

    FB.ui({
        display: 'popup',
        method: 'share',
        href: href,
    }, function (response) { });
}

var website_url = $('#website-url').val();
var admin_ajax_url = $('#admin-ajax-url').val();
$(document).ready(function () {


    $('.comment-area').click(function(){
        var comment_d = $(this).data('comment');
        $('.user_comm_d_'+comment_d).toggle();
    });


    $('#toggle').click(function () {
        if ($(this).prop("checked") == true) {
            console.log("Checkbox is checked.");
            var option = 1;
        }
        else if ($(this).prop("checked") == false) {
            console.log("Checkbox is unchecked.");
            var option = 0;
        }

        $.ajax({
            type: 'POST',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'user_fb_sharing_option',
                user_option: option
            },
            success: function (data) {
                if (data.success) {
                    // window.location.href = window.location.href;
                    //alert(data.message);

                } else {
                    alert(data.message);
                }
            },
            error: function (MLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });
});



function myFunction() {
    location.replace(website_url + "/all-reviews/");
}


function getid(id) {
    $('#deleteid').attr('value', id);
}

function delete_post(id) {
    jQuery(document).ready(function () {
        var post_id = $("#deleteid").val();
        $.ajax({
            type: "POST",
            url: website_url + '/submit-ticket/',
            data: {
                post_id: post_id,
                action: 'delete_feed_post'
            }, // serializes the form's elements.
            success: function (data) {
                var result = JSON.parse(data);
                if (result.status) {
                    alert(result.message);
                    //window.location.reload()// show response from the php script.
                } else {
                    alert(result.message);
                    location.reload();
                    //window.location.reload()// show response from the php script.
                }
            }
        });
    });
}

/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function ShareD() {
    document.getElementById("myDropdown").classList.toggle("show");
}

function ShareDmboile() {
    document.getElementById("myDropdown2").classList.toggle("showM2");
}

function popupOpenClose(popup) {
    /* Add div inside popup for layout if one doesn't exist */
    
    if ($(".wrapper").length == 0) {
        $(popup).wrapInner("<div class='wrapper'></div>");
    }

    /* Open popup */
    $(popup).show();

    /* Close popup if user clicks on background */
    $(popup).click(function (e) {
        if (e.target == this) {
            if ($(popup).is(':visible')) {
                $(popup).hide();
            }
        }
    });

    /* Close popup and remove errors if user clicks on cancel or close buttons */
    $(popup).find("i[name=close]").on("click", function () {
        if ($(".formElementError").is(':visible')) {
            $(".formElementError").remove();
        }
        $(popup).hide();
    });
}

function myphFunction(data) {
    var img12 = data;
    $('img#single_pop_img').attr("src", img12);
    $(".popup div.wrapper div.row_ph").hide();
    $(".popup div.wrapper div.row1_ph").show();
    $(".ph_arr_back1").hide();
    $(".ph_arr_back2").show();
}

//share feed
function share_feed(id) {
    jQuery(document).ready(function () {
        var tour_id = id;
        $.ajax({
            type: "POST",
            url: website_url + '/submit-ticket/',
            data: {
                feed_id: tour_id,
                action: 'feedback_share'
            }, // serializes the form's elements.
            success: function (data) {
                var result = JSON.parse(data);
                if (result.status) {
                    alert(result.message);
                    //window.location.reload()// show response from the php script.
                } else {
                    alert(result.message);
                    //window.location.reload()// show response from the php script.
                }
            }
        });
    });
}

jQuery(document).ready(function ($) {
    $('.short-post-datepicker').datetimepicker();
    var short_post_location_map = new google.maps.places.Autocomplete(document.getElementById('short-post-location'));
    var short_post_location_mobile_map = new google.maps.places.Autocomplete(document.getElementById('short-post-location-mobile'));
    new google.maps.places.Autocomplete(document.getElementById('event-location'));

    short_post_location_map.addListener('place_changed', function() {
        var place = short_post_location_map.getPlace();
        $('#short-post-lat').val(place.geometry.location.lat());
        $('#short-post-lng').val(place.geometry.location.lng());
    });

    short_post_location_mobile_map.addListener('place_changed', function() {
        var place = short_post_location_mobile_map.getPlace();
        $('#short-post-lat').val(place.geometry.location.lat());
        $('#short-post-lng').val(place.geometry.location.lng());
    });


    $('#copy-link-profile').click(function () {
        $(this).find('.copy_link_profile i').css({ 'color': '#333', 'background': '#e0e4e4' });
    });


    $('#datetimepicker1 input').datetimepicker({
        language: 'en',
        pickTime: false,
        minDate: new Date(),
    });
    $('#datetimepicker3 input').datetimepicker({
        pickDate: false,
        pick12HourFormat: true,
    });

    $('#datetimepicker2 input').datetimepicker({
        language: 'en',
        pickTime: false,
        minDate: new Date(),
    });
    $('#datetimepicker4 input').datetimepicker({
        pickDate: false,
        pick12HourFormat: true,
    });

    $('.event-invited-people').tokenize2();

    //init copy link
    var clipboard = new ClipboardJS('#copy-link-profile');
    clipboard.on('success', function (e) {
        alert('Copied');
    });

    if ($('#post-id-of-comment').val() != 0) {
        $(".post-container").show();
        $(".mytime_mob_photo_gallery").hide();
        $(".photos_header_icon").css("border-bottom", "none");
        $(".timeline_header_icon").css("border-bottom", "1px solid black");
        location.href = '#shortpost' + $('#post-id-of-comment').val();
    }

    $(".submitShortPost").on('click', function () {
        var content = $("#short-post-content").val() || $("#short-post-content-mobile").val(),
            featured_image = $(".post-featured-image-id").val(),
            location = $("#short-post-location").val() || $("#short-post-location-mobile").val(),
            startdate = $("#short-post-startdate").val() || $("#short-post-startdate-mobile").val(),
            enddate = $("#short-post-enddate").val() || $("#short-post-enddate-mobile").val(),
            feeling = $("#short-post-feeling").val(),
            tagfriend = $("#short-post-tagfriend").val(),
            tourpackage = $("#short-post-tourpackage").val(),
            lookingfriend = $('#looking-for-travel-friend').val(),
            lat = $('#short-post-lat').val(),
            lng = $('#short-post-lng').val(),
            nonce = $("#shortpostnonce").val();

        $.ajaxSetup({
            cache: false
        });
        $.ajax({
            type: 'POST',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'add_short_post',
                post_content: content,
                location: location,
                startdate: startdate,
                enddate: enddate,
                feeling: feeling,
                tagfriend: tagfriend,
                tourpackage: tourpackage,
                lookingfriend: lookingfriend,
                featured_img: featured_image,
                lat: lat,
                lng: lng,
                post_nonce: nonce
            },
            success: function (data) {
                if (data.success) {
                    window.location.reload();
                    //alert(data.link);
                    if (data.fb_sharing_option == 1) {
                        share(data.link);
                    }
                } else {
                    alert(data.message);
                }
            },
            error: function (MLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });

    $('.upload-cover').click(function (e) {
        e.preventDefault();
        custom_uploader_cover = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image for Cover',
            multiple: false,
            library: {
		          type : 'image'
		    },
		    button: {
                text: 'Confirm'
            },
            
        });
        custom_uploader_cover.on('select', function () {
        	
        	
            $.ajax({
                type: 'GET',
                url: admin_ajax_url,
                dataType: 'json',
                data: {
                    action: 'upload_user_cover',
                    cover: custom_uploader_cover.state().get('selection').first().toJSON().id
                },
                success: function (data) {
                    location.reload();
                }
            });
        });
        custom_uploader_cover.open();
        if ($(this).hasClass('upload-direct')) {
            setTimeout('$(custom_uploader_cover.el).find(\'input[type="file"]\').click()', 800);
        }
    });

    $('.remove-cover').click(function (e) {
        e.preventDefault();
        $('.profile_main_cover img,.timelinecover img').attr('src', "https://www.bulgarihotels.com/.imaging/bhr-wide-small-jpg/dam/BALI/Foto-Bali_ultime-caricate/15-(3).jpg/jcr%3Acontent");

        $.ajax({
            type: 'GET',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'upload_user_cover',
                cover: -1
            },
            success: function (data) { }
        });
    });

    $('.custom_photos').hide();
    var a = 0;

    $('.add-photo-video').click(function (e) {
        var that = $(this);
        e.preventDefault();
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image/Video',
            button: {
                text: 'Confirm'
            },
            multiple: false
        });
        custom_uploader.on('select', function () {
            if (!that.attr('only')) {
                var old_ids = '';
                if (jQuery('input.post-featured-image-id').val() != -1) {
                    old_ids = jQuery('input.post-featured-image-id').val();
                }
                attachment = custom_uploader.state().get('selection').first().toJSON();
                $('.add_c').addClass('text_after_upload');
                $('.custom_photos').show();
                a++;
                if (a == 4) {
                    $('.add_img').hide();
                }
                if (attachment.type == "video") {
                    $('.buttons_row_add').prepend('<div class="image_d close_icon" id="img_delete'+a+'" style="background:none;"><i class="fas fa-times" onclick="customImageDelete(\'img_delete'+a+'\');" data-id="#img_delete'+a+'"></i><video src="' + attachment.url + '" controls="controls"></video></div>');
                }
                else {
                    $('.buttons_row_add').prepend('<div class="close_icon image_d image_' + a + ' "id="img_mob_delete'+a+'" data-class="image_1" style="background:url(' + attachment.url + ');background-size:cover;background-position: center center;margin: 0px 3px;"><i class="fas fa-times" onclick="customImageDelete(\'img_mob_delete'+a+'\');" data-id="#img_delete'+a+'"></i></div>');
                }

                jQuery('input.post-featured-image-id').val(old_ids + attachment.id + ',');
            } else {
                location.reload();
            }
        });
        custom_uploader.open();
    });

    $('.feeling-pc select,.feeling-activity-m select').change(function () {
        $('#short-post-feeling-text').text($('#short-post-feeling-text').attr('username') + ' is feeling ' + $(this).val());
        $('#mobile-short-post-feeling-text').text(' is feeling ' + $(this).val());
        $('#short-post-feeling').val($(this).val());
        $('.Mytimelinef').hide();
        $('.feeling-activity-m').hide(100);
    });

    $('.tag_friend').click(function () {
        $('#short-post-feeling-text').text($('#short-post-feeling-text').attr('username') + ' is with ' + $(this).find('.username').text());
        $('#mobile-short-post-feeling-text').text(' is with ' + $(this).find('.username').text());
        $('#short-post-tagfriend').val($(this).attr('userid'));
        $('.Mytimetags-p').hide();
        $('.tags-friend-m').hide(100);
    });

    $('.tourpackage').click(function () {
        $('#short-post-tourpackage').val($(this).attr('tour_id'));
        $('.MytimetourP').hide();
        $('.tour-packages-Popup').hide();
    });

    var has_profile = ($('#has_profile').val() == 1);
    $('.looking-travel-friend input').click(function () {
        if (!has_profile) {
            window.location.href = website_url + "/matched";
        }
        $('.Search-DatePicker').show();
        $('#looking-for-travel-friend').val($(this).attr('atype'));
    });
    $('.for_createevent_div_desktop').click(function () {
        if (!$(this).hasClass('active_desktop')) {
            $(this).addClass('active_desktop');
        } else {
            $(this).removeClass('active_desktop');
        }
    });

    $('.looking-travel-friend_desktop input').click(function () {
        if (!has_profile) {
            window.location.href = website_url + "/matched";
        }
        $('.Search-DatePicker').show();
        $('#looking-for-travel-friend').val($(this).attr('atype'));
    });

    $('.create_event,.create_event_desktop').click(function () {
        $('.mobile-popup-test2').hide();
        $('.normal-tab').hide();
        $('.event-tab').show();
    });
    
    $('.close_ev').click(function () {
        //$('.mobile-popup-test2').show();
        $('.normal-tab').show();
        $('.event-tab').hide();
    });
    
    $('.shedule_event_main_wrapper .fa-close').click(function () {
        $('.mobile-popup-test2').show();
        $('.normal-tab').show();
        $('.event-tab').hide();
    });

    $('.cusomt_private_dropdown .event-type-option').click(function () {
        $('.cusomt_private_dropdown .event-type-chosen').html($(this).html());
        $('[name="event_type"]').val($(this).attr('type'));
    });

    $('.event-location-image').click(function (e) {
        e.preventDefault();
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Confirm'
            },
            multiple: false
        });
        custom_uploader.on('select', function () {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            jQuery('.custom_round').css('background-image', 'url(' + attachment.url + ')');
            jQuery('.custom_round p').css('visibility', 'hidden');
            jQuery('[name="event_location_image_id"]').val(attachment.id);
        });
        custom_uploader.open();
    });

    jQuery('.event-tour-package').select2({
        placeholder: "(Optional)",
        allowClear: true
    });

    $('#create-event-submit').click(function () {
        if ($(this).hasClass('disabled')) {
            return;
        }
        $(this).addClass('disabled');
        var that = $(this);
        var event_name = $('[name="event_name"]').val();
        var event_invited_people = $('[name="event_invited_people"]').val();
        var event_type = $('[name="event_type"]').val();
        var event_location = $('[name="event_location"]').val();
        var event_location_image_id = $('[name="event_location_image_id"]').val();
        var start_date = $('[name="start_date"]').val() + ' ' + $('[name="start_time"]').val();
        var end_date = $('[name="end_date"]').val() + ' ' + $('[name="end_time"]').val();
        var event_tour_package = $('[name="event_tour_package"]').val();
        var nonce = $("#eventpostnonce").val();

        if (!event_location) {
            alert('Please input the location you want go');
            $(this).removeClass('disabled');
            return;
        }

        $.ajaxSetup({
            cache: false
        });
        $.ajax({
            type: 'POST',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'add_event_post',
                event_name: event_name,
                event_invited_people: event_invited_people,
                event_type: event_type,
                event_location: event_location,
                event_location_image_id,
                event_location_image_id,
                start_date: start_date,
                end_date: end_date,
                event_tour_package: event_tour_package,
                post_nonce: nonce
            },
            success: function (data) {
                if (data.success) {
                    window.location.reload();
                } else {
                    alert(data.message);
                    that.removeClass('disabled');
                }
            },
            error: function (MLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });

    $(".like-area a, .like_mobile a").click(function () {
        if ($(this).find('i').hasClass('liked')) {
            $(this).find('i').removeClass('liked');
            $(this).find('span').text(parseInt($(this).find('span').text()) - 1);
        } else {
            $(this).find('i').addClass('liked');
            $(this).find('span').text(parseInt($(this).find('span').text()) + 1);
        }
        $.ajax({
            type: "POST",
            url: website_url + '/submit-ticket',
            data: {
                feed_id: $(this).attr('feedid'),
                action: 'feedback_like'
            },
            success: function (data) { }
        });
    });

    $('.submit-comment').keydown(function (e) {
        
    
        if (e.which == 13) {
            var feed_id = $(this).find('input[name="feed_id"]').val();
            var feed_comment = $(this).find('input[name="feed_comment"]').val();
            var login_user_image = $(this).find('input[name="loginuser_image"]').val();
            var login_user_name = $(this).find('input[name="loginuser_name"]').val();
            $.ajax({
                type: 'POST',
                url: admin_ajax_url,
                dataType: 'json',
                data: {
                    action: 'feed_comment',
                    feed_id: feed_id,
                    feed_comment: feed_comment
                },
                success: function (data) {
                    if (data.success) {
                       // window.location.href = $('#current-page-url').val() + '&feed_id=' + feed_id;
  $("#comment_show"+feed_id).append('<div class="user-comment-area user-2"><img src='+login_user_image+' /><div class="another-user-section"><a href="">'+login_user_name+'</a><span>'+feed_comment+'</span></div></div>');
                   $('input[name="feed_comment"]').val('') 
                    } else {
                        alert('System is busy. Try again later.');
                    }
                }
            });
        }
    });

    $(document).on('click','.mytime_video_popup1', function (e) {
        e.preventDefault();
        var mytimevideo = $(this).data('href');
        $('html').addClass('no-scroll');
        $('body').append('<div class="mytime_video_opened"><video src="' + mytimevideo + '" controls="controls" autoplay></div>');
    });

    $('.user-cover-comment input').keyup(function(e) {
        if (e.keyCode == 13) {
            var owner = $('.user-cover-comment').attr('owner');
            var c_name = $('.user-cover-comment').attr('c_name');
            var commentator_img = $('.user-cover-comment img').attr('src');
            var comment = $('.user-cover-comment input').val();
            $('.user-cover-comment input').val('');
            $('.comment_box').append('<div class="comment_list"> <div class="img_user"> <img style="height:40px;" src="' + commentator_img + '"/> </div> <div class="user_content"> <a href="#">' + c_name + '</a> <p>' + comment + '</p> </div> </div>');
            console.log(owner, c_name, commentator_img, comment);
            $.ajax({
                type: 'POST',
                url: admin_ajax_url,
                dataType: 'json',
                data: {
                    action: 'user_cover_comment',
                    owner: owner,
                    comment: comment
                },
                success: function(data) {}
            });
        }
    });

    var pageIndex = 1;
    var page_loading = false;
    $(window).scroll(function () {
        if (($(document).scrollTop() + 1.05 * $(window).height()) > $(document).height()) {
            if (!page_loading) {
                page_loading = true;
                pageIndex++;
                $.ajax({
                    type: "GET",
                    url: admin_ajax_url,
                    data: {
                        userid: $('.mytime_mob_photo_gallery').attr('userid'),
                        pageNum: pageIndex,
                        action: 'loading_more_photo'
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (!!data.success) {
                            for (var i in data.photos) {
                                if (data.photos[i].type.indexOf('image') != -1) {
                                    $('.mytime_mob_photo_gallery').append('<div class="mytime_ph_gallery"> <img class="mytime_ph_gallery_popup" src="' + data.photos[i].url + '"> </div>');
                                    $('.mytime_desk_photo_gallery').append('<div class="mytime_desk_photo"> <a href="' + data.photos[i].url + '" class=" mytime_photo_popup"> <img class="mytime_ph_desk_popup" src="' + data.photos[i].url + '"></a> </div>');
                                } else {
                                    $('.mytime_mob_photo_gallery').append('<div class="mytime_video_desk 123-123"> <video src="' + data.photos[i].url + '"></video> <div class="post_video_overlay"><div class="post_vd_ovlay_btn"><a href="#" data-href="' + data.photos[i].url + '" class="mytime_video_popup1"><i class="fas fa-play"></i></a><i class="fas fa-volume-up"></i></div></div></div>');
                                    $('.mytime_desk_photo_gallery').append('<div class="mytime_video_desk"> <video src="' + data.photos[i].url + '"></video><div class="post_video_overlay"><div class="post_vd_ovlay_btn"><a href="#" data-href="' + data.photos[i].url + '" class="mytime_video_popup1"><i class="fas fa-play"></i></a><i class="fas fa-volume-up"></i></div></div></div>');
                                }
                            }
                            page_loading = false;
                        }
                    }
                });
            }
        }
    });

    $('.tra_expr, .in_dest, .follow-buttom, .feed-star-display').click(function () {
        var url_r = $(this).find('.col-md-9 a').attr('href');
        window.location.href = url_r;
    });

    $('.mode_d').click(function () {
        $('#myModal').modal('show');
    });


    // Feeling Activity Popup
    $(".timefeeling-popup").click(function () {
        $('.Mytimelinef').show();
    });
    $('.mytimefeelpop_close').click(function () {
        $('.Mytimelinef').hide();
    });

    // Tags People Popup
    $(".timetourpackages-popup").click(function () {
        $('.Mytimetags-p').show();
    });
    $('.tag-close').click(function () {
        $('.Mytimetags-p').hide();
    });

    // My Tour Packages Popup
    $(".MytimelineT-popup").click(function () {
        $('.MytimetourP').show();

    });
    $('.mytimetour_close').click(function () {
        $('.MytimetourP').hide();
    });

    // Post Area Mobile
    $(".input_lo").click(function () {
        $(".mobile-popup-m1").show(100);
        $(".mobile-popup-test2").css("display", "block");
        $('.main_area').hide();
    });

    $(".m-add-photo").click(function () {
        $(".mobile-popup-m1").show(100);
    });

    $(".back-post").click(function () {
        $('.main_area').show();
        $(".mobile-popup-m1").hide(100);
    });

    // feeling activity popup for mobile
    $(".feeling-t").click(function () {
        $('.feeling-activity-m').show(100);
    });
    $('.cancel-f').click(function () {
        $('.feeling-activity-m').hide(100);
    });


    // Tags Friend  popup for mobile
    $(".tags-t").click(function () {
        $('.tags-friend-m').show(100);
    });
    $('.cancel-tags').click(function () {
        $('.tags-friend-m').hide(100);
    });

    // My Tour packages People for mobile Popup
    $(".mytour-p").click(function () {
        $('.tour-packages-Popup').show();
    });

    $('.cancel-tours').click(function () {
        $('.tour-packages-Popup').hide();
    });

    $(".ShowITc").click(function () {
        $(".show_tabs").addClass("St-1");
    });

    $(".show-post-right").click(function () {
        $(".show_tabs").removeClass("St-1");
    });
    $(".drp-edit").click(function () {
        $(".editPost").addClass("edPhide");
    });

    $(".Epostright").click(function () {
        $(".editPost").removeClass("edPhide");
    });

    $(".drp-delete").click(function () {
        $(".DeletePost").addClass("DePhide");
    });

    $(".Dpostright").click(function () {
        $(".DeletePost").removeClass("DePhide");
    });


    // Open Lightbox
    $('.mytime_desk_photo_gallery').on('click', '.mytime_photo_popup', function (e) {
        e.preventDefault();
        var mytimeimage = $(this).attr('href');
        $('html').addClass('no-scroll');
        $('body').append('<div class="mytime_photo_opened"><img src="' + mytimeimage + '"></div>');
    });

    // Close Lightbox
    $('body').on('click', '.mytime_photo_opened', function () {
        $('html').removeClass('no-scroll');
        $('.mytime_photo_opened').remove();
    });
    $('.mytime_video_popup').on('click', function (e) {
        e.preventDefault();
        var mytimevideo = $(this).attr('href');
        $('html').addClass('no-scroll');
        $('body').append('<div class="mytime_video_opened"><video src="' + mytimevideo + '" controls="controls" autoplay></div>');
    });

    
    // Close Lightbox
    $('body').on('click', '.mytime_video_opened', function () {
        $('html').removeClass('no-scroll');
        $('.mytime_video_opened').remove();
    });
    $('.mytime_mob_photo_gallery').on('click', '.mytime_ph_gallery_popup', function (e) {
        e.preventDefault();
        var mytimephgallery = $(this).attr('src');
        $('html').addClass('no-scroll');
        $('body').append('<div class="mytime_ph_gallery_opened"><img src="' + mytimephgallery + '"></div>');
    });

    // Close Lightbox
    $('body').on('click', '.mytime_ph_gallery_opened', function () {
        $('html').removeClass('no-scroll');
        $('.mytime_ph_gallery_opened').remove();
    });


    $('.div_trave').click(function () {
        $('#opta').trigger('click');
    });
    $('#opta').click(function () {
        if ($(this).prop('checked') == true) {
            $('.div_trave').css('background', '#1abc9c');
        } else {
            $('.div_trave').css('background', '#00aff0');
        }
    })

    $("[data-js=open]").on("click", function () {
        popupOpenClose($(".popup"));
    });
    $(".ph_arr_back2").click(function () {
        $(".popup div.wrapper div.row_ph").show();
        $(".popup div.wrapper div.row1_ph").hide();
        $(".ph_arr_back1").show();
        $(".ph_arr_back2").hide();
    });
    $("i.fas.fa-chevron-down.connection_header_icon").click(function () {
        $(".peoples_row").show();
        $("i.fas.fa-chevron-up.connection_header_icon").show();
        $("i.fas.fa-chevron-down.connection_header_icon").hide();
    });
    $("i.fas.fa-chevron-up.connection_header_icon").click(function () {
        $(".peoples_row").hide();
        $("i.fas.fa-chevron-up.connection_header_icon").hide();
        $("i.fas.fa-chevron-down.connection_header_icon").show();
    });
    $("i.fas.fa-chevron-down.life_expr_header_icon").click(function () {
        $(".photos_grid").show();
        $("i.fas.fa-chevron-up.life_expr_header_icon").show();
        $("i.fas.fa-chevron-down.life_expr_header_icon").hide();

    });
    $("i.fas.fa-chevron-up.life_expr_header_icon").click(function () {
        $(".photos_grid").hide();
        $("i.fas.fa-chevron-up.life_expr_header_icon").hide();
        $("i.fas.fa-chevron-down.life_expr_header_icon").show();
    });
    $(".timeline_header_icon").click(function () {
        $(".post-container").show();
        $(".mytime_mob_photo_gallery").hide();
        $(".photos_header_icon").css({"border-bottom":"none","background-color":"white"});
        $(".timeline_header_icon").css({"border-bottom":"1px solid black","background-color":"lightgray"});
    });
    $(".photos_header_icon").click(function () {
        $(".post-container").hide();
        $(".mytime_mob_photo_gallery").show();
        $(".timeline_header_icon").css({"border-bottom":"none","background-color":"white"});
        $(".photos_header_icon").css({"border-bottom":"1px solid black","background-color":"lightgray"});
    });

    $('.btn_profile_dots').click(function () {
        $('.dots_box').toggle();
    });

    $('.block-user').click(function () {
        $('.dots_box').hide();
        $.ajax({
            type: 'GET',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'block_user',
                userid: $(this).attr('userid')
            },
            success: function (data) {
                if (data.status == 0) {
                    $('.block-user').text('Block');
                }
                else {
                    $('.block-user').text('Unblock');
                }
            }
        });
    });

    $('.report_send_btn input').click(function () {
        var content = '';
        $('.report_input_other_btn input').each(function () {
            if ($(this).val() != '')
                content = $(this).val();
        });
        if (!content) {
            content = $('.report_input_btn input').val();
        }
        if (!!content) {
            $.ajax({
                type: 'POST',
                url: admin_ajax_url,
                dataType: 'json',
                data: {
                    action: 'report_user',
                    userid: $(this).attr('userid'),
                    content: content
                },
                success: function (data) {
                    $(".report_custom-model-main").removeClass('report_model-open');
                }
            });
        }
    });

    $('#update-post-submit').click(function() {
        $.ajax({
            type: 'POST',
            url: 'https://www.travpart.com/English/wp-admin/admin-ajax.php',
            dataType: 'json',
            data: {
                action: 'update_short_post',
                postid: $('.editPost [name="postid"]').val(),
                post_content: $('.editPost [name="posttext"]').val(),
                feeling: $('.editPost [name="feeling"]').val(),
                tagfriend: $('.editPost [name="tag_user_id"]').val(),
                tourpackage: $('.editPost [name="edit_tourpackage"]').val(),
                featured_img: $('.editPost [name="edit_featured_image_id"]').val()
            },
            success: function (data) {
                if (data.success) {
                    window.location.reload();
                } else {
                    alert(data.message);
                }
            },
            error: function (MLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });
    
    $('.edit-post').click(function () {
        $('.editPost [name="postid"]').val($(this).attr('postid'));
        $('.editPost [name="posttext"]').val($(this).attr('content'));
        $('.editPost [name="edit_featured_image_id"]').val('-1');
        if(!$(this).attr('feeling')) {
            $('.editPost span.edit-feeling').text('');
            $('.editPost [name="feeling"]').val('');
        }
        else {
            $('.editPost span.edit-feeling').text('feeing ' + $(this).attr('feeling'));
            $('.editPost [name="feeling"]').val($(this).attr('feeling'));
        }
        if(!$(this).attr('taguserid')) {
            $('.editPost span.edit-tag-user').text('');
            $('.editPost [name="tag_user_id"]').val('');
        }
        else {
            $('.editPost span.edit-tag-user').text(' with ' + $(this).attr('taguser'));
            $('.editPost [name="tag_user_id"]').val($(this).attr('taguserid'));
        }
        if(!$(this).attr('tour')) {
            $('.editPost [name="edit_tourpackage"]').val('');
        }
        else {
            $('.editPost [name="edit_tourpackage"]').val($(this).attr('tour'));
        }
    });

    var edit_attatchment_count = 0;
    $('.edit-photo-video-button').click(function (e) {
        e.preventDefault();

        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image/Video',
            button: {
                text: 'Confirm'
            },
            multiple: false
        });
        custom_uploader.on('select', function () {
            edit_attatchment_count++;
            if(edit_attatchment_count>4) {
                return;
            }
            var old_ids = '';
            if ($('.editPost [name="edit_featured_image_id"]').val() != -1) {
                old_ids = $('.editPost [name="edit_featured_image_id"]').val();
            }
            attachment = custom_uploader.state().get('selection').first().toJSON();
            if (edit_attatchment_count == 4) {
                $('.userPhThumbnail2').hide();
            }
            if (attachment.type == "video") {
                $('.userPhThumbnail').prepend('<video src="' + attachment.url + '" controls="controls"></video>');
            }
            else {
                $('.userPhThumbnail').prepend('<img src="' + attachment.url + '" />');
            }

            $('.editPost [name="edit_featured_image_id"]').val(old_ids + attachment.id + ',');
        });
        custom_uploader.open();
    });
    
    
    $('.edit-feeling-choose select').change(function() {
        $('.editPost span.edit-tag-user').text('');
        $('.editPost [name="tag_user_id"]').val('');
        $('.editPost span.edit-feeling').text('feeing ' + $(this).val());
        $('.editPost [name="feeling"]').val($(this).val());
        $('.edit-feeling-choose').hide();
    });
    $('.edit-feeling-activity').click(function() {
        $('.edit-feeling-choose').show();
    });
    $('.editPost .fa-times-circle').click(function () {
        $('.edit-feeling-choose').hide();
    });
    $('.edit_tag_friend').click(function() {
        $('.editPost span.edit-feeling').text('');
        $('.editPost [name="feeling"]').val('');
        $('.editPost span.edit-tag-user').text(' with ' + $(this).find('.username').text());
        $('.editPost [name="tag_user_id"]').val($(this).attr('userid'));
        $('.tag-people-Popup').hide();
    });
    $('.edit-tag-people').click(function() {
        $('.tag-people-Popup').show();
    });
    $('.tag-close').click(function () {
        $('.tag-people-Popup').hide();
    });
    $('.edit-tourpackage').click(function() {
        $('.editPost [name="edit_tourpackage"]').val($(this).attr('tour_id'));
        $('.edit-tour-packages').hide();
    });
    $('.edit-tour-package-button').click(function() {
        $('.edit-tour-packages').show();
    });
    $('.edit-tour-packages .popupClose-close').click(function() {
        $('.edit-tour-packages').hide();
    });

});