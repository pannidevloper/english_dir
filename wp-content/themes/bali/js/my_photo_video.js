$(document).ready(function () {
    var admin_ajax_url = $('#admin-ajax-url').val();

    $('.photo-video-popup').click(function () {
        $('.delete_attachment').attr('postid',$(this).attr('postid'));
        $('#photo-video-popup .img_div').hide();
        $('#photo-video-popup .video_div').hide();
        if($(this).find('img').length>0) {
            $('.delete_attachment span').text('Photo');
            $('#photo-video-popup .img_div').show();
            $('#photo-video-popup .img_div img').attr('src', $(this).find('img').attr('src'));
        }
        else {
            $('.delete_attachment span').text('Video');
            $('#photo-video-popup .video_div').show();
            $('#photo-video-popup .video_div video').attr('src', $(this).find('video').attr('src'));
        }
    });

    $('.delete_attachment').click(function() {
        $.ajax({
            type: 'POST',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'user_delete_attachment',
                postid: $(this).attr('postid')
            },
            success: function (data) {
                location.reload();
            }
        });
    });

    $('.upload-cover').click(function (e) {
        e.preventDefault();
        custom_uploader_cover = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image for Cover',
            button: {
                text: 'Confirm'
            },
            multiple: false
        });
        custom_uploader_cover.on('select', function () {
            $.ajax({
                type: 'GET',
                url: admin_ajax_url,
                dataType: 'json',
                data: {
                    action: 'upload_user_cover',
                    cover: custom_uploader_cover.state().get('selection').first().toJSON().id
                },
                success: function (data) {
                    location.reload();
                }
            });
        });
        custom_uploader_cover.open();
        if ($(this).hasClass('upload-direct')) {
            setTimeout('$(custom_uploader_cover.el).find(\'input[type="file"]\').click()', 800);
        }
    });

    $('.remove-cover').click(function (e) {
        e.preventDefault();
        $('.profile_main_cover img,.timelinecover img').attr('src', "");
        $('.profile_main_cover img,.timelinecover img').css('visibility', 'hidden');

        $.ajax({
            type: 'GET',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'upload_user_cover',
                cover: -1
            },
            success: function (data) { }
        });
    });

    $('.mytime_photo_popup').on('click', function (e) {
        e.preventDefault();
        var pro_image = $('.avatar-150').attr('src');

        $('html').addClass('no-scroll');
        $('body').append('<div class="mytime_photo_opened"><img src="' + pro_image + '"></div>');
    });

    // Close Lightbox
    $('body').on('click', '.mytime_photo_opened', function () {
        $('html').removeClass('no-scroll');
        $('.mytime_photo_opened').remove();
    });
    $('.pop_ph_vd').on('click', function (e) {
        e.preventDefault();
        var photo_vd_image = $(this).attr('href');

        $('html').addClass('no-scroll');
        $('body').append('<div class="mytime_photo_vd_opened"><img src="' + photo_vd_image + '"></div>');
    });

    // Close Lightbox
    $('body').on('click', '.mytime_photo_vd_opened', function () {
        $('html').removeClass('no-scroll');
        $('.mytime_photo_vd_opened').remove();
    });
});