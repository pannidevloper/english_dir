var app = angular.module('myApp', []);
var desti_lat = '', desti_lon = '', desti_regionid = '', desti_latinFullName = '', desti_hotel_or_destination = '';
app.controller('myCtrl', function ($scope, $http) {
    $scope.showpopup = false;
    $scope.getLocation_Hint = function (val) {

        var term = $scope.desti;
        console.log("invoked");

        return $http.get('/hotel-and-flight-bookings/wp-content/themes/adivaha/api/custom-ajax.php', {
            params: {
                action: 'autoSuggetionLookup',
                locale: 'en_US',
                //term: $scope.desti
                //term: $('#desti').val()
                // term: 'Bali'
                term: term
            }
        }).then(function (response) {

            if (JSON.stringify(response.data.hotels) != "[]" && JSON.stringify(response.data.hotels) != "[]") {
                $scope.hotel_destinations = response.data.hotels;
                $scope.city_destinations = response.data.cities;
                $scope.landmarks_destinations = response.data.landmarks;
                $scope.airports_destinations = response.data.airports;
                $scope.showpopup = true;
                $('.show-autocomplete-popup').css({ 'display': 'block' });

            } else {
                $scope.showpopup = false;
                $('.show-autocomplete-popup').css({ 'display': 'none' });
            }

        });
    };

    $scope.Update_Search_Field = function (lat, lon, regionid, latinFullName, hotel_or_destination) {
        desti_lat = lat;
        desti_lon = lon;
        desti_regionid = regionid;
        desti_latinFullName = latinFullName;
        desti_hotel_or_destination = hotel_or_destination;
        $scope.lat = lat;
        $scope.lon = lon;
        $scope.desti = latinFullName;
        $('#desti').val(latinFullName);
        $scope.regionid = regionid;
        $scope.datatype = hotel_or_destination;
        $scope.showpopup = false;
    }

});









app.controller('pageLoadAppCtrl', ["$scope", "$http", "$location", function ($scope, $http, $location, $stateParams, $rootScope, $timeout, $state, $window) {
    console.log("working");
    $scope.onloadFun = function () {
        //alert(1);
    };
    $scope.SiteUrl = "https://tourfrombali.com/test-box/hotel-and-flight-bookings/";
    $scope.TemplateUrl = "https://tourfrombali.com/test-box/hotel-and-flight-bookings/wp-content/themes/adivaha";

    var search = $location.search();



    var toDayDate = new Date();
    toDayDate.setDate(toDayDate.getDate() + 10);
    /*$('.input-daterange input[name="flight_to_start"]').datepicker('setDate', toDayDate);
    $('.input-daterange input[name="flight_to_start"]').on('changeDate', function(e){
        var nextDayDate =e.target.value;
        $('.input-daterange input[name="flight_to_end"]').datepicker('setDate', nextDayDate);
        $(this).datepicker('hide');
    });

var nextDayDate = new Date();
 nextDayDate.setDate(nextDayDate.getDate()+11);
$('.input-daterange input[name="flight_to_end"]').datepicker('setDate', nextDayDate);
$('.input-daterange input[name="flight_to_end"]').on('changeDate', function(e){
    $(this).datepicker('hide');
})
*/


    $scope.flight_to_checkIn = document.getElementById("flight_to_checkIn").value;
    $scope.flight_to_checkOut = document.getElementById("flight_to_checkOut").value;




    $scope.flight_desti = search.Flights_City_From;
    $scope.flight_to_desti = search.Flights_City_to;
    $scope.flight_locationId = search.Flights_City_From_IATACODE;
    $scope.flight_to_locationId = search.Flights_City_to_IATACODE;
    //$scope.flight_to_checkIn =search.Flights_Start_Date.replace(/-/g,'/');;
    //$scope.flight_to_checkOut =search.Flights_End_Date.replace(/-/g,'/');
    //document.getElementById("flight_to_checkIn").value = search.Flights_Start_Date.replace(/-/g,'/');
    // document.getElementById("flight_to_checkOut").value = search.Flights_End_Date.replace(/-/g,'/');

    //$scope.count = parseInt(search.Flights_Adults, 10);
    //$scope.count1 = parseInt(search.Flights_Children, 10);
    //$scope.count2 = parseInt(search.Flights_Infants, 10);
    //$scope.result = search.Flights_Category_Economy;
    $scope.directcri = "enable";
    $scope.returncri = "disable";

    $scope.getLocation_Hint_Flights_From = function (val) {

        console.log("flights from");
        //var site_language =$('#site_language').val();
        var site_language = 'zh-CN';
        if (site_language == '') { var locale = 'zh-Hans'; }
        else { var locale = site_language; }
        return $http.get('//www.jetradar.com/autocomplete/places', {
            params: {
                locale: locale,
                with_countries: "false",
                q: $scope.flight_desti

            }
        }).then(function (response) {

            if (JSON.stringify(response) != "[]") {
                $(".locationpopup_flightsfrom").removeClass("hidethisinitially");
                $scope.Flight_from_destinations = response.data;
                $scope.showpopup_flightsfrom = true;
            } else {
                $scope.showpopup_flightsfrom = false;
            }

        });
    };



    $scope.getLocation_Hint_Flights_To = function (val) {
        var site_language = 'en_US';
        if (site_language == '') { var locale = 'en_US'; }
        else { var locale = site_language; }
        return $http.get('//www.jetradar.com/autocomplete/places', {
            params: {
                locale: locale,
                with_countries: "false",
                q: $scope.flight_to_desti
            }
        }).then(function (response) {
            if (JSON.stringify(response) != "[]") {
                $(".locationpopup_flightsto").removeClass("hidethisinitially");
                $scope.Flight_To_destinations = response.data;
                $scope.showpopup_flightsto = true;
            } else {
                $scope.showpopup_flightsto = false;
            }

        });
    };


    $scope.Update_Search_Field_Flights_From = function (city_code, city_fullname) {
        //alert('popup');
        $scope.flight_locationId = city_code;
        $scope.flight_desti = city_fullname;
        $scope.showpopup_flightsfrom = false;
    }
    $scope.Update_Search_Field_Flights_To = function (id, latinFullName) {
        $scope.flight_to_locationId = id;
        $scope.flight_to_desti = latinFullName;
        $scope.showpopup_flightsto = false;
    }
    $scope.showhidecheckout = true;

    $scope.Show_OneWay_Round = function (val) {
        $scope.showhidecheckout = val;
        if (val == true) {
            $scope.directcri = "enable";
            $scope.returncri = "disable";
        } else {
            $scope.directcri = "disable";
            $scope.returncri = "enable";
        }
        $('#one_way').val(val);
    }
    $scope.done = function () {
        $(".roomgroupdata").addClass("hidnumberofrooms");
        $('#showPassenger').attr('rel', 0);
    }

    $scope.showPassenger = function () {
        alert("clicked");
        $(".roomgroupdata").removeClass("hidnumberofrooms");
        $(this).attr('rel', 1);
    }





    $scope.Search_Flights = function () {
        $scope.one_way = $('#one_way').val();
        //$scope.currency = document.getElementById("currency").value;
        $scope.currency = 'CNY';
        //$scope.site_language = document.getElementById("site_language").value;
        $scope.site_language = 'zh-CN';

        var flight_to_checkIn = document.getElementById("flight_to_checkIn").value;

        //var departDate_Arr =flight_to_checkIn.split('-');
        //var departDate =departDate_Arr[2]+'-'+departDate_Arr[0]+'-'+departDate_Arr[1];
        var departDate = flight_to_checkIn;
        var flight_to_checkOut = document.getElementById("flight_to_checkOut").value;

        var returnDate_Arr = flight_to_checkOut.split('-');
        //var returnDate =returnDate_Arr[2]+'-'+returnDate_Arr[0]+'-'+returnDate_Arr[1];
        var returnDate = flight_to_checkOut;
        if ($scope.one_way == 'true') {
            returnDate = '';
        }
        var trip_class = 0;
        if ($scope.result == 'Business') {
            var trip_class = 1;
        }

        flight_to_checkIn = flight_to_checkIn.replace(/\//g, '-');
        flight_to_checkOut = flight_to_checkOut.replace(/\//g, '-');

        var currPath = $location.path();
        var statePath = currPath.replace(/\//g, "");

        if (statePath == '') { // manage for landing page
            $scope.flight_desti = $scope.flight_desti;
            $scope.flight_to_desti = $scope.flight_to_desti;
            $scope.flight_locationId = $scope.flight_locationId;
            $scope.flight_to_locationId = $scope.flight_to_locationId;
            $scope.Flights_Adult = $scope.count;
            $scope.Flights_Children = $scope.count1;
            $scope.Flights_Infants = $scope.count2;
            $scope.Flights_Category_Economy = $scope.result;
        }


        /*if($('#site_language').val()==''){
            var locale ='zh';
        }
        else{
            var locale =$('#site_language').val();
        }*/
        var locale = 'en_US';

        if (typeof ($scope.flight_desti) == 'undefined') {
            $('.location_first1').focus();
            return false;
        }
        if (typeof ($scope.flight_to_desti) == 'undefined') {
            $('.location_first2').focus();
            return false;
        }


        console.log($scope.count);

        //var param ='marker='+$('#tpMarker').val()+'&origin_name='+$scope.flight_desti+'&origin_iata='+$scope.flight_locationId+'&destination_name='+$scope.flight_to_desti+'&destination_iata='+$scope.flight_to_locationId+'&depart_date='+departDate+'&return_date='+returnDate+'&Flights_Return_direct='+$scope.directcri+'&with_request=true&adults='+$scope.count+'&children='+$scope.count1+'&infants='+$scope.count2+'&trip_class='+trip_class+'&currency='+$('#currency').val()+'&locale='+locale+'&one_way='+$scope.one_way+'&ct_guests='+$scope.ct_guests+'passenger&ct_rooms=1';
        var param = 'marker=94099&origin_name=' + $scope.flight_desti + '&origin_iata=' + $scope.flight_locationId + '&destination_name=' + $scope.flight_to_desti + '&destination_iata=' + $scope.flight_to_locationId + '&depart_date=' + departDate + '&return_date=' + returnDate + '&Flights_Return_direct=' + $scope.directcri + '&with_request=true&adults=' + $scope.count + '&children=' + $scope.count1 + '&infants=' + $scope.count2 + '&trip_class=' + trip_class + '&currency=' + $('#currency').val() + '&locale=' + locale + '&one_way=' + $scope.one_way + '&ct_guests=' + $scope.ct_guests + 'passenger&ct_rooms=1';
        //var url = document.getElementById("template_url").value+"/api/flight_update_rates.php?action=storeFlightData&"+param;
        var url = "/hotel-and-flight-bookings/wp-content/themes/adivaha/api/flight_update_rates.php?action=storeFlightData&" + param;

        $http.get(url).success(function (response) {
            //var pageName =$('#pageName').val();
            //if(pageName=='manage-flight'){
            //$window.location.href = 'https://tourfrombali.cn/test-box/yuding-jiudian-he-hangban/flights/#/?'+param;
            //window.location.reload(true);
            //}else{
            console.log("url in");
            //        $window.location.href = 'https://tourfrombali.cn/test-box/yuding-jiudian-he-hangban/flights/#/?'+param;
            //$window.location.href = document.getElementById("siteurl").value+'/flights/#/?'+param;
            //}

        });

    }







}]);



/*== Flight controller start from there ==
 var fapp = angular.module('myflightapp', []);
app.controller('pageLoadAppCtrl', function($scope, $stateParams, $http, $rootScope, $timeout, $location, $state,$window) {
  console.log("flight invoked");
   //$scope.SiteUrl = document.getElementById("siteurl").value;
   $scope.SiteUrl = "https://tourfrombali.cn/test-box/yuding-jiudian-he-hangban";
   //$scope.TemplateUrl = document.getElementById("template_url").value;
  $scope.TemplateUrl = "https://tourfrombali.cn/yuding-jiudian-he-hangban/wp-content/themes/adivaha";
   var search = $location.search();

    if (typeof(search.Flights_City_From) == "undefined") {
		   var toDayDate = new Date();
			toDayDate.setDate(toDayDate.getDate()+10);
			$('.input-daterange input[name="flight_to_start"]').datepicker('setDate', toDayDate);
			$('.input-daterange input[name="flight_to_start"]').on('changeDate', function(e){
				var nextDayDate =e.target.value;
				$('.input-daterange input[name="flight_to_end"]').datepicker('setDate', nextDayDate);
				$(this).datepicker('hide');
			});

		var nextDayDate = new Date();
		 nextDayDate.setDate(nextDayDate.getDate()+11);
		$('.input-daterange input[name="flight_to_end"]').datepicker('setDate', nextDayDate);
		$('.input-daterange input[name="flight_to_end"]').on('changeDate', function(e){
			$(this).datepicker('hide');
		})


       $scope.flight_to_checkIn =document.getElementById("flight_to_checkIn").value;
	   $scope.flight_to_checkOut =document.getElementById("flight_to_checkOut").value;

	   var currPath = $location.path();
	   var statePath = currPath.replace(/\//g, "");
	   var pageName =$('#pageName').val();
	   //alert(pageName);
	   if(pageName=='manage-flight'){ // manage for landing page
	    $scope.flight_desti = $('#wh_origin_name').val();
        $scope.flight_to_desti = $('#wh_destination_name').val();;
        $scope.flight_locationId = $('#wh_origin_iata').val();
        $scope.flight_to_locationId =$('#wh_destination_iata').val();
		$scope.Flights_Adults = $('#wh_adults').val();
        $scope.Flights_Children = $('#wh_children').val();
        $scope.Flights_Infants = $('#wh_infants').val();
        $scope.Flights_Category_Economy = $('#wh_trip_class').val();
        $scope.result = $('#wh_trip_class').val();

        $scope.Flights_Adults = $('#wh_adults').val();
        $scope.Flights_Children = $('#wh_children').val();
        $scope.Flights_Infants= $('#wh_infants').val();

		$scope.count = parseInt($scope.Flights_Adults, 10);
        $scope.count1 = parseInt($scope.Flights_Children, 10);
        $scope.count2 = parseInt($scope.Flights_Infants, 10);

		$scope.Flights_Return_direct =$('#wh_Flights_Return_direct').val();

		document.getElementById("flight_to_checkIn").value=$('#wh_depart_date').val();
		document.getElementById("flight_to_checkOut").value=$('#wh_return_date').val();
	    $scope.flight_to_checkIn =document.getElementById("flight_to_checkIn").value;
	    $scope.flight_to_checkOut =document.getElementById("flight_to_checkOut").value;

	   }
	   else{
	    var flight_origin =$('#default_flight_origin').val();
		var flight_origin_arr =flight_origin.split('/');
		var flight_destination =$('#default_flight_destination').val();
		var flight_destination_arr =flight_destination.split('/');

	    $scope.flight_desti = flight_origin_arr[0];
		$scope.flight_locationId = flight_origin_arr[1];
        $scope.flight_to_desti = flight_destination_arr[0];
        $scope.flight_to_locationId = flight_destination_arr[1];
		$scope.Flights_Adults = 1;
        $scope.Flights_Children = 0;
        $scope.Flights_Infants = 0;
        $scope.Flights_Category_Economy = "Economy";
        $scope.result = "Economy";
        $scope.count = parseInt($scope.Flights_Adults, 10);
        $scope.count1 = parseInt($scope.Flights_Children, 10);
        $scope.count2 = parseInt($scope.Flights_Infants, 10);
	   }
    } else {
        $scope.flight_desti = search.Flights_City_From;
        $scope.flight_to_desti = search.Flights_City_to;
        $scope.flight_locationId = search.Flights_City_From_IATACODE;
        $scope.flight_to_locationId = search.Flights_City_to_IATACODE;
		$scope.flight_to_checkIn =search.Flights_Start_Date.replace(/-/g,'/');;
		$scope.flight_to_checkOut =search.Flights_End_Date.replace(/-/g,'/');
        //document.getElementById("flight_to_checkIn").value = search.Flights_Start_Date.replace(/-/g,'/');
        //document.getElementById("flight_to_checkOut").value = search.Flights_End_Date.replace(/-/g,'/');
        $scope.count = parseInt(search.Flights_Adults, 10);
        $scope.count1 = parseInt(search.Flights_Children, 10);
        $scope.count2 = parseInt(search.Flights_Infants, 10);
        $scope.result = search.Flights_Category_Economy;
    }

	$scope.Search_Flights = function() {
	    $scope.one_way =$('#one_way').val();
		$scope.currency = document.getElementById("currency").value;
		$scope.site_language = document.getElementById("site_language").value;

	    var flight_to_checkIn =document.getElementById("flight_to_checkIn").value;
		var departDate_Arr =flight_to_checkIn.split('/');
		var departDate =departDate_Arr[2]+'-'+departDate_Arr[0]+'-'+departDate_Arr[1];

		var flight_to_checkOut =document.getElementById("flight_to_checkOut").value;

		var returnDate_Arr =flight_to_checkOut.split('/');
		var returnDate =returnDate_Arr[2]+'-'+returnDate_Arr[0]+'-'+returnDate_Arr[1];

		if($scope.one_way=='true'){
			returnDate='';
		}
		var trip_class=0;
		if($scope.result=='Business'){
		 var trip_class=1;
		}

		flight_to_checkIn =flight_to_checkIn.replace(/\//g,'-');
		flight_to_checkOut =flight_to_checkOut.replace(/\//g,'-');

		var currPath = $location.path();
		var statePath = currPath.replace(/\//g, "");

		if(statePath==''){ // manage for landing page
			$scope.flight_desti =$scope.flight_desti;
			$scope.flight_to_desti=$scope.flight_to_desti;
			$scope.flight_locationId =$scope.flight_locationId;
			$scope.flight_to_locationId =$scope.flight_to_locationId;
			$scope.Flights_Adult =$scope.count;
			$scope.Flights_Children =$scope.count1;
			$scope.Flights_Infants =$scope.count2;
			$scope.Flights_Category_Economy =$scope.result;
		  }


		if($('#site_language').val()==''){
			var locale ='zh';
		}
		else{
			var locale =$('#site_language').val();
		}

		if(typeof($scope.flight_desti)=='undefined'){
		   $('.location_first1').focus();
		  return false;
		 }
		if(typeof($scope.flight_to_desti)=='undefined'){
		 $('.location_first2').focus();
		 return false;
		}

		var param ='marker='+$('#tpMarker').val()+'&origin_name='+$scope.flight_desti+'&origin_iata='+$scope.flight_locationId+'&destination_name='+$scope.flight_to_desti+'&destination_iata='+$scope.flight_to_locationId+'&depart_date='+departDate+'&return_date='+returnDate+'&Flights_Return_direct='+$scope.directcri+'&with_request=true&adults='+$scope.count+'&children='+$scope.count1+'&infants='+$scope.count2+'&trip_class='+trip_class+'&currency='+$('#currency').val()+'&locale='+locale+'&one_way='+$scope.one_way+'&ct_guests='+$scope.ct_guests+'passenger&ct_rooms=1';

		var url = document.getElementById("template_url").value+"/api/flight_update_rates.php?action=storeFlightData&"+param;
	    $http.get(url).success( function(response) {
			var pageName =$('#pageName').val();
			if(pageName=='manage-flight'){
			  $window.location.href = document.getElementById("siteurl").value+'/flights/#/?'+param;
			   window.location.reload(true);
			}else{
				$window.location.href = document.getElementById("siteurl").value+'/flights/#/?'+param;
			}

		});

    }


	$scope.getLocation_Hint_Flights_From = function(val) {
		var site_language =$('#site_language').val();
		if(site_language==''){ var locale='zh-Hans';}
		else{ var locale=site_language;}
        return $http.get('//www.jetradar.com/autocomplete/places', {
            params: {
                locale: locale,
                with_countries: "false",
                q: $scope.flight_desti
            }
        }).then(function(response) {

            if (JSON.stringify(response) != "[]") {
                $(".locationpopup_flightsfrom").removeClass("hidethisinitially");
                $scope.Flight_from_destinations = response.data;
                $scope.showpopup_flightsfrom = true;
            } else {
                $scope.showpopup_flightsfrom = false;
            }

        });
    };

    $scope.getLocation_Hint_Flights_To = function(val) {
		var site_language =$('#site_language').val();
		if(site_language==''){ var locale='zh-Hans';}
		else{ var locale=site_language;}
        return $http.get('//www.jetradar.com/autocomplete/places', {
            params: {
                locale: locale,
                with_countries: "false",
                q: $scope.flight_to_desti
            }
        }).then(function(response) {
            if (JSON.stringify(response) != "[]") {
                $(".locationpopup_flightsto").removeClass("hidethisinitially");
                $scope.Flight_To_destinations = response.data;
                $scope.showpopup_flightsto = true;
            } else {
                $scope.showpopup_flightsto = false;
            }

        });
    };

    $scope.Update_Search_Field_Flights_From = function(city_code, city_fullname) {
		//alert('popup');
        $scope.flight_locationId = city_code;
        $scope.flight_desti = city_fullname;
        $scope.showpopup_flightsfrom = false;
    }
    $scope.Update_Search_Field_Flights_To = function(id, latinFullName) {
        $scope.flight_to_locationId = id;
        $scope.flight_to_desti = latinFullName;
        $scope.showpopup_flightsto = false;
    }
	$scope.showhidecheckout = true;


	// manage for landing page
    var pageName =$('#pageName').val();
	if(pageName=='manage-flight'){
	  $scope.Flights_Return_direct =$('#wh_Flights_Return_direct').val();
	  if($scope.Flights_Return_direct=='disable'){
		$scope.showhidecheckout = false;
		$scope.directcri = "disable";
        $scope.returncri = "enable";
	  }else{
		$scope.showhidecheckout = true;
		$scope.directcri = "enable";
        $scope.returncri = "disable";
	    }
	}
	else{
	 $scope.directcri = "enable";
     $scope.returncri = "disable";
	}


    $scope.Show_OneWay_Round = function(val) {
        $scope.showhidecheckout = val;
        if (val == true) {
            $scope.directcri = "enable";
            $scope.returncri = "disable";
        } else {
            $scope.directcri = "disable";
            $scope.returncri = "enable";
        }
		$('#one_way').val(val);
    }
	$scope.done =function(){
		$(".roomgroupdata").addClass("hidnumberofrooms");
		$('#showPassenger').attr('rel',0);
	}

	$scope.showPassenger =function(){
		 $(".roomgroupdata").removeClass("hidnumberofrooms");
		 $(this).attr('rel',1);
	}

});
*/

/*== Flight controller ends from there ==*/























/*balitour2 = jQuery.noConflict();
/*balitour2(function ($) {

var desti='';
	var terms='';
	var terms_arr;
	if($('#myInput').val().length>1)
	{
		terms_arr=$('#myInput').val().split('|');
		terms=terms_arr[0];
		$.getJSON('/yuding-jiudian-he-hangban/wp-content/themes/adivaha/api/custom-ajax.php', {
			action: 'autoSuggetionLookup',
			locale: 'zh_CN',
			term: terms
		}).done(function (data) {

          	desti=terms+'&lat='+data.cities[0].lat+'&lon='+data.cities[0].lon;

		});
	}


});
*/

$(document).ready(function () {
    $('#flightpart').css({ 'display': 'none' });
    $(".show-autocomplete-popup").click(function () {
        $('.show-autocomplete-popup').css({ 'display': 'none' });
    });
});

function myHotel() {
    $('#hotelpart').css({ 'display': 'block' });
    $('#flightpart').css({ 'display': 'none' });



}
function myCar() {
    //alert("car selected");
    $('.elementor-element.elementor-element-f0f731.elementor-section-stretched.elementor-section-full_width.elementor-section-height-min-height.elementor-section-height-default.elementor-section-items-middle.elementor-section.elementor-top-section').css({ 'display': 'none' });
    $('#hotelpart').css({ 'display': 'none' });
    $('#flightpart').css({ 'display': 'none' });


}
function myFlight() {
    $('#hotelpart').css({ 'display': 'none' });
    $('#flightpart').css({ 'display': 'block' });
}




$(".elementor-button-link.elementor-button.elementor-size-xl").click(function () {
    // alert("button clicked");
});

var startElm = document.getElementById('startdate');
var endElm = document.getElementById('enddate');
window.datepickerTips =new Tooltip(startElm,{title:'please choose check in date',trigger:'manual',placement:'top-start',boundariesElement:'body',container:'body',template:'<div class="popper-tooltip" role="tooltip" style="max-width: 140px;"><div class="tooltip__arrow"></div><div class="tooltip__inner"></div></div>'});

window.endTips =new Tooltip(endElm,{title:'please choose check out date',trigger:'manual',placement:'top-start',boundariesElement:'body',container:'body',template:'<div class="popper-tooltip" role="tooltip"  style="max-width: 140px;"><div class="tooltip__arrow"></div><div class="tooltip__inner"></div></div>'});
startElm.addEventListener('focus',function () {
    datepickerTips.hide();
});
endElm.addEventListener('focus',function () {
    endTips.hide();
});


$(".searchButton2").click(function () {
    var datepicker = $("#startdate").val().trim();
    var end_date = 0;
    if ($("#enddate").length > 0) {
        end_date = $("#enddate").val().trim();
    }
    if(!end_date || !datepicker) {
        console.info(datepickerTips);
        datepickerTips.show();
        endTips.show();
        return false;
    }

    var res = datepicker.split("-");
    var checkin = res[1] + "-" + res[2] + "-" + res[0];

    var res2 = end_date.split("-");
    var checkout = res2[1] + "-" + res2[2] + "-" + res2[0];

    /*var number_of_adult = 0;
    var string_of_adult = '';
    var number_of_child = 0;
    var string_of_child = '';
    var number_of_extra_bed = 0;
    var sign_s = 0;
    var string_of_childAge = '';

    $('.adult input').each(function () {
        if (sign_s == 0) {
            string_of_adult = string_of_adult + $(this).val();
        }
        else {
            string_of_adult = string_of_adult + ',' + $(this).val();
        }
        sign_s++;
        number_of_adult += parseInt($(this).val());
    });
    sign_s = 0;
    $('.child input').each(function () {
        if (sign_s == 0) {
            string_of_child = string_of_child + $(this).val();
        }
        else {
            string_of_child = string_of_child + ',' + $(this).val();
        }
        number_of_child += parseInt($(this).val());
        if (parseInt($(this).val()) >= 1) {
            if (string_of_childAge.length == 0) {
                string_of_childAge = string_of_childAge + sign_s + '_12';
            }
            else {
                string_of_childAge = string_of_childAge + '-' + sign_s + '_12';
            }
        }
        for (var i = 1; i < parseInt($(this).val()); i++) {
            string_of_childAge = strings_of_childAge + ',12';
        }
        sign_s++;
    });*/
    var regionid;
    var desti;
    var terms = '';
    var terms_arr = '';
    $.ajaxSettings.async = false;
    if (!desti_hotel_or_destination || desti_hotel_or_destination == 'location') {
        if (desti_lon && desti_lat) {
            desti = desti_latinFullName + '&lat=' + desti_lat + '&lon=' + desti_lon;
        }
        else if ($('#desti').val().length > 1) {
            terms_arr = $('#desti').val().split(',');
            terms = terms_arr[0];

            $.getJSON('/hotel-and-flight-bookings/wp-content/themes/adivaha/api/custom-ajax.php', {
                action: 'autoSuggetionLookup',
                locale: 'en_US',
                term: terms
            }, function (data) {
				if(data.cites!=undefined && data.cities[0].lat && data.cities[0].lon)
					desti = terms + '&lat=' + data.cities[0].lat + '&lon=' + data.cities[0].lon;
				else if(data.hotels!=undefined && data.hotels[0].regionid) {
					desti_hotel_or_destination='hotel';
					desti_regionid=data.hotels[0].regionid;
				}
            });
        }
    }
    function getCookie(name) {
		var strCookie = document.cookie;
		var arrCookie = strCookie.split("; ");
		for (var i = 0; i < arrCookie.length; i++) {
			var arr = arrCookie[i].split("=");
			if (arr[0] == name)
				return unescape(arr[1]);
		}
		return "";
	} 

    if (parseInt(getCookie('tour_id')) > 1) {
        if (!desti) {
            desti = "Bali,%20Indonesia&lat=-8.4095178&lon=115.18891600000006";
        }

        if (!desti_hotel_or_destination || desti_hotel_or_destination == 'location') {
            document.location = 'https://www.travpart.com/hotel-and-flight-bookings/?tour_id=' + parseInt(getCookie('tour_id')) + "&travcust=1" + '#/search/?fn=search&desti=' + desti + '&checkIn=' + checkin + '&checkOut=' + checkout + '&language=en_US&currency=USD&hotelType=1&rooms=1&adults=1&childs=0';
        }
        else {
            document.location = 'https://www.travpart.com/hotel-and-flight-bookings/?tour_id=' + parseInt(getCookie('tour_id')) + "&travcust=1" + '#/hotel-information/' + desti_regionid + '/?fn=hotelInfo&checkIn=' + checkin + '&checkOut=' + checkout + '&language=en_US&currency=USD&hotelType=1&rooms=1&adults=1&childs=0&is_custom=0';
        }
    }
    $.ajaxSettings.async = true;
});




/* drop down list */