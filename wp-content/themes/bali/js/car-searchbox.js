$(function () {
    $('.getLocationCarsFrom').keyup(function () {
        if (!$(this).val())
            return;
        $.getJSON("https://travelbeesapi.com/webServices/cars/api.php",
            {
                action: "getCarLocation",
                term: $(this).val()
            }, function (result) {
				if(!result.airports && !result.cities && $.isEmptyObject(result.airports) && $.isEmptyObject(result.cities))
					return;
				$('.carLocationList a').remove();
				if(!!result.airports) {
					$.each(result.airports, function(i,item) {
						$('.carLocationList').append('<a class="autocomplete-dropdown carLocationFromItem" regionid="'+item.id+'">'+item.latinFullName+'</a>');
					});
				}
				if(!!result.cities) {
					$.each(result.cities, function(i,item) {
						$('.carLocationList').append('<a class="autocomplete-dropdown carLocationFromItem" regionid="'+item.id+'">'+item.latinFullName+'</a>');
					});
				}
				$('.carLocationList').show();
            });
    });
	
	$('.carLocationList').on('click', '.carLocationFromItem', function(){
		$('.getLocationCarsFrom').val($(this).text());
		$('#carLocationFrom').val($(this).attr('regionid'));
	});
	
	$('.getLocationCarsTo').keyup(function () {
        if (!$(this).val())
            return;
        $.getJSON("https://travelbeesapi.com/webServices/cars/api.php",
            {
                action: "getCarLocation",
                term: $(this).val()
            }, function (result) {
				if(!result.airports && !result.cities && $.isEmptyObject(result.airports) && $.isEmptyObject(result.cities))
					return;
				$('.carLocationList a').remove();
				if(!!result.airports) {
					$.each(result.airports, function(i,item) {
						$('.carLocationList').append('<a class="autocomplete-dropdown carLocationToItem" regionid="'+item.id+'">'+item.latinFullName+'</a>');
					});
				}
				if(!!result.cities) {
					$.each(result.cities, function(i,item) {
						$('.carLocationList').append('<a class="autocomplete-dropdown carLocationToItem" regionid="'+item.id+'">'+item.latinFullName+'</a>');
					});
				}
				$('.carLocationList').show();
            });
    });
	
	$('.carLocationList').on('click', '.carLocationToItem', function(){
		$('.getLocationCarsTo').val($(this).text());
		$('#carLocationTo').val($(this).attr('regionid'));
	});
	
	$('#AdivahaCartrawlerSubmit').click(function(){
		if($('#carLocationFrom').val()<1 || $('.getLocationCarsFrom').val()<1)
			$('.getLocationCarsFrom').focus();
		else if($('#carLocationTo').val()<1 || $('.getLocationCarsTo').val()<1)
			$('.getLocationCarsTo').focus();
		else if($('#carPickupDate').val()<1)
			$('#carPickupDate').focus();
		else if($('#carPickupTime').val()<1)
			$('#carPickupTime').focus();
		else if($('#carReturnDate').val()<1)
			$('#carReturnDate').focus();
		else if($('#carReturnTime').val()<1)
			$('#carReturnTime').focus();
		else {
			var baseurl='https://www.travpart.com/car/car-hire/?clientID=674902&CT=MP&ln=en&age=30&passNum=0&curr=USD&carGroupID=0&residenceID=&countryID=US&elID=631493726382224';
			//set tourid cookie
			var exdate=new Date();
			exdate.setDate(exdate.getDate()+1);
			if (parseInt(getCookie('tour_id')) > 1) {
				document.cookie="tour_id=" + getCookie('tour_id') + "; expires="+exdate.toGMTString()+ ";domain=.travpart.com;path=/car/";
				document.location=baseurl+'&pickupID='+$('#carLocationFrom').val()+'&pickupName='+$('.getLocationCarsFrom').val()+
								'&returnID='+$('#carLocationTo').val()+'&returnName='+$('.getLocationCarsTo').val()+
								'&pickupDateTime='+$('#carPickupDate').val()+'T'+$('#carPickupTime').val()+
								'&returnDateTime='+$('#carReturnDate').val()+'T'+$('#carReturnTime').val()+'#/vehicles';
			}
		}
	});
	
});