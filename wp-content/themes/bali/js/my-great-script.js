jQuery( document ).ready(function() {

	jQuery(".clickonwiretransfer").click(function(){
		var book_id = jQuery(".clickonwiretransfer").attr("data-bookcode");
		var themeuri = jQuery(".clickonwiretransfer").attr("data-themeuri");

		$.ajax({
			type: "POST",
  			url: themeuri+"/runwiretransferbtn.php",
  			data: {'action': 'diklat', 'book_id':book_id},
  			success: function(data) {
            	if(data=="done"){
            		jQuery(".clickonwiretransfer").attr("disabled", "disabled").text('Paid');
            	}
        	},
        	error: function() {
            	alert('Error occured');
        	}
		})
		
	});


  /* 1. Visualizing things on Hover - See next part for action on click */
  jQuery('#stars li').on('mouseover', function(){
    var onStar = parseInt(jQuery(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    jQuery(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        jQuery(this).addClass('hover');
      }
      else {
        jQuery(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    jQuery(this).parent().children('li.star').each(function(e){
      jQuery(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  jQuery('#stars li').on('click', function(){
    var onStar = parseInt(jQuery(this).data('value'), 10); // The star currently selected
    var stars = jQuery(this).parent().children('li.star');

    for (i = 0; i < stars.length; i++) {
      jQuery(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      jQuery(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt(jQuery('#stars li.selected').last().data('value'), 10);
    var bookingid = jQuery('#stars li.selected').parent().data("id");
    var msg = ratingValue + " stars";
    jQuery('.success-box').fadeIn(200);  
    jQuery('.success-box div.text-message').html("<span>" + msg + "</span>");
    
  });


  
});