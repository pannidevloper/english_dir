window.fbAsyncInit = function () {
    FB.init({
        appId: '219814049329181',
        autoLogAppEvents: true,
        xfbml: true,
        version: 'v6.0'
    });
};
function share(href) {

    FB.ui({
        display: 'popup',
        method: 'share',
        href: href,
    }, function (response) { });
}
var website_url = $('#website-url').val();
var admin_ajax_url = $('#admin-ajax-url').val();

$(document).ready(function () {
    $('#toggle').click(function () {
        if ($(this).prop("checked") == true) {
            console.log("Checkbox is checked.");
            var option = 1;
        }
        else if ($(this).prop("checked") == false) {
            console.log("Checkbox is unchecked.");
            var option = 0;
        }

        $.ajax({
            type: 'POST',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'user_fb_sharing_option',
                user_option: option
            },
            success: function (data) {
                if (data.success) {
                    // window.location.href = window.location.href;
                    //alert(data.message);

                } else {
                    alert(data.message);
                }
            },
            error: function (MLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });
});

//share feed
function share_feed(id) {
    jQuery(document).ready(function ($) {
        var tour_id = id;
        $.ajax({
            type: "POST",
            url: website_url + '/submit-ticket/',
            data: {
                feed_id: tour_id,
                action: 'feedback_share'
            }, // serializes the form's elements.
            success: function (data) {
                var result = JSON.parse(data);
                if (result.status) {
                    alert(result.message);
                    window.location.reload() // show response from the php script.
                } else {
                    alert(result.message);
                    //window.location.reload()// show response from the php script.
                }
            }
        });
    });
}

$(document).ready(function () {

    var short_post_location_map = new google.maps.places.Autocomplete(document.getElementById('short-post-location'));
    var short_post_location_mobile_map = new google.maps.places.Autocomplete(document.getElementById('short-post-location-mobile'));
    new google.maps.places.Autocomplete(document.getElementById('event-location'));

    short_post_location_map.addListener('place_changed', function() {
        var place = short_post_location_map.getPlace();
        $('#short-post-lat').val(place.geometry.location.lat());
        $('#short-post-lng').val(place.geometry.location.lng());
    });

    short_post_location_mobile_map.addListener('place_changed', function() {
        var place = short_post_location_mobile_map.getPlace();
        $('#short-post-lat').val(place.geometry.location.lat());
        $('#short-post-lng').val(place.geometry.location.lng());
    });

    var $container = '';
    jQuery('#loading_next_page').imagesLoaded(function () {
        $container = jQuery('#loading_next_page').masonry({
            itemSelector: '.box',
            columnWidth: 5
        });
    });

    var pageIndex = 1;
    var page_loading = false;
    $(window).scroll(function () {
        if (($(document).scrollTop() + 1.5 * $(window).height()) > $(document).height()) {
            if (!page_loading) {
                page_loading = true;
                pageIndex++;
                $.ajax({
                    type: "GET",
                    url: admin_ajax_url,
                    data: {
                        pageNum: pageIndex,
                        action: 'loading_more_shortpost'
                    },
                    success: function (data) {
                        if (data != 0) {
                            page_loading = false;
                            var $content = $(data);
                            $container.append($content).masonry('appended', $content);
                            //jQuery('.event_button .dropdown-toggle').dropdown();
                        }
                    }
                });
            }
        }
    });

    $(".submitShortPost").on('click', function () {

        var content = $("#short-post-content").val() || $("#short-post-content-mobile").val(),
            featured_image = $(".post-featured-image-id").val(),
            location = $("#short-post-location").val() || $("#short-post-location-mobile").val(),
            startdate = $("#short-post-startdate").val() || $("#short-post-startdate-mobile").val(),
            enddate = $("#short-post-enddate").val() || $("#short-post-enddate-mobile").val(),
            feeling = $("#short-post-feeling").val(),
            tagfriend = $("#short-post-tagfriend").val(),
            tourpackage = $("#short-post-tourpackage").val(),
            lookingfriend = $('#looking-for-travel-friend').val(),
            lat = $('#short-post-lat').val(),
            lng = $('#short-post-lng').val(),
            nonce = $("#shortpostnonce").val();

        $.ajaxSetup({
            cache: false
        });
        $.ajax({
            type: 'POST',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'add_short_post',
                post_content: content,
                location: location,
                startdate: startdate,
                enddate: enddate,
                feeling: feeling,
                tagfriend: tagfriend,
                tourpackage: tourpackage,
                lookingfriend: lookingfriend,
                featured_img: featured_image,
                lat: lat,
                lng: lng,
                post_nonce: nonce
            },
            success: function (data) {
                if (data.success) {
                    window.location.reload();
                    if (data.fb_sharing_option == 1) {
                        share(data.link);
                    }
                } else {
                    alert(data.message);
                }
            },
            error: function (MLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });

    var has_profile = ($('#has_profile').val() == 1);
    $('.looking-travel-friend_desktop input').click(function () {
        if (!has_profile) {
            window.location.href = website_url + "/matched";
        }
        $('.Search-DatePicker').show();
        $('#looking-for-travel-friend').val($(this).attr('atype'));
    });
    $('.looking-travel-friend input').click(function () {
        if (!has_profile) {
            window.location.href = website_url + "/matched";
        }
        $('.Search-DatePicker').show();
        $('#looking-for-travel-friend').val($(this).attr('atype'));
    });
    $('.event-location-image').click(function (e) {
        e.preventDefault();
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Confirm'
            },
            multiple: false
        });
        custom_uploader.on('select', function () {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            jQuery('.custom_round').css('background-image', 'url(' + attachment.url + ')');
            jQuery('.custom_round p').css('visibility', 'hidden');
            jQuery('[name="event_location_image_id"]').val(attachment.id);
        });
        custom_uploader.open();
    });

    jQuery('.event-tour-package').select2({
        placeholder: "(Optional)",
        allowClear: true
    });

    $('#create-event-submit').click(function () {
        if ($(this).hasClass('disabled')) {
            return;
        }
        $(this).addClass('disabled');
        var that = $(this);
        var event_name = $('[name="event_name"]').val();
        var event_invited_people = $('[name="event_invited_people"]').val();
        var event_type = $('[name="event_type"]').val();
        var event_location = $('[name="event_location"]').val();
        var event_location_image_id = $('[name="event_location_image_id"]').val();
        var start_date = $('[name="start_date"]').val() + ' ' + $('[name="start_time"]').val();
        var end_date = $('[name="end_date"]').val() + ' ' + $('[name="end_time"]').val();
        var event_tour_package = $('[name="event_tour_package"]').val();
        var nonce = $("#eventpostnonce").val();

        if (!event_location) {
            alert('Please input the location you want go');
            $(this).removeClass('disabled');
            return;
        }

        $.ajaxSetup({
            cache: false
        });
        $.ajax({
            type: 'POST',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'add_event_post',
                event_name: event_name,
                event_invited_people: event_invited_people,
                event_type: event_type,
                event_location: event_location,
                event_location_image_id:event_location_image_id,
                event_location_image_id:event_location_image_id,
                start_date: start_date,
                end_date: end_date,
                event_tour_package: event_tour_package,
                post_nonce: nonce
            },
            success: function (data) {
                if (data.success) {
                    window.location.reload();
                } else {
                    alert(data.message);
                    that.removeClass('disabled');
                }
            },
            error: function (MLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });

    $('#loading_next_page').on('click', '.event-going li', function () {
        var post_id = $(this).parent().attr('post_id');
        var status = $(this).attr('status');
        $(this).parent().parent().find('.event-going-current-status').text($(this).text());
        $.ajax({
            type: "POST",
            url: admin_ajax_url,
            data: {
                post_id: post_id,
                status: status,
                action: 'update_event_going'
            },
            success: function (data) { }
        });
    });

    $('#loading_next_page').on('click', '.like_mobile a', function () {
        var sc_type = 0;
        if ($(this).find('i').hasClass('liked')) {
            $(this).find('i').removeClass('liked');
            $(this).find('span').text(parseInt($(this).find('span').text()) - 1);
            sc_type = 1;
        } else {
            $(this).find('i').addClass('liked');
            $(this).find('span').text(parseInt($(this).find('span').text()) + 1);
        }
        if (!!$(this).attr('feedid')) {
            $.ajax({
                type: "POST",
                url: website_url + '/submit-ticket',
                data: {
                    feed_id: $(this).attr('feedid'),
                    action: 'feedback_like'
                },
                success: function (data) { }
            });
        } else {
            $.ajax({
                type: "POST",
                url: website_url + '/submit-ticket',
                data: {
                    post_id: $(this).attr('blogid'),
                    sc_type: sc_type,
                    action: 'blog_social_connect_l_d'
                },
                success: function (data) { }
            });
        }
    });
    $('.submit-comment').hide();
    $('#loading_next_page').on('click', '.comment_mobile', function () {
        if ($('.show_comment_' + $(this).data('comment')).is(":hidden")) {
            $(this).parent().parent().find('.submit-comment').show();
            $('.show_comment_' + $(this).data('comment')).show();
            $container.masonry('layout');
        } else {
            $('.show_comment_' + $(this).data('comment')).hide();
            $(this).parent().parent().find('.submit-comment').hide();
            $container.masonry('layout');
        }
    });

    $('#loading_next_page').on('keydown', '.submit-comment', function (e) {
        if (e.which == 13) {
            var feed_id = $(this).find('input[name="feed_id"]').val();
            var blog_id = $(this).find('input[name="blog_id"]').val();
            var feed_comment = $(this).find('input[name="feed_comment"]').val();
            var login_user_image = $(this).find('input[name="loginuser_image"]').val();
            var login_user_name = $(this).find('input[name="loginuser_name"]').val();
            if (!!feed_id) {
                $.ajax({
                    type: 'POST',
                    url: admin_ajax_url,
                    dataType: 'json',
                    data: {
                        action: 'feed_comment',
                        feed_id: feed_id,
                        feed_comment: feed_comment
                    },
                    success: function (data) {
                        if (data.success) {
         $(".show_comment_"+feed_id).append('<div class="user-comment-area"><img src='+login_user_image+'/><div class="another-user-section"><a>'+login_user_name+'</a><span>'+feed_comment+'</span></div></div>');
                   $('input[name="feed_comment"]').val('')

                         //   window.location.reload();
                        } else {
                            alert('System is busy. Try again later.');
                        }
                    }
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: website_url + '/submit-ticket',
                    data: {
                        post_id: blog_id,
                        scm_text: feed_comment,
                        action: 'blog_social_comments_form'
                    },
                    success: function (data) {
                        window.location.reload();
                    }
                });
            }
        }
    });

    $('#update-post-submit').click(function() {
        $.ajax({
            type: 'POST',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'update_short_post',
                postid: $('.editPost [name="postid"]').val(),
                post_content: $('.editPost [name="posttext"]').val(),
                feeling: $('.editPost [name="feeling"]').val(),
                tagfriend: $('.editPost [name="tag_user_id"]').val(),
                tourpackage: $('.editPost [name="edit_tourpackage"]').val(),
                featured_img: $('.editPost [name="edit_featured_image_id"]').val()
            },
            success: function (data) {
                if (data.success) {
                    window.location.reload();
                } else {
                    alert(data.message);
                }
            },
            error: function (MLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });
    
    $('#loading_next_page').on('click', '.edit-post', function () {
        $('.editPost [name="postid"]').val($(this).attr('postid'));
        $('.editPost [name="posttext"]').val($(this).attr('content'));
        $('.editPost [name="edit_featured_image_id"]').val('-1');
        if(!$(this).attr('feeling')) {
            $('.editPost span.edit-feeling').text('');
            $('.editPost [name="feeling"]').val('');
        }
        else {
            $('.editPost span.edit-feeling').text('feeing ' + $(this).attr('feeling'));
            $('.editPost [name="feeling"]').val($(this).attr('feeling'));
        }
        if(!$(this).attr('taguserid')) {
            $('.editPost span.edit-tag-user').text('');
            $('.editPost [name="tag_user_id"]').val('');
        }
        else {
            $('.editPost span.edit-tag-user').text(' with ' + $(this).attr('taguser'));
            $('.editPost [name="tag_user_id"]').val($(this).attr('taguserid'));
        }
        if(!$(this).attr('tour')) {
            $('.editPost [name="edit_tourpackage"]').val('');
        }
        else {
            $('.editPost [name="edit_tourpackage"]').val($(this).attr('tour'));
        }
    });
    
    
    $('.edit-feeling-choose select').change(function() {
        $('.editPost span.edit-tag-user').text('');
        $('.editPost [name="tag_user_id"]').val('');
        $('.editPost span.edit-feeling').text('feeing ' + $(this).val());
        $('.editPost [name="feeling"]').val($(this).val());
        $('.edit-feeling-choose').hide();
    });
    $('.edit-feeling-activity').click(function() {
        $('.edit-feeling-choose').show();
    });
    $('.editPost .fa-times-circle').click(function () {
        $('.edit-feeling-choose').hide();
    });
    $('.edit_tag_friend').click(function() {
        $('.editPost span.edit-feeling').text('');
        $('.editPost [name="feeling"]').val('');
        $('.editPost span.edit-tag-user').text(' with ' + $(this).find('.username').text());
        $('.editPost [name="tag_user_id"]').val($(this).attr('userid'));
        $('.tag-people-Popup').hide();
    });
    $('.edit-tourpackage').click(function() {
        $('.editPost [name="edit_tourpackage"]').val($(this).attr('tour_id'));
        $('.edit-tour-packages').hide();
    });
    $('.edit-tour-package-button').click(function() {
        $('.edit-tour-packages').show();
    });
    $('.edit-tour-packages .popupClose-close').click(function() {
        $('.edit-tour-packages').hide();
    });

    var edit_attatchment_count = 0;
    $('.edit-photo-video-button').click(function (e) {
        e.preventDefault();

        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image/Video',
            button: {
                text: 'Confirm'
            },
            multiple: false
        });
        custom_uploader.on('select', function () {
            edit_attatchment_count++;
            if(edit_attatchment_count>4) {
                return;
            }
            var old_ids = '';
            if ($('.editPost [name="edit_featured_image_id"]').val() != -1) {
                old_ids = $('.editPost [name="edit_featured_image_id"]').val();
            }
            attachment = custom_uploader.state().get('selection').first().toJSON();
            if (edit_attatchment_count == 4) {
                $('.userPhThumbnail2').hide();
            }
            if (attachment.type == "video") {
                $('.userPhThumbnail').prepend('<video src="' + attachment.url + '" controls="controls"></video>');
            }
            else {
                $('.userPhThumbnail').prepend('<img src="' + attachment.url + '" />');
            }

            $('.editPost [name="edit_featured_image_id"]').val(old_ids + attachment.id + ',');
        });
        custom_uploader.open();
    });

    $('.drp-delete').click(function() {
        $('.delete_post').attr('postid',$(this).attr('postid'));
    });

    $('.delete_post').click(function(){
        $.ajax({
            type: "POST",
            url: website_url + '/submit-ticket/',
            data: {
                post_id: $(this).attr('postid'),
                action: 'delete_feed_post'
            },
            success: function (data) {
                var result = JSON.parse(data);
                if (result.status) {
                    alert(result.message);
                } else {
                    alert(result.message);
                    window.location.reload();
                }
            }
        });
    });

    $('#loading_next_page').on('click', '.hide-from-timeline', function () {
        $.ajax({
            type: 'POST',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'hide_post_from_timeline',
                post_id: $(this).attr('postid')
            },
            success: function (data) {
                window.location.reload();
            }
        });
    });


    var showChar = 100;
    var ellipsestext = "...";
    var moretext = "more";
    var lesstext = "less";
    $('.more').each(function () {
        var content = $(this).html();
        if (content.length > showChar) {
            var c = content.substr(0, showChar);
            var h = content.substr(showChar - 1, content.length - showChar);
            var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
            $(this).html(html);
        }
    });
    $(".morelink").click(function () {
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
    jQuery('.short-post-datepicker').datetimepicker({});
    jQuery('#datetimepicker1').datetimepicker({
        language: 'en',
        pickTime: false,
        minDate: new Date(),
    });
    jQuery('#datetimepicker3').datetimepicker({
        pickDate: false,
        pick12HourFormat: true,
    });
    jQuery('#datetimepicker2').datetimepicker({
        language: 'en',
        pickTime: false,
        minDate: new Date(),
    });
    jQuery('#datetimepicker4').datetimepicker({
        pickDate: false,
        pick12HourFormat: true,
    });
     jQuery('#datetimepicker1').datetimepicker({
        minDate: new Date()
    });
    jQuery('.event-invited-people').tokenize2();
    $(".user-input").click(function () {
        $(".mobile-popup-m").show(100);
    });
    $(".m-add-photo").click(function () {
        $(".mobile-popup-m").show(100);
    });
    $(".back-post").click(function () {
        $(".mobile-popup-m").hide(100);
    });
    // feeling activity popup for mobile
    $(".feeling-t").click(function () {
        $('.feeling-activity-m').show(100);
    });
    $('.cancel-f').click(function () {
        $('.feeling-activity-m').hide(100);
    });
    // Tags Friend  popup for mobile
    $(".tags-t").click(function () {
        $('.tags-friend-m').show(100);
    });
    $('.cancel-tags').click(function () {
        $('.tags-friend-m').hide(100);
    });
    // Tags Friend  popup for Desktop
    $(".timetourpackages-popup").click(function () {
        $('.pop-timetourpackages').show();
    });
    $('.popupClose-close').click(function () {
        $('.pop-timetourpackages').hide();
    });
    // My Tour packages People for mobile Popup
    $(".mytour-p").click(function () {
        $('.tour-packages-Popup').show();
    });
    $('.cancel-tours').click(function () {
        $('.tour-packages-Popup').hide();
    });
    $(".ShowITc").click(function () {
        $(".show_tabs").addClass("St-1");
    });
    $(".show-post-right").click(function () {
        $(".show_tabs").removeClass("St-1");
    });
    $(".drp-edit").click(function () {
        $(".editPost").addClass("edPhide");
    });
    $(".Epostright").click(function () {
        $(".editPost").removeClass("edPhide");
    });
    $(".drp-delete").click(function () {
        $(".DeletePost").addClass("DePhide");
    });
    $(".Dpostright").click(function () {
        $(".DeletePost").removeClass("DePhide");
    });
    $('.create_event,.create_event_desktop').click(function () {
        $('.mobile-popup-test').hide();
        $('.normal-tab').hide();
        $('.event-tab').show();
    });
    $('.close_ev').click(function () {
        $('.mobile-popup-test').show();
        $('.normal-tab').show();
        $('.event-tab').hide();
    });
    $('.shedule_event_main_wrapper .fa-close').click(function () {
        $('.mobile-popup-test').show();
        $('.normal-tab').show();
        $('.event-tab').hide();
    });
    $('.feeling select,.feeling-activity-m select').change(function () {
        $('#short-post-feeling-text').text($('#short-post-feeling-text').attr('username') + ' is feeling ' + $(this).val());
        $('#mobile-short-post-feeling-text').text(' is feeling ' + $(this).val());
        $('#short-post-feeling').val($(this).val());
        $('.feeling').hide();
        $('.feeling-activity-m').hide(100);
    });
    $('.tag_friend').click(function () {
        $('#short-post-feeling-text').text($('#short-post-feeling-text').attr('username') + ' is with ' + $(this).find('.username').text());
        $('#mobile-short-post-feeling-text').text(' is with ' + $(this).find('.username').text());
        $('#short-post-tagfriend').val($(this).attr('userid'));
        $('.tag-people-Popup').hide();
        $('.tags-friend-m').hide(100);
    });
    $('.tourpackage').click(function () {
        $('#short-post-tourpackage').val($(this).attr('tour_id'));
        $('.pop-timetourpackages').hide();
        $('.tour-packages-Popup').hide();
    });
    $('.cusomt_private_dropdown .event-type-option').click(function () {
        $('.cusomt_private_dropdown .event-type-chosen').html($(this).html());
        $('[name="event_type"]').val($(this).attr('type'));
    });
    $(".feeling-popup").click(function () {
        $('.feeling').show();
    });
    $('.fa-times-circle').click(function () {
        $('.feeling').hide();
    });
    $(".timefeeling-popup").click(function () {
        $('.pop-timefeeling').show();
    });
    $('.popupClose-timefeeling').click(function () {
        $('.pop-timefeeling').hide();
    });
    $(".tagPeople-popup,.edit-tag-people").click(function () {
        $('.tag-people-Popup').show();
    });
    $('.tag-close').click(function () {
        $('.tag-people-Popup').hide();
    });
    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    if (isMobile.any()) {
        $('.change_url').attr('href', website_url + '/people/?device=app');
    } else {
        $('.change_url').attr('href', website_url + '/people');
    }
    $(".timefeeling-popup").click(function () {
        $('.feeling').show();
    });
    $('.fa-times-circle').click(function () {
        $('.feeling').hide();
    });
    $('.div_trave').click(function () {
        $('#opta').trigger('click');
    });
    $('#opta').click(function () {
        if ($(this).prop('checked') == true) {
            $('.div_trave').css('background', '#1abc9c');
        } else {
            $('.div_trave').css('background', '#00aff0');
        }
    })
    $('.custom_photos').hide();
    var a = 0;
    $('.add-photo-video').click(function (e) {
        e.preventDefault();

        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image/Video',
            button: {
                text: 'Confirm'
            },
            multiple: false
        });
        custom_uploader.on('select', function () {
            var old_ids = '';
            if (jQuery('input.post-featured-image-id').val() != -1) {
                old_ids = jQuery('input.post-featured-image-id').val();
            }
            attachment = custom_uploader.state().get('selection').first().toJSON();
            $('.add_c').addClass('text_after_upload');
            $('.custom_photos').show();
            a++;
            if (a == 4) {
                $('.add_img').hide();
            }
            if (attachment.type == "video") {
                $('.buttons_row_add').prepend('<div class="image_d" style="background:none;"><video src="' + attachment.url + '" controls="controls"></video></div>');
            }
            else {
                $('.buttons_row_add').prepend('<div class="image_d image_' + a + ' " data-class="image_1" style="background:url(' + attachment.url + ');background-size:cover;background-position: center center;margin: 0px 3px;"></div>');
            }

            jQuery('input.post-featured-image-id').val(old_ids + attachment.id + ',');
        });
        custom_uploader.open();
    });

    $('.iconsdd').click(function () {
        $('.iconsdd ').css({
            'background': '#8cfffa'
        });
        $('.size_m').css('color', '#000');
        $(this).css('background', '#22b14c');
        $(this).find('.size_m').css('color', '#fff');
    });

    $('.iconsd').click(function () {
        $('.iconsd').css('background', '#128462');
        $(this).css('background', '#22b14c');
        $('.icon1').css('background', 'transparent');
    });

});