﻿function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function calTotalRRRPPP() {
    var totalRp = 0;
    var totalRb = 0;
    $("[id*=tablepress]").find("input[type=number]").each(function () {
        var rp = parseInt($(this).val()) * parseInt($(this).attr("data-rp"));
        var rb = parseInt($(this).val()) * parseInt($(this).attr("data-rb"));

        $(this).parents("td:first")
               .next().html("Rp" + addCommas(rp.toString()))
               .next().html("$" + addCommas(rb.toString()));
        totalRp += rp;
        totalRb += rb;
    });
    $("#spanTotalRP").html("Rp" + addCommas(totalRp.toString()));
    $("#spanTotalRB").html("$" + addCommas(totalRb.toString()));

    var taxAndServiceRp = parseInt(totalRp * 0.1);
    var taxAndServiceRb = parseInt(totalRb * 0.1);
    $("#spanTaxAndServiceRP").html("Rp" + addCommas(taxAndServiceRp.toString()));
    $("#spanTaxAndServiceRB").html("$" + addCommas(taxAndServiceRb.toString()));

    var totalandfeerp = totalRp + taxAndServiceRp;
    var totalandfeerb = totalRb + taxAndServiceRb;
    $("#spanTotalAndFeeRP").html("Rp" + addCommas(totalandfeerp.toString()));
    $("#spanTotalAndFeeRb").html("$" + addCommas(totalandfeerb.toString()));
}

$(document).ready(function () {
    $("[id*=tablepress]").find("input[type=number]").each(function () {
        $(this).val("0");
        $(this).attr("min", "0");
        var datarp = $(this).parents("td:first").prev().prev().prev().html().replace("Rp", "").replace(/,/g, "");
        var datarb = $(this).parents("td:first").prev().html().replace("$", "").replace(/,/g, "");
        $(this).attr("data-rp", datarp).attr("data-rb", datarb);
    });

    calTotalRRRPPP();

    $("[id*=tablepress]").find("input[type=number]").change(function () {
        calTotalRRRPPP();
    });
});