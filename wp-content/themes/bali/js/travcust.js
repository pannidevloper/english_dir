var tourData = {};
tourData.days = 0;
/*======================================Added New Line For Preventing the Manual Entry In the Tour Package 14-02-2020 Starts Here======================================*/
tourData.popupTemplate = '<div id="travcust-dates-popup" class="cm-popup" style="display: none;"><div class="cm-popup-content"><div class="cm-popup-inner"> <i class="cm-close"><i class="far fa-times-circle"></i></i><div class="cm-popup-head"> <img src="https://www.travpart.com/English/wp-content/themes/bali/images/date-popup-img.png" alt=""></div><div class="cm-popup-title">How many days in your tour package?</div><div class="cm-popup-form"> <input id="inputTourPackageDays" class="form-control" type="number" min="1" max="99" value="" placeholder="maximum days is 99" onKeyDown="if(this.value.length==2 && event.keyCode!=8) return false;"> <button class="btn bg-org-0" id="submitTourPackageDays">OK</button></div></div></div></div>';
/*======================================Added New Line For Preventing the Manual Entry In the Tour Package 14-02-2020 Ends Here======================================*/
//timepicker
var tourDayPicker = function(container, cb, time, ap) {
    if (!time) time = '10:00';
    if (!ap) ap = 'am';
    if (cb) {
        this.save = cb;
    } else {
        this.save = function() {}
    }
    this.time = time;
    this.timeframe = ap;
    this.selectDay = 1;
    this.container = container;
    var $ = jQuery;
    var template = '<div class="datepicker-tour-outer"><div class="datepicker datepicker-tour am"><div class="datepicker-tour-dates"><div class="datepicker-tour-dates-title"><strong class="data-tour-days"></strong> <strong>Days</strong> Travel Package</div><div class="datepicker-tour-dates-label">on which days of the tour do you want this travel item to be reserved at ?</div><div class="datepicker-tour-dates-form"> <select class="datepicker-tour-dates-select form-control"> </select></div></div><div class="timepicker"><div class="timepicker-container-outer1"><div class="timepicker-container-inner1"><div class="timeline-container" ng-mousedown="timeSelectStart($event)" sm-touchstart="timeSelectStart($event)"><div class="current-time" style="transition: transform 0.4s ease; transform: translateX(-1px);"><div class="actual-time ng-binding">10:00</div></div><div class="timeline"></div><div class="hours-container"><div class="hour-mark"></div><div class="hour-mark"></div><div class="hour-mark"></div><div class="hour-mark"></div><div class="hour-mark"></div><div class="hour-mark"></div><div class="hour-mark"></div><div class="hour-mark"></div><div class="hour-mark"></div><div class="hour-mark"></div><div class="hour-mark ng-scope"></div></div></div><div class="timeline-bot"><div class="display-time"><div class="decrement-time" ng-click="adjustTime(\'decrease\')"> <svg width="24" height="24"> <path stroke="white" stroke-width="2" d="M8,12 h8"></path> </svg></div><div class="time"><div class="formatted-time">10:00 am</div></div><div class="increment-time" ng-click="adjustTime(\'increase\')"> <svg width="24" height="24"> <path stroke="white" stroke-width="2" d="M12,7 v10 M7,12 h10"></path> </svg></div></div><div class="am-pm-container"><div class="am-pm-button" ng-click="changetime(\'am\');">AM</div><div class="am-pm-button" ng-click="changetime(\'pm\');">PM</div></div></div></div></div></div></div></div><div class="d-flex justify-content-center align-items-center pb-3"><div class="datepicker-response mr-3">You chose <span class="data-tour-select-day">Day 1</span>, <span class="data-time">10:00 am</span></div><div class="save-button bg-org-0 text-white">Confirm</div></div>';
    container.html(template);
    this.container[0].removeAttribute('ng-cloak');

    var _this = this;
    var $select = $('.datepicker-tour-dates-select', _this.container).eq(0);
    $select.on('change', function() {
        _this.selectDay = parseInt($(this).val());
        $('.data-tour-select-day', _this.container).text('Day ' + _this.selectDay);
    })
    this.timepicker();

    if (getCookie('TourPackageDays') != '' && parseInt(getCookie('TourPackageDays')) > 0)
        changeTourDays(parseInt(getCookie('TourPackageDays')));
};
tourDayPicker.prototype.timepicker = function() {
    var scope = this;
    scope.picktime = true;
    var element = scope.container;
    var $ = jQuery;
    //////////////////////
    // Time Picker Code //
    //////////////////////
    if (scope.picktime) {
        var currenttime;
        var timeline;
        var timeline_width;
        var timeline_container;
        var sectionlength;

        scope.getHours = function() {
            var hours = new Array(11);
            return hours;
        };

        // scope.time = "12:00";
        scope.hour = 12;
        scope.minutes = 0;
        scope.currentoffset = 0;

        scope.timeframe = 'am';
        scope.$apply = scope.updateDate = function() {
            jQuery('.formatted-time', element).text(scope.time + ' ' + scope.timeframe);
            if (!$('.datepicker-tour', element).hasClass(scope.timeframe)) {
                $('.datepicker-tour', element).removeClass('am').removeClass('pm').addClass(scope.timeframe);
                element.closest('.date-picker-container').removeClass('am').removeClass('pm').addClass(scope.timeframe);
            }
            $('.data-time', element).text(scope.time + ' ' + scope.timeframe);
            $('.actual-time', element).text(scope.time);
        };

        scope.changetime = function(time) {
            scope.timeframe = time;
            scope.updateDate();
            scope.updateInputTime();
        };

        scope.edittime = {
            digits: []
        };

        scope.updateInputTime = function() {
            scope.edittime.input = scope.time + ' ' + scope.timeframe;
            scope.edittime.formatted = scope.edittime.input;
        };

        scope.updateInputTime();

        function checkValidTime(number) {
            validity = true;
            switch (scope.edittime.digits.length) {
                case 0:
                    if (number === 0) {
                        validity = false;
                    }
                    break;
                case 1:
                    if (number > 5) {
                        validity = false;
                    } else {
                        validity = true;
                    }
                    break;
                case 2:
                    validity = true;
                    break;
                case 3:
                    if (scope.edittime.digits[0] > 1) {
                        validity = false;
                    } else if (scope.edittime.digits[1] > 2) {
                        validity = false;
                    } else if (scope.edittime.digits[2] > 5) {
                        validity = false;
                    } else {
                        validity = true;
                    }
                    break;
                case 4:
                    validity = false;
                    break;
            }
            return validity;
        }

        function formatTime() {
            var time = "";
            if (scope.edittime.digits.length == 1) {
                time = "--:-" + scope.edittime.digits[0];
            } else if (scope.edittime.digits.length == 2) {
                time = "--:" + scope.edittime.digits[0] + scope.edittime.digits[1];
            } else if (scope.edittime.digits.length == 3) {
                time = "-" + scope.edittime.digits[0] + ':' + scope.edittime.digits[1] + scope.edittime.digits[2];
            } else if (scope.edittime.digits.length == 4) {
                time = scope.edittime.digits[0] + scope.edittime.digits[1].toString() + ':' + scope.edittime.digits[2] + scope.edittime.digits[3];
                console.log(time);
            }
            return time + ' ' + scope.timeframe;
        };

        scope.changeInputTime = function(event) {
            var numbers = { 48: 0, 49: 1, 50: 2, 51: 3, 52: 4, 53: 5, 54: 6, 55: 7, 56: 8, 57: 9 };
            if (numbers[event.which] !== undefined) {
                if (checkValidTime(numbers[event.which])) {
                    scope.edittime.digits.push(numbers[event.which]);
                    console.log(scope.edittime.digits);
                    scope.time_input = formatTime();
                    scope.time = numbers[event.which] + ':00';
                    scope.updateDate();
                    scope.setTimeBar();
                }
            } else if (event.which == 65) {
                scope.timeframe = 'am';
                scope.time_input = scope.time + ' ' + scope.timeframe;
            } else if (event.which == 80) {
                scope.timeframe = 'pm';
                scope.time_input = scope.time + ' ' + scope.timeframe;
            } else if (event.which == 8) {
                scope.edittime.digits.pop();
                scope.time_input = formatTime();
                console.log(scope.edittime.digits);
            }
            scope.edittime.formatted = scope.time_input;
            // scope.edittime.input = formatted;
        };

        var pad2 = function(number) {
            return (number < 10 ? '0' : '') + number;
        };

        scope.moving = false;
        scope.offsetx = 0;
        scope.totaloffset = 0;
        scope.initializeTimepicker = function() {
            currenttime = $('.current-time', element[0]);
            timeline = $('.timeline', element[0]);
            if (timeline.length > 0) {
                timeline_width = timeline[0].offsetWidth;
            }
            timeline_container = $('.timeline-container', element[0]);
            sectionlength = timeline_width / 24 / 6;
        };

        $(window).on('resize', function() {
            scope.initializeTimepicker();
            if (timeline.length > 0) {
                timeline_width = timeline[0].offsetWidth;
            }
            sectionlength = timeline_width / 24;
            // scope.checkWidth(true);
        });

        scope.setTimeBar = function(date) {
            currenttime = $('.current-time', element[0]);
            var timeline_width = $('.timeline', element[0])[0].offsetWidth;
            var hours = scope.time.split(':')[0];
            if (hours == 12) {
                hours = 0;
            }
            var minutes = scope.time.split(':')[1];
            var minutes_offset = (minutes / 60) * (timeline_width / 12);
            var hours_offset = (hours / 12) * timeline_width;
            scope.currentoffset = parseInt(hours_offset + minutes_offset - 1);
            currenttime.css({
                transition: 'transform 0.4s ease',
                transform: 'translateX(' + scope.currentoffset + 'px)',
            });
        };

        scope.getTime = function() {
            // get hours
            var percenttime = (scope.currentoffset + 1) / timeline_width;
            var hour = Math.floor(percenttime * 12);
            var percentminutes = (percenttime * 12) - hour;
            var minutes = Math.round((percentminutes * 60) / 5) * 5;
            if (hour === 0) {
                hour = 12;
            }
            if (minutes == 60) {
                hour += 1;
                minutes = 0;
            }

            scope.time = hour + ":" + pad2(minutes);
            scope.updateInputTime();
            scope.updateDate();
        };

        var initialized = false;


        if (!initialized) {
            $('.timeline-container', element).on('touchstart mousedown', function(event) {
                scope.timeSelectStart(event);
            });
            $('.am-pm-button:first', element).on('click', function(event) {
                scope.changetime('am');
            });
            $('.am-pm-button:last', element).on('click', function(event) {
                scope.changetime('pm');
            });
            $('.decrement-time', element).on('click', function(event) {
                scope.adjustTime('decrease');
            });
            $('.increment-time', element).on('click', function(event) {
                scope.adjustTime('increase');
            });

            $('.save-button', element).on('click', function(event) {
                scope.save(scope);
            });

            initialized = true;
        }

        scope.timeSelectStart = function(event) {
            // console.info('timeSelectStart ');
            scope.initializeTimepicker();
            var timepicker_container = element.find('.timepicker-container-inner1');
            var timepicker_offset = timepicker_container.offset().left;
            if (event.type == 'mousedown') {
                scope.xinitial = event.clientX;
            } else if (event.type == 'touchstart') {
                scope.xinitial = event.originalEvent.touches[0].clientX;
            }
            scope.moving = true;
            scope.currentoffset = scope.xinitial - timepicker_container.offset().left;
            scope.totaloffset = scope.xinitial - timepicker_container.offset().left;
            // console.log(timepicker_container.width());
            if (scope.currentoffset < 0) {
                scope.currentoffset = 0;
            } else if (scope.currentoffset > timepicker_container.width()) {
                scope.currentoffset = timepicker_container.width();
            }
            currenttime.css({
                transform: 'translateX(' + scope.currentoffset + 'px)',
                transition: 'none',
                cursor: 'ew-resize',
            });
            scope.getTime();
        };

        $(window).on('mousemove touchmove', function(event) {
            if (scope.moving === true) {
                event.preventDefault();
                if (event.type == 'mousemove') {
                    scope.offsetx = event.clientX - scope.xinitial;
                } else if (event.type == 'touchmove') {
                    scope.offsetx = event.originalEvent.touches[0].clientX - scope.xinitial;
                }
                var movex = scope.offsetx + scope.totaloffset;
                if (movex >= 0 && movex <= timeline_width) {
                    currenttime.css({
                        transform: 'translateX(' + movex + 'px)',
                    });
                    scope.currentoffset = movex;
                } else if (movex < 0) {
                    currenttime.css({
                        transform: 'translateX(0)',
                    });
                    scope.currentoffset = 0;
                } else {
                    currenttime.css({
                        transform: 'translateX(' + timeline_width + 'px)',
                    });
                    scope.currentoffset = timeline_width;
                }
                scope.getTime();
                scope.$apply();
            }
        });

        $(window).on('mouseup touchend', function(event) {
            if (scope.moving) {
                // var roundsection = Math.round(scope.currentoffset / sectionlength);
                // var newoffset = roundsection * sectionlength;
                // currenttime.css({
                //     transition: 'transform 0.25s ease',
                //     transform: 'translateX(' + (newoffset - 1) + 'px)',
                //     cursor: 'pointer',
                // });
                // scope.currentoffset = newoffset;
                // scope.totaloffset = scope.currentoffset;
                // $timeout(function () {
                //     scope.getTime();
                // }, 250);
            }
            scope.moving = false;
        });

        scope.adjustTime = function(direction) {
            // event.preventDefault();
            scope.initializeTimepicker();
            var newoffset;
            if (direction == 'decrease') {
                newoffset = scope.currentoffset - sectionlength;
            } else if (direction == 'increase') {
                newoffset = scope.currentoffset + sectionlength;
            }

            if (newoffset < 0 || newoffset > timeline_width) {
                if (newoffset < 0) {
                    newoffset = timeline_width - sectionlength;
                } else if (newoffset > timeline_width) {
                    newoffset = 0 + sectionlength;
                }
                if (scope.timeframe == 'am') {
                    scope.timeframe = 'pm';
                } else if (scope.timeframe == 'pm') {
                    scope.timeframe = 'am';
                }
            }
            currenttime.css({
                transition: 'transform 0.4s ease',
                transform: 'translateX(' + (newoffset - 1) + 'px)',
            });
            scope.currentoffset = newoffset;
            scope.totaloffset = scope.currentoffset;
            scope.getTime();
        };
    }

    // End Timepicker Code //
};
tourDayPicker.prototype.format = function() {
    return this.selectDay + 'st day,' + this.time + ' ' + this.timeframe;
}

jQuery(function($) {
    $('body').append(tourData.popupTemplate);
    var $tourPopuup = $('#travcust-dates-popup'),
        $inputTourDays = $('#inputTourPackageDays'),
        $tourPpackageNoDay = $('#tour-package-no-day'),
        $tourPackageDays = $('#tour-package-days');
    changeTourDays = function(days) {
        tourData.days = days;
        $('.data-tour-days').text(tourData.days);
        if (tourData.days) {
            $tourPpackageNoDay.hide();
            $tourPackageDays.show();
            var options = '';
            for (var i = 0; i < tourData.days; i++) {
                var t = i + 1;
                if (t == 1) {
                    options += '<option value="' + t + '" selected>Day ' + t + '</option>';
                } else {
                    options += '<option value="' + t + '">Day ' + t + '</option>';
                }

            }
            $('.datepicker-tour-dates-select').html(options);
        } else {
            $tourPpackageNoDay.show();
            $tourPackageDays.hide();
        }
    };

    if (getCookie('TourPackageDays') == '' || parseInt(getCookie('TourPackageDays')) < 1)
        $tourPopuup.show();
    else {
        $inputTourDays.val(parseInt(getCookie('TourPackageDays')));
        changeTourDays(parseInt(getCookie('TourPackageDays')));
    }

    // popup events
    $('.open-tour-day-popup').on('click', function(e) {
        e.preventDefault();
        $tourPopuup.show();
    });
    $('.cm-close,#submitTourPackageDays', $tourPopuup).on('click', function(e) {
        e.preventDefault();
        $tourPopuup.hide();
        changeTourDays(parseInt($inputTourDays.val()));
        setCookie('TourPackageDays', parseInt($inputTourDays.val()), 30);
    });

});

// car section

jQuery(function($) {
    var carBlockTop = $(".car-block-top"),
        deliveryTo = $(".delivery-left,.delivery-right");
    carBlockTop.on('click', function(e) {
        var target = $(this);
        chechDelivery(carBlockTop, target);
    });
    deliveryTo.on('click', function(e) {
        var target = $(this);
        target.closest('.car-block').find('.car-block-top').addClass('active');
        chechDelivery(deliveryTo, target);
    });

    $("#btn-delivery-select-date").on('click', function(e) {
        e.preventDefault();
        $("#delivery-step-2").removeClass("delivery-step-active");
        $("#delivery-step-3").addClass("delivery-step-active");
    });

    new tourDayPicker($('#deliveryDateTimeApp'), function(piker) {
        jQuery("#value-delivery-date").text(piker.format());
        jQuery("#delivery-step-3").removeClass("delivery-step-active");
        jQuery("#delivery-step-4").addClass("delivery-step-active");
    });

    function chechDelivery(items, target) {
        items.removeClass('active');
        target.addClass('active');
        var closest = target.closest('.car-block');
        $('.car-block').each(function(i, o) {
            if (o !== closest[0]) {
                $(o).find('.active').removeClass('active');
            }
        });
        if (closest.find('.active').length == 2) {
            $("#delivery-step-1").removeClass("delivery-step-active");
            $("#delivery-step-2").addClass("delivery-step-active");
            $(".value-delivery-type").text(closest.find('.delivery-name').text());
            $(".value-delivery-to").text(
                closest.find('.delivery-detail .delivery-area').text() + ',' +
                closest.find('.delivery-detail .active .price_span').text() + ''
            );

        }
    }

    $("#car_confirm").on('click', function(e) {
        e.preventDefault();
        $.notify({ message: 'Successfully added to my journey。' }, { type: 'success', z_index: 103100 });

    })


});

// guide section
jQuery(function($) {
    var guideBlock = $(".popupgui-block");
    guideBlock.on('click', function(e) {
        var _this = $(this);
        guideBlock.removeClass('active');
        _this.addClass('active');
        $("#guide-step-1").removeClass('delivery-step-active');
        $("#guide-step-2").addClass('delivery-step-active');
        $('.value-guide-name').text(_this.find('.popupguide-name').text());
        $('.value-guide-price').text(_this.find('.price').text());
    });

    $("#btn-guide-select-date").on('click', function(e) {

        $("#guide-step-2").removeClass('delivery-step-active');
        $("#guide-step-3").addClass('delivery-step-active');

    });

    new tourDayPicker($('#guideDateTimeApp'), function(piker) {
        jQuery("#value-guide-date").text(piker.format());
        jQuery("#guide-step-3").removeClass("delivery-step-active");
        jQuery("#guide-step-4").addClass("delivery-step-active");
    });

    $("#popupguide_confirm a").on('click', function(e) {
        e.preventDefault();
        $.notify({ message: 'Successfully added to my journey。' }, { type: 'success', z_index: 103100 });

    })

});

// spa section

jQuery(function($) {

    var spaBlock = $('.spa-block:not(.disabled)');
    spaBlock.on('click', function(e) {
        var _this = $(this);
        if (_this.hasClass('active')) {
            _this.removeClass('active');
            _this.find('[type="checkbox"]').attr('checked', false);
        } else {
            _this.addClass('active');
            _this.find('[type="checkbox"]').attr('checked', true);
        }
    });
    $("#btn-spa-select-spa").on('click', function(e) {
        e.preventDefault();
        var selectedSpa = $(".spa-block.active:not(.disabled)"),
            elSelectedSpa = $('#selected-spa'),
            cloneSpa = selectedSpa.clone(),
            selectedCount = selectedSpa.length,
            resNumberFormTemplate = '<div class="res-num-form"><div class="res-num-select"><i class="fa fa-user"></i><span>how many people</span> <div class="mini-sel"><input type="number" value="1" min="1" class="spa-num" ></div> </div> <div class="res-selected-price"><i class="fa fa-credit-card"></i> <span>Price</span><span>$ <span class="selected-price"></span></span></div> </div>';
        if (selectedCount) {
            cloneSpa.addClass('disabled').wrap('<div class="col-sm-4"></div>');
            cloneSpa.parent().append(resNumberFormTemplate);
            cloneSpa.parent().each(function(i, e) {
                var _this = $(e);
                var spa = {
                    name: _this.find('.spa-name:first').text(),
                    price: parseFloat(_this.find('.price_span:first').text()),
                    num: parseInt(_this.find('.spa-num:first').val())
                }
                _this.data('spa', spa);
                _this.find('.selected-price:first').text(spa.num * spa.price);
                _this.find('.spa-num:first').on('change', function(e) {
                    // console.info($(this).val());
                    spa.num = $(this).val();
                    _this.data('spa', spa);
                    _this.find('.selected-price:first').text(spa.num * spa.price);
                });

            });
            $('.value-spa-selected-count').text(selectedCount);
            elSelectedSpa.html('');
            elSelectedSpa.append(cloneSpa.parent());
            $("#spa-step-1").removeClass('delivery-step-active');
            $("#spa-step-2").addClass('delivery-step-active');
            $('html, body').animate({
                scrollTop: $("#spa-step-2").offset().top
            }, 500);
        } else {
            $.notify({ message: 'Please select at least one SPA!' }, { type: 'danger', z_index: 103100 });
        }
    });
    $("#btn-spa-select-date").on('click', function(e) {
        e.preventDefault();
        var result = '<div class="spa-result">',
            total = 0;


        $('#selected-spa>div').each(function(i, e) {
            var spa = $(e).data('spa');
            total += spa.num * spa.price;
            var item = '<div class="spa-item"><div class="name">' + spa.name + '</div><div class="price">$' + spa.price + '</div><div class="num">' + spa.num + 'people</div><div class="calc">$' + spa.num * spa.price + '</div> </div>';
            result += item;
        });
        total = Math.floor(total * 100) / 100;
        result += '</div><div class="spa-result-total">Total：$ <span>' + total + '</span></div>';
        $("#selected-spa-details").html(result);


        $("#spa-step-2").removeClass('delivery-step-active');
        $("#spa-step-3").addClass('delivery-step-active');
        $('html, body').animate({
            scrollTop: $("#spa-step-3").offset().top
        }, 500);

    });

    new tourDayPicker($('#spaDateTimeApp'), function(piker) {
        jQuery("#value-spa-date").text(piker.format());
        jQuery("#spa-step-3").removeClass("delivery-step-active");
        jQuery("#spa-step-4").addClass("delivery-step-active");
    });

    $("#spa-add-trav").on('click', function(e) {
        e.preventDefault();
        $.notify({ message: 'Successfully added to my journey.' }, { type: 'success', z_index: 103100 });
    })


});

// meal section


jQuery(function($) {
    //    open the meal food form
    $('.i-meals,.Meals', '.meal-block').addClass('active');

    // meal type select
    $(".meal-sub-item-head span").on('click', function(e) {
        e.preventDefault();
        var _this = $(this),
            closest = _this.closest('.meal-block');
        if (!_this.hasClass('active')) {
            _this.addClass('active');
            _this.siblings().removeClass('active');
            $('.meal-sub-item-content .active', closest).removeClass('active');
            $.each([
                ['i-meals', 'Meals'],
                ['i-break-fast', 'Breakfast'],
                ['i-beverage', 'Beverage'],
                ['i-appetizer', 'Appetizer']
            ], function(i, o) {
                if (_this.hasClass(o[0])) {
                    $('.meal-sub-item-content .' + o[1], closest).addClass('active');
                }
            });
        }
    });
    //    food select
    $('.meal-block input[type=checkbox]').on('change', function(e) {
        var _this = $(this),
            closest = _this.closest('.meal-block');
        if (closest.find('input[type=checkbox]:checked').length > 0) {
            var closestSiblings = $('.meal-block').not(closest);
            closestSiblings.removeClass('active');
            closestSiblings.find('input[type=checkbox]:checked').prop('checked', false);
            closest.addClass('active');
        } else {
            closest.removeClass('active');
        }
    });
    //    step1 to step2
    var selectedMeal = {},
        types = { Meals: "Meals", Breakfast: "Breakfast", Beverage: "Beverage", Appetizer: "Appetizer" };
    $('#btn-meal-select-food').on('click', function(e) {
            e.preventDefault();
            var activeMeal = $('.meal-block.active');
            if (activeMeal.length > 0) {
                selectedMeal.name = $('.meal-name', activeMeal).text();
                selectedMeal.area = $('.meal-area', activeMeal).text();
                selectedMeal.foods = [];
                selectedMeal.total = 0;

                var output = '<div class="selected-meal-list"><div class="items">';
                $('input[type=checkbox]:checked', activeMeal).each(function(i, o) {
                    var foodEl = $(this).closest('[data-type]'),
                        foodFields = foodEl.children('div'),
                        foodType = foodEl.attr('data-type'),
                        foodName = foodEl.attr('data-name'),
                        foodImg = foodEl.attr('data-img'),
                        foodPrice = parseFloat(foodFields.find('.price_span').text());
                    selectedMeal.foods.push({
                        type: foodType,
                        name: foodName,
                        img: foodImg,
                        price: foodPrice
                    });
                    selectedMeal.total += foodPrice;

                    output += '<div class="meal-item"><div><span data-meal-type="">' + types[foodType] + '</span></div><div><span data-meal-img=""><img src="' + foodImg + '"></span></div><div><span  data-meal="">' + foodName + '</span></div><div><span  data-meal-price="">$ ' + foodPrice + '</span></div></div>';

                });
                selectedMeal.total = Math.floor(selectedMeal.total * 100) / 100;
                output += '</div><div class="total"> <span>total：</span><strong>$ <span data-meal-total="">' + selectedMeal.total + '</span></strong></div> </div>';

                $('.selected-meal-wrap').html(output);
                $('.value-meal-selected-name').text(selectedMeal.area ? selectedMeal.name + ',' + selectedMeal.area : selectedMeal.name);
                $("#meal-step-2").addClass('delivery-step-active');
                $("#meal-step-1").removeClass('delivery-step-active');
                $('html, body').animate({
                    scrollTop: $("#meal-step-2").offset().top
                }, 500);

            } else {
                $.notify({ message: 'Please choose at least one food!' }, { type: 'danger', z_index: 103100 });
            }
        })
        //    step2 to step3

    $("#btn-meal-select-date").on('click', function(e) {
        e.preventDefault();
        $("#meal-step-2").removeClass('delivery-step-active');
        $("#meal-step-3").addClass('delivery-step-active');
        $('html, body').animate({
            scrollTop: $("#meal-step-3").offset().top
        }, 500);

    });
    new tourDayPicker($('#mealDateTimeApp'), function(piker) {
        jQuery("#value-meal-date").text(piker.format());
        jQuery("#meal-step-3").removeClass("delivery-step-active");
        jQuery("#meal-step-4").addClass("delivery-step-active");
    });
    //step 4
    $("#meal-add-trav").on('click', function(e) {
        e.preventDefault();
        $.notify({ message: 'Successfully added to my journey。' }, { type: 'success', z_index: 103100 });
    })

});

// restaurant section
var zomato_location;
jQuery(function($) {
    function appendRestuCateHtml() {
        var zomatoCatData = JSON.parse($("#zomatocatdata").html()),
            html = '',
            j = 0;

        for (var i of zomatoCatData["categories"]) {

            var catImg = i["categories"]["img"] ? i["categories"]["img"] : '';
            if (j == 0) {
                html += '<div class="item active">';
            } else {
                html += (j % 6 == 0) ? '<div class="item">' : '';
            }
            html += '<div class="col-sm-4"><div class="restaurant-block" data-cate-id="' + i["categories"]["id"] + '"  data-cate="' + i["categories"]["name"] + '"><div class="restaurant-block-top" style="background-image: url(\'' + catImg + '\')"></div><div class="restaurant-block-bottom"><div class="restaurant-cate">' + i["categories"]["name"] + '</div></div></div></div>';
            html += (j % 6 == 5) ? '</div>' : '';
            j++;
        }
        html += (j % 6 > 0) ? '</div>' : '';
        $("#restaurants-cate-list").html(html);
        $('.restaurant-block').on("click", function(e) {
            e.preventDefault();
            var _this = $(this);
            $('.restaurant-block.active').removeClass('active');
            _this.addClass('active');

            goStep2(_this.attr("data-cate-id"), _this.attr("data-cate"));
        });
    }
    appendRestuCateHtml();

    //step 0 to step 1
    $("#location-form").on('submit', function(e) {
        e.preventDefault();
        var location = $.trim($("#location-search").val());
        zomato_location = location;
        if (location) {
            $("#location-search").val('');
            goStep1(location);
        }
    });
    $(".local-list a", "#restaurants-step-0").on('click', function(e) {
        e.preventDefault();
        var location = $.trim($(this).text());
        zomato_location = location;
        goStep1(location);
    });

    function goStep1(location) {
        $(".selected-location", "#restaurants-section").html(location + ' <i class="fa fa-times-circle"></i>');
        $("#restaurants-step-0").removeClass('delivery-step-active');
        $("#restaurants-step-1").addClass('delivery-step-active');
        $('html, body').animate({
            scrollTop: $("#restaurants-step-1").offset().top
        }, 500);
        $(".selected-location", "#restaurants-section").on('click', function(e) {
            e.preventDefault();
            $(".delivery-step-active", "#restaurants-section").removeClass('delivery-step-active');
            $("#restaurants-step-0").addClass('delivery-step-active');
            $('html, body').animate({
                scrollTop: $("#restaurants-step-0").offset().top
            }, 500);
        })
    }

    //step 1 to step 2

    function goStep2(cid, cate) {
        $("#restaurants-list").html('<div class="p-5 text-center"><i class="fas fa-sync fa-spin font-xl"></i> </div>');
        $(".delivery-step-active", "#restaurants-section").removeClass('delivery-step-active');
        $("#restaurants-step-2").addClass('delivery-step-active');
        $('html, body').animate({
            scrollTop: $("#restaurants-step-2").offset().top
        }, 500);

        // add category tag
        $(".selected-cate", "#restaurants-section").html(cate + ' <i class="fa fa-times-circle"></i>');
        $(".selected-cate", "#restaurants-section").on('click', function(e) {
            e.preventDefault();
            $(".delivery-step-active", "#restaurants-section").removeClass('delivery-step-active');
            $("#restaurants-step-1").addClass('delivery-step-active');
            $('html, body').animate({
                scrollTop: $("#restaurants-step-1").offset().top
            }, 500);
        });

        // load restaurants data from api
        jQuery.getJSON('/English/wp-content/themes/bali/api.php', {
            action: 'get_restaurant_list',
            'city_name': zomato_location,
            category: cid
        }).done(function(data) {
            appendRestuHtml(data);
            $(".rest-detail", "#restaurants-list").on("click", function(e) {
                e.preventDefault();
                goStep3($(this));
            })
        });
    }

    function appendRestuHtml(data) {
        var appendData = '';
        $.each(data.restaurants, function(index, data) {
            var bgUrl = '';
            if (data.restaurant.featured_image) {
                bgUrl = data.restaurant.featured_image;
            } else if (data.restaurant.thumb) {
                bgUrl = data.restaurant.thumb;
            }

            appendData += '<div class="rest-detail"  data-cuisines="' + data.restaurant.cuisines + '" data-name="' + data.restaurant.name + '" data-price="' + data.restaurant.currency + '  ' + data.restaurant.average_cost_for_two + '" data-city="' + data.restaurant.location.city + '" data-img="' + bgUrl + '">';
            appendData += '<div class="rest-detail-container">';
            appendData += '<div class="rest-detail-left" style="background-image: url(' + bgUrl + ')">';
            if (!bgUrl) {
                appendData += '<p><span>no images</span></p>';
            }
            appendData += '</div>';
            appendData += '<div class="rest-detail-right">';
            appendData += '<div class="rest-r-top">';
            appendData += '<p class="rest-name">' + data.restaurant.name + ' </p>';
            appendData += '<p class="rest-rating" style="background-color: #' + data.restaurant.user_rating.rating_color + '; ">   ' + data.restaurant.user_rating.aggregate_rating + ' </p>';
            appendData += '</div>';
            appendData += '<p class="rest-local">  ' + data.restaurant.location.locality_verbose + ' </p>';
            appendData += '<p class="rest-cuisines"> Department of：' + data.restaurant.cuisines + '</p>';
            if (data.restaurant.currency == 'IDR')
                appendData += '<p class="rest-currency"> Two charges： <span class="change_price original" or_pic="' + data.restaurant.average_cost_for_two + '">Rp <span class="price_span">' + data.restaurant.average_cost_for_two + '</span></span> /2 persons (approximately) </p>';
            else if (data.restaurant.currency == '$')
                appendData += '<p class="rest-currency"> Two charges： <span class="change_price original" or_pic="' + Math.round(data.restaurant.average_cost_for_two / $('#cs_USD').val()) + '">Rp <span class="price_span">' + Math.round(data.restaurant.average_cost_for_two / $('#cs_USD').val()) + '</span></span> /2 persons (approximately) </p>';
            else
                appendData += '<p class="rest-currency"> Two charges： ' + data.restaurant.currency + ' ' + data.restaurant.average_cost_for_two + '/2 persons (approximately) </p>';
            appendData += '</div>';
            appendData += '</div>';
            appendData += '</div>';
        });
        $("#restaurants-list").html(appendData);
        setTimeout('$(".header_curreny").change()', 100);
    }

    //    step 2 to step 3
    function goStep3(restaurantElm) {
        var img = restaurantElm.attr('data-img');
        var detail = restaurantElm.find('.rest-detail-right').html();
        /*Added New Line For The Delete Icon For The Dining Options Starts*/
        var html = '<div class="restaurant-img ' + (!!img ? '' : 'no-image') + '" style="background-image: url(' + img + ')"></div><div class="restaurant-detail">' + detail + '<div>diners：<input type="number" value="2" class="small-input" id="restaurant-p-number" min="1"></div></div><div class="ml-sm-auto">Please order and pay directly at the restaurant。<div style="text-align:center;margin-top:10px;" class="trash-icon"><i class="fas fa-trash-alt" style="color: #3DA75A;font-size: 30px;"></i></div></div>';
        /*Added New Line For The Delete Icon For The Dining Options Starts*/
        $("#selected-restaurant").html(html);
        $('.value-restaurants-selected-name').text(restaurantElm.attr('data-name'));




        $(".delivery-step-active", "#restaurants-section").removeClass('delivery-step-active');
        $("#restaurants-step-3").addClass('delivery-step-active');
        $('html, body').animate({
            scrollTop: $("#restaurants-step-3").offset().top
        }, 500);

        $("#btn-restaurants-select-date").on('click', function(e) {
            e.preventDefault();
            /*Added New Line For The Delete Icon For The Dining Options Starts*/
            var content = '<div class="restaurant-img ' + (!!img ? '' : 'no-image') + '" style="background-image: url(' + img + ')"></div><div class="restaurant-detail">' + detail + '<div>diners：' + $("#restaurant-p-number").val() + '</div></div><div class="ml-sm-auto">Please order and pay directly at the restaurant。<div style="text-align:center;margin-top:10px;" class="trash-icon"><i class="fas fa-trash-alt" style="color: #3DA75A;font-size: 30px;"></i></div></div>';
            /*Added New Line For The Delete Icon For The Dining Options Ends*/
            $("#selected-restaurant-1").html(content);
            goStep4();
        });

    }

    //    step 3 to step 4
    function goStep4() {
        $(".delivery-step-active", "#restaurants-section").removeClass('delivery-step-active');
        $("#restaurants-step-4").addClass('delivery-step-active');
        $('html, body').animate({
            scrollTop: $("#restaurants-step-4").offset().top
        }, 500);
    }

    //    step4  to step5


    new tourDayPicker($('#restaurantDateTimeApp'), function(piker) {
        jQuery("#value-restaurants-date").text(piker.format());
        $(".delivery-step-active", "#restaurants-section").removeClass('delivery-step-active');
        $("#restaurants-step-5").addClass('delivery-step-active');
        $('html, body').animate({
            scrollTop: $("#restaurants-step-5").offset().top
        }, 500);
    });

    //    end step

    $("#restaurants-add-trav").on('click', function(e) {
        e.preventDefault();
        $.notify({ message: 'Successfully added to my journey。' }, { type: 'success', z_index: 103100 });
    })


});

// ExtraActivity section
jQuery(function($) {
    var extBlock = $('.ext-block:not(.disabled)');
    extBlock.on('click', function(e) {
        var _this = $(this);
        if (_this.hasClass('active')) {
            _this.removeClass('active');
            _this.find('[type="checkbox"]').attr('checked', false);
        } else {
            _this.addClass('active');
            _this.find('[type="checkbox"]').attr('checked', true);
        }
    });
    $("#btn-ext-select-ext").on('click', function(e) {
        e.preventDefault();
        var selectedext = $(".ext-block.active:not(.disabled)"),
            elSelectedext = $('#selected-ext'),
            cloneext = selectedext.clone(),
            selectedCount = selectedext.length,
            resNumberFormTemplate = '<div class="res-num-form"><div class="res-num-select"><i class="fa fa-user"></i><span>how many people</span> <div class="mini-sel"><input type="number" value="1" min="1" class="ext-num" ></div> </div> <div class="res-selected-price"><i class="fa fa-credit-card"></i> <span>Price</span><span>$ <span class="selected-price"></span></span></div> </div>';
        if (selectedCount) {
            cloneext.addClass('disabled').wrap('<div class="col-sm-4"></div>');
            cloneext.parent().append(resNumberFormTemplate);
            cloneext.parent().each(function(i, e) {
                var _this = $(e);
                var ext = {
                    name: _this.find('.ext-name:first').text(),
                    price: parseFloat(_this.find('.price_span:first').text()),
                    num: parseInt(_this.find('.ext-num:first').val())
                }
                _this.data('ext', ext);
                _this.find('.selected-price:first').text(ext.num * ext.price);
                _this.find('.ext-num:first').on('change', function(e) {
                    // console.info($(this).val());
                    ext.num = $(this).val();
                    _this.data('ext', ext);
                    _this.find('.selected-price:first').text(ext.num * ext.price);
                });

            });
            $('.value-ext-selected-count').text(selectedCount);
            elSelectedext.html('');
            elSelectedext.append(cloneext.parent());
            $("#ext-step-1").removeClass('delivery-step-active');
            $("#ext-step-2").addClass('delivery-step-active');
            $('html, body').animate({
                scrollTop: $("#ext-step-2").offset().top
            }, 500);
        } else {
            $.notify({ message: 'Please select at least one optional activities!' }, { type: 'danger', z_index: 103100 });
        }
    });
    $("#btn-ext-select-date").on('click', function(e) {
        e.preventDefault();
        var result = '<div class="ext-result">',
            total = 0;


        $('#selected-ext>div').each(function(i, e) {
            var ext = $(e).data('ext');
            total += ext.num * ext.price;
            var item = '<div class="ext-item"><div class="name">' + ext.name + '</div><div class="price">$' + ext.price + '</div><div class="num">' + ext.num + 'people</div><div class="calc">$' + ext.num * ext.price + '</div> </div>';
            result += item;
        });
        total = Math.floor(total * 100) / 100;
        result += '</div><div class="ext-result-total">total：$ <span>' + total + '</span></div>';
        $("#selected-ext-details").html(result);


        $("#ext-step-2").removeClass('delivery-step-active');
        $("#ext-step-3").addClass('delivery-step-active');
        $('html, body').animate({
            scrollTop: $("#ext-step-3").offset().top
        }, 500);

    });

    new tourDayPicker($('#extDateTimeApp'), function(piker) {
        jQuery("#value-ext-date").text(piker.format());
        jQuery("#ext-step-3").removeClass("delivery-step-active");
        jQuery("#ext-step-4").addClass("delivery-step-active");
    });

    $("#ext-add-trav").on('click', function(e) {
        e.preventDefault();
        $.notify({ message: 'Successfully added to my journey.' }, { type: 'success', z_index: 103100 });
    })
});

// helicopter section
jQuery(function($) {
    var helicopterBlock = $('.helicopter-block:not(.disabled)');
    helicopterBlock.on('click', function(e) {
        var _this = $(this);
        if (_this.hasClass('active')) {
            _this.removeClass('active');
            _this.find('[type="checkbox"]').attr('checked', false);
        } else {
            _this.addClass('active');
            _this.find('[type="checkbox"]').attr('checked', true);
        }
    });
    $("#btn-helicopter-select-helicopter").on('click', function(e) {
        e.preventDefault();
        var selectedhelicopter = $(".helicopter-block.active:not(.disabled)"),
            elSelectedhelicopter = $('#selected-helicopter'),
            clonehelicopter = selectedhelicopter.clone(),
            selectedCount = selectedhelicopter.length,
            resNumberFormTemplate = '<div class="res-num-form"><div class="res-num-select"><i class="fa fa-user"></i><span>how many people</span> <div class="mini-sel"><input type="number" value="1" min="1" class="helicopter-num" ></div> </div> <div class="res-selected-price"><i class="fa fa-credit-card"></i> <span>Price</span><span>$ <span class="selected-price"></span></span></div> </div>';
        if (selectedCount) {
            clonehelicopter.addClass('disabled').wrap('<div class="col-sm-4"></div>');
            clonehelicopter.parent().append(resNumberFormTemplate);
            clonehelicopter.parent().each(function(i, e) {
                var _this = $(e);
                var helicopter = {
                    name: _this.find('.helicopter-name:first').text(),
                    price: parseFloat(_this.find('.price_span:first').text()),
                    num: parseInt(_this.find('.helicopter-num:first').val())
                }
                _this.data('helicopter', helicopter);
                _this.find('.selected-price:first').text(helicopter.num * helicopter.price);
                _this.find('.helicopter-num:first').on('change', function(e) {
                    // console.info($(this).val());
                    helicopter.num = $(this).val();
                    _this.data('helicopter', helicopter);
                    _this.find('.selected-price:first').text(helicopter.num * helicopter.price);
                });

            });
            $('.value-helicopter-selected-count').text(selectedCount);
            elSelectedhelicopter.html('');
            elSelectedhelicopter.append(clonehelicopter.parent());
            $("#helicopter-step-1").removeClass('delivery-step-active');
            $("#helicopter-step-2").addClass('delivery-step-active');
            $('html, body').animate({
                scrollTop: $("#helicopter-step-2").offset().top
            }, 500);
        } else {
            $.notify({ message: 'Please select at least one optional activities!' }, { type: 'danger', z_index: 103100 });
        }
    });
    $("#btn-helicopter-select-date").on('click', function(e) {
        e.preventDefault();
        var result = '<div class="helicopter-result">',
            total = 0;


        $('#selected-helicopter>div').each(function(i, e) {
            var helicopter = $(e).data('helicopter');
            total += helicopter.num * helicopter.price;
            var item = '<div class="helicopter-item"><div class="name">' + helicopter.name + '</div><div class="price">$' + helicopter.price + '</div><div class="num">' + helicopter.num + 'people</div><div class="calc">$' + helicopter.num * helicopter.price + '</div> </div>';
            result += item;
        });
        total = Math.floor(total * 100) / 100;
        result += '</div><div class="helicopter-result-total">total：$ <span>' + total + '</span></div>';
        $("#selected-helicopter-details").html(result);


        $("#helicopter-step-2").removeClass('delivery-step-active');
        $("#helicopter-step-3").addClass('delivery-step-active');
        $('html, body').animate({
            scrollTop: $("#helicopter-step-3").offset().top
        }, 500);

    });

    new tourDayPicker($('#helicopterDateTimeApp'), function(piker) {
        jQuery("#value-helicopter-date").text(piker.format());
        jQuery("#helicopter-step-3").removeClass("delivery-step-active");
        jQuery("#helicopter-step-4").addClass("delivery-step-active");
    });

    $("#helicopter-add-trav").on('click', function(e) {
        e.preventDefault();
        $.notify({ message: 'Successfully added to my journey.' }, { type: 'success', z_index: 103100 });
    })
});

// place section
jQuery(function($) {
    $('#btn-travel-select-date').on('click', function(e) {
        e.preventDefault();
        $("#travel-location-step-1").removeClass('delivery-step-active');
        $("#travel-location-step-2").addClass('delivery-step-active');
        $('html, body').animate({
            scrollTop: $("#travel-location-step-2").offset().top
        }, 500);
    });

    $("#travel-location-add").on('click', function(e) {
        e.preventDefault();
        $.notify({ message: 'Successfully added to my journey.' }, { type: 'success', z_index: 103100 });
    });
    new tourDayPicker($('#travelLocationDateTimeApp'), function(piker) {
        jQuery("#value-travel-date").text(piker.format());
        jQuery("#travel-location-selected").text($("#place_categories").val());
        $(".delivery-step-active", "#travel-location").removeClass('delivery-step-active');
        $("#travel-location-step-3").addClass('delivery-step-active');
        $('html, body').animate({
            scrollTop: $("#travel-location-step-3").offset().top
        }, 500);
    });
});

// globe toggle button
jQuery(function($) {
    $('.btn-collapse').on('click', function(event) {
        event.preventDefault();
        $(this).toggleClass('expend');
        if ($(this).hasClass('expend')) {
            $('.showallsection').addClass('defaultshow');
        } else {
            $('.showallsection').removeClass('defaultshow');
        }
    });
    $('.button-toggle').on('click', function(e) {
        e.preventDefault();
        var _this = $(this);
        _this.toggleClass('active');
        var target = $(_this.attr("data-target"));
        target.toggleClass('fadeout');
    });
    var addthis = $('.at-below-post-page.addthis_tool');
    $("#content>.container>.row>.col-md-12").append(addthis);
});
// left float menu
jQuery(function($) {
    $("#menuresize-header").on("click", function(e) {
        e.preventDefault();
        if ($(this).hasClass('mini')) {
            $(this).removeClass('mini');
            $("#left_menu").show(1000);
        } else {
            $(this).addClass('mini');
            $("#left_menu").hide(1000);
        }

    });

});