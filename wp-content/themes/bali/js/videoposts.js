var website_url = $('#website-url').val();
var admin_ajax_url = $('#admin-ajax-url').val();


$(document).ready(function(){
	$('.click_repl').click(function(){
		var cmid = $(this).data('cmid');
		$('.'+cmid).toggle();
	})
})

function editid(id, content) {
    $('#pcontent').val(content);
    $('#ppostid').val(id);
}

function getid(id) {
    $('#deleteid').attr('value', id);
}
function delete_post(id) {
    jQuery(document).ready(function () {
        var post_id = $("#deleteid").val();
        $.ajax({
            type: "POST",
            url: website_url + '/submit-ticket/',
            data: {
                post_id: post_id,
                action: 'delete_feed_post'
            },
            success: function (data) {
                var result = JSON.parse(data);
                if (result.status) {
                    alert(result.message);
                } else {
                    alert(result.message);
                    location.reload();
                }
            }
        });
    });
}

//share feed
function share_feed(id) {
    jQuery(document).ready(function ($) {
        var tour_id = id;
        $.ajax({
            type: "POST",
            url: website_url + '/submit-ticket/',
            data: {
                feed_id: tour_id,
                action: 'feedback_share'
            }, // serializes the form's elements.
            success: function (data) {
                var result = JSON.parse(data);
                if (result.status) {
                    alert(result.message);
                    window.location.reload() // show response from the php script.
                } else {
                    alert(result.message);
                    //window.location.reload()// show response from the php script.
                }
            }
        });
    });
} 
$(document).ready(function () {

    var $container = '';
    
    $container = jQuery('#loading_next_page').masonry({
        itemSelector: '.box',
        columnWidth: 5
    });
    
    var pageIndex = 1;
    var page_loading = false;
    $(window).scroll(function () {
        if (($(document).scrollTop() + 1.5 * $(window).height()) > $(document).height()) {
            if (!page_loading) {
                page_loading = true;
                pageIndex++;
                $.ajax({
                    type: "GET",
                    url: admin_ajax_url,
                    data: {
                        pageNum: pageIndex,
                        action: 'loading_more_shortpost_with_video'
                    },
                    success: function (data) {
                        if (data != 0) {
                            page_loading = false;
                            //$('#loading_next_page').prepend(data);
                              var $content = $( data );
                              // add jQuery object
                              $container.append( $content ).masonry( 'appended', $content );
                              //$('#loading_next_page').masonry('reload');
                        }
                    }
                });
            }
        }
    });

    $('#loading_next_page').on('click', '.login-alert', function () {
        alert("Please login!");
    });

    $('#loading_next_page').on('click', '.like-the-post a', function () {
        var sc_type = 0;
        if ($(this).find('i').hasClass('liked')) {
            $(this).find('i').removeClass('liked');
            $(this).find('span').text(parseInt($(this).find('span').text()) - 1);
            sc_type = 1;
        } else {
            $(this).find('i').addClass('liked');
            $(this).find('span').text(parseInt($(this).find('span').text()) + 1);
        }
        if (!!$(this).attr('feedid')) {
            $.ajax({
                type: "POST",
                url: website_url + '/submit-ticket',
                data: {
                    feed_id: $(this).attr('feedid'),
                    action: 'feedback_like'
                },
                success: function (data) { }
            });
        } else {
            $.ajax({
                type: "POST",
                url: website_url + '/submit-ticket',
                data: {
                    post_id: $(this).attr('blogid'),
                    sc_type: sc_type,
                    action: 'blog_social_connect_l_d'
                },
                success: function (data) { }
            });
        }
    });

    $('#loading_next_page').on('click', '.comment_mobile', function () {
        $(this).parent().parent().find('.submit-comment').toggle();
    });

    $('#loading_next_page').on('keydown', '.submit-comment', function (e) {
        if (e.which == 13) {
            var feed_id = $(this).find('input[name="feed_id"]').val();
            var blog_id = $(this).find('input[name="blog_id"]').val();
            var feed_comment = $(this).find('input[name="feed_comment"]').val();
            if (!!feed_id) {
                $.ajax({
                    type: 'POST',
                    url: admin_ajax_url,
                    dataType: 'json',
                    data: {
                        action: 'feed_comment',
                        feed_id: feed_id,
                        feed_comment: feed_comment
                    },
                    success: function (data) {
                        if (data.success) {
                            window.location.href = window.location.href;
                        } else {
                            alert('System is busy. Try again later.');
                        }
                    }
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: website_url + '/submit-ticket',
                    data: {
                        post_id: blog_id,
                        scm_text: feed_comment,
                        action: 'blog_social_comments_form'
                    },
                    success: function (data) {
                        window.location.href = window.location.href;
                    }
                });
            }
        }
    });

    $('#loading_next_page').on('click', '.hide-from-timeline', function () {
        $.ajax({
            type: 'POST',
            url: admin_ajax_url,
            dataType: 'json',
            data: {
                action: 'hide_post_from_timeline',
                post_id: $(this).attr('postid')
            },
            success: function (data) {
                window.location.reload();
            }
        });
    });


    var showChar = 100;
    var ellipsestext = "...";
    var moretext = "more";
    var lesstext = "less";
    $('.more').each(function () {
        var content = $(this).html();
        if (content.length > showChar) {
            var c = content.substr(0, showChar);
            var h = content.substr(showChar - 1, content.length - showChar);
            var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
            $(this).html(html);
        }
    });
    $(".morelink").click(function () {
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

});
