<?php
/* Template Name:NonTour */
get_header();

$meta = get_post_meta( get_the_ID() );


?>
<link href="<?php bloginfo('template_url'); ?>/css/tour.css" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/jquery-ui.css" rel="stylesheet" type="text/css">

<script src="<?php bloginfo('template_url'); ?>/js/jquery-ui.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/tour.js?version=1"></script>
<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/tableresponsive.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <script>
 
  </script>
<?php if(get_post_meta(get_the_ID(), '_cs_logo', true)) { ?>
<img width="90" height="90" style="float:right; position:relative; left:10px; " class="attachment-custom-size size-custom-size wp-post-image lazy" src="<?php echo wp_get_attachment_url(get_post_meta(get_the_ID(), '_cs_logo', true)); ?>" />
<?php } ?>
<?php
if(have_posts()) {
	while(have_posts()) {
		the_post();
		the_content();
		?>

		
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div id="tursldr"><?php
							$aAImg = explode('=', get_post_meta($post->ID, '_cs_sdr_img_nontour', true));
							$aATle = explode('=', get_post_meta($post->ID, '_cs_sdr_ttl_nontour', true));
							if( $aAImg ) {
                            	echo '<ul>';
								foreach( $aAImg as $sK => $sV ) {
									if($sV) {
										$img = matthewruddy_image_resize( str_replace("http://www.soleattack.com/bali632/","http://www.tourfrombali.com/", clsImg($sV)), 569, 356, true, false );
										echo '<li><img src="'.$img['url'].'" alt=""></li>';
									}
								}
								echo '</ul>';
							} ?>
                        </div>
                    </div>
					
                    <div class="col-md-12">
                        <div class="bnrs row">
                            <div class="col-md-6 p15 p-r-7">
                                <img src="<?php bloginfo('template_url'); ?>/images/bnr-1-1.jpg" alt="">
                            </div>
                            <div class="col-md-6 p15 p-l-7 p-l-7-mobile">
                                <img src="<?php bloginfo('template_url'); ?>/images/bnr-2-2.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 tour_div p-l-7-mobile">
					<input type="hidden" class="cs_personsperroom" value="<?php if(isset($meta["_cs_personsperroom"][0])) echo $meta["_cs_personsperroom"][0]; ?>" />
				    <input type="hidden" class="cs_adultprice" value="<?php if(isset($meta["_cs_adultprice"][0])) echo $meta["_cs_adultprice"][0]; ?>" />
					<input type="hidden" class="cs_childrenprice" value="<?php if(isset($meta["_cs_childrenprice"][0])) echo $meta["_cs_childrenprice"][0]; ?>" />
				    <input type="hidden" class="cs_hotelroomprice" value="<?php if(isset($meta["_cs_hotelroomprice"][0])) echo $meta["_cs_hotelroomprice"][0]; ?>" />
				    <input type="hidden" class="cs_extrabedprice" value="<?php if(isset($meta["_cs_extrabedprice"][0])) echo $meta["_cs_extrabedprice"][0]; ?>" />
				    <input type="hidden" class="cs_addnightroomprice" value="<?php if(isset($meta["_cs_addnightroomprice"][0])) echo $meta["_cs_addnightroomprice"][0]; ?>" />
				    <input type="hidden" class="post_id" value="<?php echo get_the_ID() ?>" />
				    <input type="hidden" class="site_url" value="<?php echo get_site_url() ?>" />

 
					<div class="tour_header">
							 <div class="col-md-6 col-xs-6 information" >
							
							 <?php if( trim($meta["_cs_Depart_content"][0])!=""){ ?>
								<div> <span>Depart  </span> : <span><?php if(isset($meta["_cs_Depart_content"][0])) echo $meta["_cs_Depart_content"][0]; ?>  </span> </div>
							<?php }?>
							
							<?php if( trim($meta["_cs_tourdays"][0])!=""){ ?>
								<div> <span>Journey</span> :<span> <?php if(isset($meta["_cs_tourdays"][0])) echo $meta["_cs_tourdays"][0]; ?> days   </span> </div>
							<?php }?>	
							
							<?php if( trim($meta["_cs_Travel_type_content"][0])!=""){ ?>
								<div> <span> <?php if(isset($meta["_cs_Travel_type_heading"][0])) echo $meta["_cs_Travel_type_heading"][0]; ?></span> :<span> <?php if(isset($meta["_cs_Travel_type_content"][0])) echo $meta["_cs_Travel_type_content"][0]; ?>    </span> </div>
							<?php }?>	
							</div>
							
							
							 <div class="col-md-6 col-xs-6  information" >
							 
								<?php if( trim($meta["_cs_Destinationcontent"][0])!=""){ ?>
									<div> <span>Destination  </span> :<span> <?php if(isset($meta["_cs_Destinationcontent"][0])) echo $meta["_cs_Destinationcontent"][0]; ?>   </span> </div>
								<?php } ?>
								
								<?php if( trim($meta["_cs_Language_content"][0])!=""){ ?>
								<div> <span><?php if(isset($meta["_cs_Language_heading"][0])) echo $meta["_cs_Language_heading"][0]; ?> </span> :<span> <?php if(isset($meta["_cs_Language_content"][0])) echo $meta["_cs_Language_content"][0]; ?>   </span> </div>
							    <?php } ?>
								
							 </div>		
								
							
					</div>
					<?php $cs_addnightroomprice=0; 
					if(isset($meta["_cs_addnightroomprice"][0])) $cs_addnightroomprice=$meta["_cs_addnightroomprice"][0]; 
					?>
					
					<div class="tour_body" >

						<div class='tour_title'> 
								Select your travel date and number of people
						</div>
						<div class='input_div clearfix'> 
							<div class="col-md-6">
									<span>Departure date： </span> <input type="text" id="datepicker" class="calender" /> 
							</div>
							<?php  if(intval($cs_addnightroomprice==0)){ ?>
							<div class="col-md-6"><div class="end_date_div hidden"> End date： <span class="end_date" >  </span> </div></div>
									<?php }else{?>
								<div class="col-md-6">
										<span>End date: </span> <input type="text" id="end_date" class="calender" /> </div>
									<?php }?>
								<input  type="hidden" class="cs_tourdays" value="<?php if(isset($meta["_cs_tourdays"][0])) echo $meta["_cs_tourdays"][0]; ?>" />
						</div>
						<div class='input_div clearfix'>    
						   <div class="col-md-6"> 
								<span class="number_of_room_span">
								<?php if(isset($meta["_cs_tourdays"][0])&& $meta["_cs_tourdays"][0]>1){
											echo " Number of rooms:";
										}else if(isset($meta["_cs_tourdays"][0]))
										{
										echo "Number of people: ";
										}
										?>
								 </span>
								<input type="text" class="number_of_room" value="Click to select"/>
							
								<select class="number_of_room hidden"> 
									
									<option value="1"> 1 </option>
									<option value="2"> 2 </option>
									<option value="3"> 3 </option>
									<option value="4"> 4 </option>
									<option value="5"> 5 </option>
									<option value="6"> 6 </option>
								</select>
							</div>
						</div>
				<div class="hide_section hidden">	
				
						<div class="sp_tour"></div>
			<div class="added_room">
				<div class='input_div '> 
								<span>Room 1: </span>
								
						<?php
						$cs_personsperroom=0;
						 if(isset($meta["_cs_personsperroom"][0])) $cs_personsperroom=$meta["_cs_personsperroom"][0]; 
							
							
						?>
						<select class="room_ppl_number Adult" name="Adult"> 
						<option value="0">adults</option>
						<?php for($room=1;$room<=$cs_personsperroom;$room++){?>
									<option value="<?php echo $room;?>"><?php echo $room;?> adults </option><!-- Adult-->
						<?php } ?>
						</select> 
						<select class="room_ppl_number child"  name="child" > 
								<option value="0">Child</option>
								<?php for($room=1;$room<=$cs_personsperroom;$room++){?>
										<option value="<?php echo $room;?>"><?php echo $room;?> child </option><!-- child-->
								<?php } ?>
						</select> 
						
						<div id="hidden_adult" class="hidden"> 
						<option value="0">Adult</option>
						<?php for($room=1;$room<=$cs_personsperroom;$room++){?>
									<option value="<?php echo $room;?>"><?php echo $room;?> adults </option><!-- Adult-->
						<?php } ?>
						</div>
						
						<div id="hidden_child" class="hidden"> 
						<option value="0">Child</option>
								<?php for($room=1;$room<=$cs_personsperroom;$room++){?>
										<option value="<?php echo $room;?>"><?php echo $room;?> child </option><!-- child-->
								<?php } ?>
						</div>
						
				</div>	
			</div>
				
				<div class='input_div '> 
					<button class="confirm" > OK &nbsp;&nbsp;</button>
				</div>
				
				<div class=' notice'>  
					Adult <span class="change_price" or_pic=" <?php if(isset($meta["_cs_adultprice"][0])) echo $meta["_cs_adultprice"][0]; ?>"> Rp <span class="price_span"> <?php if(isset($meta["_cs_adultprice"][0])) echo $meta["_cs_adultprice"][0]; ?> </span>  </span> / person, 
					Child <span class="change_price" or_pic="<?php if(isset($meta["_cs_childrenprice"][0])) echo $meta["_cs_childrenprice"][0]; ?>"> Rp <span class="price_span"> <?php if(isset($meta["_cs_childrenprice"][0])) echo $meta["_cs_childrenprice"][0]; ?> </span> </span>/ person 
				</div>
			</div>
		<div class="hide_section_one_day hidden">	
				
						<div class="sp_tour"></div>
			<div class="">
				<div class='one_day_div '> 
								<span>Adult </span> <input class="one_day adult_number valid_num " type="text" value="" />
								
								<span>Child </span> <input class="one_day child_number valid_num"  type="text" value="" />
					
				</div>	
			</div>
				
				<div class='input_div '> 
					<button class="confirm" > OK &nbsp;&nbsp;</button>
				</div>
				
				<div class=' notice'>  
					Adult <span class="change_price" or_pic=" <?php if(isset($meta["_cs_adultprice"][0])) echo $meta["_cs_adultprice"][0]; ?>"> Rp <span class="price_span"> <?php if(isset($meta["_cs_adultprice"][0])) echo $meta["_cs_adultprice"][0]; ?> </span>  </span> / person, 
					Child <span class="change_price" or_pic="<?php if(isset($meta["_cs_childrenprice"][0])) echo $meta["_cs_childrenprice"][0]; ?>"> Rp <span class="price_span"> <?php if(isset($meta["_cs_childrenprice"][0])) echo $meta["_cs_childrenprice"][0]; ?> </span> </span>/ person 
				</div>
			</div>			
					</div>
					
					<div class="info">
					
					We advise you to book the trip no closer than 1 months before your arrival. The price above apply only if we get enough number of customers on the given date.
					

					</div>
			
			        <div class="result"> 
					
					<div class="col-md-6 col-xs-6"> 
						<span class="price"> Total： </span>  
						<span class="price_value"> </span> 
					</div>  
					<div class="col-md-6 col-xs-6 button_div"> 
						<button class="buy" > BOOK ! &nbsp;&nbsp;</button>
					</div>
					<div class="price_detail hidden" style="text-align:center"> Total Price = (Number of Adult/Children + Hotel) x Value Added Tax </div> 
					</div>
            </div>
        </div>
  <?php 


function nl2br_save_html($string)
{
    if(! preg_match("#</.*>#", $string)) // avoid looping if no tags in the string.
        return nl2br($string);

    $string = str_replace(array("\r\n", "\r", "\n"), "\n", $string);

    $lines=explode("\n", $string);
    $output='';
    foreach($lines as $line)
    {
        $line = rtrim($line);
        if(! preg_match("#</?[^/<>]*>$#", $line)) // See if the line finished with has an html opening or closing tag
            $line .= '<br />';
        $output .= $line . "\n";
    }

	return $output;
}
   

	?>      
        <div class="row tour_info">
            <div class="col-md-12">
                <div id="turcnt">
				
					<?php if( trim(get_post_meta($post->ID,'_cs_overview_content',true))!="") { ?>
					<p name="overview" id="overview">&nbsp;</p>
                    <h2><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_overview_heading',true)); ?></h2>
                    <?php echo do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_overview_content',true))); 
					}
                    ?>
					
					<?php

		              /*  $my_postid = 4065;//This is page id or post id
						$content_post = get_post($my_postid);
						$content = $content_post->post_content;
						$title = $content_post->post_title;
						$content = apply_filters('the_content', $content);
						$content = str_replace(']]>', ']]&gt;', $content);
						echo "<hr> <h1> ".$title."</h1> <br> ";
						echo $content;		
                        */
                    ?>
					
					<?php if( trim(get_post_meta($post->ID,'_cs_location_details_content_nontour',true))!=""){ ?>
                    <p name="meetingplace" id="meetingplace">&nbsp;</p>
                    <h2  class="ico2"><?php echo get_post_meta($post->ID,'_cs_location_details_nontour',true); ?></h2>
                    <?php echo do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_location_details_content_nontour',true))); 
						}
					?>
					
				
					<?php /*if( trim(get_post_meta($post->ID,'_cs_gps_url',true))!=""){ ?>
                    <p name="gps" id="gps" >&nbsp;</p>
                    <h2 class="ico3">GPS</h2>
                    <?php
                    $sGps = nl2br(get_post_meta($post->ID,'_cs_gps_url',true));
					if(stripos($sGps,'http')!==FALSE) {
						$img = clsImg($sGps);
						echo '<img width="1100px" src="'.$img.'" alt="" class="img-responsive" />'; 
					}
					else
					{
						echo do_shortcode(nl2br_save_html($sGps));
					}
					}*/
					?>
					
					<?php if( trim(get_post_meta($post->ID,'_cs_price_content_nontour',true))!=""){ ?>
                    <p name="Price"  id="Price">&nbsp;</p>
                    <h2 class="ico4"><?php echo nl2br(get_post_meta($post->ID,'_cs_Price_heading_nontour',true)); ?></h2>
					<?php 
						
						echo do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_price_content_nontour',true)));
					}
					?>

					<?php if( trim(get_post_meta($post->ID,'_cs_activities_content_nontour',true))!=""){ ?>
                    <p name="Hotels"  id="Hotels" >&nbsp;</p>
                    <h2  class="ico5"><?php echo nl2br_save_html(get_post_meta($post->ID,'_cs_Activities_heading_nontour',true)); ?></h2>
                    <?php 
							echo  do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_activities_content_nontour',true))); 
					}
					?>
					
					<?php if( trim(get_post_meta($post->ID,'_cs_attractionscnt_nontour',true))!=""){ ?>
                    <p name="attractions"  id="attractions" >&nbsp;</p>
                   <h2  class="ico6"><?php echo get_post_meta($post->ID,'_cs_attractions_heading_nontour',true); ?></h2>
                    <?php 

						echo  do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_attractionscnt_nontour',true))); 
					}	
					?>
					
					<?php if( trim(get_post_meta($post->ID,'_cs_Precautions_heading_nontour',true))!=""){ ?>
                    <p name="Precautions" id="Precautions">&nbsp;</p>
                   <h2  class="ico7"><?php echo get_post_meta($post->ID,'_cs_Precautions_heading_nontour',true); ?></h2>
                    <?php 

						echo do_shortcode(nl2br_save_html(get_post_meta($post->ID,'_cs_Precautionscnt_nontour',true)));
						}
					?>
					
                </div>
            </div>
        </div>
        <?php 
        // If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
        ?>
           <div id="left_menu"> 
			<ul id="left_menu_content">  
				
				<?php if( trim(get_post_meta($post->ID,'_cs_overview_content',true))!=""){ ?>
				<li> <a href="#overview"> <i class="fa fa-life-ring"></i>   <br><span>Overview</span></a> </li>
				<?php }?>			

				<?php if( trim(get_post_meta($post->ID,'_cs_meetingplace_content',true))!=""){ ?>
			    <li> <a href="#meetingplace"> <i class="fa fa-tencent-weibo"></i>  <br><span>Meeting</span></a></li>
				<?php } ?>
				
				
				<?php if( trim(get_post_meta($post->ID,'_cs_gps_url',true))!="") {?>
				<li> <a href="#gps"> <i class="fa fa-map-marker"></i>  <br><span>GPS</span></a></li>
				<?php } ?>
				
				<?php if( trim(get_post_meta($post->ID,'_cs_price_content',true))!=""){ ?>
				<li> <a href="#Price"> <i class="fa fa-money"></i> <br><span>Price</span></a></li>
				<?php } ?>
				
				
				<?php if( trim(get_post_meta($post->ID,'_cs_hotels',true))!=""){ ?>
				<li> <a href="#Hotels"> <i class="fa fa-hospital-o"></i> <br><span>Hotels</span></a></li>
				<?php }?>
				
				
				<?php if( trim(get_post_meta($post->ID,'_cs_attractions',true))!=""){ ?>
				<li> <a href="#attractions"> <i class="fa fa-picture-o"></i>  <br><span>Attractions</span></a></li>
				<?php } ?>
				
				
				<?php if( trim(get_post_meta($post->ID,'_cs_Precautions',true))!="") { ?>
				<li> <a href="#Precautions"> <i class="fa fa-slideshare"></i> <br><span>Precautions</span></a></li>
				<?php } ?>
			</ul>
	</div>       
		<?php
	}
} else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
        ?>


		<script type="text/javascript">
    	function addCommas(nStr) {
    		nStr += '';
    		x = nStr.split('.');
    		x1 = x[0];
    		x2 = x.length > 1 ? '.' + x[1] : '';
    		var rgx = /(\d+)(\d{3})/;
    		while (rgx.test(x1)) {
    			x1 = x1.replace(rgx, '$1' + ',' + '$2');
    		}
    		return x1 + x2;
    	}

    	function calTotalRRRPPP() {
    		var totalRp = 0;
    		var totalRb = 0;
    		$("#tablepress-62").find("input[type=number]").each(function () {
    			var rp = parseInt($(this).val()) * parseInt($(this).attr("data-rp"));
    			var rb = parseInt($(this).val()) * parseInt($(this).attr("data-rb"));

    			$(this).parents("td:first")
					   .next().html("Rp" + addCommas(rp.toString()))
    			       .next().html("$" + addCommas(rb.toString()));
    			totalRp += rp;
    			totalRb += rb;
    		});
    		$("#spanTotalRP").html("Rp" + addCommas(totalRp.toString()));
    		$("#spanTotalRB").html("$" + addCommas(totalRb.toString()));

    		var taxAndServiceRp = parseInt(totalRp * 0.1);
    		var taxAndServiceRb = parseInt(totalRb * 0.1);
    		$("#spanTaxAndServiceRP").html("Rp" + addCommas(taxAndServiceRp.toString()));
    		$("#spanTaxAndServiceRB").html("$" + addCommas(taxAndServiceRb.toString()));

    		var totalandfeerp = totalRp + taxAndServiceRp;
    		var totalandfeerb = totalRb + taxAndServiceRb;
    		$("#spanTotalAndFeeRP").html("Rp" + addCommas(totalandfeerp.toString()));
    		$("#spanTotalAndFeeRb").html("$" + addCommas(totalandfeerb.toString()));
    	}

    	$(document).ready(function () {
    		$("#tablepress-62").find("input[type=number]").each(function () {
    			$(this).val("0");
    			$(this).attr("min", "0");
    			var datarp = $(this).parents("td:first").prev().prev().prev().html().replace("Rp", "").replace(/,/g, "");
    			var datarb = $(this).parents("td:first").prev().html().replace("$", "").replace(/,/g, "");
    			$(this).attr("data-rp", datarp).attr("data-rb", datarb);
    		});

    		calTotalRRRPPP();

    		$("#tablepress-62").find("input[type=number]").change(function () {
    			calTotalRRRPPP();
    		});
    	});


	</script>



