<?php

function p($data, $exit = 1) {
    echo "<pre>";
    print_r($data);
    if ($exit == 1) {
        exit;
    }
}

global $wpdb;
if (isset($_POST['action']) && $_POST['action'] == 'create_ticket') {
//    p($_POST);

    $toUserMailBody = file_get_contents('https://www.travpart.com/mail/create_ticket_user.html');
    $cs_user_id = isset($_POST['cs_user_id']) ? $_POST['cs_user_id'] : '';
    $cs_user_name = isset($_POST['cs_user_name']) ? $_POST['cs_user_name'] : '';
    $toUserMailBody = str_replace("_NAME_", ucfirst($cs_user_name), $toUserMailBody);
    $cs_type = isset($_POST['cs_type']) ? $_POST['cs_type'] : '';
    $cs_description = isset($_POST['cs_description']) ? $_POST['cs_description'] : '';
    $subject = "Request receive by Travpart Help Center: " . $cs_type;

    $userType = 'Guest';
    $userEmail = '';

    $localUserData = $wpdb->get_results("SELECT * FROM user WHERE username = '$cs_user_name'", ARRAY_A);
    if (!empty($localUserData)) {
        $localUserData = $localUserData[0];
        $userEmail = isset($localUserData['email']) ? $localUserData['email'] : '';
        $access = $localUserData['access'];
        if ($access == 2) {
            $userType = 'Customer';
        } else if ($access == 1) {
            $userType = 'Agent';
        }
    }

    $toAdminMailBody = file_get_contents('https://www.travpart.com/mail/create_ticket_admin.html');
    $toAdminMailBody = str_replace("_UserName_", ucfirst($cs_user_name), $toAdminMailBody);
    $toAdminMailBody = str_replace("_SUBJECT_", $cs_type, $toAdminMailBody);
    $toAdminMailBody = str_replace("_UserType_", $userType, $toAdminMailBody);
    $toAdminMailBody = str_replace("_UserEmail_", $userEmail, $toAdminMailBody);
    $toAdminMailBody = str_replace("_DESC_", $cs_description, $toAdminMailBody);


    $wpdb->insert(
            'contact_support', array(
        'cs_user_id' => $cs_user_id,
        'cs_user_name' => $cs_user_name,
        'cs_type' => $cs_type,
        'cs_description' => $cs_description,
        'cs_created' => date('Y-m-d H:i:s'),
        'cs_updated' => date('Y-m-d H:i:s'),
            ), array(
        '%d',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s'
            )
    );
    if ($wpdb->insert_id) {
        $toUserMailBody = str_replace("_TICKET_ID_", '000' . $wpdb->insert_id, $toUserMailBody);
        $toAdminMailBody = str_replace("_TICKET_ID_", '000' . $wpdb->insert_id, $toAdminMailBody);
        sendMail($userEmail, $subject, $toUserMailBody);
        sendMail('contact@tourfrombali.com', 'New Support ticket received', $toAdminMailBody);
        $data['status'] = 1;
        $data['ticket_id'] = $wpdb->insert_id;
        $data['message'] = "Your ticket has been created success, we will reach you in next 2 working days.";
        echo json_encode($data);
        exit;
    } else {
        $data['status'] = 0;
        $data['message'] = "OPPS!Something want wrong.";
        echo json_encode($data);
        exit;
    }
}






if (isset($_POST['action']) && $_POST['action'] == 'ticket_comment') {
    $cscr_attachment = '';
    $cscr_contact_support_id = isset($_POST['cscr_contact_support_id']) ? $_POST['cscr_contact_support_id'] : '';
    $cscr_user_id = isset($_POST['cscr_user_id']) ? $_POST['cscr_user_id'] : '';
    $cscr_user_name = isset($_POST['cscr_user_name']) ? $_POST['cscr_user_name'] : '';
    $cscr_description = isset($_POST['cscr_description']) ? $_POST['cscr_description'] : '';
    $uploads_dir = 'support_upload/';
    if (isset($_FILES["cscr_attachment"]["tmp_name"]) && $_FILES["cscr_attachment"]["tmp_name"] != '') {
        $tmp_name = $_FILES["cscr_attachment"]["tmp_name"];
        $name = 'support_' . time() . basename($_FILES["cscr_attachment"]["name"]);
        $fullPath = $uploads_dir . $name;
        move_uploaded_file($tmp_name, $fullPath);
        $cscr_attachment = 'https://www.travpart.com/English/' . $fullPath;
    }
    $wpdb->insert(
            'contact_support_comment_rel', array(
        'cscr_contact_support_id' => $cscr_contact_support_id,
        'cscr_user_id' => $cscr_user_id,
        'cscr_user_name' => $cscr_user_name,
        'cscr_description' => $cscr_description,
        'cscr_attachment' => $cscr_attachment,
        'cscr_created' => date('Y-m-d H:i:s'),
        'cscr_modified' => date('Y-m-d H:i:s'),
            ), array(
        '%d',
        '%d',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s'
            )
    );
    header('Location:https://www.travpart.com/English/create-a-new-ticket/?ticketid=' . $cscr_contact_support_id);
    exit;
}

if (isset($_POST['action']) && $_POST['action'] == 'ticket_clost') {
    $ticket_id = isset($_POST['ticket_id']) ? $_POST['ticket_id'] : '';

    $wpdb->update(
            'contact_support', array(
        'cs_status' => '1',
            ), array('cs_id' => $ticket_id)
    );

    $data['status'] = 1;
    $data['message'] = "Success.";
    echo json_encode($data);
    exit;
}

function sendMail($to, $subject, $message) {

    $headers = "From: no-reply@travpart.com\r\n";
    $headers .= "Reply-To: no-reply@travpart.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

    $key = "Tt3At58P6ZoYJ0qhLvqdYyx21";
    $postdata = array('to' => $to,
        'subject' => $subject,
        'message' => $message);
    $url = "https://www.tourfrombali.com/api.php?action=sendmail&key={$key}";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $data = curl_exec($ch);
    if (curl_errno($ch) || $data == FALSE) {
        curl_close($ch);
        return FALSE;
    } else {
        curl_close($ch);
        return TRUE;
    }

//    mail($to, $subject, $message, $headers);
}
