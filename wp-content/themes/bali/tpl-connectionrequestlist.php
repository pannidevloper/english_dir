<?php
/*
Template Name: New Connection Request List
*/

get_header();

global $wpdb;

// check user status
if (!is_user_logged_in()) {
    exit(wp_redirect(home_url('login')));
}

$current_user = wp_get_current_user();


//count how many requests pending


$pending_requests_count = $wpdb->get_var(
    "SELECT COUNT(DISTINCT user1) FROM friends_requests WHERE  user2 = '{$current_user->ID}' AND status=0" );


// get pending requests
$pending_requests =  $wpdb->get_results( "SELECT * FROM `friends_requests` WHERE status=0 AND user2='{$current_user->ID}'");

//var_dump($pending_requests);
?>
<div class="connectionlist_main_area">
        <div class="container new-short-post rem_pd">
            <div class="row">
                <div class="col-md-12">
                    <div class="header_txt">
                        <h2>Connection Request List</h2>
                    </div>
                </div>
            </div>
         <div class="row">
                <div class="col-md-12">   
        <div class="connectionlistwrap">
         <div class="connectionlist">
            <div class="connectionlistitem">
            <div class="connectionlistiteminner connectionlistbtninner clearfix">
                <div class="conlistleftbtn"><p>You have received <?php echo $pending_requests_count; ?> connection requests </p></div>
                <div class="conlistrightbtn"><a href="https://www.travpart.com/English/sent-request-list/">Check my sent connection request list</a></div>
            </div>
           </div><!-- End of listitem-->
           <?php
           foreach ($pending_requests as $request) {

   ?>
           
           <div class="connectionlistitem remove_<?php echo $request->id; ?>">
            <div class="connectionlistiteminner_content">
            <div class="connectionphoto"><a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $request->user_login; ?>">
           <img alt="travpart" width="100px" height="100px" src="<?php echo um_get_avatar_url(um_get_avatar('', $request->user1)); ?>">
                                                    </a></div>
             <div class="connectiondetail">
                <?php
                $author_obj = get_user_by('id', $request->user1);
                ?>
              <div class="connectionname"><a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $author_obj->user_login; ?>"><?php  echo um_get_display_name($request->user1);  ?></a></div>
           <!--   <div class="mutualdetail"><img  src="https://www.travpart.com/English/wp-content/uploads/2019/11/30x30.png"/><b>Azzy</b> and 29 other common <span>connection</span></div>     
            <--->
              </div>
             <div class="connectionbtns connectionbtns<?php echo $request->id; ?>">
                <div class="accbtn confirm_friend<?php echo $request->id; ?>" ><a href="#"  class="confirm_friend" data-id="<?php echo $request->id; ?>" data-type="1" ><i class="fas fa-user-plus"></i> <span class="hide_t">Accept</span> </a></div>
                <div class="removebtn remv<?php echo $request->id; ?>"><a href="#" class="confirm_friend" data-id="<?php echo $request->id; ?>" data-type="2"><span class="hide_t">Reject</span> <span>x</span></a></div>
             </div>
        </div>
          </div><!-- End of listitem-->
          <?php  } ?>
         </div>
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $(document).on('click','.confirm_friend',function(){
        var btn = $(this);
        //btn.html('<img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader1.gif">');
        var url = '<?php echo site_url() ?>/submit-ticket/';
        var request_id = $(this).data('id');
        var typeo = $(this).data('type');
        $('.remv'+request_id).hide();
        /*if(typeo==1){
            $('.confirm_friend'+request_id).remove();
            $('.connectionbtns'+request_id).prepend('<div class="btn-group"><button type="button" class="btn btn-default custom_un">Connected</button><button type="button" class="btn btn-default custom_un dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h"></i><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu con_drdpw"><li><a href="#">Unconnect</a></li></ul></div>')
            $('.remv'+request_id).remove();
        }else{
            btn.text('Rejected');
        }
        return false;*/
        
        $.ajax({
            type: "POST",
            url: url,
            data: {request_id: request_id, typeo:typeo, action: 'add_friend'},
            beforeSend: function() {
                //btn.html('<div class="spinner"></div>');
            },
            success: function (data) {
                
                var result = JSON.parse(data);
                if (result.status) {
                    if(result.message==null || result.message==''){}else{
                        //$(".remove_"+request_id).remove();    
                        if(typeo==1){
                            $('.confirm_friend'+request_id).remove();
                            $('.connectionbtns'+request_id).prepend('<div class="btn-group" style="float:right;"><button type="button" class="btn btn-default custom_un">Connected</button><button type="button" class="btn btn-default custom_un dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h"></i><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu"><li><a href="#">Unconnect</a></li></ul></div>')
                        }else{
                            btn.text('Rejected');
                        }
                    }
                } else {
                    if(result.message==null || result.message==''){}else{
                        if(typeo==1){
                          $('.confirm_friend'+request_id).remove();
                          $('.connectionbtns'+request_id).prepend('<div class="btn-group"><button type="button" class="btn btn-default custom_un">Connected</button><button type="button" class="btn btn-default custom_un dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h"></i><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu"><li><a href="#">Unconnect</a></li></ul></div>')
                        }else{
                            btn.text('Rejected');
                        }
                        //$(".remove_"+request_id).remove();    
                    }
                }
            }
        });
    });
    
});
</script>


<style>
.con_drdpw li a:hover{
    background: #eaeaea;
    transition: 1s ease;
}
.con_drdpw li a{
    font-size: 14px;
}
.spinner {
    width: 20px;
    height: 20px;
    border: 4px #ddd solid;
    border-top: 4px #2e93e6 solid;
    border-radius: 50%;
    animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
    100% { 
        transform: rotate(360deg); 
    }
}
.clearfix:after {
  visibility: hidden;
  display: block;
  font-size: 0;
  content: " ";
  clear: both;
  height: 0;
}
.clearfix { display: inline-block; }    
.connectionlist {
    font-family: "Open Sans", Arial, Helvetica, sans-serif;
    font-size: 18px;
}   
.connectionphoto {
    //float: left;
    //width: 100px;
    display: inline-block;
    vertical-align: middle;
    width: 10%;
}
.custom_un{
    background: #eaeaea;
    border: 1px solid #ccc;
    font-size: 15px;
    padding: 8px;
}
.connectiondetail {
   // width: 500px;
    //float: left;
    //margin: 0 27px;
    display: inline-block;
    vertical-align: middle;
    width: 60%;
}
.connectionbtns {
   // width: 240px;
   // float: left;
}
.connectionname {
    color: #385898;
    font-weight: bold;
    padding-top: 17px;
}   
.connectionlist {
    background-color: #fff;
    box-sizing: border-box;
    padding: 0;
    box-shadow: 0 0px 5px 0px #ccc;
}
.connectionlistitem {
    border-bottom: 1px solid #ccc;
    text-align: center;
    padding-bottom: 10px;
    padding-top: 16px;
}
.connectionlistiteminner {
    text-align: left;
}
.accbtn {
    display: inline-block;
    margin-right: 0px;
    vertical-align: middle;
    background-color: #018867;
    padding: 6px 18px;
    cursor: pointer;
    box-shadow: 2px 2px 2px #024c3aeb;
}

.accbtn:hover{
    background: #01a77d;
    transition: 1s ease;
}
.accbtn a {
    color: #fff;
    text-decoration: none;
}
.removebtn {
    display: inline-block;
    cursor: pointer;
    border-radius: 3px;
    box-shadow: 2px 1px 3px 0px #908585;
    margin-right: 10px;
    vertical-align: middle;
    border:1px solid #ccc;
    padding: 6px 17px;
}
.removebtn:hover{
    background: #d8d8d8;
    transition: 1s ease;
}
.removebtn a {
    color: #000;
    font-size: 15px;
    text-decoration: none;
}
.mutualdetail {
    font-size: 16px;
}
.connectionbtns {
    //width: 240px;
    //float: left;
    //margin-top: 32px;
    display: inline-block;vertical-align: middle;
    width: 25%;
}
.mutualdetail img {
    border-radius: 52px;
}
.header_txt h2 {
    text-align: center;
    margin-bottom: 50px;
        color: #1A6D84;
    font-size: 30px;
    text-transform: uppercase;
}
.connectionlistbtninner{
    width: 87%;
}
.conlistleftbtn {
    float: left;
    
}
.conlistleftbtn p {
       background-color: #385898;
    color: #fff;
    padding: 8px 15px;
    border-radius: 4px;
}
.conlistrightbtn {
    float: right;
    width: 34%;
}
.conlistrightbtn a {
    color: #000;
    border: 1px solid #000;
    font-size: 17px;
    padding: 4px 17px;
    box-sizing: border-box;
}
@media only screen and (max-width: 960px) {
.connectiondetail {
    //width: 330px;
}
.conlistrightbtn {
    float: right;
    width: 39%;
}
.conlistrightbtn a {  
    font-size: 15px;
    padding: 4px 4px;
}
.conlistleftbtn a {
    padding: 8px 8px;
    font-size: 15px;
}
.conlistleftbtn {
    float: left;
    width: 55%;
}

}
@media only screen and (max-width: 770px) {


.removebtn{
    padding: 5px 9px;
    font-size: 11px;
    margin-right:0px; 
}
.custom_un{
    font-size: 12px;
    padding: 4px;
}
.removebtn a{
    font-size: 12px;
}
.accbtn {
    display: inline-block;
    margin-right: 0;
    padding: 1px 0px;
    box-shadow: none;
    margin-bottom: 0px;
    border-radius: 3px;
}
.accbtn a {
    font-size: 11px;
    padding: 6px 14px;
}

}
@media only screen and (max-width: 767px) {
.connectionlistiteminner {
    text-align: center;
}
.rem_pd{
    padding: 0px;
}
.con_drdpw li a{
    padding: 10px;
    color: #000;
}
.connectionphoto{
    width: 15%;
}
.connectionphoto a img{
    width: auto;
    height: 55px;
    border-radius: 100%;
}
.connectiondetail {
    width: 34%;
    float: none;
    margin: 0 auto;
}

.connectionbtns {
    width: 47%;
    text-align: center;
}

.accbtn {
    display: inline-block;
    margin-right: 0;
    //padding: 7px 0px;
    margin-bottom: 0px;
}
.conlistrightbtn {
    float: none;
    width: 100%;
    margin-top: 12px;
}
.conlistleftbtn {
    float: none;
    width: 100%;
}
.connectionlistbtninner {
    width: 100%;
}
}
@media(max-width: 600px) {
.connectionlistiteminner_content.clearfix {
    display: flex;
    margin-left: 10px;
    margin-right: 10px;
}
.connectionlist_main_area{
    padding-top:55px;
}
.header_txt h2{
    font-size: 17px;
}
.connectionphoto picture img {
    width: 70px;
}
.connectionname {
    font-size: 16px;
    padding-top: 0px;
}
.mutualdetail {
    font-size: 13px;
}
}
@media(max-width: 480px) {
    .accbtn a{
        padding: 0px 8px;
    }
}
@media(max-width: 240px) {
    .connectionphoto a img{
        height:40px;
    }
    .connectionphoto {
        width: 14%;
    }
    .custom_un{
        font-size: 12px;
        padding: 4px;
    }
    .connectionname{
        font-size:12px;
    }
    .removebtn a{
        font-size:12px;
    }
    .conlistleftbtn a{
        font-size: 10px;
    }
}

</style>
<style type="text/css">
    #footer {
        display: none;
    }
</style>
<?php
get_footer();
?>