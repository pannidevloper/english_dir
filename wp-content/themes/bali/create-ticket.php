<?php

/* Template Name: Create Ticket Template */ get_header();
?>
<style>
    h1{font-size:30px!important;}
    section{padding:10px 0; border-left:1px solid #ccc;border-right:1px solid #ccc;}
    textarea{width: 100%;min-height:300px;}
    .icon {
        display: inline-block;
        width: 1em;
        height: 1em;
        stroke-width: 0;
        stroke: currentColor;
        fill: currentColor;
    }

    .wrapper {
        width: 100%;
        height: auto;
        min-height: 100vh;
        padding: 50px 20px;
        padding-top: 100px;
    }
    .message-card {
        margin-top: 100px!important;
        padding-top: 10px;
    }
    .submit-card{margin-top:20px!important;padding-top:20px;}
    .submit-card .button{cursor:pointer;width: 100%;position: absolute;bottom:0;left:0;background:#37a000;color:white;text-align: center;font-size: 25px;padding:20px;}
    .userName, .adminName{display: inline-block;padding:2px 10px;border-left: 5px solid #276a7d;margin:0 0 10px 10px;}
    .adminName{border-left:5px solid #43ec00;}
    .messageDate{display: inline-block;color:#666;}
    .adminmessage{color:#464646;}
    .tipsy{display:none!important;}

    /* Upload file */
    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
        cursor: pointer;
    }

    .btn {
        border: 2px solid #276a7d;
        padding: 5px 20px;
        border-radius: 8px;
        font-size: 15px;
        font-weight: bold;
        background: #3b898a;
        color: white;
        cursor: pointer;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;  
        cursor: pointer;
    }
    @media only screen and (max-width: 600px) {
        .wrapper {
        padding-top: 10px!important;
        padding-bottom:10px!important;
        }    
        .message-card {
        margin-top: 10px!important;
        padding-top: 10px;
        }
        .profile-card{margin-top:100px!important;}
    }
    @media screen and (max-width: 768px) {
        .wrapper {
            height: auto;
            min-height: 100vh;
            padding-top: 100px;
        }
    }

    .profile-card, .message-card, .submit-card {
        width: 100%;
        min-height: 460px;
        margin: auto;
        box-shadow: 0px 8px 60px -10px rgba(13, 28, 39, 0.6);
        background: #fff;
        border-radius: 12px;
        max-width: 700px;
        position: relative;
    }
    .profile-card.active .profile-card__cnt {
        filter: blur(6px);
    }
    .profile-card.active .profile-card-message,
    .profile-card.active .profile-card__overlay {
        opacity: 1;
        pointer-events: auto;
        transition-delay: .1s;
    }
    .profile-card.active .profile-card-form {
        transform: none;
        transition-delay: .1s;
    }
    .profile-card__img {
        width: 150px;
        height: 150px;
        margin-left: auto;
        margin-right: auto;
        transform: translateY(-50%);
        border-radius: 50%;
        overflow: hidden;
        position: relative;
        z-index: 4;
        box-shadow: 0px 5px 50px 0px #6c44fc, 0px 0px 0px 7px rgba(107, 74, 255, 0.5);
    }
    @media screen and (max-width: 576px) {
        .profile-card__img {
            width: 120px;
            height: 120px;
        }
    }
    .profile-card__img img {
        display: block;
        width: 100%;
        height: 100%;
        object-fit: cover;
        border-radius: 50%;
    }
    .profile-card__cnt {
        margin-top: -35px;
        text-align: center;
        padding: 0 20px;
        padding-bottom: 40px;
        transition: all .3s;
    }
    .profile-card__name {
        font-weight: 700;
        font-size: 24px;
        color: #6944ff;
        margin-bottom: 15px;
    }
    .profile-card__txt {
        font-size: 18px;
        font-weight: 500;
        color: #324e63;
        margin-bottom: 15px;
    }
    .profile-card__txt strong {
        font-weight: 700;
    }
    .profile-card-loc {
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 18px;
        font-weight: 600;
    }
    .profile-card-loc__icon {
        display: inline-flex;
        font-size: 27px;
        margin-right: 10px;
    }
    .profile-card-inf {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
        align-items: flex-start;
        margin-top: 35px;
    }
    .profile-card-inf__item {
        padding: 10px 35px;
        min-width: 150px;
    }
    @media screen and (max-width: 768px) {
        .profile-card-inf__item {
            padding: 10px 20px;
            min-width: 120px;
        }
    }
    .profile-card-inf__title {
        font-weight: 700;
        font-size: 27px;
        color: #324e63;
    }
    .profile-card-inf__txt {
        font-weight: 500;
        margin-top: 7px;
    }
    .profile-card-social {
        margin-top: 25px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;
    }
    .profile-card-social__item {
        display: inline-flex;
        width: 55px;
        height: 55px;
        margin: 15px;
        border-radius: 50%;
        align-items: center;
        justify-content: center;
        color: #fff;
        background: #405de6;
        box-shadow: 0px 7px 30px rgba(43, 98, 169, 0.5);
        position: relative;
        font-size: 21px;
        flex-shrink: 0;
        transition: all .3s;
    }
    @media screen and (max-width: 768px) {
        .profile-card-social__item {
            width: 50px;
            height: 50px;
            margin: 10px;
        }
    }
    @media screen and (min-width: 768px) {
        .profile-card-social__item:hover {
            transform: scale(1.2);
        }
    }
    .profile-card-social__item.facebook {
        background: linear-gradient(45deg, #3b5998, #0078d7);
        box-shadow: 0px 4px 30px rgba(43, 98, 169, 0.5);
    }
    .profile-card-social__item.twitter {
        background: linear-gradient(45deg, #1da1f2, #0e71c8);
        box-shadow: 0px 4px 30px rgba(19, 127, 212, 0.7);
    }
    .profile-card-social__item.instagram {
        background: linear-gradient(45deg, #405de6, #5851db, #833ab4, #c13584, #e1306c, #fd1d1d);
        box-shadow: 0px 4px 30px rgba(120, 64, 190, 0.6);
    }
    .profile-card-social__item.behance {
        background: linear-gradient(45deg, #1769ff, #213fca);
        box-shadow: 0px 4px 30px rgba(27, 86, 231, 0.7);
    }
    .profile-card-social__item.github {
        background: linear-gradient(45deg, #333333, #626b73);
        box-shadow: 0px 4px 30px rgba(63, 65, 67, 0.6);
    }
    .profile-card-social__item.codepen {
        background: linear-gradient(45deg, #324e63, #414447);
        box-shadow: 0px 4px 30px rgba(55, 75, 90, 0.6);
    }
    .profile-card-social__item.link {
        background: linear-gradient(45deg, #d5135a, #f05924);
        box-shadow: 0px 4px 30px rgba(223, 45, 70, 0.6);
    }
    .profile-card-social .icon-font {
        display: inline-flex;
    }
    .profile-card-ctr {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 40px;
    }
    @media screen and (max-width: 576px) {
        .profile-card-ctr {
            flex-wrap: wrap;
        }
    }
    .profile-card__button {
        background: none;
        border: none;
        font-family: 'Quicksand', sans-serif;
        font-weight: 700;
        font-size: 15px;
        margin: 15px 35px;
        padding: 15px 40px;
        min-width: 150px;
        border-radius: 50px;
        min-height: 35px;
        color: #fff;
        cursor: pointer;
        backface-visibility: hidden;
        transition: all .3s;
    }
    @media screen and (max-width: 768px) {
        .profile-card__button {
            min-width: 170px;
            margin: 15px 25px;
        }
    }
    @media screen and (max-width: 576px) {
        .profile-card__button {
            min-width: inherit;
            margin: 0;
            margin-bottom: 16px;
            width: 100%;
            max-width: 300px;
        }
        .profile-card__button:last-child {
            margin-bottom: 0;
        }
    }
    .profile-card__button:focus {
        outline: none !important;
    }
    @media screen and (min-width: 768px) {
        .profile-card__button:hover {
            transform: translateY(-5px);
        }
    }
    .profile-card__button:first-child {
        margin-left: 0;
    }
    .profile-card__button:last-child {
        margin-right: 0;
    }
    .profile-card__button.button--blue {
        background: linear-gradient(45deg, #1da1f2, #0e71c8);
        box-shadow: 0px 4px 30px rgba(19, 127, 212, 0.4);
    }
    .profile-card__button.button--blue:hover {
        box-shadow: 0px 7px 30px rgba(19, 127, 212, 0.75);
    }
    .profile-card__button.button--orange {
        background: linear-gradient(45deg, #d5135a, #f05924);
        box-shadow: 0px 4px 30px rgba(223, 45, 70, 0.35);
    }
    .profile-card__button.button--orange:hover {
        box-shadow: 0px 7px 30px rgba(223, 45, 70, 0.75);
    }
    .profile-card__button.button--gray {
        box-shadow: none;
        background: #dcdcdc;
        color: #142029;
    }
    .profile-card-message {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        padding-top: 130px;
        padding-bottom: 100px;
        opacity: 0;
        pointer-events: none;
        transition: all .3s;
    }
    .profile-card-form {
        box-shadow: 0 4px 30px rgba(15, 22, 56, 0.35);
        max-width: 80%;
        margin-left: auto;
        margin-right: auto;
        height: 100%;
        background: #fff;
        border-radius: 10px;
        padding: 35px;
        transform: scale(0.8);
        position: relative;
        z-index: 3;
        transition: all .3s;
    }
    @media screen and (max-width: 768px) {
        .profile-card-form {
            max-width: 90%;
            height: auto;
        }
    }
    @media screen and (max-width: 576px) {
        .profile-card-form {
            padding: 20px;
        }
    }
    .profile-card-form__bottom {
        justify-content: space-between;
        display: flex;
    }
    @media screen and (max-width: 576px) {
        .profile-card-form__bottom {
            flex-wrap: wrap;
        }
    }
    .profile-card textarea {
        width: 100%;
        resize: none;
        height: 210px;
        margin-bottom: 20px;
        border: 2px solid #dcdcdc;
        border-radius: 10px;
        padding: 15px 20px;
        color: #324e63;
        font-weight: 500;
        font-family: 'Quicksand', sans-serif;
        outline: none;
        transition: all .3s;
    }
    .profile-card textarea:focus {
        outline: none;
        border-color: #8a979e;
    }
    .profile-card__overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        pointer-events: none;
        opacity: 0;
        background: rgba(22, 33, 72, 0.35);
        border-radius: 12px;
        transition: all .3s;
    }

</style>
<?php
global $wpdb;
$cs_id = '';
$cs_created = '';
$ticketDetails = [];
$ticketComments = [];
if (isset($_GET['ticketid']) && $_GET['ticketid'] != '') {
    $current_user = wp_get_current_user();
    $userId = $current_user->data->ID;
    $userName = $current_user->data->user_login;
    $cs_id = filter_input(INPUT_GET, 'ticketid', FILTER_SANITIZE_NUMBER_INT);
    $ticketDetails = $wpdb->get_results("SELECT * FROM contact_support WHERE cs_id = '$cs_id'", ARRAY_A);
    if (!empty($ticketDetails)) {
        $ticketDetails = $ticketDetails[0];
        $cs_created = date("d M y", strtotime($ticketDetails['cs_created']));
        $cs_updated = date("d M y", strtotime($ticketDetails['cs_updated']));
        $cs_status = $ticketDetails['cs_status'];
        $ticketComments = $wpdb->get_results("SELECT * FROM contact_support_comment_rel WHERE cscr_contact_support_id = '$cs_id'", ARRAY_A);
    }
    $profilePicture = get_avatar($current_user->data->ID);
}
?>
<section>
    <h1><?php echo 'Ticket details'; ?></h1>
    <div class="container">
        <?php if ($cs_id != '') { ?>
            <div class="row">
                <div class="col-md-8">
                    <div class="wrapper message-card" style="min-height: auto;">
                        <h2><?php echo isset($ticketDetails['cs_type']) ? $ticketDetails['cs_type'] : ''; ?></h2>
                        <div class="messageA">
                            <div class="userName"><?php echo isset($ticketDetails['cs_user_name']) ? ucfirst($ticketDetails['cs_user_name']) : ''; ?></div><div class="messageDate"><?php echo $cs_created; ?></div>
                            <p><?php echo isset($ticketDetails['cs_description']) ? ucfirst($ticketDetails['cs_description']) : ''; ?></p>
                        </div>
                        <?php
                        if (!empty($ticketComments)) {
                            foreach ($ticketComments as $comment) { //cscr_user_name
                                $cscr_created = date("d M y", strtotime($comment['cscr_created']));
                                ?>
                                <hr>
                                <div class="adminmessage">
                                    <div class="adminName"><?php echo isset($comment['cscr_user_name']) ? ucfirst($comment['cscr_user_name']) : ''; ?></div><div class="messageDate"><?php echo $cscr_created; ?></div>
                                    <p><?php echo isset($comment['cscr_description']) ? ucfirst($comment['cscr_description']) : ''; ?></p>
                                    <?php if (isset($comment['cscr_attachment']) && $comment['cscr_attachment'] != '') { ?>
                                        <p><a target="_blank" href="<?php echo isset($comment['cscr_attachment']) ? ucfirst($comment['cscr_attachment']) : ''; ?>">See the attachment</a></p>
                                    <?php } ?>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <div style="height:20px"></div>

                    <div class="wrapper submit-card" <?php if ($cs_status == 1) { ?>style="    min-height: auto;" <?php } ?>>
                        <?php if ($cs_status == 0) { ?>
                            <form enctype="multipart/form-data" method="POST" action="https://www.travpart.com/English/submit-ticket/" id="commentForm">
                                <input type="hidden" name="action" value="ticket_comment">
                                <input type="hidden" name="cscr_contact_support_id" value="<?php echo $cs_id; ?>">
                                <input type="hidden" name="cscr_user_id" value="<?php echo $userId; ?>">
                                <input type="hidden" name="cscr_user_name" value="<?php echo $userName; ?>">
                                <textarea required="true" name="cscr_description"></textarea>
                                <div class="upload-btn-wrapper">
                                    <button class="btn">Upload a file</button>
                                    <input type="file" name="cscr_attachment" />
                                </div>
                                <br />
                                <button class="button">SUBMIT</button>
                                <!--<a href="#"><div class="button">SUBMIT</div></a>-->
                            </form>
                            <?php
                        } else {
                            ?>
                        <h4 style="color: red; border: 1px solid red; padding: 10px;border-radius: 5px;">You can't add the comment(s) because status of this ticket is <b>closed</b>!</h4>
                            <?php
                        }
                        ?>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="wrapper">
                        <div class="profile-card js-profile-card">
                            <div class="profile-card__img">
                                <!--<img src="https://res.cloudinary.com/muhammederdem/image/upload/v1537638518/Ba%C5%9Fl%C4%B1ks%C4%B1z-1.jpg" alt="profile card">-->
                                <?php echo $profilePicture; ?>
                            </div>

                            <div class="profile-card__cnt js-profile-cnt">
                                <div class="profile-card__name"><?php echo isset($current_user->data->user_login) ? ucfirst($current_user->data->user_login) : 'John Doe'; ?></div>
                                <div class="profile-card-loc">
                                </div>

                                <div class="profile-card-inf">
                                    <div class="profile-card-inf__item">
                                        <div class="profile-card-inf__title"><?php echo '000' . $cs_id; ?></div>
                                        <div class="profile-card-inf__txt">TICKET ID</div>
                                    </div>

                                    <div class="profile-card-inf__item">
                                        <div class="profile-card-inf__title"><?php echo $cs_created; ?></div>
                                        <div class="profile-card-inf__txt">CREATED</div>
                                    </div>

                                    <div class="profile-card-inf__item">
                                        <div class="profile-card-inf__title"><?php echo $cs_updated; ?></div>
                                        <div class="profile-card-inf__txt">LAST UPDATED</div>
                                    </div>
                                </div>

                                <div class="profile-card-ctr">
                                    <?php if ($cs_status == 0) { ?>
                                        <button onclick="closeTicket('<?php echo $cs_id; ?>');" class="profile-card__button button--orange">Close this ticket</button>
                                    <?php } else {
                                        ?>
                                        <h4 style="color: red; border: 1px solid red; padding: 10px;border-radius: 5px;">Ticket is Closed</h4>
                                    <?php }
                                    ?>
                                </div>
                            </div>

                            <div class="profile-card-message js-message">
                                <form class="profile-card-form">
                                    <div class="profile-card-form__container">
                                        <textarea placeholder="Say something..."></textarea>
                                    </div>

                                    <div class="profile-card-form__bottom">
                                        <button class="profile-card__button button--blue js-message-close">
                                            Send
                                        </button>

                                        <button class="profile-card__button button--gray js-message-close">
                                            Cancel
                                        </button>
                                    </div>
                                </form>

                                <div class="profile-card__overlay js-message-close"></div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        <?php } else {
            ?>
            <h3 style="color: red;text-align: center;">OPPS!Something want wrong.</h3>
        <?php }
        ?>
    </div>

    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <?php the_content(); ?>

                <br class="clear">

                <?php edit_post_link(); ?>

            </article>
            <!-- /article -->

        <?php endwhile; ?>

    <?php else: ?>

        <!-- article -->
        <article>
            <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>
        </article>
        <!-- /article -->

    <?php endif; ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js"></script>

    <script>

                                    function closeTicket(ticket_id) {
                                        swal({
                                            title: "Are you sure?",
                                            text: "You want to close this ticket!",
                                            type: "warning",
                                            showCancelButton: true,
                                            confirmButtonClass: "btn-danger",
                                            confirmButtonText: "Yes, Close It!",
                                            cancelButtonText: "No, Keep Open!",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        },
                                                function (isConfirm) {
                                                    if (isConfirm) {
                                                        var url = 'https://www.travpart.com/English/submit-ticket/';

                                                        $.ajax({
                                                            type: "POST",
                                                            url: url,
                                                            data: {action: 'ticket_clost', ticket_id: ticket_id}, // serializes the form's elements.
                                                            success: function (data)
                                                            {
                                                                var result = JSON.parse(data);
                                                                if (result.status) {
                                                                    swal("Closed!", "Your this ticket is closed, for reopen contact support again!.", "success");
                                                                    window.location.reload()
                                                                } else {
                                                                    alert(result.message);
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        swal.close();
                                                    }
                                                });
                                    }

                                    var messageBox = document.querySelector('.js-message');
                                    var btn = document.querySelector('.js-message-btn');
                                    var card = document.querySelector('.js-profile-card');
                                    var closeBtn = document.querySelectorAll('.js-message-close');

                                    btn.addEventListener('click', function (e) {
                                        e.preventDefault();
                                        card.classList.add('active');
                                    });

                                    closeBtn.forEach(function (element, index) {
                                        console.log(element);
                                        element.addEventListener('click', function (e) {
                                            e.preventDefault();
                                            card.classList.remove('active');
                                        });
                                    });
    </script>
</section>
<!-- /section -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>