<?php
/* Template Name: searchdesign */
	session_start();
	get_header();
	$msg = "";
?>

<style type="text/css">
.rowss{
	width: 100%;
	display: table;
}
.col-mmd-10{
	width:80%;
	font-size: 12px;
	padding: 0px 10px;
}
.col-mmmd-2{
	width:20%;
	text-align: right;
}

.col-mmd-10,.col-mmmd-2{
	display: inline-table;
	vertical-align: middle;
}
.col-mmd-10 p{
	margin: 0px;
	font-size: 15px !important;
    line-height: 16px;
    text-transform: capitalize;
}
.collapse_search{
	background: #078be2;
	border-radius: 20px;
	padding:5px 15px;
	margin-bottom: 10px;
	display: none;
}
.custom_anchor a{
	font-size: 15px;
	color: #fff;
}
.imgsize{
    width: 100%!important;
}

.container h1 {
    display: block;
    text-align: center;
    margin: 0;
    background-color: #226d82;
    padding: 0;
    color: #fff !important;
    font-family: "Roboto", Sans-serif;
    font-weight: 600;
    font-size: 25px;
    padding: 10px;
}
.mob_tour_search{
    display: none;
}
.searchagent input.form-control {
    height: auto;
    padding-top: 12px;
    padding-bottom: 12px;
    border: none;
    background-color: #e0f2fe;
    -webkit-box-shadow: none;
}
.searchagent .col-sm-2 {
    padding-left: 8px;
}
.searchagent .col-sm-10 {
    padding-right: 0;
}
.tourleft {
    width: 28%;
    float: left;
}
.tourleft img.lazyloaded {
    width: 100%;
}
.tourmiddle {
    width: 52%;
    float: left;
    margin-left: 15px;
    margin-right: 15px;
}
.tourright {
   width: 16%;
   float: right;
   position:relative;
   text-align:right;
}
.tourprice {
    position: absolute;
    top: 35px;
    right: 0;
}
p.fromcur {
    font-size: 12px;
    color: grey;
    padding-right: 10px;
    margin-bottom: 5px;
}
p.price_main {
    font-size: 20px;
    padding-right: 10px;
    color: #269ad8;
    margin-bottom: 5px;
}
.tourmiddle h3 {
    margin-top: 0;
}
.tourlistitem.clearfix {
    padding: 10px;
    border: 1px solid #ccc;
    box-sizing: border-box;
    box-shadow: 0 0 4px 0 #ccc;
    margin-bottom:24px;
    background-color: white;
}
#main-discover {
    color: #FFF!important;
}
button.w3-bar-item.w3-button.tablink{
	 font-family: monospace;
}
.tourfilter{
	background: #e0f2fe;
	padding: 10px;
	padding-bottom: 0px;
	color: #FFF!important;
}
#main-discover h4 {
    text-align: center;
    background: #5fa78d;
    padding: 7px;
    font-family: monospace;
    font-size: 15px;
    color: #fff;
}
.tourfilter{
	 background-color: #e0f2fe;
}
.col-md-6.lbl-col,.lbl-2{
	color:#000;
}
.lbl-1 .option-input,.lbl-2 .option-input{
    background: #fff;
    border: 1px solid #000;
}
#main-discover input[type="text"],#main-discover .form-control {
    display: block;
    width: 100%;
    height: 34px;
    border-radius: 0px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
   
    background-image: none;
    border: 1px solid #d9d9d9;
}
#main-discover input[type="submit"] {
    color: #fff!important;
    background-color: #22b14c !important;
    text-align: center;
    width: 50%;
    text-transform: uppercase;
    font-family: monospace;
    margin-top: 10px;
    margin-left: 25%;
    box-shadow: 0 0 0 5px hsl(0, 0%, 87%);
}
.searchwarwrap{
width:726px;
margin:13px auto;
float: right;
   
}
.agentlistright{
width:71.99%;
display:flex;
}
.agentlistleft{
width:25%;
display:flex;
}
.agentlistwrap{
position:relative;
display:flex;
width:100%;
 justify-content: space-around;
}
.vouchers-banner i{
	padding: 0px;
}
.vouchers-banner img.lazyloaded {
    width: 100%;
}
a.btn.custom_btn {
    background-color: #269ad8;
    color: white;
    font-size: 15px;
    font-weight: bold;
    font-family: none;
    border-radius: 0px;
    padding-left: 15px;
    padding-right: 15px;
}
@media only screen and (max-width: 1199px) {
.tourmiddle {
    width: 48%;
}
}
@media only screen and (max-width: 998px) {
.tourleft {
    width: 100%;
    float: none;
}
.tourmiddle {
    width: 100%;
    float: none;
    margin-left: 0;
    margin-right: 0;
    margin-top: 18px;
}
.tourright {
    width: 100%;
    float: none;
    text-align: left;
}
.tourprice {
    position: static;
}
.pricerange .col-md-6.lbl-col {
    padding-left: 0;
}
.col-md-6.lbl-col {
    padding-left: 0;
}
}
@media only screen and (max-width: 767px) {
.searchagent .col-sm-2 {
    padding-left: 0;
    margin-top: 8px;
}
.searchagent .col-sm-10 {
    padding-left: 0;
}
.searchwarwrap{
width:95%;
}
.agentlistleft {
    width: 300px;
    display: block;
    height: 300px;
    margin: 0 auto;
    overflow: scroll;
}
.agentlistright {
    display: block;
    width: 95%;
    margin: 0 auto;
}
.agentlistwrap{
display:block;
}
}
@media(max-width: 600px) {  
.agentlistleft {
    margin-top: 80px;
}
.collapse_search{
	display: block;
}
.sel_pos select {
    font-size: 10px !important;
}
.mobile_collapse{
	display:none;
}
.agentlistleft{
	height: auto;
	width: 100%;
}
.searchwarwrap{
float: none;
   
}
.mob_tour_search{
    display: block;
}
.desk_tour_search{
    display: none;
}
}
@media (min-width: 180px) and (max-width: 320px) {
    .searchwarwrap {
        width: 100%;
    }
    
}
@media (min-width: 321px) and (max-width: 360px){
    .searchwarwrap {
        width: 100%;
    }
}
@media (min-width: 361px) and (max-width: 375px){
    .searchwarwrap {
        width: 96%;
    }
}
@media (min-width: 376px) and (max-width: 411px){
    .searchwarwrap {
        width: 88%;
    }
}
@media (min-width: 412px) and (max-width: 414px){
    .searchwarwrap {
        width: 87%;
    }
}

.ask_quote{
    background: #17bf2c;
    text-align: center;
    border-radius: 5px;
    width: 50%;
    margin: 0 auto;
    margin-top: 15%;
    cursor: pointer;
    padding: 10px;
}

.ask_quote h2{
    display: inline-block;
    margin-top: 15px;
    margin-bottom: 15px;
    color: #fff!important;
}

.ask_quote i{
    color: #fff;
    font-size: 26px;
    margin-right: 10px;
}

.ask-text p{
    margin-top: 30px;
    font-size: 23px;
    text-align: center;
}
.sel_pos select {
    height: 30px;
    margin-top: -34px;
    margin-right: 5px;
    font-size: 15px;
    color: grey;
}
.sel_pos p {
    margin-top: -27px;
    margin-right: 10px;
    font-size: 12px;
}
.sel_pos {
    display: flex;
    float: right;
}

</style>
<div class="searchwarwrap clearfix desk_tour_search">
<div class="searchagent">
    <form action="" method="get">
<div class="col-sm-12"><input class="form-control" type="text" name="search" placeholder=""></div>
<div class="col-sm-12 sel_tour_srch">
    <div class="sel_pos">
        <p>Sort by:</p>
        <select name="" form="">
          <option value="--Select--">--Select--</option>
          <option value="Popularity: high to low">Popularity: high to low</option>
          <option value="Price: low to high">Price: low to high</option>
          <option value="Price: high to low">Price: high to low</option>
          <option value="Duration: short to long">Duration: short to long</option>
          <option value="Duration: long to short">Duration: long to short</option>
        </select>
    </div>
</div>
<!--<div class="col-sm-2"><input class="btn btn-submit-blue" name="submit" type="submit" value="Search"></div>-->
</form></div>
</div>
<div class="agentlistwrap">
<div class="agentlistleft">
<div id="main-discover"><style>
    @keyframes click-wave {</p>
<p>0% {<br />            height: 20px;<br />            width: 20px;<br />            opacity: 0.35;<br />            position: relative;<br />        }</p>
<p>100% {<br />            height: 200px;<br />            width: 200px;<br />            margin-left: -80px;<br />            margin-top: -80px;<br />            opacity: 0;<br />        }</p>
<p>    }</p>
<p>.option-input {<br />        -webkit-appearance: none;<br />        -moz-appearance: none;<br />        -ms-appearance: none;<br />        -o-appearance: none;<br />        appearance: none;<br />        position: relative;<br />        top: 5px;<br />        right: 0;<br />        bottom: 0;<br />        left: 0;<br />        height: 20px;<br />        width: 20px;<br />        transition: all 0.15s ease-out 0s;<br />        background: #cbd1d8;<br />        border: none;<br />        color: #fff;<br />        cursor: pointer;<br />        display: inline-block;<br />        margin-right: 0.5rem;<br />        outline: none;<br />        position: relative;<br />        z-index: 1000;<br />    }</p>
<p>    .option-input:hover {<br />        background: #9faab7;<br />    }</p>
<p>    .option-input:checked {<br />        background: #40e0d0;<br />    }</p>
<p>    .option-input:checked::before {<br />        height: 20px;<br />        width: 20px;<br />        position: absolute;<br />        content: '&#x2714;';<br />        display: inline-block;<br />        font-size: 26.66667px;<br />        text-align: center;<br />        line-height: 20px;<br />    }</p>
<p>    .option-input:checked::after {<br />        -webkit-animation: click-wave 0.65s;<br />        -moz-animation: click-wave 0.65s;<br />        animation: click-wave 0.65s;<br />        background: #40e0d0;<br />        content: '';<br />        display: block;<br />        position: relative;<br />        z-index: 100;<br />    }</p>
<p>    .option-input.radio {<br />        border-radius: 50%;<br />    }</p>
<p>    .option-input.radio::after {<br />        border-radius: 50%;<br />    }</p>
</style>
<div class="tourfilter">

<?php if(!empty($_REQUEST['search'])){ ?>
<div class="collapse_search" >
	<div class="rowss">
		<div class="col-mmd-10">
			<p><?php echo $_REQUEST['search']; ?></p>
		    <?if (!empty($_REQUEST['days'])){ ?>	
		 	   <p>Days : <?php echo $_REQUEST['days']; ?> </p> 
		    <?php } ?>
		</div>
		<div class="col-mmmd-2 custom_anchor">
			<a href="#" class="click_chevron">
				<i class="fas fa-search"></i>
			</a>
		</div>
	</div>
</div>
<?php } ?>
<div class="mobile_collapse">



<?php //if($_GET['debug']==1){ ?>

<div class="w3-container">
  
  <div class="w3-bar w3-black space_bottom">
    <button class="w3-bar-item w3-button tablink w3-red" onclick="openCity(event,'London')">Tour <br> Package</button>
    <button class="w3-bar-item w3-button tablink" onclick="openCity(event,'Paris')">Hotel</button>
    <button class="w3-bar-item w3-button tablink" onclick="openCity(event,'Tokyo')">Vehicle
    </button>
  </div>
  
  <div id="London" class="w3-container w3-border city">
    <div dir="ltr" lang="en-US">
    <form class="wpcf7-form" action="https://www.travpart.com/English/toursearch" method="GET" target="_blank"><label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="search" size="40" type="text" value="<?php if (!empty($_REQUEST['search'])) echo $_REQUEST['search']; ?>" placeholder="Search Destination" />
</span>
</label><label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="agent" size="40" type="text" value="<?php if (!empty($_REQUEST['agent'])) echo $_REQUEST['agent']; ?>" placeholder="Agent's name" />
</span>
</label><label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="days" size="40" type="text" value="<?php if (!empty($_REQUEST['days'])) echo $_REQUEST['days']; ?>" placeholder="Days" />
</span>
</label><label style="width: 100%;">
<span class="wpcf7-form-control-wrap">
<!--input class="wpcf7-form-control wpcf7-text form-control date_color" id="date_range" name="date" type="date" value="" placeholder="Choose your date" /-->
</span>
</label>
<div>
<h4>Price Range</h4>
<div class="col-md-6 lbl-col"><label class="lbl-1">
<input class="option-input radio" name="range" type="radio" value="0-500" 
    <?php if ($_REQUEST['range'] === '0-500') echo ' checked="checked"'; ?>> 
$0 - $500
</label></div>
<label class="lbl-2">
<input class="option-input radio" name="range" type="radio" value="500-1000"
    <?php if ($_REQUEST['range'] === '500-1000') echo ' checked="checked"'; ?>> 
$500 - $1000
</label>

</div>
<div class="col-md-6 lbl-col"><label class="lbl-1">
<input class="option-input radio" name="range" type="radio" value="1000-5000"
    <?php if ($_REQUEST['range'] === '1000-5000') echo ' checked="checked"'; ?>> 
$1k- $5k
</label></div>
<label class="lbl-2">
<input class="option-input radio" name="range" type="radio" value="5000-0"
    <?php if ($_REQUEST['range'] === '5000-0') echo ' checked="checked"'; ?>> 
$5k +
</label>
<input class="wpcf7-form-control wpcf7-submit form-control" type="submit" value="Search" />

</form></div>
  </div>

  <div id="Paris" class="w3-container w3-border city" style="display:none">
   <div dir="ltr" lang="en-US">
   <form class="wpcf7-form" action="" method="GET" target="_blank">
       <label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="search_hotel_name" size="40" type="text" value="<?php if (!empty($_REQUEST['search_hotel_name'])) echo $_REQUEST['search_hotel_name']; ?>" placeholder="Search" />
</span>
</label>
<label style="width: 100%;">
	<span class="wpcf7-form-control-wrap">
	<!--input class="wpcf7-form-control wpcf7-text form-control date_color" id="date_range_hotel" name="date" type="date" value="" placeholder="Choose your date" /-->
	<input class="wpcf7-form-control wpcf7-text form-control date_color" type="text" name="daterange"  value="<?php if (!empty($_REQUEST['daterange'])) echo $_REQUEST['daterange']; ?>" />
	</span>
</label>
<div>
<h4>Price Range</h4>
<div class="col-md-6 lbl-col"><label class="lbl-1">
<input class="option-input radio" name="range" type="radio" value="0-500" 
    <?php if ($_REQUEST['range'] === '0-500') echo ' checked="checked"'; ?>> 
$0 - $500
</label></div>
<label class="lbl-2">
<input class="option-input radio" name="range" type="radio" value="500-1000"
    <?php if ($_REQUEST['range'] === '500-1000') echo ' checked="checked"'; ?>> 
$500 - $1000
</label>

</div>
<div class="col-md-6 lbl-col"><label class="lbl-1">
<input class="option-input radio" name="range" type="radio" value="1000-5000"
    <?php if ($_REQUEST['range'] === '1000-5000') echo ' checked="checked"'; ?>> 
$1k- $5k
</label></div>
<label class="lbl-2">
<input class="option-input radio" name="range" type="radio" value="5000-0"
    <?php if ($_REQUEST['range'] === '5000-0') echo ' checked="checked"'; ?>> 
$5k +
</label>
<input class="wpcf7-form-control wpcf7-submit form-control" type="submit" value="Search" />

</form></div>
  </div>

  <div id="Tokyo" class="w3-container w3-border city" style="display:none">
    <div dir="ltr" lang="en-US">
    <form class="wpcf7-form" action="" method="GET" target="_blank"><label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="search_vehicle_name" size="40" type="text" value="<?php if (!empty($_REQUEST['search_vehicle_name'])) echo $_REQUEST['search_vehicle_name']; ?>" placeholder="Search" />
</span>
</label><label style="width: 100%;">
<span class="wpcf7-form-control-wrap">
<!--input class="wpcf7-form-control wpcf7-text form-control date_color" id="date_range_vehicle" name="date" type="date" value="" placeholder="Choose your date" /-->
<input class="wpcf7-form-control wpcf7-text form-control date_color" type="text" name="date_range_vehicle" value="<?php if (!empty($_REQUEST['date_range_vehicle'])) echo $_REQUEST['date_range_vehicle']; ?>" /> 
</span>
</label>
<div>
<h4>Price Range</h4>
<div class="col-md-6 lbl-col"><label class="lbl-1">
<input class="option-input radio" name="range" type="radio" value="0-500" 
    <?php if ($_REQUEST['range'] === '0-500') echo ' checked="checked"'; ?>> 
$0 - $500
</label></div>
<label class="lbl-2">
<input class="option-input radio" name="range" type="radio" value="500-1000"
    <?php if ($_REQUEST['range'] === '500-1000') echo ' checked="checked"'; ?>> 
$500 - $1000
</label>

</div>
<div class="col-md-6 lbl-col"><label class="lbl-1">
<input class="option-input radio" name="range" type="radio" value="1000-5000"
    <?php if ($_REQUEST['range'] === '1000-5000') echo ' checked="checked"'; ?>> 
$1k- $5k
</label></div>
<label class="lbl-2">
<input class="option-input radio" name="range" type="radio" value="5000-0"
    <?php if ($_REQUEST['range'] === '5000-0') echo ' checked="checked"'; ?>> 
$5k +
</label>
<input class="wpcf7-form-control wpcf7-submit form-control" type="submit" value="Search" />

</form></div>
  </div>
</div>

<style>
.w3-red{
    background-color: #fff !important;
    vertical-align: middle;
    color: #22b14c !important;
}
.space_bottom{margin-bottom:10px;}
@keyframes click-wave {
0% {height: 20px;width: 20px;opacity: 0.35;position: relative;}
100% {height: 200px;width: 200px;margin-left: -80px;margin-top: -80px;opacity: 0;}
}
.option-input {-webkit-appearance: none;-moz-appearance: none;-ms-appearance: none;-o-appearance: none;appearance: none;position: relative;top: 5px;right: 0;bottom: 0;left: 0;height: 20px;width: 20px;transition: all 0.15s ease-out 0s;background: #cbd1d8;border: none;color: #fff;cursor: pointer;display: inline-block;margin-right: 0.5rem;outline: none;position: relative;}
.option-input:hover {background: #9faab7;}
.option-input:checked {background: #40e0d0;}
.option-input:checked::before {height: 20px;width: 20px;position: absolute;content: '✔';display: inline-block;font-size: 26.66667px;text-align: center;line-height: 20px;}
.option-input:checked::after {-webkit-animation: click-wave 0.65s;-moz-animation: click-wave 0.65s;animation: click-wave 0.65s;background: #40e0d0;content: '';display: block;position: relative;z-index: 100;}
.option-input.radio {border-radius: 50%;}
.option-input.radio::after {border-radius: 50%;}
button.w3-bar-item.w3-button.tablink {
       width: 32.32%;
    font-size: 10px;
     vertical-align: middle;
    background-color: #22b14c;
    padding: 0px;
    border:0px;
    min-height: 40px;
    color: #fff;
    font-weight: bold;
}
form.wpcf7-form{
    width: 100%;
}
.calendar-table .table-condensed th, td{
    padding: 0px !important;
}
 .table-condensed thead th,  .table-condensed thead td{
     background: none !important;
 }
 
</style>
<!--h4 class="discover_place">Discover the perfect journey</h4-->

<div class="vouchers-banner" style="text-align: center; padding: 15px; margin-top: 20px;position: relative;">
<a href="https://www.travpart.com/English/user/?gametab" target="_blank" rel="noopener noreferrer">
<img src="https://www.travpart.com/English/wp-content/themes/bali/images/voucher-banner-en.png">	
</a>
<i class="fa fa-info" style="cursor: pointer;margin-left: 5px;color: white;position: absolute;right: 41px;bottom: 18px;border: 1px solid #fff;padding: 1px 4px;border-radius: 100%;background: red;font-size: 8px;"></i>
</div>
<script>
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}

</script>


<?php //} ?>

<!--<div dir="ltr" lang="en-US" class="cl_searchform">
<h4>Discover the perfect journey</h4>
<form class="wpcf7-form" action="" method="get"><label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="search" size="40" type="text" value="<?php if (!empty($_REQUEST['search'])) echo $_REQUEST['search']; ?>" placeholder="Search">
</span>
</label><label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="agent_name" size="40" type="text" value="<?php if (!empty($_REQUEST['agent_name'])) echo $_REQUEST['agent_name']; ?>" placeholder="Agent's name">
</span>
</label><label>
<span class="wpcf7-form-control-wrap">
<input class="wpcf7-form-control wpcf7-text form-control" name="days" size="40" type="text" value="<?php if (!empty($_REQUEST['days'])) echo $_REQUEST['days']; ?>" placeholder="Days">
</span>
</label>
<div class="pricerange">
<h4>Price Range</h4>
<div class="col-md-6 lbl-col"><label class="lbl-1">
<input class="option-input radio" name="range" type="radio" value="0-500" 
    <?php if ($_REQUEST['range'] === '0-500') echo ' checked="checked"'; ?>> 
$0 - $500
</label></div>
<label class="lbl-2">
<input class="option-input radio" name="range" type="radio" value="500-1000"
    <?php if ($_REQUEST['range'] === '500-1000') echo ' checked="checked"'; ?>> 
$500 - $1000
</label>

</div>
<div class="col-md-6 lbl-col"><label class="lbl-1">
<input class="option-input radio" name="range" type="radio" value="1000-5000"
    <?php if ($_REQUEST['range'] === '1000-5000') echo ' checked="checked"'; ?>> 
$1k- $5k
</label></div>
<label class="lbl-2">
<input class="option-input radio" name="range" type="radio" value="5000-0"
    <?php if ($_REQUEST['range'] === '5000-0') echo ' checked="checked"'; ?>> 
$5k +
</label>
<input class="wpcf7-form-control wpcf7-submit form-control" type="submit" value="Search">

</form>

</div>
<div class="vouchers-banner" style="text-align: right; padding: 15px; max-width: 200px; margin-top: 20px;"><a href="https://www.travpart.com/English/user/?gametab" target="_blank" rel="noopener noreferrer"><img style="margin-bottom: 20px;" src="https://www.travpart.com/English/wp-content/themes/bali/images/voucher-banner-en.png"></a>
<i class="fa fa-info-circle" style="font-size: 24px; margin-left: 5px; color: #61cbc9;">
	
</i>
</div>-->
</div>

</div>
</div>
</div>
<div class="searchwarwrap clearfix mob_tour_search">
<div class="searchagent">
    <form action="" method="get">
<div class="col-sm-12"><input class="form-control" type="text" name="search" placeholder=""></div>
<div class="col-sm-12 sel_tour_srch">
    <div class="sel_pos">
        <p>Sort by:</p>
        <select name="" form="">
          <option value="--Select--">--Select--</option>
          <option value="Popularity: high to low">Popularity: high to low</option>
          <option value="Price: low to high">Price: low to high</option>
          <option value="Price: high to low">Price: high to low</option>
          <option value="Duration: short to long">Duration: short to long</option>
          <option value="Duration: long to short">Duration: long to short</option>
        </select>
    </div>
</div>
<!--<div class="col-sm-2"><input class="btn btn-submit-blue" name="submit" type="submit" value="Search"></div>-->
</form></div>
</div>
<!-- end of left -->
<?php

// Search Function for new design

global $wpdb;

// Getting Currency

$currency_usd = $wpdb->get_var("SELECT option_value FROM tourfrom_balieng2.wp_options WHERE option_name='_cs_currency_USD'");

// Site Url
$site_url = $wpdb->get_var("SELECT option_value FROM tourfrom_balieng2.wp_options WHERE option_name='siteurl'");

// Getting Search Filters
$search_destination = filter_input(INPUT_GET, 'des_query', FILTER_SANITIZE_STRING);
$search_term = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
$search_agent = filter_input(INPUT_GET, 'agent_name', FILTER_SANITIZE_STRING);
$search_days = filter_input(INPUT_GET, 'days', FILTER_SANITIZE_STRING);
$search_date = filter_input(INPUT_GET, 'date', FILTER_SANITIZE_STRING);
$search_price_range = filter_input(INPUT_GET, 'range', FILTER_SANITIZE_STRING);

if (!empty($search_date)){

    search_kw_storing($search_date);
}
if (!empty($search_days)){

    search_kw_storing($search_days);
}

if (!empty($search_agent)){

    search_kw_storing($search_agent);
}
// Implement  the price Range Function
 if (!empty($search_price_range)) {

    search_kw_storing($search_price_range);
    $search_price_range = explode('-', $search_price_range);
    $lower_limit = $search_price_range[0];

    $lower_limit = ceil($lower_limit / $currency_usd);

    $upper_limit = $search_price_range[1];
    $upper_limit = ceil($upper_limit / $currency_usd);
    }

// Implement the search Query from posts
if (!empty($search_term)) {

    search_kw_storing($search_term);
    $array_of_ids_in_search = array();

    //  selects all   starting with "term":
    $get_search = $wpdb->get_results("SELECT ID FROM tourfrom_balieng2.wp_posts WHERE  post_title LIKE '{$search_term}%'   AND post_status ='publish'");

    $row_count = $wpdb->num_rows;

    if ($row_count == 0 || $row_count == 1){

        $get_search = $wpdb->get_results("SELECT ID FROM tourfrom_balieng2.wp_posts WHERE  post_title LIKE '%{$search_term}%' AND post_status ='publish' ");
        $row_count = $wpdb->num_rows;
    }

    if ($row_count == 0 || $row_count == 1){

        $get_search = $wpdb->get_results("SELECT ID FROM tourfrom_balieng2.wp_posts WHERE  post_content LIKE '%{$search_term}%' OR post_title LIKE '%{$search_term}%' AND  post_status ='publish' ");
        $row_count = $wpdb->num_rows;
    }

    foreach ($get_search as $value){
        // Store all posts ids
        $array_of_ids_in_search[] = $value->ID;

    }
}

// Search Main query
$sql = "SELECT wp_tour.*,wp_tour_sale.number_of_people,wp_tour_sale.username,wp_tourmeta.meta_value
    FROM tourfrom_balieng2.wp_tour,tourfrom_balieng2.wp_tour_sale,tourfrom_balieng2.wp_tourmeta
    WHERE
    wp_tour.id=wp_tour_sale.tour_id
    AND wp_tourmeta.tour_id=wp_tour_sale.tour_id
    AND wp_tourmeta.meta_key='bookingdata'
    AND username!='agent2' AND username!='Lix'";

// adding price range
if (!empty($search_price_range)){

    $sql .= " AND total>={$lower_limit}";
    if ($upper_limit != 0) $sql .= " AND total<={$upper_limit}";
}
// Order by tour score
$sql .= " ORDER BY wp_tour.score DESC";

if (empty($search_price_range) and empty($search_destination) and empty($search_agent) and empty($search_days) and empty($search_date) and empty($search_term)) {

    $sql .= " LIMIT 6";

}
else {

    $sql .= " LIMIT 1000";
}
$data = $wpdb->get_results($sql);
//var_dump($data);
?>

<div class="agentlistright">
<div class="tourlist">
<?php
$empty = 0;
// Loop through the data
foreach ($data as $t) {
    // Getting Post Status
    $post_status = $wpdb->get_row("SELECT post_status FROM tourfrom_balieng2.wp_posts,tourfrom_balieng2.wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';")->post_status;

    if ($post_status != 'publish') continue;

    // Get the Posts data,like title and ID for further use if post is published
    $postdata = $wpdb->get_row("SELECT  ID,post_title FROM tourfrom_balieng2.wp_posts,tourfrom_balieng2.wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';");

    $post_ID = $postdata->ID;

    // Filter the search Term
    if (!empty($search_term)) {
        if (!in_array($post_ID, $array_of_ids_in_search)) continue;
    }
    // Filter the search destination keyword
    if (!empty($search_destination)) $hotelinfo = $wpdb->get_row("SELECT img,ratingUrl,location,start_date FROM tourfrom_balieng2.wp_hotel WHERE tour_id='{$t->id}' AND location LIKE '%{$search_destination}%' LIMIT 1");
    else $hotelinfo = $wpdb->get_row("SELECT img,ratingUrl,location,start_date FROM tourfrom_balieng2.wp_hotel WHERE tour_id='{$t->id}' LIMIT 5");

    if (!empty($search_destination) && empty($hotelinfo)) continue;

    if (!empty($search_destination)) search_kw_storing($search_destination);

    //initialize the images array
    $images = array();

    // Get the Images from the post content
    $post_content = $wpdb->get_row("SELECT post_content FROM tourfrom_balieng2.wp_postmeta, tourfrom_balieng2.wp_posts WHERE wp_posts.ID=wp_postmeta.post_id AND `meta_key`='tour_id' AND `meta_value`='{$t->id}'")->post_content;
    if (preg_match('!<img.*?src="(.*?)"!', $post_content, $post_image_match)) $images[] = $post_image_match[1];

    // Filter for Agent Name
    $agentrow = $wpdb->get_row("SELECT userid,username,uname FROM tourfrom_balieng2.user WHERE username='{$t->username}'");

    $agent_id = $agentrow->userid;

    //$agent_name = $agentrow->uname;
    $agent_name = $agentrow->username;

    if (empty($agent_name))

    $agent_name = $t->username;

    if (!empty($search_agent))
    {
        if (stripos($agent_name, $search_agent) === false) continue;

    }

    $bookingdata = unserialize($t->meta_value);

    if (!empty($hotelinfo->img))
    {

        if (is_array(unserialize($hotelinfo->img)))
        {
            foreach (unserialize($hotelinfo->img) as $hotelImgItem) $images[] = $hotelImgItem;
        }
        else $images[] = $hotelinfo->img;
    }

    /// Filter for Days
    $days_array = array(
        $bookingdata['tourlist_date'],
        $bookingdata['place_date'],
        $bookingdata['eaidate'],
        $bookingdata['boat_date'],
        $bookingdata['spa_date'],
        $bookingdata['meal_date'],
        $bookingdata['car_date'],
        $bookingdata['popupguide_date'],
        $bookingdata['spotdate'],
        $bookingdata['restaurant_date']
    );
    $manualitems = $wpdb->get_results("SELECT meta_value FROM tourfrom_balieng2.`wp_tourmeta` where meta_key='manualitem' and tour_id=" . $t->id);
    foreach ($manualitems as $manualItem)
    {
        $days_array[] = unserialize($manualItem->meta_value) ['date'] . ',';
    }
    $days = max(array_map(function ($value)
    {
        return max(explode(',', $value));
    }
    , $days_array));

    if (intval($days) <= 0) $days = 1;
    if (!empty($search_days) && $days != $search_days) continue;

    // fixed issue of range with search term
    

    $search_date = date('m-d-Y', strtotime($search_date));

    if (strtotime($hotelinfo->start_date) < strtotime($search_date)) continue;

    if (!empty($search_price_range))
    {

        $tour_price = $t->total * $currency_usd;
        $l_limit = $search_price_range[0];
        $u_limit = $search_price_range[1];
        if ($u_limit == 0) $u_limit = 999999;

        if ($tour_price > $l_limit and $tour_price < $u_limit){
        }
        else
        {
            continue;
        }

    }
    $empty++;
//}
?>

<div class="tourlistitem clearfix">
<div class="tourleft"><a href="<?php echo $site_url . '/?p=' . $post_ID ?>">
 <?php
    if (empty($images[0])) $images[0] = "https://i.travelapi.com/hotels/1000000/910000/903000/902911/26251f24_y.jpg";
?>
<img class="imgsize" src="<?php echo $images[0]; ?>" alt="travpart.com">

</a></div>
<div class="tourmiddle">
<h3><?php echo $postdata->post_title; ?></h3>
<p class="tourcity"><b>Location: </b><?php echo (empty($hotelinfo->location) ? 'Null' : $hotelinfo->location); ?></p>
<p class="tourid"><b style="color: #000;">Booking Code: </b>BALI<?php echo $t->id; ?></p>
<p class="tdescription"><?php echo wp_trim_words($post_content, 15, '...'); ?><a href="<?php echo $site_url . '/?p=' . $post_ID ?>">See all</a></p>

</div>
<div class="tourright">
<div class="tourprice">
<p class="fromcur">From USD</p>
<p class="price_main"><?php echo '$' . number_format(ceil((float)$t->total * $currency_usd)); ?></p>
<a class="btn custom_btn" href="<?php echo $site_url . '/?p=' . $post_ID ?>">See Details</a>

</div>
</div>
</div>
<?php
}
if($empty==0) {

?>

<div class="ask_quote">
    <a href="https://www.travpart.com/English/contact/"><i class="far fa-question-circle"></i>
    <h2>Ask for a Quote</h2>
</a>
    
</div>

<div class="ask-text">
    <p>We currently don't have such a tour published in our system. However, you could ask one of our travel advisor to create one for you.</p>
</div>
<?php 

}
?>
</div>
</div>
</div>

<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	jQuery(document).ready(function($){
		$('.click_chevron').click(function(){
			$(".click_chevron i").toggleClass("fa-chevron-up fa-chevron-down");
			$('.mobile_collapse').slideToggle();		
		})
	})


jQuery(document).ready(function($){
	
	  $('input[name="daterange"]').daterangepicker({
	    opens: 'right',
	    startDate: moment().startOf('hour'),
		endDate: moment().startOf('hour').add(32, 'hour'),
	  }, function(start, end, label) {
	    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
	  });
	  
	  $('input[name="date_range_vehicle"]').daterangepicker({
	    opens: 'right',
	    startDate: moment().startOf('hour'),
		endDate: moment().startOf('hour').add(32, 'hour'),
	  }, function(start, end, label) {
	    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
	  });
});
</script>
	
<?php get_footer();