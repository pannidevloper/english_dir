<?php
/* Template Name: Most Viewed */

get_header();
if(have_posts()) {
	while(have_posts()) {
		the_post(); ?>
        <div style="width: 30%; float:left" class="left-sidebarforum">
            <?php if ( is_active_sidebar( 'forum-side-bar' ) ) : ?>
                <?php dynamic_sidebar( 'forum-side-bar' ); ?>
            <?php endif; ?>
        </div>
        </div>
        <div style="width: 70%; float:left" class="travel-hub right-forum">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="/forums/forum/travel-companions/destinations/">Most Recent</a></li>
                <li role="presentation" class="active"><a href="#">Most Viewed</a></li>
                <li role="presentation"><a href="/travel-hub">Travel Hub</a></li>
            </ul>
            <hr>
            <div id="most-viewed">
                <?php the_content(); ?>
            </div>
        </div>

        <?php
	}
} else {
	get_template_part('tpl-pg404', 'pg404');
}
get_footer();
?>