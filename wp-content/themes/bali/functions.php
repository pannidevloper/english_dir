<?php

// Add new function By Nazmul (Reducing uneceessy js and css code)
function bail_timeline_page(){
    if(is_page()){
        $bail_template_name = basename(get_page_template());
        if($bail_template_name == "timeline.php" || "tpl-mytimeline.php"){
            wp_enqueue_style('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
  
            wp_enqueue_style('timeline-style', get_template_directory_uri() .'/css/timeline-style.css?v='.time(), null, time());

        }else{

        }
    }
}
add_action('wp_enqueue_scripts', 'bail_timeline_page');
// End Add new function By Nazmul


// added push up notifaction new method by farhan
function notification_for_user($id,$msgg,$identifier){

// API access key from Google API's Console
define( 'API_ACCESS_KEY', 'AIzaSyCUhKe-YxeR_gBYlGnhiuxzphZPYMUPXos' );

$registrationIds = array ( $id );
/*
// prep the bundle
$msg = array
(
'message'   => $msgg,
'title'     => 'TravPart notification',
'subtitle'  => 'This is a subtitle. subtitle',
// 'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
'vibrate'   => 1,
'sound'     => 1,
'largeIcon' => 'http://icons.iconarchive.com/icons/marcus-roberto/google-play/512/Google-plus-icon.png',
'smallIcon' => 'http://icons.iconarchive.com/icons/marcus-roberto/google-play/512/Google-plus-icon.png',
'url'=>'google.com',
'image:'=> 'http://icons.iconarchive.com/icons/marcus-roberto/google-play/512/Google-plus-icon.png',
'identifier'=>$identifier
);
*/
/*
$fields = array
(
'registration_ids'  => $registrationIds,
'data'          => $msg
);
*/
$notification = [
        'title' =>'TravPart Notification',
        'body' => $msgg,
        'icon' =>'http://icons.iconarchive.com/icons/marcus-roberto/google-play/512/Google-plus-icon.png', 
        'image:'=> 'http://icons.iconarchive.com/icons/marcus-roberto/google-play/512/Google-plus-icon.png',
        'sound' => 'mySound',
        'identifier'=>$identifier
    ];
    $extraNotificationData = ["message" => $notification,  "image:"=> "http://icons.iconarchive.com/icons/marcus-roberto/google-play/512/Google-plus-icon.png",];

    $fcmNotification = [
        //'registration_ids' => $tokenList, //multple token array
        'to'        => $id, //single token
        'notification' => $notification,
        'data' => $extraNotificationData
    ];


$headers = array
(
'Authorization: key=' . API_ACCESS_KEY,
'Content-Type: application/json'
);

$ch = curl_init();

curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmNotification ) );
$result = curl_exec($ch );
curl_close( $ch );

//echo $result;

}           

error_reporting(0);
require_once dirname(__FILE__).'/geoip/geoip2.phar';
include(dirname(__FILE__) . '/image-resize.php');
remove_action('wp_head', 'wp_generator');
add_filter('widget_text', 'do_shortcode');
add_filter('widget_text', 'do_shortcode');
if (!defined('WP_POST_REVISIONS')) {
    define('WP_POST_REVISIONS', 3);
}
add_filter('wp_mail_content_type', create_function('', 'return "text/html";'));
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
}

function cArrVal($strValue) {
    if ($strValue == '') {
        return '0=';
    } else {
        return $strValue . '=';
    }
}

function clsImg($s) {
    $f = substr($s, 0, strrpos($s, '/'));
    $t = str_replace($f, '', $s);
    if (strrpos($t, '-') !== false) {
        $e = substr($t, strrpos($t, '.', -4), strlen($t));
        if (strrpos($t, 'x') !== false) {
            $n = substr($t, 0, strrpos($t, '-'));
            return $f . $n . $e;
        } else {
            return $s;
        }
    } else {
        return $s;
    }
}

function trimContent($sC, $iMax = 250, $sEnd = '...') {
    global $post;
    $sC = strip_tags(strip_shortcodes($sC));
    if (strlen($sC) > $iMax) {
        $sC = substr($sC, 0, $iMax);
        $iBr = strrpos($sC, ' ');
        if ($iBr) {
            $sC = substr($sC, 0, strrpos($sC, ' '));
        } return $sC . ' <a href="' . get_permalink($post->ID) . '">' . $sEnd . '</a>';
    } else {
        return $sC;
    }
}

function getBcrumbs() {
    echo '<div id="bcrumb" class="hidden-xs">';
    global $post;
    $strSep = '&nbsp;>>&nbsp;';
    $sHome = 'Home';
    if (is_page('user')) {
        $sHome = 'English/Travcust';
    }
    if (is_page()) {
        echo '<a href="' . get_bloginfo('url') . '/">' . $sHome . '</a>';
        $trail = array($post);
        $parent = $post;
        while ($parent->post_parent) {
            $parent = get_post($parent->post_parent);
            array_unshift($trail, $parent);
        }
        foreach ($trail as $page) {
            if ($page->ID == $post->ID) {
                echo $strSep . $page->post_title;
            } else {
                echo $strSep . '<a href="' . get_page_link($page->ID) . '">' . $page->post_title . '</a>';
            }
        }
    } else {
        echo '<a href="' . get_option('home') . '">' . $sHome . '</a>';
        if (is_category() || is_single()) {
            $category = get_the_category($post->ID);
            if ($category[0]) {
                echo $strSep . '<a href="' . str_replace("category/", "", get_category_link($category[0]->term_id)) . '">' . $category[0]->cat_name . '</a>';
            }
            if (is_single()) {
                echo $strSep;
                the_title();
            }
        }
    }
    echo '</div>';
}

add_action('admin_print_scripts', 'admScripts');

function admScripts() {
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_script('jquery');
}

add_action('admin_print_styles', 'admStyle');

function admStyle() {
    wp_enqueue_style('thickbox');
}

add_filter('admin_head', 'admJs');

function admJs() {
    echo '<script language="javascript">
    jQuery.noConflict();
    jQuery(document).ready(function() {
        var curClicked = "";
        var frm; 
        jQuery(".selimg").live("click", function() { 
            frm = jQuery(this).attr("title");
            curClicked = frm;
            tb_show("Select Image", "media-upload.php?type=image&tab=library&TB_iframe=true");
            return false;
        });
        window.original_send_to_editor = window.send_to_editor; 
        window.send_to_editor = function(html) {
            if(frm) {
                iurl = jQuery("img",html).attr("src");
                jQuery("#fld"+curClicked).val(iurl);
                jQuery("#img"+curClicked).attr("src",iurl);
                tb_remove();
            } else {
                window.original_send_to_editor(html);
            }
        }
        jQuery(".deleteimg").live("click", function(){ jQuery(this).parent("li").remove(); return false; });
    }); 
    function addImgs( type ) {
        intcount = jQuery("#"+type+" .gally li").length + 1;
        str = "<li><strong>Image "+intcount+":</strong><br /><img src=\"\" width=\"150px\" height=\"100\" id=\"img"+type+"-"+intcount+"\" /><br /><a href=\"#\" class=\"selimg\" title=\""+type+"-"+intcount+"\">Select</a> | <a href=\"#\" class=\"deleteimg\">Delete</a><input type=\"hidden\" id=\"fld"+type+"-"+intcount+"\" name = \""+type+"imgpath[]\" /><input type=\"hidden\" id=\"fldt"+type+"-"+intcount+"\" name = \""+type+"imgtitle[]\" /></li>";
        jQuery("#"+type+" .gally").append(str);
    }
    </script>
    <style type="text/css">.gally li {margin-top: 22px; float: left; padding: 10px; width: 170px; height: 150px; }.gally li img { height: 100%; }.clr {clear:both;}</style>';
}

add_action('init', 'csInit');

function csInit() {
    add_post_type_support('page', 'excerpt');
    register_nav_menus(array('main-menu' => __('Main Menu')));
    add_post_type_support('post', 'page-attributes');
}

add_shortcode('POPULARBLOCK', 'scPOPULARBLOCK');

function scPOPULARBLOCK($atts, $content = null) {
    return '<div id="ablks" class="row">' . do_shortcode($content) . '</div>';
}

add_shortcode('POPULAR', 'scPOPULAR');

function scPOPULAR($atts, $content = null) {
    $atts = shortcode_atts(array('image' => '', 'alt' => '', 'href' => ''), $atts, 'POPULAR');
    $sImg = '';
    if ($atts['image']) {
        $img = matthewruddy_image_resize(clsImg($atts['image']), 265, 255, true, false);
        $sImg = '<a href="' . $atts['href'] . '"><img src="' . $img['url'] . '" alt="' . $atts['alt'] . '" /></a>';
    }
    return '<div class="col-md-2">
                <div class="ablk">
                    ' . $sImg . '
                    <h3><a href="' . $atts['href'] . '">' . $content . '</a></h3>
                </div>
            </div>';
}

add_shortcode('FEATURESBLOCK', 'scFEATURESBLOCK');

function scFEATURESBLOCK($atts, $content = null) {
    return '<div id="bblks" class="row">' . do_shortcode($content) . '</div>';
}

add_shortcode('FEATURES', 'scFEATURES');

function scFEATURES($atts, $content = null) {
    $atts = shortcode_atts(array('image' => '', 'alt' => '', 'href' => '', 'price' => '', 'rel' => '', 'idr_price' => '', 'rmb_price' => '', 'usd_price' => '', 'aud_price' => ''), $atts, 'FEATURES');

    $sImg = '';
    $rel = $IDR_price = $RMB_price = $USD_price = $AUD_price = '';
    if ($atts['rel']) {
        $rel = $atts['rel'];
    }
    if ($atts['idr_price']) {
        $IDR_price = $atts['idr_price'];
    }
    if ($atts['rmb_price']) {
        $RMB_price = $atts['rmb_price'];
    }
    if ($atts['usd_price']) {
        $USD_price = $atts['usd_price'];
    }
    if ($atts['aud_price']) {
        $AUD_price = $atts['aud_price'];
    }
    if ($atts['image']) {

        $img = matthewruddy_image_resize(clsImg($atts['image']), 260, 265, true, false);
        $sImg = '<a href="' . $atts['href'] . '"><img style="width:260px; height:265px" src="' . $img['url'] . '" alt="' . $atts['alt'] . '" rel="' . $rel . '" /></a>';
    }

    return '<div class="col-md-2">
                <div class="bblk">
                    ' . $sImg . '
                    <h3><a href="' . $atts['href'] . '">' . $content . '</a></h3>
                    <p class="change_price" o_p=""> From <span class="crry">USD</span>  
                    <span class="current_price"> ' . $USD_price . ' </span> 
                    <span class="IDR_price hidden"> ' . $IDR_price . ' </span> 
                    <span class="CNY_price hidden"> ' . $RMB_price . ' </span>
                    <span class="USD_price hidden"> ' . $USD_price . ' </span>
                    <span class="AUD_price hidden"> ' . $AUD_price . ' </span>
                    </p>
                </div>
            </div>';
}

add_shortcode('ICONS', 'scICONS');

function scICONS($atts, $content = null) {
    return '<div id="icns" class="row">
                <div class="col-md-2">
                    <p><img src="' . get_bloginfo('template_url') . '/images/ico-1.png" alt=""></p>
                    <p>Discipline</p>
                </div>
                <div class="col-md-2">
                    <p><img src="' . get_bloginfo('template_url') . '/images/ico-2.png" alt=""></p>
                    <p>Cheap</p>
                </div>
                <div class="col-md-2">
                    <p><img src="' . get_bloginfo('template_url') . '/images/ico-3.png" alt=""></p>
                    <p>Best in Indonesia</p>
                </div>
                <div class="col-md-2">
                    <p><img src="' . get_bloginfo('template_url') . '/images/ico-4.png" alt=""></p>
                    <p>Best quality guarantee</p>
                </div>
                <div class="col-md-2">
                    <p><img src="' . get_bloginfo('template_url') . '/images/ico-5.png" alt=""></p>
                    <p>Customer service</p>
                </div>
                <div class="col-md-2">
                    <p><img src="' . get_bloginfo('template_url') . '/images/ico-6.png" alt=""></p>
                    <p>Customized trip</p>
                </div>
            </div>';
}

add_shortcode('ROW', 'scROW');

function scROW($atts, $content = null) {
    return '<div class="row">' . do_shortcode($content) . '</div>';
}

add_shortcode('COLUMN3', 'scCOLUMN');

function scCOLUMN($atts, $content = null) {
    return '<div class="col-md-4">' . do_shortcode($content) . '</div>';
}

add_shortcode('SUBPAGE', 'scSUBPAGE');

function scSUBPAGE($atts, $content = null) {
    return '<div class="sbpg row">' . do_shortcode($content) . '</div>';
}

add_shortcode('SUBPAGECOLUMN1', 'scSUBPAGECOLUMN1');

function scSUBPAGECOLUMN1($atts, $content = null) {
    return '<div class="col col-md-8">' . do_shortcode($content) . '</div>';
}

add_shortcode('SUBPAGECOLUMN2', 'scSUBPAGECOLUMN2');

function scSUBPAGECOLUMN2($atts, $content = null) {
    return '<div class="col col-md-4">' . do_shortcode($content) . '</div>';
}

add_shortcode('ATTRACTIONS', 'scATTRACTIONS');

function scATTRACTIONS($atts, $content = null) {
    $atts = shortcode_atts(array('image' => '', 'alt' => '', 'href' => '', 'buttontext' => ''), $atts, 'FEATURES');
    $sImg = '';
    if ($atts['image']) {
        $img = matthewruddy_image_resize(clsImg($atts['image']), 320, 205, true, false);
        $sImg = '<a href="' . $atts['href'] . '"><img src="' . $img['url'] . '" alt="' . $atts['alt'] . '" /></a>';
    }
    return '<div class="rmds row">
                <div class="col col-md-4">
                    ' . $sImg . '
                </div>
                <div class="col col-md-8">
                    <div class="pull-right"><a href="' . $atts['href'] . '" class="btn btn-default arr-sm">' . $atts['buttontext'] . '</a></div>
                    ' . $content . '
                </div>
            </div>';
}

add_shortcode('SUBPAGEBANNER', 'scSUBPAGEBANNER');

function scSUBPAGEBANNER($atts, $content = null) {
    return '<div id="bnrs" class="row hidden-xs">
                <div class="col-md-6"><img src="' . get_bloginfo('template_url') . '/images/bnr-1.jpg" alt=""></div>
                <div class="col-md-6"><img src="' . get_bloginfo('template_url') . '/images/bnr-2.jpg" alt=""></div>
            </div>';
}

add_shortcode('BUTTON', 'scBUTTON');

function scBUTTON($atts, $content = null) {
    $atts = shortcode_atts(array('size' => 'small', 'arrow' => 'true', 'href' => '#'), $atts, 'BUTTON');
    $sSize = ' arr-sm';
    if ($atts['size'] == 'large') {
        $sSize = ' arr-lg';
    }
    if (!$atts['arrow']) {
        $sSize = '';
    }
    return '<a href="' . $atts['href'] . '" class="btn btn-default ' . $sSize . '">' . $content . '</a>';
}

add_action('admin_menu', 'csOpt');

function csOpt() {
    add_meta_box('csPgFlds', get_bloginfo('name') . ' Options', 'csPgFlds', 'page', 'normal', 'high');
    add_meta_box('csPgFlds2', get_bloginfo('name') . ' Tour Details', 'csPgFlds2', 'page', 'side', 'high');
}

/* for nontour */
add_action('admin_menu', 'csOpt2');

function csOpt2() {
    add_meta_box('csPgFlds12', get_bloginfo('name') . ' Non-Tour Options', 'csPgFlds12', 'page', 'normal', 'high');
    add_meta_box('csPgFlds22', get_bloginfo('name') . ' Non-Tour Details', 'csPgFlds22', 'page', 'side', 'high');
    add_meta_box('csPgFlds4', get_bloginfo('name') . ' Discount Price Table', 'csPgFlds4', 'page', 'normal', 'high');
}

// function my_theme_scripts12() {
//     wp_enqueue_script( 'my-great-script', get_template_directory_uri() . '/js/my-great-script.js', array( 'jquery' ), '1.0.0', true );
// }
// add_action( 'wp_enqueue_scripts', 'my_theme_scripts12' );


add_action('save_post', 'scsFlds', 10, 2);

function scsFlds($post_id, $post) {
    if (!isset($_POST['csFldnce'])) {
        return $post_id;
    }
    if (!wp_verify_nonce($_POST['csFldnce'], 'csFldv')) {
        return $post_id;
    }
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    if ($_POST['sdrimgpath']) {
        $strValue = $strTValue = '';
        $arrValue = $_POST['sdrimgpath'];
        for ($i = 0; $i < count($arrValue); $i++) {
            $strValue .= cArrVal($arrValue[$i]);
        }
        $arrTValue = $_POST['sdrimgtitle'];
        for ($i = 0; $i < count($arrValue); $i++) {
            $strTValue .= cArrVal($arrTValue[$i]);
        }
        if ($strValue != '0=') {
            update_post_meta($post_id, '_cs_sdr_img', $strValue);
            update_post_meta($post_id, '_cs_sdr_ttl', $strTValue);
        } else {
            delete_post_meta($post_id, '_cs_sdr_img');
            delete_post_meta($post_id, '_cs_sdr_ttl');
        }
    } else {
        delete_post_meta($post_id, '_cs_sdr_img');
        delete_post_meta($post_id, '_cs_sdr_ttl');
    }

    if ($_POST['overviewhd']) {
        update_post_meta($post_id, '_cs_overview_heading', $_POST['overviewhd']);
    } else {
        delete_post_meta($post_id, '_cs_overview_heading');
    }
    if ($_POST['overviewcnt']) {
        update_post_meta($post_id, '_cs_overview_content', $_POST['overviewcnt']);
    } else {
        delete_post_meta($post_id, '_cs_overview_content');
    }

    if ($_POST['meetingplacehd']) {
        update_post_meta($post_id, '_cs_meetingplace_heading', $_POST['meetingplacehd']);
    } else {
        delete_post_meta($post_id, '_cs_meetingplace_heading');
    }
    if ($_POST['meetingplacecnt']) {
        update_post_meta($post_id, '_cs_meetingplace_content', $_POST['meetingplacecnt']);
    } else {
        delete_post_meta($post_id, '_cs_meetingplace_content');
    }
    if ($_POST['gpsurl']) {
        update_post_meta($post_id, '_cs_gps_url', $_POST['gpsurl']);
    } else {
        delete_post_meta($post_id, '_cs_gps_url');
    }

    if ($_POST['pricecnt']) {
        update_post_meta($post_id, '_cs_price_content', $_POST['pricecnt']);
    } else {
        delete_post_meta($post_id, '_cs_price_content');
    }
    if ($_POST['Price']) {
        update_post_meta($post_id, '_cs_Price_heading', $_POST['Price']);
    } else {
        delete_post_meta($post_id, '_cs_Price_heading');
    }


    if ($_POST['hotels']) {
        update_post_meta($post_id, '_cs_hotels', $_POST['hotels']);
    } else {
        delete_post_meta($post_id, '_cs_hotels');
    }
    if ($_POST['Hotels_heading']) {
        update_post_meta($post_id, '_cs_Hotels_heading', $_POST['Hotels_heading']);
    } else {
        delete_post_meta($post_id, '_cs_Hotels_heading');
    }

    if ($_POST['Precautions']) {
        update_post_meta($post_id, '_cs_Precautions', $_POST['Precautions']);
    } else {
        delete_post_meta($post_id, '_cs_Precautions');
    }
    if ($_POST['Precautions_heading']) {
        update_post_meta($post_id, '_cs_Precautions_heading', $_POST['Precautions_heading']);
    } else {
        delete_post_meta($post_id, '_cs_Precautions_heading');
    }




    if ($_POST['placevisit']) {
        update_post_meta($post_id, '_cs_placevisit_content', $_POST['placevisit']);
    } else {
        delete_post_meta($post_id, '_cs_placevisit_content');
    }
    if ($_POST['placevisit_heading']) {
        update_post_meta($post_id, '_cs_placevisit_heading', $_POST['placevisit_heading']);
    } else {
        delete_post_meta($post_id, '_cs_placevisit_heading');
    }



    if ($_POST['Depart']) {
        update_post_meta($post_id, '_cs_Depart_content', $_POST['Depart']);
    } else {
        delete_post_meta($post_id, '_cs_Depart_content');
    }
    //if($_POST['Depart_heading']) { update_post_meta($post_id, '_cs_Depart_heading', $_POST['Depart_heading']); } else { delete_post_meta($post_id, '_cs_Depart_heading'); }



    if ($_POST['Travel_type']) {
        update_post_meta($post_id, '_cs_Travel_type_content', $_POST['Travel_type']);
    } else {
        delete_post_meta($post_id, '_cs_Travel_type_content');
    }
    if ($_POST['Travel_type_heading']) {
        update_post_meta($post_id, '_cs_Travel_type_heading', $_POST['Travel_type_heading']);
    } else {
        delete_post_meta($post_id, '_cs_Travel_type_heading');
    }


    if ($_POST['Destination']) {
        update_post_meta($post_id, '_cs_Destinationcontent', $_POST['Destination']);
    } else {
        delete_post_meta($post_id, '_cs_Destinationcontent');
    }
    //if($_POST['Destination_heading']) { update_post_meta($post_id, '_cs_Destinationheading', $_POST['Destination_heading']); } else { delete_post_meta($post_id, '_cs_Destinationheading'); }


    if ($_POST['language']) {
        update_post_meta($post_id, '_cs_Language_content', $_POST['language']);
    } else {
        delete_post_meta($post_id, '_cs_Language_content');
    }
    if ($_POST['language_heading']) {
        update_post_meta($post_id, '_cs_Language_heading', $_POST['language_heading']);
    } else {
        delete_post_meta($post_id, '_cs_Language_heading');
    }


    if ($_POST['atrimgpath']) {
        $strValue = $strTValue = '';
        $arrValue = $_POST['atrimgpath'];
        for ($i = 0; $i < count($arrValue); $i++) {
            $strValue .= cArrVal($arrValue[$i]);
        }
        $arrTValue = $_POST['atrimgtitle'];
        for ($i = 0; $i < count($arrValue); $i++) {
            $strTValue .= cArrVal($arrTValue[$i]);
        }
        if ($strValue != '0=') {
            update_post_meta($post_id, '_cs_sdr_attraction_img', $strValue);
            update_post_meta($post_id, '_cs_sdr_attraction_ttl', $strTValue);
        } else {
            delete_post_meta($post_id, '_cs_sdr_attraction_img');
            delete_post_meta($post_id, '_cs_sdr_attraction_ttl');
        }
    } else {
        delete_post_meta($post_id, '_cs_sdr_attraction_img');
        delete_post_meta($post_id, '_cs_sdr_attraction_ttl');
    }

    if ($_POST['attractions']) {
        update_post_meta($post_id, '_cs_attractions', $_POST['attractions']);
    } else {
        delete_post_meta($post_id, '_cs_attractions');
    }
    if ($_POST['attractions_heading']) {
        update_post_meta($post_id, '_cs_attractions_heading', $_POST['attractions_heading']);
    } else {
        delete_post_meta($post_id, '_cs_attractions_heading');
    }




    if ($_POST['tourdays']) {
        update_post_meta($post_id, '_cs_tourdays', $_POST['tourdays']);
    } else {
        delete_post_meta($post_id, '_cs_tourdays');
    }
    if ($_POST['personsperroom']) {
        update_post_meta($post_id, '_cs_personsperroom', $_POST['personsperroom']);
    } else {
        delete_post_meta($post_id, '_cs_personsperroom');
    }
    if ($_POST['adultprice']) {
        update_post_meta($post_id, '_cs_adultprice', $_POST['adultprice']);
    } else {
        delete_post_meta($post_id, '_cs_adultprice');
    }
    if ($_POST['childrenprice']) {
        update_post_meta($post_id, '_cs_childrenprice', $_POST['childrenprice']);
    } else {
        delete_post_meta($post_id, '_cs_childrenprice');
    }
    if ($_POST['hotelroomprice']) {
        update_post_meta($post_id, '_cs_hotelroomprice', $_POST['hotelroomprice']);
    } else {
        delete_post_meta($post_id, '_cs_hotelroomprice');
    }
    if ($_POST['extrabedprice']) {
        update_post_meta($post_id, '_cs_extrabedprice', $_POST['extrabedprice']);
    } else {
        delete_post_meta($post_id, '_cs_extrabedprice');
    }
    if ($_POST['addnightroomprice']) {
        update_post_meta($post_id, '_cs_addnightroomprice', $_POST['addnightroomprice']);
    } else {
        delete_post_meta($post_id, '_cs_addnightroomprice');
    }
    if ($_POST['sublogo']) {
        update_post_meta($post_id, '_cs_logo', $_POST['sublogo']);
    } else {
        delete_post_meta($post_id, '_cs_logo');
    }

    if ($_POST['hpsimgpath']) {
        $strValue = $strTValue = '';
        $arrValue = $_POST['hpsimgpath'];
        for ($i = 0; $i < count($arrValue); $i++) {
            $strValue .= cArrVal($arrValue[$i]);
        }
        $arrTValue = $_POST['hpsimgtitle'];
        for ($i = 0; $i < count($arrValue); $i++) {
            $strTValue .= cArrVal($arrTValue[$i]);
        }
        if ($strValue != '0=') {
            update_post_meta($post_id, '_cs_hpsdr_img', $strValue);
            update_post_meta($post_id, '_cs_hpsdr_ttl', $strTValue);
        } else {
            delete_post_meta($post_id, '_cs_hpsdr_img');
            delete_post_meta($post_id, '_cs_hpsdr_ttl');
        }
    } else {
        delete_post_meta($post_id, '_cs_hpsdr_img');
        delete_post_meta($post_id, '_cs_hpsdr_ttl');
    }
}

//--- nontour fields to save ---

add_action('save_post', 'scsFlds2', 10, 2);

function scsFlds2($post_id, $post) {

    if (!isset($_POST['csFldnce2'])) {
        return $post_id;
    }
    if (!wp_verify_nonce($_POST['csFldnce2'], 'csFldv2')) {
        return $post_id;
    }
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    if ($_POST['nsdrimgpath']) {
        // if(1) {
        $strValue2 = $strTValue2 = '';
        $arrValue2 = $_POST['nsdrimgpath'];
        for ($i = 0; $i < count($arrValue2); $i++) {
            $strValue2 .= cArrVal($arrValue2[$i]);
        }
        $arrTValue2 = $_POST['nsdrimgtitle'];
        for ($i = 0; $i < count($arrValue2); $i++) {
            $strTValue2 .= cArrVal($arrTValue2[$i]);
        }
        if ($strValue2 != '0=') {
            update_post_meta($post_id, '_cs_sdr_img_nontour', $strValue2);
            update_post_meta($post_id, '_cs_sdr_ttl_nontour', $strTValue2);
        } else {
            delete_post_meta($post_id, '_cs_sdr_img_nontour');
            delete_post_meta($post_id, '_cs_sdr_ttl_nontour');
        }
    } else {
        delete_post_meta($post_id, '_cs_sdr_img_nontour');
        delete_post_meta($post_id, '_cs_sdr_ttl_nontour');
    }


    if ($_POST['overviewhdnontour']) {
        update_post_meta($post_id, '_cs_overview_heading_nontour', $_POST['overviewhdnontour']);
    } else {
        delete_post_meta($post_id, '_cs_overview_heading_nontour');
    }
    if ($_POST['overviewcntnontour']) {
        update_post_meta($post_id, '_cs_overview_content_nontour', $_POST['overviewcntnontour']);
    } else {
        delete_post_meta($post_id, '_cs_overview_content_nontour');
    }



    if ($_POST['locationhdnontour']) {
        update_post_meta($post_id, '_cs_location_details_nontour', $_POST['locationhdnontour']);
    } else {
        delete_post_meta($post_id, '_cs_location_details_nontour');
    }
    if ($_POST['locationcntnontour']) {
        update_post_meta($post_id, '_cs_location_details_content_nontour', $_POST['locationcntnontour']);
    } else {
        delete_post_meta($post_id, '_cs_location_details_content_nontour');
    }



    if ($_POST['Pricehdnontour']) {
        update_post_meta($post_id, '_cs_Price_heading_nontour', $_POST['Pricehdnontour']);
    } else {
        delete_post_meta($post_id, '_cs_Price_heading_nontour');
    }
    if ($_POST['pricecntnontour']) {
        update_post_meta($post_id, '_cs_price_content_nontour', $_POST['pricecntnontour']);
    } else {
        delete_post_meta($post_id, '_cs_price_content_nontour');
    }


    if ($_POST['Activities_hdnontour']) {
        update_post_meta($post_id, '_cs_Activities_heading_nontour', $_POST['Activities_hdnontour']);
    } else {
        delete_post_meta($post_id, '_cs_Activities_heading_nontour');
    }
    if ($_POST['activitiescntnontour']) {
        update_post_meta($post_id, '_cs_activities_content_nontour', $_POST['activitiescntnontour']);
    } else {
        delete_post_meta($post_id, '_cs_activities_content_nontour');
    }



    if ($_POST['Precautions_hdnontour']) {
        update_post_meta($post_id, '_cs_Precautions_heading_nontour', $_POST['Precautions_hdnontour']);
    } else {
        delete_post_meta($post_id, '_cs_Precautions_heading_nontour');
    }
    if ($_POST['Precautionscntnontour']) {
        update_post_meta($post_id, '_cs_Precautionscnt_nontour', $_POST['Precautionscntnontour']);
    } else {
        delete_post_meta($post_id, '_cs_Precautionscnt_nontour');
    }


    if ($_POST['atrimgpath']) {
        $strValue = $strTValue = '';
        $arrValue = $_POST['atrimgpath'];
        for ($i = 0; $i < count($arrValue); $i++) {
            $strValue .= cArrVal($arrValue[$i]);
        }
        $arrTValue = $_POST['atrimgtitle'];
        for ($i = 0; $i < count($arrValue); $i++) {
            $strTValue .= cArrVal($arrTValue[$i]);
        }
        if ($strValue != '0=') {
            update_post_meta($post_id, '_cs_sdr_attraction_img', $strValue);
            update_post_meta($post_id, '_cs_sdr_attraction_ttl', $strTValue);
        } else {
            delete_post_meta($post_id, '_cs_sdr_attraction_img');
            delete_post_meta($post_id, '_cs_sdr_attraction_ttl');
        }
    } else {
        delete_post_meta($post_id, '_cs_sdr_attraction_img');
        delete_post_meta($post_id, '_cs_sdr_attraction_ttl');
    }


    if ($_POST['attractions_hdnontour']) {
        update_post_meta($post_id, '_cs_attractions_heading_nontour', $_POST['attractions_hdnontour']);
    } else {
        delete_post_meta($post_id, '_cs_attractions_heading_nontour');
    }
    if ($_POST['attractionscntnontour']) {
        update_post_meta($post_id, '_cs_attractionscnt_nontour', $_POST['attractionscntnontour']);
    } else {
        delete_post_meta($post_id, '_cs_attractionscnt_nontour');
    }


    if ($_POST['placevisit_hdnontour']) {
        update_post_meta($post_id, '_cs_placevisit_heading_nontour', $_POST['placevisit_hdnontour']);
    } else {
        delete_post_meta($post_id, '_cs_placevisit_heading_nontour');
    }
    if ($_POST['placevisitnontour']) {
        update_post_meta($post_id, '_cs_placevisit_content_nontour', $_POST['placevisitnontour']);
    } else {
        delete_post_meta($post_id, '_cs_placevisit_content_nontour');
    }

    if ($_POST['Travel_type_headingnontour']) {
        update_post_meta($post_id, '_cs_Travel_type_heading_nontour', $_POST['Travel_type_headingnontour']);
    } else {
        delete_post_meta($post_id, '_cs_Travel_type_heading_nontour');
    }
    if ($_POST['Travel_typecntnontour']) {
        update_post_meta($post_id, '_cs_Travel_type_content_nontour', $_POST['Travel_typecntnontour']);
    } else {
        delete_post_meta($post_id, '_cs_Travel_type_content_nontour');
    }



    if ($_POST['Destinationcntnontour']) {
        update_post_meta($post_id, '_cs_Destinationcontent_nontour', $_POST['Destinationcntnontour']);
    } else {
        delete_post_meta($post_id, '_cs_Destinationcontent_nontour');
    }



    if ($_POST['language_heading_nontour']) {
        update_post_meta($post_id, '_cs_Language_heading_nontour', $_POST['language_heading_nontour']);
    } else {
        delete_post_meta($post_id, '_cs_Language_heading_nontour');
    }
    if ($_POST['language_cnt_nontour']) {
        update_post_meta($post_id, '_cs_Language_content_nontour', $_POST['language_cnt_nontour']);
    } else {
        delete_post_meta($post_id, '_cs_Language_content_nontour');
    }




    if ($_POST['locations']) {
        update_post_meta($post_id, '_cs_locations', $_POST['locations']);
    } else {
        delete_post_meta($post_id, '_cs_locations');
    }
    if ($_POST['typeofactivity']) {
        update_post_meta($post_id, '_cs_activitytype', $_POST['typeofactivity']);
    } else {
        delete_post_meta($post_id, '_cs_activitytype');
    }



    if ($_POST['hpsimgpath']) {
        $strValue = $strTValue = '';
        $arrValue = $_POST['hpsimgpath'];
        for ($i = 0; $i < count($arrValue); $i++) {
            $strValue .= cArrVal($arrValue[$i]);
        }
        $arrTValue = $_POST['hpsimgtitle'];
        for ($i = 0; $i < count($arrValue); $i++) {
            $strTValue .= cArrVal($arrTValue[$i]);
        }
        if ($strValue != '0=') {
            update_post_meta($post_id, '_cs_hpsdr_img_nontour', $strValue);
            update_post_meta($post_id, '_cs_hpsdr_ttl_nontour', $strTValue);
        } else {
            delete_post_meta($post_id, '_cs_hpsdr_img_nontour');
            delete_post_meta($post_id, '_cs_hpsdr_ttl_nontour');
        }
    } else {
        delete_post_meta($post_id, '_cs_hpsdr_img_nontour');
        delete_post_meta($post_id, '_cs_hpsdr_ttl_nontour');
    }
}

//--- nontour fields to save end---
//---discount price table---

add_action('save_post', 'scsFlds3', 10, 2);

function scsFlds3($post_id, $post) {
    if (!isset($_POST['csFldnce'])) {
        return $post_id;
    }
    if (!wp_verify_nonce($_POST['csFldnce'], 'csFldv')) {
        return $post_id;
    }
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    if ($_POST['disbaligroup1']) {
        update_post_meta($post_id, '_cs_disbaligroup1', $_POST['disbaligroup1']);
    } else {
        delete_post_meta($post_id, '_cs_disbaligroup1');
    }

    if ($_POST['disbaligroup2']) {
        update_post_meta($post_id, '_cs_disbaligroup2', $_POST['disbaligroup2']);
    } else {
        delete_post_meta($post_id, '_cs_disbaligroup2');
    }

    if ($_POST['disbaligroup3']) {
        update_post_meta($post_id, '_cs_disbaligroup3', $_POST['disbaligroup3']);
    } else {
        delete_post_meta($post_id, '_cs_disbaligroup3');
    }

    if ($_POST['disbaligroup4']) {
        update_post_meta($post_id, '_cs_disbaligroup4', $_POST['disbaligroup4']);
    } else {
        delete_post_meta($post_id, '_cs_disbaligroup4');
    }

    if ($_POST['disbaligroup5']) {
        update_post_meta($post_id, '_cs_disbaligroup5', $_POST['disbaligroup5']);
    } else {
        delete_post_meta($post_id, '_cs_disbaligroup5');
    }


    if ($_POST['disnonbaligroup1']) {
        update_post_meta($post_id, '_cs_disnonbaligroup1', $_POST['disnonbaligroup1']);
    } else {
        delete_post_meta($post_id, '_cs_disnonbaligroup1');
    }

    if ($_POST['disnonbaligroup2']) {
        update_post_meta($post_id, '_cs_disnonbaligroup2', $_POST['disnonbaligroup2']);
    } else {
        delete_post_meta($post_id, '_cs_disnonbaligroup2');
    }

    if ($_POST['disnonbaligroup3']) {
        update_post_meta($post_id, '_cs_disnonbaligroup3', $_POST['disnonbaligroup3']);
    } else {
        delete_post_meta($post_id, '_cs_disnonbaligroup3');
    }

    if ($_POST['disnonbaligroup4']) {
        update_post_meta($post_id, '_cs_disnonbaligroup4', $_POST['disnonbaligroup4']);
    } else {
        delete_post_meta($post_id, '_cs_disnonbaligroup4');
    }

    if ($_POST['disnonbaligroup5']) {
        update_post_meta($post_id, '_cs_disnonbaligroup5', $_POST['disnonbaligroup5']);
    } else {
        delete_post_meta($post_id, '_cs_disnonbaligroup5');
    }
}

//--discount price table end---


function csPgFlds($post) {
    wp_nonce_field('csFldv', 'csFldnce');
    $sTpl = get_post_meta($post->ID, '_wp_page_template', true);
    if ($sTpl == 'tpl-tour.php') {
        echo '<strong>Slider</strong><br>';
        $aImg = explode('=', get_post_meta($post->ID, '_cs_sdr_img', true));
        $aTle = explode('=', get_post_meta($post->ID, '_cs_sdr_ttl', true));
        echo getSldr($aImg, $aTle, 'sdr');



        echo "<style> fieldset {font-family: sans-serif; border: 5px solid #1F497D;";
        echo "background: #ddd;border-radius: 5px;padding: 15px;}";

        echo "fieldset legend { background: #1F497D; color: #fff; padding: 5px 10px ; font-size: 32px;";
        echo " border-radius: 5px; box-shadow: 0 0 0 5px #ddd; margin-left: 20px; } ";
        echo " #overviewcnt_ifr,#meetingplacecnt_ifr,#pricecnt_ifr,#hotels_ifr,#Precautions_ifr,#attractions_ifr{ height:200px !important} </style>";


        echo " <fieldset><legend>Overview:</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_overview_heading', true) . "' id='overviewhd' name='overviewhd' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_overview_content', true), 'overviewcnt');
        echo "</fieldset>";



        echo '<p>&nbsp;</p>';


        echo " <fieldset><legend>Meeting Place:</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_meetingplace_heading', true) . "' id='meetingplacehd' name='meetingplacehd' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_meetingplace_content', true), 'meetingplacecnt');
        echo "</fieldset>";



        echo '<p>&nbsp;</p>';
        echo '<p>
                <strong>GPS</strong><br>
                Image URL:<br><textarea rows="1" id="gpsurl" name="gpsurl" style="width:100%" >' . get_post_meta($post->ID, '_cs_gps_url', true) . '</textarea>
              </p>';
        echo '<p>&nbsp;</p>';



        echo " <fieldset><legend>Price :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_Price_heading', true) . "' id='Price' name='Price' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_price_content', true), 'pricecnt');
        echo "</fieldset>";


        echo '<p>&nbsp;</p>';

        echo " <fieldset><legend>Hotels :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_Hotels_heading', true) . "' id='Hotels_heading' name='Hotels_heading' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_hotels', true), 'hotels');
        echo "</fieldset>";



        echo '<p>&nbsp;</p>';

        echo " <fieldset><legend>Precautions :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_Precautions_heading', true) . "' id='Precautions_heading' name='Precautions_heading' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_Precautions', true), 'Precautions');
        echo "</fieldset>";


        echo '<p>&nbsp;</p>';

        echo " <fieldset><legend>Attractions :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_attractions_heading', true) . "' id='attractions_heading' name='attractions_heading' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_attractions', true), 'attractions');
        echo "</fieldset>";


        echo '<p>&nbsp;</p>';

        echo " <fieldset><legend>Place To Visit :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_placevisit_heading', true) . "' id='placevisit_heading' name='placevisit_heading' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo "<textarea id='placevisit' name='placevisit' style='width:100%'>" . get_post_meta($post->ID, '_cs_placevisit_content', true) . "</textarea>";
        echo "</fieldset>";


        echo '<p>&nbsp;</p>';

        echo " <fieldset><legend>Depart :</legend>";
        //echo " Heading:<input type='text' value='".get_post_meta($post->ID,'_cs_Depart_heading',true)."' id='Depart_heading' name='Depart_heading' style='width:100%' /><br>";
        //echo "<p>&nbsp;</p>Content:";
        echo "<textarea id='Depart' name='Depart' style='width:100%'>" . get_post_meta($post->ID, '_cs_Depart_content', true) . "</textarea>";
        echo "</fieldset>";



        echo '<p>&nbsp;</p>';
        echo " <fieldset><legend>Travel type :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_Travel_type_heading', true) . "' id='Travel_type_heading' name='Travel_type_heading' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo "<textarea id='Travel_type' name='Travel_type' style='width:100%'>" . get_post_meta($post->ID, '_cs_Travel_type_content', true) . "</textarea>";
        echo "</fieldset>";


        echo '<p>&nbsp;</p>';
        echo " <fieldset><legend>Destination :</legend>";
        //echo " Heading:<input type='text' value='".get_post_meta($post->ID,'_cs_Destinationheading',true)."' id='Destination_heading' name='Destination_heading' style='width:100%' /><br>";
        //echo "<p>&nbsp;</p>Content:";
        echo "<textarea id='Destination' name='Destination' style='width:100%'>" . get_post_meta($post->ID, '_cs_Destinationcontent', true) . "</textarea>";
        echo "</fieldset>";



        echo '<p>&nbsp;</p>';
        echo " <fieldset><legend>language :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_Language_heading', true) . "' id='language_heading' name='language_heading' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo "<textarea id='language' name='language' style='width:100%'>" . get_post_meta($post->ID, '_cs_Language_content', true) . "</textarea>";
        echo "</fieldset>";
    } else if ($sTpl == 'tpl-homepage.php') {
        echo '<strong>Slider</strong><br>';
        $aAImg = explode('=', get_post_meta($post->ID, '_cs_hpsdr_img', true));
        $aATle = explode('=', get_post_meta($post->ID, '_cs_hpsdr_ttl', true));
        echo getSldr($aAImg, $aATle, 'hps');
    } else {
        
    }
}

function csPgFlds2($post) {
    wp_nonce_field('csFldv', 'csFldnce');
    $sTpl = get_post_meta($post->ID, '_wp_page_template', true);
    if ($sTpl == 'tpl-tour.php') {
        echo '<p>
                <strong>Tour Days</strong><br>
                <input type="text" value="' . get_post_meta($post->ID, '_cs_tourdays', true) . '" id="tourdays" name="tourdays" style="width:100%" /><br>
              </p>';
        echo '<p>
                <strong>Persons Per Room</strong><br>
                <input type="text" value="' . get_post_meta($post->ID, '_cs_personsperroom', true) . '" id="personsperroom" name="personsperroom" style="width:100%" /><br>
              </p>';
        echo '<p>
                <strong>Adult Price</strong><br>
                <input type="text" value="' . get_post_meta($post->ID, '_cs_adultprice', true) . '" id="adultprice" name="adultprice" style="width:100%" /><br>
              </p>';
        echo '<p>
                <strong>Children Price</strong><br>
                <input type="text" value="' . get_post_meta($post->ID, '_cs_childrenprice', true) . '" id="childrenprice" name="childrenprice" style="width:100%" /><br>
              </p>';
        echo '<p>
                <strong>Hotel Rooms Price</strong><br>
                <input type="text" value="' . get_post_meta($post->ID, '_cs_hotelroomprice', true) . '" id="hotelroomprice" name="hotelroomprice" style="width:100%" /><br>
              </p>';
        echo '<p>
                <strong>Extra Bed Price</strong><br>
                <input type="text" value="' . get_post_meta($post->ID, '_cs_extrabedprice', true) . '" id="extrabedprice" name="extrabedprice" style="width:100%" /><br>
              </p>';
        echo '<p>
                <strong>Additional Night Room Price</strong><br>
                <input type="text" value="' . get_post_meta($post->ID, '_cs_addnightroomprice', true) . '" id="addnightroomprice" name="addnightroomprice" style="width:100%" /><br>
              </p>';
        echo "<script>   
jQuery(document).ready(function(){   
    var upload_frame;   
    var value_id;   
    jQuery('.upload_button').live('click',function(event){   
        value_id =jQuery( this ).attr('id');       
        event.preventDefault();   
        if( upload_frame ){   
            upload_frame.open();   
            return;   
        }   
        upload_frame = wp.media({   
            title: 'Insert image',   
            button: {   
                text: 'Insert',   
            },   
            multiple: false   
        });   
        upload_frame.on('select',function(){   
            attachment = upload_frame.state().get('selection').first().toJSON(); 
            //console.log(attachment);
            jQuery('#sublogo').val(attachment.id);
            jQuery('#sublogoimg').attr('src',attachment.url);
            jQuery('#sublogoimg').show();
        });    
        upload_frame.open();   
    });
    jQuery('#Removelogo').live('click',function(event){
        event.preventDefault();
        jQuery('#sublogo').val(' ');
        jQuery('#sublogoimg').hide();
    });
    
});   
</script>";
        echo '<p>
        <strong>Logo of sub agency</strong><br>';
        if (get_post_meta($post->ID, '_cs_logo', true) && is_string(get_post_meta($post->ID, '_cs_logo', true))) {
            echo '<img id="sublogoimg" src="' . wp_get_attachment_url(get_post_meta($post->ID, '_cs_logo', true)) . '" style="width:100%" />';
        } else
            echo '<img id="sublogoimg" src="" style="width:100%;  display:none" />';
        echo '<input type="number" value="' . get_post_meta($post->ID, '_cs_logo', true) . '" name="sublogo" id="sublogo" style="width:100%; visibility:hidden" />
<a id="upload" class="upload_button button" href="#">Upload Image</a> <a id="Removelogo" class="button" href="#">Remove</a></p>';
    } else {
        
    }
}

//----------------------------for nontour field starts-----------------------------------
function csPgFlds12($post) {
    wp_nonce_field('csFldv2', 'csFldnce2');
    $sTpl = get_post_meta($post->ID, '_wp_page_template', true);
    if ($sTpl == 'tpl-nontour.php') {
        echo '<strong>Slider</strong><br>';
        $aImg2 = explode('=', get_post_meta($post->ID, '_cs_sdr_img_nontour', true));
        $aTle2 = explode('=', get_post_meta($post->ID, '_cs_sdr_ttl_nontour', true));
        echo getSldr2($aImg2, $aTle2, 'nsdr');



        echo "<style> fieldset {font-family: sans-serif; border: 5px solid #1F497D;";
        echo "background: #ddd;border-radius: 5px;padding: 15px;}";

        echo "fieldset legend { background: #1F497D; color: #fff; padding: 5px 10px ; font-size: 32px;";
        echo " border-radius: 5px; box-shadow: 0 0 0 5px #ddd; margin-left: 20px; } ";
        echo " #overviewcnt_ifr,#meetingplacecnt_ifr,#pricecnt_ifr,#hotels_ifr,#Precautions_ifr,#attractions_ifr{ height:200px !important} </style>";


        echo " <fieldset><legend>Overview:</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_overview_heading_nontour', true) . "' id='overviewhdnontour' name='overviewhdnontour' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_overview_content_nontour', true), 'overviewcntnontour');
        echo "</fieldset>";



        echo '<p>&nbsp;</p>';


        echo " <fieldset><legend>Location details:</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_location_details_nontour', true) . "' id='locationhdnontour' name='locationhdnontour' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_location_details_content_nontour', true), 'locationcntnontour');
        echo "</fieldset>";



        echo '<p>&nbsp;</p>';




        echo " <fieldset><legend>Price :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_Price_heading_nontour', true) . "' id='Pricehdnontour' name='Pricehdnontour' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_price_content_nontour', true), 'pricecntnontour');
        echo "</fieldset>";


        echo '<p>&nbsp;</p>';

        echo " <fieldset><legend>Activities :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_Activities_heading_nontour', true) . "' id='Activities_hdnontour' name='Activities_hdnontour' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_activities_content_nontour', true), 'activitiescntnontour');
        echo "</fieldset>";



        echo '<p>&nbsp;</p>';

        echo " <fieldset><legend>Precautions :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_Precautions_heading_nontour', true) . "' id='Precautions_hdnontour' name='Precautions_hdnontour' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_Precautionscnt_nontour', true), 'Precautionscntnontour');
        echo "</fieldset>";


        echo '<p>&nbsp;</p>';

        echo " <fieldset><legend>Attractions :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_attractions_heading_nontour', true) . "' id='attractions_hdnontour' name='attractions_hdnontour' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo the_editor(get_post_meta($post->ID, '_cs_attractionscnt_nontour', true), 'attractionscntnontour');
        echo "</fieldset>";


        echo '<p>&nbsp;</p>';

        echo " <fieldset><legend>Place To Visit :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_placevisit_heading_nontour', true) . "' id='placevisit_hdnontour' name='placevisit_hdnontour' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo "<textarea id='placevisitnontour' name='placevisitnontour' style='width:100%'>" . get_post_meta($post->ID, '_cs_placevisit_content_nontour', true) . "</textarea>";

        echo "</fieldset>";


        echo '<p>&nbsp;</p>';




        echo '<p>&nbsp;</p>';
        echo " <fieldset><legend>Travel type :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_Travel_type_heading_nontour', true) . "' id='Travel_type_headingnontour' name='Travel_type_headingnontour' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo "<textarea id='Travel_typecntnontour' name='Travel_typecntnontour' style='width:100%'>" . get_post_meta($post->ID, '_cs_Travel_type_content_nontour', true) . "</textarea>";
        echo "</fieldset>";


        echo '<p>&nbsp;</p>';
        echo " <fieldset><legend>Destination :</legend>";
        //echo " Heading:<input type='text' value='".get_post_meta($post->ID,'_cs_Destinationheading',true)."' id='Destination_heading' name='Destination_heading' style='width:100%' /><br>";
        //echo "<p>&nbsp;</p>Content:";
        echo "<textarea id='Destinationcntnontour' name='Destinationcntnontour' style='width:100%'>" . get_post_meta($post->ID, '_cs_Destinationcontent_nontour', true) . "</textarea>";
        echo "</fieldset>";



        echo '<p>&nbsp;</p>';
        echo " <fieldset><legend>language :</legend>";
        echo " Heading:<input type='text' value='" . get_post_meta($post->ID, '_cs_Language_heading_nontour', true) . "' id='language_heading_nontour' name='language_heading_nontour' style='width:100%' /><br>";
        echo "<p>&nbsp;</p>Content:";
        echo "<textarea id='language_cnt_nontour' name='language_cnt_nontour' style='width:100%'>" . get_post_meta($post->ID, '_cs_Language_content_nontour', true) . "</textarea>";
        echo "</fieldset>";
    } else if ($sTpl == 'tpl-homepage.php') {
        echo '<strong>Slider</strong><br>';
        $aAImg = explode('=', get_post_meta($post->ID, '_cs_hpsdr_img_nontour', true));
        $aATle = explode('=', get_post_meta($post->ID, '_cs_hpsdr_ttl_nontour', true));
        echo getSldr2($aAImg, $aATle, 'hps');
    } else {
        
    }
}

function csPgFlds22($post) {
    wp_nonce_field('csFldv2', 'csFldnce2');
    $sTpl = get_post_meta($post->ID, '_wp_page_template', true);
    if ($sTpl == 'tpl-nontour.php') {
        echo '<p>
                <strong>Locations</strong><br>
                <input type="text" value="' . get_post_meta($post->ID, '_cs_locations', true) . '" id="locations" name="locations" style="width:100%" /><br>
              </p>';
        echo '<p>
                <strong>Type of Activity</strong><br>
                <input type="text" value="' . get_post_meta($post->ID, '_cs_activitytype', true) . '" id="typeofactivity" name="typeofactivity" style="width:100%" /><br>
              </p>';

        /* echo  "<script>   
          jQuery(document).ready(function(){
          var upload_frame;
          var value_id;
          jQuery('.upload_button').live('click',function(event){
          value_id =jQuery( this ).attr('id');
          event.preventDefault();
          if( upload_frame ){
          upload_frame.open();
          return;
          }
          upload_frame = wp.media({
          title: 'Insert image',
          button: {
          text: 'Insert',
          },
          multiple: false
          });
          upload_frame.on('select',function(){
          attachment = upload_frame.state().get('selection').first().toJSON();
          //console.log(attachment);
          jQuery('#sublogo').val(attachment.id);
          jQuery('#sublogoimg').attr('src',attachment.url);
          jQuery('#sublogoimg').show();
          });
          upload_frame.open();
          });
          jQuery('#Removelogo').live('click',function(event){
          event.preventDefault();
          jQuery('#sublogo').val(' ');
          jQuery('#sublogoimg').hide();
          });

          });
          </script>" ; */
        echo '<p>
        <strong>Logo of sub agency</strong><br>';
        if (get_post_meta($post->ID, '_cs_logo', true) && is_string(get_post_meta($post->ID, '_cs_logo_nontour', true))) {
            echo '<img id="sublogoimg" src="' . wp_get_attachment_url(get_post_meta($post->ID, '_cs_logo_nontour', true)) . '" style="width:100%" />';
        } else
            echo '<img id="sublogoimg" src="" style="width:100%;  display:none" />';
        echo '<input type="number" value="' . get_post_meta($post->ID, '_cs_logo_nontour', true) . '" name="sublogo" id="sublogo" style="width:100%; visibility:hidden" />
<a id="upload" class="upload_button button" href="#">Upload Image</a> <a id="Removelogo" class="button" href="#">Remove</a></p>';
    } else {
        
    }
}

//-----------------------------------------------for nontour ends-----------------------------------
//discount price table------------------------------
function csPgFlds4($post) {
    wp_nonce_field('csFldv', 'csFldnce');
    $sTpl = get_post_meta($post->ID, '_wp_page_template', true);
    if ($sTpl == 'tpl-tour.php' || 'landarrangement.php') {

        echo "<p>&nbsp;</p>";
        echo "<h4>Discount Table</h4>";
        echo "<table>
    <thead>
        <tr>
            <th>Number Of Guest</th>
            <th>Discount Bali</th>
            <th>Discount Non Bali</th>
            
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1-2</td>
            <td><input type='text' name='disbaligroup1' id='disbaligroup1' value='" . get_post_meta($post->ID, '_cs_disbaligroup1', true) . "'/></td>
            <td><input type='text' name='disnonbaligroup1' id='disnonbaligroup1' value='" . get_post_meta($post->ID, '_cs_disnonbaligroup1', true) . "'/></td>
       </tr>
        <tr>
            <td>3-5</td>
            <td><input type='text' name='disbaligroup2' id='disbaligroup2' value='" . get_post_meta($post->ID, '_cs_disbaligroup2', true) . "'/></td>
            <td><input type='text' name='disnonbaligroup2' id='disnonbaligroup2' value='" . get_post_meta($post->ID, '_cs_disnonbaligroup2', true) . "'/></td>
       </tr>
       <tr>
            <td>5-10</td>
            <td><input type='text' name='disbaligroup3' id='disbaligroup3' value='" . get_post_meta($post->ID, '_cs_disbaligroup3', true) . "'/></td>
            <td><input type='text' name='disnonbaligroup3' id='disnonbaligroup3' value='" . get_post_meta($post->ID, '_cs_disnonbaligroup3', true) . "'/></td>
            <td></td>
       </tr>
       <tr>
            <td>10-15</td>
            <td><input type='text' name='disbaligroup4' id='disbaligroup4' value='" . get_post_meta($post->ID, '_cs_disbaligroup4', true) . "'/></td>
            <td><input type='text' name='disnonbaligroup4' id='disnonbaligroup4' value='" . get_post_meta($post->ID, '_cs_disnonbaligroup4', true) . "'/></td>
       </tr>
       <tr>
            <td>>15</td>
            <td><input type='text' name='disbaligroup5' id='disbaligroup5' value='" . get_post_meta($post->ID, '_cs_disbaligroup5', true) . "'/></td>
            <td><input type='text' name='disnonbaligroup5' id='disnonbaligroup5' value='" . get_post_meta($post->ID, '_cs_disnonbaligroup5', true) . "'/></td>
       </tr>
       </tbody>
        </table>";
    }
}

//discount price table------------------------------



function getSldr($aImg, $aTle, $sUniq = '') {
    $strHTML .= '<table id="' . $sUniq . '" border="0" class="form-table"><tr><td valign="top"><ul class="gally">';
    if ($aImg[0]) {
        for ($i = 0; $i < count($aImg); $i++) {
            if ($aImg[$i]) {
                $strHTML .= '<li ><strong>Image ' . ($i + 1) . ':</strong><br />
                <img src="' . $aImg[$i] . '" width="150" height="100" id="' . $sUniq . 'imgfld-' . ($i + 1) . '" /><br />
                <a href="#" class="selimg" title="' . $sUniq . '-' . ($i + 1) . '">Change</a> | <a href="#" class="deleteimg">Delete</a>
                <input type="hidden" id="' . $sUniq . 'imgfld-' . ($i + 1) . '" name="' . $sUniq . 'imgpath[]" style="width: 80%" value="' . $aImg[$i] . '" />
                <input type="hidden" id="' . $sUniq . 'imgfldt-' . ($i + 1) . '" name="' . $sUniq . 'imgtitle[]" value="' . $aTle[$i] . '" /></li>';
            }
        }
    }
    $strHTML .= '</ul><div class="clr"></div></td></tr></table><table border="0" class="form-table"><tr><td><input type="button" class="button" value="Add Image(s)" onclick="addImgs(\'' . $sUniq . '\')" /></td></tr></table>';
    return $strHTML;
}

function getSldr2($aImg, $aTle, $sUniq = '') {
    $strHTML .= '<table id="' . $sUniq . '" border="0" class="form-table"><tr><td valign="top"><ul class="gally">';
    if ($aImg[0]) {
        for ($i = 0; $i < count($aImg); $i++) {
            if ($aImg[$i]) {
                $strHTML .= '<li ><strong>Image ' . ($i + 1) . ':</strong><br />
                <img src="' . $aImg[$i] . '" width="150" height="100" id="' . $sUniq . 'imgfld-' . ($i + 1) . '" /><br />
                <a href="#" class="selimg" title="' . $sUniq . '-' . ($i + 1) . '">Change</a> | <a href="#" class="deleteimg">Delete</a>
                <input type="hidden" id="' . $sUniq . 'imgfld-' . ($i + 1) . '" name="' . $sUniq . 'imgpath[]" style="width: 80%" value="' . $aImg[$i] . '" />
                <input type="hidden" id="' . $sUniq . 'imgfldt-' . ($i + 1) . '" name="' . $sUniq . 'imgtitle[]" value="' . $aTle[$i] . '" /></li>';
            }
        }
    }
    $strHTML .= '</ul><div class="clr"></div></td></tr></table><table border="0" class="form-table"><tr><td><input type="button" class="button" value="Add Image(s)" onclick="addImgs(\'' . $sUniq . '\')" /></td></tr></table>';
    return $strHTML;
}

add_shortcode('HPBLOG', 'scHPBLOG');

function scHPBLOG($atts, $content = null) {
    global $post;
    $sH = '';
    $oTemp = $post;
    $oPsts = get_posts(array('suppress_filters' => 0, 'posts_per_page' => 7, 'category' => get_cat_ID('ï¿½?ï¿½å®¢')));
    $sH .= '<div class="blg">';
    $i = 1;
    foreach ($oPsts as $post) {
        setup_postdata($post);

        if ($i == 1) {
            $sH .= '<div class="blgthm">';
            if (has_post_thumbnail()) {
                $aFimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                $img = matthewruddy_image_resize(clsImg($aFimg[0]), 365, 140, true, false);
                $sH .= '<a href="' . get_the_permalink() . '"><img style="width:100%" src="' . $img['url'] . '" alt="' . get_the_title() . '" /></a>';
            }
            $sH .= '</div><h2><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h2><p class="blgdt"> ' . date('M d, Y', strtotime(get_the_date())) . '</p><p>' . get_the_excerpt() . '</p>';
        } else {
            if ($i == 2) {
                $sH .= '<br><ul>';
            }
            $sH .= '<li><h4 style="margin:15px 0px"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h4></li>';
        }
        $i++;
    }
    if ($i >= 2) {
        $sH .= '</ul>';
    }

    $sH .= '</div>';
    $post = $oTemp;
    return $sH;
}

function kriesi_pagination($pages = '', $range = 2) {

    $showitems = ($range * 2) + 1;

    global $paged;
    if (empty($paged))
        $paged = 1;

    if ($pages == '') {

        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }


    if (1 != $pages) {

        echo "<div class='pagination'>";
        if ($paged > 2 && $paged > $range + 1 && $showitems < $pages)
            echo "<a href='" . get_pagenum_link(1) . "'>&laquo;</a>";
        if ($paged > 1 && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo;</a>";

        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems )) {
                echo ($paged == $i) ? "<span class='current'>" . $i . "</span>" : "<a href='" . get_pagenum_link($i) . "' class='inactive' >" . $i . "</a>";
            }
        }

        if ($paged < $pages && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($paged + 1) . "'>&rsaquo;</a>";
        if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($pages) . "'>&raquo;</a>";
        echo "</div>\n";
    }
}

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '1');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function remove_page_from_query_string($query_string) {
    if ($query_string['name'] == 'page' && isset($query_string['page'])) {
        unset($query_string['name']);
        // 'page' in the query_string looks like '/2', so i'm spliting it out
        list($delim, $page_index) = split('/', $query_string['page']);
        $query_string['paged'] = $page_index;
    }
    return $query_string;
}

// I will kill you if you remove this. I died two days for this line 
add_filter('request', 'remove_page_from_query_string');

// following are code adapted from Custom Post Type Category Pagination Fix by jdantzer
function fix_category_pagination($qs) {
    if (isset($qs['category_name']) && isset($qs['paged'])) {
        $qs['post_type'] = get_post_types($args = array(
            'public' => true,
            '_builtin' => false
        ));
        array_push($qs['post_type'], 'post');
    }
    return $qs;
}

add_filter('request', 'fix_category_pagination');

// Register Sidebars
function header_right_sidebars() {

    $args = array(
        'id' => 'header_right_sidebar',
        'name' => __('Header right', 'tourfrombali.com'),
        'before_widget' => '<div id="rightsearchform" class="col-sm-6 col-md-5 col-lg-4">',
        'after_widget' => '</div>',
    );
    register_sidebar($args);
}

add_action('widgets_init', 'header_right_sidebars');

function forum_sidebar() {
    register_sidebar(
            array(
                'name' => __('Forum', 'tourfrombali'),
                'id' => 'forum-side-bar',
                'description' => __('Forum Sidebar', 'tourfrombali'),
                'before_widget' => '<div class="widget-content">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            )
    );
}

add_action('widgets_init', 'forum_sidebar');

function member_profile_sidebar() {
    register_sidebar(
            array(
                'name' => __('Member Profile Sidebar', 'tourfrombali'),
                'id' => 'member-profile-side-bar',
                'description' => __('Member Profile Sidebar', 'tourfrombali'),
                'before_widget' => '<div class="widget-content">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            )
    );
}

add_action('widgets_init', 'member_profile_sidebar');

// Creating the widget
class tfb_bbpress_navigation extends WP_Widget {

    function __construct() {
        parent::__construct(
// Base ID of your widget
                'tfb_bbpress_navigation',
// Widget name will appear in UI
                __('BBPress Navigation', 'tourfrombali'),
// Widget description
                array('description' => __('BBPress navigation to display forums', 'tourfrombali'),)
        );
    }

// Creating widget front-end
// This is where the action happens
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
        echo '<div class="bbpress-navigation">';
        wp_list_pages(array(
            'title_li' => '',
            'child_of' => 5040,
            'post_type' => 'forum',
            'depth' => 3
        ));
        echo '</div>';
    }

// Widget Backend
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

// Class wpb_widget ends here
// Register and load the widget
function tfb_load_widget() {
    register_widget('tfb_bbpress_navigation');
}

add_action('widgets_init', 'tfb_load_widget');

include('functions-user-profile-map.php');

add_filter('comments_template', function() {
    global $post;
    $QAcategory = CMA_Category::getPostCommentsCategory($post->ID);

    if ($QAcategory && $QAcategory->taxonomy == 'cma_category') {
        echo '<div id="comments-top" class="q-as-describe-ask clearfix">
                <h3>Q & A</h3>
                <p>
                    Questions previously answered may have different answers due to many reasons: season (date and year of travel), rates, availability, etc.Please check the <a class="a-blue-type" href="#">TourFromBali Program FAQ</a> for more information.
                </p>
                <i class="btn btn-info btn-middle js-ask-btn">Ask Us</i>
            </div>';
    }
});

function baliGetAnswersByThread($threadId, $approved = true, $orderby = 'newest', $sort = null, $limit = null) {
    global $wpdb;

    if (is_null($orderby)) {
        //  $orderby = self::getOrderBy();
    }
    if (!is_null($sort) AND strtolower($sort) != 'asc')
        $sort = 'desc';

    $join = '';
    if ($orderby == CMA_Answer::ORDER_BY_VOTES) {
        //  $join = $wpdb->prepare("LEFT JOIN $wpdb->commentmeta cmv ON cmv.comment_ID = c.comment_ID AND cmv.meta_key = %s", self::META_RATING);
        $order = ' ORDER BY cmv.meta_value';
    } else {
        $order = 'ORDER BY c.comment_date_gmt';
    }
    $order .= ' ' . (is_null($sort) ? (CMA_Settings::getOption(CMA_Settings::OPTION_ANSWER_SORTING_DESC) ? 'DESC' : 'ASC') : $sort);

    $limitPart = '';
    if (!is_null($limit)) {
        $limitPart = ' LIMIT ' . $limit;
    }

    $where = '';
    if (is_bool($approved)) {
        $where .= ' AND c.comment_approved = ' . intval($approved);
    }

    $comments = $wpdb->get_results($wpdb->prepare("SELECT c.* FROM $wpdb->comments c
            " . $join . "
            WHERE c.comment_post_ID = %d
                AND c.comment_type = %s
                $where
            $order
            $limitPart", $threadId, 'cma_answer'
    ));
    $result = array();
    foreach ($comments as $comment) {
        $result[] = $comment;
    }
    return $result;
}

add_action('pre_get_posts', 'alter_query');

function alter_query($query) {

    if (is_search()) {
        if (is_tag(219) && is_main_query()) {
            $min_price = $max_price = array();
            $destination = isset($_GET['destination']) ? trim($_GET['destination']) : '';
            if ($destination != '') {
                $destination = array(
                    'key' => 'trip_type_destination_%_destination',
                    'value' => $destination,
                    'compare' => 'LIKE'
                );
            }
            $trip_type = isset($_GET['trip-type']) ? trim($_GET['trip-type']) : '';
            if ($trip_type != '') {
                $trip_type = array(
                    'key' => 'trip_type_destination_%_trip_type',
                    'value' => $trip_type,
                    'compare' => 'LIKE'
                );
            }
            $days = isset($_GET['days']) ? trim($_GET['days']) : '';
            if ($days != '') {
                $days = array(
                    'key' => 'days',
                    'value' => $days,
                    'compare' => 'LIKE'
                );
            }
            $price_range = isset($_GET['search-box-range']) ? trim($_GET['search-box-range']) : '';
            if ($price_range != '') {
                $price_range = explode(',', $price_range);
                $min_price = array(
                    'key' => '_cs_adultprice',
                    'value' => intval($price_range[0]) / get_option("_cs_currency_USD"),
                    'compare' => '>',
                    'type' => 'NUMERIC'
                );
                $max_price = array(
                    'key' => '_cs_adultprice',
                    'value' => intval($price_range[1]) / get_option("_cs_currency_USD"),
                    'compare' => '<',
                    'type' => 'NUMERIC'
                );
            }

            $query->set('posts_per_page', 10);
            $query->set('meta_query', array(
                'relation' => 'AND',
                $destination,
                $trip_type,
                $days,
                $min_price,
                $max_price
            ));
        }
    }

    return $query;
}

add_filter('posts_where', 'new_my_posts_where');

function new_my_posts_where($where) {
    if (is_search()) {
        if (is_tag(219)) {
            $where = str_replace("meta_key = 'trip_type_destination_", "meta_key LIKE 'trip_type_destination_", $where);
            //var_dump($where);
        }
    }
    return $where;
}

function get_tour_page_max_price() {
    global $wpdb;
    $query = "SELECT wp_posts.ID, wp_postmeta.meta_key, CAST(`wp_postmeta`.`meta_value` as SIGNED) as price FROM wp_posts INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id ) WHERE 1=1 AND ( wp_term_relationships.term_taxonomy_id IN (219) ) AND ( ( wp_postmeta.meta_key = '_cs_adultprice' AND wp_postmeta.meta_value != '' ) ) AND wp_posts.post_type = 'page' AND (wp_posts.post_status = 'publish') GROUP BY wp_posts.ID, wp_postmeta.meta_key, wp_postmeta.meta_value ORDER BY price DESC LIMIT 1;";
    $result = $wpdb->get_row($query);
    if (isset($result->price)) {
        $price = $result->price * get_option("_cs_currency_USD");
        return round($price);
    } else {
        return 0;
    }
}

function get_tour_page_min_price() {
    global $wpdb;
    $query = "SELECT wp_posts.ID, wp_postmeta.meta_key, CAST(`wp_postmeta`.`meta_value` as SIGNED) as price FROM wp_posts INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id ) WHERE 1=1 AND ( wp_term_relationships.term_taxonomy_id IN (219) ) AND ( ( wp_postmeta.meta_key = '_cs_adultprice' AND wp_postmeta.meta_value != '' ) ) AND wp_posts.post_type = 'page' AND (wp_posts.post_status = 'publish') GROUP BY wp_posts.ID, wp_postmeta.meta_key, wp_postmeta.meta_value ORDER BY price ASC LIMIT 1;";
    $result = $wpdb->get_row($query);
    if (isset($result->price)) {
        $price = $result->price * get_option("_cs_currency_USD");
        return round($price);
    } else {
        return 0;
    }
}

function search_price_box() {
    $destinations_select = $triptypes_select = $days_select = $javascript = '';
    $destinations = get_field_object('field_5ae2ba823bae3');
    $triptypes = get_field_object('field_5ae2badf3bae4');
    $days = get_field_object('field_5afaad6d3b516');

    if (( isset($destinations['choices']) && sizeof($destinations['choices']) > 0 ) ||
            ( isset($triptypes['choices']) && sizeof($triptypes['choices']) > 0 ) ||
            ( isset($days['choices']) && sizeof($days['choices']) > 0 )) {

        if (isset($destinations['choices']) && sizeof($destinations['choices']) > 0) {
            $destinations_select = '<div class="search_price_box_field"><select id="destinations" name="destination">';
            $destinations_select .= "<option value=\"\">Destinations</option>";
            foreach ($destinations['choices'] as $key => $value) { {
                    $destinations_select .= "<option value=\"$value\">$value</option>";
                }
            }
            $destinations_select .= '</select></div>';
        }

        if (isset($triptypes['choices']) && sizeof($triptypes['choices']) > 0) {
            $triptypes_select = '<div class="search_price_box_field"><select id="trip-type" name="trip-type">';
            $triptypes_select .= "<option value=\"\">Types</option>";
            foreach ($triptypes['choices'] as $key => $value) { {
                    $triptypes_select .= "<option value=\"$value\">$value</option>";
                }
            }
            $triptypes_select .= '</select></div>';
        }

        if (isset($days['choices']) && sizeof($days['choices']) > 0) {
            $days_select = '<div class="search_price_box_field"><select id="days" name="days">';
            $days_select .= "<option value=\"\">Days</option>";
            foreach ($days['choices'] as $key => $value) { {
                    $days_select .= "<option value=\"$value\">$value</option>";
                }
            }
            $days_select .= '</select></div>';
        }
    }

    $result = $destinations_select . $triptypes_select . $days_select;

    $form = '<form id="destinations_trip_types1" target="_blank" method="get" action="/test-box/tag/tour-page/" >
            <p id="heading">Discover the perfect journey</p>
            <input type="hidden" name="s" value="" />
            ' . $result . '
            <div class="search_price_box_field price-range"><input type="hidden" class="search-box-range-input" name="search-box-range" value="' . get_tour_page_min_price() . ', ' . get_tour_page_max_price() . '" /></div>
            <input type="submit" value="Search" />
        </form>';

    $javascript = '<script type="text/javascript">
        jQ(".search-box-range-input").jRange({
            from: ' . get_tour_page_min_price() . ',
            to: ' . get_tour_page_max_price() . ',
            step: 1,
            format: "$%s",
            width: 300,
            showLabels: true,
            isRange : true,
            theme : "theme-blue",
            snap: true
        });
        </script>';
    $baidu_map = '';

    return $form . $javascript;
}

// added by me for non tour search box

function search_nontour_box() {
    $location_select = $activity_select = $activity_type = $javascript = '';
    $locations = get_field_object('field_5afffcbe21269');
    $activitytypes = get_field_object('field_5afffe552126a');
    $activity = get_field_object('field_5affffe22126e');
    $vehicles = get_field_object('field_5b0000402126f');
    $restaurants = get_field_object('field_5b0000a821270');


    if (( isset($locations['choices']) && sizeof($locations['choices']) > 0 ) ||
            ( isset($activitytypes['choices']) && sizeof($activitytypes['choices']) > 0 ) ||
            ( isset($activity['choices']) && sizeof($activity['choices']) > 0 )) {

        if (isset($locations['choices']) && sizeof($locations['choices']) > 0) {
            $location_select = '<div class="search_price_box_field"><select id="destinations" name="destination">';
            $location_select .= "<option value=\"\">Locations</option>";
            foreach ($locations['choices'] as $key => $value) { {
                    $location_select .= "<option value=\"$value\">$value</option>";
                }
            }
            $location_select .= '</select></div>';
        }

        if (isset($activitytypes['choices']) && sizeof($activitytypes['choices']) > 0) {


            $activity_select = '<div class="search_price_box_field"><select id="trips-type" name="trip-type" onchange="myFun(this)">';
            $activity_select .= "<option value=\"\">Activities/Vehicles/Restaurants</option>";
            foreach ($activitytypes['choices'] as $key => $value) { {
                    $activity_select .= "<option value=\"$value\">$value</option>";
                }
            }
            $activity_select .= '</select></div>';
        }
        ?>

        <script type="text/javascript">
            var countryLists = new Array(5)
            countryLists["empty"] = ["Activities/Vehicles/Restaurants"];
            countryLists["Activities"] = ["Diving", "Watersport undersea", "Reservepark", "SPA"];
            countryLists["Vehicles"] = ["Big Bus(20-40 seaters)", "Small Bus(10-20 seaters)", "Cars 4 seaters"];
            countryLists["Restaurants"] = ["Sea Food", "Afternoon Tea", "Western Restaurants", "Indonesian Restaurants", "Eastern Restaurants"];


            function myFun(selectObj) {
                var idx = selectObj.selectedIndex;
                var which = selectObj.options[idx].value;
                cList = countryLists[which];
                var cSelect = document.getElementById("days2");
                var len = cSelect.options.length;
                while (cSelect.options.length > 0) {
                    cSelect.remove(0);
                }
                var newOption;
                for (var i = 0; i < cList.length; i++) {
                    newOption = document.createElement("option");
                    newOption.value = cList[i];  // assumes option string and value are the same 
                    newOption.text = cList[i];
                    try {
                        cSelect.add(newOption);  // this will fail in DOM browsers but is needed for IE 
                    } catch (e) {
                        cSelect.appendChild(newOption);
                    }
                }

            }


        </script>


        <?php
        if (isset($activity['choices']) && sizeof($activity['choices']) > 0) {
            $activity_type = '<div class="search_price_box_field"><select id="days2" name="days2">';
            $activity_type .= "<option value=\"\">Select activity</option>";
            /* foreach( $activity['choices'] as $key => $value )
              {
              {
              $activity_type .= "<option value=\"$value\">$value</option>";
              }
              } */
            $activity_type .= '</select></div>';
        }
    }


    $result = $location_select . $activity_select . $activity_type;

    $form2 = '<form id="destinations_trip_types" method="get" action="/test-box/tag/tour-page/">
            <p id="heading">Vehicles/Restaurants/SPA</p>
            <input type="hidden" name="s" value="" />
            ' . $result . '
            <div class="search_price_box_field price-range"><input type="hidden" class="search-box-range-input" name="search-box-range" value="' . get_tour_page_min_price() . ', ' . get_tour_page_max_price() . '" /></div>
            <input type="submit" value="Search" />
        </form>';




    $baidu_map = '';


    return $form2;
}

//$budget_used = $wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m')");
//if (floatval($budget_used) < floatval(get_option('budget_limit'))) {
$display_referal = false;
if(is_user_logged_in() && get_user_meta(wp_get_current_user()->ID, 'can_referral', true)==1) {
    $referral_budget_used=$wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND type='invite'");
    if(floatval($referral_budget_used) < floatval(get_option('referral_budget_limit')))
        $display_referal = true;
}

//silky code for custom tabs on Um user databse page
//ä¸‹ï¿½?ï¿½å‡ ä¸ªåŠŸèƒ½æ³¨é‡Šäº†æŽ‰äº†ï¼Œä¿®æ”¹è¿‡çš„ä»£ï¿½?æ”¾åœ¨æ–°æ–‡ä»¶ä¸­ï¼Œæ–¹ä¾¿æ‚¨åˆ‡å›žåŽŸï¿½?ï¿½çš„ä»£ï¿½?ã€‚
include __DIR__ . '/inc/user-profile.php';
///* add a custom tab to show user pages for Client role */
//add_filter('um_profile_tabs', 'add_profile_tabs', 1000 );
//function add_profile_tabs( $tabs ) {
//  global $wpdb;
//  //global $display_referal;
//  $tabs['pages'] = array(
//      'name' => 'Tour Booking Codes Client',
//      'icon' => 'um-faicon-pencil',
//      'custom' => true
//  );
//  //if( $display_referal )
//  {
//      $tabs['referal'] = array(
//          'name' => 'Referal',
//          'icon' => 'um-faicon-share',
//          'custom' => true
//      );
//  }
//  if( is_user_logged_in() && wp_get_current_user()->ID==um_profile_id() )
//  {
//      $tabs['settlement'] = array(
//          'name' => 'Settlement',
//          'icon' => 'um-faicon-money',
//          'custom' => true
//      );
//  }
//  return $tabs;
//}
//
//
//       /* Tell the tab what to display */
//add_action('um_profile_content_pages_default', 'um_profile_content_pages_default');
//function um_profile_content_pages_default( $args ) {
//
//  $profile_id = um_profile_id();
//  global $wpdb;
// // $profile_id = 596; // delete this , just for testing
//
//
//    $query = "SELECT * from `finalitinerary` where id='" . $profile_id . "' group by bookingcode";
//    $result = $wpdb->get_results($query);
//
//    echo '<table class="table" border = "1">';
//  echo '<tr>';
//  echo '<th> Booking Code </th>';
//  echo '<th> Nominal Account </th>';
//  echo '<th> Agent Name </th>';
//  echo '<th> Feedback </th>';
//  echo '</tr>';
//
//   foreach ($result as $singlepost) {
//          // echo '<a href="https://www.travpart.com/English/tour-details/?bookingusercode=' . $singlepost->bookingcode . '" target="_blank"><p>' .$singlepost->bookingcode. '</p></a>';
/*  ?>
  <!--      <tr>-->
  <!--      <td> --><?php //echo '<a href="https://www.travpart.com/English/tour-details/?bookingusercode=' . $singlepost->bookingcode . '" target="_blank"><p>' .$singlepost->bookingcode. '</p></a>' ?><!-- </td>-->
  <!--      <td> --><?php //echo $singlepost->totalpricewithdiscount ?><!-- </td>-->
  <!--      <td> --><?php //echo $singlepost->userid ?><!-- </td>-->
  <!--      <td> Rating </td>-->
  <!--  </tr>-->
  <!---->
  <!--  --><?php
  //        }
  / */
//  echo '</table>';
//
//  echo '<a href="https://www.travpart.com/English/travcust/" target="_blank" class="btn btn-default left-btnapp">Add a Tour</a>';
//
//  echo '<a href="https://www.travpart.com/English/logout/" class="btn btn-default right-btnapp">Logout</a>';
//
//
//  echo '<style>.btn.btn-default.left-btnapp {
//    float: left;
//    margin-top: 30px;
//}
//.btn.btn-default.right-btnapp {
//    float: right;
//    margin-top: 30px;
//}
//.um-14065.um .um-profile-body {
//    max-width: 100% !important;
//}
//.um-profile a, .um-profile a:hover {
//    text-decoration: none !important;
//    text-align: center;
//}
//.um-profile a p {
//    font-size: 14px;
//    font-weight: 400;
//}
//.um-14065.um .um-profile-body {
//    max-width: 600px;
//    border: 1px solid #ccc;
//}
//
//</style>';
//}
//
//add_action('um_profile_content_referal_default', 'um_profile_content_referal');
//function um_profile_content_referal($args)
//{
//  global $wpdb;
//  global $display_referal;
//  $profile_id=um_profile_id();
//  $username=$wpdb->get_row("SELECT * FROM `wp_users` WHERE `ID` = {$profile_id}")->user_login;
//  $success_count=$wpdb->get_var("SELECT COUNT(*) FROM `rewards`,`user` WHERE rewards.userid=user.userid AND type='invite' AND user.username='{$username}'");
//
//    $bar_class = "bar1";
//    if($success_count>=50) {
//        $bar_class = "bar4";
//    } else if ($success_count>=20) {
//        $bar_class ="bar3";
//    } else if($success_count>=5) {
//        $bar_class = "bar1";
//    }
//  $share_url = "https://www.travpart.com/English/travchat/signup.php?code=$username";
//    $encode_share_url = urlencode($share_url);
//
//    echo '<div class="img-wrap"><img src="https://www.travpart.com/English/wp-content/uploads/2018/10/user-profiletab-referal-head-en.jpg" alt=""></div>
//<div class="profile-content">
//      <h2>Send a $5 credit to your friends, and earn even more for yourself when they join</h2>
//      <p>You currently have ' . $success_count . ' Referals</p>
//      <div class="sf-input-wrap"><input id="sf-input" type="text" value="https://www.travpart.com/English/travchat/signup.php?code=' . $username . '" /><button  data-clipboard-target="#sf-input" class="btn btn-primary sf-btn">Copy</button></div>
//
//      <p>share it on Facebook, Twitter, Google+, Pinterest, Whatsapp</p>
//      <div class="share-sf-input-wrap">
//      <input style="text-align: left;" id="share-sf-input" type="text" value="' . $share_url . '" />
//      <div class="share-buttons">
//          <a class="share-facebook" href="http://www.facebook.com/sharer/sharer.php?u='.$encode_share_url.'" target="_blank"><i class="fa fa-facebook"></i> </a>
//          <a class="share-twitter" href="http://twitter.com/share?url='.$encode_share_url.'&text='.urlencode('I recommend you use the travcust app for making money to sell tour packages. Download and get $5 credit.').'" target="_blank"><i class="fa fa-twitter"></i> </a>
//          <a href="#" style="background: #cb2027;" onclick="gotoshare(\'pinterest\')"><i class="fa fa-pinterest"></i></a>
//          <a href="#" class="share-google-plus" onclick="gotoshare(\'google.plus\')"><i class="fa fa-google-plus"></i> </a>
//          <a href="#" style="background: #00e676;" onclick="gotoshare(\'whatsapp\')"><i class="fa fa-whatsapp"></i></a>
//        </div>
//      </div>
//
//      <div class="ref-progress-bar">
//          <div class="bar"><div class="bar-inner '.$bar_class.'"></div></div>
//          <div class="desc">
//              <div class="d1"><h5>Join</h5></div>
//              <div class="d2">
//                  <h5>Supporter</h5>
//                  <p>5  Referrals<br>Get $10</p>
//              </div>
//              <div class="d3">
//                  <h5>Ambassador</h5>
//                  <p>20  Referrals <br>Get $10</p>
//              </div>
//              <div class="d4">
//                  <h5>VIP</h5>
//                  <p>50  Referrals <br> $200 Creative Bunds</p>
//                  <p><small>50  Referrals $200 Creative Bunds</small></p>
//              </div>
//            </div>
//        </div>
//      </div>
//  ';
//  if(!$display_referal)
//      echo '<p style="background: yellow; color: red; font-size: 18px; text-align: center;">The campaign is currently unavailable!</p>';
//
//  echo '<link rel="stylesheet" href="/English/wp-content/themes/bali/css/social-share-media.css">
//      <script src="/English/wp-content/themes/bali/js/social-share-media.js"></script>
//<script>
//const socialmediaurls = GetSocialMediaSiteLinks_WithShareLinks({
//  "url":"' . $share_url . '",
//  "title": "I recommend you use the travcust app for making money to sell tour packages. Download and get $5 credit."
//});
//function gotoshare(medianame)
//{
//  if(medianame=="whatsapp") {
//      window.open("https://web.whatsapp.com/send?text="+encodeURIComponent("I recommend you use the travcust app for making money to sell tour packages. Download ' . $share_url . ' and get $5 credit."));
//  }
//  else {
//      window.open(socialmediaurls[medianame]);
//  }
//}
//</script>';
//}
//
//add_action('um_profile_content_settlement_default', 'um_profile_content_settlement');
//function um_profile_content_settlement($args)
//{
//    global $wpdb;
//    $withdraw_log_limit = 5;
//    $current_user = wp_get_current_user();
//  $username = $current_user->user_login;
//    $action = htmlspecialchars($_GET['action']);
//
//    if (empty($action) || $action == 'default') {
//
//        $balance = $wpdb->get_var("SELECT balance FROM `balance`,`user` WHERE balance.userid=user.userid AND user.username='{$username}'");
//        if (empty($balance) || $balance < 0)
//            $balance = '0.00';
//        echo '<p>Available: $' . $balance . '</p>';
//        echo '<p><button>Get Paid Now</button>   <a href="' . home_url("/user/{$username}/?profiletab=settlement&action=add_settlement_method") . '">Add settlement method</a></p>';
//        echo '<div><b>Recent transactions</b></div><hr/>';
//        echo '<table>
//          <tr> <th>Date</th> <th>Type</th> <th>Amount</th> <th>Balance</th> </tr>';
//        if ($balance > 0) {
//            $withdraw_log = $wpdb->get_results("SELECT withdraw_log.* FROM `withdraw_log`,`user` WHERE withdraw_log.userid=user.userid AND user.username='{$username}' ORDER BY `withdraw_log`.`wdate` DESC LIMIT {$withdraw_log_limit}");
//            if (count($withdraw_log) > 0) {
//                foreach ($withdraw_log as $log_item) {
//                    echo "<tr> <td>{$log_item->wdate}</td> <td>{$log_item->type}</td> <td>\${$log_item->amount}</td> <td>\${$log_item->balance}</td>  </tr>";
//                }
//            } else {
//                echo '<tr> <td colspan="3">You don\'t have any transactions.</td> </tr>';
//            }
//        } else {
//            echo '<tr> <td colspan="3">You don\'t have any transactions.</td> </tr>';
//        }
//        echo '</table>';
//        echo '<p><a href="' . home_url("/user/{$username}/?profiletab=settlement&action=show_log") . '">View all transactions ></a>';
//
//    } else if ($action == 'show_log') {
//        if (empty($_GET['pp']) || intval($_GET['pp']) < 1)
//            $page = 1;
//        else
//            $page = intval($_GET['pp']);
//        $number_per_page = 10;
//        $total = $wpdb->get_var("SELECT COUNT(*) FROM `withdraw_log`,`user` WHERE withdraw_log.userid=user.userid AND user.username='{$username}'");
//        $total_page = ceil($total / $number_per_page);
//
//        if ($page > $total_page)
//            $page = $total_page;
//
//        echo '<h4>Transactions</h4><hr/>';
//        echo '<table>
//              <tr> <th>Date</th> <th>Type</th> <th>Amount</th> <th>Balance</th> </tr>';
//        if ($total > 0) {
//            $withdraw_log = $wpdb->get_results("SELECT withdraw_log.* FROM `withdraw_log`,`user` WHERE withdraw_log.userid=user.userid AND user.username='{$username}' ORDER BY `withdraw_log`.`wdate` DESC LIMIT " . ($page - 1) . ",{$number_per_page}");
//            foreach ($withdraw_log as $log_item) {
//                echo "<tr> <td>{$log_item->wdate}</td> <td>{$log_item->type}</td> <td>\${$log_item->amount}</td> <td>\${$log_item->balance}</td>  </tr>";
//            }
//        } else {
//            echo '<tr> <td colspan="3">You don\'t have any transactions.</td> </tr>';
//        }
//        echo '</table>';
//
//        if ($total_page > 1) {
//            echo '<p>
//              ' . (($page > 1) ? ('<a href="' . home_url("/user/{$username}/?profiletab=settlement&action=show_log&pp=" . ($page - 1)) . '">< Last</a>') : '') . '
//              ' . (($page < $total_page) ? ('<a href="' . home_url("/user/{$username}/?profiletab=settlement&action=show_log&pp=" . ($page + 1)) . '">Next ></a>') : '') . '
//          </p>';
//        } else {
//            echo '<p>End~</p>';
//        }
//    } else if ($action == 'add_settlement_method') {
//      echo '<h4>Add Settlement Method</h4><hr/><br>';
//      if(!empty($_POST['confirm_password'])) {
//          if( wp_check_password($_POST['confirm_password'], $current_user->user_pass, $current_user->ID) ) {
//              $saved_status=false;
//              if(!empty($_POST['paypal'])) {
//                  if( update_user_meta($current_user->ID, 'paypal_payment', htmlspecialchars($_POST['paypal'])) )
//                      $saved_status=true;
//              }
//              if(!empty($_POST['bank_swift_code']) && !empty($_POST['account_number']) &&
//                 !empty($_POST['account_name']) && !empty($_POST['account_address']) &&
//                 !empty($_POST['account_city']) && !empty($_POST['account_country']) &&
//                 !empty($_POST['account_phone_number']) ) {
//                  $bank_payment=array('bank_swift_code'=>htmlspecialchars($_POST['bank_swift_code']),
//                                      'account_number'=>htmlspecialchars($_POST['account_number']),
//                                      'account_name'=>htmlspecialchars($_POST['account_name']),
//                                      'account_address'=>htmlspecialchars($_POST['account_address']),
//                                      'account_city'=>htmlspecialchars($_POST['account_city']),
//                                      'account_country'=>htmlspecialchars($_POST['account_country']),
//                                      'account_phone_number'=>htmlspecialchars($_POST['account_phone_number'])
//                                  );
//                  if( update_user_meta($current_user->ID, 'bank_payment', $bank_payment) )
//                      $saved_status=true;
//              }
//              if($saved_status) {
//                  echo '<p style="font-size: 18px; color: green;"><b>Successfully saved!</b></p>';
//              }
//          } else {
//              echo '<p style="font-size: 18px; color: red;"><b>Authentication failed, please check your password!</b></p>';
//          }
//      }
//
//      $bank_payment=get_user_meta($current_user->ID, 'bank_payment', true);
//
//      echo '<form action="' . home_url("/user/{$username}/?profiletab=settlement&action=add_settlement_method") . '" method="POST" >
//            <p style="font-size: 17px"><b>PayPal</b></p>
//            <input name="paypal" type="text" value="' . (empty(get_user_meta($current_user->ID, 'paypal_payment', true))? (empty($_POST['paypal'])?'':htmlspecialchars($_POST['paypal'])) : get_user_meta($current_user->ID, 'paypal_payment', true)) . '" /><br/>
//            <p style="font-size: 17px"><b>Wire transfer</b></p>
//            <p><span>Bank SWIFT Code</span> <input name="bank_swift_code" style="width: 45%; display: inline !important; margin-left:40px !important;" type="text" value="' . (empty($bank_payment)?'':($bank_payment['bank_swift_code'])) . '" /></p>
//            <p><span>Account Number</span> <input name="account_number" style="width: 45%; display: inline !important; margin-left:53px !important;" type="text" value="' . (empty($bank_payment)?'':($bank_payment['account_number'])) . '" /></p>';
//      if(!empty($_POST['bank_swift_code']) && empty($_POST['account_number']) )
//          echo '<p style="color: red;">*Please input the Bank SWIFT Code</p>';
//
//      echo  '<p><span>Name on Account</span> <input name="account_name" style="width: 45%; display: inline !important; margin-left:46px !important;" type="text" value="' . (empty($bank_payment)?'':($bank_payment['account_name'])) . '" /></p>';
//      if(!empty($_POST['bank_swift_code']) && empty($_POST['account_name']) )
//          echo '<p style="color: red;">*Please input the Name on Account</p>';
//
//      echo  '<p><span>Address</span> <input name="account_address" style="width: 45%; display: inline !important; margin-left:109px !important;" type="text" value="' . (empty($bank_payment)?'':($bank_payment['account_address'])) . '" /></p>';
//      if(!empty($_POST['bank_swift_code']) && empty($_POST['account_address']) )
//          echo '<p style="color: red;">*Please input the Address</p>';
//
//      echo  '<p><span>City and State/Province</span> <input name="account_city" style="width: 45%; display: inline !important; margin-left:8px !important;" type="text" value="' . (empty($bank_payment)?'':($bank_payment['account_city'])) . '" /></p>';
//      if(!empty($_POST['bank_swift_code']) && empty($_POST['account_city']) )
//          echo '<p style="color: red;">*Please input the City and State/Province</p>';
//
//      echo  '<p><span>Country<span> <input name="account_country" style="width: 45%; display: inline !important; margin-left:112px !important;" type="text" value="' . (empty($bank_payment)?'':($bank_payment['account_country'])) . '" /></p>';
//      if(!empty($_POST['bank_swift_code']) && empty($_POST['account_country']) )
//          echo '<p style="color: red;">*Please input the Country</p>';
//
//      echo  '<p><span>Phone number<span> <input name="account_phone_number" style="width: 45%; display: inline !important; margin-left:65px !important;" type="text" value="' . (empty($bank_payment)?'':($bank_payment['account_phone_number'])) . '" /></p>';
//      if(!empty($_POST['bank_swift_code']) && empty($_POST['account_phone_number']) )
//          echo '<p style="color: red;">*Please input the Phone number</p>';
//      //<!--input name="bank_account" type="text" value="' . (empty(get_user_meta($current_user->ID, 'bank_payment', true))? (empty($_POST['bank_account'])?'':htmlspecialchars($_POST['bank_account'])) : get_user_meta($current_user->ID, 'bank_payment', true)) . '" /--><br/>
//      echo  '<p style="font-size: 17px"><b>Confirm Password</b></p>
//            <input name="confirm_password" type="password" />';
//      if(!empty($_POST['save']) && empty($_POST['confirm_password']) )
//          echo '<p style="color: red;">*Please input your password</p>';
//      echo '<br/><input name="save" type="submit" value="Save" style="font-size: 16px" />
//            </form>';
//  }
//}


add_action('wp_enqueue_scripts', 'clipboard_scripts');

function clipboard_scripts() {
    wp_enqueue_script("clipboard@2", "https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js");
}

//silky code


function person_price_ration_init() {
    add_settings_field('person_price_ration', 'Adult and children,travcust vehicles,meals,guide,spa and activities price ration', 'person_price_ration_callback', 'general');
    register_setting('general', 'person_price_ration');
}

function person_price_ration_callback() {
    echo '<input name="person_price_ration" type="text" id="person_price_ration" value="' . esc_attr(get_option('person_price_ration')) . '" class="regluar-text ltr" />';
    echo '<p class="description" id="ration-description">The price ration work for adult and children ,travcust vehicles,meals,guide,spa and activities price in tour page. The final price = current adult and children price * price ration </p>';
}

add_action('admin_init', 'person_price_ration_init');


/* This Code causes HTTP 500 Error */
/* add_action( 'wp_enqueue_scripts', 'clipboard_scripts' ); 
  function clipboard_scripts() {
  wp_enqueue_script("clipboard@2","https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js");
  wp_enqueue_script("qrcode@1","http://cdn.staticfile.org/jquery.qrcode/1.0/jquery.qrcode.min.js");
  } */

/*
add_filter('the_content', 'backtoformcustom_html');

function backtoformcustom_html($content) {
    global $wpdb;
    if (in_the_loop() && is_main_query()) {
        if (!empty($_COOKIE['tour_id']))
            $tour_id = $_COOKIE['tour_id'];
        else
            $tour_id = '';
        $backtoformcustom_html = '
        <a href="' . site_url() . '/travcust" class="btn btn-primary firstbackbtn mb-2"><span>Modify my current selected tour package</span> </a>
        <a href="' . (empty($tour_id) ? '#' : (site_url() . '/tour-details/?bookingcode=BALI' . $tour_id)) . '" ' . (empty($tour_id) ? 'data-toggle="tooltip" title="You have not added anything yet"' : '') . ' class="btn btn-primary firstbackbtn mb-2"><span>' . (empty($tour_id) ? '' : ("(Booking Code BALI{$tour_id})")) . ' Check my journey</span><i class="fa fa-angle-right" style="color:#fff;margin-left:15px;"></i> </a>
        <a href="' . get_template_directory_uri() . '/newtour.php" title="Create a new tour package" class="btn btn-primary firstbackbtn mb-2"><img src="https://www.travpart.com/English/wp-content/uploads/2018/11/add-list.png" alt="Create a new tour package" width="18"> <span>Create a new tour package</span></a>';

        $show_top_button = false;
        if (is_user_logged_in()) {
            $current_user = wp_get_current_user();
            $urow = $wpdb->get_row("SELECT access FROM `user` WHERE `username`='{$current_user->user_login}'");
            if ((!empty($urow->access) && $urow->access == 1) || current_user_can('um_sales-agent'))
                $show_top_button = true;
        }

        if ($show_top_button)
            $content = str_replace('[backtoformcustom]', $backtoformcustom_html, $content);
        else
            $content = str_replace('[backtoformcustom]', ' ', $content);
    }
    return $content;
}
*/

add_filter('the_content', 'meal_and_car_html');

function meal_and_car_html($content) {
    if (in_the_loop() && is_main_query()) {
        /* Get the content of meal */
        $meal_list_html = '';
        $TravcustMeal = unserialize(get_option('_cs_travcust_meal'));
        if (!empty($TravcustMeal)) {
            $i = 0;
            foreach ($TravcustMeal as $t) {
                //$meal_list_html.=($i>0 && $i%2==0)?'<div style="clear:both;"></div>':'';
                if ($i == 0)
                    $meal_list_html .= '<div class="item active">';
                else
                    $meal_list_html .= ($i % 2 == 0) ? '<div class="item">' : '';
                $meal_list_html .= '<div class="col-sm-6"><div class="meal-block">
              <div class="meal-block-header" style="background-image: url(\'' . $t['img'] . '\')"><div class="header-inner">            
            <input class="meal_selected" type="hidden" value="0" />
            <p class="meal-name">' . $t['item'] . '</p>
            <p class="meal-area" style="' . (empty($t['area']) ? 'display:none;' : '') . '">' . $t['area'] . '</p></div>
            </div>';
                if (!empty($t['subitem'])) {
                    $meal_list_html .= '<div item="' . $i . '" class="meal-sub-item"><div class="meal-sub-item-head"><span class="i-meals">Meals</span><span class="i-break-fast">Breakfast</span> <span class="i-beverage">Beverage</span><span class="i-appetizer">Appetizer</span> </div><div class="meal-sub-item-content">';
                    $empty_value = array();
                    foreach ($t['subitem'] as $st) {
                        $meal_list_html .= '<div data-type="' . $st['type'] . '"  data-img="' . $st['img'] . '"  data-name="' . $st['name'] . '" data-price="' . round($st['price'] * get_option('person_price_ration')) . '" class="' . $st['type'] . '" style="display:none;"><div><input type="checkbox" /></div><div><img  src="' . $st['img'] . '" /></div><div>' . $st['name'] . '</div><div><span class="change_price" or_pic="' . round($st['price'] * get_option('person_price_ration')) . '">Rp<span class="price_span">' . round($st['price'] * get_option('person_price_ration')) . '</span></span></div></div>';
                        $empty_value[] = $st['type'];
                    }
                    foreach (array("Meals", "Breakfast", "Beverage", "Appetizer") as $class) {
                        if (!in_array($class, $empty_value)) {
                            $meal_list_html .= '<div class="' . $class . ' no-result" style="display:none;"><div>No menu can be found</div></div>';
                        }
                    }
                    $meal_list_html .= '</div>';
                    $meal_list_html .= '</div>';
                }

                $meal_list_html .= '</div></div>';
                $meal_list_html .= ($i % 2 == 1) ? '</div>' : '';
                $i++;
            }
            $meal_list_html .= ($i % 2 > 0) ? '</div>' : '';
        }
        /* Get the content of car */

        $car_list_html = '';
        $TravcustCar = unserialize(get_option('_cs_travcust_car'));
        if (!empty($TravcustCar)) {
            $i = 0;
            foreach ($TravcustCar as $t) {
                ob_start();
                $half_price = round($t['price'] * get_option('person_price_ration')) / 2;
                if ($i == 0)
                    $car_list_html .= '<div class="item active">';
                else
                    $car_list_html .= ($i % 2 == 0) ? '<div class="item">' : '';
                $car_list_html .= '<div class="col-sm-6">
              <div class="car-block">
            <div class="car-block-top"> <div class="carimg-wrap"> <img class="carimg carhover" src="' . $t['img'] . '"  /></div>
            <input class="car_selected" type="hidden" value="0" />
            <p class="delivery-name">' . $t['item'] . '</p></div>
            <div class="delivery-detail">
            <div class="delivery-left">
            <p class="delivery-area ' . (empty($t['area']) ? ' hide' : '') . '">' . $t['area'] . '</p>
            <div class="delivery-price ' . (empty($t['price']) ? ' hide' : '') . '">
            <div class="change_price car_full_price" or_pic="' . round($t['price'] * get_option('person_price_ration')) . '">Rp<span class="price_span">' . round($t['price'] * get_option('person_price_ration')) . '</span></div></div>
            </div>
            <div class="delivery-right">
            <p class="delivery-area-2"> Half day or Airport Transfer </p>
            <p class="deliver-price' . (empty($t['price']) ? ' hide' : '') . '">
            <div class="change_price car_half_price" or_pic="' . $half_price . '">Rp<span class="price_span">' . $half_price . '</span></div></p>
            </div>
            </div>
            </div>';
                $car_list_html .= '</div>';
                $car_list_html .= ($i % 2 == 1) ? '</div>' : '';
                $i++;
            }
            $car_list_html .= ($i % 2 > 0) ? '</div>' : '';
        }
        /* Get the content of popupguide */
        $popupguide_list_html = '';
        $TravcustPopupguide = unserialize(get_option('_cs_travcust_popupguide'));
        if (!empty($TravcustPopupguide)) {
            $i = 0;
            foreach ($TravcustPopupguide as $t) {
                ob_start();

                if ($i == 0)
                    $popupguide_list_html .= '<div class="item active">';
                else
                    $popupguide_list_html .= ($i % 2 == 0) ? '<div class="item">' : '';
                $popupguide_list_html .= '<div class="col-sm-6">
              <div class="popupgui-block">
              <div class="popupgui-block-top">
            <img class="" src="' . $t['img'] . '"  />
            <input class="popupguide_selected" type="hidden" value="0" />
            <div class="popupguide-name">' . $t['item'] . '</div>
            </div>
            <div class="popupgui-bottom">
            <div>
            <p class="area" style="' . (empty($t['area']) ? 'display:none;' : '') . '">' . $t['area'] . '</p>
            <p class="price" style="' . (empty($t['price']) ? 'display:none;' : '') . '">
            <span class="change_price" or_pic="' . round($t['price'] * get_option('person_price_ration')) . '">Rp<span class="price_span">' . round($t['price'] * get_option('person_price_ration')) . '</span></span></p>
            </div>
            </div>
            </div>';
                $popupguide_list_html .= '</div>';
                $popupguide_list_html .= ($i % 2 == 1) ? '</div>' : '';
                $i++;
                echo ob_get_contents();
                ob_end_flush();
            }
            $popupguide_list_html .= ($i % 2 > 0) ? '</div>' : '';
        }
        /* Get the content of spa */
        $spa_list_html = '';
        $TravcustSpa = unserialize(get_option('_cs_travcust_spa'));
        if (!empty($TravcustSpa)) {
            /*
              foreach($TravcustSpa as $t)
              {
              ob_start();

              $spa_list_html.='<li>
              <img class="spaimg carhover" src="'.$t['img'].'" style="width:250px!important;opacity:0.7;" />
              <input class="spa_selected" type="hidden" value="0" />
              <p style="margin-left:3%">'.$t['item'].'</p>
              <p>'.$t['area'].'</p>';
              if(!empty($t['subitem']))
              $spa_list_html.='<table class="spa-subitem">';
              foreach($t['subitem'] as $st)
              {
              $spa_list_html.='<tr><td><input type="checkbox" /></td><td>'.$st['name'].'</td><td><span class="change_price" or_pic="'.round($st['price']*get_option('person_price_ration')).'">Rp<span class="price_span">'.round($st['price']*get_option('person_price_ration')).'</span></span></td><td><img width=64 height=64 src="'.$st['img'].'" /></td></tr>';
              }
              if(!empty($t['subitem']))
              $spa_list_html.='</table>';
              $spa_list_html.='</li>';
              echo ob_get_contents();
              ob_end_flush();
              }
             */
            $i = 0;
            $itempoint = 1;
            foreach ($TravcustSpa as $t) {

                foreach ($t['subitem'] as $st) {

                    if ($i == 0)
                        $spa_list_html .= '<div class="item active">';
                    else
                        $spa_list_html .= ($i % 6 == 0) ? '<div class="item">' : '';
                    $spa_list_html .= '<div class="col-sm-4"><div class="spa-block">
<div class="spa-block-top" style="background-image: url(' . $st['img'] . ')"><div class="c-checkbox"><input item="' . $itempoint . '" type="checkbox"  /><label></label></div></div> 
<div class="spa-block-bottom"><div class="spa-name">' . $st['name'] . '</div>
<div class="spa-price"><span class="change_price" or_pic="' . round($st['price'] * get_option('person_price_ration')) . '">Rp<span class="price_span">' . round($st['price'] * get_option('person_price_ration')) . '</span></span></div> 
</div>
</div></div>';
                    $spa_list_html .= ($i % 6 == 5) ? '</div>' : '';
                    $i++;
                }
                $itempoint++;
            }
            $spa_list_html .= ($i % 6 > 0) ? '</div>' : '';
        }
        /* Get the content of boats and helicopter */
        $boat_list_html = '';
        $TravcustBoat = unserialize(get_option('_cs_travcust_boat'));
        if (!empty($TravcustBoat)) {
            /*  foreach($TravcustBoat as $t)
              {
              ob_start();

              $boat_list_html.='<li>
              <img class="boatimg carhover" src="'.$t['img'].'" style="width:30%;opacity:0.7;" />
              <input class="boat_selected" type="hidden" value="0" />
              <p>'.$t['item'].'</p>
              <p>'.$t['area'].'</p>';
              if(!empty($t['subitem']))
              $boat_list_html.='<table class="boat-subitem">';
              foreach($t['subitem'] as $st)
              {
              $boat_list_html.='<tr><td><input type="checkbox" /></td><td>'.$st['name'].'</td><td><span class="change_price" or_pic="'.round($st['price']*get_option('person_price_ration')).'">Rp<span class="price_span">'.round($st['price']*get_option('person_price_ration')).'</span></span></td><td><img width=64 height=64 src="'.$st['img'].'" /></td></tr>';
              }
              if(!empty($t['subitem']))
              $boat_list_html.='</table>';
              $boat_list_html.='</li>';
              echo ob_get_contents();
              ob_end_flush();
              } */

            $i = 0;
            $itempoint = 1;
            foreach ($TravcustBoat as $t) {

                foreach ($t['subitem'] as $st) {

                    if ($i == 0)
                        $boat_list_html .= '<div class="item active">';
                    else
                        $boat_list_html .= ($i % 6 == 0) ? '<div class="item">' : '';
                    $boat_list_html .= '<div class="col-sm-4"><div class="helicopter-block">
<div class="helicopter-block-top" style="background-image: url(' . $st['img'] . ')"><div class="c-checkbox"><input item="' . $itempoint . '" type="checkbox"  /><label></label></div></div> 
<div class="helicopter-block-bottom"><div class="helicopter-name">' . $st['name'] . '</div>
<div class="helicopter-price"><span class="change_price" or_pic="' . round($st['price'] * get_option('person_price_ration')) . '">Rp<span class="price_span">' . round($st['price'] * get_option('person_price_ration')) . '</span></span></div> 
</div>
</div></div>';
                    $boat_list_html .= ($i % 6 == 5) ? '</div>' : '';
                    $i++;
                }
                $itempoint++;
            }
            $boat_list_html .= ($i % 6 > 0) ? '</div>' : '';
        }

        $content = str_replace(array('[meal_part]', '[car_part]', '[popupguide_part]', '[spa_part]', '[boat_part]'), array($meal_list_html, $car_list_html, $popupguide_list_html, $spa_list_html, $boat_list_html), $content);
    }
    return $content;
}

function my_datepicker_function() {
    //Enqueue date picker UI from WP core:
    wp_enqueue_script('jquery-ui-datepicker');
    //Enqueue the jQuery UI theme css file from google:
    wp_enqueue_style('e2b-admin-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css', false, "1.9.0", false);
}

add_action('wp_enqueue_scripts', 'my_datepicker_function');


// $conn = mysqli_connect("localhost","root","EITJuKIfoeZl","tourfrom_travchat_en");
// if (!$conn) {
//  die("Connection failed: " . mysqli_connect_error());
// }
// working fine
// function runAfterRegisterToStoreDataInUserTable( $user_id )
// {        
//      mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated,photo,phone) values ('"+$row->user_login+"', '"+$row->user_login+"','"+$row->user_email+"', '"+$row->user_pass+"', '"+$row->user_login+"','"+$row->user_login+"','"+$row->user_login+"','"+$row->user_login+"')"); 
// }
// add_action( 'user_register', 'runAfterRegisterToStoreDataInUserTable');
// add_action( 'um_registration_complete', 'my_registration_complete', 10, 2 );
// function my_registration_complete( $user_id, $args ) {
//  print_r($args);
//      mysqli_query($conn, "insert into `user` (uname, username,email,password,access,activated,photo,phone) values ('testtesttest', 'testtesttest','testtesttest@test.com', 'testtesttest', '2','1','','103956')");   
// }


add_filter('the_content', 'meal_and_car_html2');
add_action('wp_ajax_nopriv_meal_and_car_html2', 'meal_and_car_html2');
add_action('wp_ajax_meal_and_car_html2', 'meal_and_car_html2');

//add_action( 'the_content', 'meal_and_car_html2' );


function meal_and_car_html2($content) {



    if (in_the_loop() && is_main_query()) {
        /* Get the content of meal */
        $pagenum = $_POST['pagenum'];
        echo $_POST['numberOfPromos'];
        echo $_POST['pagenum'];
        $limit = 4;
        if ($pagenum == 0) {
            $pagenum = 1;
        }
        $start_from = ($pagenum - 1) * $limit;

        $meal_list_html2 = '';

        //$TravcustMeal=unserialize(get_option('_cs_travcust_meal'));
        $TravcustMeal2 = array();
        $MealId = array();
        global $wpdb;
        $sqlmeal = "SELECT id,_cs_travcust_meal from `travcust_meal_new` ORDER BY id ASC LIMIT $start_from,$limit";
        $resultmeal = $wpdb->get_results($sqlmeal);

        if ($resultmeal != 0) {
            foreach ($resultmeal as $datameal) {
                array_push($TravcustMeal2, unserialize($datameal->_cs_travcust_meal));
                array_push($MealId, $datameal->id);
            }
        }


        if (!empty($TravcustMeal2)) {
            $i = 0;
            foreach ($TravcustMeal2 as $t) {
                //$meal_list_html.=($i>0 && $i%2==0)?'<div style="clear:both;"></div>':'';
                if ($i == 0)
                    $meal_list_html2 .= '<div class="item active">';
                else
                    $meal_list_html2 .= ($i % 2 == 0) ? '<div class="item">' : '';
                $meal_list_html2 .= '<div class="col-sm-6"><div class="meal-block">
              <div class="meal-block-header" style="background-image: url(\'' . $t['img'] . '\')"><div class="header-inner">            
            <input class="meal_selected" type="hidden" value="0" />
            <p class="meal-name">' . $t['item'] . '</p>
            <p class="meal-area" style="' . (empty($t['area']) ? 'display:none;' : '') . '">' . $t['area'] . '</p></div>
            </div>';

                if (!empty($t['subitem'])) {
                    $meal_list_html2 .= '<div item="' . $MealId[$i] . '" class="meal-sub-item"><div class="meal-sub-item-head"><span class="i-meals">Meals</span><span class="i-break-fast">Breakfast</span> <span class="i-beverage">Beverage</span><span class="i-appetizer">Appetizer</span> </div><div class="meal-sub-item-content">';
                    $empty_value = array();
                    foreach ($t['subitem'] as $st) {
                        $hidden_subitem = (empty($st['price']) || round($st['price']) <= 0);
                        $meal_list_html2 .= '<div data-type="' . $st['type'] . '"  data-img="' . $st['img'] . '"  data-name="' . $st['name'] . '" data-price="' . round($st['price'] * get_option('person_price_ration')) . '" class="' . $st['type'] . '" style="display:none' . (($hidden_subitem) ? '!important' : '') . ';"><div><input type="checkbox" /></div><div><img  src="' . $st['img'] . '" /></div><div>' . $st['name'] . '</div><div><span class="change_price" or_pic="' . round($st['price'] * get_option('person_price_ration')) . '">Rp<span class="price_span">' . round($st['price'] * get_option('person_price_ration')) . '</span></span></div></div>';
                        $empty_value[] = $st['type'];
                    }
                    foreach (array("Meals", "Breakfast", "Beverage", "Appetizer") as $class) {
                        if (!in_array($class, $empty_value)) {
                            $meal_list_html2 .= '<div class="' . $class . ' no-result" style="display:none;"><div>No menu can be found</div></div>';
                        }
                    }
                    $meal_list_html2 .= '</div>';
                    $meal_list_html2 .= '</div>';
                }

                $meal_list_html2 .= '</div></div>';
                $meal_list_html2 .= ($i % 2 == 1) ? '</div>' : '';
                $i++;
            }
            $meal_list_html2 .= ($i % 2 > 0) ? '</div>' : '';
        }


        $content = str_replace(array('[meal_part2]'), array($meal_list_html2), $content);
    }
    return $content;
}

add_action('wp_enqueue_scripts', 'add_ajax_javascript_file');

function add_ajax_javascript_file() {
    wp_localize_script('ajax_for_frontend', 'ajax_for_frontend', array('ajaxurl' => admin_url('admin-ajax.php')));
    //wp_enqueue_script('ajax_custom_script', get_stylesheet_directory_uri() . '/js/ajax-javascript.js', array('jquery'));
}

function remove_update_notifications($value) {

    if (isset($value) && is_object($value)) {
        unset($value->response['ultimate-member/ultimate-member.php']);
        unset($value->response['external-login/external-login.php']);
    }

    return $value;
}

add_filter('site_transient_update_plugins', 'remove_update_notifications');

function set_travchat_loginstatus() {
    global $wpdb;
    global $current_user;
    session_start();
    if (is_user_logged_in() && empty($_SESSION['id'])) {
        $row = $wpdb->get_row("select * from `user` where username='{$current_user->user_login}' and activated=1");
        if (!empty($row->userid)) {
            $_SESSION['id'] = $row->userid;
            $_SESSION['access'] = $row->access;
            $_SESSION['user_name'] = $row->uname;
            setcookie('access', $row->access, time() + (86400 * 30), "/");
        }
    }
}

add_action('init', 'set_travchat_loginstatus');



/* custom editor role settings */

function custom_loginlogo() {
    echo '<style type="text/css"> h1 a {background-image: url(' . get_bloginfo('template_directory') . '/images/logo.jpg)!important; height:85px!important; width:355px!important; background-size:355px 85px!important; } </style>';
}

add_action('login_head', 'custom_loginlogo');

add_action('login_headerurl', create_function(false, "return get_bloginfo('url');"));
add_action('login_headertitle', create_function(false, "return get_bloginfo('name');"));

function custom_loginform() {
    echo '<p><input name="AgreeTerms" type="checkbox" id="AgreeTerms" value="agree" checked="checked" /><a href="' . get_bloginfo('url') . '">I agree to the terms and condition</a></p>';
}

add_action('login_form', 'custom_loginform');

function custom_login_redirect($redirect_to, $request, $user) {
    if (is_array($user->roles) && in_array('editor', $user->roles)) {
        return admin_url('edit.php?post_type=blog');
    } else if (is_array($user->roles) && in_array('css_js_designer', $user->roles)) {
        return admin_url('edit.php?post_type=custom-css-js');
    } else
        return admin_url();
}

add_filter('login_redirect', 'custom_login_redirect', 10, 3);

//redirect non-admin role to wp-admin profile page
function restrict_non_admin_with_redirect()
{
    $query_path=add_query_arg();
    $pos=strripos($query_path,'wp-admin');
    if(stripos($query_path,'wp-admin/index.php')!==false || (strlen($query_path)-$pos)<=9) {
        if (is_admin() && !current_user_can('administrator') && (!wp_doing_ajax())) {
            wp_safe_redirect( admin_url('profile.php') );
            exit;
        }
    }
}
add_action( 'init', 'restrict_non_admin_with_redirect', 1);

function custom_admin_menu() {
    remove_menu_page('index.php');
    remove_menu_page('edit-comments.php');
    //remove_menu_page('edit.php');
    remove_menu_page('tools.php');
    remove_menu_page('edit.php?post_type=wcp_carousel');
    remove_menu_page('edit.php?post_type=elementor_library');
    remove_menu_page('wpcf7');
}

if (!current_user_can('administrator'))
    add_action('admin_menu', 'custom_admin_menu', 100);

function custom_page_list($query) {
    if (strpos($_SERVER['REQUEST_URI'], '/wp-admin/edit.php') !== false) {
        if (current_user_can('editor')) {
            global $current_user;
            $query->set('author', $current_user->id);
        }
    }
}

if (current_user_can('editor'))
    add_filter('parse_query', 'custom_page_list');

function custom_thirduser_page_counts($views) {
    $iCurrentUserID = get_current_user_id();
    if ($iCurrentUserID !== 0) {
        global $wpdb;
        foreach ($views as $index => $view) {
            if (in_array($index, array('all', 'publish', 'mine')))
                continue;
            $viewValue = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type='page' AND post_author=$iCurrentUserID AND post_status='$index'");
            $views[$index] = preg_replace('/\(.+\)/U', '(' . $viewValue . ')', $views[$index]);
        }
        unset($views['all']);
        unset($views['publish']);
    }
    return $views;
}

if (current_user_can('editor'))
    add_filter("views_edit-page", 'custom_thirduser_page_counts', 10, 1);

function custom_thirduser_post_counts($views) {
    $iCurrentUserID = get_current_user_id();
    if ($iCurrentUserID !== 0) {
        global $wpdb;
        foreach ($views as $index => $view) {
            if (in_array($index, array('all', 'publish', 'mine')))
                continue;
            $viewValue = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type='post' AND post_author=$iCurrentUserID AND post_status='$index'");
            $views[$index] = preg_replace('/\(.+\)/U', '(' . $viewValue . ')', $views[$index]);
        }
        unset($views['all']);
        unset($views['publish']);
    }
    return $views;
}

if (current_user_can('editor'))
    add_filter("views_edit-post", 'custom_thirduser_post_counts', 10, 1);

function custom_default_page_template() {
    global $post;
    if ('page' == $post->post_type && 0 != count(get_page_templates($post)) && get_option('page_for_posts') != $post->ID // Not the page for listing posts
            && '' == $post->page_template // Only when page_template is not set
    ) {
        $post->page_template = "tpl-tour.php";
    }
}

if (current_user_can('editor'))
    add_action('add_meta_boxes', 'custom_default_page_template', 1);

function set_default_template($post_id) {
    global $post;
    if (!wp_is_post_revision($post_id) && 'page' == $post->post_type) {
        update_post_meta($post_id, '_wp_page_template', 'tpl-tour.php');
    }
}

if (current_user_can('editor'))
    add_action('save_post', 'set_default_template');

function remove_page_custom_fields() {
    remove_meta_box('pageparentdiv', 'page', 'normal');
    remove_meta_box('slugdiv', 'page', 'normal');
}

if (current_user_can('editor'))
    add_action('admin_menu', 'remove_page_custom_fields');

function remove_third_metabox() {
    remove_meta_box('wpseo_meta', 'page', 'normal');
    remove_meta_box('mymetabox_revslider_0', 'page', 'normal');
    remove_meta_box('um-admin-restrict-content', 'page', 'normal');
    remove_meta_box('csPgFlds4', 'page', 'normal');
    remove_meta_box('dpsp_share_statistics', 'page', 'normal');
    remove_meta_box('wp_add_custom_css', 'page', 'advanced');
    remove_meta_box('at_widget', 'page', 'advanced');
    remove_meta_box('php_everywhere_options_id', 'page', 'side');

    remove_meta_box('wpseo_meta', 'post', 'normal');
    remove_meta_box('mymetabox_revslider_0', 'post', 'normal');
    remove_meta_box('um-admin-restrict-content', 'post', 'normal');
    remove_meta_box('csPgFlds4', 'post', 'normal');
    remove_meta_box('dpsp_share_statistics', 'post', 'normal');
    remove_meta_box('wp_add_custom_css', 'post', 'advanced');
    remove_meta_box('at_widget', 'post', 'advanced');
    remove_meta_box('php_everywhere_options_id', 'post', 'side');
}

if (current_user_can('editor'))
    add_action('add_meta_boxes', 'remove_third_metabox', 11);

if (current_user_can('editor'))
    add_filter('show_admin_bar', '__return_false');

function custom_toolbar($toolbar) {
    $toolbar->remove_node('wp-logo');
    $toolbar->remove_node('update');
    $toolbar->remove_node('comments');
    $toolbar->remove_node('wpseo-menu');
    $toolbar->remove_node('jwl_links');
}

if (current_user_can('editor'))
    add_action('admin_bar_menu', 'custom_toolbar', 999);

if (current_user_can('editor'))
    add_filter('screen_options_show_screen', '__return_false');

function remove_help($old_help, $screen_id, $screen) {
    $screen->remove_help_tabs();
    return $old_help;
}

if (current_user_can('editor'))
    add_filter('contextual_help', 'remove_help', 999, 3);

function remove_footer_admin() {
    echo '';
}

if (current_user_can('editor')) {
    add_filter('admin_footer_text', 'remove_footer_admin');
}
/* end custom editor role settings */

add_filter('password_change_email', '__return_false'); //Close password change notification
add_filter('wp_new_user_notification_email_admin', '__return_false'); // Close new user notification for admin

session_start();
//Redirection for game tab
if (isset($_GET['gametab'])) {
    if (is_user_logged_in() && current_user_can('um_clients')) {
        exit(wp_redirect(home_url('user/'.get_current_user()->user_login.'/?profiletab=vouchers')));
    }
    else {
        $_SESSION['gametab']=1;
        exit(wp_redirect(home_url('login')));
    }
}

if( !empty($_SESSION['gametab']) && intval($_SESSION['gametab'])==1 ) {
    if (is_user_logged_in()) {
        $_SESSION['gametab']=2;
        if(current_user_can('um_clients'))
            exit(wp_redirect(home_url('user/'.get_current_user()->user_login.'/?profiletab=vouchers')));
    }
}

/* Amrut started For admin list of all the tickets */

/** Step 2 (from text above). */
add_action('admin_menu', 'list_contact_support_menu');
add_action('admin_menu', 'view_contact_support');

/** Step 1. */
function list_contact_support_menu() {
    add_options_page('Contact Support', 'Contact Support', 'manage_options', 'list-contact-support', 'list_contact_support');
}

function view_contact_support() {
    add_options_page('View Contact Support', 'View Contact Support', 'manage_options', 'ticket-details', 'ticket_details');
}

function ticket_details() {
    global $wpdb;
    $cs_id = '';
    $cs_created = '';
    $ticketDetails = [];
    $ticketComments = [];
    if (isset($_GET['ticket_id']) && $_GET['ticket_id'] != '') {
        $current_user = wp_get_current_user();
        $userId = $current_user->data->ID;
        $userName = $current_user->data->user_login;
        $cs_id = filter_input(INPUT_GET, 'ticket_id', FILTER_SANITIZE_NUMBER_INT);
        $ticketDetails = $wpdb->get_results("SELECT * FROM contact_support WHERE cs_id = '$cs_id'", ARRAY_A);
        if (!empty($ticketDetails)) {
            $ticketDetails = $ticketDetails[0];
            $cs_created = date("d M y", strtotime($ticketDetails['cs_created']));
            $cs_updated = date("d M y", strtotime($ticketDetails['cs_updated']));
            $cs_status = $ticketDetails['cs_status'];
            $ticketComments = $wpdb->get_results("SELECT * FROM contact_support_comment_rel WHERE cscr_contact_support_id = '$cs_id'", ARRAY_A);
        }
        $profilePicture = get_avatar($current_user->data->ID);
    }
    ?>
    <style>
        .messageA{
            background: #FFF;
            padding: 10px;
            border-radius: 10px;
            box-shadow: 3px 3px 10px #ffba00;
            margin-bottom: 10px;
        }

        textarea {
            padding: 2px 6px;
            height: 130px;
            resize: vertical;
            width: 70%;
        }
        .messageDate{
            display: inline-block;
            float: right;
            font-weight: 900;
        }
        .userName{
            display: inline-block;
            font-weight: 900;
        }
    </style>
    <div class="wrap">
        <div class="col-md-8" style="width: 70%">
            <div class="wrapper message-card" style="min-height: auto;">
                <h2>Subject : <?php echo isset($ticketDetails['cs_type']) ? $ticketDetails['cs_type'] : ''; ?></h2>
                <div class="messageA">
                    <div class="userName"><?php echo isset($ticketDetails['cs_user_name']) ? ucfirst($ticketDetails['cs_user_name']) : ''; ?></div><div class="messageDate"><?php echo $cs_created; ?></div>
                    <p><?php echo isset($ticketDetails['cs_description']) ? ucfirst($ticketDetails['cs_description']) : ''; ?></p>
                </div>
                <?php
                if (!empty($ticketComments)) {
                    foreach ($ticketComments as $comment) { //cscr_user_name
                        $cscr_created = date("d M y", strtotime($comment['cscr_created']));
                        ?>
                        <div class="messageA">
                            <div class="userName"><?php echo isset($comment['cscr_user_name']) ? ucfirst($comment['cscr_user_name']) : ''; ?></div><div class="messageDate"><?php echo $cscr_created; ?></div>
                            <p><?php echo isset($comment['cscr_description']) ? ucfirst($comment['cscr_description']) : ''; ?></p>
                            <?php if (isset($comment['cscr_attachment']) && $comment['cscr_attachment'] != '') { ?>
                                <p><a target="_blank" href="<?php echo isset($comment['cscr_attachment']) ? ucfirst($comment['cscr_attachment']) : ''; ?>">See the attachment</a></p>
                            <?php } ?>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <div style="height:20px"></div>

            <div class="wrapper submit-card" <?php if ($cs_status == 1) { ?>style="    min-height: auto;" <?php } ?>>
                <?php if ($cs_status == 0) { ?>
                    <form enctype="multipart/form-data" method="POST" action="https://www.travpart.com/English/submit-ticket/" id="commentForm">
                        <input type="hidden" name="action" value="ticket_comment">
                        <input type="hidden" name="from" value="admin">
                        <input type="hidden" name="cscr_contact_support_id" value="<?php echo $cs_id; ?>">
                        <input type="hidden" name="cscr_user_id" value="<?php echo $userId; ?>">
                        <input type="hidden" name="cscr_user_name" value="<?php echo $userName; ?>">
                        <textarea required="true" name="cscr_description"></textarea>
                        <div class="upload-btn-wrapper">
                            <input type="file" name="cscr_attachment" />
                        </div>
                        <br />
                        <button class="button">SUBMIT</button>
                        <a href="https://www.travpart.com/English/wp-admin/options-general.php?page=list-contact-support" style="text-decoration: underline;">Go back</a>
                    </form>
                    <?php
                } else {
                    ?>
                    <h4 style="color: red; border: 1px solid red; padding: 10px;border-radius: 5px;">You can't add the comment(s) because status of this ticket is <b>closed</b>!</h4>
                    <?php
                }
                ?>
            </div>

        </div>
    </div>
    <?php
}

/** Step 3. */
/* function p($data, $exit = 1) {
  echo "<pre>";
  print_r($data);
  if ($exit == 1) {
  exit;
  }
  } */

/* For Connect Project 2 START */
add_shortcode('customer_request_home', 'getAllListForHome');

function getAllListForHome() {
    global $wpdb;
    $listOfAllRequest = $wpdb->get_results("SELECT * FROM contact_sales_agent", ARRAY_A);
    ?>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.5/slick.min.css'> 
    <script src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.5/slick.min.js'></script>

    <div class="slider5" style="border:1px solid #ccc;padding:20px;margin:30px 0">
        <div class="row">
            <div class="col-md-12 heroSlider-fixed">
                <div class="overlay">
                </div>
                <!-- Slider -->
                <div class="slider responsive">
                    <?php
                    if (!empty($listOfAllRequest)) {
                        foreach ($listOfAllRequest as $request) {
                            ?>
                            <div>
                                <img src="http://placehold.it/200x150" alt="customer's request" />
                                <div class="requestname"><i class="fas fa-user"></i> <span>Adam Smith</span><br>
                                    <i class="fas fa-exclamation-circle"></i> I need 5 days tour
                                    <hr>
                                    <div class="requestcontent">We have $500 to spend for 5 days and...</div>

                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>


                </div>
                <!-- control arrows -->
                <div class="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                </div>
                <div class="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                </div>

            </div>
        </div>
    </div>

    <style>
        html {
            box-sizing: border-box;
        }

        *, *:before, *:after {
            box-sizing: inherit;
        }
        .slick-dots {
            text-align: center;
            margin: 0 0 10px 0;
            padding: 0;
            bottom: -30px!important;
        }
        .slick-dots li {
            display: inline-block;
            margin-left: 4px;
            margin-right: 4px;
        }
        .slick-dots li.slick-active button {
            background-color: black;
        }
        .slick-dots li button {
            font: 0/0 a;
            text-shadow: none;
            color: transparent;
            background-color: #999;
            border: none;
            width: 15px;
            height: 15px;
            border-radius: 50%;
        }
        .slick-dots li :hover {
            background-color: black;
        }

        /* Custom Arrow */
        .prev {
            color: #999;
            position: absolute;
            top: 38%;
            left: -2em;
            font-size: 1.5em;
        }
        .prev :hover {
            cursor: pointer;
            color: black;
        }

        .next {
            color: #999;
            position: absolute;
            top: 38%;
            right: -2em;
            font-size: 1.5em;
        }
        .next :hover {
            cursor: pointer;
            color: black;
        }

        .requestname{border:2px solid #f0f0f0 ;width: 200px;color:#363636;padding:1px 5px;cursor: pointer}
        .requestname span{font-weight: bold}
        .requestname hr{margin:10px;}
        .requestname:hover{border:2px solid #226d82;}
        .fa-user, .fa-exclamation-circle{color:#226d82;}
        .requestcontent{padding:0 15px 5px 15px;}
        @media screen and (max-width: 800px) {
            .next {
                display: none !important;
            }
        }
        @media screen and (max-width: 600px) {
            .slick-dots{bottom:-55px!important;}
        }
    </style>
    <script>
        $('.responsive').slick({
            dots: true,
            prevArrow: $('.prev'),
            nextArrow: $('.next'),
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    </script>
    <?php
}

/* For Connect Project 2 END */

function list_contact_support() {
    global $wpdb;

    $start = 0;
    $end = 10;
    if (isset($_GET['current_page']) && $_GET['current_page'] != '1') {
        $start = 1;
        $current_page = filter_input(INPUT_GET, 'current_page', FILTER_SANITIZE_NUMBER_INT);
        $endNew = $current_page * 10;
        $start = ($endNew - 10);
    }

    $searchString = "1=1";
    if (isset($_GET['cs_user_name']) && $_GET['cs_user_name'] != '') {
        $cs_user_name = filter_input(INPUT_GET, 'cs_user_name', FILTER_SANITIZE_STRING);
        $searchString .= " AND cs_user_name LIKE '$cs_user_name'";
    }

    if (isset($_GET['cs_id']) && $_GET['cs_id'] != '') {
        $cs_id = filter_input(INPUT_GET, 'cs_id', FILTER_SANITIZE_NUMBER_INT);
        $searchString .= " AND cs_id LIKE '$cs_id'";
    }

    if (isset($_GET['search_request']) && $_GET['search_request'] != '') {
        $search_request = filter_input(INPUT_GET, 'search_request', FILTER_SANITIZE_STRING);
        $searchString .= " AND (cs_type LIKE '$search_request' OR cs_description LIKE '$search_request' OR cs_user_name LIKE '$search_request')";
    }

//    p("SELECT * FROM contact_support WHERE $searchString LIMIT $start,$end");
    $myTicketsCount = $wpdb->get_results("SELECT * FROM contact_support", ARRAY_A);
    $totalTicket = count($myTicketsCount);
    $totalPage = ceil(($totalTicket / 10));
    $myTickets = $wpdb->get_results("SELECT * FROM contact_support WHERE $searchString LIMIT $start,$end", ARRAY_A);
    ?>
    <style>
        .close-ticket-status{
            background: #b53030;
            padding: 3px;
            color: #FFF;
            border-radius: 5px;
        }
        .open-ticket-status{
            background: #61c161;
            padding: 3px;
            color: #FFF;
            border-radius: 5px;
        }
    </style>
    <div class="wrap">
        <h1 class="wp-heading-inline">Support Tickets</h1>
        <form method="GET" action="https://www.travpart.com/English/wp-admin/options-general.php?page=list-contact-support">
            <p class="search-box">
                <input type="hidden" value="list-contact-support" name="page">
                <input type="text"  name="search_request" value="<?php echo isset($_GET['search_request']) ? $_GET['search_request'] : ''; ?>" placeholder="Search request">
                <!--<input type="submit" id="search-submit-sr" class="button" value="GO">-->
                <button type="submit">GO</button>
            </p>
        </form>
        <form method="GET" action="https://www.travpart.com/English/wp-admin/options-general.php?page=list-contact-support">
            <p class="search-box" style="    margin-right: 30px;">

                <input type="hidden" value="list-contact-support" name="page">
                <input type="text"  name="cs_id" value="<?php echo isset($_GET['cs_id']) ? $_GET['cs_id'] : ''; ?>" placeholder="Search Ticket ID">
                <!--<input type="submit" id="search-submit-cs-id" class="button" value="GO">-->
                <button type="submit">GO</button>
            </p>
        </form>
        <form method="GET" action="https://www.travpart.com/English/wp-admin/options-general.php?page=list-contact-support">
            <p class="search-box" style="    margin-right: 30px;">
                <input type="hidden" value="list-contact-support" name="page">
                <input type="text"  name="cs_user_name" value="<?php echo isset($_GET['cs_user_name']) ? $_GET['cs_user_name'] : ''; ?>" placeholder="Search Username">
                <!--<input type="submit" id="search-submit-cs-name" class="button" value="GO">-->
                <button type="submit">GO</button>
            </p>
        </form>

        <table class="wp-list-table widefat fixed striped pages">
            <thead>
                <tr>
                    <th scope="col" id="author" class="manage-column column-author">Username</th>
                    <th scope="col" id="tags" class="manage-column column-tags">ID</th>
                    <th scope="col" id="tags" class="manage-column column-tags">Subject</th>
                    <th scope="col" id="tags" class="manage-column column-tags">Created</th>
                    <th scope="col" id="tags" class="manage-column column-tags">Last Activity</th>
                    <th scope="col" id="tags" class="manage-column column-tags">Status</th>
                    <th scope="col" id="tags" class="manage-column column-tags">Action</th>

                </tr>
            </thead>
            <?php if (!empty($myTickets)) { ?>
                <tbody id="the-list">
                    <?php
                    foreach ($myTickets as $ticket) {
                        $cs_created = date("d M Y H:i:s A", strtotime($ticket['cs_created']));
                        $cs_updated = date("d M Y H:i:s A", strtotime($ticket['cs_updated']));
                        if ($ticket['cs_status'] == '0') {
                            $cs_status = "<span class='open-ticket-status'>OPEN</span>";
                        } else {
                            $cs_status = "<span class='close-ticket-status'>SOLVED</span>";
                        }
                        ?>
                        <tr id="post-29800" class="iedit author-other level-0 post-29800 type-page status-publish hentry">
                            <td>
                                <?php echo isset($ticket['cs_user_name']) ? $ticket['cs_user_name'] : 'N/A'; ?>
                            </td>
                            <td>
                                <?php echo isset($ticket['cs_id']) ? '000' . $ticket['cs_id'] : 'N/A'; ?>
                            </td>
                            <td>
                                <?php echo isset($ticket['cs_type']) ? $ticket['cs_type'] : 'N/A'; ?>
                            </td>
                            <td>
                                <?php echo $cs_created ?>
                            </td>
                            <td>
                                <?php echo $cs_updated ?>
                            </td>
                            <td>
                                <span><?php echo $cs_status; ?></span>
                            </td>
                            <td>
                                <span><a style="    text-decoration: underline;" href="https://www.travpart.com/English/wp-admin/options-general.php?page=ticket-details&ticket_id=<?php echo $ticket['cs_id']; ?>">View</a></span>&nbsp;&nbsp;&nbsp;
                                <?php if ($ticket['cs_status'] == '0') {
                                    ?>
                                    <span> <a style="    text-decoration: underline;" href="javascript:;" onclick="closeTicket('<?php echo $ticket['cs_id']; ?>');">Close this</a></span>
                                <?php } else {
                                    ?>
                                    <span><a style="    text-decoration: underline;" href="javascript:;" onclick="reopenTicket('<?php echo $ticket['cs_id']; ?>');">Re-open</a></span>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            <?php } else {
                ?>
                <h3 style="color: red">No tickets available to shown.</h3>
            <?php }
            ?>
        </table>
        <?php if ($totalTicket > 10) { ?>
            <div class="tablenav">
                <div class="tablenav-pages">
                    <span class="displaying-num"><?php echo $totalTicket; ?> items</span>
                    <?php for ($p = 1; $p <= $totalPage; $p++) { ?>
                        <a class="next-page" href="https://www.travpart.com/English/wp-admin/options-general.php?page=list-contact-support&current_page=<?php echo $p; ?>">
                            <!--<span class="screen-reader-text">Next page</span>-->
                            <span aria-hidden="true"><?php echo $p; ?></span>
                        </a>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

    </div>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js"></script>

    <script>
                            function reopenTicket(ticket_id) {
                                swal({
                                    title: "Are you sure?",
                                    text: "You want to re-open this ticket!",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Yes, Re-open It!",
                                    cancelButtonText: "No, Keep Closed!",
                                    closeOnConfirm: false,
                                    closeOnCancel: false
                                },
                                        function (isConfirm) {
                                            if (isConfirm) {
                                                var url = 'https://www.travpart.com/English/submit-ticket/';

                                                jQuery.ajax({
                                                    type: "POST",
                                                    url: url,
                                                    data: {action: 'ticket_reopen', ticket_id: ticket_id}, // serializes the form's elements.
                                                    success: function (data)
                                                    {
                                                        var result = JSON.parse(data);
                                                        if (result.status) {
                                                            swal("Reopen!", "Your this ticket is re-open.!.", "success");
                                                            window.location.reload()
                                                        } else {
                                                            alert(result.message);
                                                        }
                                                    }
                                                });
                                            } else {
                                                swal.close();
                                            }
                                        });
                            }

                            function closeTicket(ticket_id) {
                                swal({
                                    title: "Are you sure?",
                                    text: "You want to close this ticket!",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Yes, Close It!",
                                    cancelButtonText: "No, Keep Open!",
                                    closeOnConfirm: false,
                                    closeOnCancel: false
                                },
                                        function (isConfirm) {
                                            if (isConfirm) {
                                                var url = 'https://www.travpart.com/English/submit-ticket/';

                                                jQuery.ajax({
                                                    type: "POST",
                                                    url: url,
                                                    data: {action: 'ticket_clost', ticket_id: ticket_id}, // serializes the form's elements.
                                                    success: function (data)
                                                    {
                                                        var result = JSON.parse(data);
                                                        if (result.status) {
                                                            swal("Closed!", "Your this ticket is closed.", "success");
                                                            window.location.reload()
                                                        } else {
                                                            alert(result.message);
                                                        }
                                                    }
                                                });
                                            } else {
                                                swal.close();
                                            }
                                        });
                            }
    </script>

    <?php
}

/* End Amrut */


//Add post view count
function getPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return 0;
    }
    return $count;  
}  
function setPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count=='') {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '1');  
    }
    else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }  
}

//get front notification
function getNotification($wp_user_id)
{
    global $wpdb;
    if(empty($wp_user_id)||intval($wp_user_id)<=0)
        return array();
    return $wpdb->get_results("SELECT * FROM notification WHERE wp_user_id='{$wp_user_id}' AND read_status=0 ORDER BY create_time DESC LIMIT 5", ARRAY_A);
}
function getNotificationcount($wp_user_id)
{
    global $wpdb;
    if(empty($wp_user_id)||intval($wp_user_id)<=0)
        return 0;

    return  $wpdb->get_var( "SELECT COUNT(*) FROM notification WHERE wp_user_id='{$wp_user_id}' AND read_status=0" );
}

//update notification status
add_action('wp_ajax_read_notification', 'ajax_read_notification');
function ajax_read_notification() {
    global $wpdb;
    
    $userid=wp_get_current_user()->ID;
    $wpdb->update('notification',array('read_status'=>1),array('wp_user_id'=>$userid, 'read_status'=>0));
    echo json_encode(array('success'=>true));

    wp_die();
}

//get unread travchat message count
function getUnreadMessageCount($wp_user_login)
{
    global $wpdb;
    if(empty($wp_user_login))
        return 0;
    return intval($wpdb->get_var("SELECT COUNT(*) FROM `chat`,`user` WHERE `c_to`=user.userid AND user.username='{$wp_user_login}' AND `status`=0"));
}

//get friend request count
function getFriendRequestCount($wp_user_id)
{
    global $wpdb;
    if(empty($wp_user_id)||intval($wp_user_id)<=0)
        return 0;
    return intval($wpdb->get_var("SELECT COUNT(*) FROM `friends_requests` WHERE `user2` = {$wp_user_id} AND `status` = 0"));
}

//Update user last login time
function update_user_last_login_time($user_login,$user)
{
    global $wpdb;
    if(empty($user_login))
        return;
    if(empty($wpdb->get_var("SELECT `login_date` FROM `wp_user_last_login` WHERE `user_login`='{$user_login}'")))
        $wpdb->insert('wp_user_last_login',array('user_login'=>$user_login));
    else
        $wpdb->query("UPDATE `wp_user_last_login` SET `login_date` = CURRENT_TIME() WHERE `user_login`='{$user_login}'");
}
add_action('wp_login', 'update_user_last_login_time', 10, 2);

//Redirect user to email preference page if they login from that page
function user_redirect_email_preference_page($user_login,$user)
{
    global $wpdb;
    if($_GET['from']=='email-preferences') {
        exit(wp_redirect(home_url('email-preferences')));
    }
}
add_action('wp_login', 'user_redirect_email_preference_page', 100, 2);

function travchat_logout()
{
    session_start();
    session_destroy();
}
add_action('wp_logout', 'travchat_logout');

//Record the location of user
if( is_user_logged_in() && filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, array('flags'=>FILTER_FLAG_NO_PRIV_RANGE|FILTER_FLAG_NO_RES_RANGE)) ) {
    $geoip=new GeoIp2\Database\Reader(dirname(__FILE__).'/geoip/GeoLite2-City.mmdb');
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else if ($_SERVER['REMOTE_ADDR'] != '') {
        $ip = $_SERVER['REMOTE_ADDR'];
    } else {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    $geoipRecord=$geoip->city($ip);
    if(!empty($geoipRecord->country->name) && !empty($ip)) {
        if($wpdb->get_var("SELECT region FROM `user` WHERE username='".wp_get_current_user()->user_login."'")!=$geoipRecord->city->name)
            $wpdb->update('user', array('country'=>$geoipRecord->country->name, 'region'=>$geoipRecord->city->name, 'timezone'=>$geoipRecord->location->timeZone),
                            array('username'=>wp_get_current_user()->user_login) );
    }
}

//Update user referred game point
function update_user_referred_game_point($user_login, $user)
{
    global $wpdb;
    if(get_user_meta($user->ID,'refer_point_reward',true)!=1) {
        $inviteby=$wpdb->get_var("SELECT inviteby FROM `user` WHERE username='{$user_login}'");
        if(!empty($inviteby)) {
            $tp_pts_refr=get_option('_travpart_vo_option', null)['tp_pts_refr'];
            update_user_meta($user->ID, 'game_points', get_user_meta($user->ID, 'game_points', true)+$tp_pts_refr);
            $inviteby=get_user_by('login', $inviteby);
            if(!empty($inviteby->ID)) {
                update_user_meta($inviteby->ID, 'game_points', get_user_meta($inviteby->ID, 'game_points', true)+$tp_pts_refr);
            }
        }
        //prevent duplicate rewards
        update_user_meta($user->ID, 'refer_point_reward', 1);
    }
}
add_action('wp_login', 'update_user_referred_game_point', 10, 2);

//API for customer request list filter
add_action('wp_ajax_customer_request_list_filter', 'customer_request_list_filter');
function customer_request_list_filter()
{
    global $wpdb;
    if(!empty($_POST['destination']) && is_array($_POST['destination'])) {
        if(in_array('_all',$_POST['destination'])) {
            $destination='';
        }
        else {
            $destination='';
            foreach($_POST['destination'] as $item) {
                $destination.="OR `csa_destination_plan`='".htmlspecialchars($item)."' ";
            }
            if(!empty($destination))
                $destination='AND ('.ltrim($destination, 'OR').')';
        }
    }
    $orderby=filter_input(INPUT_POST, 'orderby', FILTER_SANITIZE_STRING);
    $orderby=($orderby=='asc')?'ASC':'DESC';
    $request_list=$wpdb->get_results("SELECT contact_sales_agent.*,user.userid FROM `contact_sales_agent`,`user` WHERE `csa_user_name`=`user`.`username` AND DATE_SUB(CURDATE(), INTERVAL 1 MONTH)<=DATE(csa_created) {$destination} ORDER BY `csa_created` {$orderby}");
    $customer_request_list_html='';
    foreach($request_list as $row) {
        $customer_request_list_html.='
<section
    class="elementor-element elementor-element-958e3a8 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section animated fadeInUp"
    data-id="958e3a8" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;}">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-287e043 requestlink elementor-column elementor-col-25 elementor-top-column request-item" csa_id="'.$row->csa_id.'"
                data-id="287e043" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-33d5e30 elementor-view-stacked elementor-position-left elementor-shape-circle elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                            data-id="33d5e30" data-element_type="widget" data-widget_type="icon-box.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-icon-box-wrapper">
                                    <div class="elementor-icon-box-icon">
                                        <span class="elementor-icon elementor-animation-">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <div class="elementor-icon-box-content">
                                        <h3 class="elementor-icon-box-title">
                                            <span>'.$row->csa_user_name.'</span>
                                        </h3>
                                        <!--<p class="elementor-icon-box-description"><a
                                                href="mailto:customer@gmail.com">customer@gmail.com</a></p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-element elementor-element-6413d67 requestlink elementor-column elementor-col-50 elementor-top-column request-item" csa_id="'.$row->csa_id.'"
                data-id="6413d67" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-18eecd4 elementor-view-stacked elementor-position-left elementor-shape-circle elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                            data-id="18eecd4" data-element_type="widget" data-widget_type="icon-box.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-icon-box-wrapper">
                                    <div class="elementor-icon-box-icon">
                                        <span class="elementor-icon elementor-animation-">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <div class="elementor-icon-box-content">
                                        <h3 class="elementor-icon-box-title">
                                            <span>Request Date</span>
                                        </h3>
                                        <p class="elementor-icon-box-description">'.$row->csa_created.'</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--div class="elementor-element elementor-element-6bf4453 elementor-view-stacked elementor-position-left elementor-shape-circle elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                            data-id="6bf4453" data-element_type="widget" data-widget_type="icon-box.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-icon-box-wrapper">
                                    <div class="elementor-icon-box-icon">
                                        <span class="elementor-icon elementor-animation-">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <div class="elementor-icon-box-content">
                                        <h3 class="elementor-icon-box-title">
                                            <span>Travel Plan date</span>
                                        </h3>
                                        <p class="elementor-icon-box-description">05/May/2019</p>
                                    </div>
                                </div>
                            </div>
                        </div-->
                        <div class="elementor-element elementor-element-eba81b3 elementor-view-stacked elementor-position-left elementor-shape-circle elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                            data-id="eba81b3" data-element_type="widget" data-widget_type="icon-box.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-icon-box-wrapper">
                                    <div class="elementor-icon-box-icon">
                                        <span class="elementor-icon elementor-animation-">
                                            <i class="fa fa-plane" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <div class="elementor-icon-box-content">
                                        <h3 class="elementor-icon-box-title">
                                            <span>Destination Plan</span>
                                        </h3>
                                        <p class="elementor-icon-box-description">
                                            '.$row->csa_destination_plan.'
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-element elementor-element-b428c1f elementor-column elementor-col-25 elementor-top-column"
                data-id="b428c1f" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-d5e35c0 elementor-align-center requestlink elementor-widget elementor-widget-button"
                            data-id="d5e35c0" data-element_type="widget" data-widget_type="button.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-button-wrapper">
                                    <a href="'.site_url().'/travchat/user/addnewmember.php?user='.$row->userid.'"
                                        class="elementor-button-link elementor-button elementor-size-md" target="_blank"
                                        role="button">
                                        <span class="elementor-button-content-wrapper">
                                            <span class="elementor-button-icon elementor-align-icon-left">
                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                            </span>
                                            <span class="elementor-button-text">Message</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-cf31235 elementor-align-center elementor-widget elementor-widget-button"
                            data-id="cf31235" data-element_type="widget" data-widget_type="button.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-button-wrapper">
                                    <a href="'.site_url().'/travcust"
                                        class="elementor-button-link elementor-button elementor-size-md" target="_blank"
                                        role="button">
                                        <span class="elementor-button-content-wrapper">
                                            <span class="elementor-button-icon elementor-align-icon-left">
                                                <i class="fa fa-forward" aria-hidden="true"></i>
                                            </span>
                                            <span class="elementor-button-text">Travcust</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        ';
    }
    if(empty($customer_request_list_html)) {
        echo json_encode(array('status'=>false));
    }
    else {
        echo json_encode(array('status'=>true, 'html'=>$customer_request_list_html));
    }
    wp_die();
}

//login api for travcust
function travcust_login_api()
{
    global $wpdb;
    if(!is_user_logged_in()) {
        $username=filter_input(INPUT_GET, 'username', FILTER_SANITIZE_STRING);
        $token=filter_input(INPUT_GET, 'token', FILTER_SANITIZE_STRING);
        if(!empty($username) && !empty($token)) {
            if($wpdb->get_var("SELECT COUNT(*) FROM `user` WHERE `username`='{$username}' AND `token`='{$token}'")==='1') {
                $user=get_user_by('login',$username); 
                if($user) {
                    wp_set_current_user($user->ID, $user->user_login);
                    wp_set_auth_cookie($user->ID);
                    do_action('wp_login', $user->user_login, $user);
                }
            }
        }
    }
}
add_action('init', 'travcust_login_api');

function skip_um_image_upload_nonce() {
    if(is_user_logged_in())
        return false;
    else
        return true;
}
add_filter('um_image_upload_nonce', 'skip_um_image_upload_nonce');

//choose coupon
add_action('wp_ajax_choose_coupon', 'choose_coupon');

function choose_coupon() {
    global $wpdb;
    $tour_id = filter_input(INPUT_POST, 'tour_id', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 999999)));
    $coupon_id = filter_input(INPUT_POST, 'coupon', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 999999)));
    if ($tour_id === false || $coupon_id === false) {
        echo json_encode(array('error_msg' => 'Invaild.'));
    } else if (!is_user_logged_in()) {
        echo json_encode(array('error_msg' => 'You need login your account before you choose the coupon.'));
    } else if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_tour` WHERE `id` = '{$tour_id}'") < 1) {
        echo json_encode(array('error_msg' => 'Tour is not existed.'));
    } else if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_tour` WHERE confirm_payment=1 AND `id` = '{$tour_id}'") == 1) {
        echo json_encode(array('error_msg' => 'The tour has been paid.'));
    } else {
        $current_user = wp_get_current_user();
        $tp_cpn_valdty=intval(get_option('_travpart_vo_option')['tp_cpn_valdty']);
        if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_coupons` WHERE `id` = {$coupon_id} AND `user_id` = {$current_user->ID}") < 1) {
            echo json_encode(array('error_msg' => 'The coupon is not belong to you.'));
        } else {
            $wpdb->query("UPDATE wp_coupons SET tour_id=NULL,discount_rp=NULL WHERE user_id={$current_user->ID} AND tour_id='{$tour_id}'");
            $wpdb->query("UPDATE wp_coupons SET tour_id='{$tour_id}' WHERE user_id={$current_user->ID} AND id='{$coupon_id}' AND DATE_SUB(CURDATE(), INTERVAL {$tp_cpn_valdty} DAY)<=date(get_time)");
            echo json_encode(array('msg' => 'success'));
        }
    }
    wp_die();
}

//Send game point after user logs in 
if(is_user_logged_in() && current_user_can('um_clients')) {
    $current_user=wp_get_current_user();
    $last_activity_date=get_user_meta($current_user->ID, 'last_activity_date', true);
    if($last_activity_date!=date('Y-m-d')) {
        if($last_activity_date==date('Y-m-d', time()-3600*24))
            update_user_meta($current_user->ID, 'login_days', get_user_meta($current_user->ID, 'login_days', true)+1);
        else
            update_user_meta($current_user->ID, 'login_days', 1);
        //update daily game points rewards
        update_user_meta($current_user->ID, 'game_points', get_user_meta(wp_get_current_user()->ID, 'game_points', true)+get_option('_travpart_vo_option', null)['tp_pts_lg_dly']);
        //update weekly game points rewards
        if(get_user_meta($current_user->ID, 'login_days', true)==7) {
            update_user_meta($current_user->ID, 'login_days', 0);
            update_user_meta($current_user->ID, 'game_points', get_user_meta(wp_get_current_user()->ID, 'game_points', true)+get_option('_travpart_vo_option', null)['tp_pts_lg_wkly']);
        }
        update_user_meta($current_user->ID, 'show_login_game_popup', 1);
        update_user_meta($current_user->ID, 'last_activity_date', date('Y-m-d'));
    }
}

function covid_popup_redirect()
{
    global $wpdb;
    if(is_user_logged_in() && get_option('show_covid_app_notifaction')==1 && !is_page_template('covid19.php')) {
        $current_user=wp_get_current_user();
        $data=$wpdb->get_row("SELECT datetime,user_option FROM `wp_covid19_data` WHERE wp_user_id='{$current_user->ID}' ORDER BY datetime DESC LIMIT 1");
        if(!empty($data->user_option) && $data->user_option=='yes' && floor((time()-$data->datetime)/86400)<14) {
        }
        else if(date('Y-m-d') != date('Y-m-d', $data->datetime)) {
            exit(wp_redirect(home_url('covid19')));
        }
    }
}
add_action('template_redirect', 'covid_popup_redirect');

function ajax_update_covid19_data()
{
    global $wpdb;
    
    $user_option = filter_input(INPUT_POST, 'user_option', FILTER_SANITIZE_NUMBER_INT);
    $lat = filter_input(INPUT_POST, 'lat', FILTER_SANITIZE_NUMBER_FLOAT);
    $lng = filter_input(INPUT_POST, 'lng', FILTER_SANITIZE_NUMBER_FLOAT);
    $user_option = $user_option==1?'yes':'no';
    $current_user = wp_get_current_user();

    if($user_option=='no') {
        $lat='0.00';
        $lng='0.00';
    }
    else if(empty($lat) || empty($lng)) {
        echo json_encode(array('success'=>false, 'msg'=>'Lat/lng can be empty'));
        wp_die();
    }

    if($wpdb->get_var("SELECT COUNT(*) FROM `wp_covid19_data` WHERE wp_user_id='{$current_user->ID}'")>0) {
        $wpdb->update('wp_covid19_data',
         array('user_option'=>$user_option, 'datetime'=>time(), 'lat'=>$lat, 'lng'=>$lng),
         array('wp_user_id'=>$current_user->ID));
    }
    else {
        $wpdb->insert('wp_covid19_data',
         array('wp_user_id'=>$current_user->ID, 'user_option'=>$user_option, 'datetime'=>time(), 'lat'=>$lat, 'lng'=>$lng));
    }
    echo json_encode(array('success'=>true, 'msg'=>'Updated'));
    wp_die();
}
add_action('wp_ajax_update_covid19_data', 'ajax_update_covid19_data');

function page_trace()
{
    global $wpdb;
    if (is_user_logged_in() && get_the_ID() > 0) {
        $current_user = wp_get_current_user();
        $post_id = get_the_ID();
        if ($wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}user_trace
            WHERE user_id={$current_user->ID} AND type='page' AND value='{$post_id}'") == 0) {
            $i = $wpdb->insert(
                $wpdb->prefix . 'user_trace',
                array('user_id' => $current_user->ID, 'type' => 'page', 'value' => $post_id)
            );
        }
    }
}
add_action('wp', 'page_trace');

function custom_blog_post()
{
    $args=array(
        'labels'=>array('name'=>__('Blog'),
            'singular_name'=>__('Blog'),
            'menu_name'=>'Blog'),
        'public'=>true,
        'supports'=>array('title', 'editor', 'author', 'thumbnail', 'comments'),
        'taxonomies'=>array('blog_category'),
        'map_meta_cap'=>true,
        'has_archive'=>true
    );
    register_post_type('blog', $args);
}
add_action('init', 'custom_blog_post');

function taxonomies_blog()
{
    register_taxonomy('blog_category', 'blog', array('hierarchical' => true));
}
add_action('init', 'taxonomies_blog', 0);

function bali_blog_widget_init()
{
    register_sidebar( array(
        'name' => esc_html__( 'Blog Right Sidebar', 'bali' ),
        'id' => 'blogright',
        'description' => esc_html__( 'Right sidebar for the blog', 'bali' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
}
add_action( 'widgets_init', 'bali_blog_widget_init' );

function custom_short_post()
{
    $args=array(
        'labels'=>array('name'=>__('ShortPost'),
            'singular_name'=>__('ShortPost'),
            'menu_name'=>'ShortPost'),
        'public'=>true,
        'supports'=>array('editor', 'author', 'thumbnail', 'comments'),
        'map_meta_cap'=>true,
        'has_archive'=>true
    );
    register_post_type('shortpost', $args);
}
add_action('init', 'custom_short_post');

function shortpost_custom_columns($column)
{
    global $post,$wpdb;
    switch ($column) {
        case "looking":
            if($wpdb->get_var("SELECT COUNT(*) FROM `wp_postmeta` WHERE post_id='{$post->ID}'
                AND `meta_key`='attachment' AND meta_value IN (SELECT ID FROM `wp_posts` WHERE `post_mime_type` LIKE '%video%')")>0) {
                echo 'Video';
            }
            else {
                echo intval(get_post_meta($post->ID, 'lookingfriend', true))>=1?'TravMatch':'N/A';
            }
            break;
    }
}
function set_shortpost_columns($columns) {
    return array(
        'cb' => '<input type="checkbox" />',
        'title' => __('Title'),
        'looking' => __('Type'),
        'Author' => __('Author'),
        'date' => __('Date')
    );
}
add_action('manage_shortpost_posts_custom_column','shortpost_custom_columns');
add_filter('manage_shortpost_posts_columns', 'set_shortpost_columns');

function search_shortpost_author($search, $query)
{
    if(empty($search)) {
        return $search;
    }
    if($query->query['post_type']=='shortpost' && !empty($query->query['s'])) {
        $users=get_users(
            array('search' =>'*'.htmlspecialchars($query->query['s']).'*',
            'search_columns'=>array('user_login','user_nicename','display_name'),
            'number'=>15));
        $users_id=array();
        foreach($users as $item) {
            $users_id[]=$item->ID;
        }
        if(!empty($users_id)) {
            $search .= ' OR wp_posts.post_author IN ('.implode(',', $users_id).') ';
        }
    }
    return $search;
}
add_action('posts_search', 'search_shortpost_author', 500, 2);

function custom_event_post()
{
    $args=array(
        'labels'=>array('name'=>__('EventPost'),
            'singular_name'=>__('EventPost'),
            'menu_name'=>'EventPost'),
        'public'=>true,
        'supports'=>array('title', 'editor', 'author', 'thumbnail', 'comments'),
        'map_meta_cap'=>true,
        'has_archive'=>true
    );
    register_post_type('eventpost', $args);
}
add_action('init', 'custom_event_post');

function add_short_post()
{
    global $wpdb;
    session_start();
    try {
        if (!wp_verify_nonce($_POST['post_nonce'], 'short_post_nonce_action')) {
            throw new Exception(
                __("Sorry! You failed the security check", 'short-post-publishing'),
                1
            );
        }

        if(isset($_SESSION['post_nonce']) && $_SESSION['post_nonce']==$_POST['post_nonce'] && time()-$_SESSION['post_time']<10 ) {
            echo json_encode(array('success'=>true));
            wp_die();
        }
        else {
            $_SESSION['post_nonce']=htmlspecialchars($_POST['post_nonce']);
            $_SESSION['post_time']=time();
        }

        $post_content = wp_rel_nofollow($_POST['post_content']);
        $location = htmlspecialchars($_POST['location']);
        $lat = filter_input(INPUT_POST, 'lat', FILTER_SANITIZE_NUMBER_FLOAT);
        $lng = filter_input(INPUT_POST, 'lng', FILTER_SANITIZE_NUMBER_FLOAT);
        $feeling = htmlspecialchars($_POST['feeling']);
        $tour_id = intval($_POST['tourpackage']);
        $tag_user_id = intval($_POST['tagfriend']);
        $lookingfriend = filter_input(INPUT_POST, 'lookingfriend', FILTER_SANITIZE_NUMBER_INT);
        $startdate = filter_input(INPUT_POST, 'startdate', FILTER_SANITIZE_STRING);
        $enddate = filter_input(INPUT_POST, 'enddate', FILTER_SANITIZE_STRING);
        $current_user = wp_get_current_user();

        if (!empty($lookingfriend) && $lookingfriend>=1) {
            if(empty($location) || empty($lat) || empty($lng)) {
                throw new Exception("Location is required", 1);
            }
            if(empty($startdate)) {
                throw new Exception("Start date is required", 1);
            }
            if(empty($enddate)) {
                throw new Exception("End date is required", 1);
            }
        }

        $new_post = array(
            'post_content'   => wp_kses_post($post_content),
            'post_type'      => 'shortpost',
            'post_status'    => 'publish'
        );

        $new_post_id = wp_insert_post($new_post, true);

        // get friends and add notifaction
        $friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE ID IN(
                SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$current_user->ID}')
                OR ID IN(
                    SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$current_user->ID}')");

            foreach($friends as $f) {

             $count_last_24_hour_notifaction ="SELECT COUNT(*) FROM `notification` WHERE create_time>= NOW()- INTERVAL 24 HOUR AND wp_user_id='{$f->ID}' AND identifier = 'timelinepost'";

             


                $msg ="<a href='https://www.travpart.com/English/timeline/'>".um_get_display_name($current_user->ID)." posted something new in timeline</a>";
                $wpdb->insert('notification', array('wp_user_id'=>$f->ID, 'content'=>$msg,'identifier'=>'timelinepost'));

                // added for sending notifaction to friends 16 feb 2020
                $token= to_get_user_device_token_from_wp_id($f->ID);

                if( $count_last_24_hour_notifaction<1 ){
                 if(!empty($token)){


                    $msg = um_get_display_name($current_user->ID)." Posted something New in timeline";

                   //  sendtomobilefunction($msg,$token);
                   notification_for_user( $token,$msg,'timelinepost');
                    } 
                }
            }

                // notifaction to fellowers

            $followers = $wpdb->get_results(" SELECT sf_user_id FROM `social_follow` WHERE `sf_agent_id` = '{$current_user->ID}'");

            foreach($followers as $f1) {

             $count_last_24_hour_notifaction ="SELECT COUNT(*) FROM `notification` WHERE create_time>= NOW()- INTERVAL 24 HOUR AND wp_user_id='{$f->ID}' AND identifier = 'timelinepost'";

             

                $msg ="<a href='https://www.travpart.com/English/timeline/'>".um_get_display_name($current_user->ID)." posted something new in timeline</a>";
                $wpdb->insert('notification', array('wp_user_id'=>$f1->ID, 'content'=>$msg,'identifier'=>'timelinepost'));

                // added for sending notifaction to friends 16 feb 2020
                $token= to_get_user_device_token_from_wp_id($f1->ID);

                if( $count_last_24_hour_notifaction<1 ){
                    if(!empty($token)){

                    $msg = um_get_display_name($current_user->ID)." Posted something New in timeline";

                    // sendtomobilefunction($msg,$token);
                    notification_for_user( $token,$msg,'timelinepost');
                    } 
                }
            }


        if (is_wp_error($new_post_id))
            throw new Exception($new_post_id->get_error_message(), 1);
        
        if (!empty($location)) {
            update_post_meta($new_post_id, 'location', $location);
        }

        if (!empty($startdate)) {
            update_post_meta($new_post_id, 'startdate', $startdate);
        }

        if (!empty($enddate)) {
            update_post_meta($new_post_id, 'enddate', $enddate);
        }
        
        if (!empty($feeling)) {
            update_post_meta($new_post_id, 'feeling', base64_encode($feeling));
        }

        if (!empty($_POST['tagfriend'])) {
            update_post_meta($new_post_id, 'tag_user_id', $tag_user_id);


            $msg ="<a href='https://www.travpart.com/English/shortpost/$new_post_id'>".um_get_display_name($current_user->ID)." Tagged You in a post</a>";
                $wpdb->insert('notification', array('wp_user_id'=>$tag_user_id, 'content'=>$msg,'identifier'=>'taginpost'));


            // pop up notifaction

            $device_token = to_get_user_device_token_from_wp_id( $tag_user_id );

            if(!empty($device_token)){

            $tag_msg = um_get_display_name($current_user->ID)." Tagged you in a post";

            // sendtomobilefunction($msg,$token);
            notification_for_user( $device_token,$tag_msg,'taginpost');
            } 
        }

        if (!empty($lookingfriend) && $lookingfriend>=1) {
            update_post_meta($new_post_id, 'lookingfriend', $lookingfriend);

            $friends = $wpdb->get_results("SELECT ID,user_login FROM `wp_users` WHERE ID IN(
                SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$current_user->ID}')
                OR ID IN(
                    SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$current_user->ID}')");
                    
            foreach($friends as $f) {
                $wpdb->insert('notification', array('wp_user_id'=>$f->ID, 'content'=>um_get_display_name($current_user->ID)." is looking for a travel friend",'identifier'=>'travmatch'));

    // added for sending notifaction to friends 16 feb 2020
    $token= to_get_user_device_token_from_wp_id($f->ID);

    if(!empty($token)){

    $msg = um_get_display_name($current_user->ID)." is looking to hangout";

    // sendtomobilefunction($msg,$token);
    notification_for_user( $token,$msg,'travmatch');
    } 
    }
/*
     $get_users_list = $wpdb->get_results("SELECT * FROM `wp_users`");

    foreach ($get_users_list as $value) {

    $user_id = $value->ID;

    $token= to_get_user_device_token_from_wp_id($user_id);

    if(!empty($token)){

     $msg = um_get_display_name($current_user->ID)." is looking to hangout";

    
    notification_for_hangout ( $token,$msg,'travmatch');
    } 
    }*/
    
        }
        
        if (!empty($_POST['tourpackage'])) {
            $tour=$wpdb->get_row("SELECT ID,post_title FROM `wp_posts`,`wp_postmeta`
                WHERE post_type='post' AND post_status='publish' AND wp_posts.ID=wp_postmeta.post_id
                AND `meta_key`='tour_id' AND meta_value='{$tour_id}'");
            update_post_meta($post_id, 'tour_id', $tour_id);
            update_post_meta($new_post_id, 'tour_post', $tour->ID);
            update_post_meta($new_post_id, 'tour_title', $tour->post_title);

            $hotelinfo = $wpdb->get_row("SELECT img,location FROM wp_hotel WHERE tour_id='{$tour_id}' LIMIT 1");
            if (!empty($hotelinfo->img)) {
                if (is_array(unserialize($hotelinfo->img))) {
                    $hotelimg = unserialize($hotelinfo->img)[0];
                } else
                    $hotelimg = $hotelinfo->img;
            }
            if (!empty($hotelimg)) {
                update_post_meta($new_post_id, 'tour_img', $hotelimg);
            }
        }
        
        if (!empty($_POST['featured_img']) && $_POST['featured_img'] != -1) {
            if(stripos($_POST['featured_img'],',')===FALSE) {
                update_post_meta($new_post_id, 'attachment', intval($_POST['featured_img']));
            }
            else {
                $featured_imgs=explode(',', trim($_POST['featured_img'],','));
                foreach($featured_imgs as &$i) {
                    if(!empty($i) && intval($i)>1) {
                        add_post_meta($new_post_id, 'attachment', $i);
                    }
                }
            }
        }
        $fb_sharing_option = get_user_meta( $current_user->ID, 'fb_sharing_option' , true );
        if(!empty ($fb_sharing_option)){
            $fb_sharing_option = $fb_sharing_option;
        }
        else{
            $fb_sharing_option = 0;
        }

        if (!empty($lookingfriend) && $lookingfriend>=1) {
            $wpdb->insert('wp_user_plans',
                array(
                    'user_id' => $current_user->ID,
                    'user_name' => $current_user->user_login,
                    'start_date' => date('d/m/Y',strtotime($startdate)),
                    'end_Date' => date('d/m/Y',strtotime($enddate)),
                    'no_of_days' => ceil(abs((strtotime($enddate)-strtotime($startdate))/86400)),
                    'location' => $location,
                    'lat' => $lat,
                    'lng' => $lng
                )
            );
        }

        $data['success'] = true;
        $data['post_id'] = $new_post_id;
       $data['fb_sharing_option'] = $fb_sharing_option;
        $data['link'] = 'https://www.travpart.com/English/shortpost/'.$new_post_id;

    } catch (Exception $ex) {
        $data['success'] = false;
        $data['message'] = sprintf(
            'Error:%s',
            $ex->getMessage()
        );
    }
    die(json_encode($data));
}
add_action('wp_ajax_add_short_post', 'add_short_post');

function update_short_post()
{
    global $wpdb;
    try {
        $post_id = filter_input(INPUT_POST, 'postid', FILTER_SANITIZE_NUMBER_INT);
        $post_content = wp_rel_nofollow($_POST['post_content']);
        $feeling = filter_input(INPUT_POST, 'feeling', FILTER_SANITIZE_STRING);
        $tour_id = filter_input(INPUT_POST, 'tourpackage', FILTER_SANITIZE_NUMBER_INT);
        $tag_user_id = filter_input(INPUT_POST, 'tagfriend', FILTER_SANITIZE_NUMBER_INT);
        $current_user = wp_get_current_user();

        if(get_post_field('post_author',$post_id) != $current_user->ID) {
            throw new Exception("You are not the author!", 1);
        }

        $r = wp_update_post(array(
            'ID'           => $post_id,
            'post_content' => wp_kses_post($post_content)
        ));

        if ($r!=$post_id) {
            throw new Exception("Update failed", 1);
        }
        
        if (!empty($feeling)) {
            update_post_meta($post_id, 'feeling', base64_encode($feeling));
        }
        else {
            delete_post_meta($post_id, 'feeling');
        }

        if (!empty($_POST['tagfriend'])) {
            update_post_meta($post_id, 'tag_user_id', $tag_user_id);
        }
        else {
            delete_post_meta($post_id, 'tag_user_id');
        }
        
        if (!empty($_POST['tourpackage'])) {
            $tour=$wpdb->get_row("SELECT ID,post_title FROM `wp_posts`,`wp_postmeta`
                WHERE post_type='post' AND post_status='publish' AND wp_posts.ID=wp_postmeta.post_id
                AND `meta_key`='tour_id' AND meta_value='{$tour_id}'");
            update_post_meta($post_id, 'tour_id', $tour_id);
            update_post_meta($post_id, 'tour_post', $tour->ID);
            update_post_meta($post_id, 'tour_title', $tour->post_title);

            $hotelinfo = $wpdb->get_row("SELECT img,location FROM wp_hotel WHERE tour_id='{$tour_id}' LIMIT 1");
            if (!empty($hotelinfo->img)) {
                if (is_array(unserialize($hotelinfo->img))) {
                    $hotelimg = unserialize($hotelinfo->img)[0];
                } else {
                    $hotelimg = $hotelinfo->img;
                }
            }
            if (!empty($hotelimg)) {
                update_post_meta($post_id, 'tour_img', $hotelimg);
            }
        }
        
        if (!empty($_POST['featured_img']) && $_POST['featured_img'] != -1) {
            if (stripos($_POST['featured_img'], ',')===false) {
                update_post_meta($post_id, 'attachment', intval($_POST['featured_img']));
            } else {
                delete_post_meta($post_id, 'attachment');
                $featured_imgs=explode(',', trim($_POST['featured_img'], ','));
                foreach ($featured_imgs as &$i) {
                    if (!empty($i) && intval($i)>1) {
                        add_post_meta($post_id, 'attachment', $i);
                    }
                }
            }
        }

        $data['success'] = true;
    } catch (Exception $ex) {
        $data['success'] = false;
        $data['message'] = sprintf(
            'Error:%s',
            $ex->getMessage()
        );
    }
    die(json_encode($data));
}
add_action('wp_ajax_update_short_post', 'update_short_post');

function user_fb_sharing_option(){

    global $wpdb;
    $current_user = wp_get_current_user();
    try{
        $option= htmlspecialchars($_POST['user_option']);

        $updated = update_user_meta( $current_user->ID, 'fb_sharing_option', $option );
        $data['success'] = true;
        $data['message'] = $option;


    }
    catch (Exception $ex) {
        $data['success'] = false;
        $data['message'] = sprintf(
            'Error:%s',
            $ex->getMessage()
        );
    }
    die(json_encode($data));
}

add_action('wp_ajax_user_fb_sharing_option', 'user_fb_sharing_option');
function add_event_post()
{
    global $wpdb;
    try {
        if (!wp_verify_nonce($_POST['post_nonce'], 'event_post_nonce_action'))
            throw new Exception(
                __("Sorry! You failed the security check", 'event-post-publishing'),
                1
            );

        $event_name = filter_input(INPUT_POST, 'event_name', FILTER_SANITIZE_STRING);
        $event_type = filter_input(INPUT_POST, 'event_type', FILTER_SANITIZE_STRING);
        $event_invited_people = $_POST['event_invited_people'];
        $event_location = filter_input(INPUT_POST, 'event_location', FILTER_SANITIZE_STRING);
        $event_location_image_id = filter_input(INPUT_POST, 'event_location_image_id', FILTER_SANITIZE_NUMBER_INT);
        $start_date = filter_input(INPUT_POST, 'start_date', FILTER_SANITIZE_STRING);
        $end_date = filter_input(INPUT_POST, 'end_date', FILTER_SANITIZE_STRING);
        $event_tour_package = filter_input(INPUT_POST, 'event_tour_package', FILTER_SANITIZE_NUMBER_INT);
        $current_user = wp_get_current_user();

        $new_post = array(
            'post_title'   => $event_name,
            'post_type'      => 'eventpost',
            'post_status'    => 'publish'
        );

        $new_post_id = wp_insert_post($new_post, true);
        if (is_wp_error($new_post_id))
            throw new Exception($new_post_id->get_error_message(), 1);

        $event_type = $event_type=='public'?'public':'private';
        update_post_meta($new_post_id, 'event_type', $event_type);

        if (!empty($event_invited_people)) {
            update_post_meta($new_post_id, 'event_invited_people', $event_invited_people);
            foreach($event_invited_people as $p) {
                if(intval($p)>0){

                $msg ="<a href='https://www.travpart.com/English/eventpost/$event_name'>".um_get_display_name($current_user->ID) ."  invited you to their event</a>";

                    $wpdb->insert('notification', array('wp_user_id' => intval($p), 'content' => $msg,'identifier'=>'event_invite'));

                 // added for sending notifaction  to mobile
                $token= to_get_user_device_token_from_wp_id(intval($p));

                    if(!empty($token)){

                $msg = um_get_display_name($current_user->ID)." Invited you to their event";

                    // sendtomobilefunction($msg,$token);
                    notification_for_user( $token,$msg,'event_invite');
                    } 


                }
            }
        }

        if (!empty($event_location)) {
            update_post_meta($new_post_id, 'event_location', $event_location);
        }

        if (!empty($event_location_image_id)) {
            update_post_meta($new_post_id, 'event_location_image_id', $event_location_image_id);
        }

        if (!empty($start_date)) {
            update_post_meta($new_post_id, 'start_date', strtotime($start_date));
        }

        if (!empty($end_date)) {
            update_post_meta($new_post_id, 'end_date', strtotime($end_date));
        }

        if (!empty($event_tour_package)) {
            update_post_meta($new_post_id, 'event_tour_package', $event_tour_package);
            $hotelinfo = $wpdb->get_row("SELECT img,location FROM wp_hotel WHERE tour_id='{$event_tour_package}' LIMIT 1");
            if (!empty($hotelinfo->img)) {
                if (is_array(unserialize($hotelinfo->img))) {
                    $hotelimg = unserialize($hotelinfo->img)[0];
                }
                else
                    $hotelimg = $hotelinfo->img;
            }
            if (!empty($hotelimg)) {
                update_post_meta($new_post_id, 'tour_img', $hotelimg);
            }
        }

        $data['success'] = true;
        $data['post_id'] = $new_post_id;
    } catch (Exception $ex) {
        $data['success'] = false;
        $data['message'] = sprintf(
            'Error:%s',
            $ex->getMessage()
        );
    }
    die(json_encode($data));
}
add_action('wp_ajax_add_event_post', 'add_event_post');

function ajax_update_event_going()
{
    global $wpdb;
    
    $post_id = filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT);
    $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_NUMBER_INT);
    $status = $status==1?1:0;
    $current_user = wp_get_current_user();

    if($wpdb->get_var("SELECT COUNT(*) FROM `event_going` WHERE user_id='{$current_user->ID}' AND post_id='{$post_id}'")>0) {
        $wpdb->update('event_going', array('status'=>$status), array('user_id'=>$current_user->ID, 'post_id'=>$post_id));
    }
    else {
        $wpdb->insert('event_going', array('user_id'=>$current_user->ID, 'post_id'=>$post_id, 'status'=>$status));
    }
}
add_action('wp_ajax_update_event_going', 'ajax_update_event_going');

// custom end point for blogs
function blog_posts_for_app($request_data)
{
    $num_of_posts = 10;
    $parameters = $request_data->get_params();
    if (isset($parameters['offset']) && is_numeric($parameters['offset'])) {
        $offset = sanitize_text_field($parameters["offset"]);
    }
    if (isset($parameters['kw'])) {
        $kw = sanitize_text_field($parameters["kw"]);
        if (!empty($kw)) {
            $num_of_posts = -1;
        }
    }

    $args = array(
        'posts_per_page' => $num_of_posts,
        'post_type' => 'blog',
        's' => $kw,
        'offset'  => $offset,
        'post_status'      => 'publish'

    );

    $posts = get_posts($args);

    foreach ($posts as $post) {

        $img =  wp_get_attachment_url(get_post_thumbnail_id($post->ID));
        $author_id = $post->post_author;
        $display_name = get_the_author_meta('display_name', $author_id);
        $data[] = array(
            'id' => $post->ID,
            'title' => $post->post_title,
            'img' => $img,
            'author' =>  $display_name,
            'content' => wp_strip_all_tags($post->post_content),
            'url' => get_the_permalink($post->ID)
        );
    }

    $request['data'] = $data;
    return $request;
}

//social comment vote(like/dislike)
add_action('wp_ajax_social_comment_vote', 'social_comment_vote');

function social_comment_vote()
{
    global $wpdb;
    $scm_id = filter_input(INPUT_POST, 'scm_id', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1)));
    $vote_type = filter_input(INPUT_POST, 'vote_type', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1)));
    $vote_type = ($vote_type == 0) ? 0 : 1;
    if ($scm_id === false || $vote_type === false) {
        echo json_encode(array('success' => false, 'error_msg' => 'Invaild.'));
    } else if (!is_user_logged_in()) {
        echo json_encode(array('success' => false, 'error_msg' => 'You need login your account firstly'));
    } else if ($wpdb->get_var("SELECT COUNT(*) FROM `social_comments` WHERE `scm_id` = '{$scm_id}'") < 1) {
        echo json_encode(array('error_msg' => 'Comment is not existed.'));
    } else {
        $current_user = wp_get_current_user();
        $voted_type = $wpdb->get_var("SELECT vote_type FROM social_commentvote WHERE user_id={$current_user->ID} AND scm_id={$scm_id}");
        if (is_null($voted_type)) {
            $wpdb->query("INSERT INTO `social_commentvote` (`user_id`, `scm_id`, `vote_type`) VALUES ('{$current_user->ID}', '{$scm_id}', '{$vote_type}')");
        } else if($voted_type!=$vote_type) {
            $wpdb->query("UPDATE `social_commentvote` SET `vote_type` = '{$vote_type}' WHERE user_id={$current_user->ID} AND scm_id={$scm_id}");
        }
        else {
            $wpdb->query("DELETE FROM social_commentvote WHERE user_id={$current_user->ID} AND scm_id={$scm_id}");
        }
        echo json_encode(array('success' => true));
    }
    wp_die();
}

//add feed comment
add_action('wp_ajax_feed_comment', 'ajax_feed_comment');
function ajax_feed_comment()
{
    global $wpdb;
    $feed_id = filter_input(INPUT_POST, 'feed_id', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1)));
    $feed_comment = filter_input(INPUT_POST, 'feed_comment', FILTER_SANITIZE_STRING);

    if ($wpdb->get_var("SELECT COUNT(*) FROM `wp_posts` WHERE `ID` = '{$feed_id}'") < 1 || $feed_comment=='') {
        echo json_encode(array('success' => false, 'error_msg' => 'feed is not existed.'));
    } else {


        $current_user = wp_get_current_user();
        $wpdb->insert(
            'user_feed_comments',
            array(
                'feed_id' => $feed_id,
                'user_id' => $current_user->ID,
                'comment' => $feed_comment
            )
        );

$the_author = $wpdb->get_row( "SELECT `post_author` FROM wp_posts WHERE `ID` = '{$feed_id}'" );

$device_token = to_get_user_device_token_from_wp_id ($the_author->post_author);

if($device_token){

$msg=  um_get_display_name($current_user->ID)." commented on your post";
notification_for_user($device_token,$msg,'shortpost_comment_'.$feed_id);
}
//notifaction

$not_msg ="<a href='https://www.travpart.com/English/shortpost/$feed_id'>".um_get_display_name($current_user->ID)." commented on your post".
"</a>";
$wpdb->insert('notification', array('wp_user_id'=>$the_author->post_author, 'content'=>$not_msg,'identifier'=>'shortpost_comment_'.$feed_id));


        echo json_encode(array('success' => true));
    }
    wp_die();
}

function event_week_time_display($time) {
    if((date('j',$time)-date('j'))==1) {
        echo 'Tomorrow';
    }
    else {
        echo date('l',$time);
    }
}

add_action('wp_ajax_loading_more_shortpost', 'ajax_loading_more_shortpost');
function ajax_loading_more_shortpost()
{
    global $wpdb;
    $current_user = wp_get_current_user();
    $pageNum = filter_input(INPUT_GET, 'pageNum', FILTER_SANITIZE_NUMBER_INT);
    $pageNum = ($pageNum > 0) ? ($pageNum - 1) : 0;
    
    $post_per_page = 15;
    $postOffset = $pageNum * $post_per_page;

    $friends_ids_query = $wpdb->get_results("SELECT ID FROM `wp_users` WHERE ID IN(
    SELECT user2 FROM `friends_requests` WHERE status=1 AND user1='{$current_user->ID}')
    OR ID IN(
        SELECT user1 FROM `friends_requests` WHERE status=1 AND user2='{$current_user->ID}')");
    foreach ($friends_ids_query as $id) {
        $friends_ids [] = $id->ID;
    }
    $friends_ids[]=$current_user->ID;
    $List = implode(',', $friends_ids);
    $hide_sql = '';
    if(is_user_logged_in()) {
        $hide_sql = " AND wp_posts.ID NOT IN(SELECT short_post_id FROM `short_posts_hide_from_timeline` WHERE user_id='{$current_user->ID}') ";
    }
    $timeLinePosts = $wpdb->get_results("SELECT wp_posts.ID,post_author,user_login,post_title,post_date,post_type,post_content
                                        FROM `wp_posts`,`wp_users` WHERE
                                        ((post_author IN ($List) AND (`post_type`='shortpost' OR `post_type`='eventpost')) OR `post_type`='blog')
                                        AND post_status='publish' AND post_author=wp_users.ID
                                        {$hide_sql}
                                        ORDER BY post_date DESC LIMIT {$postOffset},{$post_per_page}");

    if (empty($timeLinePosts)) {
        return;
    }
    
    include_once('timeline-post-content.php');

    wp_die();
}

add_action('wp_ajax_loading_more_eventpost', 'ajax_loading_more_eventpost');
add_action('wp_ajax_nopriv_loading_more_eventpost', 'ajax_loading_more_eventpost');
function ajax_loading_more_eventpost()
{
    global $wpdb;
    if (is_user_logged_in()) {
        $current_user = wp_get_current_user();
    }
    $pageNum = filter_input(INPUT_GET, 'pageNum', FILTER_SANITIZE_NUMBER_INT);
    $pageNum = ($pageNum > 0) ? ($pageNum - 1) : 0;
    $totalPost = $wpdb->get_var("SELECT COUNT(*) FROM `wp_posts` WHERE `post_type`='eventpost' AND post_status='publish'");

    $post_per_page = 15;
    if (ceil($totalPost / $post_per_page) < $pageNum) {
        return;
    }
    $postOffset = $pageNum * $post_per_page;
    $eventPosts = $wpdb->get_results("SELECT wp_posts.ID,post_author,user_login,post_title,post_date,post_type,post_content
                                        FROM `wp_posts`,`wp_users` WHERE `post_type`='eventpost'
                                        AND post_status='publish' AND post_author=wp_users.ID
                                        ORDER BY post_date DESC LIMIT {$postOffset},{$post_per_page}");

    if (empty($eventPosts)) {
        return;
    }

    foreach ($eventPosts as $item) {
        if (get_post_meta($item->ID, 'event_type', true) == 'private') {
            $event_invited_people = get_post_meta($item->ID, 'event_invited_people', true);
            if (!is_user_logged_in() && $current_user->ID != $item->post_author
                && !in_array($current_user->ID, $event_invited_people)) {
                continue;
            }
        }
        $event_name = get_post_meta($item->ID, 'event_name', true);
        $event_location = get_post_meta($item->ID, 'event_location', true);
        $attachment_url = get_template_directory_uri() . '/assets/img/rome.jpg';
        $attachment = get_post_meta($item->ID, 'event_location_image_id', true);
        if (!empty($attachment)) {
            $attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
        }
        $start_date = get_post_meta($item->ID, 'start_date', true);
        $event_tour_package = get_post_meta($item->ID, 'event_tour_package', true);
        $tour_img = get_post_meta($item->ID, 'tour_img', true);
        if (empty($tour_img)) {
            $tour_img = 'https://i.travelapi.com/hotels/25000000/24160000/24157900/24157865/e01875da_y.jpg';
        }
        if (is_user_logged_in()) {
            $liked = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE user_id = '{$current_user->ID}' AND feed_id = '$item->ID'");
            $going_status = $wpdb->get_var("SELECT status FROM `event_going` WHERE post_id='{$item->ID}' AND user_id='{$current_user->ID}'");
        }
        else {
            $liked = 0;
        }
        $like_count = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE feed_id = '{$item->ID}'");
        $going_count = $wpdb->get_var("SELECT COUNT(*) FROM `event_going` WHERE post_id = '{$item->ID}' AND status=1");
        ?>
        <div class="col-md-4 post-md-4 box">
            <div class="post-area-content">
                <div class="post-content">
                    <div class="post_header">
                        <div class="post_profile_img">
                            <div class="for_img">
                                <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>">
                                    <img alt="" src="<?php echo um_get_avatar_url(um_get_avatar('', $item->post_author, 52)); ?>">
                                </a>
                            </div>
                        </div>
                        <?php
                        $display_name = um_user('display_name'); ?>
                        <div class="header_user_content">
                            <div class="content_mobile">
                                <h4 class="mobile_agent_name" style="margin-bottom: 20px;">
                                    <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>"><?php echo $display_name; ?> </a>
                                    <span class="date_pub_mobile">
                                        <p style="margin: 0px; font-size: 10px">
                                            <?php echo human_time_diff(strtotime($item->post_date), current_time('timestamp')); ?>
                                        </p>
                                    </span>
                                </h4>

                                <div class="right-section-dot dropdown-toggle text-right" data-toggle="dropdown" aria-expanded="false">
                                    <a href="#" class="Pdeltebtn"><i class="fas fa-ellipsis-h"></i></a>
                                    <br>
                                    <button style="padding: 0px 10px;line-height: 12px;" class="btn btn-success">Created an <br> Event</button>
                                </div>

                                <!-- Post Delete Option PC-->
                                <div id="PostDId" class="PostDelete dropdown-menu" role="menu" aria-labelledby="menu1">
                                    <li><a href="#">Turn On notifications For this post</a></li>
                                    <li class="ShowITc"><a href="#">Show in tab</a></li>
                                    <li><a href="#">Hide From timeline</a></li>
                                    <li><a href="#">Copy link to this pos1</a></li>

                                </div>
                                <!--End Post Delete Option -->

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="post-image">
                        <img src="<?php echo $attachment_url; ?>">
                    </div>

                    <div class="post-content-txt">
                        <div class="event_date">
                            <div class="event_date text-center">
                                <h5 class="color_d_p"><?php echo date('M', $start_date); ?></h5>
                                <h5><?php echo date('d', $start_date); ?></h5>
                            </div>
                        </div>
                        <div class="event_details">
                            <div class="event_details_content">
                                <p class="title_fes"><?php echo $event_name; ?></p>
                                <p class="date_fes"><?php echo event_week_time_display($start_date); ?> - <?php echo $event_location; ?></p>
                                <p class="peoplegoing_fes"><?php echo number_format($going_count); ?> people going</p>
                            </div>
                        </div>
                        <?php if(is_user_logged_in()) { ?>
                        <div class="event_button">
                            <div class="event_inner_div">
                                <!-- Single button -->
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-check"></i> <span class="event-going-current-status"><?php echo $going_status == 1 ? 'Going' : 'Not Going'; ?></span> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu event-going" post_id="<?php echo $item->ID; ?>">
                                        <li status="1"><a href="#">Going</a></li>
                                        <li status="0"><a href="#">Not Going</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if (!empty($event_tour_package)) { ?>
                            <div class="tour_details_evvent">
                                <div class="event_display_table_cell">
                                    <img src="<?php echo $tour_img; ?>">
                                </div>
                                <div class="event_display_image_cell">
                                    <p>BALI<?php echo $event_tour_package; ?></p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="buttons_for_mobilepost">
                        <hr class="style-mobile-six">
                        <div class="main_buttons_div">
                            <div class="like_mobile <?php echo is_user_logged_in()?'like-the-post':'login-alert' ?>">
                                <a href="#" feedid="<?php echo $item->ID; ?>">
                                    <i class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>
                                    <span><?php echo $like_count; ?></span>
                                </a>
                            </div>
                            <div class="comment_mobile">
                                <a href="#">
                                    <i class="far fa-comment-alt"></i>
                                    Comment
                                </a>
                            </div>
                            <div class="share_mobile">
                                <a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
                                    <i class="fas fa-share"></i> Share
                                </a>
                                <div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2" aria-labelledby="menu2" style="right: 40px;left: inherit;">
                                    <li onclick="share_feed(<?php echo $item->ID; ?>)">Share To Timeline</li>
                                    <li><a target="_blank" href="https://web.whatsapp.com/send?text=<?php echo substr($value->post_content, 0, 80) . "  ( see-more ) "; ?>https://www.travpart.com/English/my-time-line/?user=<?php echo esc_html($username); ?>">Share to whats app</a></li>
                                    <li class="mores"><a class="a2a_dd" href="#"><i class="fas fa-plus-square"></i> More...</a></li>
                                </div>
                            </div>
                        </div>

                        <?php
                        $comments = $wpdb->get_results("SELECT comment,user_id,user_login FROM `user_feed_comments`,`wp_users` WHERE wp_users.ID=user_id AND `feed_id` = '{$item->ID}' ORDER BY user_feed_comments.id DESC");
                        if (!empty($comments)) {
                            foreach ($comments as $comment) {
                        ?>
                                <div class="user-comment-area">
                                    <?php echo get_avatar($comment->user_id, 50);  ?>
                                    <div class="another-user-section">
                                        <a href="<?php home_url('user/') . $comment->user_login; ?>"><?php echo um_get_display_name($comment->user_id); ?></a>
                                        <span><?php echo $comment->comment;  ?></span>
                                    </div>
                                </div>
                        <?php }
                        } ?>

                        <?php if(is_user_logged_in()) { ?>
                        <div class="user-comment-area submit-comment">
                            <?php echo get_avatar($current_user->ID, 50);  ?>
                            <input placeholder="Write a Comment" type="text" name="feed_comment" required="">
                            <input type="hidden" name="feed_id" value="<?php echo $item->ID; ?>">
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php }

    wp_die();
}


add_action('wp_ajax_loading_more_shortpost_with_video', 'ajax_loading_more_shortpost_with_video');
add_action('wp_ajax_nopriv_loading_more_shortpost_with_video', 'ajax_loading_more_shortpost_with_video');
function ajax_loading_more_shortpost_with_video()
{
    global $wpdb;
    if (is_user_logged_in()) {
        $current_user = wp_get_current_user();
    }
    $pageNum = filter_input(INPUT_GET, 'pageNum', FILTER_SANITIZE_NUMBER_INT);
    $pageNum = ($pageNum > 0) ? ($pageNum - 1) : 0;
    $totalPost = $wpdb->get_var("SELECT COUNT(*) FROM `wp_posts`,`wp_postmeta`
    WHERE wp_posts.ID=wp_postmeta.post_id AND `meta_key`='attachment' AND post_type='shortpost' AND post_status='publish'
    AND meta_value IN (SELECT ID FROM `wp_posts` WHERE `post_mime_type` LIKE '%video%')");

    $post_per_page = 15;
    if (ceil($totalPost / $post_per_page) < $pageNum) {
        return;
    }
    $postOffset = $pageNum * $post_per_page;

    $hide_sql = '';
    if(is_user_logged_in()) {
        $hide_sql = " AND wp_posts.ID NOT IN(SELECT short_post_id FROM `short_posts_hide_from_timeline` WHERE user_id='{$current_user->ID}') ";
    }
    $videoPosts = $wpdb->get_results("SELECT wp_posts.ID,post_author,user_login,post_title,post_date,post_content FROM `wp_posts`,`wp_postmeta`,`wp_users`
    WHERE wp_posts.ID=wp_postmeta.post_id AND post_author=wp_users.ID AND `meta_key`='attachment' AND post_type='shortpost' AND post_status='publish'
    AND meta_value IN (SELECT ID FROM `wp_posts` WHERE `post_mime_type` LIKE '%video%')
    {$hide_sql}
    ORDER BY post_date DESC LIMIT {$postOffset},{$post_per_page}");

    if (empty($videoPosts)) {
        return;
    }

    foreach ($videoPosts as $item) {
        if (is_user_logged_in()) {
            $liked = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE user_id = '{$current_user->ID}' AND feed_id = '$item->ID'");
        }
        else {
            $liked = 0;
        }
        $like_count = $wpdb->get_var("SELECT COUNT(*) FROM `user_feed_likes` WHERE feed_id = '$item->ID'");
    ?>

        <div class="col-md-4 post-md-4 box">
            <div class="post-area-content">
                <div class="post-content">
                    <div class="post_header">
                        <div class="post_profile_img">
                            <div class="for_img">
                                <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>">
                                    <img alt="" src="<?php echo um_get_avatar_url(um_get_avatar('', $item->post_author, 52)); ?>">
                                </a>
                            </div>
                        </div>
                        <?php
                        $display_name = um_user('display_name'); ?>
                        <div class="header_user_content">
                            <div class="content_mobile">
                                <h4 class="mobile_agent_name" style="margin-bottom: 20px;margin-top: 5px;">
                                    <a class="title_post_username" href="https://www.travpart.com/English/my-time-line/?user=<?php echo $item->user_login; ?>"><?php echo $display_name; ?> </a>
                                    <span class="date_pub_mobile">
                                        <p style="margin: 0px; font-size: 10px">
                                            <?php echo human_time_diff(strtotime($item->post_date), current_time('timestamp')); ?>
                                        </p>
                                    </span>
                                    <?php if (get_post_meta($item->ID, 'lookingfriend', true) == 1) { ?>
                                        <!--<p class="changes_for_mob button_green" style="background: #22B14C;color: #fff;padding: 5px 10px;border-radius: 3px;position: absolute;">Looking For A Travel Friend</p>-->
                                        <a href="https://www.travpart.com/English/people/" class="changes_for_mob  button_green">Looking For A Travel Friend</a>
                                    <?php } ?>
                                </h4>
                                <p class="mobiel_feeling"></p>

                                <!-- Right dot dropdown -->
                                <div class="right-section-dot dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <a href="#" class="Pdeltebtn"><i class="fas fa-ellipsis-h"></i></a>
                                </div>
                                <!-- Right dot dropdown -->
                                <?php
                                $post_edit_text = preg_replace('/[^A-Za-z0-9 ]/', '', $item->post_content);
                                ?>
                                <!-- Post Delete Option PC-->
                                <div id="PostDId" class="PostDelete dropdown-menu" role="menu" aria-labelledby="menu1">
                                    <?php if (is_user_logged_in()) {
                                        $author_id = get_post_field('post_author', $item->ID);
                                        if ($author_id == get_current_user_id()) { ?>
                                            <li class="drp-edit" onclick="editid('<?php echo $item->ID; ?>','<?php echo $post_edit_text; ?>')"><a href="#">Edit Post</a></li>
                                            <li class="drp-delete" onclick="getid('<?php echo $item->ID; ?>')"><a href="#">Delete</a></li>
                                    <?php
                                        }
                                    } ?>
                                    <li><a href="#">Turn On notifications For this post</a></li>
                                    <li class="ShowITc"><a href="#">Show in tab</a></li>
                                    <li class="hide-from-timeline" postid="<?php echo $item->ID ?>"><a href="#">Hide From timeline</a></li>
                                    <li><a target="_blank" href="https://www.travpart.com/English/large-single-post/">Copy link to this post</a></li>

                                </div>
                                <!--End Post Delete Option -->



                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <?php if (!empty(get_post_meta($item->ID, 'feeling', true))) { ?>
                            <span class="short-post-feeling">
                                <?php echo $item->user_login . ' is feeling ' . base64_decode(get_post_meta($item->ID, 'feeling', true)); ?>
                            </span>
                        <?php } else if (!empty(get_post_meta($item->ID, 'tag_user_id', true))) { ?>
                            <span class="short-post-tagfriend">
                                <?php echo $item->user_login . ' is with ' . um_get_display_name(get_post_meta($item->ID, 'tag_user_id', true)); ?>
                            </span>
                        <?php } ?>
                    </div>

                    <?php
                    $attachment = get_post_thumbnail_id($value->ID);
                    $attachment_url = '';
                    if (empty($attachment)) {
                        $attachment = get_post_meta($item->ID, 'attachment', true);
                    }
                    if (!empty($attachment)) {
                        if (wp_attachment_is('image', $attachment)) {
                            $attachment_url = wp_get_attachment_image_url($attachment, array(200, 200));
                        } else {
                            $attachment_url = wp_get_attachment_url($attachment);
                        }
                    } else if (!empty(get_post_meta($item->ID, 'tour_img', true))) {
                        $attachment_url = get_post_meta($item->ID, 'tour_img', true);
                    }
                    $tour_post_id = get_post_meta($item->ID, 'tour_post', true);
                    if (!empty($attachment_url)) {
                    ?>
                        <div class="post-image">
                            <?php if (!is_array(get_post_meta($item->ID, 'attachment'))) { ?>
                                <?php if (empty($attachment) || wp_attachment_is('image', $attachment)) { ?>
                                    <a href="<?php echo !empty($tour_post_id) ? get_permalink($tour_post_url) : '#'; ?>">
                                        <img src="<?php echo $attachment_url; ?>">
                                    </a>
                                <?php } else { ?>
                                    <video src="<?php echo $attachment_url; ?>" controls="controls"></video>
                                <?php } ?>
                                <?php } else {
                                foreach (get_post_meta($item->ID, 'attachment') as $row) {
                                    $attachment_url = wp_get_attachment_url($row);
                                ?>
                                    <?php if (wp_attachment_is('image', $row)) { ?>
                                        <div class="image_div" style="background: url('<?php echo $attachment_url; ?>');background-size: cover;background-position: center center;">
                                            <a href="<?php echo $attachment_url; ?>" data-fancybox="gallery"></a>
                                        </div>
                                    <?php } else {
                                        $video_thumbnail_url = wp_get_attachment_url(get_post_meta($row, '_kgflashmediaplayer-poster-id', true)) ?>
                                        <video poster="<?php echo $video_thumbnail_url ?>"
                                            src="<?php echo $attachment_url; ?>" controls="controls"></video>
                                    <?php } ?>
                            <?php
                                }
                            } ?>
                        </div>
                    <?php } ?>

                    <?php if (!empty(get_post_meta($item->ID, 'location', true))) { ?>
                        <div class="post-category">
                            <a href="#"><?php echo get_post_meta($item->ID, 'location', true); ?></a>
                        </div>
                    <?php } ?>

                    <div class="post-content-txt">
                        <div class="more">
                            <?php echo strip_tags($item->post_content); ?>
                        </div>
                    </div>

                    <?php if (!empty(get_post_meta($item->ID, 'tour_post', true))) { ?>
                        <div class="post-category">
                            <a href="<?php echo get_permalink(get_post_meta($item->ID, 'tour_post', true)); ?>">
                                <?php echo get_post_meta($item->ID, 'tour_title', true); ?>
                            </a>
                        </div>
                        <br>
                    <?php } ?>
                    <div class="buttons_for_mobilepost">
                        <hr class="style-mobile-six" />
                        <div class="main_buttons_div">
                            <div class="like_mobile <?php echo is_user_logged_in()?'like-the-post':'login-alert' ?>">
                                <a href="#" feedid="<?php echo $item->ID; ?>">
                                    <i class="far fa-thumbs-up <?php echo $liked == 1 ? 'liked' : ''; ?>"></i>
                                    <span><?php echo $like_count; ?></span>
                                </a>
                            </div>
                            <div class="comment_mobile">
                                <a href="#">
                                    <i class="far fa-comment-alt"></i>
                                    Comment
                                </a>
                            </div>
                            <div class="share_mobile">
                                <!-- pc view -->
                                <!-- <a href="#" onclick="share_feed(<?php //echo $item->ID; 
                                                                        ?>)">
                        <i class="fas fa-share"></i> Share </a> -->

                                <a href="#" class="dropbtn dropdown-toggle" data-toggle="dropdown">
                                    <i class="fas fa-share"></i> Share</a>


                                <!--Share My DropDown PC-->
                                <div id="myDropdown" class="ShareDropdown dropdown-menu" role="menu2" aria-labelledby="menu2" style="right: 40px;left: inherit;">
                                    <li onclick="share_feed(<?php echo $item->ID; ?>)">Share To Feed</li>
                                    <li><a target="_blank" href="https://web.whatsapp.com/send?text=<?php echo substr($value->post_content, 0, 80) . "  ( see-more ) "; ?>https://www.travpart.com/English/my-time-line/?user=<?php echo esc_html($username); ?>">Share to whats app</a></li>

                                    <li class="mores"><a class="a2a_dd" href="#"><i class="fas fa-plus-square"></i> More...</a></li>

                                </div>

                                <!--End My DropDown-->


                                <!-- End Pc view -->
                            </div>
                        </div>
                        <?php
                        $comments = $wpdb->get_results("SELECT comment,user_id,user_login FROM `user_feed_comments`,`wp_users` WHERE wp_users.ID=user_id AND `feed_id` = '{$item->ID}' ORDER BY user_feed_comments.id DESC");
                        if (!empty($comments)) {
                            foreach ($comments as $comment) {
                        ?>
                                <div class="user-comment-area">
                                    <?php echo get_avatar($comment->user_id, 50);  ?>
                                    <div class="another-user-section">
                                        <a href="https://www.travpart.com/English/my-time-line/?user=<?php echo $comment->user_login; ?>"><?php echo um_get_display_name($comment->user_id); ?></a>
                                        <span><?php echo $comment->comment;  ?></span>
                                    </div>
                                </div>
                        <?php }
                        } ?>

                        <?php if(is_user_logged_in()) { ?>
                        <div class=" user-comment-area submit-comment">
                            <?php echo get_avatar($current_user->ID, 50);  ?>
                            <input placeholder="Write a Comment" type="text" name="feed_comment" required="">
                            <input type="hidden" name="feed_id" value="<?php echo $item->ID; ?>">
                        </div>
                        <?php } ?>
                    </div>
                </div>

            </div>
        </div>
    <?php }

    wp_die();
}


add_action('wp_ajax_loading_more_photo', 'ajax_loading_more_photo');
function ajax_loading_more_photo()
{
    global $wpdb;
    $userid = filter_input(INPUT_GET, 'userid', FILTER_SANITIZE_NUMBER_INT);
    $pageNum = filter_input(INPUT_GET, 'pageNum', FILTER_SANITIZE_NUMBER_INT);
    $pageNum = ($pageNum > 0) ? ($pageNum - 1) : 0;
    $photo_per_page = 6;

    $limit = $pageNum * $photo_per_page . ',' . $photo_per_page;

    if (!($user = get_user_by('id', $userid))) {
        echo json_encode(array('success'=>false));
        wp_die();
        return;
    }

    $total_photo = $wpdb->get_var("SELECT COUNT(*) FROM `wp_posts` WHERE `post_author` = '{$user->ID}'
    AND post_status='inherit' AND `post_type` = 'attachment'");

    if (floor($total_photo / $photo_per_page) < $pageNum) {
        echo json_encode(array('success'=>false));
        wp_die();
        return;
    }

    $user_photos = $wpdb->get_results("SELECT guid as url,post_mime_type as type FROM `wp_posts` WHERE `post_author` = '{$user->ID}'
    AND post_status='inherit' AND `post_type` = 'attachment'
    ORDER BY `post_date` DESC LIMIT {$limit}");

    echo json_encode(array('success'=>true, 'photos'=>$user_photos));

    wp_die();
}

//add user cover comment
add_action('wp_ajax_user_cover_comment', 'ajax_user_cover_comment');
function ajax_user_cover_comment()
{
    global $wpdb;
    $owner = filter_input(INPUT_POST, 'owner', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1)));
    $comment = filter_input(INPUT_POST, 'comment', FILTER_SANITIZE_STRING);
    if (!get_user_by('id', $owner)) {
        echo json_encode(array('success' => false, 'error_msg' => 'User is not existed.'));
    } else {
        $current_user = wp_get_current_user();
        $wpdb->insert(
            'user_cover_comment',
            array(
                'owner' => $owner,
                'commentator' => $current_user->ID,
                'comment' => $comment
            )
        );
        echo json_encode(array('success' => true));
    }
    wp_die();
}

//delete attachment
add_action('wp_ajax_user_delete_attachment', 'ajax_user_delete_attachment');
function ajax_user_delete_attachment()
{
    $postid = filter_input(INPUT_POST, 'postid', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1)));
    if(get_post($postid)->post_author!=wp_get_current_user()->ID) {
        echo json_encode(array('success' => false));
    }
    else if(!wp_delete_attachment($postid)) {
        echo json_encode(array('success' => false));
    }
    else {
        echo json_encode(array('success' => true));
    }
    wp_die();
}


//unsubscribe email
add_action('wp_ajax_nopriv_unsubscribe_email', 'ajax_unsubscribe_email');
add_action('wp_ajax_unsubscribe_email', 'ajax_unsubscribe_email');
function ajax_unsubscribe_email()
{
    global $wpdb;
    $type=filter_input(INPUT_GET, 'type', FILTER_SANITIZE_NUMBER_INT);
    $subscribe=filter_input(INPUT_GET, 'subscribe', FILTER_SANITIZE_NUMBER_INT);
    if(is_null($type) || $type<0 || is_null($subscribe) || $subscribe<0) {
        echo json_encode(array('status'=>false));
        wp_die();
    }
    else if(is_user_logged_in()) {
        $email=wp_get_current_user()->user_email;
    }
    else if(!empty($_COOKIE['email'])) {
        $email=htmlspecialchars($_COOKIE['email']);
    }
    else {
        echo json_encode(array('status'=>false));
        wp_die();
    }
    $subscribe=$subscribe==0?0:1;
    if($type==0) {
        for($i=1; $i<=7; $i++) {
            if($wpdb->get_var("SELECT COUNT(*) FROM `unsubscribed_emails` WHERE email='{$email}' AND type='{$i}'")>0) {
                $wpdb->query("UPDATE `unsubscribed_emails` SET `subscribe`='{$subscribe}',`modify_date`=CURRENT_TIMESTAMP WHERE email='{$email}' AND type='{$i}'");
            }
            else {
                $wpdb->insert('unsubscribed_emails', array('email'=>$email, 'type'=>$i, 'subscribe'=>$subscribe));
            }
        }
    }
    else if($wpdb->get_var("SELECT COUNT(*) FROM `unsubscribed_emails` WHERE email='{$email}' AND type='{$type}'")>0) {
        $wpdb->query("UPDATE `unsubscribed_emails` SET `subscribe`='{$subscribe}',`modify_date`=CURRENT_TIMESTAMP WHERE email='{$email}' AND type='{$type}'");
    }
    else {
        $wpdb->insert('unsubscribed_emails', array('email'=>$email, 'type'=>$type, 'subscribe'=>$subscribe));
    }
    echo json_encode(array('status'=>true));
    wp_die();
}

function ajax_hide_post_from_timeline()
{
    global $wpdb;
    
    $current_user = wp_get_current_user();
    $post_id = filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT);

    if($post_id<=0) {
        echo json_encode(array('success'=>false));
    }
    else {
        $already_exist = $wpdb->get_var("SELECT COUNT(*) FROM short_posts_hide_from_timeline WHERE short_post_id='{$post_id}' AND user_id='{$current_user->ID}'");

        if (empty($already_exist)) {
            $wpdb->insert(
                'short_posts_hide_from_timeline',
                array(
                'short_post_id' => $post_id,
                'user_id' => $current_user->ID
            ),
                array('%d', '%d')
            );
            echo json_encode(array('success'=>true));
        }
        else {
            echo json_encode(array('success'=>false));
        }
    }
    wp_die();
}
add_action('wp_ajax_hide_post_from_timeline', 'ajax_hide_post_from_timeline');


//Fix profile photo rotating issue
add_action('um_before_upload_db_meta', 'fix_profile_photo_rotate', 10, 2);
function fix_profile_photo_rotate($user_id,$field_key)
{
    if(!empty($user_id)) {
        $upload_dir = UM()->uploader()->get_upload_base_dir().$user_id.'/';
        $imagelist = scandir($upload_dir);
        foreach($imagelist as $i) {
            if(stripos($i, 'temp')!==FALSE) {
                $img_exif = exif_read_data($upload_dir.$i);
                if(!empty($img_exif['Orientation']) && $img_exif['Orientation']!=1) {
                    $image = wp_get_image_editor($upload_dir.$i);
                    if($img_exif['Orientation']==6) {
                        $image->rotate(-90);
                    }
                    else if($img_exif['Orientation']==8) {
                        $image->rotate(90);
                    } 
                    $image->save($upload_dir.$i);
                }
            }
        }
        
    }
}

// Register  custom route to access On Android App
add_action('rest_api_init', 'app_blog_api');
function app_blog_api()
{

    register_rest_route('v2/androidapp', '/app_blog/', array(
        'methods' => 'GET',
        'callback' => 'blog_posts_for_app',
    ));
}

// Code added by farhan on 30 May  for User Clicks on Tour

/*   Function to  add user clicks in db */

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

// Function for user sharing and getting points.this is part of game project.
/*
function user_shared_points(){
    global $wpdb;
    $tour_id = intval($_POST['id1']);

    if(is_user_logged_in()) {
        $current_user=wp_get_current_user();
        if($wpdb->get_var("SELECT COUNT(*) FROM `wp_tour_sharing_points` WHERE user_id='{$current_user->ID}' AND tour_id='{$tour_id}'")==0) {
            $wpdb->insert(
                $wpdb->prefix.'tour_sharing_points',
                array(
                    'user_id' => $current_user->ID,
                    'tour_id' => $tour_id
                ),
                array(
                    '%d',
                    '%d'
                )
            );
            update_user_meta($current_user->ID, 'game_points', get_user_meta(wp_get_current_user()->ID, 'game_points', true)+get_option('_travpart_vo_option', null)['tp_pts_tor_pkg']);
        }
    }
}
*/

// Function for tour clicks count
function click_to_update()
{
    $tour_id =  $_POST['id'];
    $clicks = $_POST['id1'];
    $ip = get_client_ip();
    global $wpdb;
    $wpdb->insert(
        $wpdb->prefix . 'tour_clicks',
        array(
            'tour_id' => $tour_id,
            'clicks' => $clicks,
            'ip' => $ip
        ),
        array(
            '%s',
            '%d',
            '%s'
        )
    );
}

// Function for live chat click count
function live_chat_click()
{
    global $wpdb;
    $tour_id =  $_POST['id'];
    $clicks = $_POST['id1'];

    $last_data = $wpdb->get_row("SELECT * FROM `wp_live_chat_clicks` WHERE `tour_id` = '{$tour_id}'");
    if (empty($last_data)) {
        $wpdb->insert(
            'wp_live_chat_clicks',
            array(
                'tour_id' => $tour_id,
                'clicks' => 1
            ),
            array(
                '%d',
                '%d'
            )
        );
    } else {
        $click_count = $last_data->clicks;
        $click_count = $click_count + 1;
        $wpdb->update(
            'wp_live_chat_clicks',
            array(
                'clicks' => $click_count, // string 
            ),
            array('tour_id' => $tour_id),
            array('%d'),
            array('%d')
        );
    }
}

// Register Script for clicks count ( By farhan)
 function my_enqueue($hook) {
          wp_enqueue_script('ajax-script', home_url() . '/wp-content/themes/bali/js/countclicks.js' , array(
          'jquery'
          ), '1.1');

          // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value

          wp_localize_script('ajax-script', 'ajax_object', array(
          'ajax_url' => admin_url('admin-ajax.php') ,
          'we_value' => 'txt'
          ));   
          }
        add_action( 'admin_enqueue_scripts', 'my_enqueue' );
        add_action('wp_enqueue_scripts', 'my_enqueue');
        add_action( 'wp_ajax_click_to_update', 'click_to_update' );
        add_action( 'wp_ajax_nopriv_click_to_update', 'click_to_update' );
        //add_action( 'wp_ajax_user_shared_points', 'user_shared_points' );
        //add_action( 'wp_ajax_nopriv_user_shared_points', 'user_shared_points' );
        add_action( 'wp_ajax_live_chat_click', 'live_chat_click' );
        add_action( 'wp_ajax_nopriv_live_chat_click', 'live_chat_click' );


              // function to create clicks table need to run once only

?>
<?php
//add_action('wp_head', 'metafortour');

function metafortour($title,$des,$Image,$url){ ?>
    <meta property="og:type"               content="article" /> 
    <meta property="og:url"          content="<?php echo $url ?>" /> 
    <meta property="og:title" content="<?php echo $title ?>" />
    <meta property="og:description"   content="<?php echo $des ?>"/>
    <meta property="og:image"         content="<?php echo $Image ?>" />
   
<?php
}

// Store keywords on search with count

function search_kw_storing($kw)
{
    global $wpdb;
    $get_search = $wpdb->get_row("SELECT id,count FROM wp_search_keywords WHERE kw LIKE '{$kw}' ");
    if (empty($get_search)) {
        $wpdb->insert(
            $wpdb->prefix . 'search_keywords',
            array(
                'kw' => $kw,
                'count' => 1
            ),
            array(
                '%s',
                '%d'
            )
        );
        $kw_id = $wpdb->insert_id;
    } else {
        $next_count = $get_search->count + 1;
        $wpdb->update(
            $wpdb->prefix . 'search_keywords',
            array(
                'count' => $next_count
            ),
            array('kw' => $kw),
            array(
                '%d'    // value2
            ),
            array('%s')
        );
        $kw_id = $get_search->id;
    }
    if (is_user_logged_in()) {
        $current_user = wp_get_current_user();
        if ($wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}user_trace
            WHERE user_id={$current_user->ID} AND type='keyword' AND value='{$kw_id}'") == 0) {
            $wpdb->insert(
                $wpdb->prefix . 'user_trace',
                array('user_id' => $current_user->ID, 'type' => 'keyword', 'value' => $kw_id)
            );
        }
    }
}

// Added on 8 Oct for sending mobile notifactions

function to_get_user_device_token_from_wp_id($tour_creator_id){
    global $wpdb;
/*
$get_user_data =$wpdb->get_row("SELECT * FROM `wp_users` where  ID= " . $tour_creator_id);
$user_email = $get_user_data->user_email;
/*
if(!empty($user_email)) {

$get_user_data_tabl2 = $wpdb->get_row("SELECT * FROM `user` where  email = '{$user_email}'");
$user_id = $get_user_data_tabl2->userid;

if(!empty($user_id)) {

$get_user_data_tabl3 =$wpdb->get_row("SELECT * FROM `sessions` where  user_id = " . $user_id);
$user_device_token = $get_user_data_tabl3->device_token;

return $user_device_token;
}
}*/

$get_user_data =$wpdb->get_row("SELECT device_token FROM `sessions` where  wp_user_id= " . $tour_creator_id);
if($get_user_data){
$user_device_token = $get_user_data->device_token;
return $user_device_token;
}
}

function sendtomobilefunction($message, $id) {



    $url = 'https://fcm.googleapis.com/fcm/send';



    $fields = array (
            'registration_ids' => array (
                    $id
            ),
            'data' => array (
                    "message" => $message
            )
    );


    $headers = array (
            'Authorization: key=' . "AIzaSyCUhKe-YxeR_gBYlGnhiuxzphZPYMUPXos",
            'Content-Type: application/json'
    );


    

    $ch = curl_init ();
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS,  json_encode ( $fields));

    $result = curl_exec ( $ch );
    //print_r( $result );
    curl_close ( $ch );
}




    function post_published_notification( $ID, $post ) {

    global $wpdb;
    $seller_id = $post->post_author;
    $seller_name = get_the_author_meta( 'user_nicename', $seller_id );

    $get_users_list = $wpdb->get_results("SELECT * FROM `wp_users`");


    foreach ($get_users_list as $value) {

    $user_id = $value->ID;

    $token= to_get_user_device_token_from_wp_id($user_id);

    if(!empty($token)){

    $msg = "Check out '" .$post->post_title. "' a new published blog post";

    $notification = '<a href="'.get_permalink( $ID ).'">'.$msg.'</a>';
    $wpdb->insert('notification', array(
    'wp_user_id'=>$user_id,
    'content'=>$notification,'identifier'=>'new_blog_post'),
    array('%d','%s','%s'));
    sendtomobilefunction($msg,$token);
    } 
    }

    }
   // add_action( 'publish_post', 'post_published_notification', 10, 2 );


    // Potential Seller by Pappu

    function potential_buyer_assets(){

    wp_enqueue_style('potential-buyers-style', get_template_directory_uri() .'/css/potential_buyers.css', null, time());
}
add_action('wp_enqueue_scripts', 'potential_buyer_assets');

/*function to add async to all scripts*/
function js_async_attr($tag){

# Do not add async to these scripts
$scripts_to_exclude = array('script-name1.js', 'script-name2.js', 'script-name3.js');
 
foreach($scripts_to_exclude as $exclude_script){
    if(true == strpos($tag, $exclude_script ) )
    return $tag;  
}

# Add async to all remaining scripts
return str_replace( ' src', ' async="async" src', $tag );
}
//add_filter( 'script_loader_tag', 'js_async_attr', 10 );



function add_stylesheet_attributes( $html, $handle ) {
    //if ( 'font-awesome-5' === $handle ) {
        return str_replace( "rel='stylesheet'", "rel='stylesheet' async='async' disable", $html );
    
    //return $html;
}
//add_filter( 'style_loader_tag', 'add_stylesheet_attributes', 10, 2 );

// if profile pic is not uploaded by user,it will give notifaction to user every 2 day


add_action( '__profile_pic_checker', '__profile_pic_checker' );
 function __profile_pic_checker(){

        global $wpdb;

    $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");

    foreach ($get_users_list as $value) {

      $user_have_no_image =  um_get_user_avatar_url ($value->ID);

      if( $user_have_no_image=="https://www.travpart.com/English/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg" ){


        $msg ="<a href='https://www.travpart.com/English/timeline'>Upload a profile picture to get attracts more connection</a>";
                $wpdb->insert('notification', array('wp_user_id'=>$value->ID, 'content'=>$msg,'identifier'=>'profile_pic_missing'));

        $token= to_get_user_device_token_from_wp_id($value->ID);

        if(!empty($token)){

        $msg = " Upload a profile picture to get attracts more connection";

        //sendtomobilefunction($msg,$token);
          notification_for_user( $token,$msg,'profile_pic_missing');

           }
         }
     }
 }

// check user education ,school, and send notifaction based weekly

add_action( '___user_info_checker', '__user_info_checker' );
 function __user_info_checker(){

        global $wpdb;

    $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");

    foreach ($get_users_list as $value) {

      $check_edu_level = get_user_meta($value->ID,'edu_level',true);


 $careers = $wpdb->get_results("SELECT * FROM `wp_career` WHERE userid=" . $value->ID);
   $educations = $wpdb->get_results("SELECT * FROM `wp_education` WHERE userid=" . $value->ID);

      if( empty( $check_edu_level )  || empty ($careers)  || empty($educations)){

      $token= to_get_user_device_token_from_wp_id($value->ID);

        $msg ="<a href='https://www.travpart.com/English/user/?profiletab=profile'>Fill out your profile page completely so that you can connect with your friends !</a>";
                $wpdb->insert('notification', array('wp_user_id'=>$value->ID, 'content'=>$msg,'identifier'=>'profile_data_missing'));

        if(!empty($token)){

        $msg = " Fill out your profile page completely so that you can connect with your friends !";

        //sendtomobilefunction($msg,$token);
       notification_for_user( $token,$msg,'profile_data_missing');

        } 
    }

  }
}


// check  user posts if he is active ,cron job
add_action( '__user_shortpost_checker', '__user_shortpost_checker' );
//__user_shortpost_checker();
function __user_shortpost_checker(){

    global $wpdb;


$all_members = get_users( array(
'orderby'             => 'ID',
'order'               => 'ASC',
'has_published_posts' => array( 'shortpost' )
// 'blog_id'             => absint( get_current_blog_id() )
) );
  foreach($all_members as $member){
    $post_count = count_user_posts($member->ID);
    if(empty($post_count)) {
        $bad_writers[] = $member->ID;
        continue;
    } else {
        // do something;
    }
}

foreach ($bad_writers as $value) {

$token= to_get_user_device_token_from_wp_id($value);

$msg ="<a href='https://www.travpart.com/English/timeline/'>You have not shared anything in your page for a while. Your connection miss you</a>";
                $wpdb->insert('notification', array('wp_user_id'=>$value, 'content'=>$msg,'identifier'=>'nopost_for_long_time'));

    if(!empty($token)){

    $msg = "You have not shared anything in your page for a while. Your connection miss you";

    //sendtomobilefunction($msg,$token);
   notification_for_user( $token,$msg,'nopost_for_long_time');

    } 
}

}


// check user plans and send notifaction

add_action( '__user_plan_checker', '__user_plan_checker' );
 function __user_plan_checker(){

global $wpdb;
    $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");

    foreach ($get_users_list as $value) {

        
        $today_date = date("d/m/Y H:i:s");
        $plans_count = $wpdb->get_var("SELECT  COUNT(*) FROM `wp_user_plans` WHERE  user_id='{$value->ID}'");
        
    
      if( $plans_count==0 ){

      

        $token= to_get_user_device_token_from_wp_id($value->ID);

        $msg ="<a href='#'>You currently dont have any travel plans, share your plans so that your friends can join you</a>";
                $wpdb->insert('notification', array('wp_user_id'=>$value->ID, 'content'=>$msg,'identifier'=>'plans_missing'));

        if(!empty($token)){

        $msg = "You currently dont have any travel plans, share your plans so that your friends can join you";

        //sendtomobilefunction($msg,$token);
       notification_for_user( $token,$msg,'plans_missing');

        } 
    }

  }
}


// send notifaction to user near 4 PM

add_action( '__user_4pm_notifaction', '__user_4pm_notifaction' );
//__user_4pm_notifaction();
 function __user_4pm_notifaction(){

global $wpdb;

    $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");

    foreach ($get_users_list as $value) {

        
      $msg ="<a href='#'>Check out where your friends are heading for the next holiday </a>";
                $wpdb->insert('notification', array('wp_user_id'=>$value->ID, 'content'=>$msg,'identifier'=>'user_daily_notifaction'));

        $token= to_get_user_device_token_from_wp_id($value->ID);

        if(!empty($token)){

        $msg = "Check out where your friends are heading for the next holiday ";

        //sendtomobilefunction($msg,$token);
       notification_for_user( $token,$msg,'user_daily_notifaction');

        
    }

  }
}

// notifaction for fellowers check

add_action( '__user_fellower_notifaction', '__user_fellower_notifaction' );
 function __user_fellower_notifaction(){

global $wpdb;
   // $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");

        $args = array(
        'role'    => 'um_travel-advisor',
        'orderby' => 'user_nicename',
        'order'   => 'ASC'
        );
        $get_users_list = get_users( $args );

    foreach ($get_users_list as $value) {

    $followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_agent_id = '$value->ID'", ARRAY_A);
    
     if(empty($followData)){

        $msg ="<a href='https://www.travpart.com/English/timeline'>No one followed you yet, connect and post something in your timeline now ! </a>";
                $wpdb->insert('notification', array('wp_user_id'=>$value->ID, 'content'=>$msg,'identifier'=>'no_follower_notifaction'));
     }
      

        $token= to_get_user_device_token_from_wp_id($value->ID);

        if(!empty($token)){

        $msg = "No one followed you yet, connect and post something in your timeline now ! ";

        //sendtomobilefunction($msg,$token);
       notification_for_user( $token,$msg,'no_follower_notifaction');

        
  }
}
}

// user friend suggestion notifaction

add_action( '__user_friend_suggestions_notifaction', '__user_friend_suggestions_notifaction' );
 function __user_friend_suggestions_notifaction(){

global $wpdb;

 $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");

foreach ($get_users_list as $value) {
$current_user_id = $value->ID;

 $friends_check = $wpdb->get_results("SELECT user1,user2 FROM `friends_requests` WHERE ( user1 = '$value->ID'  OR user2='$value->ID') AND status=1");


  foreach ($friends_check as $f) {

        if($f->user1 == $value->ID){

        $friends [] = $f->user2;
        }
        else{

        $friends [] = $f->user1;
        }
    }

    if( count( $friends ) >0 ){

     $rand =   rand(0,count ($friends));
     $friends_check_for_friend = $wpdb->get_results("SELECT user1,user2 FROM `friends_requests` WHERE ( user1 = '$friends[$rand]'  OR user2='$friends[$rand]') AND status=1");

     foreach ($friends_check_for_friend as $f1) {
        
    

        if($f1->user1 == $friends[$rand] ){

        $friends1 [] = $f1->user2;

        }
        else {

        $friends1 [] = $f1->user1;

        }
    
    }


        $key = array_search($current_user_id, $friends1);
        if (false !== $key) {
        unset($friends1[$key]);
        }

        if(count ($friends1)>0){
        
        $rand_select = rand (0,count($friends1));

        $selected_id = $friends1[$rand_select];

        echo $friends[$rand];
            
        }

 
}
    if($selected_id>0){

        $friend_name = um_get_display_name ( $friends[$rand]);

        $_next_friend_name = um_get_display_name ( $selected_id);
        $msg ="<a href='https://www.travpart.com/English/people/'>".$friend_name." connected and followed " .$_next_friend_name."</a>";
        echo $msg;
        $wpdb->insert('notification', array('wp_user_id'=>$current_user_id, 'content'=>$msg,'identifier'=>'user_friend_suggestions'));

        $token= to_get_user_device_token_from_wp_id($value->ID);

        if(!empty($token)){

        $msg = $friend_name ." connected and followed ". $_next_friend_name;

        //sendtomobilefunction($msg,$token);
     notification_for_user( $token,$msg,'user_friend_suggestions');
    }
 }

}
}

// define the um_user_login callback  after login
add_action( 'um_on_login_before_redirect', 'save_user_login_time', 5, 1 );
function save_user_login_time( $user_id ) {
    // your code here
    global $wpdb;
    $now = date('Y-m-d H:i:s', time());
    $check_session = $wpdb->get_var("SELECT id FROM `sessions` WHERE  wp_user_id='{$user_id}'");

    $get_user = $wpdb->get_row("SELECT user_login,user_email FROM `wp_users` WHERE  ID='{$user_id}'");

    if($get_user){

    $useridd = $wpdb->get_var("SELECT userid from `user` WHERE  username='{$get_user->user_login}' OR email='{$get_user->user_email}' ");

                $wpdb->update(
                'user', array(
                'online_time'=>$now,
                ), array('userid' => $useridd), array(
                '%s',

                ), array('%d')
                );

    }

           if( $check_session){

                    $wpdb->update(
                    'sessions', array(
                    'last_activity'=>$now,
                    ), array('id' => $check_session), array(
                    '%s',

                    ), array('%d')
                    );
                }
           else{

            $sql = $wpdb->prepare("INSERT INTO `sessions` (`last_activity`, `wp_user_id`) values (%s,%s)", $now,$user_id);
            $wpdb->query($sql);

            }
        }



// define the  callback  for chat check
///add_action( 'buyer_check_for_chat_with_sellers', 'buyer_check_for_chat_with_sellers' );
function buyer_check_for_chat_with_sellers() {
    // your code here
       global $wpdb;
           
        
        $get_users_list = $wpdb->get_results("SELECT userid,username,token FROM `user` WHERE access=2");

        foreach ($get_users_list as $user) {

        $userid = $user->userid;

       $check_chat= $wpdb->get_var("SELECT COUNT(*) FROM `chat_member` WHERE chatroomid='{$userid}'");


        if($check_chat==0){
        // echo $userid;
         $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$user->username}'");

         $msg ="<a href='https://www.travpart.com/English/travchat/'> you have not contacted any travel advisor yet, please reach one of them now</a>";
                $wpdb->insert('notification', array('wp_user_id'=>$wp_user_ID, 'content'=>$msg,'identifier'=>'no_seller_chat'));
         
         $token= to_get_user_device_token_from_wp_id($wp_user_ID);

            if(!empty($token)){

            $msg = " you have not contacted any travel advisor yet, please reach one of them now ";

            //sendtomobilefunction($msg,$token);
            notification_for_user( $token,$msg,'no_seller_chat');
        }
       }
     }
 }
///add_shortcode('tess','buyer_check_for_chat_with_sellers');

/**
 * Generate breadcrumbs
 * @author CodexWorld
 * @authorURL www.codexworld.com
 */
function get_breadcrumb() {
    echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
    if (is_category() || is_single()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        the_category(' &bull; ');
            if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }
    } elseif (is_page()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo the_title();
    } elseif (is_search()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }
}

function wdm_user_role_dropdown($all_roles) {

global $pagenow,$wp_roles;

    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    if( $pagenow == 'user-edit.php' ) {
        // if current user is editor AND current page is edit user page
        unset($all_roles['um_sales-agent']);
        $wp_roles->roles['um_travel-advisor']['name'] = 'Service Provider';
        $wp_roles->role_names['um_travel-advisor'] = 'Service Provider';
    }

return $all_roles;
}
add_filter('editable_roles','wdm_user_role_dropdown');


function friends_suggestions_for_timeline()
{
    if (is_user_logged_in()) {
        global $wpdb;
        $realted_users_id = array();
        $current_user = wp_get_current_user();
        $user_id = $current_user->ID;
        $edu_level = get_user_meta($user_id, 'edu_level', true);
        // $religion = get_user_meta( $user_id, 'religion' , true );
        $hometown = get_user_meta($user_id, 'hometown', true);
        $current_city = get_user_meta($user_id, 'current_city', true);
        $seeking = get_user_meta($user_id, 'seeking', true);
        $gender = get_user_meta($user_id, 'gender', true);
        $marital_status = get_user_meta($user_id, 'marital_status', true);

        if ($marital_status=='single') {
            $suggestions_gender = ($gender=='Male') ? "Female": "Male";
        }

        // removed from suggestions
        $removed_ids_query = $wpdb->get_results("SELECT `removed_user_id` FROM `wp_remove_suggestions` WHERE wp_user_id = '$user_id'");
        foreach ($removed_ids_query as $user) {
            $removed_ids [] = $user->removed_user_id;
        }
        array_push($removed_ids, $user_id);


        $args = array(
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key' => 'edu_level',
                    'value' => $edu_level,
                    'compare' => '='
                ),
                array(
                    'key' => 'hometown',
                    'value' => $hometown,
                    'compare' => '='
                ),
                array(
                    'key' => 'current_city',
                    'value' => $current_city,
                    'compare' => 'LIKE'
                ),
            ),
            'exclude' => $removed_ids,
            'include' => $user_ids
            //'role' => 'um_travel-advisor'
        );

        // added for gender as we need to show in next row not in 1st row
        $args2 = array(
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key' => 'gender',
                    'value' => $suggestions_gender,
                    'compare' => 'LIKE'
                )
            ),
        );

        $query="SELECT kw FROM user_friends_search_keywords WHERE user_id = {$current_user->ID}";
        $searchs = $wpdb->get_results($query);
        $search_ids = array();

        foreach ($searchs as $row) {
            $user = get_user_by('login', $row->kw);

            if ($user) {
                $search_ids[] = $user->ID;
            }
        }

        $phone_contact_users = $wpdb->get_col("SELECT wp_users.ID FROM `user`,`wp_users` WHERE wp_users.user_login=user.username
            AND phone IN ( SELECT contact FROM `user_contacts` WHERE wp_user_id = {$current_user->ID} )");
        $search_ids = array_merge($search_ids, $phone_contact_users);
        
        $args3 = array(
            'include'=>$search_ids
        );
        $wp_user_query2 = new WP_User_Query($args2);
        $wp_user_query3 = new WP_User_Query($args3);
        $wp_user_query = new WP_User_Query($args);
        $suggestions = $wp_user_query->get_results();
        $suggestions2 = $wp_user_query2->get_results();
        $suggestions3 = $wp_user_query3->get_results();

        return array_merge($suggestions, $suggestions3, $suggestions2);
    }
}


 function block_user_load_js_script() {
  
  if ( is_page_template('tpl-mytimeline.php') ) {
  
    wp_enqueue_script( 'js-file', get_template_directory_uri() . '/js/block_user.js?v=1');

}
  
}

add_action('wp_enqueue_scripts', 'block_user_load_js_script');


add_filter( 'post_password_expires', 'wpse_custom_post_password_expires' );
function wpse_custom_post_password_expires( $expires ) {
    return time() + 60; // Expire in 30 seconds
}

?>



