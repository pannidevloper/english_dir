
<?php
/*Template Name: matched*/
get_header();
?> 
<link rel="stylesheet" type="text/css"  href="https://www.travpart.com/English/wp-content/themes/bali/css/matched.css?v43"  />

<div class="row remove_mar">
	<div class="col-md-12 remove_pad">
		<div class="main_bg_green">
		  <div class="matched_inner_div text-center">	  	
			<h1>MATCHED !</h1>
			<div class="social_picture1">
				<img class="img_1" src="<?php bloginfo('template_url'); ?>/css/images/boy.png" />
				<img class="img_2" src="<?php bloginfo('template_url'); ?>/css/images/girl.png" />
			</div>
			<div class="content_div"> 
			
				<p class="content_white">
					Upload a profile picture to attract more travel buddies to connect you.
				</p>
				<div class="button_div">
					<a href="<?php echo home_url('/my-time-line/?user='.wp_get_current_user()->user_login); ?>" class="btn custom_btn_matched">Profile Page</a>
				</div>
				<div class="link_div">
					<a href="javascript:history.back()" class="back_link"> Back to Travpart</a>
				</div>
			</div>	
		  </div>	
		</div>
	</div>
</div>


<?php
get_footer();
?>