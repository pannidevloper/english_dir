<?php
/*
Template Name: Career and Education Viewer Page
*/

global $wpdb;
get_header();

?>
<div class="ce_v_main_area">
  <div class="row">
    <div class="col-md-12">
      <div class="profilecover">
        <img src="https://www.travpart.com/English/wp-content/uploads/2019/11/sunrise-1014712_1920.jpg"> 
      </div>
        <img src="https://www.travpart.com/English/wp-content/uploads/2019/11/profile_img.png" alt="Avatar" class="avatar">       
    <div class="p-connect-area deskaddfr">
      <a href="#"><i class="fa fa-snowflake"></i>Add Friend</a>
    </div> 
    <div class="p-connect-area mobile_conn">
      <a href="#"><i class="fa fa-snowflake"></i>Connect</a>
    </div>
    <div class="p-follow-area-top">
          <a href="#"><i class="fas fa-wifi"></i>Follow</a>
        </div>
        <div class="p-mess-area">
          <a href="#"><i class="far fa-comment-alt"></i>Messages</a>
        </div>
        <div class="p-dots-area">
          <a href="#"><p>...</p></a>
        </div>
        <div class="profile-name">
          <center><h2>Muhammad Farhan</h2></center>
        </div>
        <div class="mobile_lets_hang">
        <img src="https://www.travpart.com/English/wp-content/uploads/2019/11/lets-hangout-new.png" alt="">
        <div class="lets_text">Let's Hangout !</div>
      </div>
      </div>
  </div>
</div> 
<div class="button_area1">
  <div class="row">
    <div class="col-md-10">
      <div class="col-md-3">
      	<a href="#" class="time-btn-info" role="button">Timeline</a>
      </div>
      <div class="col-md-3">
        <a href="#" class="about-btn-info" role="button">About</a>
      </div>
      <div class="col-md-3">
        <a href="#" class="conn-btn-info" role="button">Connection</a>
      </div>
      <div class="col-md-3">
      	<a href="#" class="photo-btn-info" role="button">Photos & Videos</a>
      </div>
    </div>
  </div>
</div>
<div class="button_area2">
  <div class="row">
    <div class="col-md-6">
      <div class="col-md-2">
      </div>
      <div class="col-md-4">
        <a href="#" class="pro-gen-btn-info" role="button">General Profile</a>
      </div>
      <div class="col-md-4">
        <a href="#" class="car-edu-btn-info" role="button">Career & Education</a>
      </div>
      <div class="col-md-2">
      </div>
    </div>
  </div>
</div> 
<div class="button_area mobile-button">
  <div class="row">
    <div class="col-md-6 button-display">
      <div class="col-md-6">
        <a href="#" class="pro-gen-btn-info" role="button">General Profile</a>
      </div>
      <div class="col-md-6">
        <a href="#" class="car-edu-btn-info" role="button">Career & Education</a>
      </div>
    </div>
  </div>
</div> 
<div class="educonarea carrerwraptop career_desk">
	<div class="row">
		<div class="col-md-8 edutitle"><h3>Career</h3></div>
	</div>
	<div class="row">
		<div class="carrerwrap">
			<div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/Capture.jpg"/></div>
			<div class="carrerright">
				<h4>Retail Sales Manager</h4>
				<div class="comname">Microsoft</div>
				<div class="extime">Aug 2015 - Present . 4yrs 4 mos</div>
				<div class="exloc">Urbana Champaign, IL USA</div>
				<ul>
					<li>Responsible in Managing a Team consist of 50 people to reach the target.</li>
					<li>Stategic Directing, including business plan & sales strategy development.</li>
				</ul>
			</div>
		</div>
	</div>

</div>  
<div class="educonarea carrerwraptop expr_mobile">
	<div class="row">
		<div class="col-md-8 edutitle"><h3>Experience</h3></div>
	</div>
	<div class="row">
		<div class="carrerwrap">
			<div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/gma.png"/></div>
			<div class="carrerright">
				<h4>Founder</h4>
				<div class="comname">Gentlemen Marketing Agency</div>
				<div class="extime">Nov 2013 - Present . 6yrs 1 mo</div>
				<div class="exdes">Digital Agency specialist of China : SEO,SEM,Social Media,Word-of-mouth marketing.</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="carrerwrap">
			<div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/agence-marketing-chine.png"/></div>
			<div class="carrerright">
				<h4>Fondateur</h4>
				<div class="comname">Agence Marketing Chine</div>
				<div class="extime">Oct 2012 - Oct 2013 . 1yrs 1 mo</div>
				<div class="exdes">Digital Agency specialist of China : SEO,SEM,Social Media,Word-of-mouth marketing.</div>
			</div>
		</div>
	</div>
	<div class="ex_see_more">
		<p>See More</p><i class="fas fa-angle-down"></i>
	</div>

</div>  
<div class="educonarea carrerwraptop">
	<div class="row">
		<div class="col-md-8 edutitle"><h3>Education</h3></div>
	</div>
	<div class="row">
		<div class="carrerwrap">
			<div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/uni_logo.jpg"/></div>
			<div class="carrerright">
				<h4>University of Indiana</h4>
				<div class="comname">Master's Degree, Master of Science in Finance</div>
				<div class="extime">2015</div>
				
			</div>
		</div>

	</div>

</div>  

<?php
get_footer();
?>

<style type="text/css">
	.profilecover {
      position: relative;
  }
  .profilecover img {
      width: 100%;
      max-height: 350px;
  }
  picture.avatar img{
      width: 150px;
      height: 150px;
      border-radius: 50%;
      position: absolute;
      bottom:0%;

  }
  .p-follow-area-top{
    background: #cacaca;
    padding: 5px 10px;
    position: absolute;
    bottom: 8%;
    right: 15%;   
  }
  .p-follow-area-top a{
      color: #000;
  }
  .p-follow-area-top:hover{
      background: #1abc9c;
      cursor: pointer;
  }
  .p-mess-area{
    background: #cacaca;
    padding: 5px 10px;
    position: absolute;
    bottom: 8%;
    right: 8%;
  }

  .p-mess-area a{
      color: #000;
  }

  .p-mess-area:hover{
      background: #1abc9c;
      cursor: pointer;
  }
  .p-connect-area{
    background: #cacaca;
    padding: 5px 10px;
    position: absolute;
    bottom: 8%;
    right: 21%;
  }

  .p-connect-area a{
      color: #000;
  }

  .p-connect-area:hover{
      background: #1abc9c;
      cursor: pointer;
  }
  .p-dots-area{
    background: #cacaca;
    padding: 0px 8px;
    position: absolute;
    height: 25px;
    bottom: 8%;
    right: 5%;
  }

  .p-dots-area a{
      color: #000;
  }

  .p-dots-area:hover{
      background: #1abc9c;
      cursor: pointer;
  }
  .p-dots-area p{
    margin-top: -8px;
    font-size: 20px
  }
  .profile-name {
    position: absolute;
    bottom: 4%;
    left: 16%;
  }
  .profile-name h2{
    color:white !important;
  }
  i.fa-wifi{
    transform: rotate(55deg);
    position: relative;
    right: 3px;
    top: 0px;
  }
  i.far.fa-comment-alt {
    padding-right: 4px;
  }
  i.fa.fa-snowflake {
    padding-right: 4px;
  }
  .button_area1 {
    margin: 15px;
  }
  .button_area2 {
    margin-top: 30px;
    margin-bottom: 30px;
  }
  .about-btn-info {
    color: green;
    background-color: white;
    border: solid 1px green;
    font-size: 14px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 30px;
    padding-right: 30px;
  }
  .about-btn-info:hover {
    color: green;
    text-decoration: none;
  }
  .time-btn-info {
    color: black;
    background-color: white;
    font-size: 14px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 30px;
    padding-right: 30px;
  }
  .time-btn-info:hover {
    color: green;
    background-color: white;
    border: solid 1px green;
    font-size: 14px;
  }
  .conn-btn-info {
    color: black;
    background-color: white;
    font-size: 14px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 30px;
    padding-right: 30px;
  }
  .conn-btn-info:hover {
    color: green;
    background-color: white;
    border: solid 1px green;
    font-size: 14px;
  }
  .photo-btn-info {
    color: black;
    background-color: white;
    font-size: 14px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 30px;
    padding-right: 30px;
  }
  .photo-btn-info:hover {
    color: green;
    background-color: white;
    border: solid 1px green;
    font-size: 14px;
  }
  .pro-gen-btn-info {
    color: black;
    background-color: white;
    padding: 7px;
    font-size: 14px;
  }
  .pro-gen-btn-info:hover {
    color: green;
    background-color: white;
    border: solid 1px green;
    padding: 7px;
    font-size: 14px;
  }
  .car-edu-btn-info {
    color: green;
    background-color: white;
    border: solid 1px green;
    padding: 7px;
    font-size: 14px;
  }
  .car-edu-btn-info:hover {
    color: green;
    text-decoration: none;
  }
  .carrerwrap {
    display: flex;
    width: 100%;
  }
  .educonarea {
    margin: 0 auto;
    background-color: #fff;
    padding: 15px;
    margin-bottom: 30px;
}
.edutitle h3 {
    font-weight: bold;
    font-size: 18px;
    text-transform: uppercase;
    font-family: 'Heebo', sans-serif!important;
}
.carrerleft {
    width: auto;
    margin-left: 15px;
}
.carrerright {
    margin-left: 15px;
    font-family: 'Heebo', sans-serif!important;
    font-size: 14px;
    color: rgba(0,0,0,0.6);
    width: 92%;
    border-bottom: 1px solid #e6e9ec;
    padding-bottom: 6px;
    margin-bottom: 20px;
}
.carrerright h4 {
    font-size: 16px;
    font-family: 'Heebo', sans-serif!important;
    font-weight: bold;
    color: rgba(0,0,0,.7);
    margin-bottom: 0;
}
.comname {
    color: rgba(0,0,0,0.9);
    font-size: 14px;
}
.carrerright ul {
    margin-right: 15px;
    margin-top: 7px;
}
.mobile_conn{
	display: none;
}
.mobile-button{
    display: none;
  }
  .expr_mobile{
  	display: none;
  }
  .mobile_lets_hang{
    display: none;
}


@media(max-width: 600px){
	picture.avatar img{
      width: 100px;
      height: 100px;
    }
    .p-connect-area{
      right: 48%;
    }
    .p-follow-area-top{
      right: 32%;   
    }
    .p-mess-area{
      right: 13%;
    }
    .profile-name {
      bottom: 17%;
      left: 34%;
    }
    .profile-name h2{
      font-size: 25px !important;
    }
    .deskaddfr{
		display: none;
   }
   .mobile_conn{
		display: block;
	}
	.button_area1 {
	    display: none;
	}
	.mobile-button{
      display: block;
    }
    .button_area2 {
      display: none;
    }
    .button-display{
      display: flex;
    }
    .button_area{
    	margin-bottom: 20px;
    	margin-top: 20px;
    }
    .car-edu-btn-info {
    border: solid 2px green;
  }
  .pro-gen-btn-info {
  	border: solid 2px #3333;
  	color: grey;
  }
  .career_desk{
  	display: none;
  }
  .expr_mobile{
  	display: block;
  }
  .extime{
  	color: rgba(0,0,0,0.9);
  }
  .ex_see_more {
    display: flex;
}
  .ex_see_more p {
    color: #1f5064;
    font-size: 15px;
  }
  i.fas.fa-angle-down {
    font-size: 20px;
    color: #1f5064;
    margin: 3px;
}
	.mobile_lets_hang {
        display: block;
        position: absolute;
        top: 5%;
        right: -16%;
    }
    .lets_text {
        left: 33%;
        font-size: 18px;
        position: absolute;
	    top: 55%;
	    color: white;
	    transform: translate(-50%, -50%);
    }
    .mobile_lets_hang picture img {
        width: 65%;
    }

}
</style>