<?php
global $wpdb;
$user = get_user_by('ID', um_profile_id());


$tab_car_edu = (!empty($_GET['tab']) && $_GET['tab'] == 'career_education');

$chatUser = $wpdb->get_row("SELECT `userid`,`username`,`photo`,`access`,`country`,`region`,`timezone` FROM `user`,wp_users
  WHERE wp_users.user_login=user.username AND wp_users.ID='{$user->ID}'");
$travpart_user_id = $chatUser->userid;
if (empty($chatUser->photo)) {
  $avatar = um_get_user_avatar_data(um_profile_id())['url'];
} else {
  if (strpos($chatUser->photo, 'https:') !== false)
    $avatar = $chatUser->photo;
  else
    $avatar = home_url() . '/travchat/' . $chatUser->photo;
}
$chatInitMsgRow = $wpdb->get_row("SELECT id,msg FROM `wp_chat_messages` WHERE type=0 ORDER BY RAND() LIMIT 1");
$chatInitMsg = $chatInitMsgRow->msg;
if (empty($chatInitMsg)) {
  $chatInitMsg = 'Hi, can I help you?<br/>We have a limited discount for you. Would you like to know more?';
}


$cover_img ="";
if (!empty($user->ID)) {
    $cover_img = wp_get_attachment_url(get_user_meta($user->ID, 'cover', true));
}


if (empty($cover_img)) {
    $cover_img = "https://www.travpart.com/English/wp-content/uploads/2019/11/sunrise-1014712_1920.jpg";
}

if($_GET['debug']==1){
  echo 'asd '.$cover_img;exit();
}




if (!$tab_car_edu) {
  $age = '';
  if (!empty($user->date_of_birth)) {
    $date_of_birth = strtotime($user->date_of_birth);
    if ($data_of_birth < time()) {
      $age = date('Y') - date('Y', $date_of_birth);
      if (date('n') < date('n', $date_of_birth)) {
        $age--;
      } else if (date('n') == date('n', $date_of_birth) && date('j') < date('j', $date_of_birth)) {
        $age--;
      }
    }
  }

  $social_reviews = $wpdb->get_results("SELECT * FROM social_reviews WHERE parent_id=0 AND user_id='{$user->ID}' ORDER BY create_time DESC LIMIT 3");

  $ratings = $wpdb->get_row("SELECT SUM(CASE WHEN rating=5 THEN 1 ELSE 0 END) as star5,
                                  SUM(CASE WHEN rating=4 THEN 1 ELSE 0 END) as star4,
                                  SUM(CASE WHEN rating=3 THEN 1 ELSE 0 END) as star3,
                                  SUM(CASE WHEN rating=2 THEN 1 ELSE 0 END) as star2,
                                  SUM(CASE WHEN rating=1 THEN 1 ELSE 0 END) as star1
                                FROM social_reviews WHERE user_id='{$user->ID}'", ARRAY_A);
  $rating_people_count = array_sum($ratings);
  $avg_rating = $wpdb->get_var("SELECT AVG(rating) FROM `social_reviews` WHERE user_id='{$user->ID}'");
} else {
  $careers = $wpdb->get_results("SELECT * FROM `wp_career` WHERE userid=" . um_profile_id());
  $educations = $wpdb->get_results("SELECT * FROM `wp_education` WHERE userid=" . um_profile_id());
}
$current_user = wp_get_current_user();
$current_user_id = $current_user->ID;
$followData = $wpdb->get_results("SELECT * FROM `social_follow` WHERE sf_user_id = '$current_user_id' AND sf_agent_id = '$user->ID'", ARRAY_A);
       if (!empty($followData)) {

            $fellow ="Following";
        }else{
             $fellow ="Follow";
        }

  $user_meta = get_userdata($user->ID);
  $user_roles = $user_meta->roles;

    if ( in_array( 'um_travel-advisor', $user_roles, true ) ) {
      $role ="Seller";
    }
    elseif ( in_array( 'um_clients', $user_roles, true ) ) {
      $role = "Buyer";
    }
    else 
    {
      $role="";
    }

    //check if user is blocked

$check_block = $wpdb->get_var("SELECT id FROM `friends_requests` WHERE user2 = '$current_user_id' AND user1 = '{$user->ID}' AND status=2");
?>
 
<link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/chatbox.css?r1" /> 

<link rel="stylesheet" href="https://www.travpart.com/English/wp-content/themes/bali/css/profile_viewer.css?v46" />


<script src="<?php bloginfo('template_url'); ?>/js/chatbox.js?r1"></script>


<?php

 if($role=='Buyer'){ ?>
<style>
  @media (max-width: 600px) {
    .comment_buttons{
      max-width:53% !important;
    }
  }
</style>
<?php } ?>
<style>


</style>



<script type="text/javascript">
  $(document).ready(function(){
    
    $(".p-connect-area1").click(function(){
        var x = $('.p-connect-area-text').text();
        x = $.trim(x);
        if(x == 'Connect')
        {
            $(this).find('a').text('Disconnect');
            $(".p-connect-area i.fas.fa-unlink").show();
            $(".p-connect-area i.fa.fa-snowflake").hide();
        }
        else{
            $(this).find('a').text('Connect');
            $(".p-connect-area i.fas.fa-unlink").hide();
            $(".p-connect-area i.fa.fa-snowflake").show();
        }
    });
});
</script>

<script>
  $(function() {
    var messageSendCount = 0;

    function getChat() {
      if (!!$('textarea[name="chat_message"]').val() || !!$('#guestEmail').val())
        return;
      $.ajax({
        url: '<?php echo home_url() . '/travchat/user/chatbox_fetch_chat.php'; ?>',
        type: 'POST',
        async: false,
        data: {
          fetch: 1,
          agent: <?php echo $chatUser->userid; ?>,
        },
        success: function(response) {
          if (!response.length) {
            response = '<span class="chat_msg_item chat_msg_item_admin">\
              <div class="chat_avatar"><img src="<?php echo $avatar; ?>" /></div>\
              <?php echo htmlspecialchars($chatInitMsg);  ?> </span>' + response;
          } else
            messageSendCount++;
          $('#chat_converse').html(response);
          $("#chat_converse").scrollTop($("#chat_converse")[0].scrollHeight)
        }
      });
    }

    window.getChat = function() {
      getChat();
    }

    getChat();
    //setTimeout("document.getElementById('chatbox_alert_sound').play();", 9000);
    var chatInit = false;

    $('#chatwithuser').click(function() {
      toggleFab();
    });

    $(document).ready(function() {
      if (!chatInit) {
        $.ajax({
          url: '<?php echo home_url() . '/travchat/user/chatbox_init.php'; ?>',
          type: 'POST',
          async: false,
          data: {
            agent: <?php echo $chatUser->userid; ?>,
          },
          success: function(response) {}
        });
        setInterval("getChat();", 30000);
      }
      chatInit = true;
    });

    // added on enter
    var sending = false;
    var input = document.getElementById("chatSend");
    input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        event.preventDefault();

        if (sending) {
          sending = true;
          return;
        }

        if (messageSendCount == 0) {
          var MsgObj = {
            agent: <?php echo $chatUser->userid; ?>,
            msg: $('textarea[name="chat_message"]').val(),
            InitMsgId: <?php echo $chatInitMsgRow->id; ?>,
            InitMsg: '<?php echo $chatInitMsg; ?>'
          };
        } else {
          var MsgObj = {
            agent: <?php echo $chatUser->userid; ?>,
            msg: $('textarea[name="chat_message"]').val(),
          };
        }

        if (!!$('textarea[name="chat_message"]').val()) {
          $.ajax({
            url: '<?php echo home_url() . '/travchat/user/send_message.php'; ?>',
            type: 'POST',
            data: MsgObj,
            success: function(response) {
              sending = false;
              $('textarea[name="chat_message"]').val('');
              getChat();
            }
          });
        } else
          alert('Please write message first');

      }
    });

    // added on click
    var sending = false;
    $('#fab_send').click(function() {
      if (sending) {
        sending = true;
        return;
      }

      if (messageSendCount == 0) {
        var MsgObj = {
          agent: <?php echo $chatUser->userid; ?>,
          msg: $('textarea[name="chat_message"]').val(),
          InitMsgId: <?php echo $chatInitMsgRow->id; ?>,
          InitMsg: '<?php echo $chatInitMsg; ?>'
        };
      } else {
        var MsgObj = {
          agent: <?php echo $chatUser->userid; ?>,
          msg: $('textarea[name="chat_message"]').val(),
        };
      }

      if (!!$('textarea[name="chat_message"]').val()) {
        $.ajax({
          url: '<?php echo home_url() . '/travchat/user/send_message.php'; ?>',
          type: 'POST',
          data: MsgObj,
          success: function(response) {
            sending = false;
            $('textarea[name="chat_message"]').val('');
            getChat();
          }
        });
      } else
        alert('Please write message first');
    });

    $('.give_rating .p_v_s_star i').click(function() {
      var that = $(this);
      var i = 0;
      $('.give_rating .p_v_s_star i').removeClass('fas').addClass('far');
      $('.give_rating .p_v_s_star i').each(function() {
        $(this).removeClass('far').addClass('fas');
        i++;
        if ($(this).is(that))
          return false;
      });
      $('#review_rating').val(i);
    });

    $('.all-rev-reply').click(function() {
      $('#parent_id').val($(this).attr('parent_id'));
      $('#review_content').attr('placeholder', 'Reply ' + $(this).attr('username')).focus();
    });

    $('#submit_review').click(function() {
      if ($(this).hasClass('disabled')) {
        return;
      }

      $(this).addClass('disabled');

      var rating = $('#review_rating').val(),
          content = $('#review_content').val(),
          parent_id= $('#parent_id').val() || 0;

      $.ajax({
        type: 'POST',
        url: '<?php echo admin_url('admin-ajax.php'); ?>',
        dataType: 'json',
        data: {
          action: 'add_social_view',
          user_id: <?php echo um_profile_id(); ?>,
          parent_id: parent_id,
          content: content,
          rating: rating
        },
        success: function(data) {
          if (data.success) {
            window.location.href = "<?php echo add_query_arg(); ?>";
          } else {
            alert(data.msg);
          }
          $('#submit_review').removeClass('disabled');
        },
        error: function(MLHttpRequest, textStatus, errorThrown) {
          alert(errorThrown);
        }
      });

    });

  });
</script>


  
<div class="p_v_main_area">

<!--<div class="ribbon-content Available">
  <div class="ribbon base">
    Available 
    <?php 
      $social_status = get_user_meta(um_profile_id(),'social_status',true);
      echo 'samad => '.$social_status;
    ?>  
  </div>
</div>-->

<div class="ribbon-content">
  <div class="ribbon base <?php echo (($social_status=='on')?'':"ribbon-extra"); ?>">
    <?php 
      $social_status = get_user_meta(um_profile_id(),'social_status',true);
      echo (($social_status=='on')?'Available':"Not Available");
    ?>
  </div>
</div>

  <div class="row">
  
    
    <div class="col-md-12">
      
       <?php
          $button_data = $user->ID;
          $logged_in = wp_get_current_user();
          $Data = $wpdb->get_row("SELECT * FROM `friends_requests` WHERE user2 = '$button_data' AND user1 = '{$logged_in->ID}'");
        if(empty($Data)){
          $button_text = 'Connect';
           $connect_button='connect_button';
        } elseif($Data->status==0){
          $button_text ='Request <br /> sent';
          $connect_button='connect_button_sent';
        }elseif($Data->status==1){
          $button_text ='Connected';
          $data_request_id = $Data->id;
          $connect_button='connect_button_connected';
        }
        elseif($Data->status==2){
          $button_text ='Blocked';
          $data_request_id = $Data->id;
          $connect_button='connect_button_blocked';
        }
       ?>
      <div class="profilecover">
        <img src="<?php echo $cover_img; ?>">
        <?php //if($_GET['debug']==1){ 
          if (is_user_logged_in()) {
            if(empty( $check_block )){
          ?>
         
         <div class="buttons_main_div">
          
          
            <button class="btn_profile_con btn_connect conn_text marg_s <?php echo $connect_button; ?>" id='<?php echo $connect_button  ?>'>
              <div class="col-md-33 rm_d">
                <?php if(empty($Data)){ ?>
                  <i class="fas fa-user-plus"></i>
                <?php }elseif($Data->status == 0){ ?>
                  <i class="fas fa-user-minus"></i> 
                <?php } ?>
              </div>
              <div class="col-md-99 rm_d">
                <a href="#" style="color:white" class="p-connect-area-text" data-connect_friend="<?php echo $button_data; ?>"><?php echo $button_text; ?></a>
              </div>
            </button>
            <?php if ($role == 'Seller') { ?>
                <button id="follow_press" class="btn_follow marg_s" <?php echo(($fellow=="Following")?"style='background:rgb(218, 216, 216);color:#000'":""); ?>>
                  <i class="fas fa-wifi"></i>
                  <span class="tx"><?php echo $fellow; ?></span>
                </button>
            <?php } ?>
            <button class="btn_profile_msg btn_messages">
         <a target="_blank" style="color: white" href="https://www.travpart.com/English/travchat/user/index.php?user=<?php echo $travpart_user_id ?>">
          <i class="far fa-comment-alt"></i>
          Messages
         </a>
          </button>
          <button class="p-dots-area_1">
        <a class="dropdown-toggle" id="dropdownMenuButton_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-ellipsis-h"></i>
        </a>
        <div class="dropdown-menu dropdownMenuButton_1" aria-labelledby="dropdownMenuButton_1">
          <?php if( $button_text=='Connected' ){ ?>
          <a id='unconnect' class="dropdown-item" href="#">Disconnect</a>
        <?php } ?>

          <a  class="dropdown-item" href="#" data-toggle="modal" data-target="#ReportModalCenter" id='report'>Report</a>
        </div>
      </button>
            

         </div>
        
      <?php }} ?>
      </div>
        
    
       <div class="profile_pic_div">
            <picture class="avatar pro_view_pic">
            <?php echo um_get_avatar('', um_profile_id(), 150); ?>
          </picture>
       </div>   
     
      <div class="for-small-mobile" style="display: none;">
        <div class="btn-group" role="group" aria-label="...">
        <button id='connect_user' type="button" class="btn btn-default"><i class="fas fa-user-plus"></i> <a href="#" class="p-connect-area-text" data-connect_friend="<?php echo $button_data; ?>"><?php echo $connect_button; ?></a></button>
        <button class="btn_profile btn_messages"><a target="_blank" style="color: white" href="https://www.travpart.com/English/travchat/user/index.php?user=<?php echo $travpart_user_id?>"><i class="far fa-comment-alt"></i> Messages</a></button>
        <button type="button" class="btn btn-default">...</button>
      </div>
      </div>
      
     
      <div class="comment_buttons" style="display:none;">
        <div class="p-follow-area-top request" id='<?php echo $connect_button  ?>'>
         <a href="#" class='conn_text'>
          <i class="fas fa-user-plus"></i> 
          <?php echo $button_text  ?>
         </a>
        </div>
          <?php  if ( $role=='Seller' ) {?>
          <div class="p-follow-area-top" id='follow_press'>
            <a href="#" id='bt_text'>
              <i  class="fas fa-wifi"></i>
              <?php echo $fellow  ?>
            </a>
          </div>
        <?php } ?>

        <div class="p-mess-area">
  
          <a target="_blank" style="color: white" href="https://www.travpart.com/English/travchat/user/index.php?user=<?php echo $travpart_user_id?>"><i class="far fa-comment-alt"></i>Messages</a>
        </div>
         
        
    
    <!-- DropDown Dots -->
    <div class="p-dots-area">
      <a class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <p>...</p>
      </a>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <?php if( $button_text=='Connected' ){ ?>
        <a id='unconnect' class="dropdown-item" href="#">Unconnected</a>
      <?php } ?>

        <a  class="dropdown-item" href="#" data-toggle="modal" data-target="#ReportModalCenter" id='report'>Report</a>
      </div>
    </div>
    <!-- End DropDown Dots -->

    <!-- Start Report Popup -->
    <div class="modal fade" id="ReportModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content report-md-content">
          <div class="modal-header report-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Report User</h5>
            <button type="button" class="close report-close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <div class="report-content">
            <h3>Why would you like to report?<small>Your report will be kept anonymous</small></h3>
          

            <radiogroup>
              <label class="fake-radio-green radio-text"><input type="radio" name="report_type" value="misleading"><span class="radio-img"></span>The user’s profile is misleading</label>

              <label class="fake-radio-green radio-text"><input type="radio" name="report_type" value="inappropriate behavior"><span class="radio-img"></span>The user’s content and/or behavior is inappropriate</label>

              <label class="fake-radio-green radio-text"><input type="radio" name="report_type" value="spam"><span class="radio-img"></span>The user sends me spam messages</label>

              <label class="fake-radio-green radio-text"><input type="radio" name="report_type" value="other issues"><span class="radio-img"></span>Other
              <div class="toggle"><textarea name="reason"></textarea></div>
              </label>
            </radiogroup>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" id='userreport' class="btn btn-primary report-next">Next</button>
          </div>
        </div>
      </div>
    </div>

    <!-- End Report Popup -->




      </div>
      <div class="profile-name">
        <h2><?php echo um_get_display_name(um_profile_id()); ?></h2>
      </div>
      <!--<div class="mobile_lets_hang">
        <img src="https://www.travpart.com/English/wp-content/uploads/2019/11/lets-hangout-new.png" alt="">
        <div class="lets_text">Let's Hangout !</div>
      </div>-->
      
      
      
    </div>
  </div>
</div>

<div class="button_area">
  <div class="row">
    <div class="col-md-6 for_padd_top">
      <div class="col-md-667">
        <a href="<?php echo $tab_car_edu ? add_query_arg(array('tab' => '')) : '#'; ?>" class="gen-btn-info for_shad <?php echo $tab_car_edu ? '' : 'gen-btn-active'; ?>" role="button">General Profile</a>
      </div>
      <div class="col-md-667">
        <a href="<?php echo !$tab_car_edu ? add_query_arg(array('tab' => 'career_education')) : '#'; ?>" class="gen-btn-info for_shad <?php echo $tab_car_edu ? 'gen-btn-active' : ''; ?>" role="button">Career & Education</a>
      </div>
    </div>
    <div class="col-md-6">
      <div class="link_timeline_page_desktop">
          <a href="<?php echo home_url('my-time-line').'/?user='.$user->user_login; ?>"><span class="timeline-text">View <?php echo um_get_display_name(um_profile_id()); ?> Timeline Page</span></a>
        </div> 
    </div>
  </div>
</div>
<div class="button_area mobile-button">
  <div class="row">
    <div class="col-md-6 button-display">
      <div class="col-md-6 genprodisplay">
        <a href="<?php echo $tab_car_edu ? add_query_arg(array('tab' => '')) : '#'; ?>" class="gen-btn-info for_shad <?php echo $tab_car_edu ? '' : 'gen-btn-active'; ?>" role="button">General Profile</a>
      </div>
      <div class="col-md-6 caredudisplay">
        <a href="<?php echo !$tab_car_edu ? add_query_arg(array('tab' => 'career_education')) : '#'; ?>" class="gen-btn-info for_shad <?php echo $tab_car_edu ? 'gen-btn-active' : ''; ?>" role="button">Career & Education</a>
      </div>
    </div>
    
  </div>
</div>

<?php if (!$tab_car_edu) { ?>
  <div class="form-area">
    <div class="row">
      <div class="col-md-12">
        <form class="form-horizontal size_d" method="post" action="">
          <div class="form-group mb-3 c-bg">
            <div class="col-md-6 col-sm-6 col-xs-12 form_display_mo">
              <div class="col-md-4 p-v-label">
                <label for="name" class="control-label">Name:</label>
              </div>
              <div class="col-sm-8 p-v-text">
                <h3><?php echo um_get_display_name(um_profile_id()); ?></h3>
              </div>
            </div>
            <div class="col-md-6 let_hang">
              <div class="col-md-4">
              </div>
              <!--<div class="col-sm-8 desk_lets_hang">
                <img src="https://www.travpart.com/English/wp-content/uploads/2019/11/lets-hangout-new.png" alt="">
                <div class="lets_text">Let's Hangout !</div>
              </div>-->
                
            </div>
          </div>


          <div class="form-group mb-3">
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <label for="name" class="control-label">Sex:</label>
              </div>
              <div class="col-sm-8 p-v-text">
                <h3><?php echo $user->gender != 'Female' ? 'Male' : 'Female'; ?></h3>
              </div>
            </div>
            <div class="link_timeline_page">
              <a href="<?php echo home_url('my-time-line').'/?user='.$user->user_login; ?>"><span class="timeline-text">View <?php echo um_get_display_name(um_profile_id()); ?> Timeline Page</span></a>
            </div> 
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fa-atom"></i>
                <label for="Hobby" class="control-label">Hobby</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="hobby" value="<?php echo $user->hobby; ?>" disabled>
              </div>
            </div>
          </div>
          <div class="form-group mb-3">
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fa-home"></i>
                <label for="Hometown" class="control-label">Hometown</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="hometown" value="<?php echo $user->hometown; ?>" disabled>
              </div>
            </div>
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fa-city"></i>
                <label for="Hobby" class="control-label">Current City</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="current_city" value="<?php echo $user->current_city; ?>" disabled>
              </div>
            </div>
          </div>
          <div class="form-group mb-3">
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fa-ruler-vertical"></i>
                <label for="Height" class="control-label">Height</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="height" value="<?php echo empty($user->height) ? '' : ($user->height . ' ' . $user->height_unit); ?>" disabled>
              </div>
            </div>
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fa-wine-glass-alt"></i>
                <label for="Drinking" class="control-label">Drinking</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="drinking" value="<?php echo $user->drinking; ?>" disabled>
              </div>
            </div>
          </div>
          <div class="form-group mb-3">
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fa-graduation-cap"></i>
                <label for="Education-Level" class="control-label">Education Level</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="edu_level" value="<?php echo $user->edu_level; ?>" disabled>
              </div>
            </div>
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fa-smoking"></i>
                <label for="Smoking" class="control-label">Smoking</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="smoking" value="<?php echo $user->smoking; ?>" disabled>
              </div>
            </div>
          </div>
          <div class="form-group mb-3">
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fas fa-landmark"></i>
                <label for="Political-View" class="control-label">Political View</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="political_view" value="<?php echo $user->political_view; ?>" disabled>
              </div>
            </div>
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fa-universal-access"></i>
                <label for="Seeking" class="control-label">Seeking</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="seeking" value="<?php echo $user->seeking; ?>" disabled>
              </div>
            </div>
          </div>
          <div class="form-group mb-3">
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fa-praying-hands"></i>
                <label for="Religion" class="control-label">Religion</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="religion" value="<?php echo $user->religion; ?>" disabled>
              </div>
            </div>
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fa fa-language"></i>
                <label for="Language" class="control-label">Language</label>
              </div>
              <div class="col-sm-8">           
                <textarea type="text" class="form-control" id="language" disabled><?php echo implode(', ',$user->language); ?></textarea>
              </div>
            </div>
          </div>
          <div class="form-group mb-3 desk-bio-age">
            <div class="col-md-6">
              <div class="col-md-6 p-v-label">
                <label for="Religion" class="control-label">Biographical Info</label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fa-table"></i>
                <label for="Language" class="control-label">Age:</label>
              </div>
              <div class="col-sm-8 p-v-text">
                <h3><?php echo $age; ?></h3>
              </div>
            </div>
          </div>
          <div class="form-group mb-3 mobile-bio-age">
            <div class="col-md-6 form_display_mo">
              <div class="col-md-4 p-v-label">
                <i aria-hidden="true" class="fas fa-table"></i>
                <label for="Language" class="control-label">Age:</label>
              </div>
              <div class="col-sm-8 p-v-text">
                <h3><?php echo $age; ?></h3>
              </div>
            </div>
            <div class="col-md-6">
              <div class="col-md-6 p-v-label">
                <label for="Religion" class="control-label">Biographical Info</label>
              </div>
            </div>
          </div>
          <div class="form-group mb-3">
            <div class="col-md-1">
            </div>
            <div class="col-sm-10">
              <textarea class="form-control" name="description" id="description" rows="5" cols="30" disabled><?php echo $user->description; ?></textarea>
            </div>
            <div class="col-md-1">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="social-reviews-area">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-1">
        </div>
        <div class="col-md-10 p_v_so_rev">
          <div class="col-md-12 social-sec">
            <div class="col-md-4 social_text">
              <h2>Social Reviews</h2>
            </div>
            <div class="col-md-8 so_rate_display">
              <div class="col-md-4">
                <p class="p_v_avg_rate"><?php echo round($avg_rating,1); ?></p>
                <div class="p_v_s_star">
                  <?php for ($i = 0; $i < ceil($avg_rating); $i++) { ?>
                    <i class="fas fa-star" aria-hidden="true"></i>
                  <?php } ?>
                  <?php for ($i = ceil($avg_rating); $i < 5; $i++) { ?>
                    <i class="far fa-star" aria-hidden="true"></i>
                  <?php } ?>
                </div>
                <p class="p_v_people"><?php echo $rating_people_count; ?></p>
              </div>
              <div class="col-md-8 social_bar">
                <div class="col-md-12 so_dar_display">
                  <div class="side">
                    <div>5</div>
                  </div>
                  <div class="middle">
                    <div class="bar-container">
                      <div class="bar-5" style="width:<?php echo $ratings['star5']/$rating_people_count*100; ?>%"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 so_dar_display">
                  <div class="side">
                    <div>4</div>
                  </div>
                  <div class="middle">
                    <div class="bar-container">
                      <div class="bar-4" style="width:<?php echo $ratings['star4']/$rating_people_count*100; ?>%"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 so_dar_display">
                  <div class="side">
                    <div>3</div>
                  </div>
                  <div class="middle">
                    <div class="bar-container">
                      <div class="bar-3" style="width:<?php echo $ratings['star3']/$rating_people_count*100; ?>%"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 so_dar_display">
                  <div class="side">
                    <div>2</div>
                  </div>
                  <div class="middle">
                    <div class="bar-container">
                      <div class="bar-2" style="width:<?php echo $ratings['star2']/$rating_people_count*100; ?>%"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 so_dar_display">
                  <div class="side">
                    <div>1</div>
                  </div>
                  <div class="middle">
                    <div class="bar-container">
                      <div class="bar-1" style="width:<?php echo $ratings['star1']/$rating_people_count*100; ?>%"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="see_all_rev">
                <a href="<?php echo home_url('all-reviews').'/?user='.$user->user_login; ?>"><p>See All Reviews</p></a>
              </div>
            </div>
          </div>
          <div class="row star_bg_vv">
            <div class="col-md-6">
            </div>
            <div class="col-md-6">
              <div class="all_rev_button">
                <a href="<?php echo home_url('all-reviews').'/?user='.$user->user_login; ?>"><span class="">View All Reviews</span></a>
              </div>
            </div>
          </div>
      
          <div class="row star_bg_v">
           
          <?php foreach ($social_reviews as $r) { ?>
            
           
            <div class="col-md-12 mrg_bottom">
              <div class="col-md-2 all-rev-text forsiz">
                <p>By <?php echo um_get_display_name($r->creator_id); ?></p>
              </div>
              <div class="col-md-4 all-rev-text forsiz">
                <p><?php echo str_replace("\n", "<br>", $r->review); ?></p>
              </div>
              <div class="col-md-4 forsiz">
                <div class="p_v_s_star">
                  <?php for ($i = 0; $i < $r->rating; $i++) { ?>
                    <i class="fas fa-star" aria-hidden="true"></i>
                  <?php } ?>
                  <?php for ($i = $r->rating; $i < 5; $i++) { ?>
                    <i class="far fa-star" aria-hidden="true"></i>
                  <?php } ?>
                </div>
                <?php $reply=$wpdb->get_var("SELECT review FROM social_reviews WHERE parent_id={$r->id} AND user_id=" . um_profile_id() ." LIMIT 1"); ?>
                <?php if(empty($reply)) { ?>
                  <?php if(wp_get_current_user()->ID==um_profile_id()) { ?>
                  <div class="all-rev-reply" username="<?php echo um_get_display_name($r->creator_id); ?>" parent_id="<?php echo $r->id; ?>">
                    <a href="#">Reply</a>
                  </div>
                  <?php } ?>
                <?php } else { ?>
                <div class="reply-text forsiz" style="">
                  <p><b>Reply: </b><?php echo str_replace("\n", "<br>", $reply); ?></p>
                </div>
                <?php } ?>
              </div>
              <div class="col-md-2 all-rev-text forsiz">
                <p><?php echo date("Y-m-d H:i", strtotime($r->create_time)); ?></p>
              </div>
            </div>
          <?php } ?>
            </div>
          <?php if (empty($social_reviews)) { ?>
            <div class="star_bg_v">
            <div class="col-md-12">
              <p style="text-align: center; font-size: 20px;">No review yet.</p>
            </div>
            </div>
          <?php } ?>
        

          <div class="col-md-12 give-review-section">
            <div class="give_rating">
              <!--<div class="p_v_s_star">
                <i class="fa-star fas" aria-hidden="true"></i>
                <i class="fa-star fas" aria-hidden="true"></i>
                <i class="fa-star fas" aria-hidden="true"></i>
                <i class="fa-star fas" aria-hidden="true"></i>
                <i class="fa-star fas" aria-hidden="true"></i>
              </div>-->
              
              <?php //if($_GET['debug']==1){ ?>
                 <!-- Rating Stars Box -->
          <div class='rating-stars text-center'>
            <ul id='stars'>
              <li class='star' title='1' data-value='1'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='2' data-value='2'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='3' data-value='3'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='4' data-value='4'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='5' data-value='5'>
                <i class='fa fa-star fa-fw'></i>
              </li>
            </ul>
          </div>
              <?php //} ?>
              
              <input id="review_rating" type="hidden" value="5" />
            </div>
            <div class="give_review">
              <textarea id="review_content"></textarea>
            </div>
            <div class="give_rev_button">
              <input id="parent_id" type="hidden" value="0" />
              <a id="submit_review" href="#">Give Review</a>
            </div>
          </div>
          
        </div>
        <div class="col-md-1">
        </div>
      </div>
    </div>
  </div>
<?php } else { ?>
  <div class="educonarea carrerwraptop career_desk">
    <div class="row">
      <div class="col-md-8 edutitle">
        <h3>Career</h3>
      </div>
    </div>

    <?php foreach ($careers as $career) { ?>
      <div class="row">
        <div class="carrerwrap">
          <div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/Capture.jpg" /></div>
          <div class="carrerright">
            <h4><?php echo $career->job_title; ?></h4>
            <div class="comname"><?php echo $career->company; ?></div>
            <?php if ($career->working == 1) { ?>
              <div class="extime">
                <?php echo date('M Y', strtotime($career->start_date)); ?>
                - Present .
                <?php echo (new DateTime())->diff(new DateTime($career->start_date))->format('%y years %m months'); ?>
              </div>
            <?php } else { ?>
              <div class="extime">
                <?php echo date('M Y', strtotime($career->start_date)); ?>
                -
                <?php echo date('M Y', strtotime($career->end_date)); ?>
              </div>
            <?php } ?>
            <div class="exloc"><?php echo $career->location; ?></div>
            <div class="description">
              <p>
                <?php echo str_replace("\n", "<br>", $career->description); ?>
              </p>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
    <?php if (empty($careers)) { ?>
      <div class="carrerwrap">
        <p style="margin-left: 20px;">None</p>
      </div>
    <?php } ?>

  </div>

  <div class="educonarea carrerwraptop expr_mobile">
    <div class="row">
      <div class="col-md-8 edutitle">
        <h3>Career</h3>
      </div>
    </div>

    <?php foreach ($careers as $career) { ?>
      <div class="row">
        <div class="carrerwrap">
          <div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/Capture.jpg" /></div>
          <div class="carrerright">
            <h4><?php echo $career->job_title; ?></h4>
            <div class="comname"><?php echo $career->company; ?></div>
            <?php if ($career->working == 1) { ?>
              <div class="extime">
                <?php echo date('M Y', strtotime($career->start_date)); ?>
                - Present .
                <?php echo (new DateTime())->diff(new DateTime($career->start_date))->format('%y years %m months'); ?>
              </div>
            <?php } else { ?>
              <div class="extime">
                <?php echo date('M Y', strtotime($career->start_date)); ?>
                -
                <?php echo date('M Y', strtotime($career->end_date)); ?>
              </div>
            <?php } ?>
            <div class="exdes"><?php echo str_replace("\n", "<br>", $career->description); ?></div>
          </div>
        </div>
      </div>
    <?php } ?>
    <?php if (empty($careers)) { ?>
      <div class="carrerwrap">
        <p style="margin-left: 20px;">None</p>
      </div>
    <?php } ?>

  </div>

  <div class="educonarea carrerwraptop">
    <div class="row">
      <div class="col-md-8 edutitle">
        <h3>Education</h3>
      </div>
    </div>

    <?php foreach ($educations as $edu) { ?>
      <div class="row">
        <div class="carrerwrap">
          <div class="carrerleft"><img src="https://www.travpart.com/English/wp-content/uploads/2019/11/uni_logo.jpg" /></div>
          <div class="carrerright">
            <h4><?php echo $edu->school; ?></h4>
            <div class="comname"><?php echo $edu->degree; ?>, <?php echo $edu->area; ?></div>
            <div class="extime"><?php echo $edu->startdate; ?> - <?php echo $edu->enddate; ?></div>
            <div class="description">
              <p>
                <?php echo str_replace("\n", "<br>", $edu->description); ?>
              </p>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
    <?php if (empty($educations)) { ?>
      <div class="carrerwrap">
        <p style="margin-left: 20px;">None</p>
      </div>
    <?php } ?>

  </div>
<?php } ?>

<div class="fabs">
  <div class="chat">
    <div class="chat_header">
      <div class="chat_option">
        <div class="header_img">
          <img src="<?php echo $avatar; ?>" style="height: 55px;" />
        </div>
        <span id="chat_head"><?php echo $user->user_login; ?></span> <br>
        <span class="agent"><?php echo ($chatUser->access == '1') ? 'Travel advisor' : 'Client'; ?></span>
        <span id="chat_fullscreen_loader" class="chat_fullscreen_loader">
          <i class="fullscreen zmdi zmdi-window-maximize"></i></span>
      </div>
    </div>

    <div id="chat_converse" class="chat_conversion chat_converse">
    </div>

    <div class="fab_field">
      <a id="fab_send" class="fab"><i class="zmdi zmdi-mail-send"></i></a>
      <textarea id="chatSend" name="chat_message" placeholder="Send a message" class="chat_field chat_message"></textarea>
    </div>
    <audio id="chatbox_alert_sound" src="<?php bloginfo('template_url'); ?>/messagealert.mp3"></audio>
  </div>
  <a id="prime" class="fab"><i class="prime zmdi zmdi-comment-outline"></i></a>
</div>
<style type="text/css">
  
  .connect_button_color{
    background :#008a66!important;
  }
</style>

<script>

 // $('.connect_button_sent').one('click',function() {
  $(document).one("click",".connect_button_sent",function(){

      var user_id = <?php echo $user->ID  ?>;
      var  website_url ='https://www.travpart.com/English';
      $.ajax({
      type: "POST",
      url: website_url + '/submit-ticket/',
      data: {
      friend_id: user_id,
      action: 'connection_request_cancel'
      }, // serializes the form's elements.
      success: function(data) {
      var result = JSON.parse(data);
      if (result.status==1) {
  
      $(".connect_button_sent").addClass('connect_button_color');
     $(".connect_button_sent").addClass('connect_button');
       $(".connect_button_sent").removeClass('connect_button_sent');
      $(".btn_profile_con ").html('<div class="col-md-99"><i class="fas fa-user-plus"></i><a href="#" style="color:white!important">Connect</a></div>');
      } 
      }
      });
      });

</script>

<script>
//  $('#request').one('click', function() {
  $(document).one("click",".connect_button",function(){
    
    $(".connect_button").addClass('connect_button_sent');
        $(".p-connect-area-text").html('<div class="col-md-99 rm_d"><a href="#" style="color:white">Request <br> sent</a></div>');
    
    var user_id = <?php echo $user->ID  ?>;
        var  website_url ='https://www.travpart.com/English';

        $.ajax({
                type: "POST",
                url: website_url + '/submit-ticket/',
                data: {
                    friend_id: user_id,
                    action: 'connect_friend'
                }, // serializes the form's elements.
                success: function(data) {
                    var result = JSON.parse(data);
                    if (result.status) {
                
                        $(".connect_button").addClass('connect_button_sent');
                        $(".connect_button").removeClass('connect_button');
                        $(".p-connect-area-text").html('<div class="col-md-99 rm_d"><a href="#" style="color:white">Request <br> sent</a></div>');
                    } else {
                      
                    }
                }
            });
        });

  $('#follow_press').on('click', function() {
      
      var txt = $(this).find('.tx').text();
      if(txt=="Follow"){
      $("#follow_press").css('background','#dad8d8');
      $("#follow_press a,#follow_press").css('color','#000');
      $("#follow_press").html('<i class="fas fa-wifi"></i><span class="tx">Following</span>');
    }
    if(txt=="Following"){
      $("#follow_press").css('background','#008a66');
      $("#follow_press a,#follow_press").css('color','#fff');
      $("#follow_press").html('<i class="fas fa-wifi"></i><span class="tx">Follow</span>');
    }
      
      
        var user_id = <?php echo $user->ID  ?>;
        var  website_url ='https://www.travpart.com/English';
        $.ajax({
                    type: "POST",
                    url: website_url + '/submit-ticket/',
                    data: {
                        user_id: user_id,
                        action: 'social_follow'
                    },
                    success: function(data) {
                        var result = JSON.parse(data);
                        if (result.status) {
                          
                        } 
                        else{
                            if (result.action == 'unfollow') {
                              $("#follow_press").css('background','#008a66');
                              $("#follow_press a,#follow_press").css('color','#fff');
                                $("#follow_press").html('<i class="fas fa-wifi"></i><span class="tx">Follow</span>');
                            } else if (result.action == 'follow') {
                              $("#follow_press").css('background','#dad8d8');
                              $("#follow_press a,#follow_press").css('color','#000');
                              $("#follow_press").html('<i class="fas fa-wifi"></i><span class="tx">Following</span>');
                            }
                        }
                        
                    }
                });
      });
  
 
</script>
<script>
   $('#unconnect').on('click', function() {
  var request_id = <?php echo $data_request_id ?>;
        var  website_url ='https://www.travpart.com/English';
        var request_type = 3;
        $.ajax({
                type: "POST",
                url: website_url + '/submit-ticket/',
                data: {
                    request_id: request_id,
                    typeo: request_type,
                    action: 'add_friend'
                }, // serializes the form's elements.
                success: function(data) {
                    var result = JSON.parse(data);
                    if (result.status) {
                        if(result.action='removed'){
                          $(".conn_text").html("<i class='fas fa-user-plus'></i>connect");
                        }
                        
                        //window.location.reload()// show response from the php script.
                    } else {
                       // alert(result.message);
                        //$("#bt_text2").html("Request_sent");
                      
                    }
                }
            });
        });

</script>

<script>
   //report 

    $('#userreport').on('click', function() {
      var selectedValue = $("input[name='report_type']:checked").val();
         if (selectedValue) {
       //   alert (selectedValue);
          var  website_url ='https://www.travpart.com/English';
          var user_id = <?php echo $user->ID  ?>;
        $.ajax({
                type: "POST",
                url: website_url + '/submit-ticket/',
                data: {
                    user_id: user_id,
                    type :selectedValue,
                    action: 'user_report'
                }, // serializes the form's elements.
                success: function(data) {
                    var result = JSON.parse(data);
                    alert(result.message);
                    $("#ReportModalCenter").css("display", "none");
                  //  window.location.reload()
              }
            });
        }
        else{
             alert ('Please select reason');
           }     
   });
</script>
  

<style type="text/css">
    #footer {
        display: none;
    }
</style>