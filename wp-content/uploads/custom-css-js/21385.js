<!-- start Simple Custom CSS and JS -->
<script type="text/javascript">
jQuery(document).ready(function( $ ){
  
$(".d-flex").on("click", function() {
  $('.defaultsection8').toggle();
});
  
  $("#buttonOptionalLocation").on("click", function() {
  $('.defaultsection8').toggle();
});
  
  $("#AddAnotherTravelItemManually").on("click", function() {
  $('.defaultsection8').css({'display':'none'});
});
  
  
$('#AddAnotherTravelItemManually').click(function(e) {
            e.preventDefault();
            if(!existedManualItemBox)
                $(this).parent().next().find('.v2-list').append('<div class="v2-list-item d-flex flex-column pb-3 mb-3 border-bottom-blue"><div class="v2-list-left mb-3"><div class="upload-wrap"><a class="img_video_upload"><i class="fa fa-cloud-upload-alt"></i><strong>Click to   Upload a Pictures or Video</strong></a><input class="item_attachment" type="hidden" value="" /><input                class="item_attachment_url" type="hidden" value="" /><input class="item_attachment_type" type="hidden"   value="" /></div></div><div class="v2-list-right"><div class="d-flex flex-row "><div class="v2-actions order-last fixed-width-80"><a item="0" class="manual-item-trash btn bg-org-0 text-white"><i class="fa fa-trash"></i> Delete</a></div><div class="flex-fill"><h4 class="v2-title mt-0 mb-3 d-flex pr-2 align-items-center"><i class="fa fa-flag text-gray-0"></i><input class="item_name flex-fill" type="text"  placeholder="travel item\'s name"     value="" /></h4></div></div><div class="d-flex flex-row"><div class="pt-5 pb-5 gutter"><div class="date-picker-container"><div class="deliver-app-container manual-item-date" ng-app="deliveryDateTimeApp" ng-controller="dateTimeCtrl as ctrl"></div></div></div></div><div class="d-flex flex-column justify-content-between align-items-stretch"><div class="row "><div class="col-md-4 manualmobile"><i class="fa fa-diagnoses text-gray-0"></i> Unit Price: <input id="item_unit_price" class="item_unit_price" type="text" value="0.00" /></div><div class="col-md-4 manualmobile">Quantity:<input class="item_qty" type="number" min="1" max="999" style="width:150px!important"/></div><div class="col-md-4 manualmobile">Total cost: <span class="item_price_syml">'+$(".header_curreny").find("option:selected").attr("syml")+'</span><input class="item_price" type="text" value="0.00" readonly /></div><div class="fixed-width-80"></div></div><div class="row manualmobile"><div class="flex-fill p-2"><textarea class="item_description" placeholder="Description" style="width: 100%;"></textarea></div><div class="fixed-width-80 pt-2 pb-2"><a class="btn bg-org-0 text-white add_manualitem"><i class="fa fa-plus"></i> Add</a></div></div></div></div></div>');
            new Tooltip(document.getElementById('item_unit_price'),{title:'Insert your net price ',trigger:'focus',placement:'top',container:'body',template:'<div class="popper-tooltip" role="tooltip"><div class="tooltip__arrow"></div><div class="tooltip__inner"></div></div>'});
            new tourDayPicker($('.manual-item-date'),function (piker) {});
            $('.manual-item-date .save-button').remove();
            $('.manual-item-box .v2-list').last().find('.item_name').focus();
            existedManualItemBox=true;
        });
        
        $(document).on('change', '.item_unit_price,.item_qty', function (event) {
            var unit_price=parseFloat($('.item_unit_price').val());
            var qty=parseFloat($('.item_qty').val());
            if(unit_price>0 && qty>0) {
                $('.item_price').val((unit_price*qty).toFixed(2));
            }
            else {
                $('.item_price').val('0.00');
            }
        });
        
        var additem;
        $(document).on('click', '.add_manualitem', function (event) {
            additem = $(this).parent().parent().parent().parent().parent();
            
            if (additem.find(".item_name").val() == '') {
                $('.item_name').focus();
                additem.find('.item_name').css('border-color', 'red');
                return;
            }
            additem.find('.item_name').css('border-color', '');
            
            if (additem.find(".datepicker-tour-dates-select").val() == '') {
                $('.datepicker-tour-dates-select').focus();
                additem.find(".datepicker-tour-dates-select").css('border-color', 'red');
                return;
            }
            additem.find(".datepicker-tour-dates-select").css('border-color', '');
            
            if (additem.find(".data-time").text() == '') {
                $('.data-time').focus();
                //additem.find(".data-time").css('border-color', 'red');
                return;
            }
            //additem.find(".item_time").css('border-color', '');
            
            if (!(parseInt(additem.find(".item_qty").val())>0)) {
                $('.item_qty').focus();
                additem.find(".item_qty").css('border-color', 'red');
                return;
            }
            additem.find(".item_qty").css('border-color', '');
            
            if (!(parseFloat(additem.find(".item_unit_price").val())>0)) {
                $('.item_unit_price').focus();
                additem.find(".item_unit_price").css('border-color', 'red');
                return;
            }
            additem.find(".item_unit_price").css('border-color', '');
            
            if (additem.find(".item_description").val() == '') {
                $('.item_description').focus();
                additem.find(".item_description").css('border-color', 'red');
                return;
            }
            additem.find(".item_description").css('border-color', '');

            var rp_price=0;
            if($('.header_curreny').val()=='IDR')
                rp_price=Math.round(additem.find(".item_price").val());
            else if($('.header_curreny').val()=='CNY')
                rp_price=Math.round(additem.find(".item_price").val() / $("#cs_RMB").val());
            else if($('.header_curreny').val()=='USD')
                rp_price=Math.round(additem.find(".item_price").val() / $("#cs_USD").val());
            else if($('.header_curreny').val()=='AUD')
                rp_price=Math.round(additem.find(".item_price").val() / $("#cs_AUD").val());
            else
                return;
            var contents = {
                name: additem.find(".item_name").val(),
                attachment: additem.find(".item_attachment").val(),
                attachment_type: additem.find(".item_attachment_type").val(),
                date: additem.find(".datepicker-tour-dates-select").val(),
                time: additem.find(".data-time").text(),
                qty: additem.find(".item_qty").val(),
                price: rp_price,
                description: additem.find(".item_description").val(),
            };
            $.ajax({
                type: "POST",
                url: '/English/wp-content/themes/bali/api.php?action=addManualItem&tour_id=',
                data: contents,
                success: function (data) {
                    console.log(data);
                    if (data != 0) {
                        existedManualItemBox=false;
                        var price = rp_price;
                        var newitem = '<div class="v2-list-item d-flex flex-column flex-sm-row pb-3 mb-3 border-bottom-blue"><div class="v2-list-left mb-3 mr-sm-3"><div class="thumb-wrap">';

                        if (additem.find(".item_attachment_type").val() == 'image') {
                            newitem+='<img src="' + additem.find(".item_attachment_url").val() + '" width="180" alt="" class="img-responsive">';
                        }
                        else if (additem.find(".item_attachment_type").val() == 'video') {
                            var attachment_url = additem.find(".item_attachment_url").val();
                            newitem+='<video class="wp-video-shortcode" width="200" controls="controls" src="' + attachment_url + '"><source type="video/mp4" src="' + attachment_url + '"></video>';
                        }
                        
                        newitem+='</div></div><div class="v2-list-right"><div class="d-flex flex-row"><div class="v2-actions order-last">';
            
                        newitem+='<a item="' + data + '" class="manual-item-trash cir bg-org-0 text-white"><i class="fa fa-trash"></i></a>';
                        
                        newitem+='</div><div class="mr-auto">';
                        
                        newitem+='<h4 class="v2-title mt-0 mb-3"><i class="fa fa-flag text-gray-0"></i>' + additem.find(".item_name").val() + '</h4>';
                        
                        newitem+='</div></div><div class="d-flex flex-column flex-sm-row justify-content-between align-items-stretch"><div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">';
                        
                        newitem+='<div class="flex-fill p-2 border-right "><i class="fa fa-calendar-alt mr-1 text-blue-0"></i>Day' + additem.find(".datepicker-tour-dates-select").val() +' '+ additem.find(".data-time").text() + '</div>';

                        newitem+='<div class="flex-fill p-2 border-right ">Quantity: ' + additem.find(".item_qty").val() + '</div>';

                        newitem+='<div class="flex-fill p-2 border-right "><span class="change_price" or_pic="' + price + '"> Rp <span class="price_span">' + price + ' </span></span></div>';
                        
                        newitem+='</div></div><div class="d-flex flex-row justify-content-between align-items-center  flex-grow-1 text-center">';
                        
                        newitem+='<p>' + additem.find(".item_description").val() + '</p></div></div></div>';
                        
                        additem.after(newitem).remove();
                        
                        $('.total-cost .change_price').attr('or_pic', (parseInt($('.total-cost .change_price').attr('or_pic'))+price ) );
                        $('.manualitemsection .change_price').attr('or_pic', (parseInt($('.manualitemsection .change_price').attr('or_pic'))+price ));
                        $(".header_curreny").change();
						setCookie('update', 1, 1);
                    }
                }
            });
        });
        
        $('.v2-list').on('click','.manual-item-trash',function(){
            var item=$(this).parent().parent().parent().parent();
            var price=parseInt(item.find('.change_price').attr('or_pic'));
            var i=parseInt($(this).attr('item'));
            
            if(i==0) {
                item.remove();
                existedManualItemBox=false;
                return;
            }

            $('.manualitemsection .change_price').attr('or_pic', (parseInt($('.manualitemsection .change_price').attr('or_pic'))-price ));
            $('.total-cost .change_price').attr('or_pic', (parseInt($('.total-cost .change_price').attr('or_pic'))-price ) );
            $(".header_curreny").change();
            item.remove();
    
            $.ajax({
                type: "GET",
                url: '/English/wp-content/themes/bali/api.php?action=delManualItem&tourid=&item='+i,
                success: function (data) {
                    console.log(data);
                }
            });
        });

        var upload_frame;
        var t;
        jQuery(document).on('click', '.img_video_upload', function (event) {
            t = jQuery(this);
            event.preventDefault();
            if (upload_frame) {
                upload_frame.open();
                return;
            }
            upload_frame = wp.media({
                title: 'Insert image',
                button: {
                    text: 'Insert',
                },
                multiple: false
            });
            upload_frame.on('select', function () {
                attachment = upload_frame.state().get('selection').first().toJSON();
                console.log(attachment);
                t.parent().find(".item_attachment").val(attachment.id);
                t.parent().find(".item_attachment_url").val(attachment.url);
                t.parent().find(".item_attachment_type").val(attachment.type);
            });
            upload_frame.open();
        });
    }); 
  
});

</script>
<!-- end Simple Custom CSS and JS -->
