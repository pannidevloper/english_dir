<!-- start Simple Custom CSS and JS -->
<script type="text/javascript">
jQuery(document).ready(function( $ ){
  
  $("#AddAnotherTravelItemManually").on("click", function(){
  if ($('.v2-list').is(':visible')){
       $("#addManualItem > i").removeClass("fa-plus");
     $("#addManualItem > i").addClass("fa-minus");
  }
  });
  $("#buttonOptionalLocation").on("click", function(){
  if ($('.defaultsection8').is(':visible')){
  $("#buttonOptionalLocation > i").removeClass("fa-plus");
     $("#buttonOptionalLocation > i").addClass("fa-minus");
  }
    else{
        $("#buttonOptionalLocation > i").removeClass("fa-minus");
     $("#buttonOptionalLocation > i").addClass("fa-plus");
    }
  });
  $(".op-location").on("click", function(){
  if ($('.defaultsection8').is(':visible')){
  $("#buttonOptionalLocation > i").removeClass("fa-plus");
     $("#buttonOptionalLocation > i").addClass("fa-minus");
  }
    else{
        $("#buttonOptionalLocation > i").removeClass("fa-minus");
     $("#buttonOptionalLocation > i").addClass("fa-plus");
    }
  });
  
  $(".btn-collapse").on("click", function(){
  if ($('.defaultsection8').is(':visible')){
  $("#buttonOptionalLocation > i").removeClass("fa-minus");
     $("#buttonOptionalLocation > i").addClass("fa-plus");
  }
    else{
       $("#buttonOptionalLocation > i").removeClass("fa-plus");
     $("#buttonOptionalLocation > i").addClass("fa-minus");
    }
  });
  
  $(".hotel,#btnHotel,.btn-collapse").on("click", function(){
  if ($('.elementor-14047 .elementor-element.elementor-element-ec6b550').is(':visible')){
  $("#btnHotel > i").removeClass("fa-minus");
     $("#btnHotel > i").addClass("fa-plus");
  }
    else{
       $("#btnHotel > i").removeClass("fa-plus");
     $("#btnHotel > i").addClass("fa-minus");
    }
  });
  
  
  $(".dining,#buttonMeal,.btn-collapse").on("click", function(){
  if ($('.elementor-14047 .elementor-element.elementor-element-1a5be19').is(':visible')){
  $("#buttonMeal > i").removeClass("fa-minus");
     $("#buttonMeal > i").addClass("fa-plus");
  }
    else{
       $("#buttonMeal > i").removeClass("fa-plus");
     $("#buttonMeal > i").addClass("fa-minus");
    }
  });
  
  
  $(".transportation,#buttonCar,.btn-collapse").on("click", function(){
  if ($('.elementor-14047 .elementor-element.elementor-element-ff47405').is(':visible')){
  $("#buttonCar > i").removeClass("fa-minus");
     $("#buttonCar > i").addClass("fa-plus");
  }
    else{
       $("#buttonCar > i").removeClass("fa-plus");
     $("#buttonCar > i").addClass("fa-minus");
    }
  });
  
  
  $(".tour_guide,#buttonPop,.btn-collapse").on("click", function(){
  if ($('.elementor-14047 .elementor-element.elementor-element-ea64e8f').is(':visible')){
  $("#buttonPop > i").removeClass("fa-minus");
     $("#buttonPop > i").addClass("fa-plus");
  }
    else{
       $("#buttonPop > i").removeClass("fa-plus");
     $("#buttonPop > i").addClass("fa-minus");
    }
  });
  
  
  $(".r_place,#buttonPlace,.btn-collapse").on("click", function(){
  if ($('.elementor-14047 .elementor-element.elementor-element-eb4a1fd').is(':visible')){
  $("#buttonPlace > i").removeClass("fa-minus");
     $("#buttonPlace > i").addClass("fa-plus");
  }
    else{
       $("#buttonPlace > i").removeClass("fa-plus");
     $("#buttonPlace > i").addClass("fa-minus");
    }
  });
  
  $(".spa,#buttonSpa,.btn-collapse").on("click", function(){
  if ($('.elementor-14047 .elementor-element.elementor-element-4538030').is(':visible')){
  $("#buttonSpa > i").removeClass("fa-minus");
     $("#buttonSpa > i").addClass("fa-plus");
  }
    else{
       $("#buttonSpa > i").removeClass("fa-plus");
     $("#buttonSpa > i").addClass("fa-minus");
    }
  });
  
  
  $(".helicopter,#buttonBoats,.btn-collapse").on("click", function(){
  if ($('.elementor-14047 .elementor-element.elementor-element-cd602c2').is(':visible')){
  $("#buttonBoats > i").removeClass("fa-minus");
     $("#buttonBoats > i").addClass("fa-plus");
  }
    else{
       $("#buttonBoats > i").removeClass("fa-plus");
     $("#buttonBoats > i").addClass("fa-minus");
    }
  });
  
  
  $(".activities,#buttonLast,.btn-collapse").on("click", function(){
  if ($('#sectionseven').is(':visible')){
  $("#buttonLast > i").removeClass("fa-minus");
     $("#buttonLast > i").addClass("fa-plus");
  }
    else{
       $("#buttonLast > i").removeClass("fa-plus");
     $("#buttonLast > i").addClass("fa-minus");
    }
  });
});

</script>
<!-- end Simple Custom CSS and JS -->
