<?php
/**
 * Plugin Name: Extra activity price list
 * Version:     1.0
 * Author:      Lix
 * Text Domain: Extra activity price list
 * Description: Extra activity price list
 */

 
 
 register_activation_hook( __FILE__, 'extraactivity_activate' );
 
function extraactivity_activate() {
	if(class_exists( 'WP_Roles' )) {
		$wp_roles=new WP_Roles();
		$wp_roles->add_cap( 'administrator', 'manage_travcust_data' );
	}
}

 /***************************** show the subscribe in admin page  ******************************/
 
 add_action('admin_menu', 'add_extra_activity_admin_menu');
 add_action('admin_init', 'add_extra_activity_admin_menu_init');
 function add_extra_activity_admin_menu(){
 
	add_options_page( 'extra activity price list', 'extra activity price list', 'manage_travcust_data', __FILE__, 'show_extra_activity' );
	
 }
 
 function add_extra_activity_admin_menu_init(){
	
	

 }
 function show_extra_activity(){
   wp_enqueue_media();
 ?>
 <h2>Extra activity price list </h2>
 <form action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST" >
 <?php 
   $priceratio=get_option('person_price_ration');
 if($_POST['save'])
 {
  
   update_option('_cs_locationprice', $_POST['locationprice']); 
	$i=1;
	$ExtraActivity=array();
	while($i<=(int)$_POST['ea_length'])
	{
		if(!empty($_POST["ea{$i}_item"])){
		array_push($ExtraActivity,array('item'=>$_POST["ea{$i}_item"],
						  'img'=>$_POST["ea{$i}_img"],
						  'price'=>$_POST["ea{$i}_price"]*$priceratio
					));
		}
		$i++;
	}
	if(!empty($ExtraActivity)) { update_option('_cs_extraactivity', serialize($ExtraActivity)); }
}
	
	$ExtraActivity=unserialize(get_option('_cs_extraactivity'));
   		echo '<table class="extraactivity">
				<thead>
					<tr><th>Item</th><th>Img</th><th>Price(Rp)</th></tr>
				</thead>
				<tbody>
                <tr><th>Location Cost:</th><th></th><th><input name="locationprice" type="text" value="'.$_POST['locationprice'].'" /></th></tr>';
		for($i=1;$i<=count($ExtraActivity);$i++)
		{
			echo '<tr><th><input name="ea'.$i.'_item" type="text" value="'.$ExtraActivity[$i-1]['item'].'" /></th><th><img width=64 height=64 src="'.$ExtraActivity[$i-1]['img'].'" style="display:none;" /><input class="ea_img_upload" type="button" value="Upload img" /><input name="ea'.$i.'_img" type="hidden" value="'.$ExtraActivity[$i-1]['img'].'" /></th><th><input name="ea'.$i.'_price" type="text" value="'.$ExtraActivity[$i-1]['price'].'" /></th>';
		}
		echo '	</tbody>
			  </table>
			  <input id="ea_length" name="ea_length" type="hidden" value="'.count($ExtraActivity).'"/>
			  <input type="button" class="button" value="Add Row" id="AddEARow" />
			  <br/><br/>
			  <input type="submit" name="save" id="save" class="button button-primary button-large" value="Save">
			  <br/><br/>';
		echo "<script>
				jQuery(document).ready(function(){
					var ea_length;
					jQuery('#AddEARow').click(function (){
						ea_length=parseInt(jQuery('#ea_length').val())+1;
						jQuery('#ea_length').val(ea_length);
						jQuery('table.extraactivity>tbody').append('<tr> <th><input name=\"ea'+ea_length+'_item\" type=\"text\" value=\"\" /></th> <th><img width=64 height=64 src=\"\" style=\"display:none;\" /><input class=\"ea_img_upload\" type=\"button\" value=\"Upload img\" /><input name=\"ea'+ea_length+'_img\" type=\"hidden\" value=\"\" /></th> <th><input name=\"ea'+ea_length+'_price\" type=\"text\" value=\"\" /></th>');
					});
					
					jQuery('.ea_img_upload').each( function(){
						var t=jQuery(this).prev();
						if(t.attr('src'))
						{
							t.show();
						}
					});
					
					var upload_frame;   
					var value_id;
					var t;
					jQuery(document).on('click', '.ea_img_upload', function(event){
						t=jQuery(this);
						value_id =jQuery( this ).attr('id');       
						event.preventDefault();   
						if( upload_frame ){   
							upload_frame.open();   
							return;   
						}   
						upload_frame = wp.media({   
							title: 'Insert image',   
							button: {   
								text: 'Insert',   
							},   
							multiple: false   
						});   
						upload_frame.on('select',function(){   
							attachment = upload_frame.state().get('selection').first().toJSON();
							t.next().val(attachment.url);
							t.prev().attr('src',attachment.url);
							t.prev().show();
						});	   
						upload_frame.open();   
					});
	
				});
			  </script>";
 ?>
</form>
 <?php
 }
 ?>