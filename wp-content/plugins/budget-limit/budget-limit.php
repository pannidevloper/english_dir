<?php
/**
 * Plugin Name: Budget limit
 * Version:     2.0
 * Author:      Lix
 * Text Domain: Budget limit
 * Description: Budget limitt
 */

 
 
 register_activation_hook( __FILE__, 'budgetLimit_activate' );
 
 function budgetLimit_activate() {

}

 /***************************** show the subscribe in admin page  ******************************/
 
 add_action('admin_menu', 'add_budget_limit_admin_menu');
 add_action('admin_init', 'add_budget_limit_admin_menu_init');
 function add_budget_limit_admin_menu(){
 
	add_options_page( 'budget_limit', 'budget limit', 'manage_options', __FILE__, 'set_budget_limit' );
	
 }
 
 function add_budget_limit_admin_menu_init()
 { }

function set_budget_limit()
{
	global $wpdb;

	if(!empty($_POST['save']))
	{
		update_option('budget_limit',floatval($_POST['budget_limit']));
		update_option('referral_budget_limit',floatval($_POST['referral_budget_limit']));
		update_option('budget_person_limit',floatval($_POST['budget_person_limit']));
		update_option('budget_referral_person_limit',floatval($_POST['budget_referral_person_limit']));
		update_option('reward_amount', floatval($_POST['reward_amount']));
	}
	
	if(!empty($_POST['removeReferral'])) {
		$username=filter_input(INPUT_POST, 'referralSaleAgent', FILTER_SANITIZE_STRING);
		if($user_id=username_exists($username)) {
			delete_user_meta($user_id, 'can_referral');
			$removeReferralMsg='Removed';
		}
		else {
			$removeReferralMsg='Invaild username.';
		}
	}
	
	if(!empty($_POST['addReferral'])) {
		$username=filter_input(INPUT_POST, 'saleAgent', FILTER_SANITIZE_STRING);
		if($user_id=username_exists($username)) {
			update_user_meta($user_id, 'can_referral', 1);
			$addReferralMsg='Added';
		}
		else {
			$addReferralMsg='Invaild username.';
		}
	}

	$budget_used=$wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND type='publish'");
	$referral_budget_used=$wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND type='invite'");
	?>
	<form action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST">
	<br/><br/>
	<table>
		<tr>
			<td> <b>Total Tour Package Budget limit:</b> </td>
			<td>  $<input name="budget_limit" type="text" id="budget_limit" value="<?php echo esc_attr(get_option('budget_limit')); ?>" class="regluar-text ltr" /></td>
			<td> <span style="margin-left:100px;"><b>Limit per person:</b><span></td>
			<td>  $<input name="budget_person_limit" type="text" id="budget_person_limit" value="<?php echo esc_attr(get_option('budget_person_limit')); ?>" class="regluar-text ltr" /></td>
			</tr>
		<tr>
			<td colspan="2">
				<p class="description">The budget for post to earn and share to earn</p>
				<p class="description">Set to 0 to turn it off</p>
			</td>
			<td colspan="2">
				<div style="margin-left:100px;">
					<p class="description">For tour package creation</p>
				</div>
			</td>
		</tr>
	</table>
		  
	<hr>
	<table>
		<tr><td><b>Reward amount:</b></td> <td>  $<input name="reward_amount" type="text" id="reward_amount" value="<?php echo esc_attr(get_option('reward_amount')); ?>" class="regluar-text ltr" /></td></tr>
	</table>
	<p class="description">Reward amount for publish to earn</p>
	<hr>
	<table>
		<tr>
			<td> <b>Total Referal Budget limit:</b> </td>
			<td>  $<input name="referral_budget_limit" type="text" id="referral_budget_limit" value="<?php echo esc_attr(get_option('referral_budget_limit')); ?>" class="regluar-text ltr" /></td>
			<td> <b>Limit per person:</b></td>
			<td> $<input name="budget_referral_person_limit" type="text" id="budget_referral_person_limit" value="<?php echo esc_attr(get_option('budget_referral_person_limit')); ?>" class="regluar-text ltr" /></td>
		</tr>
	</table>
	<p class="description">For referral program</p>
	<hr>
	<input type="submit" name="save" id="save" class="button button-primary button-large" value="Save">
	</form>
	<hr>
	<form action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST">
	<table>
		<tr>
			<td>
				<input name="referralSaleAgent" />
			</td>
			<td>
				<input type="submit" name="removeReferral" id="removeReferral" class="button button-primary button-large" value="Disable Referral">
			</td>
			<?php if(!empty($removeReferralMsg)) { ?>
			<td>
				<span style="color: white; background: cadetblue;"><?php echo $removeReferralMsg; ?></span>
			</td>
			<?php } ?>
		</tr>
		<tr>
			<td>
				<input name="saleAgent" />
			</td>
			<td>
				<input type="submit" name="addReferral" id="addReferral" class="button button-primary button-large" value="Enable Referral">
			</td>
			<?php if(!empty($addReferralMsg)) { ?>
			<td>
				<span style="color: white; background: cadetblue;"><?php echo $addReferralMsg; ?></span>
			</td>
			<?php } ?>
		</tr>
	</table>
	</form>
	<p>The tour packeage budget have used at <?php echo date('M'); ?> : <b>$<?php echo $budget_used; ?></b></p>
	<p>The referral budget have used at <?php echo date('M'); ?> : <b>$<?php echo $referral_budget_used; ?></b></p>
<?php } ?>