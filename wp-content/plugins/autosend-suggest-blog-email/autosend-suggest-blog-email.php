<?php

/**
 * Plugin Name: Send blog email to all user
 * Version:     1.3
 * Author:      Lix
 * Description: Auto send blog email to all user
 */

require_once(plugin_dir_path(__FILE__) . '/libraries/action-scheduler/action-scheduler.php');

register_activation_hook(__FILE__, 'auto_send_blog_email_activate');


function auto_send_blog_email_activate()
{
}

function autoSendBlogEmailToAllUser($ID, $post)
{
    global $wpdb;
    if (strtotime($post->post_date)<(time()-24*60*60)) {
        return;
    }
    if (get_post_meta($ID, 'sendemail', true)!=1) {
        update_post_meta($ID, 'sendemail', 1);
        $autosendblog_recent_posts_count=intval(get_option('autosendblog_recent_posts_count'))+1;
        update_option('autosendblog_recent_posts_count', $autosendblog_recent_posts_count);
    } else {
        return;
    }
    if ($autosendblog_recent_posts_count<4) {
        return;
    }
    
    update_option('autosendblog_recent_posts_count', '0');
    as_enqueue_async_action('send_blog_email_to_user', array($ID,$post));
}
add_action('publish_blog', 'autoSendBlogEmailToAllUser', 10, 2);

function send_blog_email_to_user_run($ID, $post)
{
    set_time_limit(0);
    global $wpdb;
    $recent_posts=wp_get_recent_posts(array('numberposts'=>4, 'post_type' => 'blog', 'post_status'=>'publish'));
    include(plugin_dir_path(__FILE__) . "/template/suggest_email_template.php");
    $userList=$wpdb->get_results("SELECT username,email,access,user_registered FROM `user`,wp_user_last_login
								  WHERE (`access`=1 OR `access`=2) AND activated=1 AND email IS NOT NULL
									AND wp_user_last_login.user_login=user.username
									AND DATE_SUB(CURDATE(), INTERVAL 7 DAY)<=DATE(login_date)");
    $email_title='Aspiration from Travpart';
    foreach ($userList as $row) {
        $enable_notification=$wpdb->get_var("SELECT subscribe FROM `unsubscribed_emails` WHERE email='{$row->email}' AND type=1");
        if (is_null($enable_notification) || $enable_notification==1) {
            $unsubscribe_link=home_url('unsubscribe').'/?qs='.base64_encode($row->email.'||'.md5('EC'.$row->username.$row->email.$row->user_registered));
            if ($row->access==1) {
                $user_content=str_replace('UNSUBSCRIBE_LINK', $unsubscribe_link, $sale_agent_mail_content);
                wp_mail($row->email, $email_title, $user_content);
            } else {
                $user_content=str_replace('UNSUBSCRIBE_LINK', $unsubscribe_link, $customer_mail_content);
                wp_mail($row->email, $email_title, $user_content);
            }
        }
    }
}
add_action('send_blog_email_to_user', 'send_blog_email_to_user_run', 10, 2);
