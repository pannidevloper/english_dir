<?php
/**
 * Plugin Name: Travcust Car
 * Version:     1.0
 * Author:      Lix
 * Description: Travcust Car setting
 */

 
 
 register_activation_hook( __FILE__, 'travcust_car_activate' );
 
 function travcust_car_activate() {

}

 /***************************** show the subscribe in admin page  ******************************/
 
 add_action('admin_menu', 'add_travcust_car_admin_menu');
 add_action('admin_init', 'add_travcust_car_admin_menu_init');
 function add_travcust_car_admin_menu(){
 
	add_options_page( 'Travcust Car', 'Travcust Car', 'manage_travcust_data', __FILE__, 'show_travcust_car' );
	
 }
 
 function add_travcust_car_admin_menu_init(){
	
	

 }
 function show_travcust_car(){
   wp_enqueue_media();
 ?>
 <h2>Travcust Car/Bus Setting </h2>
 <form action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST" >
 <?php 
 $priceratio=get_option('person_price_ration');
   if($_POST['import'])
 {
	 if($xlsx=SimpleXLSX::parse($_FILES['import_file_upload']['tmp_name']))
	 {
		 $xlsx_data=$xlsx->rows();
		 $details_index=4;
		 $price_index=3;
		 $area_index=5;
		 $vendor_index=6;	 
		 $phone_index=7;
		 if($_POST['import_type']=='append')
			$TravcustCar=unserialize(get_option('_cs_travcust_car'));
		 else if($_POST['import_type']=='replace')
			$TravcustCar=array();
		 $i=0;
		 foreach($xlsx_data as $t)
		 {
			 if(!empty($t[$details_index]) && $i>1)
			 {
				 /*echo 'Name:'.$t[$details_index]."<br/>";
				 echo 'Price:'.$t[$price_index]."<br/>";
				 echo 'Area:'.$t[$area_index]."<br/>";
				 echo 'Vendor:'.$t[$vendor_index]."<br/>";
				 echo 'Phone:'.$t[$phone_index]."<br/>";*/
				 
				 $find=false;
				 foreach($TravcustCar as &$car)
				 {			 
					 if($car['item']==$t[$details_index])
					 {
						 //echo "Append subitem<br/>";
						$car['price']=$t[$price_index];
						$car['area']=$t[$area_index];
						$car['vendor']=$t[$vendor_index];
						$car['phone']=$t[$phone_index];
						$find=true;
						break;
					 }
				 }
				 if($find==false)
				 {
					 //echo "New subitem<br/>";
					array_push($TravcustCar,array('item'=>$t[$details_index],
						  'img'=>'',
						  'price'=>$t[$price_index],
						  'area'=>$t[$area_index],
						  'vendor'=>$t[$vendor_index],
						  'phone'=>$t[$phone_index]
						));
				 }
			 }
			 
			 $i++;
		 }
		 //var_dump($TravcustCar);
		 if(!empty($TravcustCar)) { update_option('_cs_travcust_car', serialize($TravcustCar)); }
	 }
	 else
	 {
		 echo SimpleXLSX::parse_error();
	 }
 }

 if($_POST['save'])
 {
	$i=1;
	$TravcustCar=array();
	while($i<=(int)$_POST['ea_length'])
	{
		if(!empty($_POST["ea{$i}_item"])){
		array_push($TravcustCar,array('item'=>$_POST["ea{$i}_item"],
						  'img'=>$_POST["ea{$i}_img"],
						  'price'=>$_POST["ea{$i}_price"]*$priceratio,
						  'area'=>$_POST["ea{$i}_area"],
						  'vendor'=>$_POST["ea{$i}_vendor"],
						  'phone'=>$_POST["ea{$i}_phone"]
					));
		}
		$i++;
	}
	if(!empty($TravcustCar)) { update_option('_cs_travcust_car', serialize($TravcustCar)); }
}
	
	$TravcustCar=unserialize(get_option('_cs_travcust_car'));
		echo '<table class="extraactivity">
				<thead>
					<tr><th>Details</th><th>Pictures</th><th>Price(Rp)</th><th>Area</th><th>Vendor name</th><th>Phone number</th><th>Language</th></tr>
				</thead>
				<tbody>';
		for($i=1;$i<=count($TravcustCar);$i++)
		{
			echo '<tr>
					<td><input name="ea'.$i.'_item" type="text" value="'.$TravcustCar[$i-1]['item'].'" /></td>
					<td><img width=64 height=64 src="'.$TravcustCar[$i-1]['img'].'" style="display:none;" /><input class="ea_img_upload" type="button" value="Upload img" /><input name="ea'.$i.'_img" type="hidden" value="'.$TravcustCar[$i-1]['img'].'" /></td>
					<td><input name="ea'.$i.'_price" type="text" value="'.$TravcustCar[$i-1]['price'].'" /></td>
					<td><input name="ea'.$i.'_area" type="text" value="'.$TravcustCar[$i-1]['area'].'" /></td>
					<td><input name="ea'.$i.'_vendor" type="text" value="'.$TravcustCar[$i-1]['vendor'].'" /></td>
					<td><input name="ea'.$i.'_phone" type="text" value="'.$TravcustCar[$i-1]['phone'].'" /></td>
					<td>
					  <select>					  	
					    <option value="English">English</option>
					    <option value="chinese">Chinese</option>
					  </select>
					</td>
				  </tr>';
		}
		echo '</tbody>
			  </table>
			  <input id="ea_length" name="ea_length" type="hidden" value="'.count($TravcustCar).'"/>
			  <input type="button" class="button" value="Add Row" id="AddEARow" />
			  <br/><br/>
			  <input type="submit" name="save" id="save" class="button button-primary button-large" value="Save">
			  <br/><br/>';
		echo "<script>
                jQuery.noConflict();
				jQuery(document).ready(function(){
					var ea_length;
					jQuery('#AddEARow').click(function (){
						ea_length=parseInt(jQuery('#ea_length').val())+1;
						jQuery('#ea_length').val(ea_length);
						jQuery('table.extraactivity>tbody').append('<tr> <td><input name=\"ea'+ea_length+'_item\" type=\"text\" value=\"\" /></td> <td> <img width=64 height=64 src=\"\" style=\"display:none;\" /> <input class=\"ea_img_upload\" type=\"button\" value=\"Upload img\" /> <input name=\"ea'+ea_length+'_img\" type=\"hidden\" value=\"\" /> </td> <td><input name=\"ea'+ea_length+'_price\" type=\"text\" value=\"\" /></td> <td><input name=\"ea'+ea_length+'_area\" type=\"text\" value=\"\" /></td> <td><input name=\"ea'+ea_length+'_vendor\" type=\"text\" value=\"\" /></td> <td><input name=\"ea'+ea_length+'_phone\" type=\"text\" value=\"\" /></td> </tr>');
					});
					
					jQuery('.ea_img_upload').each( function(){
                    	var t=jQuery(this).prev();
						if(t.attr('src'))
						{
							t.show();
						}
					});
					
					var upload_frame;   
					var value_id;
					var t;
					jQuery(document).on('click', '.ea_img_upload', function(event){
						t=jQuery(this);
						value_id =jQuery( this ).attr('id');       
						event.preventDefault();   
						if(upload_frame){   
							upload_frame.open();   
							return;   
						}   
						upload_frame = wp.media({   
							title: 'Insert image',   
							button: {   
								text: 'Insert',   
							},   
							multiple: false   
						});   
						upload_frame.on('select',function(){   
							attachment = upload_frame.state().get('selection').first().toJSON();
							t.next().val(attachment.url);
							t.prev().attr('src',attachment.url);
							t.prev().show();
						});	   
						upload_frame.open();   
					});
	
				});
			  </script>";
 ?>
</form>
<div id="data-import-form" class="postbox">
<form action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST" enctype="multipart/form-data" >
<table class="tablepress-postbox-table fixed">
<tbody>
	<tr style="display: table-row;">
		<th scope="row">Select file:</th>
		<td>
			<input name="import_file_upload" type="file" style="box-sizing: border-box;">
		</td>
	</tr>
	<tr>
		<th scope="row">Import Format:</th>
		<td>
			<select name="import[format]">
				<option value="xlsx">XLSX - Microsoft Excel 2007-2013 (experimental)</option>
			</select>
		</td>
	</tr>
	<tr>
		<th scope="row">Replace, or Append?:</th>
		<td>
			<label for="tables-import-type-replace"><input name="import_type" id="tables-import-type-replace" type="radio" value="replace"> Replace existing table</label>
			<label for="tables-import-type-append"><input name="import_type" id="tables-import-type-append" type="radio" value="append" checked="checked"> Append rows to existing table</label>
		</td>
	</tr>
	<tr>
		<th scope="row"></th>
		<td><input type="submit" value="Import" name="import"></td>
	</tr>
</tbody>
</table>
</form>
</div>
 <?php
 }
 ?>