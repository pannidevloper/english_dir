<?php
/*
Plugin Name: Seller Responsiveness checker
Description: calculate seller responsiveness
Version: 1.0
Author: M Farhan
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
// Adding Options page for front_end on admin side.
add_action('admin_menu', function ()
{
    add_options_page('Seller Response Time', 'Seller responsiveness', 'manage_options', 'Seller responsiveness', 'seller_score_show_to_admin');
});
add_action('admin_init', function ()
{
   /// register_setting('tour-rank-plugin-settings', 'front_apps');
    register_setting('tour-rank-plugin-settings', 'retention_rate_percentage');
    register_setting('tour-rank-plugin-settings', 'number_shared_percentage');
    register_setting('tour-rank-plugin-settings', 'number_comments_percentage');
    register_setting('tour-rank-plugin-settings', 'number_likes_percentage');
    register_setting('tour-rank-plugin-settings', 'number_dislikes_percentage');
    register_setting('tour-rank-plugin-settings', 'number_followers_percentage');
    register_setting('tour-rank-plugin-settings', 'number_clicks_percentage');
    register_setting('tour-rank-plugin-settings', 'number_views_percentage');
    register_setting('tour-rank-plugin-settings', 'live_chat_click_percentage');
});

 function view_responsiveness($user_id)
 {
      global $wpdb;

      $results = $wpdb->get_results("SELECT DISTINCT userid FROM `chat` WHERE c_to = $user_id", ARRAY_A);

      foreach ($results as $value) {

        $ids[]= $value['userid'];
      }
      $i=0;

        if(!empty($ids)){

       foreach ($ids as $value) {
       
        $reply = $wpdb->get_var("SELECT DISTINCT  chat_date FROM chat WHERE (c_to=$value AND c_from=$user_id) ORDER BY chatid DESC");

        $msg_by_buyer = $wpdb->get_var("SELECT DISTINCT  chat_date FROM chat WHERE (c_to=$user_id AND c_from=$value) ORDER BY chatid DESC");

            if($reply!= NULL){

            $current_date = strtotime($reply);



            $buyer = strtotime($msg_by_buyer);


            $difference_in_hours = ($current_date-$buyer)/(60*60) ;

          // $difference_in_hours = round($difference_in_hours);


            $i++;
          }
          else
          {

             $nowDay = strtotime($wpdb->get_var("SELECT NOW()"));
              $buyer = strtotime($msg_by_buyer);
              $response = ($nowDay-$buyer)/3600;
           $i++;
          }
           $total_count  =  $difference_in_hours+$response;
        } 
        $final_score = round($total_count/$i );
    
          $wpdb->update( 
          'user', 
          array( 
          'seller_responsiveness' => $final_score
          ), 
          array( 'userid' => $user_id), 
          array( 
          '%s', // value1
          ), 
          array( '%d' ) 
          );
          
          

      }// end of if
        
        
         else  {
            $wpdb->update( 
          'user', 
          array( 
          'seller_responsiveness' =>24
          ), 
          array( 'userid' => $user_id), 
          array( 
          '%s', // value1
          ), 
          array( '%d' ) 
          );
           }

            }

        function get_agents_list_and_process()
        {
           global $wpdb;
          $AgentList = $wpdb->get_results("SELECT userid from user where  access=1 AND activated=1");

           foreach ($AgentList as $agent) {

            view_responsiveness($agent->userid);
             
          }
        }


        // Add daily scheduler
      
        register_activation_hook(__FILE__, 'seller_responsiveness_event');

        function seller_responsiveness_event() {
        if (! wp_next_scheduled ( 'daily' )) {
        wp_schedule_event(time(), 'daily', 'seller_responsiveness_event_hook');
        }
        }

        add_action('seller_responsiveness_event_hook', 'get_agents_list_and_process');

        register_deactivation_hook(__FILE__, 'my_deactivation');

        function my_deactivation() {
        wp_clear_scheduled_hook('seller_responsiveness_event_hook');
        }



        function seller_score_show_to_admin(){  
         
      //  echo "Backend work done!";
        get_agents_list_and_show_for_admin();
        }

         function get_agents_list_and_show_for_admin()
        {
           global $wpdb;
          $AgentList = $wpdb->get_results("SELECT * from user where  access=1 AND activated=1 AND uname!=''");

          echo'<ul>';
           foreach ($AgentList as $agent) {

            echo'<li>';
            echo $agent->uname .  "--".  $agent->seller_responsiveness;
            echo'<li>';
              
             
          }
          echo'</ul>';
        }