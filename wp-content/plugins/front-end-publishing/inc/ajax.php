<?php

/**
 * Matches submitted content against restrictions set in the options panel
 *
 * @param array $content The content to check
 * @return string: An html string of errors
 */
function fep_post_has_errors($content)
{	global $wpdb;
	$fep_plugin_options = get_option('fep_post_restrictions');
	$fep_messages = fep_messages();
	$min_words_title = $fep_plugin_options['min_words_title'];
	$max_words_title = $fep_plugin_options['max_words_title'];
	$min_words_content = $fep_plugin_options['min_words_content'];
	$max_words_content = $fep_plugin_options['max_words_content'];
	$min_words_bio = $fep_plugin_options['min_words_bio'];
	$max_words_bio = $fep_plugin_options['max_words_bio'];
	$max_links = $fep_plugin_options['max_links'];
	$max_links_bio = $fep_plugin_options['max_links_bio'];
	$min_tags = $fep_plugin_options['min_tags'];
	$max_tags = $fep_plugin_options['max_tags'];
	$thumb_required = $fep_plugin_options['thumbnail_required'];
	$error_string = '';
	$format = '%s<br/>';		$current_user = wp_get_current_user();	$agent_username=$current_user->user_login;	$urow=$wpdb->get_row("SELECT * FROM `user` WHERE `username`='{$agent_username}'");	if(empty($urow->access) || $urow->access!=1) {		$error_string .= sprintf($format, $fep_messages['no_privilege_error']);	}

	if (($min_words_title && empty($content['post_title'])) || ($min_words_content && empty($content['post_content'])) || ($min_words_bio && empty($content['about_the_author'])) || ($min_tags && empty($content['post_tags']))) {
		$error_string .= sprintf($format, $fep_messages['required_field_error']);
	}

	$tags_array = explode(',', $content['post_tags']);
	$stripped_bio = strip_tags($content['about_the_author']);
	$stripped_content = strip_tags($content['post_content']);

	if (!empty($content['post_title']) && str_word_count($content['post_title']) < $min_words_title)
		$error_string .= sprintf($format, $fep_messages['title_short_error']);
	if (!empty($content['post_title']) && str_word_count($content['post_title']) > $max_words_title)
		$error_string .= sprintf($format, $fep_messages['title_long_error']);
	if (!empty($content['post_content']) && str_word_count($stripped_content) < $min_words_content)
		$error_string .= sprintf($format, $fep_messages['article_short_error']);
	if (str_word_count($stripped_content) > $max_words_content)
		$error_string .= sprintf($format, $fep_messages['article_long_error']);
	if (!empty($content['about_the_author']) && $stripped_bio != -1 && str_word_count($stripped_bio) < $min_words_bio)
		$error_string .= sprintf($format, $fep_messages['bio_short_error']);
	if ($stripped_bio != -1 && str_word_count($stripped_bio) > $max_words_bio)
		$error_string .= sprintf($format, $fep_messages['bio_long_error']);
	if (substr_count($content['post_content'], '</a>') > $max_links)
		$error_string .= sprintf($format, $fep_messages['too_many_article_links_error']);
	if (substr_count($content['about_the_author'], '</a>') > $max_links_bio)
		$error_string .= sprintf($format, $fep_messages['too_many_bio_links_error']);
	if (!empty($content['post_tags']) && count($tags_array) < $min_tags)
		$error_string .= sprintf($format, $fep_messages['too_few_tags_error']);
	if (count($tags_array) > $max_tags)
		$error_string .= sprintf($format, $fep_messages['too_many_tags_error']);
	if ($thumb_required == 'true' && $content['featured_img'] == -1)
		$error_string .= sprintf($format, $fep_messages['featured_image_error']);

	if (str_word_count($error_string) < 2)
		return false;
	else
		return $error_string;
}

/**
 * Ajax function for fetching a featured image
 *
 * @uses array $_POST The id of the image
 * @return string: A JSON encoded string
 */
function fep_fetch_featured_image()
{
	$image_id = $_POST['img'];
	echo wp_get_attachment_image($image_id, array(200, 200));
	die();
}

add_action('wp_ajax_fep_fetch_featured_image', 'fep_fetch_featured_image');

/**
 * Ajax function for deleting a post
 *
 * @uses array $_POST The id of the post and a nonce value
 * @return string: A JSON encoded string
 */
function fep_delete_posts()
{
	try {
		if (!wp_verify_nonce($_POST['delete_nonce'], 'fepnonce_delete_action'))
			throw new Exception(__('Sorry! You failed the security check', 'frontend-publishing'), 1);

		if (!current_user_can('delete_post', $_POST['post_id']))
			throw new Exception(__("You don't have permission to delete this post", 'frontend-publishing'), 1);

		$result = wp_delete_post($_POST['post_id'], true);
		if (!$result)
			throw new Exception(__("The article could not be deleted", 'frontend-publishing'), 1);

		$data['success'] = true;
		$data['message'] = __('The article has been deleted successfully!', 'frontend-publishing');
	} catch (Exception $ex) {
		$data['success'] = false;
		$data['message'] = $ex->getMessage();
	}
	die(json_encode($data));
}

add_action('wp_ajax_fep_delete_posts', 'fep_delete_posts');
add_action('wp_ajax_nopriv_fep_delete_posts', 'fep_delete_posts');

/**
 * Ajax function for adding a new post.
 *
 * @uses array $_POST The user submitted post
 * @return string: A JSON encoded string
 */
function fep_process_form_input()
{
	$fep_messages = fep_messages();
	try {
		if (!wp_verify_nonce($_POST['post_nonce'], 'fepnonce_action'))
			throw new Exception(
				__("Sorry! You failed the security check", 'frontend-publishing'),
				1
			);

		$send_notifiction=false;
		if ($_POST['post_id'] != -1 && get_post_status($_POST['post_id'])=='publish') {
			wp_update_post(array('ID'=>$_POST['post_id'], 'post_status'=>'pending'));
			if(!current_user_can('edit_post', $_POST['post_id']))
				wp_update_post(array('ID'=>$_POST['post_id'], 'post_status'=>'publish'));
			else
				$send_notifiction=true;
		}

		if ($_POST['post_id'] != -1 && !current_user_can('edit_post', $_POST['post_id']))
			throw new Exception(
				__("You don't have permission to edit this post.", 'frontend-publishing'),
				1
			);

		$fep_role_settings = get_option('fep_role_settings');
		$fep_misc = get_option('fep_misc');

		if ($fep_role_settings['no_check'] && current_user_can($fep_role_settings['no_check']))
			$errors = false;
		else
			$errors = fep_post_has_errors($_POST);

		if ($errors)
			throw new Exception($errors, 1);

		if ($fep_misc['nofollow_body_links'])
			$post_content = wp_rel_nofollow($_POST['post_content']);
		else
			$post_content = $_POST['post_content'];

		if (preg_match_all('!<img.*?src=["\']{1}http[s]?://(.*?)[/"\']!i',wp_kses_post($post_content), $image_host_match, PREG_PATTERN_ORDER)) {
			foreach($image_host_match[1] as $value) {
				if(!preg_match('!.*travpart.com$!', $value, $out)) {
					throw new Exception(
						__("Sorry, we don't allow external link to be written in your post.", 'frontend-publishing'),1
					);
				}
			}
		}
		
		if (preg_match_all('!<a.*?href=["\']{1}http[s]?://(.*?)[/"\']!i',wp_kses_post($post_content), $link_host_match, PREG_PATTERN_ORDER)) {
			foreach($link_host_match[1] as $value) {
				if(!preg_match('!.*(travpart.com|pixabay.com)$!', $value, $out)) {
					throw new Exception(
						__("Sorry, we don't allow external link to be written in your post.", 'frontend-publishing'),1
					);
				}
			}
		}

		$current_post = empty($_POST['post_id']) ? null : get_post($_POST['post_id']);
		$current_post_date = is_a($current_post, 'WP_Post') ? $current_post->post_date : '';

		$new_post = array(
			'post_title'     => sanitize_text_field($_POST['post_title']),
			'post_category'  => array($_POST['post_category']),
			'tags_input'     => sanitize_text_field($_POST['post_tags']),
			'post_content'   => wp_kses_post($post_content),
			'post_date'      => $current_post_date,
			'comment_status' => get_option('default_comment_status')
		);

		if ($fep_role_settings['instantly_publish'] && current_user_can($fep_role_settings['instantly_publish'])) {
			$post_action = __('published', 'frontend-publishing');
			$new_post['post_status'] = 'publish';
		} else {
			$post_action = __('submitted', 'frontend-publishing');
			$new_post['post_status'] = 'pending';
		}

		if ($_POST['post_id'] != -1) {
			$new_post['ID'] = $_POST['post_id'];
			$post_action = __('updated', 'frontend-publishing');
		}

		$new_post_id = wp_insert_post($new_post, true);
		if (is_wp_error($new_post_id))
			throw new Exception($new_post_id->get_error_message(), 1);

		if (!$fep_misc['disable_author_bio']) {
			if ($fep_misc['nofollow_bio_links'])
				$about_the_author = wp_rel_nofollow($_POST['about_the_author']);
			else
				$about_the_author = $_POST['about_the_author'];
			update_post_meta($new_post_id, 'about_the_author', $about_the_author);
		}
		
		if (empty(get_post_meta($new_post_id, 'tour_id', true))) {
            if (!empty($_COOKIE['btour_id'])) {
                global $wpdb;
                $tourid = intval($_COOKIE['btour_id']);
                $earn = intval($_COOKIE['earn']);
				if($earn==1)
					$data['show_banner']=true;
				if($earn==0)
					$data['show_earn_false_banner']=true;
                setcookie('earn', '0', (time() + 86400 * 30) , '/English/', '.travpart.com');
                setcookie('earn', '0', (time() + 86400 * 30) , '/english/', '.travpart.com');
                setcookie('btour_id', '', (time() + 86400 * 30) , '/English/', '.travpart.com');
                setcookie('btour_id', '', (time() + 86400 * 30) , '/english/', '.travpart.com');
                update_post_meta($new_post_id, 'tour_id', $tourid);
				update_post_meta($new_post_id, 'earn', $earn);
                /*$current_user = wp_get_current_user();
                $agent_username = $current_user->user_login;
                $urow = $wpdb->get_row("SELECT * FROM `user` WHERE `username`='{$agent_username}'");
                $budget_used = $wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m')");
                $reward_amount=floatval(get_option('reward_amount'))>0?floatval(get_option('reward_amount')):0;
				if ($earn == 1 && floatval($budget_used) < floatval(get_option('budget_limit'))) {
                    if ($wpdb->query("INSERT INTO `rewards` (`userid`, `tourid`, `commission`, `type`, `create_time`) VALUES ('{$urow->userid}', '{$tourid}', '{$reward_amount}', 'publish', NOW())")) {
                        if (empty($wpdb->get_row("SELECT * FROM `balance` WHERE userid='{$urow->userid}'"))) {
                            $wpdb->query("INSERT INTO `balance` (`userid`, `balance`, `update_time`) VALUES ('{$urow->userid}', '{$reward_amount}', CURRENT_TIMESTAMP)");
                        } else {
                            $wpdb->query("UPDATE `balance` SET balance=balance+{$reward_amount}, update_time=CURRENT_TIMESTAMP where userid='{$urow->userid}'");
                        }
                        if ($wpdb->get_var("SELECT COUNT(*) as publish_count FROM `rewards` WHERE userid='{$urow->userid}' AND type='publish'") == 1) {
                            $inviteby = $wpdb->get_row("SELECT * FROM `user` WHERE `username`='{$urow->inviteby}'");
                            if (!empty($inviteby->userid)) {
                                $invite_commisson = 0;
                                if ($wpdb->get_var("SELECT COUNT(*) as publish_count FROM `rewards` WHERE userid='{$inviteby->userid}' AND type='invite'") < 5) $invite_commisson = 1.0;
                                else $invite_commisson = 0.75;
                                $wpdb->query("INSERT INTO `rewards` (`userid`, `tourid`, `commission`, `type`, `create_time`) VALUES ('{$inviteby->userid}', '{$tourid}', '{$invite_commisson}', 'invite', NOW())");
                            }
                            if (empty($wpdb->get_row("SELECT * FROM `balance` WHERE userid='{$inviteby->userid}'"))) {
                                $wpdb->query("INSERT INTO `balance` (`userid`, `balance`, `update_time`) VALUES ('{$inviteby->userid}', '{$invite_commisson}', CURRENT_TIMESTAMP)");
                            } else {
                                $wpdb->query("UPDATE `balance` SET balance=balance+{$invite_commisson}, update_time=CURRENT_TIMESTAMP where userid='{$inviteby->userid}'");
                            }
                        }
                    }
                }*/
            }
        }
		
		if($send_notifiction && !empty(get_post_meta($new_post_id, 'tour_id', true))) {
			//send notification to admin email
			$notification_content='<p>Dear Travpart,
			<p>We just received a complete tour details post.</p>
			<p>Please check their post here <a href="'.get_permalink($new_post_id).'">'.sanitize_text_field($_POST['post_title']).'</a></p>';
			wp_mail(get_option('admin_email') , 'Travpart - Received a tour package submission', $notification_content);
		}
		
		if ($_POST['featured_img'] != -1)
			set_post_thumbnail($new_post_id, $_POST['featured_img']);

		$data['success'] = true;
		$data['post_id'] = $new_post_id;
		$data['message'] = sprintf(
			'%s<br/><a href="#" id="fep-continue-editing">%s</a>',
			sprintf(__('Your article has been %s successfully!', 'frontend-publishing'), $post_action),
			__('Continue Editing', 'frontend-publishing')
		);
	} catch (Exception $ex) {
		$data['success'] = false;
		$data['message'] = sprintf(
			'<strong>%s</strong><br/>%s',
			$fep_messages['general_form_error'],
			$ex->getMessage()
		);
	}
	die(json_encode($data));
}

add_action('wp_ajax_fep_process_form_input', 'fep_process_form_input');
add_action('wp_ajax_nopriv_fep_process_form_input', 'fep_process_form_input');