<?php
global $wpdb;
wp_enqueue_style('fep-style');
wp_enqueue_script('fep-script');
wp_enqueue_script('mce-view');
wp_enqueue_media();

$current_user = wp_get_current_user();
$post = false;
$post_id = -1;
$featured_img_html = '';
if (isset($_GET['fep_id']) && isset($_GET['fep_action']) && $_GET['fep_action'] == 'edit') {
	$post_id = $_GET['fep_id'];
	$p = get_post($post_id, 'ARRAY_A');
	if ($p['post_author'] != $current_user->ID) return __("You don't have permission to edit this post", 'frontend-publishing');
	$category = get_the_category($post_id);
	$tags = wp_get_post_tags($post_id, array('fields' => 'names'));
	$featured_img = get_post_thumbnail_id($post_id);
	$featured_img_html = (!empty($featured_img)) ? wp_get_attachment_image($featured_img, array(200, 200)) : '';
	$post = array(
		'title'            => $p['post_title'],
		'content'          => $p['post_content'],
		'about_the_author' => get_post_meta($post_id, 'about_the_author', true)
	);

	if (isset($category[0]) && is_array($category))
		$post['category'] = $category[0]->cat_ID;
	if (isset($tags) && is_array($tags))
		$post['tags'] = implode(', ', $tags);
}
?>
<style>
    .popper-tooltip.popper-tooltip-wide {
        max-width: 100%;
        text-align: left;
        position: relative;
        margin-top: 5px;
    }
    .popper-tooltip.popper-tooltip-wide[x-placement^="right"] .tooltip__arrow {
        top: 10px;
    }
    body {
      background: #fff;
    }
    .btn-blue {
      font-size: 18px!important;
      line-height: 1!important;
      padding: 22px 30px;
      font-weight: bold;
      width: 100%;
      color: #fff;
      border-radius: 5px;
      border: solid 1px #e2e4da;
      background-color: #339999;
    }
    .btn-blue:hover,.btn-blue:focus,.btn-blue:active {
      background-color: #2c7979!important;
    }
    button#fep-submit-post {
      font-size: 18px!important;
      line-height: 1!important;
      padding: 22px 30px!important;
      width: 100%;
      color: #fff;
      border-radius: 5px;
      border: solid 1px #e2e4da!important;
      background-color: #ff6633!important;
    }
    button#fep-submit-post:hover,button#fep-submit-post:focus,button#fep-submit-post:active {
      border: solid 1px #e2e4da!important;
      background-color: #f0603c !important;
    }
</style>

<noscript>
	<div id="no-js"
		 class="warning"><?php _e('This form needs JavaScript to function properly. Please turn on JavaScript and try again!', 'frontend-publishing'); ?></div>
</noscript>
<?php
$budget_used=$wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m')");
if (get_option('budget_limit')>0 && floatval($budget_used)<=floatval(get_option('budget_limit'))) {
    ?>
<div style="display:none;" class="publish_to_earn_message spu-open-18274"></div>
<div style="display:none;" class="publish_to_earn_false_message spu-open-18271"></div>
<?php } else { ?>
<div style="display:none;" class="publish_to_earn_message"></div>
<div style="display:none;" class="publish_to_earn_false_message"></div>
<?php } ?>
<div id="fep-new-post">
  <div class="row">
    <div class="col-md-8">
      <p>Ensure that you do the following instructions below for your tour package to be verified on our system. It might take hours or days for your tour package to be officially published. Because We just need to check if your content does not contains spams, pornography, gambling, drugs, and/or any other malicious illegal activity forbidden by the relevant local law.</p>
    </div>
  </div>
	<div id="fep-message" class="warning"></div>
	<form id="fep-submission-form">
		<label for="fep-post-title"><?php _e('Title', 'frontend-publishing'); ?></label>
        <div class="row">
            <div class="col-md-8">
                <input type="text" name="post_title" id="fep-post-title" value="<?php echo ($post) ? $post['title'] : ''; ?>">
            </div>
            <div class="col-md-4">
                <div id="newpost-title-tips" class="popper-tooltip popper-tooltip-wide" role="tooltip" x-placement="right"><div class="tooltip__arrow"></div><div class="tooltip__inner">
                    Point of Location, How many days and Night<br>
                    Good Title Example : Bali 5 days 4 Night
                </div></div>
            </div>
        </div>

        <label for="fep-category"><?php _e('Journey Type', 'frontend-publishing'); ?></label>
        <div class="row">
            <div class="col-md-8">
                <?php wp_dropdown_categories(array('id' => 'fep-category', 'hide_empty' => 0, 'name' => 'post_category', 'orderby' => 'name', 'selected' => $post['category'], 'hierarchical' => true, 'show_option_none' => __('None', 'frontend-publishing'))); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div id="fep-featured-image">
                    <div id="fep-featured-image-container"><?php echo $featured_img_html; ?></div>
                    <a id="fep-featured-image-link" href="#"><i class="fa fa-cloud-upload-alt"></i> <strong>Click to Choose File</strong><span>PNG, JPG, GIF, in-app cropping   </span></a>
                    <input type="hidden" id="fep-featured-image-id" value="<?php echo (!empty($featured_img)) ? $featured_img : '-1'; ?>"/>
                </div>
            </div>
        </div>



        <label for="fep-post-content"><?php _e('Content', 'frontend-publishing'); ?></label>
        <div class="row">
            <div class="col-md-8">
                <?php
                $enable_media = (isset($fep_roles['enable_media']) && $fep_roles['enable_media']) ? current_user_can($fep_roles['enable_media']) : 1;
				wp_editor(htmlspecialchars_decode($post['content'],ENT_QUOTES), 'fep-post-content', array('textarea_name' => 'post_content', 'textarea_rows' => 7, 'media_buttons' => $enable_media));
				wp_nonce_field('fepnonce_action', 'fepnonce');
                if(!empty($_GET['bookingcode']))
                    $tour_id=intval(substr($_GET['bookingcode'], 4));
                else if(!empty($_COOKIE['tour_id']))
                    $tour_id=$_COOKIE['tour_id'];
                else
                    $tour_id='';
                if(empty($tour_id)) {
                    $tour_id=get_post_meta($post_id, 'tour_id', true);
                }
                ?>
                <style>
                    #wp-fep-post-content-wrap {
                        margin-bottom: 0!important;
                    }
                    #post-status-info {
                        border-top: 1px solid #e5e5e5;
                        width: 100%;
                        border-spacing: 0;
                        border: 1px solid #e5e5e5;
                        background-color: #f7f7f7;
                        box-shadow: 0 1px 1px rgba(0,0,0,.04);
                        display: table;
                        border-collapse: separate;
                        margin-bottom: 20px!important;
                    }
                    #post-status-info td {
                        font-size: 12px;
                        text-align: left!important;
                    }
                    #wp-word-count,.max-word  {

                        padding: 2px 10px;
                    }
                    #post-status-info td.text-right {
                        text-align: right!important;
                    }
                </style>
                <table id="post-status-info" style=""><tbody><tr>
                        <td id="wp-word-count" class="hide-if-no-js">Word count: <span class="word-count">0</span></td>
                        <td  class="max-word text-right">Maximum: <?php $options = get_option('fep_post_restrictions'); echo $options['max_words_content']?>, Minimum:<?php  echo $options['min_words_content']?></td>
                    </tr></tbody></table>
                <script>
                    var tour_id=<?php echo $tour_id; ?>;
                    var thankYouCreatingPackagePage='<?php echo home_url(); ?>/thank-you-creating-package/';
                    ( function() {
                        function WordCounter( settings ) {
                            var key,
                                shortcodes;

                            // Apply provided settings to object settings.
                            if ( settings ) {
                                for ( key in settings ) {

                                    // Only apply valid settings.
                                    if ( settings.hasOwnProperty( key ) ) {
                                        this.settings[ key ] = settings[ key ];
                                    }
                                }
                            }

                            shortcodes = this.settings.l10n.shortcodes;

                            // If there are any localization shortcodes, add this as type in the settings.
                            if ( shortcodes && shortcodes.length ) {
                                this.settings.shortcodesRegExp = new RegExp( '\\[\\/?(?:' + shortcodes.join( '|' ) + ')[^\\]]*?\\]', 'g' );
                            }
                        }

                        // Default settings.
                        WordCounter.prototype.settings = {
                            HTMLRegExp: /<\/?[a-z][^>]*?>/gi,
                            HTMLcommentRegExp: /<!--[\s\S]*?-->/g,
                        spaceRegExp: /&nbsp;|&#160;/gi,
                            HTMLEntityRegExp: /&\S+?;/g,

                            // \u2014 = em-dash
                            connectorRegExp: /--|\u2014/g,

                            // Characters to be removed from input text.
                            removeRegExp: new RegExp( [
                            '[',

                            // Basic Latin (extract)
                            '\u0021-\u0040\u005B-\u0060\u007B-\u007E',

                            // Latin-1 Supplement (extract)
                            '\u0080-\u00BF\u00D7\u00F7',

                            /*
                             * The following range consists of:
                             * General Punctuation
                             * Superscripts and Subscripts
                             * Currency Symbols
                             * Combining Diacritical Marks for Symbols
                             * Letterlike Symbols
                             * Number Forms
                             * Arrows
                             * Mathematical Operators
                             * Miscellaneous Technical
                             * Control Pictures
                             * Optical Character Recognition
                             * Enclosed Alphanumerics
                             * Box Drawing
                             * Block Elements
                             * Geometric Shapes
                             * Miscellaneous Symbols
                             * Dingbats
                             * Miscellaneous Mathematical Symbols-A
                             * Supplemental Arrows-A
                             * Braille Patterns
                             * Supplemental Arrows-B
                             * Miscellaneous Mathematical Symbols-B
                             * Supplemental Mathematical Operators
                             * Miscellaneous Symbols and Arrows
                             */
                            '\u2000-\u2BFF',

                            // Supplemental Punctuation
                            '\u2E00-\u2E7F',
                            ']'
                        ].join( '' ), 'g' ),

                            // Remove UTF-16 surrogate points, see https://en.wikipedia.org/wiki/UTF-16#U.2BD800_to_U.2BDFFF
                            astralRegExp: /[\uD800-\uDBFF][\uDC00-\uDFFF]/g,
                            wordsRegExp: /\S\s+/g,
                            characters_excluding_spacesRegExp: /\S/g,

                            /*
                             * Match anything that is not a formatting character, excluding:
                             * \f = form feed
                             * \n = new line
                             * \r = carriage return
                             * \t = tab
                             * \v = vertical tab
                             * \u00AD = soft hyphen
                             * \u2028 = line separator
                             * \u2029 = paragraph separator
                             */
                            characters_including_spacesRegExp: /[^\f\n\r\t\v\u00AD\u2028\u2029]/g,
                            l10n: window.wordCountL10n || {}
                    };

                        /**
                         * Counts the number of words (or other specified type) in the specified text.
                         *
                         * @summary  Count the number of elements in a text.
                         *
                         * @since    2.6.0
                         * @memberof wp.utils.wordcounter
                         *
                         * @param {String}  text Text to count elements in.
                         * @param {String}  type Optional. Specify type to use.
                         *
                         * @return {Number} The number of items counted.
                         */
                        WordCounter.prototype.count = function( text, type ) {
                            var count = 0;

                            // Use default type if none was provided.
                            type = type || this.settings.l10n.type;

                            // Sanitize type to one of three possibilities: 'words', 'characters_excluding_spaces' or 'characters_including_spaces'.
                            if ( type !== 'characters_excluding_spaces' && type !== 'characters_including_spaces' ) {
                                type = 'words';
                            }

                            // If we have any text at all.
                            if ( text ) {
                                text = text + '\n';

                                // Replace all HTML with a new-line.
                                text = text.replace( this.settings.HTMLRegExp, '\n' );

                                // Remove all HTML comments.
                                text = text.replace( this.settings.HTMLcommentRegExp, '' );

                                // If a shortcode regular expression has been provided use it to remove shortcodes.
                                if ( this.settings.shortcodesRegExp ) {
                                    text = text.replace( this.settings.shortcodesRegExp, '\n' );
                                }

                                // Normalize non-breaking space to a normal space.
                                text = text.replace( this.settings.spaceRegExp, ' ' );

                                if ( type === 'words' ) {

                                    // Remove HTML Entities.
                                    text = text.replace( this.settings.HTMLEntityRegExp, '' );

                                    // Convert connectors to spaces to count attached text as words.
                                    text = text.replace( this.settings.connectorRegExp, ' ' );

                                    // Remove unwanted characters.
                                    text = text.replace( this.settings.removeRegExp, '' );
                                } else {

                                    // Convert HTML Entities to "a".
                                    text = text.replace( this.settings.HTMLEntityRegExp, 'a' );

                                    // Remove surrogate points.
                                    text = text.replace( this.settings.astralRegExp, 'a' );
                                }

                                // Match with the selected type regular expression to count the items.
                                text = text.match( this.settings[ type + 'RegExp' ] );

                                // If we have any matches, set the count to the number of items found.
                                if ( text ) {
                                    count = text.length;
                                }
                            }

                            return count;
                        };

                        // Add the WordCounter to the WP Utils.
                        window.wp = window.wp || {};
                        window.wp.utils = window.wp.utils || {};
                        window.wp.utils.WordCounter = WordCounter;
                    } )();
                    ( function( $, counter ) {
                        $( function() {
                            var $content = $( '#fep-post-content' ),
                                $count = $( '#wp-word-count' ).find( '.word-count' ),
                                prevCount = 0,
                                contentEditor;

                            /**
                             * Get the word count from TinyMCE and display it
                             */
                            function update() {
                                var text, count;

                                if ( ! contentEditor || contentEditor.isHidden() ) {
                                    text = $content.val();
                                } else {
                                    text = contentEditor.getContent( { format: 'raw' } );
                                }

                                count = counter.count( text );

                                if ( count !== prevCount ) {
                                    $count.text( count );
                                }

                                prevCount = count;
                            }

                            /**
                             * Bind the word count update triggers.
                             *
                             * When a node change in the main TinyMCE editor has been triggered.
                             * When a key has been released in the plain text content editor.
                             */
                            $( document ).on( 'tinymce-editor-init', function( event, editor ) {
                                if ( editor.id !== 'fep-post-content' ) {
                                    return;
                                }

                                contentEditor = editor;

                                editor.on( 'nodechange keyup', _.debounce( update, 1000 ) );
                            } );

                            $content.on( 'input keyup', _.debounce( update, 1000 ) );

                            update();
                        } );

                    } )( jQuery, new wp.utils.WordCounter() );


                </script>

                <?php if (!$fep_misc['disable_author_bio']): ?>
                  <label for="fep-about"><?php _e('Author Bio', 'frontend-publishing'); ?></label>
                  <div class="row">
                    <div class="col-md-8">
                      <textarea name="about_the_author" id="fep-about" rows="5"><?php echo ($post) ? $post['about_the_author'] : ''; ?></textarea>
                    </div>
                  </div>
                <?php else: ?>
                  <input type="hidden" name="about_the_author" id="fep-about" value="-1">
                <?php endif; ?>

                <?php/*<label for="fep-tags"><?php _e('Tags', 'frontend-publishing'); ?></label>*/?>
              <input type="hidden" name="post_tags" id="fep-tags" value="<?php echo ($post) ? $post['tags'] : ''; ?>">

              <input type="hidden" name="post_id" id="fep-post-id" value="<?php echo $post_id ?>">
              <div class="row mt-4 mb-4">
                <div class="col-sm-6">
                  <a class="btn btn-blue btn-block" href="<?php echo site_url().'/tour-details/?bookingcode=BALI'.$tour_id;?>" target="_blank" data-toggle="tooltip" title="If you forgot your tour details and the travel item list in the last page. Click here to review again."><i class="fa fa-list mr-2"></i> Check my tour details again</a>
                  <img class="mt-2" src="https://www.travpart.com/English/wp-content/themes/bali/images/notify-previous.png" alt="">
                </div>
                <div class="col-sm-6">
                  <button type="button" id="fep-submit-post" class="active-btn btn btn-org btn-block"><i class="fas fa-cloud-upload-alt mr-2"></i> Publish & Preview Post </button>
                  <img  class="mt-2" src="https://www.travpart.com/English/wp-content/themes/bali/images/notify-next.png" alt="">
                </div>
              </div>

              <img class="fep-loading-img" src="<?php echo plugins_url('static/img/ajax-loading.gif', dirname(__FILE__)); ?>"/>

            </div>
            <div class="col-md-4">
              <img class="mb-2" src="https://www.travpart.com/English/wp-content/themes/bali/images/notify-insert.png" alt="">
                <div id="newpost-content-tips" class="popper-tooltip popper-tooltip-wide" role="tooltip" x-placement="right"><div class="tooltip__arrow"></div><div class="tooltip__inner">
                      You are advised to insert as many details and picture/video as possible. You may also include a brief personal opinion, local news, blogs, cultures experience, and/or anything else that will fascinates your potential viewers about the journey. <br><br>
                      You may refer to this tour package page's design <a href="https://www.travpart.com/English/bali-2-days-1-night/" target="_blank">BALI1870</a><br>
                       <br>
                       An Example of a good complete tour detail content :<br>
                        <br>
                        Day 1<br>
                        Visit Uluwatu Temple at 09:00<br>
                        &nbsp;&nbsp;- Bali Cultural Heritage Temple<br>
                        Lunch at Indonesian  Restaurant at 12:00<br>
                        &nbsp;&nbsp;- Included meals and drinks<br>
                        Rest in Hotel Jambu, Bali Area at 20:00<br>
                        <br>
                        Day 2<br>
                        Depart to Kuta Beach at 08:00 from your hotel.<br>
                        Snorkeling at Kuta Beach at 09:00<br>
                        &nbsp;&nbsp;- View the great oceanic coral reef<br>
                        Dinner at American Restaurant at 18:00<br>
                        &nbsp;&nbsp;- Not Included meals and drinks<br>
                        Rest in Hotel Pullman, Kuta Beach area at 20:00<br>
                        <br>
                        My friends took this journey and they were having fun singing on the beach 
                        while cooking barbecue.<br>
                        <br>
                        
                </div></div>

            </div>
        </div>


	</form>
</div>