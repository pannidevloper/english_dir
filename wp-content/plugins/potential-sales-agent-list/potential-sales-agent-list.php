<?php
/**
 * Plugin Name: Potential Travel advisor List
 * Version:     1.4
 * Author:      Lix
 * Description: Potential Travel advisor List
 */
register_activation_hook(__FILE__, 'potential_travel_advisor_list_activate');

function potential_travel_advisor_list_activate() {
    
}

/* * *************************** show the subscribe in admin page  ***************************** */

add_action('admin_menu', 'add_potential_travel_advisor_list_admin_menu');
add_action('admin_init', 'add_potential_travel_advisor_list_admin_menu_init');

function add_potential_travel_advisor_list_admin_menu() {
    add_options_page('Potential Travel Advisor List', 'Potential Travel Advisor List', 'manage_options', __FILE__, 'show_potential_travel_advisor_list');
}

function add_potential_travel_advisor_list_admin_menu_init() {
    wp_register_style('potential-travel-advisor-list', plugins_url('css/datatables.min.css', __FILE__));
    wp_enqueue_style('potential-travel-advisor-list');
    wp_register_script('potential-travel-advisor-list', plugins_url('js/datatables.min.js', __FILE__));
    wp_enqueue_script('potential-travel-advisor-list');
}

function show_potential_travel_advisor_list() {
    global $wpdb;
	
	if(!empty($_POST['import'])) {
		if($xlsx=SimpleXLSX::parse($_FILES['import_file_upload']['tmp_name'])) {
			$xlsx_data=$xlsx->rows();
			$name_index=0;
			$company_index=6;
			$phone_index=2;
			$email_index=1;	
			$location_index=3;
			for($i=1; $i<count($xlsx_data) && !empty($xlsx_data[$i][$email_index]); $i++) {
				if($wpdb->get_var("SELECT COUNT(*) FROM `wp_potential_sale_agent` WHERE email='{$xlsx_data[$i][$email_index]}'")<1
				   && $wpdb->get_var("SELECT COUNT(*) FROM `user` WHERE email='{$xlsx_data[$i][$email_index]}'")<1 ) {
					$wpdb->insert('wp_potential_sale_agent',array(
						'name'=>$xlsx_data[$i][$name_index],
						'email'=>$xlsx_data[$i][$email_index],
						'phone'=>$xlsx_data[$i][$phone_index],
						'location'=>$xlsx_data[$i][$location_index],
					));
				}
			}
			echo '<div><p style="background: darkgrey;color: green;">Upload success!</p></div>';
		}
		else {
			echo 'The file is invaild.';
		}
	}

    $AgentList = $wpdb->get_results("SELECT * FROM `wp_potential_sale_agent`");
	//$lasted_email_send_time=strtotime($wpdb->get_var("SELECT csa_created FROM `contact_sales_agent` ORDER BY `csa_created` DESC LIMIT 1"));
    ?>

    <table id="potentialTravelAdvisorList" style="text-align:left;">
        <thead>
            <tr>
				<th>Company/Name</th>
                <th>Email Address</th>
                <th>Phone Number</th> 
                <th>Country</th>
				<th>Responsiveness</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($AgentList as $r) {
				$lasted_email_send_time=$wpdb->get_var("SELECT `csa_created` FROM `contact_sales_agent` WHERE `csa_created`>'{$r->create_time}' ORDER BY `csa_created` ASC LIMIT 1");
				$create_time=strtotime($r->create_time);
                ?>
                <tr>
                    <td><?php echo $r->name; ?></td>
                    <td><?php echo $r->email; ?></td>
                    <td><?php echo $r->phone; ?></td>
                    <td><?php echo $r->location; ?></td>
					<td>
					<?php
					if(!empty($lasted_email_send_time) && $create_time<strtotime($lasted_email_send_time)) {
						if((time()-strtotime($lasted_email_send_time))<=15*24*60*60)
							echo 'Normal';
						else
							echo 'Suspend';
					}
					else
						echo 'Normal';
					?>
					</td>
                </tr>
            <?php } ?>
            <?php if (count($AgentList) <= 0) { ?>
                <tr> <td colspan="7">The Potential Travel Advisor List is empty!</td> </tr>
    <?php } ?>
        </tbody>
    </table>
	
	<div id="data-import-form" class="postbox">
		<form action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST" enctype="multipart/form-data" >
			<table class="tablepress-postbox-table fixed">
				<tbody>
					<tr style="display: table-row;">
						<th scope="row">Select file:</th>
						<td>
							<input name="import_file_upload" type="file" style="box-sizing: border-box;">
						</td>
					</tr>
					<tr>
						<th scope="row">Import Format:</th>
						<td>
							<select name="import[format]">
								<option value="xlsx">XLSX - Microsoft Excel 2007-2013 (experimental)</option>
							</select>
						</td>
					</tr>
					<tr>
						<th scope="row"></th>
						<td><input type="submit" value="Import" name="import"></td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
	
    <script>
        jQuery(document).ready(function () {
            jQuery('#potentialTravelAdvisorList').DataTable({
                dom: 'Bfrtip',
                pageLength: 15,
                ordering: true,
                "search": {
                    "caseInsenstive": false
                },
                "buttons": [
                    {
                        'extend': 'excelHtml5',
                        'text': 'Download',
                        'exportOptions': {
                            'modifier': {
                            }
                        }
                    }
                ]
            });

        });
    </script>

    <?php
}
?>
