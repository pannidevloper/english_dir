<?php
/*
Plugin Name: automatic  Emails For Travpart
Description: Send Emails  to users
Author: M Farhan
Version: 1.0
*/

//wp_enqueue_style( 'hook_custom_wp_admin_css', plugins_url('css/notifactionsstying.css', __FILE__) );
include_once(ABSPATH . 'wp-includes/pluggable.php');
add_action('admin_menu', 'au_email_admin_menu');



function au_email_admin_menu()
{
    add_menu_page('AU Emails', 'AU_Email', 'manage_options', 'AU_Email');

    add_submenu_page(
        'covid19',
        'AU_Email',
        'AU Email',
        'manage_options',
        'AU_Email',
        'AU_Email'
    );
}
include('emails/full-name-is-following-your-travparts-page.php');


if (isset($_GET['page']) && $_GET['page'] == 'AU_Email') {

   
    //wp_unseen_notifaction_cron_weekly();
 
    //print_r($suggestions_ids);

    //some_one_see_your_page();
    //some_one_following_your_page_email('farhancsuet@gmail.com','johny','https://www.travpart.com/English/wp-content/uploads/ultimatemember/261/profile_photo-190x190.png?1594080359',5);
    //weekly_suggestions_for_user();
}


 

function wp_unseen_notifaction_cron_weekly()
{
    include('emails/notifactions.php');
    global $wpdb;

    $get_users_list = $wpdb->get_results("SELECT ID,user_email,display_name FROM `wp_users`");

    foreach ($get_users_list as $user) {
        $enable_notification=$wpdb->get_var("SELECT subscribe FROM `unsubscribed_emails` WHERE email='{$user->user_email}' AND type=3");
        if (is_null($enable_notification) || $enable_notification==1) {
            $userinfo=$wpdb->get_row("SELECT username,user_registered FROM user WHERE email='{$user->user_email}'");
            $unsubscribe_link=home_url('unsubscribe').'/?qs='.base64_encode($user->user_email.'||'.md5('EC'.$userinfo->username.$user->user_email.$userinfo->user_registered));
            $notifications_count = $wpdb->get_var("SELECT COUNT(*) FROM notification WHERE wp_user_id='{$user->ID}' AND read_status=0");
            if ($notifications_count>5) {
                //$user_name = um_get_display_name ( $user->ID );
                $user_name = $user->display_name;
                $user_image = get_avatar_url($user->ID);
                send_weekly_notifactions_count($user->user_email, $user_name, $notifications_count, $user_image, $unsubscribe_link);
                sleep(15);
            }
        }
    }
}
add_action('wp_unseen_notifaction_cron_weekly', 'wp_unseen_notifaction_cron_weekly');

function cronstarter_activation()
{
    if (!wp_next_scheduled('wp_unseen_notifaction_cron_weekly')) {
     wp_schedule_event(strtotime( 'next Monday' ), 'weekly', 'wp_unseen_notifaction_cron_weekly');
    }
    if (!wp_next_scheduled('weekly_suggestions_for_user')) {
        wp_schedule_event(strtotime( 'next Monday' ), 'weekly', 'weekly_suggestions_for_user');
    }
    if (!wp_next_scheduled('monthly_seller_report_email')) {
        wp_schedule_event(strtotime( 'next Friday' ), 'monthly', 'monthly_seller_report_email');
    }
}
// and make sure it's called whenever WordPress loads
register_activation_hook( __FILE__, 'cronstarter_activation' );


function cronstarter_deactivate()
{
    // find out when the last event was scheduled
    $timestamp = wp_next_scheduled('wp_unseen_notifaction_cron_weekly');
    // unschedule previous event if any
    wp_unschedule_event($timestamp, 'wp_unseen_notifaction_cron_weekly');

    wp_unschedule_event($timestamp, 'weekly_suggestions_for_user');

    wp_unschedule_event($timestamp, 'monthly_seller_report_email');
}
register_deactivation_hook(__FILE__, 'cronstarter_deactivate');



function weekly_suggestions_for_user()
{
    include('emails/full-name-see-full-name-and-more-in-your-feed.php');
    global $wpdb;
    $args = array(
        'role'    => 'um_clients',
        'orderby' => 'user_nicename',
        'order'   => 'ASC'
        // 'include' => array(1316,1047,3),
         //'number'=>3,
    );
    $users = get_users($args);
    foreach ($users as $user) {
        $enable_notification=$wpdb->get_var("SELECT subscribe FROM `unsubscribed_emails` WHERE email='{$user->user_email}' AND type=7");
        if (is_null($enable_notification) || $enable_notification==1) {
            $userinfo=$wpdb->get_row("SELECT username,user_registered FROM user WHERE email='{$user->user_email}'");
            $unsubscribe_link=home_url('unsubscribe').'/?qs='.base64_encode($user->user_email.'||'.md5('EC'.$userinfo->username.$user->user_email.$userinfo->user_registered));
            
            $current_user_id= $user->ID;

        $suggestions = $wpdb->get_results("SELECT suggestion_id AS ID FROM `friends_suggestions` WHERE wp_user_id={$current_user_id} ORDER BY total_score DESC LIMIT 5");

            
        if(!empty($suggestions) || count ($suggestions) >2 ){

            foreach ($suggestions as $value) {
               if (get_avatar_url($value->ID)!='https://www.travpart.com/English/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg') {
                    $search_idss[] = $value->ID;
                }
            }
        }
            

            if ($search_idss) {
                $count =  count($search_idss);
                if ($count>2) {
                    $user_id = $search_idss[0];
                    $user_id2= $search_idss[1];

                   $user_name = um_get_display_name($user_id);
                   $user_nam2 = um_get_display_name($user_id2);
                    $user_image = get_avatar_url($user_id);
                    $email= $value->user_email;
                some_one_see_your_page($user_image, $user_name, $user_nam2, $email, $unsubscribe_link);
                    sleep(10);
                }
            }
       }
    }// users for each loop
    
}

add_action('weekly_suggestions_for_user', 'weekly_suggestions_for_user');



function monthly_seller_report_email()
{
    include'emails/service_provider.php';
    global $wpdb;
    $args = array(
        'role'    => 'um_travel-advisor',
        'orderby' => 'user_nicename',
        'order'   => 'ASC',
      //  'include' => array(261,794),
        // 'number'=>3,
    );
   // $users = get_users($args);
    foreach ($users as $user) {
        $enable_notification=$wpdb->get_var("SELECT subscribe FROM `unsubscribed_emails` WHERE email='{$user->user_email}' AND type=5");
        if (is_null($enable_notification) || $enable_notification==1) {
            $userinfo=$wpdb->get_row("SELECT username,user_registered FROM user WHERE email='{$user->user_email}'");
            $unsubscribe_link=home_url('unsubscribe').'/?qs='.base64_encode($user->user_email.'||'.md5('EC'.$userinfo->username.$user->user_email.$userinfo->user_registered));
            service_provider($user->ID, $unsubscribe_link);
        }
    }
}
//add_shortcode ('test_seller_report','monthly_seller_report_email');

add_action('monthly_seller_report_email', 'monthly_seller_report_email');