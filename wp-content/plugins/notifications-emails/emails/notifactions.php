<?php
/*Template Name: list-notifications */ 

function send_weekly_notifactions_count($to,$username,$no_of_notifactions,$Image, $unsubscribe_link){
?>

<?php 
$msg = '<div >
	<div style="border-radius: 3px;
		margin: auto;
		background: #fff;
		box-shadow: 0px 0px 9px 1px #ccc;
		padding: 10px;">
		<div >
			<div  style="display: inline-block;
		vertical-align: middle;">
				<img src="https://www.travpart.com/English/wp-content/themes/bali/images/lglogo.png" />
			</div>
			<div style="display: inline-block;
		vertical-align: middle;">
		<h4 style="margin: 0px;  
		padding-left: 10px;
		color: #0d9033;">
		<strong>You have new notifications</strong>
		</h4>
			</div>
		</div>
		<div style="	padding: 10px;
		font-size:18px;
		font-weight: bold;">
			<p>A lot is going on Travpart since the last time you were around. Here are few notifications that you may have missed and interested from your connections.</p>
		</div>
		<div style="border: 1px solid #ccc;
		padding: 30px;
		padding-top: 10px !important;">
			
			<div >
				<div >
					<img style="width: auto;height: 50px;padding: 5px;
		border: 1px solid #000;
		border-radius: 3px;" src='.$Image.' />
				</div> 	
				<div style="
		font-size: 15px;
		font-weight: bold;">
					'.$username.'
				</div>
			</div>
			<div style="
		font-size: 20px;
		font-weight: bold;">
				<p>'.$no_of_notifactions.' Unread Notifications </p>
			</div>
		</div>
		
		<div style="text-align: center;
	    padding: 10px;">
			<div  style="width: 45%;
		text-align: center;
		display: inline-block;
		margin: 5px;">
				<a href="https://www.travpart.com" style="background: none !important;
		width: 100%;
		font-size: 15px;
		color: #22b14c !important;
		font-weight: bold;
		border: 1px solid #22b14c;padding: 10px;
		border-radius: 0px;">Go To Travpart</a>
			</div>
			<div style="width: 45%;
		text-align: center;
		display: inline-block;
		margin: 5px;">
				<a href="https://www.travpart.com/English/see-all-notifications/"  style="width: 100%;
		background: #22b14c !important;
		font-size: 15px;
		font-weight: bold;
		color: #fff !important;
		border: 1px solid green;
		border-radius: 0px;padding: 10px;">Notifications</a>
			</div>
		</div>
		<div style="font-size: 15px;
		text-align: center;
		padding: 10px;">
			<hr />
			<p style="text-align: center;font-size: 12px;"><span style="color: #999999;">Jl. Dewi Sartika Jakarta, RT.5/RW.2, Indonesia 13630<br />
				&copy; <a href="http://www.travpart.com">www.travpart.com</a></span>
			</p>
			<p style="text-align: center;font-size: 12px;"><span style="color: #999999;">You are receiving this email because of your relationship with us.<br />
			<a href="'.$unsubscribe_link.'" target="_blank" rel="noopener">Unsubscribe</a>&nbsp;from travpart.<br /></span></p>
		</div>
		
		<div style="text-align: center;
	padding: 10px;
	font-size: 15px;
	color: #a9a9a9">
			<p style="margin: 10px;">
				<p style="text-align: center;">&nbsp;<a href="https://www.travpart.com/English/contact/">Contact us</a>&nbsp;&bull; <a href="https://www.travpart.com/English/termsofuse/">Privacy&amp;&nbsp;Policy</a></p>
			</p>
		
		</div>
		
	</div>
</div>';
$subject = $username." you have ".$no_of_notifactions." new notifications";
$headers = array('Content-Type: text/html; charset=UTF-8');
wp_mail( $to, $subject, $msg, $headers );
} ?>


