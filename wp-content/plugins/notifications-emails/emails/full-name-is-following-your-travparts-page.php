<?php
/*Template Name: list-notifications */ 

function some_one_following_your_page_email($to,$follower_name,$follower_image,$connection_count,$unsubscribe_link){
?>

<?php 
$msg = '<html>
<head>
<title>Full</title>
<link async="async" rel="icon" href="https://www.travpart.com/English/wp-content/uploads/2020/01/travpart_icon_lnA_icon.ico"/> 
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
<meta name="" content="">
</head>
<body data-rsssl=1 data-rsssl=1 style="font-family: Open Sans, sans-serif;">
	<div style="margin: auto;width: 540px;background-color: #f0f0f0;padding: 15px;"> 
	   <div>
			<div  style="width: 210px;margin-bottom: 25px;">
				<img style="width: 100%;" src="https://www.travpart.com/English/wp-content/uploads/2020/02/travpart-logo.png" />
			</div>
			<div  style="display: flex;align-items: flex-start;background-color: #fff;border: 1px solid #ccc;padding: 10px;">
				<div><img style="width:80px;" src='.$follower_image.'></div>
				<div style="margin-left: 15px;font-size: 16px;"><span style="color: #1c0488;font-size: 16px;">'.$follower_name.'</span> is following your TravParts page <div style="color: #ccc;font-size: 13px;">'.$connection_count.' connections</div></div>
			</div>			
			<div style="text-align: center; padding: 0;margin: 0;font-size: 12px;color: #45484b9c;margin: 0 auto;border-top: 1px solid #ccc;
    margin-top: 24px; width: 368px;padding-top: 10px;">
<p style="text-align: center;"><span style="color: #999999;">Jl. Dewi Sartika Jakarta, RT.5/RW.2, Indonesia 13630<br /><a href="https://www.travpart.com">www.travpart.com</a></span></p>
<p style="text-align: center;"><span style="color: #999999;">You are receiving this email because of your relationship with us.<br /><a href="'.$unsubscribe_link.'" target="_blank" rel="noopener">Unsubscribe</a>&nbsp;from travpart.<br /></span></p>
<p style="text-align: center;">&nbsp;<a href="https://www.travpart.com/English/contact/">Contact us</a>&nbsp;&bull; <a href="https://www.travpart.com/English/termsofuse/">Privacy&amp;&nbsp;Policy</a></p></div>
		</div> <!-- inner_rapper -->
	</div><!-- email_pro_wrap -->
</body>
</html>';
//$subject = $username." you have ".$no_of_notifactions." new notifications";
$subject= $follower_name." is following your Travpart's page ";
$headers = array('Content-Type: text/html; charset=UTF-8');
wp_mail( $to, $subject, $msg, $headers );
} ?>


