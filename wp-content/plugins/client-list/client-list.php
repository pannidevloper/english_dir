<?php
/**
 * Plugin Name: Client  List
 * Version:     1.63
 * Author:      Lix
 * Description: Client List
 */

//Added Chat Downloading Option
register_activation_hook(__FILE__, 'client_list_activate');

function client_list_activate() {

}

/* * *************************** show the subscribe in admin page  ***************************** */

add_action('admin_menu', 'add_client_list_admin_menu');
add_action('admin_init', 'add_client_list_admin_menu_init');


function add_client_list_admin_menu() {
    add_options_page('Client List', 'Client List', 'manage_options', __FILE__, 'show_client_lists');
}

function add_client_list_admin_menu_init() {
    wp_register_style('client-list-css', plugins_url('css/datatables.min.css', __FILE__));
    wp_enqueue_style('client-list-css');
    wp_register_script('client-list-js', plugins_url('js/datatables.min.js', __FILE__));
    wp_register_script('client-list-js-natural', plugins_url('js/datatables-natural.js', __FILE__));
    wp_enqueue_script('client-list-js');
    wp_enqueue_script('client-list-js-natural');
}


function show_client_lists() {
    global $wpdb;
    global $wp;

    date_default_timezone_set('Asia/Jakarta');

    $args = array(
        'role'    => 'um_clients',
        'orderby' => 'user_nicename',
        'order'   => 'ASC',
    );
    $ClientList = get_users( $args );
  
    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

   
    <table id="ClientList" style="text-align:left;">
    <thead>
        <tr>
          <th>Username</th>
          <th>Name</th>
          <th>Registered Date</th>
          <th>Email</th>
        </tr>
    </thead>
    <tbody>
      <?php
      if($ClientList){ 

      	foreach ( $ClientList as $user ) { 
        $fullname = $user->first_name. ' '.$user->last_name;
        if($user->first_name == ""){
          $name = $user->user_login;
        }else{
          $name = $fullname;
        }
         ?>
  
      	<tr>
        	<td><?php echo esc_html( $user->user_login ); ?></td>
        	<td><?php echo $name;  ?></td>
        	<td><?php echo esc_html( $user->user_registered ); ?></td>
        	<td><?php echo esc_html( $user->user_email ); ?></td>
      	</tr> <?php
  
    	}
    }


    ?>
    </tbody>
    </table>
    <script>
        jQuery(document).ready(function () {
            jQuery('#ClientList').DataTable({
                dom: 'Bfrtip',
                pageLength: 15,
                ordering: true,
        
                "search": {
                    "caseInsenstive": false
                },
                "buttons": [
                    {
                        'extend': 'excelHtml5',
                        'text': 'Download',
                        'exportOptions': {
                            'modifier': {
                            }
                        }
                    }
                ]
            });
        });
    </script>

    <?php
}
?>
