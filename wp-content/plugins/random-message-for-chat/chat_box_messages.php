<?php
/**
 * Plugin Name: Random messages for chat box
 * Version:     1.1
 * Author:      Farhan
 * Description: Add Random Message for showing to users..
 */


// function to create the DB table on install	


	function chat_messages_table() {


	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();

	$table_name = $wpdb->prefix . 'chat_messages';

	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	msg text NOT NULL,
	PRIMARY KEY  (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	dbDelta( $sql );

	}

	// run the install scripts upon plugin activation
register_activation_hook(__FILE__,'chat_messages_table');


 // Add admin sitting

  add_action( 'admin_menu', 'my_admin_menu' );

	function my_admin_menu() {
		
	add_menu_page( 'Add chat Message', 'Chat Messages', 'manage_options', 'chat_messages.php', 'wp_add_chat_msg', 'dashicons-tickets', 6  );
		
	}


if ( isset( $_GET['page'] ) && $_GET['page'] == 'chat_messages.php' ) {

	wp_enqueue_style( 'custom_wp_admin_css', plugins_url('chatmsg.css', __FILE__) );

}

add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

function wp_add_chat_msg() {
	global $wpdb;
	$msg_table = $wpdb->prefix . 'chat_messages';
?>

<div class="row">
	<div class="col-sm-6">
		<form action="<?php  echo  $_SERVER['REQUEST_URI'] ?>" method="post">
			<div>
				<label for="username"><h3>chat message</h3></label>
				<textarea rows="4" cols="50" name="msg" required=""></textarea>
			</div>
			<div>
				<select name="type">
				<option value="0">Tour page</option>
				<option value="1">Main page</option>
				</select>
			</div>
			<div>
				<input class="inputstyle" type="submit" name="submit" value="Add"/>
			</div>
		</form>
	</div>


</div>
<div>

</div>


		<?php

			if(isset($_POST['submit'])){

				$msg = $_POST['msg'];
				$type = intval($_POST['type']);
				$wpdb->insert( 
					$msg_table,
					array( 
						'msg' => $msg,
						'type' => $type,
					), 
					array( 
						'%s',
						'%d',
					) 
				);

			}
			if(isset($_POST['remove'])){

				$id = $_POST['id'];

				$wpdb->delete( $msg_table, array( 'id' => $id ), array( '%s' ) );
				echo  '<p><h3> Message with id ('.$id.')  deleted</h3></p>';

			}?>

<h2 style="color:green">
	Messages List
</h2>
<?php
			$records = $wpdb->get_results( 
				"SELECT * FROM  $msg_table ORDER BY id"
			);
			if(!empty($records)):
?> 

<table id="customers">
	<tr>
		<th>id</th>
		<th>msg</th>
		<th>Type</th>
		<th>Reply Count</th>
	</tr>

	<?php

			foreach ( $records as $result ) {

				echo "<tr><td>";
				echo $result->id;
				echo "</td>";
				echo "<td>";
				echo $result->msg;
				echo "</td>";
				echo '<td>'.(($result->type==0)?'Tour page':'Main page').'</td>';
				echo '<td>'.$result->reply_count.'</td>';
				echo "<td><form action= ".$_SERVER['REQUEST_URI']." method='post'>";
				echo '<input type="hidden" name="id" value="'.$result->id.'"/>';
				echo '<input class="deletestyle" type="submit" name="remove" value="Delete"/>';
				echo "</td></form>";

				echo"</tr>";
			}
			else:
			echo "No data";
			endif;
	?>
	</tr>
</table>


<?php
		}