<?php

/**
 * Plugin Name: User trace
 * Version:     1.0
 * Author:      Lix
 * Description: Trace the keyword which user searched and the page which user visited
 */

register_activation_hook(__FILE__, 'user_trace_activate');
function user_trace_activate()
{ }

add_action('admin_menu', 'add_user_trace_admin_menu');
function add_user_trace_admin_menu()
{
    add_options_page('User trace', 'User Trace', 'manage_options', __FILE__, 'display_user_trace_data');
}

function display_user_trace_data()
{
    global $wpdb;
    $type = (empty($_GET['type']) || $_GET['type'] == 'keyword') ? 'keyword' : 'page';
    if ($type == 'keyword') {
        $data = $wpdb->get_results("SELECT wp_users.user_login,wp_users.user_email,wp_search_keywords.kw
            FROM `wp_users`,`wp_user_trace`,`wp_search_keywords`
            WHERE wp_users.ID=wp_user_trace.user_id AND type='keyword' AND value=wp_search_keywords.id");
        ?>
        <table id="UserTraceList" style="text-align:left;width:60%">
            <thead>
                <tr style="text-align:center;">
                    <th>Keyword trace</th>
                    <th></th>
                    <th><a href="<?=add_query_arg(array('type' => 'page'))?>">Page Trace</a></th>
                </tr>
                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Keyword</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $row) { ?>
                <tr>
                    <td><?=$row->user_login?></td>
                    <td><?=$row->user_email?></td>
                    <td><?=$row->kw?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php
    }
    else {
        $data = $wpdb->get_results("SELECT wp_users.user_login,wp_users.user_email,value,wp_posts.post_title
            FROM `wp_users`,`wp_user_trace`,`wp_posts`
            WHERE wp_users.ID=wp_user_trace.user_id AND type='page' AND wp_posts.ID=value");
        ?>
        <table id="UserTraceList" style="text-align:left;width:60%">
            <thead>
                <tr style="text-align:center;">
                    <th><a href="<?=add_query_arg(array('type' => 'keyword'))?>">Keyword trace</a></th>
                    <th></th>
                    <th>Page Trace</th>
                </tr>
                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Page</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $row) { ?>
                <tr>
                    <td><?=$row->user_login?></td>
                    <td><?=$row->user_email?></td>
                    <td><a href="<?=get_permalink($row->value)?>" target="_blank"><?=$row->post_title?></a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php
    }
    ?>
    <script>
    jQuery(document).ready(function() {
        jQuery('#UserTraceList').DataTable({
            dom: 'Bfrtip',
            pageLength: 15,
            ordering: true,
            "search": {
                "caseInsenstive": false
            },
            "buttons": [{
                'extend': 'excelHtml5',
                'text': 'Download',
                'exportOptions': {
                    'modifier': {}
                }
            }]
        });
    });
    </script>
    <?php
}