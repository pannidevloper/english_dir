<?php

/**
 * Plugin Name: Travcust meal categories
 * Version:     1.0
 * Author:      Lix
 * Text Domain: Travcust meal categories
 * Description: Travcust meal categories
 */


register_activation_hook(__FILE__, 'meal_categories_activate');

function meal_categories_activate()
{

}

/***************************** show the subscribe in admin page  ******************************/

add_action('admin_menu', 'add_meal_categories_admin_menu');
add_action('admin_init', 'add_meal_categories_admin_menu_init');
function add_meal_categories_admin_menu()
{
	add_options_page('Travcust meal categories', 'Travcust meal categories', 'manage_travcust_data', __FILE__, 'show_meal_categories');
}

function add_meal_categories_admin_menu_init()
{

}
function show_meal_categories()
{
    ?>
 <h2>Travcust meal categories </h2>
 <form action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST" >
 <?php

if ($_POST['save']) {
    $i = 1;
    $MealCategories = array();
    while ($i <= (int)$_POST['ea_length']) {
        if (!empty($_POST["ea{$i}_item"])) {
			$key=md5($_POST["ea{$i}_item"]);
			$MealCategories[$key]=$_POST["ea{$i}_img"];
        }
        $i++;
    }
    if (!empty($MealCategories)) {
        update_option('_cs_meal_categories', serialize($MealCategories));
    }
}

$url = "https://developers.zomato.com/api/v2.1/categories";
$headers = array(
    'Content-type: application/json',
    'Accept: application/json',
    'user-key: 0138313f73553d4524409f5b600a1ac8'
);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
$data = curl_exec($ch);
$categories = json_decode($data,TRUE)['categories'];
if (curl_error($ch)) {
    echo '<b>Error: Loading meal categories failed. Please try again or connect developers!</b>';
	$categories = array();
}
curl_close($ch);
$MealCategories = unserialize(get_option('_cs_meal_categories'));
//var_dump($categories);
echo '<table class="MealCategories">
				<thead>
					<tr><th>Category</th><th>Img</th></tr>
				</thead>
				<tbody>
                ';
for ($i = 1; $i <= count($categories); $i++) {
	$key=md5($categories[$i - 1]['categories']['name']);
    echo '<tr><th><input readonly="readonly" name="ea' . $i . '_item" type="text" value="' . $categories[$i - 1]['categories']['name'] . '" /></th><th><img width=64 height=64 src="' . (empty($MealCategories[$key])?'':$MealCategories[$key]) . '" style="display:none;" /><input class="ea_img_upload" type="button" value="Upload img" /><input name="ea' . $i . '_img" type="hidden" value="' . (empty($MealCategories[$key])?'':$MealCategories[$key]) . '" /></th>';
}
echo '	</tbody>
			  </table>
			  <input id="ea_length" name="ea_length" type="hidden" value="' . count($categories) . '"/>
			  <br/><br/>
			  <input type="submit" name="save" id="save" class="button button-primary button-large" value="Save">
			  <br/><br/>';
echo "<script>
				jQuery(document).ready(function(){
					
					jQuery('.ea_img_upload').each( function(){
						var t=jQuery(this).prev();
						if(t.attr('src'))
						{
							t.show();
						}
					});
					
					var upload_frame;   
					var value_id;
					var t;
					jQuery(document).on('click', '.ea_img_upload', function(event){
						t=jQuery(this);
						value_id =jQuery( this ).attr('id');       
						event.preventDefault();   
						if( upload_frame ){   
							upload_frame.open();   
							return;   
						}   
						upload_frame = wp.media({   
							title: 'Insert image',   
							button: {   
								text: 'Insert',   
							},   
							multiple: false   
						});   
						upload_frame.on('select',function(){   
							attachment = upload_frame.state().get('selection').first().toJSON();
							t.next().val(attachment.url);
							t.prev().attr('src',attachment.url);
							t.prev().show();
						});	   
						upload_frame.open();   
					});
	
				});
			  </script>";
?>
</form>
 <?php

}
?>