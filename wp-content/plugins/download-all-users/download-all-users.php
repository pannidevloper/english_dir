<?php

/**
 * Plugin Name: Download all users
 * Version:     1.0
 * Author:      Lix
 * Description: Download all users to excel
 */

register_activation_hook(__FILE__, 'download_all_users_activate');

function download_all_users_activate()
{
}

add_action('admin_menu', 'add_download_all_users_menu');
add_action('admin_init', 'add_download_all_users_menu_init');

function add_download_all_users_menu()
{
    add_options_page('Download all users', 'Download all users', 'manage_options', __FILE__, 'download_all_users_run');
}

function add_download_all_users_menu_init()
{
    wp_register_style('download-all-users', plugins_url('css/datatables.min.css', __FILE__));
    wp_enqueue_style('download-all-users');
    wp_register_script('download-all-users', plugins_url('js/datatables.min.js', __FILE__));
    wp_enqueue_script('download-all-users');
}

function download_all_users_run()
{
    global $wpdb;
    $users=get_users();
    $roles=wp_roles()->roles;
?>
    <table id="AllUsers" style="text-align:left;">
        <thead>
            <tr>
                <th>Username</th>
                <th>Name</th>
                <th>Email</th>
                <th>Connections</th>
                <th>Followers</th>
                <th>Role</th>
                <th>Status</th>
                <th>Registered date</th>
                <th>Last login</th>
                <th>profile completenes</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($users as $u) {
                $profile_completeness = get_user_meta($u->ID, 'profile_completeness',true);
                $profile_completeness = empty($profile_completeness)?0:ceil($profile_completeness*100);
                $last_login=$wpdb->get_var("SELECT login_date FROM wp_user_last_login WHERE user_login='{$u->user_login}'");
            // added connection count on 21/6/2020
            $connection_count = $wpdb->get_var(
            "SELECT COUNT(*) FROM friends_requests WHERE ( user2 = '{$u->ID}' OR user1 = '{$u->ID}') AND status=1"
        );
            if( $roles[$u->roles[0]]['name'] =='Travel advisor'){
            $follower_count = $wpdb->get_var("SELECT COUNT(*) FROM `social_follow` WHERE `sf_agent_id` = '{$u->ID}'");
        }
        else{
            $follower_count ='-';
        }
            ?>
            <tr>
                <td><?php echo $u->user_login; ?></td>
                <td><?php echo $u->display_name; ?></td>
                <td><?php echo $u->user_email; ?></td>
                <td><?php echo $connection_count; ?></td>
                 <td><?php echo $follower_count; ?></td>
                <td><?php echo $roles[$u->roles[0]]['name']; ?></td>
                <td><?php echo $u->user_status==0?'Approved':'Unapproved'; ?></td>
                <td><?php echo $u->user_registered; ?></td>
                <td><?php echo empty($last_login)?'':$last_login; ?></td>
                <td><?php echo $profile_completeness ?>%</td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <script>
        jQuery(document).ready(function() {
            jQuery('#AllUsers').DataTable({
                dom: 'Bfrtip',
                pageLength: 15,
                "search": {
                    "caseInsenstive": false
                },
                "buttons": [{
                    'extend': 'excelHtml5',
                    'text': 'Download',
                    'exportOptions': {
                        'modifier': {}
                    }
                }]
            });
        });
    </script>
<?php
}
