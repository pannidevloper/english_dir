<?php

/**
 * Plugin Name: Currency Setting
 * Version:     1.0
 * Author:      Lix
 * Description: Currency Setting
 */


register_activation_hook(__FILE__, 'currency_setting_activate');

function currency_setting_activate()
{ }

add_action('admin_menu', 'add_currency_setting_admin_menu');
add_action('admin_init', 'add_currency_setting_admin_menu_init');

function add_currency_setting_admin_menu()
{
    add_options_page('Currency Setting', 'Currency Setting', 'manage_travcust_data', __FILE__, 'show_currency_setting');
}

function add_currency_setting_admin_menu_init()
{ }

function get_currency($currencyName)
{
    $currencyList = get_option('currency_list');
    if (!empty($currencyList[$currencyName])) {
        return $currencyList[$currencyName];
    }
    return false;
}

function show_currency_setting()
{
    ?>
    <h2>Currency Setting</h2>
    <form action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST">
        <?php
            if ($_POST['save']) {
                $i = 1;
                $currencyList = array();
                while ($i <= (int) $_POST['CurrencyListCount']) {
                    if (!empty($_POST["name{$i}"])) {
                        $currencyList[$_POST["name{$i}"]] = array(
                            'symbol' => htmlspecialchars(trim($_POST["symbol{$i}"])),
                            'rate' => htmlspecialchars(trim($_POST["rate{$i}"]))
                        );
                    }
                    $i++;
                }
                if (!empty($currencyList)) {
                    update_option('currency_list', $currencyList);
                }
            }

            $currencyList = get_option('currency_list');

            echo '<table class="currencyList">
				<thead>
					<tr><th>Currency</th><th>Symbol</th><th>Exchange rate</th></tr>
				</thead>
                <tbody>';
            $i = 1;
            foreach ($currencyList as $name => $currency) {
                echo '<tr>
					<td><input name="name' . $i . '" type="text" value="' . $name . '" /></td>
					<td><input name="symbol' . $i . '" type="text" value="' . $currency['symbol'] . '" /></td>
					<td><input name="rate' . $i . '" type="text" value="' . $currency['rate'] . '" /></td>
                  </tr>';
                $i++;
            }
            echo '</tbody>
			  </table>
			  <input id="CurrencyListCount" name="CurrencyListCount" type="hidden" value="' . count($currencyList) . '"/>
			  <input type="button" class="button" value="Add Currency" id="AddCurrency" />
			  <br/><br/>
			  <input type="submit" name="save" id="save" class="button button-primary button-large" value="Save">
			  <br/><br/>';
            echo "<script>
				jQuery(document).ready(function(){
					var CurrencyListCount;
					jQuery('#AddCurrency').click(function (){
						CurrencyListCount=parseInt(jQuery('#CurrencyListCount').val())+1;
						jQuery('#CurrencyListCount').val(CurrencyListCount);
						jQuery('table.currencyList>tbody').append('<tr> <td><input name=\"name'+CurrencyListCount+'\" type=\"text\" value=\"\" /></td> <td><input name=\"symbol'+CurrencyListCount+'\" type=\"text\" value=\"\" /></td> <td><input name=\"rate'+CurrencyListCount+'\" type=\"text\" value=\"\" /></td> </tr>');
					});
				});
			  </script>";
            ?>
    </form>
<?php
}
