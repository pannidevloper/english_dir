<?php
/*
Plugin Name: Friends Suggestions algo
Description: suggestion friends based on data
Author: M Farhan
Version: 1.0
*/

//wp_enqueue_style( 'hook_custom_wp_admin_css', plugins_url('css/notifactionsstying.css', __FILE__) );
include_once(ABSPATH . 'wp-includes/pluggable.php');
add_action('admin_menu', 'suggestions__admin_menu');
include_once dirname(__FILE__).'/functions.php';

function suggestions__admin_menu(){

add_menu_page('Friends suggestions', 'Friends suggestions', 'manage_options', 'Friends suggestions');

add_submenu_page( 'Friends suggestions', 'Friends suggestions', 'Friends suggestions',
    'manage_options', 'Friends suggestions','Friends suggestions');
;
}


if ( isset( $_GET['page'] ) && $_GET['page'] == 'Friends suggestions' ) {

///to_check_communcation (261);
//front_end_suggestions (261);
  //check_user_profile_interactions_for_another_user (892);
  
}




/*********************************************************************/


  /************************************************************************/


function cron_job_for_mutual_friends_check(){

global $wpdb;

$get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");

    foreach ($get_users_list as $user1) {

    $get_users_list_2 = $wpdb->get_results("SELECT ID FROM `wp_users`");

      foreach ($get_users_list_2 as $user2) {
        
         mutual_friends_check($user1->ID,$user2->ID);
      }

    }
  }

  add_action( 'cron_job_for_mutual_friends_check', 'cron_job_for_mutual_friends_check');




  /*****************************************************************************/



  function cron_job_for_single_status_check(){

global $wpdb;

$get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");


    foreach ($get_users_list as $user1) {

      // single status
    $marital_status = get_user_meta( $user1->ID, 'marital_status' , true );

     if($marital_status=='single'){
      $gender = get_user_meta( $current_user_id, 'gender' , true );
        $suggestions_gender = ( $gender=='Male') ? "Female": "Male";
         get_user_data_for_suggestions ('gender',$gender,$user1->ID,7);
     }
    }
  }

  add_action( 'cron_job_for_single_status_check', 'cron_job_for_single_status_check' );



  /****************************************************************************/



  function cron_job_for_same_school_check(){

global $wpdb;

$get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");


    foreach ($get_users_list as $user1) {

      check_school ($user1->ID);
   
    }
  }

  add_action( 'cron_job_for_same_school_check', 'cron_job_for_same_school_check' );


  /****************************************************************************/


  function cron_job_for_same_company_check(){

global $wpdb;

$get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");


    foreach ($get_users_list as $user1) {

      check_company($user1->ID);
   
    }
  }

  add_action( 'cron_job_for_same_company_check', 'cron_job_for_same_company_check' );


  /*******************************************************************************/


  function  cron_job_for_user_contacts_score(){

  global $wpdb;

 $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");


    foreach ($get_users_list as $user1) {

       user_contacts_suggestions( $user1->ID );
   
    }
   
  }

   add_action( 'cron_job_for_user_contacts_score', 'cron_job_for_user_contacts_score' );





  /*********************************************************************/

  function  cron_job_for_user_search_keyword_score(){

  global $wpdb;

 $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");


    foreach ($get_users_list as $user1) {

       user_searchs_keyword( $user1->ID );
   
    }
   
  }

   add_action( 'cron_job_for_user_search_keyword_score', 'cron_job_for_user_search_keyword_score' );


  /**************************************************************************/


  function  cron_job_for_total_suggestion_score(){

  global $wpdb;


  $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");
  foreach ($get_users_list as $user1) {

    
   $get_users_list2 = $wpdb->get_results("SELECT suggestion_id FROM `user_suggestions_cron1` WHERE wp_user_id={$user1->ID}");

  foreach ($get_users_list2 as $user2) {
 calculate_suggestion_Score ( $user1->ID,$user2->suggestion_id);

    
 }
}
   
  }

   add_action( 'cron_job_for_total_suggestion_score', 'cron_job_for_total_suggestion_score' );




  /***************************************************************************/

  function  cron_job_for_communcation_Score(){

  global $wpdb;


  $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");
  foreach ($get_users_list as $user1) {

    to_check_communcation($user1->ID);
}
   
  }

   add_action( 'cron_job_for_communcation_Score', 'cron_job_for_communcation_Score' );


  /************************************************************************/



  function  cron_job_for_profile_view_score(){

  global $wpdb;


  $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");
  foreach ($get_users_list as $user1) {

    check_user_profile_interactions_for_another_user($user1->ID);
}
   
  }

   add_action( 'cron_job_for_profile_view_score', 'cron_job_for_profile_view_score' );


  /*******************************************************************/

  function  cron_job_for_photo_video_view_score(){

  global $wpdb;


  $get_users_list = $wpdb->get_results("SELECT ID FROM `wp_users`");
  foreach ($get_users_list as $user1) {

    check_user_photo_video_interactions_for_another_user($user1->ID);
}
   
  }

   add_action( 'cron_job_for_photo_video_view_score', 'cron_job_for_photo_video_view_score' );


  /***********************************************************************/


register_activation_hook( __FILE__, 'travpart_score_activation' );
 
function travpart_score_activation() {
   
   wp_schedule_event(strtotime( 'next Friday' ), 'weekly', 'cron_job_for_same_school_check' );
   wp_schedule_event( strtotime( 'next Friday' ), 'weekly', 'cron_job_for_same_company_check' );
   wp_schedule_event( strtotime( 'next Friday' ), 'weekly', 'cron_job_for_user_contacts_score' );
   wp_schedule_event( strtotime( 'next Friday' ), 'weekly', 'cron_job_for_user_search_keyword_score' );
   wp_schedule_event( strtotime( 'next Saturday' ), 'weekly', 'cron_job_for_mutual_friends_check' );
   wp_schedule_event( strtotime( 'next Saturday' ), 'weekly', 'cron_job_for_communcation_Score' );
   wp_schedule_event( strtotime( 'next Sunday' ), 'weekly', 'cron_job_for_single_status_check' );
   wp_schedule_event( strtotime( 'next Sunday' ), 'weekly', 'cron_job_for_profile_view_score' );
   wp_schedule_event( strtotime( 'next Sunday' ), 'weekly', 'cron_job_for_photo_video_view_score' );
   wp_schedule_event( strtotime( 'next Monday' ), 'weekly', 'cron_job_for_total_suggestion_score' );
}

register_deactivation_hook( __FILE__, 'travpart_score_my_deactivation' );
 
function travpart_score_my_deactivation() {
  
    wp_clear_scheduled_hook( 'cron_job_for_mutual_friends_check' );
  //  wp_clear_scheduled_hook( 'cron_job_for_edu_level_check' );
    wp_clear_scheduled_hook( 'cron_job_for_same_company_check' );
    wp_clear_scheduled_hook( 'cron_job_for_same_school_check' );
    wp_clear_scheduled_hook( 'cron_job_for_single_status_check' );
    wp_clear_scheduled_hook( 'cron_job_for_user_contacts_score' );
    wp_clear_scheduled_hook( 'cron_job_for_user_search_keyword_score' );
    wp_clear_scheduled_hook( 'cron_job_for_total_suggestion_score' );
    wp_clear_scheduled_hook( 'cron_job_for_communcation_Score' );
    wp_clear_scheduled_hook( 'cron_job_for_profile_view_score' );
    wp_clear_scheduled_hook( 'cron_job_for_photo_video_view_score' );
}



  /**************************************************************************/


  // cron job for cron_job_for_mutual_friends_score_check


function front_end_suggestions ($current_user_id){

global $wpdb;
 $get_suggestions = $wpdb->get_results("SELECT suggestion_id AS ID FROM `friends_suggestions` WHERE wp_user_id={$current_user_id} ORDER BY total_score DESC LIMIT 15");
return $get_suggestions;
   
}




 


 




 


 
