<?php
function get_user_data_for_suggestions($meta_key,$meta_value,$current_user_id,$score){
 
    global $wpdb;
     $query1 = "SELECT a.ID, a.user_login 
     FROM wp_users a
     JOIN wp_usermeta b ON a.ID = b.user_id 
     WHERE b.meta_key = '{$meta_key}' and b.meta_value like '%$meta_value%'
     ORDER BY a.user_nicename";

     $users = $wpdb->get_results( $query1);

     foreach ($users as $user) {

      if($user->ID==$current_user_id){
        continue;
      }

    $_already_exist = $wpdb->get_row("SELECT * FROM user_suggestions_cron1 WHERE wp_user_id='{$current_user_id}' AND suggestion_id='{$user->ID}' AND keyy='{$meta_key}'");

    if($_already_exist){

    $wpdb->update(
    'user_suggestions_cron1', array(
    'score' => $score,

    ), array('id' => $_already_exist->id), array(
    '%s',
    ), array('%d')
    );

    }
    else{
    
     $wpdb->insert(
      'user_suggestions_cron1',
      array(
      'wp_user_id' => $current_user_id,
      'suggestion_id' => $user->ID,
      'keyy'=>$meta_key,
      'score'=>$score
      ),
      array(
      '%d',
      '%d',
      '%s',
      '%s'
      )
      );
   }
     
}
}

// same company check
function check_company($current_user_id){

  global $wpdb;
 $get_results =  $wpdb->get_results("SELECT company FROM `wp_career` WHERE userid=" . $current_user_id);
   foreach ($get_results as $company) {
     
     $current_user_companies[] =$company->company;

   }
   // make  string from array
   $current_user_companies = join("','",$current_user_companies); 
  $query ="SELECT userid AS ID FROM `wp_career` WHERE company IN ('$current_user_companies')" ;
  $get_same_company_users =  $wpdb->get_results($query);

  foreach ($get_same_company_users as $user) {
     if($user->ID==$current_user_id){
        continue;
      }

     $_already_exist = $wpdb->get_row("SELECT * FROM user_suggestions_cron1 WHERE wp_user_id='{$current_user_id}' AND suggestion_id='{$user->ID}' AND keyy='company'");
    if($_already_exist){

    $wpdb->update(
    'user_suggestions_cron1', array(
    'score' => 2,

    ), array('id' => $_already_exist->id), array(
    '%s',
    ), array('%d')
    );

    }
    else{
    
     $wpdb->insert(
      'user_suggestions_cron1',
      array(
      'wp_user_id' => $current_user_id,
      'suggestion_id' => $user->ID,
      'keyy'=>'company',
      'score'=>2
      ),
      array(
      '%d',
      '%d',
      '%s',
      '%s'
      )
      );
   }
    
  }
  

}

function check_school($current_user_id){

  global $wpdb;
 $get_results =  $wpdb->get_results("SELECT school FROM `wp_education` WHERE userid=" . $current_user_id);
   foreach ($get_results as $school) {
     
     $current_user_school[] =$school->school;

   }
   if( count($current_user_school) >0 ){
   // make  string from array
   $current_user_school = join("','",$current_user_school); 
  $query ="SELECT userid AS ID FROM `wp_education` WHERE school IN ('$current_user_school')" ;
  $get_same_school_users =  $wpdb->get_results($query);

  foreach ($get_same_school_users as $user) {

    if($user->ID==$current_user_id){
        continue;
      }
     $_already_exist = $wpdb->get_row("SELECT * FROM user_suggestions_cron1 WHERE wp_user_id='{$current_user_id}' AND suggestion_id='{$user->ID}' AND keyy='school'");
    if($_already_exist){

    $wpdb->update(
    'user_suggestions_cron1', array(
    'score' => 8.0,

    ), array('id' => $_already_exist->id), array(
    '%s',
    ), array('%d')
    );

    }
    else{
    
     $wpdb->insert(
      'user_suggestions_cron1',
      array(
      'wp_user_id' => $current_user_id,
      'suggestion_id' => $user->ID,
      'keyy'=>'school',
      'score'=>8.0
      ),
      array(
      '%d',
      '%d',
      '%s',
      '%s'
      )
      );
   }
    
  }
}
  

}

function mutual_friends_check($current_user_id,$user_id){

global $wpdb;

      
  // friends of 1st
 $connections = $wpdb->get_results(
"SELECT * FROM friends_requests WHERE  (user2 = '{$current_user_id}' OR  user1 = '{$current_user_id}') AND status=1" );
 foreach ($connections as $p1) {

  if ($p1->user1 == $current_user_id){
  $friends_list1[] = $p1->user2;
}
else{
  $friends_list1[] = $p1->user1;
}
}

// friends of 2nd user
$connections2 = $wpdb->get_results(
"SELECT * FROM friends_requests WHERE  (user2 = '{$user_id}' OR  user1 = '{$user_id}') AND status=1" );
 foreach ($connections2 as $p2) {

  if ($p2->user1 == $user_id){
  $friends_list2[] = $p2->user2;
}
else{
  $friends_list2[] = $p2->user1;
}
}
$match = array_intersect($friends_list1, $friends_list2);

if(count($match)>0){

   $_already_exist = $wpdb->get_row("SELECT * FROM user_suggestions_cron1 WHERE wp_user_id='{$current_user_id}' AND suggestion_id='{$user_id}' AND keyy='mutual_count'");
    if($_already_exist){

    $wpdb->update(
    'user_suggestions_cron1', array(
    'score' => 7.0,

    ), array('id' => $_already_exist->id), array(
    '%s',
    ), array('%d')
    );

    }
    else{
    
     $wpdb->insert(
      'user_suggestions_cron1',
      array(
      'wp_user_id' => $current_user_id,
      'suggestion_id' => $user_id,
      'keyy'=>'mutual_count',
      'score'=>7.0
      ),
      array(
      '%d',
      '%d',
      '%s',
      '%s'
      )
      );
   }

}
}


function profile_completness_check($current_user_id,$user_id){

global $wpdb;


$score = get_user_meta( $user_id, 'profile_completeness' , true );

 $score = 2* $score;
if( $score>1){
 
   $_already_exist = $wpdb->get_row("SELECT * FROM user_suggestions_cron1 WHERE wp_user_id='{$current_user_id}' AND suggestion_id='{$user_id}' AND keyy='profile_completeness'");
    if($_already_exist){

    $wpdb->update(
    'user_suggestions_cron1', array(
    'score' => $score,

    ), array('id' => $_already_exist->id), array(
    '%s',
    ), array('%d')
    );

    }
    else{
    
     $wpdb->insert(
      'user_suggestions_cron1',
      array(
      'wp_user_id' => $current_user_id,
      'suggestion_id' => $user_id,
      'keyy'=>'profile_completeness',
      'score'=>$score
      ),
      array(
      '%d',
      '%d',
      '%s',
      '%s'
      )
      );
   }
}
}


function user_contacts_suggestions($current_user_id){

  global $wpdb;
  $phone_contact_users = $wpdb->get_col("SELECT wp_users.ID FROM `user`,`wp_users` WHERE wp_users.user_login=user.username
     AND phone IN ( SELECT contact FROM `user_contacts` WHERE wp_user_id = {$current_user_id} )");


  foreach ($phone_contact_users as $value) {

    $user_id = $value;
    $score=9;
    $_already_exist = $wpdb->get_row("SELECT * FROM user_suggestions_cron1 WHERE wp_user_id='{$current_user_id}' AND suggestion_id='{$user_id}' AND keyy='contact'");
    if($_already_exist){

    $wpdb->update(
    'user_suggestions_cron1', array(
    'score' => $score,

    ), array('id' => $_already_exist->id), array(
    '%s',
    ), array('%d')
    );

    }
    else{
    
     $wpdb->insert(
      'user_suggestions_cron1',
      array(
      'wp_user_id' => $current_user_id,
      'suggestion_id' => $user_id,
      'keyy'=>'contact',
      'score'=>$score
      ),
      array(
      '%d',
      '%d',
      '%s',
      '%s'
      )
      );
   }
}
}


function user_searchs_keyword($current_user_id){

global $wpdb;
  $query="SELECT kw FROM user_friends_search_keywords WHERE user_id = {$current_user_id}";
    $searchs = $wpdb->get_results($query);
    $search_ids = array();

        foreach($searchs as $row){

        $user = get_user_by('login',$row->kw);

        if( $user) {
        if($user->ID!=$user_id){
        $search_ids[] = $user->ID;
        }
      }
    }

    foreach ($search_ids as $user) {

      $user_id =  $user;
      $score =3;
      $_already_exist = $wpdb->get_row("SELECT * FROM user_suggestions_cron1 WHERE wp_user_id='{$current_user_id}' AND suggestion_id='{$user_id}' AND keyy='search_kw'");
    if($_already_exist){


    $wpdb->update(
    'user_suggestions_cron1', array(
    'score' => $score,

    ), array('id' => $_already_exist->id), array(
    '%s',
    ), array('%d')
    );

    }
    else{
    
     $wpdb->insert(
      'user_suggestions_cron1',
      array(
      'wp_user_id' => $current_user_id,
      'suggestion_id' => $user_id,
      'keyy'=>'search_kw',
      'score'=>$score
      ),
      array(
      '%d',
      '%d',
      '%s',
      '%s'
      )
      );
   }
      
    }

}


function calculate_suggestion_Score($current_user_id,$user_id){

global $wpdb;

 $query="SELECT SUM(score) FROM user_suggestions_cron1 WHERE wp_user_id = {$current_user_id} AND suggestion_id={$user_id}";
    $total_Score = $wpdb->get_var($query);
  
  if( $total_Score>0){
   $_already_exist = $wpdb->get_row("SELECT * FROM friends_suggestions WHERE wp_user_id='{$current_user_id}' AND suggestion_id='{$user_id}'");

   if($_already_exist){

    $wpdb->update(
    'friends_suggestions', array(
    'total_score' => $total_Score,

    ), array('id' => $_already_exist->id), array(
    '%s',
    ), array('%d')
    );

    

   } else{

    $wpdb->insert(
      'friends_suggestions',
      array(
      'wp_user_id' => $current_user_id,
      'suggestion_id' => $user_id,
      'total_score'=>$total_Score
      ),
      array(
      '%d',
      '%d',
      '%s'
      )
      );

   }
}

}

function to_check_communcation($current_user_id){

global $wpdb;

$username = $wpdb->get_var("SELECT user_login FROM `wp_users` WHERE ID ='{$current_user_id}'");
if($username){
$user_id = $wpdb->get_var("SELECT userid FROM `user` WHERE username ='{$username}'");

$get_user_ids =  $wpdb->get_results("SELECT c_to,c_from FROM `chat` WHERE `c_from`='{$user_id}' OR `c_to`='{$user_id}'");

foreach ($get_user_ids as $single) {

  if($single->c_to==$user_id){

  $ids[] =$single->c_from;
  }
  else{

  $ids[] = $single->c_to;
  }
}
$unique_array =array_unique($ids);

foreach ($unique_array as $value) {

  $wp_user_name = $wpdb->get_var("SELECT username FROM `user` WHERE   userid='{$value}'");

  if(!empty($wp_user_name)){
  $wp_user_ID = $wpdb->get_var("SELECT wp_users.ID FROM `wp_users`,`user` WHERE wp_users.user_login=user.username AND username='{$wp_user_name}'");
  if($wp_user_ID!=NULL){
    
    $user_id = $wp_user_ID;
    $score=7;
     $_already_exist = $wpdb->get_row("SELECT * FROM user_suggestions_cron1 WHERE wp_user_id='{$current_user_id}' AND suggestion_id='{$user_id}' AND keyy='communcation'");
    if($_already_exist){


    $wpdb->update(
    'user_suggestions_cron1', array(
    'score' => $score,

    ), array('id' => $_already_exist->id), array(
    '%s',
    ), array('%d')
    );

    }
    else{
    
     $wpdb->insert(
      'user_suggestions_cron1',
      array(
      'wp_user_id' => $current_user_id,
      'suggestion_id' => $user_id,
      'keyy'=>'communcation',
      'score'=>$score
      ),
      array(
      '%d',
      '%d',
      '%s',
      '%s'
      )
      );
   }
      
  }
  
}
}
}

}

function visitor_profile_logs($page,$owner_id,$by_id) {
  global $wpdb;
  $check = $wpdb->get_row("SELECT id,count  FROM `visitor_profile_logs` WHERE page='{$page}' AND owner_id='{$owner_id}' AND  by_id='{$by_id}'");
    if($check->id) {

     $wpdb->update(
    'visitor_profile_logs', array(
    'count' => $check->count+1,

    ), array('id' => $check->id), array(
    '%d',
    ), array('%d')
    );

  } else {
      $wpdb->insert(
      'visitor_profile_logs',
      array(
      'page' => $page,
      'owner_id' => $owner_id,
      'by_id'=>$by_id,
      'count'=>1
      ),
      array(
      '%s',
      '%d',
      '%d',
      '%d'
      )
      );

  }

}

function check_user_profile_interactions_for_another_user($current_user_id){

  global $wpdb;

  $results = $wpdb->get_results("SELECT owner_id,count  FROM `visitor_profile_logs` WHERE page='timeline' AND  by_id='{$current_user_id}'");

  foreach ($results as $value) {
    
    $user_id = $value->owner_id;
     $_already_exist = $wpdb->get_row("SELECT * FROM user_suggestions_cron1 WHERE wp_user_id='{$current_user_id}' AND suggestion_id='{$user_id}' AND keyy='profile_view'");

     $score=3;

    if($_already_exist){

    $wpdb->update(
    'user_suggestions_cron1', array(
    'score' => $score,

    ), array('id' => $_already_exist->id), array(
    '%s',
    ), array('%d')
    );

    }
    else{
    
    if($current_user_id!=$user_id){

     $wpdb->insert(
      'user_suggestions_cron1',
      array(
      'wp_user_id' => $current_user_id,
      'suggestion_id' => $user_id,
      'keyy'=>'profile_view',
      'score'=>$score
      ),
      array(
      '%d',
      '%d',
      '%s',
      '%s'
      )
      );
   }
   }

    
  }


}


function check_user_photo_video_interactions_for_another_user($current_user_id){

  global $wpdb;

  $results = $wpdb->get_results("SELECT owner_id,count  FROM `visitor_profile_logs` WHERE page='timeline' AND  by_id='{$current_user_id}'");

  foreach ($results as $value) {
    
    $user_id = $value->owner_id;
     $_already_exist = $wpdb->get_row("SELECT * FROM user_suggestions_cron1 WHERE wp_user_id='{$current_user_id}' AND suggestion_id='{$user_id}' AND keyy='my_photo_videos'");

     $score=6;

    if($_already_exist){

    $wpdb->update(
    'user_suggestions_cron1', array(
    'score' => $score,

    ), array('id' => $_already_exist->id), array(
    '%s',
    ), array('%d')
    );

    }
    else{
    
    if($current_user_id!=$user_id){

     $wpdb->insert(
      'user_suggestions_cron1',
      array(
      'wp_user_id' => $current_user_id,
      'suggestion_id' => $user_id,
      'keyy'=>'my_photo_videos',
      'score'=>$score
      ),
      array(
      '%d',
      '%d',
      '%s',
      '%s'
      )
      );
   }
   }

    
  }


}


