<?php
/**
 * Plugin Name: Ban/Suspend user
 * Version:     1.0
 * Author:      Lix
 * Text Domain: https://www.travpart.com
 * Description: Ban/Suspend user
*/

register_activation_hook(__FILE__, 'ban_suspend_account_activate');

function ban_suspend_account_activate()
{
}

//add ban/suspend button at users page
add_filter('user_row_actions', function ($actions, $user) {
    $capability	= (is_multisite())?'manage_site':'manage_options';
    $ban_suspend = get_user_meta($user->ID, 'ban_suspend', true);
    if (current_user_can($capability)) {
        if ($ban_suspend == 1) {
            $actions['unban_user']	= '<a href="'.wp_nonce_url("users.php?action=unban_user&users=$user->ID", 'bulk-users').'">Unban</a>';
        } else {
            $actions['ban_user']	= '<a href="'.wp_nonce_url("users.php?action=ban_user&users=$user->ID", 'bulk-users').'">Ban</a>';
        }
        if ($ban_suspend == 2) {
            $actions['unsuspend_user']	= '<a href="'.wp_nonce_url("users.php?action=unsuspend_user&users=$user->ID", 'bulk-users').'">Unsuspend</a>';
        } else {
            $actions['suspend_user']	= '<a href="'.wp_nonce_url("users.php?action=suspend_user&users=$user->ID", 'bulk-users').'">Suspend</a>';
        }
    }
    return $actions;
}, 10, 2);

//ban/suspend a user
add_filter('handle_bulk_actions-users', function ($sendback, $action, $user_ids) {
    global $wpdb;
    if ($action == 'ban_user') {
        update_user_meta($user_ids, 'ban_suspend', '1');
        $token=md5($user_ids.time());
        $wpdb->query("UPDATE `user` SET token='{$token}' WHERE user.username IN (SELECT user_login FROM `wp_users` WHERE wp_users.ID={$user_ids})");
    } elseif ($action == 'suspend_user') {
        update_user_meta($user_ids, 'ban_suspend', '2');
        $token=md5($user_ids.time());
        $wpdb->query("UPDATE `user` SET token='{$token}' WHERE user.username IN (SELECT user_login FROM `wp_users` WHERE wp_users.ID={$user_ids})");
    } elseif ($action == 'unban_user' || $action == 'unsuspend_user') {
        update_user_meta($user_ids, 'ban_suspend', '0');
    }
    return $sendback;
}, 10, 3);


//Add Ban/Suspend column to users list table
function custom_add_ban_suspend_column($columns)
{
    $columns['ban_suspend'] = 'Ban/Suspend';
    return $columns;
}
add_filter('manage_users_columns', 'custom_add_ban_suspend_column');

//Add content to Ban/Suspend column
function custom_show_ban_suspend_column_content($value, $column_name, $user_id)
{
    $ban_suspend = get_user_meta($user_id, 'ban_suspend', true);
    if ('ban_suspend' == $column_name) {
        if ($ban_suspend == '1') {
            return 'BAN';
        } elseif ($ban_suspend == '2') {
            return 'SUSPEND';
        }
    }
    return $value;
}
add_filter('manage_users_custom_column', 'custom_show_ban_suspend_column_content', 10, 3);

//Add ban/suspend user list filter option
function custom_ban_suspend_views($views)
{
    global $wpdb;
    $capability	= (is_multisite())?'manage_site':'manage_options';
    if (current_user_can($capability)) {
        $count = $wpdb->get_var("SELECT COUNT(*) FROM wp_usermeta WHERE meta_key='ban_suspend' AND (meta_value=1 OR meta_value=2)");
        $views['ban_suspend'] = '<a href="' . esc_url(add_query_arg('ban_suspend', '1', 'users.php')) . '"' . (isset($_GET['ban_suspend']) && $_GET['ban_suspend'] == '1' ? ' class="current" aria-current="page"' : '') . '> Ban/Suspend <span class="count">(' . number_format($count) . ')</span></a>';
    }
    return $views;
}
add_filter('views_users', 'custom_ban_suspend_views');

//Add ban/suspend user list filter
function custom_ban_suspend_filter($args)
{
    global $wpdb;
    if (isset($_GET['ban_suspend']) && $_GET['ban_suspend']==1) {
        $args['include']=$wpdb->get_col("SELECT DISTINCT `user_id` FROM wp_usermeta WHERE meta_key='ban_suspend' AND (meta_value=1 OR meta_value=2)");
    }
    return $args;
}
add_filter('users_list_table_query_args', 'custom_ban_suspend_filter');

//Check account status when login
function check_account_ban_suspend($user, $username, $password)
{
    if ($user instanceof WP_User) {
        $ban_suspend = get_user_meta($user->ID, 'ban_suspend', true);
        if ($ban_suspend == 1) {
            $user = new WP_Error('account_is_banned', __("This account is no longger active"));
        } elseif ($ban_suspend == 2) {
            $user = new WP_Error('account_is_suspended', __("Your account is currently restricted by our Trust & Safety team."));
        }
    }
    return $user;
}
add_action('authenticate', 'check_account_ban_suspend', 1000, 3);

//Logout account if user is banned/suspended
function logout_account_ban_suspend($query)
{
    if (is_user_logged_in()) {
        $ban_suspend = get_user_meta(wp_get_current_user()->ID, 'ban_suspend', true);
        if ($ban_suspend == 1 || $ban_suspend == 2) {
            wp_logout();
        }
    }
}
add_action('pre_get_posts', 'logout_account_ban_suspend');
