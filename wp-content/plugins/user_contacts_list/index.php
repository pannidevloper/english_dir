<?php
/**
 * Plugin Name: Show user contacts
 * Version:     1.0
 * Author:      Farhan
 * Description: This Plugin show the user mobile contact list,retrived with api
 */



 // Add admin sitting

add_action( 'admin_menu', 'my_admin_menu_for_showing_contacts' );

function my_admin_menu_for_showing_contacts() {

add_menu_page( 'User contacts', 'User contacts', 'manage_options', 'contactlist.php', 'show_user_list', 'dashicons-tickets', 6  );

}

	// show data
	function show_user_list(){

	$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
	$wp_user_id = filter_input(INPUT_GET, 'user_id', FILTER_SANITIZE_STRING);
	switch ($action) {

	case 'show_single_user_contact_list':
	show_single_user_contact_list($wp_user_id);
	break;
	default:
	show_contact_list();
	break;
	}

	}


if ( isset( $_GET['page'] ) && $_GET['page'] == 'contactlist.php' ) {

wp_enqueue_style( 'custom_wp_admin_css', plugins_url('contactlist.css', __FILE__) );

}

  add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

	function show_contact_list() {
	global $wpdb;
	?>

	<div class="row">
	<div class="col-sm-6">
	<h2 style="color:green">
	Users list
	</h2>
	<?php
	$records = $wpdb->get_results( 
	"SELECT DISTINCT wp_user_id FROM  user_contacts ORDER BY id DESC"
	);
	if(!empty($records)):
	?> 
	<table id="customers">
	<tr>
	<th>User Name</th>
	<th>No of Contacts</th>
	</tr>

	<?php

	foreach ( $records as $result ) {

	$total_query = "SELECT COUNT(*) FROM user_contacts AS total_count WHERE wp_user_id='{$result->wp_user_id}'";
	$total_contacts = $wpdb->get_var( $total_query );

	//echo "<tr><td>";
	//echo '<a href="">'.$result->wp_user_id.'</a>';
	//echo "</td>";
	echo "<td>";
	echo um_get_display_name($result->wp_user_id);
	echo "</td>";
	echo "<td>";?>
	<a href="<?php echo $url . add_query_arg(array('action' => 'show_single_user_contact_list','user_id' =>$result->wp_user_id )); ?>" target="_blank"><?php echo $total_contacts; ?> </a>

	<?php echo "</td>";

	echo"</tr>";
	}
	else:
	echo "No data";
	endif;
	?>
	</tr>
	</table>

	</div>


	</div>
	<div>

	</div>



	<?php
	}

function show_single_user_contact_list($wp_user_id) {
global $wpdb;
?>

<div class="row">
<div class="col-sm-6">
<h2 style="color:green">
Contact list
</h2>
<?php
$records = $wpdb->get_results( 
"SELECT contact_name,contact  FROM  user_contacts WHERE wp_user_id='{$wp_user_id}'"
);
if(!empty($records)):
?> 
<table id="customers">
<tr>
<th>Name</th>
<th>Contact</th>
</tr>

<?php

foreach ( $records as $result ) {

	$phone = str_replace(' ','',$result->contact); 

		// some saved numbers also have - in that so remove
	$phone = str_replace('-','',$phone); 

	//$phone = substr($phone, -7);
/*
	$check_phone = $wpdb->get_row("SELECT wp_users.*,user.* FROM `wp_users`,`user` WHERE  wp_users.user_login=user.username AND phone LIKE '%{$phone}' ");

	if($check_phone){

		$status =" Registered";
	}
	else{
		$status =" Not Registered";
	}
	*/

echo "<tr><td>";
echo '<a href="">'.$result->contact_name.'</a>';
echo "</td>";
echo "<td>";
echo '<a href="">'.$result->contact.'</a>';
echo "</td>";
//echo "<td>";
//echo '<a href="">'.$status.'</a>';
//echo "</td>";
}
else:
echo "No data";
endif;
?>
</tr>
</table>

</div>


</div>
<div>

</div>



<?php
}
