<?php
/*
Plugin Name: Tours-Details-for-bali-website
Description: This plugin is for Tourfrombali website ,It returns Json of All Tours
Version: 1.0
Author: Farhan

*/


// Function to get Tours 

 function get_tours_details($request_data) {

    global $wpdb;

    $currency_usd=$wpdb->get_var("SELECT option_value FROM wp_options WHERE option_name='_cs_currency_USD'");
    $sql = "SELECT wp_tour.*,wp_tour_sale.number_of_people,wp_tour_sale.netprice,wp_tour_sale.username,wp_tourmeta.meta_value
    FROM wp_tour,.wp_tour_sale,.wp_tourmeta
    WHERE
    wp_tour.id=wp_tour_sale.tour_id
    AND wp_tourmeta.tour_id=wp_tour_sale.tour_id
    AND wp_tourmeta.meta_key='bookingdata'";

      $data = $wpdb->get_results($sql);

      foreach ($data as $t) {

      $post_status = $wpdb->get_row("SELECT post_status FROM wp_posts,wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';")->post_status;


      // Checking if Tour Status is Published 
     if ($post_status != 'publish')
      	   continue;

      // Getting Hotel Info
    $hotelinfo = $wpdb->get_row("SELECT img,ratingUrl,location,start_date FROM wp_hotel WHERE tour_id='{$t->id}' LIMIT 1");
      // Agent Info
    $agentrow = $wpdb->get_row("SELECT userid,uname FROM user WHERE username='{$t->username}'");

    $agent_id = $agentrow->userid;

    $agent_name = $agentrow->uname;

    if (empty($agent_name))

    $agent_name = $t->username;

    // get data

    $images = array();

    $bookingdata = unserialize($t->meta_value);

    //var_dump($bookingdata);

    $spotdates = empty($bookingdata['spotdate']) ? array() : explode(':,', rtrim($bookingdata['spotdate'], ':'));

    $days = count($spotdates);

    if ($days <= 0)
    $days = 1;

    $post_content=$wpdb->get_row("SELECT post_content FROM wp_postmeta, wp_posts WHERE wp_posts.ID=wp_postmeta.post_id AND `meta_key`='tour_id' AND `meta_value`='{$t->id}'")->post_content;
    if(preg_match('!<img.*?src="(.*?)"!', $post_content, $post_image_match))
    $images[]=$post_image_match[1];

    if(!empty($hotelinfo->img)) {
    $images[]=$hotelinfo->img;
    }

    $hotel_rating = '';
    if (empty($hotelinfo->ratingUrl))
    $hotel_rating = '★';
    else if(preg_match('!/([0-9\.]+)\-!', $hotelinfo->ratingUrl, $match_rating)) {
    for ($j = 0; $j < ceil($match_rating[1]); $j++)
    $hotel_rating .= '★';
    }
    else
    $hotel_rating = '★';

    foreach ($images as $img) {
    //echo $img;
    //echo"</br>";
     
    $data1['images']= $images;

    }

    $post_ID=$wpdb->get_row("SELECT  ID FROM wp_posts,wp_postmeta WHERE wp_posts.ID=wp_postmeta.post_id AND wp_postmeta.meta_key='tour_id' AND wp_postmeta.meta_value='{$t->id}';")->ID;

    // Return Data In Json Formate for website

    $data1['days'] = $days;
    $data1['rating'] = $hotel_rating;
    $data1['location'] = (empty($hotelinfo->location) ? 'Null' : $hotelinfo->location);
    $data1['cost'] = '$ '. ceil((float) $t->netprice * $currency_usd / $t->number_of_people);
    $data1['agent_id'] = $agent_id;
    $data1['agent_name'] = $agent_name;
    $data1['published_post_id'] = $post_ID;
    $data1['tour_id'] = $t->id;
    
    // Adding to request
    $request[] = $data1;
 }

   // Return data
    return $request; 

    }


  // REST API END POINT 
 add_action('rest_api_init', 'provide_tours_to_bali');

            function provide_tours_to_bali(){

                register_rest_route('v2/bali', '/get_tours/', array(
                    'methods' => 'GET',
                    'callback' => 'get_tours_details',
                ));
          }


 ///add_shortcode('tours','get_tours_details');