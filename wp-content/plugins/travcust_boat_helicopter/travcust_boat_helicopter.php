<?php
/**
 * Plugin Name: Travcust Boats and Helicopter
 * Version:     1.0
 * Author:      Lix
 * Description: Travcust Boats and Helicopter setting
 */

 
 
 register_activation_hook( __FILE__, 'travcust_boat_activate' );
 
 function travcust_boat_activate() {

}

 /***************************** show the subscribe in admin page  ******************************/
 
 add_action('admin_menu', 'add_travcust_boat_admin_menu');
 add_action('admin_init', 'add_travcust_boat_admin_menu_init');
 function add_travcust_boat_admin_menu(){
 
	add_options_page( 'Travcust Boat & Helicopter', 'Travcust Boat & Helicopter', 'manage_travcust_data', __FILE__, 'show_travcust_boat' );
	
 }
 
 function add_travcust_boat_admin_menu_init(){
	
	

 }
 function show_travcust_boat(){
 ?>
 <h2>Travcust Boats & Helicopter Setting </h2>
 <form action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST" >
 <?php
 if($_POST['save'])
 {
	$i=1;
	$TravcustBoat=array();
	while($i<=(int)$_POST['ea_length'])
	{
		if(!empty($_POST["ea{$i}_item"])){
			$subitem=array();
			if(!empty($_POST["ea{$i}_subname"]))
			{
				for($si=0; $si<count($_POST["ea{$i}_subname"]); $si++)
				{
					if(!empty($_POST["ea{$i}_subname"][$si])){
						array_push($subitem, array('name'=>stripslashes($_POST["ea{$i}_subname"][$si]),
								'price'=>(int)$_POST["ea{$i}_subprice"][$si],
								'img'=>$_POST["ea{$i}_subimg"][$si]
						));
					}
				}
			}
			array_push($TravcustBoat,array('item'=>stripslashes($_POST["ea{$i}_item"]),
						  'img'=>$_POST["ea{$i}_img"],
						  'price'=>(int)$_POST["ea{$i}_price"],
						  'area'=>stripslashes($_POST["ea{$i}_area"]),
						  'vendor'=>stripslashes($_POST["ea{$i}_vendor"]),
						  'phone'=>stripslashes($_POST["ea{$i}_phone"]),
						  'subitem'=>$subitem
					));
		}
		$i++;
	}
	if(!empty($TravcustBoat)) { update_option('_cs_travcust_boat', serialize($TravcustBoat)); }
}
	
	$TravcustBoat=unserialize(get_option('_cs_travcust_boat'));
		echo '<table class="extraactivity">
				<thead>
					<tr><th>Details</th><th>Pictures</th><th>Price(Rp)</th><th>Area</th><th>Vendor name</th><th>Phone number</th><th>Language</th></tr>
				</thead>
				<tbody>';
		for($i=1;$i<=count($TravcustBoat);$i++)
		{
			echo '<tr>
					<td><input name="ea'.$i.'_item" type="text" value="'.$TravcustBoat[$i-1]['item'].'" /></td>
					<td><img width=64 height=64 src="'.$TravcustBoat[$i-1]['img'].'" style="display:none;" /><input class="ea_img_upload" type="button" value="Upload img" /><input name="ea'.$i.'_img" type="hidden" value="'.$TravcustBoat[$i-1]['img'].'" /></td>
					<td><input name="ea'.$i.'_price" type="text" value="'.$TravcustBoat[$i-1]['price'].'" /></td>
					<td><input name="ea'.$i.'_area" type="text" value="'.$TravcustBoat[$i-1]['area'].'" /></td>
					<td><input name="ea'.$i.'_vendor" type="text" value="'.$TravcustBoat[$i-1]['vendor'].'" /></td>
					<td><input name="ea'.$i.'_phone" type="text" value="'.$TravcustBoat[$i-1]['phone'].'" /></td>
					<td>
					  <select>					  	
					    <option value="English">English</option>
					    <option value="chinese">Chinese</option>
					  </select>
					</td>
				  </tr>';
				if(!empty($TravcustBoat[$i-1]['subitem']))
				{
					echo '<tr><td></td><td>Details</td><td>Price(Rp)</td><td>Pictures</td></tr>';
				foreach($TravcustBoat[$i-1]['subitem'] as $sub)
				{
					echo '<tr> <td></td>
					<td><input name="ea'.$i.'_subname[]" type="text" value="'.$sub['name'].'" /></td>					
					<td><input name="ea'.$i.'_subprice[]" type="text" value="'.$sub['price'].'" /></td>
					<td>
						<img width=64 height=64 src="'.$sub['img'].'" style="display:none;" />
						<input class="ea_img_upload" type="button" value="Upload img" />
						<input name="ea'.$i.'_subimg[]" type="hidden" value="'.$sub['img'].'" />
					</td>
				  </tr>';
				}
				}
				else
					echo '<input type="hidden" value="0" />';
			echo '<tr> <td></td>
					<td><input type="hidden" value="'.$i.'" /><input type="button" class="addsubitem" value="Add sub item" /></td>
				  </tr>';
		}
		echo '	</tbody>
			  </table>
			  <input id="ea_length" name="ea_length" type="hidden" value="'.count($TravcustBoat).'"/>
			  <input type="button" class="button" value="Add Row" id="AddEARow" />
			  <br/><br/>
			  <input type="submit" name="save" id="save" class="button button-primary button-large" value="Save">
			  <br/><br/>';
		echo "<script>
				jQuery(document).ready(function(){
					var ea_length,subnum;
					jQuery('#AddEARow').click(function (){
						ea_length=parseInt(jQuery('#ea_length').val())+1;
						jQuery('#ea_length').val(ea_length);
						jQuery('table.extraactivity>tbody').append('<tr> <td><input name=\"ea'+ea_length+'_item\" type=\"text\" value=\"\" /></td> <td> <img width=64 height=64 src=\"\" style=\"display:none;\" /> <input class=\"ea_img_upload\" type=\"button\" value=\"Upload img\" /> <input name=\"ea'+ea_length+'_img\" type=\"hidden\" value=\"\" /> </td> <td><input name=\"ea'+ea_length+'_price\" type=\"text\" value=\"\" /></td> <td><input name=\"ea'+ea_length+'_area\" type=\"text\" value=\"\" /></td> <td><input name=\"ea'+ea_length+'_vendor\" type=\"text\" value=\"\" /></td> <td><input name=\"ea'+ea_length+'_phone\" type=\"text\" value=\"\" /></td> </tr> <input type=\"hidden\" value=\"0\" /> <tr> <td></td> <td><input type=\"hidden\" value=\"'+ea_length+'\" /><input type=\"button\" class=\"addsubitem\" value=\"Add sub item\" /></td></tr>');
					});
					
					jQuery(document).on('click', '.addsubitem', function(){
						subnum=parseInt(jQuery(this).prev().val());
						if(jQuery(this).parent().parent().prev().val().length>0 && jQuery(this).parent().parent().prev().val()==0)
							jQuery('<tr><td></td><td>Details</td><td>Price(Rp)</td><td>Pictures</td></tr>').insertBefore(jQuery(this).parent().parent());
						jQuery('<tr> <td></td> <td><input name=\"ea'+subnum+'_subname[]\" type=\"text\" /></td> <td><input name=\"ea'+subnum+'_subprice[]\" type=\"text\" /></td> <td> <img width=64 height=64 src=\"\" style=\"display:none;\" /> <input class=\"ea_img_upload\" type=\"button\" value=\"Upload img\" /> <input name=\"ea'+subnum+'_subimg[]\" type=\"hidden\" /> </td> </tr>').insertBefore(jQuery(this).parent().parent());
					});
					
					jQuery('.ea_img_upload').each( function(){
						var t=jQuery(this).prev();
						if(t.attr('src'))
						{
							t.show();
						}
					});
					
					var upload_frame;   
					var value_id;
					var t;
					jQuery(document).on('click', '.ea_img_upload', function(event){
						t=jQuery(this);
						value_id =jQuery( this ).attr('id');       
						event.preventDefault();   
						if( upload_frame ){   
							upload_frame.open();   
							return;   
						}   
						upload_frame = wp.media({   
							title: 'Insert image',   
							button: {   
								text: 'Insert',   
							},   
							multiple: false   
						});   
						upload_frame.on('select',function(){   
							attachment = upload_frame.state().get('selection').first().toJSON();
							t.next().val(attachment.url);
							t.prev().attr('src',attachment.url);
							t.prev().show();
						});	   
						upload_frame.open();   
					});
	
				});
			  </script>";
 ?>
</form>
 <?php
 }
 ?>