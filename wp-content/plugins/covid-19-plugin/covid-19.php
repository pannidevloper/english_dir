<?php
/**
 * Plugin Name: Covid-19
 * Version:     1.0
 * Author:      Lix
 * Description: APP Version Setting
 */



/***************************** show  at admin page  ******************************/

add_action('admin_menu', 'covid19_admin_menu');



function covid19_admin_menu(){

add_menu_page('Covid-19', 'Covid-19', 'manage_options', 'covid19');

add_submenu_page( 'covid19', 'Covid-19-App-sitting', 'Covid-19-App-sitting',
    'manage_options', 'covid19','covid_app_sitting');

add_submenu_page( 'covid19', 'Users Data', 'Users Data',
    'manage_options', 'covid_userdata','covid19_userdata');



}

function covid_app_sitting()
{
?>
    <h2>APP Version Setting</h2>
    <form action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST">
        <?php
        if ($_POST['save']) {
           	$show_covid_app_notifaction = htmlspecialchars($_POST['show_covid_app_notifaction']);
            update_option('show_covid_app_notifaction', $show_covid_app_notifaction);
        }

      
        ?>
        <table class="form-table">
            <tbody>
                
                <tr>
                    <th scope="row">Show App Notifaction</th>
                    <td><input name="show_covid_app_notifaction" type="checkbox" value="1" <?php echo get_option( 'show_covid_app_notifaction' )?'checked':''; ?>></td>
                </tr>
            </tbody>
        </table>
        <p class="submit"><input type="submit" name="save" class="button button-primary" value="Save Changes"></p>
    </form>
<?php
}

function covid19_userdata (){

global $wpdb;
$data = $wpdb->get_results("SELECT * FROM `wp_covid19_data` ");
?>
<table>
  <tr>
    <th>Username</th>
    <th>Location Co-ordiantes (Lat,Lng) </th>
     <th>Date </th>
    
  </tr>
<?php 
foreach ($data as $single_user) {
	if($single_user->lat && $single_user->lng){
    		$lat = $single_user->lat;
    		$lng= $single_user->lng;
    	}
    	else{
    		$lat ='Not found';
    		$lng='Not found';
    	}
   $user_info = get_userdata($single_user->wp_user_id);

?>
  <tr>
    <td>
     <?php  echo $user_info->user_login; ?>
    </td>
    <td>
     <?php  echo $lat. ",".$lng;?>
    </td>
    <td>
     <?php  echo date('m/d/Y', $single_user->datetime);?>
    </td>
</tr>
<?php 
}?>
</table>
<?php 
}

if ( isset( $_GET['page'] ) && $_GET['page'] == 'covid_userdata' ) {



wp_enqueue_style( 'hook_custom_wp_admin_css', plugins_url('covid-19.css', __FILE__) );



}





?>

