=== Privacy Policy Generator WordPress Plugin: WP Legal Pages ===
Contributors: WPEka Club
Donate link: https://club.wpeka.com/product/wplegalpages
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: Privacy Policy, GDPR, Terms and Conditions, Terms of Use, Terms of Service
Requires at least: 5.0+
Requires PHP: 5.0
Requires PHP: 5.0
Tested up to: 5.3.2
Stable tag: 2.3.8

Easily generate privacy policy pages on your WordPress website in a few clicks. Legal Pages for blogs, ecommerce and marketing websites.

== Description ==

Generate WordPress Legal Pages like Privacy Policy, Terms of Use, Disclaimers. Covers 25+ Legal Templates for all types of WordPress Websites.

WP Legal Pages is a popular and regularly updated privacy policy generator plugins with more than 180,000 downloads. From WordPress Bloggers, Affiliate Marketers, Corporate websites to ecommerce stores, the privacy pages are designed for different types of websites.

= Privacy Policy Generator =
Generate a standard privacy policy to comply with ad networks. Required for blogs, businesses and ecommerce websites.

= Privacy Notice for California Consumer Privacy Act (CCPA) =
Generate a standard privacy policy to comply with the new California Consumer Privacy Act (CCPA) and California Online Protection Act (CalOPPA). For a customized policy created using a guided wizard, please use the [Pro Version](https://club.wpeka.com/product/wplegalpages/?utm_source=wporg&utm_medium=wplegalpages&utm_campaign=wplegalpages&utm_content=readme-ccpa).

= Terms & Conditions Generator =
Generate a standard terms & conditions policy page. For a customized policy created using a guided wizard, please use the [Pro Version](https://club.wpeka.com/product/wplegalpages/?utm_source=wporg&utm_medium=wplegalpages&utm_campaign=wplegalpages&utm_content=readme-terms-and-conditions).

= DMCA Policy Generator =
Generate a standard DMCA privacy policy page. Required for blogs, businesses and ecommerce websites that provide content sourced from other content producers.

[youtube https://www.youtube.com/watch?v=iqdLl9qsBHc]

== Privacy Policy Generator WordPress Plugin: WP Legal Pages ==

WP Legal Pages makes it super easy to generate privacy policy pages for websites.

1. Add your business information.
1. Select a legal policy page template.
1. Click 'Generate'.

== Why WP Legal Pages? ==

= Easy to install =
Install and add legal policy pages to your website in 5 minutes or less.

= GUIDED WIZARD TO GENERATE LEGAL PAGES (PREMIUM FEATURE) =
Generate complex legal pages like "Terms & Conditions" and "California Consumer Privacy Act (CCPA)" using a step-by-step guided wizard. Available in [Pro Version](https://club.wpeka.com/product/wplegalpages/?utm_source=wporg&utm_medium=wplegalpages&utm_campaign=wplegalpages&utm_content=readme-guided-wizard) only.
Customizable for various types of websites which sell in multiple countries like e-commerce, marketplace, SaaS.

= Easy to customize privacy policy pages =
Generated policy pages are regular WordPress pages. You can easily edit and update them.

= 25+ pre-built policy templates (Premium Feature) =
WP Legal Pages comes with ready-to-use policy page templates. Fill in your business information and hit generate.

= Option to force agreement (Premium Feature) =
Force users to accept your policy before they can access a page or post. Available in [Pro Version](https://club.wpeka.com/product/wplegalpages/?utm_source=wporg&utm_medium=wplegalpages&utm_campaign=wplegalpages&utm_content=readme-force-agreement) only.

= One-click affiliate disclosure addition (Premium Feature) =
Confirm with FTC guidelines. Add affiliate disclosure to the end of a page or post. Available in [Pro Version](https://club.wpeka.com/product/wplegalpages/?utm_source=wporg&utm_medium=wplegalpages&utm_campaign=wplegalpages&utm_content=readme-affiliate-disclosure) only.

== Premium Policy Pages ==
[WP Legal Pages Pro](https://club.wpeka.com/product/wplegalpages/?utm_source=wporg&utm_medium=wplegalpages&utm_campaign=wplegalpages&utm_content=readme-premium-policy-pages) can generate 25+ premium policy pages including:

= GDPR Privacy Policy =
Generate a privacy policy to help comply with GDPR's privacy policy requirements. Required for businesses who gather visitor information from EU countries. Also available for France - RGPD and Germany - DSGVO.

= Facebook Privacy Policy =
Generate a Facebook privacy policy for your app, website or ads account.

= Generic Privacy Policy =
Generate a generic privacy policy to comply with ad networks. Required for businesses who advertise on ad networks like Facebook and Google AdSense.

= Website Terms & Conditions Pro =
Wizard to customize Terms of Service or Terms & Conditions for different types of websites.

= Cookie Policy =
Generate a cookie policy (in addition to the above Privacy policies) to help comply with GDPR, CCPA and CalOPPA (California Online Privacy Protection Act). Includes a third-party cookie audit table that can be generated using the [GDPR Cookie Consent](https://wordpress.org/plugins/gdpr-cookie-consent/) Plugin.

= Amazon Affiliate Disclosure =
Generate an Amazon Affiliate Disclosure policy page. Required for affiliate websites promoting Amazon products.

= Affiliate Agreement Policy =
Generate a standard affiliate agreement policy. Useful for businesses that run affiliate programs on their websites.

= Affiliate Disclosure Policy =
Generate an affiliate income disclosure policy page. Required by FTC for businesses promoting products by earning affiliate commissions.

= Earnings Disclaimer Policy =
Generate an earnings disclaimer policy page. Required by FTC for website and blogs that have content related to income schemes, private finance etc.

= Medical Disclaimer Policy =
Generate a medical disclaimer policy to indemnify your website/blog from medical claims. Required for website and blog that provide content related to healthcare.

= Refund Policy =
Generate a standard refunds policy page. Useful for ecommerce websites or websites that sell products/services on their website.

= Returns Policy =
Generate a standard returns policy page. Useful for ecommerce websites or websites that sell physical products on their website.

= Digital Goods Refunds Policy =
Generate a standard refunds policy page. Useful for ecommerce websites or websites that sell digital products/services on their website.

= Children’s Online Privacy Policy (COPPA) =
Generate a standard COPPA policy page on your WordPress website. Required for website and apps whose audience consists of children below 13 years of age.

= Newsletter Subscription Policy =
Generates a standard Newsletter Subscription Policy. Useful for businesses and blogs that run a newsletter subscription.

= External Links Policy =
Generates a standard External Links policy page. Covers the links to or from external websites, which are not owned or controlled by your website.

= Anti Spam Policy =
Generate a standard anti-spam policy. Required for businesses, applications or websites who allow users to send emails in bulk.

= Age Verification Policy with Consent Popup =
Generate an age verification policy with consent popup. Requried  for websites, blogs and ecommerce sites that provide adult content or sell products/services meant for adults.

= Cookie Consent Notice =
Display a standard cookie consent notice on your WordPress website. Use the [GDPR Cookie Consent](https://wordpress.org/plugins/gdpr-cookie-consent/) plugin to display a cookie consent notice according to GDPR standards.

Learn more about [WP Legal Pages Pro](http://bit.ly/32n3Qrq).

== Installation ==

= From within WordPress =

1. Visit 'Plugins > Add New'.
1. Search for 'WP Legal Pages'.
1. Activate WP Legal Pages from your Plugins page.
1. Click on the 'WP Legal Pages' admin menu to get started.

= Manually =

1. Upload the `wp-legal-pages` folder to the `/wp-content/plugins/` directory.
1. Activate the WP Legal Pages plugin through the 'Plugins' menu in WordPress.
1. WP Legal Pages menu will appear in left hand side. Click on it & get started to use.
1. Click on the 'WP Legal Pages' admin menu to get started.

== Screenshots ==
1. general settings page.png
2. legal templates.png
3. adding legal page as popup.png
4. affiliate disclosure.png

== Frequently Asked Questions ==

= Is this a Free Privacy Policy generator for WordPress? =
Yes! The privacy policy generator feature in this plugin is Free.

The Pro version is ideal:
- If you need to customize it for GDPR
- Use the guided wizard (Terms & Conditions)
- Specific Legal Needs (Disclaimers & Disclosures - Affiliates, Consultants, Medical Blogs, Marketers)
- Returns and Refunds (WooCommerce or e-Commerce Websites)

= Why use WPLegalPages? =
GDPR, FTC and other privacy regulators along with ad networks require you to have legal policy pages on your website. Instead of spending hours researching and creating your own policy pages, use WP Legal Pages to generate them in a few clicks.

= How to use WPLegalPages? =
1. Click on the 'WP Legal Pages' admin menu.
1. Accept the disclaimer.
1. Fill in business details.
1. Click create legal pages >> Select the type of legal template >> Publish.
1. You can also use shortcodes of each template and save it as a post or page.

= Will it protect my website? =
WP Legal Pages provides you pre-approved templates. You may want to customize it as per your needs. Please consult your lawyer for any specific needs for compliance with your local law regulators.

== Changelog ==
= 2.3.8 =
* Feature: Added Terms of use templates for French and German Translations.

= 2.3.7 =
Update: Minor plugin updates.

= 2.3.6 =
Update: Updated Terms of use template.

= 2.3.5 =
Feature: Added standard California Consumer Privacy Act (CCPA) and Terms and Conditions policy templates.
Update: Made compatible with WordPress 5.3.2.

= 2.3.4 =
Update: Updated link for show credit.
Update: Made compatible with WordPress 5.3.1.

= 2.3.3 =
Update: Minor plugin updates.

= 2.3.2 =
Fix: Fatal error if Analytics SDK is included in two or more plugins installed on same instance.

= 2.3.1 =
Feature: Added feedback form on plugin deactivation.

= 2.3.0 =
Update: Updated template HTML structure.
Update: Code updated as per WordPress standards.

= 2.2.9 =
Fix: Pro integration issue.

= 2.2.8 =
Fix: Minification issue.

= 2.2.7 =
Fix: Filter for Legal template count.

= 2.2.6 =
Integration of WPLegalPages Pro version.

= 2.2.5 =
Fixed issue with filter for Legal Pages rendering

= 2.2.4 =
Improved design of Admin screens.
Compatibility with WordPress 5.2.2

= 2.2.3 =
Updated Admin menu for WPLegalPages Cookies.
Made compatible with WordPress 5.2.1

= 2.2.2 =
Made compatible with WPLegalPages Cookie Consent Addon.
Fixed broken links.

= 2.2.1 =
Compatible with WordPress 5.1.1

= 2.2 =
Admin panel UI improvement
Template not updated after activation of the plugin

= 2.1 =
Updated EU cookie template and Privacy policy template to remain compliant with upcoming changes to the EU data protection law

=2.0.6 =
Minor dashboard changes.

= 2.0.5 =
Fixed conflict issues.

= 2.0.4 =
Minor changes in dashboard.
Fixed issues related to shortcode rendering.

= 2.0.3 =
Tested upto WordPress version 4.8.

= 2.0.2 =
Added subscription form.

= 2.0.1 =
Change tags.

= 1.5.9 =
Made Compatible with WordPress 4.7.1
Code Cleanup

= 1.5.8 =
Fix give credit option issue.

= 1.5.6 =
Fix for css/js load to prevent conflict with other plugins.

= 1.5.5 =
Rewrite Plugin.

= 1.5.4 =
Added EU Cookies Policy.

= 1.5.3 =
Added Give Credit option.

= 1.5.2 =
Compatible with PHP 7.0.

= 1.5.1 =
Added lest of Pro templates

= 1.5 =
Added new DMCA template

= 1.4 =
Updated for Compatibility with WP 4.5.3. Improved Admin Dashboard for easy creation of policy pages

= 1.3 =
WPLegal Pages is now compatible WordPress 4.5.

= 1.2 =
Added banner to upgrade WPLegal Pages Pro version on plugin dashboard

= 1.1 =
Enhanced security

= 1.0.0 =
Base version of the plugin.

== What Next? ==

If you like this plugin, then consider checking out our other projects:

- [GDPR Cookie Consent](https://club.wpeka.com/product/wp-gdpr-cookie-consent/?utm_source=wporg&utm_medium=wp-legal-pages&utm_campaign=link&utm_content=gdpr-cookie-consent) - ICO Compliant GDPR Cookie Consent Plugin

- [WP Local Plus](https://club.wpeka.com/product/wp-local-plus/?utm_source=wporg&utm_medium=wp-legal-pages&utm_campaign=link&utm_content=wp-local-plus) - WordPress Business Directory Plugin

- [Woo Auction Software](https://club.wpeka.com/product/woo-auction-software/?utm_source=wporg&utm_medium=wp-legal-pages&utm_campaign=link&utm_content=woo-auction-software) - Host eBay like auctions on your WooCommerce website

- [Survey Funnel](https://club.wpeka.com/product/survey-funnel/?utm_source=wporg&utm_medium=wp-legal-pages&utm_campaign=link&utm_content=survey-funnel) - Generate leads on your WordPress website with targeted opt-in offers and surveys

- [WP Raffle](https://club.wpeka.com/product/wp-raffle/?utm_source=wporg&utm_medium=wp-legal-pages&utm_campaign=link&utm_content=survey-funnel) - Run giveaways and contests on your WordPress website

== Official Site ==
For more information and to take a look at the advanced WP Legal Pages version, visit our website - <a href="http://wplegalpages.com/?utm_source=wporg&utm_campaign=wplegalpages&utm_content=wp-legal-pages-version">WP Legal Pages version</a>.