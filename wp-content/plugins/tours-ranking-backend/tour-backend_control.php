<?php
/*
Plugin Name: Tours Ranking Backend
Description: Control Tours Ranking parameters and cron jon for updating scores
Version: 2.0
Author: M Farhan
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
// Adding Options page for front_end on admin side.
add_action('admin_menu', function ()
{
    add_options_page('Tour Rank Percentage', 'Tour Rank Percentage', 'manage_options', 'Tour Rank Percentage', 'rank_sitting');
});
add_action('admin_init', function ()
{
   /// register_setting('tour-rank-plugin-settings', 'front_apps');
    register_setting('tour-rank-plugin-settings', 'retention_rate_percentage');
    register_setting('tour-rank-plugin-settings', 'number_shared_percentage');
    register_setting('tour-rank-plugin-settings', 'number_comments_percentage');
    register_setting('tour-rank-plugin-settings', 'number_likes_percentage');
    register_setting('tour-rank-plugin-settings', 'number_dislikes_percentage');
    register_setting('tour-rank-plugin-settings', 'number_followers_percentage');
    register_setting('tour-rank-plugin-settings', 'number_clicks_percentage');
    register_setting('tour-rank-plugin-settings', 'number_views_percentage');
    register_setting('tour-rank-plugin-settings', 'live_chat_click_percentage');
});

function rank_sitting(){
?> 
<div class="wrap">
      <form action="options.php" method="post">
 
        <?php
    settings_fields('tour-rank-plugin-settings');
    do_settings_sections('tour-rank-plugin-settings');
?>
        <table>
             
    
            <tr>
                <th>Retention Rate Percentage (Only Number)</th>
                <td><input type="number" placeholder="30" name="retention_rate_percentage" value="<?php
    echo esc_attr(get_option('retention_rate_percentage')); ?>" size="50" /></td>
            </tr>
            <tr>
                <th>Number of Shared Percentage( Only Number) </th>
                <td><input type="number" placeholder="20" name="number_shared_percentage" value="<?php
    echo esc_attr(get_option('number_shared_percentage')); ?>" size="50" /></td>
            </tr>
            <tr>
                <th>Number of Likes Percentage( Only Number) </th>
                <td><input type="number" placeholder="10" name="number_likes_percentage" value="<?php
    echo esc_attr(get_option('number_likes_percentage')); ?>" size="50" /></td>
            </tr>
            <tr>
                <th>Number of comments Percentage( Only Number) </th>
                <td><input type="number" placeholder="10" name="number_comments_percentage" value="<?php
    echo esc_attr(get_option('number_comments_percentage')); ?>" size="50" /></td>
            </tr>
            <tr>
                <th>Number of Dislikes Percentage( Only Number) </th>
                <td><input type="number" placeholder="20" name="number_dislikes_percentage" value="<?php
    echo esc_attr(get_option('number_dislikes_percentage')); ?>" size="50" /></td>
            </tr>
            <tr>
                <th>Number of Fellowers Percentage( Only Number) </th>
                <td><input type="number" placeholder="20" name="number_followers_percentage" value="<?php
    echo esc_attr(get_option('number_followers_percentage')); ?>" size="50" /></td>
            </tr>
             <tr>
                <th>Number of Clicks Percentage( Only Number) </th>
                <td><input type="number" placeholder="5" name="number_clicks_percentage" value="<?php
    echo esc_attr(get_option('number_clicks_percentage')); ?>" size="50" /></td>
            </tr>
             <tr>
                <th>Number of views Percentage( Only Number) </th>
                <td><input type="number" placeholder="5" name="number_views_percentage" value="<?php
    echo esc_attr(get_option('number_views_percentage')); ?>" size="50" /></td>
            </tr>
            <tr>
                <th>Live Chat Click Percentage( Only Number) </th>
                <td><input type="number" placeholder="5" name="live_chat_click_percentage" value="<?php
    echo esc_attr(get_option('live_chat_click_percentage')); ?>" size="50" /></td>
            </tr>
              <tr>
                <td><?php
    submit_button(); ?></td>
            </tr>
 
        </table>
 
      </form>
    </div>
  <?php
}

//Function to perform all backend ranking and update score.
function rank_tours_based_on_data($tourid,$sale_agent)
{
    global $wpdb;

    $retention_rate_percentage = esc_attr(get_option('retention_rate_percentage'));
    $number_shared_percentage =  esc_attr(get_option('number_shared_percentage'));
    $number_likes_percentage = esc_attr(get_option('number_likes_percentage'));
    $number_comments_percentage = esc_attr(get_option('number_comments_percentage'));
    $number_followers_percentage = esc_attr(get_option('number_followers_percentage'));
    $number_dislikes_percentage = esc_attr(get_option('number_dislikes_percentage'));
    $number_clicks_percentage = esc_attr(get_option('number_clicks_percentage'));
    $number_views_percentage = esc_attr(get_option('number_views_percentage'));
    $live_chat_click_percentage = esc_attr(get_option('live_chat_click_percentage'));
    $tour_id= $tourid;
    
    $likecount = $wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$tour_id}' AND `sc_type` = 0");

     $dislikecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$tour_id}' AND `sc_type` = 1");

     if(isset($sale_agent))
     {
     $sale_agent = $sale_agent;
     $follow_count=$wpdb->get_var("SELECT COUNT(*) FROM `social_follow` WHERE `sf_agent_id` = '{$sale_agent}'");
     }
     else
     {
        $follow_count =0;
     }
     

$tourCommentData = $wpdb->get_var("SELECT COUNT(*) FROM `social_comments`  WHERE scm_tour_id = '$tour_id'");

 $post_id = $wpdb->get_row("SELECT post_id FROM `wp_postmeta` WHERE `meta_key`='tour_id' AND `meta_value`='{$tour_id}'")->post_id;
 //echo  $post_id;
 if(function_exists('getPostViews')){
 $views = number_format(getPostViews($post_id));
}
 $query_to_get_total ="SELECT SUM(clicks) as total FROM wp_tour_clicks WHERE tour_id ='$tour_id'";
 $sum_of_clicks = $wpdb->get_var($query_to_get_total);

$last_data = $wpdb->get_row("SELECT * FROM `wp_live_chat_clicks` WHERE `tour_id` = '{$tour_id}'");
if (!empty($last_data))
{
  $click_count_for_chat = $last_data->clicks;  
}
/*
$likecount =100;
$dislikecount =10;
$follow_count = 100;
$tourCommentData =100;
$retention_rate = 10;
*/
$likedata = $likecount*($number_likes_percentage/100);
$dislikedata = $dislikecount * ($number_dislikes_percentage/100);
$follow_data = $follow_count *($number_followers_percentage/100);
$comments_data = $tourCommentData * ($number_comments_percentage/100);
//$retention_data = $retention_rate * ($retention_rate_percentage/100);
$views_data = $views * ($number_views_percentage/100);
$Clicks_data = $sum_of_clicks * ($number_clicks_percentage/100);
$Live_chat_button_data = $click_count_for_chat*($live_chat_click_percentage/100);
$retention_data = 0;
$score = $likedata +$follow_data +$comments_data-$dislikedata+$Clicks_data+$views_data+$Live_chat_button_data;
return $score;

}


// Function for Cron Job

function __score_update_in_database(){

set_time_limit(0);
    global $wpdb;

 $sql = "SELECT wp_tour.*,wp_tour_sale.number_of_people,wp_tour_sale.netprice,wp_tour_sale.username,wp_tourmeta.meta_value
                    FROM tourfrom_balieng2.wp_tour,tourfrom_balieng2.wp_tour_sale,tourfrom_balieng2.wp_tourmeta
                    WHERE
                        wp_tour.id=wp_tour_sale.tour_id
                        AND wp_tourmeta.tour_id=wp_tour_sale.tour_id
                        AND wp_tourmeta.meta_key='bookingdata'";
                       
    $data = $wpdb->get_results($sql);
    foreach($data as $single_tour) {

        $tour_id = $single_tour->id ;

   
        $creater = $wpdb->get_row("SELECT meta_value FROM `wp_tourmeta` where meta_key='userid' and tour_id=" .  $tour_id);
        $agent_id = $creater->meta_value;
        if(empty($agent_id) || $agent_id==NULL )
        {
            $agent_id =0;
        }

        $score = rank_tours_based_on_data($tour_id,$agent_id) ;
         $wpdb->update('wp_tour', array( 'score' => $score), array('id' =>$tour_id),
                    array( 
        '%f'
    ), 
                    array( '%s' ) 


            );
        
        }

}

// My three hour shedudlar

function my_three_hours( $schedules ) {
    // add a 'hourly' schedule to the existing set
    $schedules['three_hourly'] = array(
        'interval' => 10800,
        'display' => __('Every Three Hour')
    );
    return $schedules;
}
add_filter( 'cron_schedules', 'my_three_hours' ); 

// Cron jOB scheduler


function my_activation_for_rank() {
        if (! wp_next_scheduled ( '__score_update_in_database' )) {
            wp_schedule_event(time(), 'three_hourly', '__score_update_in_database');
        }
        
    }
    add_action('__score_update_in_database','__score_update_in_database');

    function my_deactivation_for_rank() {
        wp_clear_scheduled_hook('__score_update_in_database');
        
    }

        register_activation_hook(__FILE__, 'my_activation_for_rank');    // Register hook to activate the scheduler
        register_deactivation_hook(__FILE__, 'my_deactivation_for_rank');
//__score_update_in_database();
//exit();