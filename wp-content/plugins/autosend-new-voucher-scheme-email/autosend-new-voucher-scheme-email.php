<?php

/**
 * Plugin Name: Send voucher scheme email to buyer
 * Version:     1.0
 * Author:      Lix
 * Description: Auto send voucher email to all buyer
 */


register_activation_hook(__FILE__, 'auto_send_voucher_email_activate');


function auto_send_voucher_email_activate()
{
}

function autoSendVoucherEmailToBuyer($ID, $post)
{
	global $wpdb;
	if (strtotime($post->post_date) < (time() - 24 * 60 * 60))
		return;
	if (get_post_meta($ID, 'sendvoucheremail', true) != 1) {
		update_post_meta($ID, 'sendvoucheremail', 1);
	} else
		return;

	$userList = $wpdb->get_results("SELECT username,email FROM `user` WHERE access=2 AND activated=1");
	$scheme_name = $post->post_title;
	foreach ($userList as $user) {
		$username = $user->username;
		$scheme_link = home_url('user/') . $username . '/?profiletab=vouchers';
		include("template/email_template.php");
		wp_mail($user->email, 'New Voucher scheme for you', $mail_content);
	}
}
add_action('publish_voucher_scheme', 'autoSendVoucherEmailToBuyer', 10, 2);
