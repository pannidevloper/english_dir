<?php
/**
 * Plugin Name: Travcust Tour Guide
 * Version:     1.0
 * Author:      Lix
 * Description: Travcust Tour Guide setting
 */

 
 
 register_activation_hook( __FILE__, 'travcust_popupguide_activate' );
 
 function travcust_popupguide_activate() {

}

 /***************************** show the subscribe in admin page  ******************************/
 
 add_action('admin_menu', 'add_travcust_popupguide_admin_menu');
 add_action('admin_init', 'add_travcust_popupguide_admin_menu_init');
 function add_travcust_popupguide_admin_menu(){
 
	add_options_page( 'Travcust Tour Guide', 'Travcust Tour Guide', 'manage_travcust_data', __FILE__, 'show_travcust_popupguide' );
	
 }
 
 function add_travcust_popupguide_admin_menu_init(){
	
	

 }
 function show_travcust_popupguide(){
   wp_enqueue_media();
 ?>
 <h2>Travcust Tour Guide Setting </h2>
 <form action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST" >
 <?php 
  $priceratio=get_option('person_price_ration');
   if($_POST['import'])
 {
	 if($xlsx=SimpleXLSX::parse($_FILES['import_file_upload']['tmp_name']))
	 {
		 $xlsx_data=$xlsx->rows();
		 $details_index=2;
		 $price_index=6;
		 $area_index=4;
		 $vendor_index=3;	 
		 $phone_index=5;
         $language_index = 7;
		 if($_POST['import_type']=='append')
			$TravcustGuide=unserialize(get_option('_cs_travcust_popupguide'));
		 else if($_POST['import_type']=='replace')
			$TravcustGuide=array();
		 $i=0;
		 foreach($xlsx_data as $t)
		 {
			 if(!empty($t[$details_index]) && $i>1)
			 {
				 /*echo 'Name:'.$t[$details_index]."<br/>";
				 echo 'Price:'.$t[$price_index]."<br/>";
				 echo 'Area:'.$t[$area_index]."<br/>";
				 echo 'Vendor:'.$t[$vendor_index]."<br/>";
				 echo 'Phone:'.$t[$phone_index]."<br/>";*/
				 
				 $find=false;
				 foreach($TravcustGuide as &$guide)
				 {			 
					 if($guide['item']==$t[$details_index])
					 {
						 //echo "Append subitem<br/>";
						$guide['price']=$t[$price_index];
						$guide['area']=$t[$area_index];
						$guide['vendor']=$t[$vendor_index];
						$guide['phone']=$t[$phone_index];
                        $guide['language']=$t[$language_index];
						$find=true;
						break;
					 }
				 }
				 if($find==false)
				 {
					 //echo "New subitem<br/>";
					array_push($TravcustGuide,array('item'=>$t[$details_index],
						  'img'=>'',
						  'price'=>$t[$price_index],
						  'area'=>$t[$area_index],
						  'vendor'=>$t[$vendor_index],
						  'phone'=>$t[$phone_index],
                          'language'=>$t[$language_index]
						));
				 }
			 }
			 
			 $i++;
		 }
		 //var_dump($TravcustGuide);
		 if(!empty($TravcustGuide)) { update_option('_cs_travcust_popupguide', serialize($TravcustGuide)); }
	 }
	 else
	 {
		 echo SimpleXLSX::parse_error();
	 }
 }
 if($_POST['save'])
 {
	$i=1;
	$TravcustPopupguide=array();
	while($i<=(int)$_POST['ea_length'])
	{
		if(!empty($_POST["ea{$i}_item"])){
		array_push($TravcustPopupguide,array('item'=>$_POST["ea{$i}_item"],
						  'img'=>$_POST["ea{$i}_img"],
						  'price'=>$_POST["ea{$i}_price"]*$priceratio,
						  'area'=>$_POST["ea{$i}_area"],
						  'vendor'=>$_POST["ea{$i}_vendor"],
						  'phone'=>$_POST["ea{$i}_phone"],
                          'language'=>$_POST["ea{$i}_language"],
					));
		}
		$i++;
	}
	if(!empty($TravcustPopupguide)) { update_option('_cs_travcust_popupguide', serialize($TravcustPopupguide)); }
}
	
	$TravcustPopupguide=unserialize(get_option('_cs_travcust_popupguide'));
		echo '<table class="extraactivity">
				<thead>
					<tr><th>Details</th><th>Pictures</th><th>Price(Rp)</th><th>Area</th><th>Vendor name</th><th>Phone number</th><th>Language</th><th>Language</th></tr>
				</thead>
				<tbody>';
		for($i=1;$i<=count($TravcustPopupguide);$i++)
		{
			echo '<tr>
					<td><input name="ea'.$i.'_item" type="text" value="'.$TravcustPopupguide[$i-1]['item'].'" /></td>
					<td><img width=64 height=64 src="'.$TravcustPopupguide[$i-1]['img'].'" style="display:none;" /><input class="ea_img_upload" type="button" value="Upload img" /><input name="ea'.$i.'_img" type="hidden" value="'.$TravcustPopupguide[$i-1]['img'].'" /></td>
					<td><input name="ea'.$i.'_price" type="text" value="'.$TravcustPopupguide[$i-1]['price'].'" /></td>
					<td><input name="ea'.$i.'_area" type="text" value="'.$TravcustPopupguide[$i-1]['area'].'" /></td>
					<td><input name="ea'.$i.'_vendor" type="text" value="'.$TravcustPopupguide[$i-1]['vendor'].'" /></td>
					<td><input name="ea'.$i.'_phone" type="text" value="'.$TravcustPopupguide[$i-1]['phone'].'" /></td>
                    <td><input name="ea'.$i.'_language" type="text" value="'.$TravcustPopupguide[$i-1]['language'].'" /></td>
					<td>
					  <select>					  	
					    <option value="English">English</option>
					    <option value="chinese">Chinese</option>
					  </select>
					</td>
				  </tr>';
		}
		echo '	</tbody>
			  </table>
			  <input id="ea_length" name="ea_length" type="hidden" value="'.count($TravcustPopupguide).'"/>
			  <input type="button" class="button" value="Add Row" id="AddEARow" />
			  <br/><br/>
			  <input type="submit" name="save" id="save" class="button button-primary button-large" value="Save">
			  <br/><br/>';
		echo "<script>
				jQuery(document).ready(function(){
					var ea_length;
					jQuery('#AddEARow').click(function (){
						ea_length=parseInt(jQuery('#ea_length').val())+1;
						jQuery('#ea_length').val(ea_length);
						jQuery('table.extraactivity>tbody').append('<tr> <td><input name=\"ea'+ea_length+'_item\" type=\"text\" value=\"\" /></td> <td> <img width=64 height=64 src=\"\" style=\"display:none;\" /> <input class=\"ea_img_upload\" type=\"button\" value=\"Upload img\" /> <input name=\"ea'+ea_length+'_img\" type=\"hidden\" value=\"\" /> </td> <td><input name=\"ea'+ea_length+'_price\" type=\"text\" value=\"\" /></td> <td><input name=\"ea'+ea_length+'_area\" type=\"text\" value=\"\" /></td> <td><input name=\"ea'+ea_length+'_vendor\" type=\"text\" value=\"\" /></td> <td><input name=\"ea'+ea_length+'_phone\" type=\"text\" value=\"\" /></td> <td><input name=\"ea'+ea_length+'_language\" type=\"text\" value=\"\" /></td> </tr>');
					});
					
					jQuery('.ea_img_upload').each( function(){
						var t=jQuery(this).prev();
						if(t.attr('src'))
						{
							t.show();
						}
					});
					
					var upload_frame;   
					var value_id;
					var t;
					jQuery(document).on('click', '.ea_img_upload', function(event){
						t=jQuery(this);
						value_id =jQuery( this ).attr('id');       
						event.preventDefault();   
						if( upload_frame ){   
							upload_frame.open();   
							return;   
						}   
						upload_frame = wp.media({   
							title: 'Insert image',   
							button: {   
								text: 'Insert',   
							},   
							multiple: false   
						});   
						upload_frame.on('select',function(){   
							attachment = upload_frame.state().get('selection').first().toJSON();
							t.next().val(attachment.url);
							t.prev().attr('src',attachment.url);
							t.prev().show();
						});	   
						upload_frame.open();   
					});
	
				});
			  </script>";
 ?>
</form>
<div id="data-import-form" class="postbox">
<form action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST" enctype="multipart/form-data" >
<table class="tablepress-postbox-table fixed">
<tbody>
	<tr style="display: table-row;">
		<th scope="row">Select file:</th>
		<td>
			<input name="import_file_upload" type="file" style="box-sizing: border-box;">
		</td>
	</tr>
	<tr>
		<th scope="row">Import Format:</th>
		<td>
			<select name="import[format]">
				<option value="xlsx">XLSX - Microsoft Excel 2007-2013 (experimental)</option>
			</select>
		</td>
	</tr>
	<tr>
		<th scope="row">Replace, or Append?:</th>
		<td>
			<label for="tables-import-type-replace"><input name="import_type" id="tables-import-type-replace" type="radio" value="replace"> Replace existing table</label>
			<label for="tables-import-type-append"><input name="import_type" id="tables-import-type-append" type="radio" value="append" checked="checked"> Append rows to existing table</label>
		</td>
	</tr>
	<tr>
		<th scope="row"></th>
		<td><input type="submit" value="Import" name="import"></td>
	</tr>
</tbody>
</table>
</form>
</div>

 <?php
 }
 ?>