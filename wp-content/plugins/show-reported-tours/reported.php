<?php
/**
 * Plugin Name: Show Reported Tours
 * Version:     1.0
 * Author:      Farhan
 * Description: List of Tours  Reported  by buyer/sellers..
 */



 // Add admin sitting

add_action( 'admin_menu', 'my_admin_menu_for_reported_tours' );

function my_admin_menu_for_reported_tours() {

add_menu_page( 'Reported Tours', 'Reported Tours', 'manage_options', 'reported_tours.php', 'travpart_tour_reports', 'dashicons-tickets', 6  );

}


if ( isset( $_GET['page'] ) && $_GET['page'] == 'reported_tours.php' ) {

wp_enqueue_style( 'custom_wp_admin_css', plugins_url('chatmsg.css', __FILE__) );

}

add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

	function travpart_tour_reports() {
	global $wpdb;
	?>

	<div class="row">
	<div class="col-sm-6">
	<h2 style="color:green">
	Reported  Tours List
	</h2>
	<?php
	$records = $wpdb->get_results( 
	"SELECT * FROM  reported_tours ORDER BY id DESC"
	);
	if(!empty($records)):
	?> 
	<table id="customers">
	<tr>
	<th>Reported Tour id</th>
	<th>Reported by </th>
	<th>Status </th>
	</tr>

	<?php

	foreach ( $records as $result ) {

	$tour_link= "https://www.travpart.com/English/tour-details/?bookingcode=BALI".$result->reported_tour_id;

	echo "<tr><td>";
	echo '<a href= '.$tour_link.'>'.$result->reported_tour_id .'</a>';
	echo "</td>";
	echo "<td>";
	echo $result->report_by_user_email;
	echo "</td>";
	echo "<td>";
	echo $result->status==0 ? 'Pending' : 'Solved';
	echo "</td>";

	echo"</tr>";
	}
	else:
	echo "No data";
	endif;
	?>
	</tr>
	</table>

	</div>


	</div>
	<div>

	</div>



<?php
		}