<?php

/**
 * Plugin Name: User Activty History
 * Version:     1.0
 * Author:      Farhan
 * Description: Check the user activty 
 */





/***************************** show  at admin page  ******************************/

add_action('admin_menu', 'user_hooks_admin_menu');

function user_hooks_admin_menu()
{
    add_menu_page( 'User Hook Performance', 'User Hook Performance', 'manage_options', 'user_hook_performance.php', 'show_data', 'dashicons-tickets', 6  );


}

if ( isset( $_GET['page'] ) && $_GET['page'] == 'user_hook_performance.php' ) {

wp_enqueue_style( 'hook_custom_wp_admin_css', plugins_url('user_hook_performance.css?v2', __FILE__) );

}

// sign up users list
function show_signup_users_list(){
  global $wpdb;
  $date = filter_input(INPUT_GET, 'tid', FILTER_SANITIZE_STRING);
  $access = filter_input(INPUT_GET, 'access', FILTER_SANITIZE_STRING);

  $list = $wpdb->get_results(" SELECT * from user where user_registered LIKE  '%{$date}%' AND username!='Guest' and access='{$access}'");?>
  <table>
  <tr>
  <th><?php echo "User name" ?></th>
  <th><?php echo "Country" ?></th>
  <th><?php echo "Email" ?></th>
  </tr>
  <?php foreach ($list as $value) {

  ?>
  <tr>
  <td>
  <?php echo $value->username;  ?>
  </td>
  <td>
  <?php echo $value->country;  ?>
  </td>
  <td>
  <?php echo $value->email;  ?>
  </td>
  </tr>
  <?php  } ?>
  </table>
  <?php die();
  }

// login users list
  function show_login_users_list(){
  global $wpdb;
  $date = filter_input(INPUT_GET, 'tid', FILTER_SANITIZE_STRING);
  $access = filter_input(INPUT_GET, 'access', FILTER_SANITIZE_STRING);

  $list = $wpdb->get_results(" SELECT * from user where online_time LIKE  '%{$date}%' AND username!='Guest' and access='{$access}'");?>
  <table>
  <tr>
  <th><?php echo "User name" ?></th>
  <th><?php echo "Country" ?></th>
  <th><?php echo "Email" ?></th>
  </tr>
  <?php foreach ($list as $value) {

  ?>
  <tr>
  <td>
  <?php echo $value->username;  ?>
  </td>
  <td>
  <?php echo $value->country;  ?>
  </td>
  <td>
  <?php echo $value->email;  ?>
  </td>
  </tr>
  <?php  } ?>
  </table>
  <?php die();
  }


// show data
  function show_data(){

  $action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
  switch ($action) {

  case 'show_login_users_list':
  show_login_users_list();
  break;

  case 'show_signup_users_list':
  show_signup_users_list();
  break;


  default:
  }

  // show data on load
  ?>
  <table id="UserHookPerformance">
<thead>
<tr style="display:none;">
  <th></th><th></th>
  <th></th><th></th>
  <th></th><th></th>
  <th></th><th></th>
  </tr>
  </thead>
  <tbody>
  <?php
  show_users_signup();
  

  show_users_login();
  ?>
  </tbody>
</table>
<script>
jQuery('#UserHookPerformance').DataTable({
                dom: 'Bfrtip',
                "bFilter": false,
                "paging": false,
                "ordering": false,
                "bLengthChange": false,
                "language": {
                    "sInfo": ""
                },
                "buttons": [
                    {
                        'extend': 'excel',
                        'text': 'Download',
                        'filename': 'User Hook Performance',
                        'exportOptions': {
                            'modifier': {
                            }
                        }
                    }
                ]
            });
            
</script>
  <?php
  }






function show_users_signup(){

global $wpdb;
$dt_today= date('Y-m-d');
$dt_yesterday = date('Y-m-d',strtotime("-1 days"));

$dt_2 = date('Y-m-d',strtotime("-2 days"));
$dt_3 = date('Y-m-d',strtotime("-3 days"));
$dt_4 = date('Y-m-d',strtotime("-4 days"));
$dt_5 = date('Y-m-d',strtotime("-5 days"));
$dt_6 = date('Y-m-d',strtotime("-6 days"));
$dt_7 = date('Y-m-d',strtotime("-7 days"));


//for sellers signup

$today_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_today}%' AND username!='Guest' AND access=1");

$yesterday_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_yesterday}%' AND username!='Guest' AND access=1");

$two_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_2}%' AND username!='Guest' AND access=1");

$three_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_3}%' AND username!='Guest' AND access=1");

$four_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_4}%' AND username!='Guest' AND access=1");

$five_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_5}%' AND username!='Guest' AND access=1");

$six_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_6}%' AND username!='Guest' AND access=1");

$seven_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_7}%' AND username!='Guest' AND access=1");

// for buyers signup

$today_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_today}%' AND username!='Guest' AND access=2");

$yesterday_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_yesterday}%' AND username!='Guest' AND access=2");

$two_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_2}%' AND username!='Guest' AND access=2");

$three_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_3}%' AND username!='Guest' AND access=2");

$four_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_4}%' AND username!='Guest' AND access=2");

$five_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_5}%' AND username!='Guest' AND access=2");

$six_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_6}%' AND username!='Guest' AND access=2");

$seven_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE user_registered LIKE  '%{$dt_7}%' AND username!='Guest' AND access=2");



    ?>

<tr class="table-th"><td> Sellers Register </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>

  <tr>
    <td><?php echo $dt_7?></td>
    <td><?php echo $dt_6?></td>
    <td><?php echo $dt_5?></td>
    <td><?php echo $dt_4?></td>
    <td><?php echo $dt_3?></td>
    <td><?php echo $dt_2?></td>
    <td><?php echo $dt_yesterday?></td>
    <td><?php echo $dt_today ?></td>
  </tr>
  <tr>
  
      <td>
    <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_7,'access'=>1)); ?>" target="_blank"><?php echo ($seven_days_seller); ?> </a>
  </td>
    
    <td>
       <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_6,'access'=>1)); ?>" target="_blank"><?php echo ($six_days_seller); ?> </a>
    </td>
    <td>
       <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_5,'access'=>1)); ?>" target="_blank"><?php echo ($five_days_seller); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_4,'access'=>1)); ?>" target="_blank"><?php echo ($four_days_seller); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_3,'access'=>1)); ?>" target="_blank"><?php echo ($three_days_seller); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_3,'access'=>1)); ?>" target="_blank"><?php echo ($two_days_seller); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_yesterday,'access'=>1)); ?>" target="_blank"><?php echo ($yesterday_seller); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_today,'access'=>1)); ?>" target="_blank"><?php echo ($today_seller); ?> </a>
    </td>
  </tr>
  <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
  <tr class="table-th"><td> Buyers Register </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
  <tr>
    <td><?php echo $dt_7?></td>
    <td><?php echo $dt_6?></td>
    <td><?php echo $dt_5?></td>
    <td><?php echo $dt_4?></td>
    <td><?php echo $dt_3?></td>
    <td><?php echo $dt_2?></td>
    <td><?php echo $dt_yesterday?></td>
    <td><?php echo $dt_today ?></td>
   
  </tr>
  <tr>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_7,'access'=>2)); ?>" target="_blank"><?php echo ($seven_days_buyer); ?> </a>
    </td>
  
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_6,'access'=>2)); ?>" target="_blank"><?php echo ($six_days_buyer); ?> </a>
    </td>
  
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_5,'access'=>2)); ?>" target="_blank"><?php echo ($five_days_buyer); ?> </a>
    </td>
  
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_4,'access'=>2)); ?>" target="_blank"><?php echo ($four_days_buyer); ?> </a>
    </td>
    
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_3,'access'=>2)); ?>" target="_blank"><?php echo ($three_days_buyer); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_2,'access'=>2)); ?>" target="_blank"><?php echo ($two_days_buyer); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_yesterday,'access'=>2)); ?>" target="_blank"><?php echo ($yesterday_buyer); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_signup_users_list', 'tid' => $dt_today,'access'=>2)); ?>" target="_blank"><?php echo ($today_buyer); ?> </a>
    </td>
  </tr>
  <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>


<?php
}

function show_users_login(){

global $wpdb;
$dt_today= date('Y-m-d');
$dt_yesterday = date('Y-m-d',strtotime("-1 days"));

$dt_2 = date('Y-m-d',strtotime("-2 days"));
$dt_3 = date('Y-m-d',strtotime("-3 days"));
$dt_4 = date('Y-m-d',strtotime("-4 days"));
$dt_5 = date('Y-m-d',strtotime("-5 days"));
$dt_6 = date('Y-m-d',strtotime("-6 days"));
$dt_7 = date('Y-m-d',strtotime("-7 days"));


//for sellers signup

$today_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_today}%' AND username!='Guest' AND access=1");

$yesterday_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_yesterday}%' AND username!='Guest' AND access=1");

$two_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_2}%' AND username!='Guest' AND access=1");

$three_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_3}%' AND username!='Guest' AND access=1");

$four_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_4}%' AND username!='Guest' AND access=1");

$five_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_5}%' AND username!='Guest' AND access=1");

$six_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_6}%' AND username!='Guest' AND access=1");

$seven_days_seller = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_7}%' AND username!='Guest' AND access=1");

// for buyers signup

$today_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_today}%' AND username!='Guest' AND access=2");

$yesterday_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_yesterday}%' AND username!='Guest' AND access=2");

$two_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_2}%' AND username!='Guest' AND access=2");

$three_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_3}%' AND username!='Guest' AND access=2");

$four_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_4}%' AND username!='Guest' AND access=2");

$five_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_5}%' AND username!='Guest' AND access=2");

$six_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE online_time LIKE  '%{$dt_6}%' AND username!='Guest' AND access=2");

$seven_days_buyer = $wpdb->get_var("SELECT COUNT(*) FROM user WHERE  online_time LIKE  '%{$dt_7}%' AND username!='Guest' AND access=2");



    ?>

  <tr class="table-th"><td> Sellers Login </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
  <tr>
    <td><?php echo $dt_7?></td>
    <td><?php echo $dt_6?></td>
    <td><?php echo $dt_5?></td>
    <td><?php echo $dt_4?></td>
    <td><?php echo $dt_3?></td>
    <td><?php echo $dt_2?></td>
    <td><?php echo $dt_yesterday?></td>
    <td><?php echo $dt_today ?></td>
   
  </tr>
  <tr>
    <td>
    <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_7,'access'=>1)); ?>" target="_blank"><?php echo ($seven_days_seller); ?> </a>
  </td>
    <td>
<a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_6,'access'=>1)); ?>" target="_blank"><?php echo ($six_days_seller); ?> </a>
    </td>
    <td>
        <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_5,'access'=>1)); ?>" target="_blank"><?php echo ($five_days_seller); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_4,'access'=>1)); ?>" target="_blank"><?php echo ($four_days_seller); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_3,'access'=>1)); ?>" target="_blank"><?php echo ($three_days_seller); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_2,'access'=>1)); ?>" target="_blank"><?php echo ($two_days_seller); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_yesterday,'access'=>1)); ?>" target="_blank"><?php echo ($yesterday_seller); ?> </a>
  
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_today,'access'=>1)); ?>" target="_blank"><?php echo ($today_seller); ?> </a>
    </td>
  </tr>
  <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
  <tr class="table-th"><td> Buyers Login </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
  <tr>
    <td><?php echo $dt_7?></td>
    <td><?php echo $dt_6?></td>
    <td><?php echo $dt_5?></td>
    <td><?php echo $dt_4?></td>
    <td><?php echo $dt_3?></td>
    <td><?php echo $dt_2?></td>
    <td><?php echo $dt_yesterday?></td>
    <td><?php echo $dt_today ?></td>
   
  </tr>
  <tr>
    <td>
     <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_7,'access'=>2)); ?>" target="_blank"><?php echo ($seven_days_buyer); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_6,'access'=>2)); ?>" target="_blank"><?php echo ($six_days_buyer); ?> </a>
    </td>
    <td>
   <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_5,'access'=>2)); ?>" target="_blank"><?php echo ($five_days_buyer); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_4,'access'=>2)); ?>" target="_blank"><?php echo ($four_days_buyer); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_3,'access'=>2)); ?>" target="_blank"><?php echo ($three_days_buyer); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_2,'access'=>2)); ?>" target="_blank"><?php echo ($two_days_buyer); ?> </a>
    </td>
    <td>
     <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_yesterday,'access'=>2)); ?>" target="_blank"><?php echo ($yesterday_buyer); ?> </a>
    </td>
    <td>
      <a href="<?php echo $url . add_query_arg(array('action' => 'show_login_users_list', 'tid' => $dt_today,'access'=>2)); ?>" target="_blank"><?php echo ($today_buyer); ?> </a>
    </td>
  </tr>



<?php
}

?>
