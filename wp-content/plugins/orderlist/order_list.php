<?php

/**
 * Plugin Name: Order List
 * Version:     1.83
 * Author:      Lix
 * Description: Check the order list
 */

/*

 Updated: 19 July 2018 by farhan
 Added clicks details on tour score link

*/
register_activation_hook(__FILE__, 'order_list_activate');

function order_list_activate()
{
}

/***************************** show the order list at admin page  ******************************/

add_action('admin_menu', 'add_order_list_admin_menu');
add_action('admin_init', 'add_order_list_admin_menu_init');
add_action('wp_ajax_post_passed', 'post_passed');
add_action('wp_ajax_post_notpassed', 'post_notpassed');
add_action('wp_ajax_post_setpending', 'post_setpending');
function add_order_list_admin_menu()
{
    add_options_page('Order List', 'Order List', 'manage_options', __FILE__, 'show_order_list');
}

function add_order_list_admin_menu_init()
{
}

if(!function_exists('sendmail')) {
function sendmail($to, $subject, $message)
{
	$key="Tt3At58P6ZoYJ0qhLvqdYyx21";
	$postdata=array('to'=>$to,
					'subject'=>$subject,
					'message'=>$message);
	$url="https://www.tourfrombali.com/api.php?action=sendmail&key={$key}";
	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
	$data=curl_exec($ch);
	if(curl_errno($ch) || $data==FALSE)
	{
		curl_close($ch);
		return FALSE;
	}
	else
	{
		curl_close($ch);
		return TRUE;
	}
}
}

function post_passed()
{
    global $wpdb;
    $post_id = filter_input(INPUT_POST, 'post_id', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 999999)));
    if (!current_user_can('administrator'))
        wp_die(-1);
    else if ($post_id == false) {
        echo json_encode(array('error_msg' => 'Post id is invaild.'));
    } else {
        $post_status = get_post_status($post_id);
        if (empty($post_status)) {
            echo json_encode(array('error_msg' => 'Post is not existed.'));
        } else if ($post_status == 'publish') {
            echo json_encode(array('error_msg' => 'The post has been passed.'));
        } else if ($post_status == 'pending') {
            if (wp_update_post(array('ID' => $post_id, 'post_status' => 'publish'))) {

                if (get_post_meta($post_id, 'reward', true) != 1) {
                    $post_author_id = get_post($post_id)->post_author;
                    $user = get_user_by('id', $post_author_id);
                    $agent_username = $user->user_login;
                    $earn = get_post_meta($post_id, 'earn', true);
                    $urow = $wpdb->get_row("SELECT * FROM `user` WHERE `username`='{$agent_username}'");
                    $budget_used = $wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND type='publish'");
                    $budget_user_used = $wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE type = 'publish' AND DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND `userid`={$urow->userid}");
                    $reward_amount = floatval(get_option('reward_amount')) > 0 ? floatval(get_option('reward_amount')) : 0;
                    $tourid = get_post_meta($post_id, 'tour_id', true);
                    if ($earn == 1 && !empty($tourid)
                        && floatval($budget_used) < floatval(get_option('budget_limit'))
                        && floatval($budget_user_used) < floatval(get_option('budget_person_limit'))) {
                        update_post_meta($post_id, 'reward', 1);
                        if ($wpdb->query("INSERT INTO `rewards` (`userid`, `tourid`, `commission`, `type`, `create_time`) VALUES ('{$urow->userid}', '{$tourid}', '{$reward_amount}', 'publish', NOW())")) {
                            if (empty($wpdb->get_row("SELECT * FROM `balance` WHERE userid='{$urow->userid}'"))) {
                                $wpdb->query("INSERT INTO `balance` (`userid`, `balance`, `update_time`) VALUES ('{$urow->userid}', '{$reward_amount}', CURRENT_TIMESTAMP)");
                            } else {
                                $wpdb->query("UPDATE `balance` SET balance=balance+{$reward_amount}, update_time=CURRENT_TIMESTAMP where userid='{$urow->userid}'");
                            }

                        }
                    }
					
					$referral_budget_used=$wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND type='invite'");
                    if (!empty($tourid) && !empty($urow->inviteby) && floatval($referral_budget_used) < floatval(get_option('referral_budget_limit'))) {
                        if ($wpdb->get_var("SELECT COUNT(*) as publish_count FROM `rewards` WHERE userid='{$urow->userid}' AND type='publish'") == 1) {
                            $inviteby = $wpdb->get_row("SELECT * FROM `user` WHERE `username`='{$urow->inviteby}'");
                            $invite_wp_userid = get_user_by('login', $urow->inviteby)->ID;
							$enabledReferral=(get_user_meta($invite_wp_userid, 'can_referral', true)==1);
                            $budget_referral_user_used = $wpdb->get_var("SELECT SUM(commission) FROM `rewards` WHERE type = 'invite' AND DATE_FORMAT(create_time, '%Y%m')=DATE_FORMAT(CURDATE(), '%Y%m') AND `userid`={$invite_wp_userid}");
                            if ($enabledReferral && floatval($budget_referral_user_used) < floatval(get_option('budget_referral_person_limit'))) {
                                if (!empty($inviteby->userid)) {
                                    $invite_commisson = 5.0;
                                    /*if ($wpdb->get_var("SELECT COUNT(*) as publish_count FROM `rewards` WHERE userid='{$inviteby->userid}' AND type='invite'") < 5)
										$invite_commisson = 1.0;
                                    else
										$invite_commisson = 0.75;*/
                                    $wpdb->query("INSERT INTO `rewards` (`userid`, `tourid`, `commission`, `type`, `create_time`) VALUES ('{$inviteby->userid}', '{$tourid}', '{$invite_commisson}', 'invite', NOW())");
                                }
                                if (empty($wpdb->get_row("SELECT * FROM `balance` WHERE userid='{$inviteby->userid}'"))) {
                                    $wpdb->query("INSERT INTO `balance` (`userid`, `balance`, `update_time`) VALUES ('{$inviteby->userid}', '{$invite_commisson}', CURRENT_TIMESTAMP)");
                                } else {
                                    $wpdb->query("UPDATE `balance` SET balance=balance+{$invite_commisson}, update_time=CURRENT_TIMESTAMP where userid='{$inviteby->userid}'");
                                }
                            }
                        }
                    }
                }
				
				echo json_encode(array('msg' => 'Passed'));
				
				//send mail to the tour creator
				$post_author_id=get_post($post_id)->post_author;
				$user=get_user_by('id',$post_author_id);
				$mailto=$user->user_email;
				if(empty($user->display_name))
					$username=$user->nicename;
				else
					$username=$user->display_name;
				$tourid=get_post_meta($post_id,'tour_id',true);
				if(!empty($tourid))
					$bookingcode='BALI'.$tourid;
				else
					$bookingcode='';
				include(plugin_dir_path(__FILE__).'template/passed-notification.php');
				sendmail($mailto, 'Tour package notification', $passed_mail_content);
				
				//send email to the user who followed the creator
				if(get_post_meta($post_id, 'send_email_to_follow_client', true) != 1) {
					$tour_post=get_post($post_id, 'ARRAY_A');
					include("template/client-email.php");
					$client_emails=$wpdb->get_results("SELECT user.email FROM social_follow,wp_users,user WHERE sf_agent_id='{$tour_post['post_author']}' AND sf_user_id=wp_users.ID AND wp_users.user_login=user.username AND user.access=2");
					foreach($client_emails as $row)
						wp_mail($row->email,'A new tour for you',$mail_content);
					update_post_meta($post_id, 'send_email_to_follow_client', 1);
				}
            } else {
                echo json_encode(array('error_msg' => 'The system is busy. Please try again.'));
            }
        } else {
            echo json_encode(array('error_msg' => 'The post can\'t been passed. '));
        }
    }
    wp_die();
}

function post_notpassed()
{
	$post_id=filter_input(INPUT_POST, 'post_id', FILTER_VALIDATE_INT, array('options'=>array('min_range'=>1,'max_range'=>999999)));
	if( !current_user_can('administrator') )
		wp_die( -1 );
	else if($post_id==false) {
		echo json_encode(array('error_msg'=>'Post id is invaild.'));
	}
	else {
		$post_status=get_post_status($post_id);
		if(empty($post_status)) {
			echo json_encode(array('error_msg'=>'Post is not existed.'));
		}
		else if($post_status=='publish') {
			echo json_encode(array('error_msg'=>'The post has been passed.'));
		}
		else if($post_status=='pending') {
			if( wp_update_post(array('ID'=>$post_id, 'post_status'=>'draft')) ) {
				$post_author_id=get_post($post_id)->post_author;
				$user=get_user_by('id',$post_author_id);
				$mailto=$user->user_email;
				if(empty($user->display_name))
					$username=$user->nicename;
				else
					$username=$user->display_name;
				$tourid=get_post_meta($post_id,'tour_id',true);
				if(!empty($tourid))
					$bookingcode='BALI'.$tourid;
				else
					$bookingcode='';
				include(plugin_dir_path(__FILE__).'template/notpassed-notification.php');
				sendmail($mailto, 'Tour package notification', $mail_content);
				echo json_encode(array('msg'=>'Not Passed'));
			}
			else {
				echo json_encode(array('error_msg'=>'The system is busy. Please try again.'));
			}
		}
		else {
			echo json_encode(array('error_msg'=>'The post can\'t been set to not passed. '));
		}
	}
	wp_die();
}

function post_setpending()
{
	$post_id=filter_input(INPUT_POST, 'post_id', FILTER_VALIDATE_INT, array('options'=>array('min_range'=>1,'max_range'=>999999)));
	if( !current_user_can('administrator') )
		wp_die( -1 );
	else if($post_id==false) {
		echo json_encode(array('error_msg'=>'Post id is invaild.'));
	}
	else {
		$post_status=get_post_status($post_id);
		if(empty($post_status)) {
			echo json_encode(array('error_msg'=>'Post is not existed.'));
		}
		else if($post_status!='publish') {
			echo json_encode(array('error_msg'=>'The post has not been published.'));
		}
		else {
			if( wp_update_post(array('ID'=>$post_id, 'post_status'=>'pending')) ) {
				echo json_encode(array('msg'=>'Set to pending successfully.'));
			}
			else {
				echo json_encode(array('error_msg'=>'The system is busy. Please try again.'));
			}
		}
	}
	wp_die();
}

 function show_click_details()
 {
 	global $wpdb;
 	 $tour_id = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 9999)));


 	if(!empty($tour_id))	{


 $post_id=$wpdb->get_row("SELECT post_id FROM `wp_postmeta` WHERE `meta_key`='tour_id' AND `meta_value`='{$tour_id}'")->post_id;

$query_to_get_total ="SELECT SUM(clicks) as total FROM wp_tour_clicks WHERE tour_id ='$tour_id'";

$sum_of_clicks = $wpdb->get_var($query_to_get_total);

$likecount = $wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$tour_id}' AND `sc_type` = 0");

$dislikecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$tour_id}' AND `sc_type` = 1");

$tourCommentData = $wpdb->get_var("SELECT COUNT(*) FROM `social_comments`  WHERE scm_tour_id = '$tour_id'");

$shared_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_tour_sharing_points` WHERE  tour_id='{$tour_id}'");

$views  = number_format(getPostViews($post_id));

 echo "<h2> Tour Score Details</h2>";

//echo (empty($post_id) ? 'Null' : $post_id);
 
 echo "<p> Total clicks: ";
 echo (empty($sum_of_clicks) ? 'Null' : $sum_of_clicks);
 echo "</p>";
 echo "<p> Number of Likes: ";
 echo (empty($likecount) ? 'Null' : $likecount);
 echo "</p>";
 echo "<p> Number of Dis-Likes: ";
 echo (empty($dislikecount) ? 'Null' : $dislikecount);
 echo "</p>";
 echo "<p> Number of Comments  ";
 echo (empty($tourCommentData) ? 'Null' : $tourCommentData);
 echo "</p>";
 echo "<p> Number of Shares: ";
 echo (empty($shared_count) ? 'Null' : $shared_count);
 echo "</p>";
 echo "<p> Number of Views: ";
 echo (empty($views) ? 'Null' : $views);
 echo "</p>";

  }

 	die();
 }
function show_order_list()
{
	
    $action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
    switch ($action) {

        case 'show_click_details':
      		  show_click_details();
            break;

        default:
}
	global $wpdb;
    ?>
    <table id="orderList" style="text-align:left;">
		<thead>
        <tr>
            <th>Agent's name</th> <th>Booking Code</th> <th>Price(Rp)</th>
			<th>Complete Tour Details Page</th> <th>Verification</th> <th>Paid/Not Paid</th>
			<th>Live chat Clicked</th> <th>Tour Score</th>
			<th>Likers</th> <th>Dislikers</th> <th>Viewers</th>
			<th>Shared Count</th>
			<th>Deleted</th>
        </tr>
		</thead>
		<tbody>
		<?php
			$list=$wpdb->get_results("SELECT wp_tour.id as tour_id, wp_tour.confirm_payment,wp_tour.total,wp_tour.score,wp_users.user_login,wp_tour_sale.number_of_people as published
										FROM wp_users,user,wp_tour,wp_tourmeta
										LEFT JOIN wp_tour_sale ON wp_tourmeta.tour_id=wp_tour_sale.tour_id
										WHERE access=1 AND wp_users.user_login=user.username AND wp_tourmeta.tour_id=wp_tour.id AND wp_tourmeta.meta_key='userid' AND wp_tourmeta.meta_value=wp_users.ID");
			if(count($list)>0)
			{
				foreach($list as $row)
				{
					$post_id=$wpdb->get_row("SELECT post_id FROM `wp_postmeta` WHERE `meta_key`='tour_id' AND `meta_value`='{$row->tour_id}'")->post_id;
					if(!empty($post_id)) {
						$post_status=get_post_status($post_id);
					}
					else
						$post_status='';
					//Get the count of like and dislike
					$likecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$row->tour_id}' AND `sc_type` = 0");
					$dislikecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$row->tour_id}' AND `sc_type` = 1");
					$last_data = $wpdb->get_row("SELECT * FROM `wp_live_chat_clicks` WHERE `tour_id` = '{$row->tour_id}'");
					if (!empty($last_data))
					{
					$click_count_for_chat = $last_data->clicks;  
					}
					else
					{
						$click_count_for_chat=0;
					}
					$shared_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_tour_sharing_points` WHERE  tour_id='{$row->tour_id}'");
					?>
				<tr>
					<td><?php echo $row->user_login; ?></td>
					<td> <a href="<?php echo site_url()."/tour-details/?bookingcode=BALI{$row->tour_id}"; ?>" target=_blank><?php echo "BALI{$row->tour_id}"; ?></a> </td>
					<td><?php echo $row->total; ?></td>
					<td>
						<?php if(!empty($post_id)) { ?>
						<a href="<?php echo home_url()."/?p={$post_id}&preview=true"; ?>" target=_blank>Click to Check</a>
						<?php } else { ?>
						Click to Check
						<?php } ?>
					</td>
					<td>
						<?php if(!empty($post_status) && $post_status=='publish') { ?>
						<!--<span>Passed</span>
						<a class="btn btn-primary" style="margin-left: 12px;padding: 3px;">Submit again</a>-->
						<select class="postStatus" post_id="<?php echo $post_id; ?>">
							<option value="Pending">Pending</option>
							<option value="Passed" selected="selected">Passed</option>
						</select>
						<?php } else if(!empty($post_status) && $post_status=='draft') { ?>
						Not Passed
						<?php } else if(!empty($post_status) && $post_status=='pending') { ?>
						<select class="postStatus" post_id="<?php echo $post_id; ?>">
							<option value="Pending">Pending</option>
							<option value="Passed">Passed</option>
							<option value="NotPassed">Not Passed</option>
						</select>
						<?php } else if(empty($post_status)) { ?>
						Post is not existed.
						<?php } ?>
						<?php if(empty($row->published)) { ?>
						<span>(Wait to be published)</span>
						<?php } ?>
					</td>
					<td>
					<?php echo ($row->confirm_payment==1)?'Paid':'Not Paid'; ?>
					</td>

					<td>
					<?php echo $click_count_for_chat; ?>
					</td>
					<td>
					<a href="<?php echo $url . add_query_arg(array('action' => 'show_click_details', 'tid' => $row->tour_id)); ?>" target="_blank">
					<?php echo ($row->score); ?>
				    </a>
					</td>

					<td>
					<?php echo $likecount; ?>
					</td>
					<td>
					<?php echo $dislikecount; ?>
					</td>
					<td>
					<?php echo number_format(getPostViews($post_id)); ?>
					</td>
					<td>
					<?php echo $shared_count; ?>
					</td>
					<td></td>
				</tr>
			<?php }
			}
		?>
		<?php
			//Show deleted tour
			$list=$wpdb->get_results("SELECT wp_tour.id as tour_id, wp_tour.confirm_payment,wp_tour.total,wp_tour.score FROM wp_tour,wp_tourmeta
										WHERE wp_tourmeta.tour_id=wp_tour.id AND wp_tourmeta.meta_key='userid' AND wp_tourmeta.meta_value='-1'");
			foreach($list as $row) {
				$creator=$wpdb->get_var("SELECT wp_users.user_login FROM wp_users,user,wp_tourmeta WHERE access=1 AND wp_users.user_login=user.username AND wp_tourmeta.tour_id='{$row->tour_id}' AND wp_tourmeta.meta_key='original_userid' AND wp_tourmeta.meta_value=wp_users.ID");
				$post_id=$wpdb->get_row("SELECT post_id FROM `wp_postmeta` WHERE `meta_key`='tour_id' AND `meta_value`='{$row->tour_id}'")->post_id;
				if(!empty($post_id)) {
					$post_status=get_post_status($post_id);
				}
				else
					$post_status='';
				//Get the count of like and dislike
				$likecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$row->tour_id}' AND `sc_type` = 0");
				$dislikecount=$wpdb->get_var("SELECT COUNT(*) FROM `social_connect` WHERE `sc_tour_id` = '{$row->tour_id}' AND `sc_type` = 1");
				$last_data = $wpdb->get_row("SELECT * FROM `wp_live_chat_clicks` WHERE `tour_id` = '{$row->tour_id}'");
				if (!empty($last_data))
				{
					$click_count_for_chat = $last_data->clicks;  
				}
				else
				{
					$click_count_for_chat=0;
				}
				$shared_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_tour_sharing_points` WHERE  tour_id='{$row->tour_id}'");
				?>
				<tr>
					<td><?php echo empty($creator)?'N/A':$creator; ?></td>
					<td> <a href="<?php echo site_url()."/tour-details/?bookingcode=BALI{$row->tour_id}"; ?>" target=_blank><?php echo "BALI{$row->tour_id}"; ?></a> </td>
					<td><?php echo $row->total; ?></td>
					<td>
						<?php if(!empty($post_id)) { ?>
						<a href="<?php echo home_url()."/?p={$post_id}&preview=true"; ?>" target=_blank>Click to Check</a>
						<?php } else { ?>
						Click to Check
						<?php } ?>
					</td>
					<td>
						<?php if(!empty($post_status) && $post_status=='publish') { ?>
						<!--<span>Passed</span>
						<a class="btn btn-primary" style="margin-left: 12px;padding: 3px;">Submit again</a>-->
						<select class="postStatus" post_id="<?php echo $post_id; ?>">
							<option value="Pending">Pending</option>
							<option value="Passed" selected="selected">Passed</option>
						</select>
						<?php } else if(!empty($post_status) && $post_status=='draft') { ?>
						Not Passed
						<?php } else if(!empty($post_status) && $post_status=='pending') { ?>
						<select class="postStatus" post_id="<?php echo $post_id; ?>">
							<option value="Pending">Pending</option>
							<option value="Passed">Passed</option>
							<option value="NotPassed">Not Passed</option>
						</select>
						<?php } else if(empty($post_status)) { ?>
						Post is not existed.
						<?php } ?>
					</td>
					<td>
					<?php echo ($row->confirm_payment==1)?'Paid':'Not Paid'; ?>
					</td>

					<td>
					<?php echo $click_count_for_chat; ?>
					</td>
					<td>
					<a href="<?php echo $url . add_query_arg(array('action' => 'show_click_details', 'tid' => $row->tour_id)); ?>" target="_blank">
					<?php echo ($row->score); ?>
				    </a>
					</td>

					<td>
					<?php echo $likecount; ?>
					</td>
					<td>
					<?php echo $dislikecount; ?>
					</td>
					<td>
					<?php echo number_format(getPostViews($post_id)); ?>
					</td>
					<td>
					<?php echo $shared_count; ?>
					</td>
					<td>Deleted</td>
				</tr>
				<?php
			}
		?>
		</tbody>
    </table>
	
	<script>
	jQuery(document).ready(function() {
		var postStatusitem;
		var postId;
		jQuery('#orderList td').on('change','.postStatus',function() {
			postStatusitem=jQuery(this);
			postId=jQuery(this).attr('post_id');
			if(jQuery(this).val()=='Passed') {
				jQuery(this).hide();
				jQuery(this).parent().append('Loading');
				var data = {
					'action': 'post_passed',
					'post_id': jQuery(this).attr('post_id')
					};
				jQuery.post(ajaxurl, data, function(response) {
					if(response.error_msg!=undefined && response.error_msg.length>1)
						postStatusitem.parent().text('Error:'+response.error_msg);
					else {
						//postStatusitem.parent().text('Passed');
						postStatusitem.parent().html('<select class="postStatus" post_id="'+postId+'">\
							<option value="Pending">Pending</option>\
							<option value="Passed" selected="selected">Passed</option>\
						</select>');
					}
				},'json');
			}
			else if(jQuery(this).val()=='NotPassed') {
				jQuery(this).hide();
				jQuery(this).parent().append('Loading');
				var data = {
					'action': 'post_notpassed',
					'post_id': jQuery(this).attr('post_id')
					};
				jQuery.post(ajaxurl, data, function(response) {
					if(response.error_msg!=undefined && response.error_msg.length>1)
						postStatusitem.parent().text('Error:'+response.error_msg);
					else
						postStatusitem.parent().text('Not Passed');
				},'json');
			}
			else if(jQuery(this).val()=='Pending') {
				jQuery(this).hide();
				jQuery(this).parent().append('Loading');
				var data = {
					'action': 'post_setpending',
					'post_id': jQuery(this).attr('post_id')
					};
				jQuery.post(ajaxurl, data, function(response) {
					if(response.error_msg!=undefined && response.error_msg.length>1)
						postStatusitem.parent().text('Error:'+response.error_msg);
					else {
						postStatusitem.parent().html('<select class="postStatus" post_id="'+postId+'">\
							<option value="Pending">Pending</option>\
							<option value="Passed">Passed</option>\
							<option value="NotPassed">Not Passed</option>\
						</select>');
					}
				},'json');
			}
		});
		
		jQuery('#orderList').DataTable( {
			dom: 'Bfrtip',
			pageLength: 15,
			ordering: true,
			"search": {
				"caseInsenstive":false
			},
			"buttons": [
				{
					'extend': 'excelHtml5',
					'text': 'Download',
					'exportOptions': {
						'modifier': {
						}
					}
				}
			]
		});
	});
	</script>
	
    <?php
}
?>
