<?php
$mail_content=<<<EOD
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
</head>

<body>

<div style="margin: 0 auto;border-radius: 3px;  background-color: #deeff0;max-width: 600px;padding-top: 30px;padding-left: 10px; padding-right: 10px;padding-bottom: 25px;">

    <div class="mail-template">

        <table cellspacing="0" border="0" cellpadding="0" style="width: 100%;">

            <tr>

                <td style="text-align: center;padding-bottom: 18px;" align="center"><a href="https://www.travpart.com/English/"><img style="border: none;" src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/logo.jpg" width="232" height="69" alt="www.travpart.com"></a></td>

            </tr>

            <tr>

                <td style="background-color: #fff;padding: 60px 40px;">

                    <table cellspacing="0" border="0" cellpadding="0" style="width: 100%;">

                        <tr>

                            <td style="font-family: OpenSans;font-size: 18px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.7;  letter-spacing: -1.4px;text-align: left;  color: #42454e;padding-bottom: 0px;">Dear our partner,<br>

                                <strong style="font-weight: 600;">Travel advisor {$username}</strong><br>

                                <br>

                                We are contacting you regarding the tour package you've created with <stong style="font-weight: 600;">booking code {$bookingcode}</stong><br>

                                <br>

                                Thank you for creating the tour package in our travpart platform. However we found the content within the complete tour details section was not written according to a standardized tour package guideline. Since the more details and pictures/video uploaded which described about the tour package will attract more buyers to your tour package. Therefore; we advise you to rewrite your complete tour detail page in the following standardized example format guideline :<br>

                                <br>

                                <span style="font-size: 24px;">Day 1</span><br>

                                Visit Uluwatu Temple at 09:00<br>

                                - Bali Cultural Heritage Temple<br>

                                Lunch at Indonesian Restaurant at 12:00<br>

                                Rest in Hotel Jambu, Bali Area at 20:00<br>

                                <br>

                                <span style="font-size: 24px;">Day 2</span><br>

                                Depart to Kuta Beach at 08:00 from your hotel.<br>

                                Snorkeling at Kuta Beach at 09:00<br>

                                - View the great oceanic coral reef<br>

                                Dinner at American Restaurant at 18:00<br>

                                Rest in Hotel Pullman, Kuta Beach Area at 20:00<br>

                                We hope this is an useful information.<br>

                                <br>
								
								You may refer to this tour package page's design <a href="https://www.travpart.com/English/bali-2-days-1-night/" target="_blank">BALI1870</a> or <a href="https://docs.google.com/spreadsheets/d/17hkAnAgW9YntPpRhsGL0jjUMGT6moCJtrLuU0KP9u6M/edit?pli=1#gid=0" target="_blank">click here</a> ; whereas you are advised to insert as many details and pictures/video as possible so we could bring maximum traffic for you. Please take note that you may not replicate the guideline example above to your tour package and you are not allowed to insert any contact information that will drives away your potential buyers from your page without purchasing in our platform.
								
								<br>

                                If you have any other questions, please don't hesitate to contact us at <a href="#" style="color: #61cbc9!important;text-decoration: none!important;"> contact@tourfrombali.com</a><br>

                                <br>

                                Sincerely,<br>

                                Travpart crew



                            </td>

                        </tr>



                    </table>

                </td>

            </tr>

            <tr>

                <td style="font-family: OpenSans;font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;padding-top: 30px;">Download our mobile app on</td>

            </tr>

            <tr>

                <td style="font-family: OpenSans;font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;padding-bottom: 40px;"><a href="https://www.travpart.com/English/download" style="color: #66cccc;text-decoration: none!important;">iPhone</a>  or <a href="https://www.travpart.com/English/download" style="color: #66cccc;text-decoration: none!important;">Android</a> </td>

            </tr>

            <tr>

                <td style="padding-bottom: 40px;text-align: center" align="center">

                    <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/1.jpg" width="27" height="24"></a> <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/2.jpg" width="27" height="24"></a> <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/3.jpg" width="27" height="24"></a> <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/4.jpg" width="27" height="24"></a>

                </td>

            </tr>

            <tr>

                <td style="font-family: OpenSans;font-size: 14px;font-weight: normal;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;">&copy; 2018 Travpart Inc.</td>

            </tr>

        </table>

    </div>

</div>
</body>
</html>
EOD;
?>