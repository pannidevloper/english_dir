<?php
	/*
		Plugin Name: People Search API
		Description: Performs searches against user locations
		Version: 1.2
		Author: Lionel
	*/

/**
 * Performs a search for people nearby
 */
function people_search_nearby() {
	global $wpdb;

	// Results array
	$results = array();

	// Get the user ID and distance query
	$userId = get_current_user_id();
	$distance = $_REQUEST['distance'];

	// Fetch the current user location
	$query = $wpdb->prepare("SELECT lat, lng FROM user_location WHERE ID = %d", $userId);
	$rows = $wpdb->get_results($query, ARRAY_A);

	// User details placeholders
	$lat = 0.0;
	$lng = 0.0;

	// Check for results
	if ($wpdb->num_rows > 0) {
		$lat = $rows[0]['lat'];
		$lng = $rows[0]['lng'];

		$query = $wpdb->prepare("SELECT ID, (6371 * ACOS(COS(RADIANS(%f)) * COS(RADIANS(lat)) * "
			. "COS(RADIANS(lng) - RADIANS(%f)) + SIN(RADIANS(%f)) * SIN(RADIANS(lat)))) AS distance FROM "
			. "user_location WHERE ID <> %d HAVING distance < %d ORDER BY distance", $lat, $lng, $lat, $userId, 
			$distance);
		$rows = $wpdb->get_results($query, ARRAY_A);

		foreach ($rows as $row) {
			// Fetch the user details
			$query = $wpdb->prepare("SELECT username, region, photo, is_active FROM user WHERE userid = %d",
				$row['ID']);
			$inner_rows = $wpdb->get_results($query, ARRAY_A);
			if (!empty($inner_rows)) {
				$results[] = array('userid' => $row['ID'], 'username' => $inner_rows[0]['username'],
					'hometown' => $inner_rows[0]['region'], 'photo' => $inner_rows[0]['photo'],
					'status' => $inner_rows[0]['is_active'], 'distance' => $row['distance']);
			}
		}
	}

	echo json_encode(array("result" => $results));

	wp_die();
}

/**
 * Performs a filtered search for people
 */
function people_search_filter() {
	global $wpdb;

	// Results array
	$results = array();

	// Get the user ID and distance query
	$userId = get_current_user_id();
	$distance = filter_input(INPUT_POST, 'distance', FILTER_VALIDATE_INT, array('options'=>array('default'=>0, 'min_range'=>0)));
	$hometown = filter_input(INPUT_POST, 'hometown', FILTER_SANITIZE_STRING);
	$age_min = filter_input(INPUT_POST, 'age_min', FILTER_VALIDATE_INT, array('options'=>array('default'=>17, 'min_range'=>17, 'max_range'=>65)));
	$age_max = filter_input(INPUT_POST, 'age_max', FILTER_VALIDATE_INT, array('options'=>array('default'=>65, 'min_range'=>17, 'max_range'=>65)));
	$gender = filter_input(INPUT_POST, 'gender', FILTER_SANITIZE_STRING);
	$gender = (strtolower($gender)=='female')?'Female':'Male';
	$marital_status = strtolower(filter_input(INPUT_POST, 'marital_status', FILTER_SANITIZE_STRING));
	$marital_status = empty($marital_status)?'single':$marital_status;
	$star_min = filter_input(INPUT_POST, 'star_min', FILTER_VALIDATE_INT, array('options'=>array('default'=>0, 'min_range'=>0, 'max_range'=>5)));
	/*$status = $_REQUEST['status'];
	$minLikes = $_REQUEST['min_likes'];
	$maxLikes = $_REQUEST['max_likes'];
	$minDislikes = $_REQUEST['min_dislikes'];
	$maxDislikes = $_REQUEST['max_dislikes'];
	$minFollowers = $_REQUEST['min_followers'];
	$maxFollowers = $_REQUEST['max_followers'];
	$minViewers = $_REQUEST['min_viewers'];
	$maxViewers = $_REQUEST['max_viewers'];*/

	// Fetch the current user location
	$query = $wpdb->prepare("SELECT lat, lng FROM user_location WHERE ID = %d", $userId);
	$rows = $wpdb->get_row($query, ARRAY_A);

	// User details placeholders
	$lat = 0.0;
	$lng = 0.0;

	// Check for results
	if ($wpdb->num_rows > 0) {
		$lat = $rows['lat'];
		$lng = $rows['lng'];

		$baseQuery = "SELECT wp_users.ID as wp_user_id, userid, (6371 * ACOS(COS(RADIANS({$lat})) * COS(RADIANS(lat)) * "
			. "COS(RADIANS(lng) - RADIANS({$lng})) + SIN(RADIANS({$lat})) * SIN(RADIANS(lat)))) AS distance, "
			. "is_active AS status, username, likes, dislikes, followers, viewers FROM user_location,wp_users,user "
			. "WHERE user_location.ID = wp_users.ID AND wp_users.user_login=user.username AND wp_users.ID <> {$userId} ";
		$filterQuery = "";
		if (!empty($hometown))
			$filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'hometown' AND `meta_value` LIKE '%{$hometown}%')";
		/*if (!empty($status))
			$filterQuery .= " AND is_active = " . intval($status);
		if (!empty($minLikes))
			$filterQuery .= " AND likes > " . intval($minLikes);
		if (!empty($maxLikes))
			$filterQuery .= " AND likes < " . intval($maxLikes);
		if (!empty($minDislikes))
			$filterQuery .= " AND dislikes > " . intval($minDislikes);
		if (!empty($maxDislikes))
			$filterQuery .= " AND dislikes < " . intval($maxDislikes);
		if (!empty($minFollowers))
			$filterQuery .= " AND followers > " . intval($minFollowers);
		if (!empty($maxFollowers))
			$filterQuery .= " AND followers < " . intval($maxFollowers);
		if (!empty($minViewers))
			$filterQuery .= " AND viewers > " . intval($minViewers);
		if (!empty($maxViewers))
			$filterQuery .= " AND viewers < " . intval($maxViewers);*/
		
		$filterQuery .= " AND wp_users.ID IN(
			SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'date_of_birth'
			AND TIMESTAMPDIFF(YEAR, meta_value, CURDATE()) BETWEEN {$age_min} AND {$age_max}
		)";
		$filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'gender' AND `meta_value`='{$gender}')";
		$filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `wp_usermeta` WHERE `meta_key` = 'marital_status' AND `meta_value`='{$marital_status}')";
		$filterQuery .= " AND wp_users.ID IN(SELECT user_id FROM `social_reviews` GROUP BY user_id HAVING(AVG(rating)>={$star_min}))";

		if ($distance>0)
			$filterQuery .= " HAVING(distance<{$distance})";
		

		//$query = $wpdb->prepare($baseQuery . $filterQuery . " ORDER BY distance", $lat, $lng, $lat, $userId, "%cheng%");
		$query = $baseQuery . $filterQuery . " ORDER BY distance";
		$rows = $wpdb->get_results($query, ARRAY_A);

		foreach ($rows as $row) {
			// Fetch the user details
			$results[] = array(
				'userid' => $row['userid'], 'username' => $row['username'],
				'hometown' => get_user_meta($row['wp_user_id'],'hometown',true),
				'photo' => um_get_user_avatar_data($row['wp_user_id'])['url'],
				'status' => $row['is_active'], 'distance' => $row['distance'],
				'likes' => $row['likes'], 'dislikes' => $row['dislikes'],
				'followers' => $row['followers'], 'viewers' => $row['viewers']
			);
		}
	}

	echo json_encode(array("result" => $results));

	wp_die();
}

/**
 * Returns invitations for user
 */
function people_invitations() {
	global $wpdb;

	// Results array
	$results = array();

	// Get the user ID
	$userId = get_current_user_id();

	// Fetch the invitations
	$query = $wpdb->prepare("SELECT user1 FROM `friends_requests` WHERE status=0 AND user2=%d", $userId);
	$rows = $wpdb->get_results($query, ARRAY_A);

	foreach ($rows as $row) {
		$results[] = array(
			'userid' => $row['user1'],
			'username' => get_user_by('id', $row['user1'])->user_login,
			'name' => um_get_display_name($row['user1']),
			'hometown' => get_user_meta($row['user1'], 'hometown', true),
			'photo' => um_get_user_avatar_url($row['user1']),
		);
	}
	echo json_encode(array("result" => $results));

	wp_die();
}

/**
 * Connect to an invited user
 */
function people_invitations_connect() {
	global $wpdb;

	// Results array
	$results = array();

	// Get the user ID
	$userId = get_current_user_id();
	$personId = $_REQUEST['person_id'];

	// Fetch the invitations
	$query = $wpdb->prepare("UPDATE friends_requests SET action_by_user = %d, status = 1 WHERE status = 0 AND user1 = %d AND user2 = %d", $userId, $personId, $userId);

	$outcome = intval($wpdb->query($query));

	if ($outcome > 0) {
		$results[] = array('success' => true);
	} else {
		$results[] = array('success' => false);		
	}

	echo json_encode(array("result" => $results));

	wp_die();
}

/**
 * Disconnect from an invited user
 */
function people_invitations_disconnect() {
	global $wpdb;

	// Results array
	$results = array();

	// Get the user ID
	$userId = get_current_user_id();
	$personId = $_REQUEST['person_id'];

	// Fetch the invitations
	$query = $wpdb->prepare("UPDATE friends_requests SET action_by_user = %d, status = 0 WHERE status = 1 AND user1 = %d AND user2 = %d", $userId, $userId, $personId);
	$outcome = intval($wpdb->query($query));

	if ($outcome > 0) {
		$results[] = array('success' => true);
	} else {
		$results[] = array('success' => false);		
	}

	echo json_encode(array("result" => $results));

	wp_die();
}

/**
 * Returns people suggestions
 */
function people_suggestions()
{
	global $wpdb;
	
	$paged = filter_input(INPUT_GET, 'paged', FILTER_VALIDATE_INT, array('options'=>array('default'=>1, 'min_range'=>1)));

    $user_id = get_current_user_id();
    $edu_level = get_user_meta($user_id, 'edu_level', true);
    $religion = get_user_meta($user_id, 'religion', true);
    $hometown = get_user_meta($user_id, 'hometown', true);
    $hobby = get_user_meta($user_id, 'hobby', true);
    $seeking = get_user_meta($user_id, 'seeking', true);
    $political_view = get_user_meta($user_id, 'political_view', true);

    $exclude_row = $wpdb->get_results("SELECT ID FROM `wp_users` WHERE ID IN(
		SELECT user2 FROM `friends_requests` WHERE user1='{$user_id}')
		OR ID IN(
			SELECT user1 FROM `friends_requests` WHERE user2='{$user_id}')", ARRAY_A);
    foreach ($exclude_row as $r) {
        $exclude[] = $r['ID'];
    }
    $exclude[] = $user_id;

    $args = array(
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key'     => 'edu_level',
                'value'   => $edu_level,
                'compare' => '='
            ),
            array(
                'key'     => 'hometown',
                'value'   => $hometown,
                'compare' => '='
            ),
            array(
                'key'     => 'hobby',
                'value'   => $hobby,
                'compare' => 'LIKE'
            ), array(
                'key'     => 'seeking',
                'value'   => $seeking,
                'compare' => 'LIKE'
            ),
            array(
                'key'     => 'political_view',
                'value'   => $political_view,
                'compare' => 'LIKE'
            )
        ),
		'exclude' => $exclude,
		'number'  => 10,
		'paged'   => $paged,
    );

    $wp_user_query = new WP_User_Query($args);
    $users = $wp_user_query->get_results();
    $results = array();

    foreach ($users as $user) {
        // Fetch the user data
        $query = $wpdb->prepare("SELECT username, region, photo, is_active FROM user WHERE username = %s", $user->user_login);
        $rows = $wpdb->get_results($query, ARRAY_A);

        foreach ($rows as $row) {
            $results[] = array(
				'userid' => $user->ID,
				'username' => um_get_display_name($user->ID),
				'hometown' => $row['region'],
				'photo' => um_get_user_avatar_url($user->ID),
				'status' => $row['is_active']
            );
        }
    }

    echo json_encode(array("result" => $results));

    wp_die();
}

/**
 * Connect to a user
 */
function people_connect() {
	global $wpdb;

	// Results array
	$results = array();

	// Get the user ID
	$userId = get_current_user_id();
	$personId = intval($_REQUEST['person_id']);

	$request_count = $wpdb->get_var("SELECT COUNT(*) FROM `friends_requests` WHERE (user1={$userId} AND user2={$personId}) OR (user1={$personId} AND user2={$userId})");

	if($request_count==0) {
		$wpdb->insert('friends_requests',
			array('user1'=>$userId, 'user2'=>$personId, 'action_by_user'=>$userId)
		);
		$results[] = array('success' => true);
	}
	else if($wpdb->get_var("SELECT COUNT(*) FROM `friends_requests` WHERE user1={$personId} AND user2={$userId} AND status=0")==1) {
		$wpdb->update('friends_requests',
			array('status'=>1),
			array('user1'=>$personId, 'user2'=>$userId)
		);
		$results[] = array('success' => true);
	}
	else {
		$results[] = array('success' => false, 'msg'=>'You have sent the request.');
	}

	echo json_encode(array("result" => $results));

	wp_die();
}

/**
 * Disconnects from a user
 */
function people_disconnect() {
	global $wpdb;

	// Results array
	$results = array();

	// Get the user ID
	$userId = get_current_user_id();
	$personId = $_REQUEST['person_id'];

	// Remove the invitations
	$query = $wpdb->prepare("DELETE FROM friends_requests WHERE user1 = %d AND user2 = %d", 
		$userId, $personId);
	$outcome = intval($wpdb->query($query));
	$query = $wpdb->prepare("DELETE FROM friends_requests WHERE user1 = %d AND user2 = %d", 
		$personid, $userId);
	$outcome = $outcome + intval($wpdb->query($query));

	if ($outcome > 0) {
		$results[] = array('success' => true);
	} else {
		$results[] = array('success' => false);		
	}

	echo json_encode(array("result" => $results));

	wp_die();
}

/**
 * Returns contacts for user
 */
function people_contacts() {
	global $wpdb;

	// Results array
	$results = array();

	// Get the user ID
	$userId = get_current_user_id();

	// Fetch the contacts
	$query = $wpdb->prepare("SELECT user1, username, region, photo, is_active FROM friends_requests 
		LEFT JOIN user ON (friends_requests.user1 = user.userid) WHERE status = 1 AND user2 = %d", $userId);
	$rows = $wpdb->get_results($query, ARRAY_A);

	foreach ($rows as $row) {
		$results[] = array('userid' => $row['user1'], 'username' => $row['username'],
			'hometown' => $row['region'], 'photo' => $row['photo'], 'is_active' => $row['is_active']);
	}

	$query = $wpdb->prepare("SELECT user2, username, region, photo, is_active FROM friends_requests 
		LEFT JOIN user ON (friends_requests.user2 = user.userid) WHERE status = 1 AND user1 = %d", $userId);
	$rows = $wpdb->get_results($query, ARRAY_A);

	foreach ($rows as $row) {
		$results[] = array('userid' => $row['user2'], 'username' => $row['username'],
			'hometown' => $row['region'], 'photo' => $row['photo'], 'status' => $row['is_active']);
	}

	echo json_encode(array("result" => $results));

	wp_die();
}

add_action('wp_ajax_people_search_nearby', 'people_search_nearby');

add_action('wp_ajax_people_invitations', 'people_invitations');

add_action('wp_ajax_people_invitations_connect', 'people_invitations_connect');

add_action('wp_ajax_people_invitations_disconnect', 'people_invitations_disconnect');

add_action('wp_ajax_people_connect', 'people_connect');

add_action('wp_ajax_people_disconnect', 'people_disconnect');

add_action('wp_ajax_people_suggestions', 'people_suggestions');

add_action('wp_ajax_people_contacts', 'people_contacts');

add_action('wp_ajax_people_search_filter', 'people_search_filter');