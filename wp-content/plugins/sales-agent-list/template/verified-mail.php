<?php
$mail_content=<<<EOD
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
</head>

<body>
    <div style="margin: 0 auto;border-radius: 3px;  background-color: #deeff0;max-width: 600px;padding-top: 30px;padding-left: 10px; padding-right: 10px;padding-bottom: 25px;">
        <div class="mail-template">
            <table cellspacing="0" border="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td style="text-align: center;padding-bottom: 18px;" align="center"><a href="https://www.travpart.com/English/"><img
                                style="border: none;" src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/logo.jpg"
                                width="232" height="69" alt="www.travpart.com"></a></td>
                </tr>
                <tr>
                    <td style="background-color: #fff;padding: 60px 40px;">
                        <table cellspacing="0" border="0" cellpadding="0" style="width: 100%;">
                            <tr>
                                <td style="font-family: OpenSans;font-size: 24px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.5;  letter-spacing: -1.4px;text-align: left;  color: #42454e;padding-bottom: 36px;">Hi
								{$username},</td>
                            </tr>
                            <tr>
                                <td style="font-family: OpenSans;font-size: 24px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.5;   letter-spacing: -1.4px;text-align: left;  color: #42454e;padding-bottom: 10px;">Congratulations!
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: OpenSans;font-size: 18px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.5;  letter-spacing: -1.4px;text-align: left;  color: #42454e;">
                                    Your identifications has been reviewed and accepted. Thanks for helping us maintain
                                    a trusted workplace for all.<br>
                                    <br>
                                    You can now get paid for your commissions.</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 60px;padding-top: 60px;">
                                    <a href="https://www.travpart.com/English/travcust/" style="width: 280px;height: 50px;line-height: 50px; font-family: OpenSans; font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;letter-spacing: -0.5px;color: #ffffff;border-radius: 5px;box-shadow: 0px 2px 0 0 rgba(93, 202, 197, 0.31);  background-color: #66cccc;display: block;text-align: center;text-decoration: none!important;">
                                        Start creating a tour package</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: OpenSans;font-size: 18px;font-weight: normal; font-style: normal;  font-stretch: normal;  line-height: 1.5;  letter-spacing: -1.4px;text-align: left;  color: #42454e;">Thanks,<br>
                                    The Travpart Team</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: OpenSans;font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;padding-top: 30px;">Download
                        our mobile app on</td>
                </tr>
                <tr>
                    <td style="font-family: OpenSans;font-size: 18px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;padding-bottom: 40px;"><a
                            href="https://www.travpart.com/English/download" style="color: #66cccc;text-decoration: none!important;">iPhone</a>
                        or <a href="https://www.travpart.com/English/download" style="color: #66cccc;text-decoration: none!important;">Android</a>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom: 40px;text-align: center" align="center">
                        <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img
                                src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/1.jpg" width="27"
                                height="24"></a><a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img
                                src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/2.jpg" width="27"
                                height="24"></a> <a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img
                                src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/3.jpg" width="27"
                                height="24"></a><a href="#" style="color: #ccc;text-decoration: none!important;margin-left: 10px;margin-right: 10px;"><img
                                src="https://www.travpart.com/English/wp-content/themes/bali/images/mail/4.jpg" width="27"
                                height="24"></a>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: OpenSans;font-size: 14px;font-weight: normal;font-style: normal;font-stretch: normal;line-height: 1.5;letter-spacing: -0.5px;text-align: center;color: #333333;">&copy;
                        2018 Travpart Inc.</td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>
EOD;
?>