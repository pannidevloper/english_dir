<?php
// load wp core
$path = explode( 'wp-content', __FILE__ );

if ( is_file( reset( $path ) . 'wp-load.php' ) ) {
	include_once( reset( $path ) . 'wp-load.php' );
} else {
	return;
}

function download_chat_history() {
	global $wpdb;
	$buyer = filter_input(INPUT_GET, 'buyer', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 99999999)));
    $seller = filter_input(INPUT_GET, 'uid', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 99999999)));
    $results2 = $wpdb->get_results("SELECT * FROM chat WHERE (c_from={$buyer} AND c_to={$seller}) OR (c_from={$seller} AND c_to={$buyer}) ORDER BY chatid");
    $file = plugin_dir_path(__FILE__) . '/chat.txt';
	$open = fopen($file, "w");
	foreach ($results2 as $value2) {
		$username_buyer = $wpdb->get_row("SELECT * FROM user  WHERE  userid ={$value2->userid} ");
		$write = fputs($open,  $username_buyer->username . " : " . $value2->message . PHP_EOL);
	}
	fclose($open);
	header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header('Accept-Ranges:bytes');
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=".basename($file));
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".filesize($file));
	readfile($file);
	// Delete file
    unlink($file);
}

if(is_user_logged_in() && current_user_can('administrator'))
	download_chat_history();

exit;

