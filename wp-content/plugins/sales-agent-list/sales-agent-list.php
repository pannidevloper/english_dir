<?php
/**
 * Plugin Name: Travel Advisor List
 * Version:     1.63
 * Author:      Lix
 * Description: Travel Advisor List
 */

//Added Chat Downloading Option
register_activation_hook(__FILE__, 'travel_advisor_list_activate');

function travel_advisor_list_activate() {
    
}

/* * *************************** show the subscribe in admin page  ***************************** */

add_action('admin_menu', 'add_travel_advisor_list_admin_menu');
add_action('admin_init', 'add_travel_advisor_list_admin_menu_init');
add_action('wp_ajax_verified', 'verified');
add_action('wp_ajax_unverified', 'unverified');
add_action('wp_ajax_addpaidlog', 'addpaidlog');

function add_travel_advisor_list_admin_menu() {
    add_options_page('Travel Advisor List', 'Travel Advisor List', 'manage_options', __FILE__, 'show_travel_advisor_main');
}

function add_travel_advisor_list_admin_menu_init() {
    wp_register_style('travel-advisor-list', plugins_url('css/datatables.min.css', __FILE__));
    wp_enqueue_style('travel-advisor-list');
    wp_register_script('travel-advisor-list', plugins_url('js/datatables.min.js', __FILE__));
    wp_register_script('travel-advisor-list-natural', plugins_url('js/datatables-natural.js', __FILE__));
    wp_enqueue_script('travel-advisor-list');
    wp_enqueue_script('travel-advisor-list-natural');
}

if (!function_exists('sendmail')) {

    function sendmail($to, $subject, $message) {
        $key = "Tt3At58P6ZoYJ0qhLvqdYyx21";
        $postdata = array('to' => $to,
            'subject' => $subject,
            'message' => $message);
        $url = "https://www.tourfrombali.com/api.php?action=sendmail&key={$key}";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        $data = curl_exec($ch);
        if (curl_errno($ch) || $data == FALSE) {
            curl_close($ch);
            return FALSE;
        } else {
            curl_close($ch);
            return TRUE;
        }
    }

}

function show_travel_advisor_main() {
    $action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
    switch ($action) {
        case 'check-verification':
            check_verification();
            break;

        case 'paid-to-agent':
            paid_to_agent();
            break;
        case 'download_chat':
            download_chat();
            break;

        default:
            show_travel_advisor_list();
    }
}

function download_chat()
{
    $uid = filter_input(INPUT_GET, 'uid', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 9999)));

    global $wpdb;
	$uid = $wpdb->get_var("SELECT user.userid FROM user,wp_users WHERE wp_users.ID='{$uid}' AND user.username=wp_users.user_login");
    $results = $wpdb->get_results("SELECT DISTINCT c_to,c_from FROM chat WHERE c_from={$uid} OR c_to={$uid} ORDER BY chatid", ARRAY_N);
	$userList=array();
	foreach($results as $row) {
		$userList[]=($row[0]==$uid?$row[1]:$row[0]);
	}
	$userList=array_unique($userList);
	
	
    $username_seller = $wpdb->get_row("SELECT * FROM user  WHERE  userid ={$uid}");
    echo "<h3>";
    echo $username_seller->username;
    echo "  Chat With Buyers </h3>";
	echo '<table>';
    foreach ($userList as $value) {
		$username_chat = $wpdb->get_row("SELECT * FROM user  WHERE  userid ={$value}");
		$chat_start_time = $wpdb->get_var("SELECT chat_date FROM chat WHERE (c_from={$uid} AND c_to={$value}) OR (c_from={$value} AND c_to={$uid}) ORDER BY chatid LIMIT 1");

        if (!empty($username_chat->username)) {
            echo '<tr>
					<td>'.$username_chat->username.'</td>';
			echo '<td>'.$chat_start_time.'</td>';
            echo '<td>
					<a href="'.esc_url( plugins_url( "download_chat_history.php?uid={$uid}&buyer={$username_chat->userid}", __FILE__ ) ).'" target=_blank>Retrive</a>
				  </td>
				  </tr>';
        }

    }
	echo '</table>';
}

function check_verification() {
    if (($uid = filter_input(INPUT_GET, 'uid', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 99999999)))) == false)
        echo "*Invaild User ID";
    else if (get_user_meta($uid, 'verification_status', true) != 1 && get_user_meta($uid, 'verification_status', true) != 2 && get_user_meta($uid, 'verification_status', true) != 3) {
        echo "*The User ID is not existed or the user haven't uploaded the ID verification";
    } else if (!empty(get_user_meta($uid, 'hiredPlatform', true)) && get_user_meta($uid, 'verification_status', true) == 2) {
        echo "*The User is hired at a platform.";
    } else {
        $bank_payment = get_user_meta($uid, 'bank_payment', true);
        $paypal_payment = get_user_meta($uid, 'paypal_payment', true)
        ?>
        <table id="checkVerification" style="text-align:left;width:60%">
            <thead>
            <th colspan="2" style="text-align:center;">Check Verification</th>
        </thead>
        <tbody>
            <tr>
                <td>ID Type</td>
                <td>
                    <?php
                    $ID_Type = get_user_meta($uid, 'verification_IDType', true);
                    switch ($ID_Type) {
                        case 'passport':
                            echo 'Passport';
                            break;
                        case 'national_id_card':
                            echo 'National ID Card';
                            break;
                        case 'driver_license':
                            echo 'Driver’s licence';
                            break;
                        default:
                            echo 'Other';
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>Scan of ID</td>
                <td>
                    <?php
                    $file_url = get_user_meta($uid, 'verification_file_url', true);
                    $file_type = get_user_meta($uid, 'verification_file_type', true);
                    if (stripos($file_type, 'image') !== false)
                        echo '<img style="width:65%;" src="' . wp_upload_dir()['baseurl'] . '/' . $file_url . '" />';
                    if (stripos($file_type, 'pdf') !== false)
                        echo '<a href="' . wp_upload_dir()['baseurl'] . '/' . $file_url . '">Download</a>';
                    ?>
                </td>
            </tr>
            <tr>
                <td>ID Issuing Country</td>
                <td><?php echo get_user_meta($uid, 'verification_IDIssuingCountry', true); ?></td>
            </tr>
            <tr>
                <td>ID Card's Name</td>
                <td><?php echo get_user_meta($uid, 'verification_IDName', true); ?></td>
            </tr>
            <tr>
                <td>ID Number</td>
                <td><?php echo get_user_meta($uid, 'verification_IDNumber', true); ?></td>
            </tr>
            <tr>
                <td>ID Expires on</td>
                <td><?php echo date('Y-n-j', get_user_meta($uid, 'verification_IDExpireson', true)); ?></td>
            </tr>
            <?php if (!empty($paypal_payment)) { ?>
                <tr>
                    <td>PayPal</td>
                    <td><?php echo $paypal_payment; ?></td>
                </tr>
            <?php } ?>
            <?php if (!empty($bank_payment)) { ?>
                <tr>
                    <td>Bank SWIFT Code</td>
                    <td><?php echo $bank_payment['bank_swift_code']; ?></td>
                </tr>
				<tr>
                    <td>Bank Name</td>
                    <td><?php echo empty($bank_payment['bank_name'])?'':$bank_payment['bank_name']; ?></td>
                </tr>
                <tr>
                    <td>Account Number</td>
                    <td><?php echo $bank_payment['account_number']; ?></td>
                </tr>
                <tr>
                    <td>Name on Account</td>
                    <td><?php echo $bank_payment['account_name']; ?></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><?php echo $bank_payment['account_address']; ?></td>
                </tr>
                <tr>
                    <td>City and State/Province</td>
                    <td><?php echo $bank_payment['account_city']; ?></td>
                </tr>
                <tr>
                    <td>Country</td>
                    <td><?php echo $bank_payment['account_country']; ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td>Real Name</td>
                <td><input id="user_realname" type="text" value="<?php echo get_user_by('id', $uid)->nickname; ?>" /></td>
            </tr>
        </tbody>
        </table>
        <div id="verify_box" style="margin-left:auto;margin-right:auto;width:300px;">
            <?php if (get_user_meta($uid, 'verification_status', true) == 1) { ?>
                <button id="verified">Verified</button><span style="margin-left:45px;"></span> <button id="unverified">Unverified</button>
                <p id="error_msg" style="color:red;display:none;"></p>
            <?php } else if (get_user_meta($uid, 'verification_status', true) == 2) { ?>
                <p>The user's verification have been verified</p>
            <?php } else if (get_user_meta($uid, 'verification_status', true) == 3) { ?>
                <p>The user's verification have been unverified</p>
            <?php } ?>
        </div>
        <script>
            jQuery(document).ready(function () {
                jQuery('#verified').click(function () {
                    jQuery('#verify_box>button').attr('disabled', 'disabled');
                    var data = {
                        'action': 'verified',
                        'uid': <?php echo $uid; ?>,
                        'user_realname': jQuery('#user_realname').val()
                    };
                    jQuery.post(ajaxurl, data, function (response) {
                        if (response.error_msg != undefined && response.error_msg.length > 1) {
                            jQuery('#error_msg').text(response.error_msg);
                            jQuery('#error_msg').show();
                            jQuery('#verify_box>button').removeAttr('disabled');
                        } else {
                            jQuery('#error_msg').hide();
                            jQuery('#verify_box>button').remove();
                            jQuery('#verify_box').append('<p>The user\'s verification have been verified</p>');
                        }
                    }, 'json');
                });

                jQuery('#unverified').click(function () {
                    jQuery('#verify_box>button').attr('disabled', 'disabled');
                    var data = {
                        'action': 'unverified',
                        'uid': <?php echo $uid; ?>
                    };
                    jQuery.post(ajaxurl, data, function (response) {
                        if (response.error_msg != undefined && response.error_msg.length > 1) {
                            jQuery('#error_msg').text(response.error_msg);
                            jQuery('#error_msg').show();
                            jQuery('#verify_box>button').removeAttr('disabled');
                        } else {
                            jQuery('#error_msg').hide();
                            jQuery('#verify_box>button').remove();
                            jQuery('#verify_box').append('<p>The user\'s verification have been unverified</p>');
                        }
                    }, 'json');
                    /*<?php
        $url = $_SERVER['HTTP_HOST'];
        if ($_SERVER['HTTPS'] == 'on')
            $url = 'https://' . $url;
        else
            $url = 'http://' . $url;
        $url .= add_query_arg(array('action' => 'unverified', 'uid' => $uid))
        ?>
                     var callurl='<?php echo $url; ?>';
                     jQuery('#verify_box>button').attr('disabled','disabled');
                     jQuery.ajax({
                     type: "GET",
                     url: callurl,
                     success: function (data) {
                     jQuery('#verify_box>button').remove();
                     jQuery('#verify_box').append('<p>The user\'s verification have been unverified</p>');
                     }
                     });*/
                });

                jQuery('#checkVerification').DataTable();

            });
        </script>
        <?php
    }
}

function paid_to_agent() {
    if (($userid = filter_input(INPUT_GET, 'userid', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 99999999)))) == false)
        echo "*Invaild User ID";
    else {
        ?>
        <table id="addPaidLog" style="text-align:left;width:60%">
            <thead>
            <th colspan="2" style="text-align:center;">Add Paid Log</th>
        </thead>
        <tbody>
            <tr>
                <td>User ID</td>
                <td><?php echo $userid; ?></td>
            </tr>
            <tr>
                <td>Type</td>
                <td>
                    <select id="payment_type">
                        <option value="paypal">PayPal</option>
                        <option value="bank">Wire transfer</option>
                        <option value="payoneer">Payoneer</option>
                        <option value="freelancerplatform">Freelancer Platform</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Transaction Number</td>
                <td><input id="transaction_number" type="text" style="width:70%;" /></td>
            </tr>
        </tbody>
        </table>
        <div style="margin-left:auto;margin-right:auto; margin-top:20px; width:300px;">
            <p id="addPaidLogMsg" style="color:red;display:none;"></p>
            <a id="SavePaidLog" class="btn btn-default" style="padding:5px; background-color:green; color:white;">Save</a>
        </div>

        <script>
            jQuery(document).ready(function () {
                jQuery('#SavePaidLog').click(function () {
                    jQuery('#SavePaidLog').attr('disabled', 'disabled');
                    var data = {
                        'action': 'addpaidlog',
                        'userid': <?php echo $userid; ?>,
                        'type': jQuery('#payment_type').val(),
                        'transaction_number': jQuery('#transaction_number').val()
                    };
                    jQuery.post(ajaxurl, data, function (response) {
                        if (response.error_msg != undefined && response.error_msg.length > 1) {
                            jQuery('#addPaidLogMsg').text(response.error_msg);
                            jQuery('#addPaidLogMsg').css('color', 'red');
                            jQuery('#addPaidLogMsg').show();
                            jQuery('#SavePaidLog').removeAttr('disabled');
                        } else {
                            jQuery('#addPaidLogMsg').text('Saved');
                            jQuery('#addPaidLogMsg').css('color', 'green');
                            jQuery('#addPaidLogMsg').show();
                            jQuery('#transaction_number').val('');
                            jQuery('#SavePaidLog').removeAttr('disabled');
                        }
                    }, 'json');
                });

                jQuery('#addPaidLog').DataTable();
            });
        </script>
        <?php
    }
}

function update_travchat_nickname($username, $nickname) {
    global $wpdb;
    $wpdb->query("UPDATE `user` SET `uname` = '{$nickname}' WHERE username='{$username}'");
}

function verified() {
    if (!current_user_can('administrator'))
        wp_die(-1);
    else if (($uid = filter_input(INPUT_POST, 'uid', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 99999999)))) == false)
        echo json_encode(array('error_msg' => '*Invaild User ID'));
    else if (get_user_meta($uid, 'verification_status', true) == 2)
        echo json_encode(array('error_msg' => 'The user verification have been verified.'));
    else if (get_user_meta($uid, 'verification_status', true) != 1)
        echo json_encode(array('error_msg' => 'The user haven\'t uploaded verification.'));
    else {
        if (get_user_by('id', $uid)) {
            update_user_meta($uid, 'verification_status', 2);
            if (!empty($_POST['user_realname'])) {
                wp_update_user(array('ID' => $uid, 'nickname' => htmlspecialchars($_POST['user_realname']), 'display_name' => htmlspecialchars($_POST['user_realname'])));
                update_travchat_nickname(get_user_by('id', $uid)->user_login, htmlspecialchars($_POST['user_realname']));
            }
            $mailto = get_user_by('id', $uid)->user_email;
            $username = get_user_meta($uid, 'verification_IDName', true);
            include(plugin_dir_path(__FILE__) . 'template/verified-mail.php');
            sendmail($mailto, 'Your identifications has been reviewed', $mail_content);
            echo json_encode(array('msg' => 'The user identifications have been verified.'));
        } else
            echo json_encode(array('error_msg' => 'The user is not existed.'));
    }
    wp_die();
}

function unverified() {
    if (!current_user_can('administrator'))
        wp_die(-1);
    else if (($uid = filter_input(INPUT_POST, 'uid', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 99999999)))) == false)
        echo json_encode(array('error_msg' => '*Invaild User ID'));
    else if (get_user_meta($uid, 'verification_status', true) == 2)
        echo json_encode(array('error_msg' => 'The user verification have been verified.'));
    else if (get_user_meta($uid, 'verification_status', true) != 1)
        echo json_encode(array('error_msg' => 'The user haven\'t uploaded verification.'));
    else {
        if (get_user_by('id', $uid)) {
            update_user_meta($uid, 'verification_status', 3);
            $mailto = get_user_by('id', $uid)->user_email;
            $username = get_user_meta($uid, 'verification_IDName', true);
            include(plugin_dir_path(__FILE__) . 'template/unverified-mail.php');
            sendmail($mailto, 'Your identifications has been reviewed', $mail_content);
            echo json_encode(array('msg' => 'The user identifications have been unverified.'));
        } else
            echo json_encode(array('error_msg' => 'The user is not existed'));
    }
    wp_die();
}

function auto_unverified($mailto, $username) {
    include(plugin_dir_path(__FILE__) . 'template/unverified-mail.php');
    sendmail($mailto, 'Your identifications has been reviewed', $mail_content);

    return true;
}

function addpaidlog() {
    global $wpdb;
    if (!current_user_can('administrator'))
        wp_die(-1);
    else if (($userid = filter_input(INPUT_POST, 'userid', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 99999999)))) == false)
        echo json_encode(array('error_msg' => '*Invaild User ID'));
    else if (($pay_type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING)) == false)
        echo json_encode(array('error_msg' => '*Invaild Payment Type'));
    else if (($balance = $wpdb->get_row("SELECT `balance` FROM `balance` WHERE `userid`={$userid}")->balance) <= 0)
        echo json_encode(array('error_msg' => '*The balance is zero.'));
    else {
        $transaction_number = filter_input(INPUT_POST, 'transaction_number', FILTER_SANITIZE_STRING);
        if ($wpdb->query("INSERT INTO `withdraw_log` (`userid`, `transaction_number`, `wdate`, `type`, `amount`, `balance`) VALUES ('{$userid}', '{$transaction_number}', CURRENT_TIMESTAMP, '{$pay_type}', '{$balance}', 0)")) {
            $wpdb->query("UPDATE `balance` SET `balance` = '0' WHERE `userid`={$userid}");
			$wp_user_id=$wpdb->get_var("SELECT wp_users.ID FROM user,wp_users WHERE `userid`='{$userid}' AND user.username=wp_users.user_login");
			$wpdb->insert('notification', array(
				'wp_user_id'=>$wp_user_id,
				'content'=>'Congratulation! Your commission has been released.'),
				array('%d','%s'));
            echo json_encode(array('msg' => 'Added successfully'));
        } else
            echo json_encode(array('error_msg' => '*System is busy. Please try again.'));
    }
    wp_die();
}

function show_travel_advisor_list() {
    global $wpdb;
    global $wp;

    date_default_timezone_set('Asia/Jakarta');

    $AgentList = $wpdb->get_results("SELECT `wp_users`.`ID`,`user`.`userid`,`username`,`uname`,`phone`,`email`,UNIX_TIMESTAMP(`user`.`user_registered`) as user_registered,`balance` FROM (`user`,`wp_users`) LEFT JOIN `balance` ON `user`.`userid`=`balance`.`userid` WHERE `access`=1 AND `user`.`username`=`wp_users`.`user_login`");
    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <table id="travelAdvisorList" style="text-align:left;">
        <thead>
            <tr>
                <th>Name</th> 
                <th>Username</th> 
                <th>UserId</th> 
                <th>Registered Date</th> 
                <th>Email Address</th>
                <th>Phone Number</th> 
                <th>Commission Balance</th>
                <th>Commission earned</th>
                <th>Paid/Unpaid</th>
                <th>ID Card's Name</th> 
                <th>ID Card's Number</th> 
                <th>Bank or E-wallet Account</th>
                <th>Personal ID Verification</th> 
                <th>Download Chat History</th> 
                <th>Verification status</th>
                <th>Followers</th>
				<th>Referral</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $url = $_SERVER['HTTP_HOST'];
            if ($_SERVER['HTTPS'] == 'on')
                $url = 'https://' . $url;
            else
                $url = 'http://' . $url;
            foreach ($AgentList as $r) {
                $verified = get_user_meta($r->ID, 'verification_status', true);
                $hiredPlatform = get_user_meta($r->ID, 'hiredPlatform', true);
                $id_name = get_user_meta($r->ID, 'verification_IDName', true);
                $id_number = get_user_meta($r->ID, 'verification_IDNumber', true);
                $bank_payment = get_user_meta($r->ID, 'bank_payment', true);
                $commission_earned = $wpdb->get_var("SELECT SUM(amount) FROM `withdraw_log` WHERE `userid`={$r->userid}");
				//Get follow count
				$follow_count=$wpdb->get_var("SELECT COUNT(*) FROM `social_follow` WHERE `sf_agent_id` = '{$r->ID}'");
				//Count the chat user
				$chatUserList=$wpdb->get_results("SELECT DISTINCT c_to,c_from FROM chat WHERE c_from={$r->userid} OR c_to={$r->userid} ORDER BY chatid", ARRAY_N);
				$userList=array();
				foreach($chatUserList as $row) {
					$userList[]=($row[0]==$r->userid?$row[1]:$row[0]);
				}
				$userList=array_unique($userList);
				$chatUserCount=0;
				if(count($userList)>0) {
					$userList=implode(',', $userList);
					$chatUserCount=$wpdb->get_var("SELECT COUNT(*) FROM user WHERE userid IN ({$userList})");
				}
                ?>
                <tr>
                    <td><?php echo (empty($r->uname)) ? $r->username : $r->uname; ?></td>
                    <td><?php echo $r->username; ?></td>
                    <td><?php echo $r->userid; ?></td>
                    <td><?php echo date('Y-m-d H:i:s', $r->user_registered); ?></td>
                    <td><?php echo $r->email; ?></td>
                    <td><?php echo $r->phone; ?></td>
                    <td><?php echo (empty($r->balance) || $r->balance <= 0) ? 0 : $r->balance; ?></td>
                    <td><?php echo (empty($commission_earned) || floatval($commission_earned) <= 0) ? 0 : $commission_earned; ?></td>
                    <td><?php echo (!empty($r->balance) && $r->balance > 0) ? ('<a href="' . $url . add_query_arg(array('action' => 'paid-to-agent', 'userid' => $r->userid)) . '" target=_blank>Unpaid</a>') : 'Paid'; ?></td>
                    <td><?php echo empty($id_name) ? '' : $id_name; ?></td>
                    <?php if (!empty($hiredPlatform) && $verified == 2) { ?>
                        <td><?php echo $hiredPlatform; ?></td><td></td>
                    <?php } else { ?>
                        <td><?php echo empty($id_number) ? '' : $id_number; ?></td>
                        <td><?php echo empty($bank_payment['account_number']) ? '' : $bank_payment['account_number']; ?></td>
                    <?php } ?>
                    <td>
                        <?php
                        if (!empty($verified) && ($verified == 1 || $verified == 2 || $verified == 3)) {
                            if (!empty($hiredPlatform) && $verified == 2)
                                echo $hiredPlatform;
                            else {
                                ?>
                                <a href="<?php echo $url . add_query_arg(array('action' => 'check-verification', 'uid' => $r->ID)); ?>" target=_blank>Click to Check</a>
                            <?php
                            }
                        }
                        ?>
                    </td>
                    <td> <!--<a href="javascript:;" data-toggle="modal" data-target="#myModal">Click to Download</a> -->
                        <a href="<?php echo $url . add_query_arg(array('action' => 'download_chat', 'uid' => $r->ID)); ?>" target=_blank><?php echo $chatUserCount; ?> users chat</a>
                    </td>
                    <td>
                        <?php
                        if (empty($verified))
                            echo 'Unverified';
                        else if ($verified == 1)
                            echo 'Pending';
                        else if ($verified == 2)
                            echo 'Verified';
                        else
                            echo 'Unverified';
                        ?>
                    </td>
                    <td><?php echo number_format($follow_count); ?></td>
					<td>
						<?php echo (get_user_meta($r->ID, 'can_referral', true)==1)?'Enabled':'Disabled'; ?>
					</td>
                </tr>
            <?php } ?>
            <?php if (count($AgentList) <= 0) { ?>
                <tr> <td colspan="7">The Travel Advisor List is empty!</td> </tr>
    <?php } ?>
        </tbody>
    </table>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Download Chat History</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <p>Some text in the modal.</p>
                        <input name="dates" type="text">
                        <button type="submit" class="btn btn-default">Download</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <script>
        jQuery(document).ready(function () {

            jQuery('input[name="dates"]').daterangepicker();

            jQuery('#travelAdvisorList').DataTable({
                dom: 'Bfrtip',
                pageLength: 15,
                ordering: true,
                columnDefs: [
       				{ type: 'natural-nohtml', targets: 13 }
     			],
                "search": {
                    "caseInsenstive": false
                },
                "buttons": [
                    {
                        'extend': 'excelHtml5',
                        'text': 'Download',
                        'exportOptions': {
                            'modifier': {
                            }
                        }
                    }
                ]
            });
        });
    </script>

    <?php
}
?>
