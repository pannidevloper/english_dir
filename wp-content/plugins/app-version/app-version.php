<?php

/**
 * Plugin Name: APP Version Setting
 * Version:     1.0
 * Author:      Lix
 * Description: APP Version Setting
 */


register_activation_hook(__FILE__, 'app_version_setting_activate');

function app_version_setting_activate()
{
}

add_action('admin_menu', 'add_app_version_setting_admin_menu');
add_action('admin_init', 'add_app_version_setting_admin_menu_init');

function add_app_version_setting_admin_menu()
{
    add_options_page('APP Version Setting', 'APP Version Setting', 'manage_travcust_data', __FILE__, 'show_app_version_setting');
}

function add_app_version_setting_admin_menu_init()
{
}

function show_app_version_setting()
{
?>
    <h2>APP Version Setting</h2>
    <form action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST">
        <?php
        if ($_POST['save']) {
            $app_version = array(
                'version' => htmlspecialchars($_POST['version']),
                'force_update' => ($_POST['force_update'] == 1)
            );
            update_option('app_version', $app_version);
        }

        $app_version = get_option('app_version');
        ?>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row"><label>APP Version</label></th>
                    <td><input name="version" type="text" value="<?php echo $app_version['version']; ?>" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row">Force Update</th>
                    <td><input name="force_update" type="checkbox" value="1" <?php echo $app_version['force_update']?'checked':''; ?>></td>
                </tr>
            </tbody>
        </table>
        <p class="submit"><input type="submit" name="save" class="button button-primary" value="Save Changes"></p>
    </form>
<?php
}
