<?php
/**
 * Plugin Name: Show user comments_on_feed
 * Version:     1.0
 * Author:      Farhan
 * Description: This Plugin shows user comments on feeds
 */



 // Add admin sitting

	add_action( 'admin_menu', 'my_admin_menu_for_showing_comments' );

	function my_admin_menu_for_showing_comments() {

	add_menu_page( 'User comments', 'User comments', 'manage_options', 'feedlist.php', 'show_feed_posts', 'dashicons-tickets', 6  );

	}

	// show data
	function show_feed_posts(){

	$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
	$feed_id = filter_input(INPUT_GET, 'feed_id', FILTER_SANITIZE_STRING);
	$comment_id = filter_input(INPUT_GET, 'comment_id', FILTER_SANITIZE_STRING);
	switch ($action) {

	case 'show_single_post_comment_list':
	show_single_post_comment_list($feed_id);
	break;
	case 'delete_comment':
	delete_comment($comment_id);
	show_single_post_comment_list($feed_id);
	break;
	default:
	show_posts_list();
	break;
	}

	}


	if ( isset( $_GET['page'] ) && $_GET['page'] == 'feedlist.php' ) {

	wp_enqueue_style( 'custom_wp_admin_css', plugins_url('contactlist.css', __FILE__) );

	}

	add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

	function show_posts_list() {
	global $wpdb;
	?>

	<div class="row">
	<div class="col-sm-6">
	<h2 style="color:green">
	Posts list
	</h2>
	<?php
	$records = $wpdb->get_results( 
	"SELECT DISTINCT feed_id FROM  user_feed_comments ORDER BY id DESC"
	);
	if(!empty($records)):
	?> 
	<table id="customers">
	<tr>
	<th>Post id</th>
	<th>No of comments</th>
	</tr>

	<?php

	foreach ( $records as $result ) {

	$total_query = "SELECT COUNT(*) FROM user_feed_comments AS total_count WHERE feed_id='{$result->feed_id}'";
	$total_comments = $wpdb->get_var( $total_query );

	//echo "<tr><td>";
	//echo '<a href="">'.$result->wp_user_id.'</a>';
	//echo "</td>";
	echo "<td>";
	echo $result->feed_id;
	echo "</td>";
	echo "<td>";?>
	<a href="<?php echo $url . add_query_arg(array('action' => 'show_single_post_comment_list','feed_id' =>$result->feed_id )); ?>" target="_blank"><?php echo $total_comments; ?> </a>

	<?php echo "</td>";

	echo"</tr>";
	}
	else:
	echo "No data";
	endif;
	?>
	</tr>
	</table>

	</div>


	</div>
	<div>

	</div>
	<?php
	}

	function show_single_post_comment_list($feed_id) {
	global $wpdb;
	?>

	<div class="row">
	<div class="col-sm-6">
	<h2 style="color:green">
	Comments of  post-id : <?php echo $feeed_id; ?>
	</h2>
	<?php
	$records = $wpdb->get_results( 
	"SELECT id,user_id,comment  FROM  user_feed_comments WHERE feed_id='{$feed_id}'"
	);
	if(!empty($records)):
	?> 
	<table id="customers">
	<tr>
	<th>user_id</th>
	<th>Comment</th>
	<th> Delete</th>
	</tr>

	<?php

	foreach ( $records as $result ) {

	echo "<tr><td>";
	echo '<a href="">'.$result->user_id.'</a>';
	echo "</td>";
	echo "<td>";
	echo '<a href="">'.$result->comment.'</a>';
	echo "</td>";
	echo "<td>";?>
	<a href="<?php echo $url . add_query_arg(array('action' => 'delete_comment','comment_id' =>$result->id )); ?>" target="_blank"><?php echo 'delete'; ?> </a>

	<?php echo "</td>";
	//echo "<td>";
	//echo '<a href="">'.$status.'</a>';
	//echo "</td>";
	}
	else:
	echo "No data";
	endif;
	?>
	</tr>
	</table>

	</div>


	</div>
	<div>

	</div>



	<?php
	}

	function delete_comment($comment_id) {
	global $wpdb;

	$wpdb->delete( 'user_feed_comments', array( 'id' => $comment_id ) );
	}
