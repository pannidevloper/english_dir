<?php
/**
 * @package  TravpartVo
 */

/**
 * Plugin Name:       Travpart Vouchar
 * Description:       Travpart vouchar & general configuration
 * Version:           1.0.0
 * Author:            Travpart
 * Author URI:        https://www.travpart.com
 * Text Domain:       travpart-vo
 * License:           GPLv2 or later
 */

// If this file is called directly, abort!!!
defined('ABSPATH') or die();

global $wp_version;
if (version_compare($wp_version, '4.5', '<')) {

    add_action('admin_notices', function () {

        echo '<div class="error notice">';
        echo '<p>' . __('Travpart Vouchar plugin requires wordpress version atleast 4.5', 'travpart-vo') . '</p>';
        echo '</div>';
    });

} else if (version_compare(phpversion(), '7.0', '<')) {
    add_action('admin_notices', function () {

        echo '<div class="error notice">';
        echo '<p>' . __('Travpart Vouchar plugin requires PHP version atleast 7.0', 'travpart-vo') . '</p>';
        echo '</div>';
    });

} else {

// Composer Autoload
    if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
        require_once dirname(__FILE__) . '/vendor/autoload.php';
    }

/**
 * The code that runs during plugin activation
 */
    function activateTravpartVo()
    {
        Inc\Base\Activate::activate();
    }

    register_activation_hook(__FILE__, 'activateTravpartVo');

/**
 * The code that runs during plugin deactivation
 */
    function deactivateTravpartVo()
    {
        Inc\Base\Deactivate::deactivate();
    }

    register_deactivation_hook(__FILE__, 'deactivateTravpartVo');

/**
 * Returns the main instance of TravpartVo.
 */
    if (class_exists('Inc\\Init')) {
        return Inc\Init::registerServices();
    }

}
