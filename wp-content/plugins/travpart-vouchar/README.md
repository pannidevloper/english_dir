# Travpart Voucher plugin integration (tutorial)

 Within admin panel there are two section for this plugin. Settings page where you can enter general information like "Feedback From Sales Agents, Login Points based on Weekly or Daily basis, Points for share a tour package etc.". Voucher Schemes, from where you can manage different schemes like "Gold Holidays Schemes, Silver Holidays Schemes" etc..

In the Voucher Schemes page you can add multiple Icons. each icon cosist icon image, and a textarea where you can add following points:

    * Discount: Only % or $
    * Figure: The discount value. Only enter numeric value
    * Prabability: Only enter decimal value like 0.50 or 2.50 or 20.00 etc.
    * Availability: Only enter numeric value like 110, 114, 50 etc.

You need to segregate each value using "|" i.e %|40|0.50|30

To create more than one row just take the following example:

    * %|40|0.50|30
    * $|20|20.00|150
    * $|100|0.20|50
