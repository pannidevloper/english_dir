<table class="form-table">

    <tr>
        <th scope="row"><label for="blogname"><?php echo __('Vouchers', 'travpart-vo'); ?></label></th>
        <td>
            <div id="meta-inner">
                <div id="meta-inner-content">
					<?php
					$counter=0;
					?>
					<p>Flight Gold:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					<p>Flight Sliver:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					
					<p>Meal Gold:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					<p>Meal Sliver:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					
					<p>Vehicle Gold:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					<p>Vehicle Sliver:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					
					<p>Massage Gold:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					<p>Massage Sliver:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					
					<p>Helicopter Gold:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					<p>Helicopter Sliver:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					
					<p>Hotel Gold:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					<p>Hotel Sliver:</p>
					<p>Discount:
						<select name="tp_icon_vs[<?php echo $counter; ?>][discount]">
							<option value="$" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='$'?'selected':''; ?> >$</option>
							<option value="%" <?php echo $tp_vsicon_args['icon'][$counter]['discount']=='%'?'selected':''; ?> >%</option>
						</select>
						<input name="tp_icon_vs[<?php echo $counter; ?>][figure]" type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['figure']??''; ?>" />
					</p>
					<p>Probability: %<input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['probability']??''; ?>" name="tp_icon_vs[<?php echo $counter; ?>][probability]" /> </p>
					<p>Availability: <input type="text" value="<?php echo $tp_vsicon_args['icon'][$counter]['availability']??''; ?>" name="tp_icon_vs[<?php echo $counter++; ?>][availability]" /></p>
                    <br/>
					<?php
/*
$counter = 0;
if (isset($tp_vsicon_args['icon'])) {

    foreach ($tp_vsicon_args['icon'] as $icon_arg) {

        ?>
                    <p style="padding-bottom:10px; margin-bottom:10px; border-bottom:1px solid #f1f1f1;">
                        <input type="text" name="tp_icon_vs[<?php echo $counter; ?>][icon]"
                            class="regular-text tpvs-media-uploader"
                            placeholder="<?php echo __('Icon', 'travpart-vo'); ?>"
                            value="<?php echo $icon_arg['icon']; ?>" required readonly>
                            &nbsp; <img src="<?php echo $icon_arg['icon']; ?>" style="width:60px;">
                        <br /><br />
                        <textarea name="tp_icon_vs[<?php echo $counter; ?>][args]" class="regular-text"
                            placeholder="<?php echo __('%| 70 | 2.8 | 114', 'travpart-vo'); ?>"
                            required=""><?php echo $icon_arg['args']; ?></textarea>
                        <span
                            class="description"><?php echo __('Discount (% / $) | Figure | Prabability | Availability', 'travpart-vo') ?></strong></span>
                        <span style="display:block; margin-top:10px; width:70px; text-align:center;"
                            class="remove-icon button button-primary"><?php echo __('Remove', 'travpart-vo'); ?></span>
                    </p>

                    <?php

        $counter++;
    }
}
*/
?>
                </div>

<?php /*
                <span id="icon-fld" data-count="<?php echo $counter; ?>"></span>
                <button type="button" class="add-icon button button-primary"
                    name="add_icon"><?php echo __('Add Icon', 'travpart-vo'); ?></button>
	*/ ?>
            </div>
        </td>
    </tr>


    <tr>
        <th scope="row"><label for="blogname"><?php echo __('Points need to play', 'travpart-vo'); ?></label></th>
        <td><input id="tp_pts_vs" type="number" name="tp_pts_vs" class="regular-text"
                value="<?php echo (isset($tp_vsicon_args['pts_to_play']) && !is_null ($tp_vsicon_args['pts_to_play']))?$tp_vsicon_args['pts_to_play']:'';?>"
                placeholder="20"></td>
    </tr>


    <tr>
        <th scope="row"><label for="blogname"><?php echo __('Time Limitation', 'travpart-vo'); ?></label></th>
        <td>
            <input id="tp_tfrm_vs" type="date" name="tp_tfrm_vs" class="regular-text"
                value="<?php echo (isset($tp_vsicon_args['start_date']) && !is_null ($tp_vsicon_args['start_date']))?$tp_vsicon_args['start_date']:'';?>">
            <input id="tp_tto_vs" type="date" name="tp_tto_vs" class="regular-text"
                value="<?php echo (isset($tp_vsicon_args['end_date']) && !is_null ($tp_vsicon_args['end_date']))?$tp_vsicon_args['end_date']:'';?>">
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <input id="tp_tmng" type="radio" name="tp_tmng" value="1"
                <?php echo (isset($tp_vsicon_args['status']) && !is_null ($tp_vsicon_args['status']) && $tp_vsicon_args['status'] == 1)?'checked="checked"':'';?>><?php echo __('Enable', 'travpart-vo'); ?>
            <input id="tp_tmng" type="radio" name="tp_tmng" value="0"
                <?php echo (isset($tp_vsicon_args['status']) && !is_null ($tp_vsicon_args['status']) && $tp_vsicon_args['status'] == 0)?'checked="checked"':'';?>><?php echo __('Disable', 'travpart-vo'); ?>
        </td>
    </tr>
</table>


<style>
#meta-inner .description {
    display: block;
}
</style>

<script>
(function($) {
    $(document).ready(function() {

        $(".tpvs-media-uploader").live('click', function(e) {
            e.preventDefault();
            var self_obj = $(this);
            var custom_uploader = wp.media({
                title: '<?php echo __('Upload Icon', 'travpart-vo'); ?>',
                button: {
                    text: '<?php echo __('Add Icon', 'travpart-vo'); ?>'
                },
                multiple: false
            }).on('select', function() {
                var attachment = custom_uploader.state().get('selection').first().toJSON();
                self_obj.val(attachment.url);
            }).open();
        });


        $(".add-icon").click(function() {
            var count = parseInt(jQuery('#icon-fld').attr('data-count'));

            $('#meta-inner-content').append(
                '<p style="padding-bottom:10px; margin-bottom:10px; border-bottom:1px solid #f1f1f1;"><input type="text" name="tp_icon_vs[' +
                count +
                '][icon]" class="regular-text tpvs-media-uploader" placeholder="<?php echo __('Icon', 'travpart-vo'); ?>" required readonly><br /><br /><textarea name="tp_icon_vs[' +
                count +
                '][args]"  class="regular-text" placeholder="<?php echo __('%| 70 | 2.8 | 114', 'travpart-vo'); ?>" required=""></textarea><span class="description"><?php echo __('Discount (% / $) | Figure | Prabability | Availability', 'travpart-vo') ?></strong></span><span style="display:block; margin-top:10px; width:70px; text-align:center;" class="remove-icon button button-primary"><?php echo __('Remove', 'travpart-vo'); ?></span></p>'
            );

            $('#icon-fld').attr('data-count', ++count);
            return false;
        });
        $(".remove-icon").live('click', function() {
            $(this).parent().remove();
        });



    });
})(jQuery);
</script>