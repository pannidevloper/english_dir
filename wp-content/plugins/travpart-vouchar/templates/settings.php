<div class="wrap">

    <h1><?php _e('Travpart Vouchar Settings', 'travpart-vo'); ?></h1>



    <div id="travpart-updated" class="updated settings-error notice is-dismissible" style="display:none"></div>



    <form id="travpart-settings-form" class="postbox">

        <div class="form-group inside">

            <p class="notice notice-warning p-10">
                <?php _e( 'Only enter numeric values in the below fields .', 'feedier' ); ?>
            </p>

            <h3><?php _e('How to obtain points', 'travpart-vo'); ?> :</h3>

            <table class="form-table">
                <tbody>
                    <tr>
                        <td scope="row">
                            <h4><?php _e('Feedback From Travel Advisor', 'travpart-vo'); ?> :</h4>

                            <input name="tp_pts_fb_5_str" id="tp_pts_fb_5_str" class="regular-text" type="number"
                                value="<?php echo (isset($travpart_vo_option['tp_pts_fb_5_str'])) ? $travpart_vo_option['tp_pts_fb_5_str'] : ''; ?>" />
                            <p class="description"><?php _e('Enter points for 5 Star', 'travpart-vo'); ?></p>
                            <br />
                            <input name="tp_pts_fb_4_str" id="tp_pts_fb_4_str" class="regular-text" type="number"
                                value="<?php echo (isset($travpart_vo_option['tp_pts_fb_4_str'])) ? $travpart_vo_option['tp_pts_fb_4_str'] : ''; ?>" />
                            <p class="description"><?php _e('Enter points for 4 Star', 'travpart-vo'); ?></p>
                            <br />
                            <input name="tp_pts_fb_3_str" id="tp_pts_fb_3_str" class="regular-text" type="number"
                                value="<?php echo (isset($travpart_vo_option['tp_pts_fb_3_str'])) ? $travpart_vo_option['tp_pts_fb_3_str'] : ''; ?>" />
                            <p class="description"><?php _e('Enter points for 3 Star', 'travpart-vo'); ?></p>
                            <br />
                            <input name="tp_pts_fb_2_str" id="tp_pts_fb_2_str" class="regular-text" type="number"
                                value="<?php echo (isset($travpart_vo_option['tp_pts_fb_2_str'])) ? $travpart_vo_option['tp_pts_fb_2_str'] : ''; ?>" />
                            <p class="description"><?php _e('Enter points for 2 Star', 'travpart-vo'); ?></p>
                            <br />
                            <input name="tp_pts_fb_1_str" id="tp_pts_fb_1_str" class="regular-text" type="number"
                                value="<?php echo (isset($travpart_vo_option['tp_pts_fb_1_str'])) ? $travpart_vo_option['tp_pts_fb_1_str'] : ''; ?>" />
                            <p class="description"><?php _e('Enter points for 1 Star', 'travpart-vo'); ?></p>
                        </td>
                    </tr>


                    <tr>
                        <td scope="row">
                            <h4><?php _e('Login Points', 'travpart-vo'); ?> : </h4>

                            <input name="tp_pts_lg_dly" id="tp_pts_lg_dly" class="regular-text" type="number"
                                value="<?php echo (isset($travpart_vo_option['tp_pts_lg_dly'])) ? $travpart_vo_option['tp_pts_lg_dly'] : ''; ?>" />
                            <p class="description"><?php _e('Enter points for daily login', 'travpart-vo'); ?></p>
                            <br />
                            <input name="tp_pts_lg_wkly" id="tp_pts_lg_wkly" class="regular-text" type="number"
                                value="<?php echo (isset($travpart_vo_option['tp_pts_lg_wkly'])) ? $travpart_vo_option['tp_pts_lg_wkly'] : ''; ?>" />
                            <p class="description"><?php _e('Enter points for weekly login', 'travpart-vo'); ?></p>
                        </td>
                    </tr>


                    <tr>
                        <td scope="row">
                            <h4><?php _e('Points for request a tour package', 'travpart-vo'); ?> : </h4>

                            <input name="tp_pts_tor_pkg" id="tp_pts_tor_pkg" class="regular-text" type="number"
                                value="<?php echo (isset($travpart_vo_option['tp_pts_tor_pkg'])) ? $travpart_vo_option['tp_pts_tor_pkg'] : ''; ?>" />
                            <p class="description"><?php _e('Enter points for request a tour package', 'travpart-vo'); ?>
                            </p>
                        </td>
                    </tr>


                    <tr>
                        <td scope="row">
                            <h4><?php _e('Points for referring a relative', 'travpart-vo'); ?> : </h4>

                            <input name="tp_pts_refr" id="tp_pts_refr" class="regular-text" type="number"
                                value="<?php echo (isset($travpart_vo_option['tp_pts_refr'])) ? $travpart_vo_option['tp_pts_refr'] : ''; ?>" />
                            <p class="description"><?php _e('Enter points for referring a relative', 'travpart-vo'); ?>
                            </p>
                        </td>
                    </tr>

                </tbody>
            </table>


            <h3><?php _e('Other Settings', 'travpart-vo'); ?> :</h3>



            <table class="form-table">
                <tbody>

                    <tr>
                        <td scope="row">
                            <h4><?php _e("Coupon's Validity", 'travpart-vo'); ?> : </h4>

                            <input name="tp_cpn_valdty" id="tp_cpn_valdty" class="regular-text" type="number"
                                value="<?php echo (isset($travpart_vo_option['tp_cpn_valdty'])) ? $travpart_vo_option['tp_cpn_valdty'] : ''; ?>" />
                            <p class="description"><?php _e("Enter coupon's validity in Day/s", 'travpart-vo'); ?>
                            </p>
                        </td>
                    </tr>
					
					<tr>
                        <td scope="row">
                            <h4><?php _e("Day/s of coupon list at main page", 'travpart-vo'); ?> : </h4>

                            <input name="tp_coupon_display_kept_day" id="tp_coupon_display_kept_day" class="regular-text" type="number"
                                value="<?php echo (isset($travpart_vo_option['tp_coupon_display_kept_day'])) ? $travpart_vo_option['tp_coupon_display_kept_day'] : ''; ?>" />
                            </p>
                        </td>
                    </tr>

                </tbody>
            </table>
			
			<h3>Enable Game voucher:</h3>

			<table class="form-table">
				<tr>
					<input type="radio" name="tp_game_enable" value="1" 
					<?php echo (isset($travpart_vo_option['tp_game_enable']) && !is_null ($travpart_vo_option['tp_game_enable']) && $travpart_vo_option['tp_game_enable'] == 1)?'checked="checked"':'';?> >Enable
					<span style="margin-left:60px;"></span>
					<input type="radio" name="tp_game_enable" value="0" 
					<?php echo (isset($travpart_vo_option['tp_game_enable']) && !is_null ($travpart_vo_option['tp_game_enable']) && $travpart_vo_option['tp_game_enable'] == 0)?'checked="checked"':'';?> >Disable
				</tr>
			</table>

            <div class="inside">

                <button class="button button-primary" id="travpartvo-settings-save" type="submit">
                    <?php _e( 'Save', 'travpart-vo' ); ?>
                </button>
            </div>

        </div>



    </form>

</div>