<div class="wrap">

    <h1><?php _e('Plugin Help Text', 'travpart-vo');?></h1>

    <p>
        <?php _e('Within admin panel there are two section for this plugin. Settings page where you can enter general  information like "Feedback From Travel Advisor, Login Points based on Weekly or Daily basis, Points for share a tour package etc.". Voucher Schemes, from where you can manage different schemes like "Gold Holidays Schemes, Silver Holidays Schemes" etc..','travpart-vo');?>
    </p>

    <p>
        <?php _e('In the Voucher Schemes page you can add multiple Icons. each icon cosist  icon image, and a textarea where you can add following points:','travpart-vo');?>
    </p>
    <ul>
        <li>
            <b><?php _e('Discount:','travpart-vo');?></b>
            <span> <?php _e('Only % or $','travpart-vo');?> </span>
        </li>
        <li>
            <b><?php _e('Figure:','travpart-vo');?></b>
            <span> <?php _e('The discount value. Only enter numeric value','travpart-vo');?> </span>
        </li>
        <li>
            <b><?php _e('Prabability:','travpart-vo');?></b>
            <span> <?php _e('Only enter decimal value like 0.50 or 2.50 or 20.00 etc.','travpart-vo');?> </span>
        </li>
        <li>
            <b><?php _e('Availability:','travpart-vo');?></b>
            <span> <?php _e('Only enter numeric value like 110, 114, 50 etc.','travpart-vo');?> </span>
        </li>
    </ul>

    <p>
        <?php _e('You need to segregate each value using "|" i.e  %|40|0.50|30','travpart-vo');?>
    </p>
    <p>
        <?php _e('To create more than one row just take the following example:','travpart-vo');?>
    </p>

    <ul>
        <li>
            <b>%|40|0.50|30</b>
        </li>
        <li>
            <b>$|20|20.00|150</b>
        </li>
        <li>
            <b>$|100|0.20|50</b>
        </li>

    </ul>


</div>