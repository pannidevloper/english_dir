<?php

/**
 * @package  TravpartVo
 */

namespace Inc\Pages;

use Inc\Base\BaseController;

use Inc\Api\Callback\GeneralSettingsCallback;

class GeneralSettings extends BaseController
{ 
    public $callbacks;  
    public $pages = [];
    public $sub_pages = [];

    public function register()
    {

        add_filter("plugin_action_links_$this->plugin", array($this, 'settingsUrl'));

        $this->callbacks = new GeneralSettingsCallback();
        
        add_action('wp_ajax_travpartvo_settings_data', array($this->callbacks, 'manage_option'));
        
        $this->setPages()->setSubPages()->registerMenu();

    }

    public function settingsUrl($links) {
        $settings_link = "<a href='admin.php?page=travpart-vouchar-settings'>" . __('Settings', 'wc-sms') . "</a>";
        array_push($links, $settings_link);
        return $links;
    }

    public function setPages()
    {
        $this->pages = [
            [
                'page_title' => __('Travpart Vouchar', 'travpart-vo'),
                'menu_title' => __('Travpart Vouchar', 'travpart-vo'),
                'capability' => 'manage_options',
                'menu_slug' => 'travpart-vouchar-settings',
                'callback' => array($this->callbacks, 'travpartSettings'),
                'icon_url' => 'dashicons-admin-settings',
                'position' => 110,
            ],
        ];

        return $this;
    }

    public function setSubPages()
    {
        $this->sub_pages = [
            [
                'parent_slug' => 'travpart-vouchar-settings',
                'page_title' => __('Settings', 'travpart-vo'),
                'menu_title' => __('Settings', 'travpart-vo'),
                'capability' => 'manage_options',
                'menu_slug' => 'travpart-vouchar-settings',
                'callback' => array($this->callbacks, 'travpartSettings'),
            ],
        ];

        return $this;
    }

    public function registerMenu()
    {
        if (!empty($this->pages) || !empty($this->sub_pages)) {
            add_action('admin_menu', array($this, 'addAdminMenu'));
        }
    }

    public function addAdminMenu()
    {

        foreach ($this->pages as $page) {
            add_menu_page($page['page_title'], $page['menu_title'], $page['capability'], $page['menu_slug'], $page['callback'], $page['icon_url'], $page['position']);
        }

        foreach ($this->sub_pages as $page) {
            add_submenu_page($page['parent_slug'], $page['page_title'], $page['menu_title'], $page['capability'], $page['menu_slug'], $page['callback']);
        }
    }

}