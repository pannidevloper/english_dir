<?php

/**
 * @package  TravpartVo
 */

namespace Inc\Pages;

use Inc\Base\BaseController;

use Inc\Api\Callback\HelpCallback;

class Help extends BaseController {

    public $callbacks;  
    public $sub_pages = [];

    public function register() {        
        add_filter("plugin_action_links_$this->plugin", array($this, 'helpUrl'));
        $this->callbacks = new HelpCallback();
        $this->setSubPages()->registerMenu();
    }

    public function helpUrl($links) {
        $help_link = "<a href='admin.php?page=travpart-vouchar-help'>" . __('Help', 'wc-sms') . "</a>";
        array_push($links, $help_link);
        return $links;
    }

    public function setSubPages()
    {
        $this->sub_pages = [
            [
                'parent_slug' => 'travpart-vouchar-settings',
                'page_title' => __('Help', 'travpart-vo'),
                'menu_title' => __('Help', 'travpart-vo'),
                'capability' => 'manage_options',
                'menu_slug' => 'travpart-vouchar-help',
                'callback' => array($this->callbacks, 'travpartHelpTxt'),
            ],
        ];

        return $this;
    }

    public function registerMenu()
    {
        if (!empty($this->sub_pages)) {
            add_action('admin_menu', array($this, 'addAdminMenu'));
        }
    }

    public function addAdminMenu()
    {        
        foreach ($this->sub_pages as $page) {
            add_submenu_page($page['parent_slug'], $page['page_title'], $page['menu_title'], $page['capability'], $page['menu_slug'], $page['callback']);
        }
    }

}
