<?php

/**
 * @package  TravpartVo
 */

namespace Inc\Pages;

use Inc\Api\Callback\VoucherSchemesCallback;
use Inc\Base\BaseController;

class VoucherSchemes extends BaseController
{
    public $callbacks;

    public function register()
    {

        $this->callbacks = new VoucherSchemesCallback();

        add_action('init', array($this, 'registerTravpartPostType'));
        add_action('add_meta_boxes', array($this->callbacks, 'voucherSchemeMetaBox'));
        add_action('save_post', array($this->callbacks, 'voucherSchemeSaveMetaBox'));

    }

    public function registerTravpartPostType()
    {
        register_post_type('voucher_scheme', array(
            'labels' => array(
                'name' => __('Voucher Schemes', 'travpart-vo'),
                'singular_name' => __('Voucher Scheme', 'travpart-vo'),
                'add_new' => __('Add New', 'travpart-vo'),
                'add_new_item' => __('Add New Voucher Scheme', 'travpart-vo'),
                'new_item' => __('New Voucher Scheme', 'travpart-vo'),
                'edit_item' => __('Edit Voucher Scheme', 'travpart-vo'),
                'view_item' => __('View Voucher Scheme', 'travpart-vo'),
                'all_items' => __('Voucher Schemes', 'travpart-vo'),
                'search_items' => __('Search Voucher Scheme', 'travpart-vo'),
                'not_found' => __('No Voucher Scheme found.', 'travpart-vo'),
                'not_found_in_trash' => __('No Voucher Scheme found in Trash.', 'travpart-vo'),
            ),
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => 'travpart-vouchar-settings',
            'rewrite' => array('slug' => 'voucher-scheme'),
            'supports' => array('title'),
        ));

        flush_rewrite_rules();
    }

}
