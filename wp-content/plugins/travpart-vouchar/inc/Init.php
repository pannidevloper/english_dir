<?php

/**
 * @package  TravpartVo
 */

namespace Inc;

// If this file is called directly, abort!!!
defined('ABSPATH') or die();

final class Init
{

    /**
     * Store all the classes inside an array
     * @return array Full list of classes
     */
    public static function getServices()
    {

        if (self::is_request('admin')) {

            return [
                Base\Enqueue::class,
                Pages\GeneralSettings::class,
                Pages\VoucherSchemes::class,
                Pages\Help::class,
            ];

        } else {
            
            // Front end scope will be here.
            return [

            ];
        }
    }

    /**
     * Loop through classes, initialize and call class method
     */
    public static function registerServices()
    {

        foreach (self::getServices() as $class) {
            $service = self::instantiate($class);
            if (method_exists($service, 'register')) {
                $service->register();
            }
        }

    }

    /**
     * Initialize /**
     * @param $class class names
     */
    public static function instantiate($class)
    {
        return new $class;
    }



    /**
     * What type of request is this?
     *
     * @param  string $type admin or frontend.
     * @return bool
     */

    public static function is_request($type)
    {
        switch ($type) {
            case 'admin':
                return is_admin();
            case 'frontend':
                return (!is_admin());
        }
    }

}
