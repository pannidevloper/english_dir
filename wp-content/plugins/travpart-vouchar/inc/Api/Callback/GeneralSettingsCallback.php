<?php
/**
 * @package  TravpartVo
 */

namespace Inc\Api\Callback;

use Inc\Base\BaseController;

class GeneralSettingsCallback extends BaseController
{

    public $data = [
        'tp_pts_fb_5_str' => '',
        'tp_pts_fb_4_str' => '',
        'tp_pts_fb_3_str' => '',
        'tp_pts_fb_2_str' => '',
        'tp_pts_fb_1_str' => '',
        'tp_pts_lg_dly' => '',
        'tp_pts_lg_wkly' => '',
        'tp_pts_tor_pkg' => '',
        'tp_pts_refr' => '',
        'tp_cpn_valdty' => '',
        'tp_ply_tm' => '',
		'tp_coupon_display_kept_day' => '',
		'tp_game_enable' => '',
    ];

    public function travpartSettings()
    {

        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'travpart-vo'));
        }        

        $travpart_vo_option = get_option('_travpart_vo_option', null);

        require_once "{$this->plugin_path}templates/settings.php";
    }

    public function manage_option()
    {

        if (wp_verify_nonce($_POST['security'], $this->_nonce) === false) {
            wp_die(__('Invalid Request! Reload your page.', 'travpart-vo'));
        }

  

        foreach ($_POST as $field => $value) {
            if (substr($field, 0, 3) !== "tp_") {
                continue;
            }

            if (!array_key_exists($field,$this->data)) {
                continue;
            }


            $this->data[$field] = intval($value);

        }


        update_option('_travpart_vo_option', $this->data);

        echo __('Saved Successfully', 'travpart-vo');
		die();
     
    }

  

}
