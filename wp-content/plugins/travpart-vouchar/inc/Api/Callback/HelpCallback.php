<?php
/**
 * @package  TravpartVo
 */

namespace Inc\Api\Callback;

use Inc\Base\BaseController;

class HelpCallback extends BaseController
{

    public function travpartHelpTxt()
    {

        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'travpart-vo'));
        }            

        require_once "{$this->plugin_path}templates/help.php";
    }

    

  

}
