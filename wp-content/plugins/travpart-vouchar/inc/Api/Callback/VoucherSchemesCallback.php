<?php
/**
 * @package  TravpartVo
 */

namespace Inc\Api\Callback;

use Inc\Base\BaseController;

class VoucherSchemesCallback extends BaseController
{

    public function voucherSchemeMetaBox()
    {
        add_meta_box('travpart-vo-settings', __('Settings', 'travpart-vo'), array($this, 'settingsLayout'), 'voucher_scheme', 'normal', 'high');
    }

    public function voucherSchemeSaveMetaBox($post_id)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        if (!isset($_POST['tp-vsicon-nonce'])) {
            return;
        }

        if (!wp_verify_nonce($_POST['tp-vsicon-nonce'], basename(__FILE__))) {
            return;
        }

        if (!current_user_can("edit_post", $post_id)):
            return $post_id;
        endif;

        $icon_args = [];
        $icon_vs = $_POST['tp_icon_vs'];
        $tpvs['pts_to_play'] = intval($_POST['tp_pts_vs']);
        $tpvs['start_date'] = ($_POST['tp_tfrm_vs']) ?? null;
        $tpvs['end_date'] = ($_POST['tp_tto_vs']) ?? null;
        $tpvs['status'] = ($_POST['tp_tmng']) ?? null;

        if ($tpvs['start_date'] != null || $tpvs['end_date'] != null) {
            $tpvs['start_date'] = date("Y-m-d", strtotime($tpvs['start_date']));
            $tpvs['end_date'] = date("Y-m-d", strtotime($tpvs['end_date']));

            if ($tpvs['start_date'] > $tpvs['end_date']) {
                return;
            }

            $tpvs['status'] = intval($tpvs['status']);
        }

        if (count($icon_vs)) {

            foreach ($icon_vs as $key => $val) {
                $icon = esc_url($val['icon']) ?? null;
                $args = esc_textarea($val['args']) ?? null;
				$discount = esc_textarea($val['discount']) ?? null;
				$figure = esc_textarea($val['figure']) ?? null;
				$probability = esc_textarea($val['probability']) ?? null;
				$availability = esc_textarea($val['availability']) ?? null;

                if (true) {
					$discount=empty($discount)?'$':$discount;
					$figure=empty($figure)?0:floatval($figure);
					$probability=empty($probability)?0:floatval($probability);
					$availability=empty($availability)?0:floatval($availability);
                    $icon_args[$key]['icon'] = '';
                    $icon_args[$key]['args'] = $discount.'|'.$figure.'|'.$probability.'|'.$availability;
					$icon_args[$key]['discount'] = $discount;
					$icon_args[$key]['figure'] = $figure;
					$icon_args[$key]['probability'] = $probability;
					$icon_args[$key]['availability'] = $availability;
                }
            }

        }

        $tpvs['icon'] = $icon_args;

        update_post_meta($post_id, '_tp_vsicon_args', $tpvs);

    }

    public function settingsLayout($post)
    {

        $tp_vsicon_args = get_post_meta($post->ID, '_tp_vsicon_args', true);

        $counter = 0;

        wp_nonce_field(basename(__FILE__), 'tp-vsicon-nonce');
        require_once "{$this->plugin_path}templates/voucher_scheme.php";

    }

}
