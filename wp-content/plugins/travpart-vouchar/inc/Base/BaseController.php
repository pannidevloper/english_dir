<?php

/**
 * @package  TravpartVo
 */

namespace Inc\Base;

class BaseController
{

    /**
     * TravpartVo plugin path
     */
    public $plugin_path = '';

    /**
     * TravpartVo plugin url
     */
    public $plugin_url = '';

    /**
     * TravpartVo version.
     */
    public $version = '1.0.0';

    /**
     * TravpartVo plugin base relative path.
     */
    public $plugin;

     /**
     * wp_create_nonce
     */

    public $_nonce = 'travpartvo_admin';

    public function __construct()
    {

        $this->plugin_path = plugin_dir_path(dirname(__FILE__, 2));
        $this->plugin_url = plugin_dir_url(dirname(__FILE__, 2));
        $this->plugin = plugin_basename(dirname(__FILE__, 3)) . '/travpart-vouchar.php';
    }

    public function activated(string $key)
    {
        $option = get_option('_travpart_vo_option', null);

        return isset($option[$key]) ? $option[$key] : false;
    }

    public function getOption(string $key)
    {
        $option = get_option('_travpart_vo_option', null);

        return isset($option[$key]) ? $option[$key] : false;
    }

}
