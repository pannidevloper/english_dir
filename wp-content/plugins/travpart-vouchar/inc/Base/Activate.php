<?php

/**
 * @package  TravpartVo
 */

namespace Inc\Base;

class Activate
{

    public static function activate()
    {
        flush_rewrite_rules();

        $default = array();

        if (!get_option('_travpart_vo_option')) {
            update_option('_travpart_vo_option', $default);
        }
    }

}
