<?php

/**
 * @package  TravpartVo
 */

namespace Inc\Base;

use Inc\Base\BaseController;

/**
 *
 */
class Enqueue extends BaseController
{

    public function register()
    {
        add_action('admin_enqueue_scripts', array($this, 'enqueue'));
    }

    public function enqueue()
    {
        wp_enqueue_media();
        wp_enqueue_style('travpartvo-admin', "{$this->plugin_url}assets/css/admin.css", false, 1.0);
        wp_enqueue_script('travpartvo-admin', "{$this->plugin_url}assets/js/admin.js", array('jquery'), 1.0);

        $admin_options = array(
            'ajax_url' => admin_url('admin-ajax.php'),
            '_nonce' => wp_create_nonce($this->_nonce),
        );

        wp_localize_script('travpartvo-admin', 'travpartvo_exchanger', $admin_options);

    }

}
