(function ($) {
    $(document).ready(function () {

        $(document).on('submit', '#travpart-settings-form', function (e) {

            e.preventDefault();

            $(this).append('<input type="hidden" name="action" value="travpartvo_settings_data" />');
            $(this).append('<input type="hidden" name="security" value="' + travpartvo_exchanger._nonce + '" />');

            jQuery.ajax({
                url: travpartvo_exchanger.ajax_url,
                type: 'post',
                data: $(this).serialize(),
                success: function (response) {
                    $("html,body").scrollTop(0);
                    $("#travpart-updated").html("<p><strong>" + response + "</strong></p>").show();
                }
            });
        });
    });

})(jQuery);