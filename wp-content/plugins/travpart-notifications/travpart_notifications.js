/**
	Javascript code for notifications plugin
*/

$(document).ready(function() {

	// Handles notification dismissal
	$('.notificationDismiss').click(function() {
		var notificationId = $(this).data('notification');
		//alert(notificationId); 
		//var idRegex = /notification([\d+]*)/g;
		//var notificationId = idRegex.exec(notificationSpan.attr('id'))[1];

		// Dismiss server side
		$.ajax({ 
			url: notifications_vars.ajaxurl
				+ '?action=notifications_dismiss&notification_id=' + notificationId
		}).done(function() {
			// Hide the dismissed notification
			$('.notifica_'+notificationId).hide();
			//notificationSpan.css('display', 'none');
		});
		return false;
	});
	// Handles marking notifications as read
	//$('.notification').children('a').filter(':first').click(function() {
	$('.notification').click(function() {
		
		var notificationId = $(this).data('notid');
		//var idRegex = /notification([\d+]*)/g;
		//var notificationId = idRegex.exec(notificationSpan.attr('id'))[1];

		// Mark read server side
		$.ajax({
			url: notifications_vars.ajaxurl
				+ '?action=notifications_markread&notification_id=' + notificationId
		}).done(function() {
			// Hide the dismissed notification
			$('.notifica_'+notificationId).hide();
		});

		return true;
	});

});