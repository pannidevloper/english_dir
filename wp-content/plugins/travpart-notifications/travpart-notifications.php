<?php
	/*
		Plugin Name: Travpart Notifications
		Description: Displays latest notifications
		Version: 1.0
		Author: Lionel
	*/

	/**
		Displays notifications for the currently logged-in user
	*/
	function notifications_display() {
		global $wpdb;

		// Get currently logged-in user
		$current_user = wp_get_current_user();
		$user_id = $current_user->ID;

		// Fetch the notifications from the database
		$query = $wpdb->prepare('SELECT id, read_status, content FROM notification '
			. 'WHERE wp_user_id = %d ORDER BY read_status, create_time DESC LIMIT 4', 899);
		$results = $wpdb->get_results($query);

		//echo '<div id="notifications">';

		// Iterate result
		//$results=array();
		if(count($results)>0){
			foreach ($results as $notification) {
				$id = $notification->id;
				$read_status = $notification->read_status;
				$content = $notification->content;

				// Check unread and display appropriately
				if (empty($read_status))
					
					echo '<div class="row_c notifica_' . $id . '"><div class="col-mmd-2" style="width: 20%;display: table-cell;vertical-align: middle;text-align: center;"><a href="#"><i class="fas fa-circle" style="color: maroon;font-size: 15px;"></i></a></div><div class="col-mmd-8" style="width: 60%;display: table-cell;vertical-align: middle;" id="notification' . $id . '"><p style="margin: 0px;" class="notification unread" data-notid="' . $id . '">' . $content . ' </p></div><div class="col-mmd-2" style="width: 20%;display: table-cell;vertical-align: middle;text-align: center;"><a href="#"  class="notificationDismiss"  data-notification="'. $id . '"><i class="fas fa-times" style="color: #22b14c;font-size: 15px;"></i></a></div></div>';
					//echo '<div  class="notification unread"> ';
				
				else {
					echo '<div class="row_c notifica_' . $id . '"><div class="col-mmd-2" style="width: 20%;display: table-cell;vertical-align: middle;text-align: center;"><a href="#"><i class="fas fa-circle" style="color: maroon;font-size: 15px;"></i></a></div><div class="col-mmd-8" style="width: 60%;display: table-cell;vertical-align: middle;" id="notification' . $id . '"><p style="margin: 0px;" class="notification read">' . $content . ' </p></div><div class="col-mmd-2" style="width: 20%;display: table-cell;vertical-align: middle;text-align: center;"><a href="#" class="notificationDismiss" data-notification="' . $id . '"><i class="fas fa-times" style="color: #22b14c;font-size: 15px;"></i></a></div></div>';
					//echo '<div id="notification' . $id . '" class="notification read">' . $content . ' ';
				 } 

				 // Add dismiss button
				 //echo '<a href="#" class="">&times;</a>';

				 //echo '</div>';
			}
		}else{
			echo "<div class='notification_nfound'>You currently don't have any Notification</div>";
		}
		

		//echo '</div>';
	}

	/**
		Dismiss a notification
	*/
	function notifications_dismiss() {
		global $wpdb;

		// Get notification ID
		$notificationId = $_REQUEST['notification_id'];

		// Get currently logged-in user
		$current_user = wp_get_current_user();
		$user_id = $current_user->ID;

		// Remove the notifications from the database
		$query = $wpdb->prepare('DELETE FROM notification '
			. 'WHERE id = %d AND wp_user_id = %d', $notificationId, $user_id);
		$wpdb->query($query);
	}

	/**
		Marks a notification as read
	*/
	function notifications_markread() {
		global $wpdb;

		// Get notification ID
		$notificationId = $_REQUEST['notification_id'];
		// Get currently logged-in user
		$current_user = wp_get_current_user();
		$user_id = $current_user->ID;
		// Remove the notifications from the database
		$query = $wpdb->prepare('UPDATE notification SET read_status = 1 '
			. 'WHERE id = %d AND wp_user_id = %d', $notificationId, $user_id);
		$wpdb->query($query);
	}

	/**
		Enqueue the scripts for notifications
	*/
	function notifications_addscripts() {
		$ajaxurl = admin_url('admin-ajax.php', 
			isset($_SERVER['HTTPS'] ) ? 'https://' : 'http://');

		wp_register_script('travpart_notifications', plugins_url('travpart_notifications.js', __FILE__));
		wp_localize_script('travpart_notifications', 'notifications_vars', 
			array('ajaxurl' => $ajaxurl));  
		wp_enqueue_script('travpart_notifications');
	}

	// Set up the actions
	add_action('wp_ajax_notifications_dismiss', 'notifications_dismiss');
	add_action('wp_ajax_notifications_markread', 'notifications_markread');
	add_action('wp_enqueue_scripts', 'notifications_addscripts');

	add_shortcode('notifications', 'notifications_display');
?>