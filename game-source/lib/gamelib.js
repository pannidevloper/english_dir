var stage, background, centerPos;
var images = images || {};

const medalSpritesheet = new createjs.SpriteSheet({
    images:getSpriteSources(medalImages),
    frames:{width:137, height:137}
});

const iconSpritesheet = new createjs.SpriteSheet({
    images:getSpriteSources(icons),
    frames:{width:220, height:220}
});

var medals = [];
var lastMedal, stopMedal, targetMedal, targetMedalCount, medalHalfWidth;
const medalContainer = new createjs.Container();
const medalSpacing = 150;
const medalSpeed = 350;
var requestedStopMedal;
var stopPosition;

var isRolling = false;
var rollSpeed = 0;

var buttonPlay, debugText, spark, targetImage, loadingText;

function init() {
    stage = new createjs.Stage("canvas");
    stage.canvas.style.backgroundColor = "#444444";

    loadingText = new createjs.Text("Loading...", "20px Verdana", "#ffffff");
    stage.addChild(loadingText);
    stage.update();

    centerPos = {x:stage.canvas.width / 2, y:stage.canvas.height / 2};

    var preloader = new createjs.LoadQueue(false);
    preloader.on("fileload", handleFileLoad, this);
    preloader.on("complete", handleComplete, this);
    preloader.loadManifest(manifest);
    preloader.loadManifest(icons);
    preloader.loadManifest(medalImages);
}

function handleFileLoad(event) {
    var item = event.item; // A reference to the item that was passed in to the LoadQueue
    var type = item.type;

    // Add any images to the page body.
    if (type == createjs.Types.IMAGE) {
        //console.log("Loaded " + item.id);
        images[item.id] = item;
    }
}

function handleComplete() {
    //console.log("Complete Loading");

    setTimeout(initStage, 2000);
}

function initStage() {

    stage.removeChild(loadingText);

    // Background
    background = new createjs.Bitmap(manifest[0].src);
    stage.addChild(background);

    // Spark Effect
    spark = new createjs.Bitmap(manifest[2].src);
    //centerAnchor(spark, true);
    centerFixed(spark, {width:392, height:400}, true);
    spark.scale = 2;
    spark.visible = false;
    stage.addChild(spark);

    // Medal Container
    stage.addChild(medalContainer);

    // Target Image
    targetImage = new createjs.Bitmap(manifest[3].src);
    //centerAnchor(targetImage, true);
    centerFixed(targetImage, {width:225, height:225}, true);
    stage.addChild(targetImage);

    // Button Play
    buttonPlay = new createjs.Container();
    buttonPlay.x = centerPos.x;
    buttonPlay.y = centerPos.y + 250;
    buttonPlay.addChild(centerFixed(new createjs.Bitmap(manifest[1].src), {width:230, height:61}));
    const buttonLabel = new createjs.Text(DisplayText.play, "35px Comic Sans MS", "#444444");
    buttonLabel.textAlign = "center";
    buttonLabel.textBaseline = "middle";
    buttonPlay.label = buttonLabel;
    buttonPlay.addChild(buttonLabel);
    buttonPlay.addEventListener('mousedown', onPressPlayButton, this);
    buttonPlay.addEventListener('pressup', onReleasePlayButton, this);
    stage.addChild(buttonPlay);

    // Medal List
    initMedalList(7, centerPos.x - 368, medalSpacing);
    lastMedal = medals[medals.length - 1];

    // debugText = new createjs.Text("DEBUG", "20px Arial", "#ffffff");
    // stage.addChild(debugText);

    createjs.Ticker.framerate = 60;
    createjs.Ticker.addEventListener("tick", update);
}

function onPressPlayButton() {
    buttonPlay.scale = 0.95;    
}

function onReleasePlayButton() {
    buttonPlay.scale = 1;
    buttonPlay.visible = false;

    //createjs.Sound.play("click");
    click.play();

    isRolling = !isRolling;
    if (isRolling) {
        Voucher.init();
        //createjs.Sound.play("bgm", {loop:-1});
        bgm.play();
        setTimeout(showStopButton, 3000);
    } else {
        requestedStopMedal = true;
        createjs.Tween.get(background).to({alpha:0.5}, 500);
        //createjs.Sound.play("lightsaber");
        lightsaber.play();
    }
}

function showStopButton() {
    buttonPlay.label.text = DisplayText.stop;
    buttonPlay.scale = 0;
    buttonPlay.visible = true;
    createjs.Tween.get(buttonPlay).to({scaleX: 1, scaleY: 1}, 500, createjs.Ease.backOut);
}

function centerAnchor(bitmap, centerPosition) {
    const bounds = bitmap.getBounds();
    bitmap.regX = bounds.width / 2;
    bitmap.regY = bounds.height / 2;
    if (centerPosition) {
        bitmap.x = stage.canvas.width / 2;
        bitmap.y = stage.canvas.height / 2;
    }
    return bitmap;
}

function initMedalList(count, startX, spacing) {
    let posX = startX;
    for (let i = 0; i < count; i++) {
        const medal = createMedal(posX);
        medal.randomize();
        posX += spacing;
        medalContainer.addChild(medal);
        medals.push(medal);
    }
}

function createMedal(startX) {
    const medal = new createjs.Container();

    const background = new createjs.Sprite(medalSpritesheet); //new createjs.Bitmap(medalImage.src);
    const bounds = background.getBounds();
    medal.x = startX || medal.x; //400 - bounds.width / 2 * medal.scale;
    medal.y = 300 - bounds.height / 2 * medal.scale;
    medal.addChild(background);

    const icon = new createjs.Sprite(iconSpritesheet); //new createjs.Bitmap(iconImage.src);
    centerAnchor(icon);
    icon.x = bounds.width / 2;
    icon.y = bounds.height / 2;
    icon.scale = 0.3;
    medal.addChild(icon);

    medal.setDisplay = function(medalName, iconName) {
        //console.log("Set medal display: ", medalName, iconName);
        let id = 0;
        for (let i = 0; i < medalImages.length; i++) {
            if (medalImages[i].id === medalName) {
                id = i;
                break;
            }
        }
        background.gotoAndStop(id);
        for (let i = 0; i < icons.length; i++) {
            if (icons[i].id === iconName) {
                id = i;
                break;
            }
        }
        icon.gotoAndStop(id);

        medal.type = medalName;
        medal.icon = icons[id];
        medal.info = medalName + ":" + iconName;
    }

    medal.randomize = function() {
        let randId = Math.floor(Math.random() * medalImages.length);
        const medalImage = medalImages[randId];
        background.gotoAndStop(randId);

        randId = Math.floor(Math.random() * icons.length);
        const iconImage = icons[randId];
        icon.gotoAndStop(randId);

        medal.type = medalImage.id;
        medal.icon = iconImage;
        medal.info = medalImage.id + ":" + iconImage.id; 
    }

    medal.fadeOut = function() {
        createjs.Tween.get(medal).to({alpha:0}, 500);
    }

    return medal;
}

function getSpriteSources(spriteList) {
    const list = [];
    for (let i = 0; i < spriteList.length; i++) {
        list.push(spriteList[i].src);
    }
    return list;
}

function update(event) {

    const deltaTime = event.delta / 1000;

    if (isRolling) {
        if (rollSpeed < medalSpeed) rollSpeed += deltaTime * 100;
        else rollSpeed = medalSpeed;
    }
    else if (stopMedal != null && rollSpeed > 0) {
        if (rollSpeed > 0) {
            rollSpeed = lerp(stopMedal.x, centerPos.x, event.delta / 60);
            if (rollSpeed > medalSpeed) rollSpeed = medalSpeed;
        }
        if (targetMedal && targetMedal.x + medalHalfWidth < centerPos.x) {
            rollSpeed = 0;
            onStopRolling();
        }
        else if (rollSpeed <= 0) {
            rollSpeed = deltaTime * 30;
        }
        //debugText.text = "Target Medal X: " + (targetMedal? targetMedal.x : 0) + ", Roll Speed: " + rollSpeed;
    }

    if (rollSpeed > 0) {
        for (let i = 0; i < medals.length; i++) {
            const medal = medals[i];
    
            medal.x -= deltaTime * rollSpeed;
    
            if (medal.x < -200) {
                
                medal.x = lastMedal.x + medalSpacing;
                lastMedal = medal;

                //createjs.Sound.play("click");
                click.play();

                if (targetMedalCount > 0) {
                    targetMedalCount--;
                    if (targetMedalCount == 0) {
                        targetMedal = medal;
                        const voucher = Voucher.request();
                        targetMedal.setDisplay(voucher.medal, voucher.icon);
                        medalHalfWidth = targetMedal.getBounds().width / 2;
                        //console.log("Target Medal " + medal.info);                        
                    }
                } else {
                    medal.randomize();
                }

                if (requestedStopMedal) {
                    stopMedal = medal;
                    requestedStopMedal = false;
                    targetMedalCount = 2;
                }
            }
        }
    }

    if (spark.visible) {
        spark.rotation += deltaTime * 50;
    }

    stage.update();
}

function lerp (start, end, amt){
    return (1-amt)*start+amt*end
}

var Voucher = {
    weights:[],
    weightTotal:0,
    icons:{
        flightgold:0,
        flightsilver:1,
        mealgold:2,
        mealsilver:3,
        vehiclegold:4,
        vehiclesilver:5,
        massagegold:6,
        massagesilver:7,
        helicoptergold:8,
        helicoptersilver:9,
        hotelgold:10,
        hotelsilver:11
    },
    init:function() {
        this.weightTotal = 0;
        const data = window.PROB || "0.1, 0.2, 15, 20, 5, 7, 15, 25, 0.2, 0.8, 5, 10";
        const probability = data.split(',');
        for (let i = 0; i < probability.length; i++) {
            const prob = parseFloat(probability[i]);
            this.weights.push(prob);
            this.weightTotal += prob;
        }
    },
    randomWeighted:function() {
        let result = 0; 
        let total = 0;
        const randVal = Math.floor(Math.random() * this.weightTotal);
        for (result = 0; result < this.weights.length; result++)
        {
            total += this.weights[result];
            if (total > randVal) break;
        }
        //console.log("Random Weighted. Total : " + this.weightTotal + ", randVal: " + randVal + ", result: " + result);   
        return result;
    },
    request:function() {
        const GOLD_MEDAL = "gold";
        const SILVER_MEDAL = "silver";
        const FLIGHT = "flight";
        const MEAL = "meal";
        const VEHICLE = "vehicle";
        const MASSAGE = "massage";
        const HELICOPTER = "helicopter";
        const HOTEL = "hotel";

        let medalName, iconName;
        
        switch (this.randomWeighted())
        {
            case this.icons.flightgold:
                medalName = GOLD_MEDAL;
                iconName = FLIGHT;
                break;
            case this.icons.flightsilver:
                medalName = SILVER_MEDAL;
                iconName = FLIGHT;
                break;
            case this.icons.mealgold:
                medalName = GOLD_MEDAL;
                iconName = MEAL;
                break;
            case this.icons.mealsilver:
                medalName = SILVER_MEDAL;
                iconName = MEAL;
                break;
            case this.icons.vehiclegold:
                medalName = GOLD_MEDAL;
                iconName = VEHICLE;
                break;
            case this.icons.vehiclesilver:
                medalName = SILVER_MEDAL;
                iconName = VEHICLE;
                break;
            case this.icons.massagegold:
                medalName = GOLD_MEDAL;
                iconName = MASSAGE;
                break;
            case this.icons.massagesilver:
                medalName = SILVER_MEDAL;
                iconName = MASSAGE;
                break;
            case this.icons.helicoptergold:
                medalName = GOLD_MEDAL;
                iconName = HELICOPTER;
                break;
            case this.icons.helicoptersilver:
                medalName = SILVER_MEDAL;
                iconName = HELICOPTER;
                break;
            case this.icons.hotelgold:
                medalName = GOLD_MEDAL;
                iconName = HOTEL;
                break;
            case this.icons.hotelsilver:
                medalName = SILVER_MEDAL;
                iconName = HOTEL;
                break;
        }

        return {medal:medalName, icon:iconName};
    }
};

function onStopRolling() {
    createjs.Tween.get(background).to({alpha:1}, 1000);

    spark.alpha = 0;
    spark.visible = true;
    createjs.Tween.get(spark).to({alpha:1}, 1500);
    createjs.Tween.get(targetImage).to({alpha:0}, 500);

    //createjs.Sound.play("tada");
    tada.play();

    for (let i = 0; i < medals.length; i++) {
        const medal = medals[i];
        if (medal != targetMedal) medal.fadeOut();
    }

    // Popup Dialog
    const popup = new createjs.Container();
    popup.addChild(new createjs.Bitmap("img/box.png"));
    const popupBounds = {width:363, height:485}

    testObject = popup;

    const iconImage = new createjs.Bitmap(targetMedal.icon.src);
    centerAnchor(iconImage);
    iconImage.x = popupBounds.width / 2;
    iconImage.y = popupBounds.height / 2 - 50;
    popup.addChild(iconImage);

    const displayTextName = targetMedal.icon.id + targetMedal.type;
    //console.log("Show display text " + displayTextName);
    const displayText = new createjs.Text(DisplayText[displayTextName], "30px Arial", "#fcf9b1");
    displayText.textAlign = "center";
    displayText.x = popupBounds.width / 2;
    displayText.y = popupBounds.height / 2 + 80;
    popup.addChild(displayText);

    //centerAnchor(popup, true);
    centerFixed(popup, popupBounds, true);
    stage.addChild(popup);

    popup.visible = false;
    
    setTimeout(function() {
        popup.scale = 0;
        popup.visible = true;
        createjs.Tween.get(popup).to({scaleX:1, scaleY:1}, 500, createjs.Ease.backOut);

        window.RES = targetMedal.info;
        if (window.onResultGet) window.onResultGet(window.RES);
    }, 1500);
    
}

function centerFixed(obj, bounds, centerPosition) {
    obj.regX = bounds.width / 2;
    obj.regY = bounds.height / 2;
    if (centerPosition) {
        obj.x = stage.canvas.width / 2;
        obj.y = stage.canvas.height / 2;
    } 
    return obj;
}