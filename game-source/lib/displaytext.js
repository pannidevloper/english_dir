var DisplayText = {
    play:"Play",
    stop:"Stop",
    flightgold:"Flight 10% discount",
    flightsilver:"Flight 5% discount",
    mealgold:"Meal 20% discount",
    mealsilver:"Meal 40% discount",
    vehiclegold:"Vehicle 40% discount",
    vehiclesilver:"Vehicle 50% discount",
    massagegold:"Massage 40% discount",
    massagesilver:"Massage 50% discount",
    helicoptergold:"Helicopter 10% discount",
    helicoptersilver:"Helicopter 5% discount",
    hotelgold:"Hotel 10% discount",
    hotelsilver:"Hotel 5% discount"
};